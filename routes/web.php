<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

/*
Route::get('/', function () {
    return view('home');
    //return view('welcome');
});
*/

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');
Route::get('/', 'HomeController@index')->name('home');

Route::get('/deuda_real_2018', 'HomeController@getDeudaReal');
Route::get('/restan_deuda', 'HomeController@getRestanDeuda');

Route::get('/home/indicadores', 'HomeController@getIndicadores');

/**
 * Buscador, es para select con ajax
 */
Route::get("buscador_proveedor_nombre/","BuscadorController@buscaProveedorNombre");
Route::get("buscador_proveedor_rut/","BuscadorController@buscaProveedorRut");
Route::get("buscador_proveedor/","BuscadorController@buscaProveedorNombreRut");

/**
 * Documentos ex facturas
 */
Route::resource('documentos','DocumentosController');

Route::get("documentos/test/obtener_oc", "DocumentosController@getOrdenCompra");

Route::get("documentos/test/actualiza_validacion", "DocumentosController@getActualizaValidacion");

Route::get("documentos/test/actualiza_montos/{year}/{trimestre}","DocumentosController@getActualizaMontos");

Route::get("documentos/test/obtener_archivos","DocumentosController@getArchivosDocumentos");

Route::get("documentos/test/relaciones_duplicadas","DocumentosController@getRelacionesDuplicadas");

Route::post("documentos/data_table_index","DocumentosController@dataTableIndex");

Route::get("documentos/buscador/docs","DocumentosController@getBuscadorDocumentos");
Route::post("documentos/data_table_buscador","DocumentosController@dataTableBuscador");
Route::post("documentos/buscador/excel","DocumentosController@postBuscadorExcel");


Route::get("documentos/listado/eliminados", "DocumentosController@getDocumentosEliminados");
Route::post("documentos/data_table_eliminados","DocumentosController@dataTableEliminados");
Route::post("documentos/eliminados/excel", "DocumentosController@postEliminadosExcel");
Route::get("documentos/modal/recuperar/{id}","DocumentosController@getModalRecuperarDocumento");
Route::post("documentos/recuperar","DocumentosController@postRecuperarDocumento");

Route::get("documentos/generar/pdf_trazabilidad_documento/{id}","DocumentosController@getGenerarPDFTrazabilidadDocumento");
Route::get("documentos/imprimir/todos_los_archivos/{id}","DocumentosController@getImprmirTodosLosArchivos");

Route::get("documentos/get_proveedor/{id_proveedor}","DocumentosController@getProveedor");
Route::get("documentos/get_oc_proveedor/{idProveedor}","DocumentosController@getOcProveedor");
Route::get("documentos/modal/agregar_item/{ids_items}/{idDocBodega}","DocumentosController@getModalAgregarItem");
Route::get("documentos/obtener_item/{id_item}","DocumentosController@getItem");
Route::get("documentos/get_documentos_proveedor/{id_proveedor}/{id_tipo_documento}/{id_documento?}","DocumentosController@getDocumentosProveedor");
Route::get("documentos/get_facturas_rechazadas_acepta/{idProveedor}","DocumentosController@getFacturasRechazadasArchivosAceptaProveedor");

Route::get("documentos/get_documentos_bodega_proveedor/{idProveedor}/{idTipoDocumento}","DocumentosController@getDocumentosBodegaProveedor");
Route::get("documentos/get_guias_despacho_bodega_proveedor/{idProveedor}","DocumentosController@getGuiasDespachoBodegaProveedor");
Route::get("documentos/modal/ver/{id_documento}/{eliminado?}","DocumentosController@getModalVerDocumento");
Route::get("documentos/modal/traza/{id}/{eliminado?}","DocumentosController@getModalTrazaDocumento");
Route::get("documentos/modal/editar/{id_documento}/{datatable?}","DocumentosController@getModalEditarDocumento");
Route::post("documentos/editar/","DocumentosController@postEditarDocumento");
Route::get("documentos/get_predeterminado_modalidad_compra/{id_modalidad_compra}","DocumentosController@getPredeterminadoModalidadCompra");
Route::get("documentos/modal/eliminar/{id_documento}","DocumentosController@getModalEliminarDocumento");
Route::post("documentos/eliminar/","DocumentosController@postEliminarDocumento");

Route::get("documentos/upload/masivo","DocumentosController@getUpload");
Route::post("documentos/upload/masivo","DocumentosController@postUpload");

Route::post("documentos/filtrar/","DocumentosController@postFiltrarDocumentos");
Route::post("documentos/general/excel/","DocumentosController@postGenerarExcel");

Route::get("documentos/listado/bodega","DocumentosController@geDocumentosConBodega");
Route::post("documentos/listado/bodega/filtrar","DocumentosController@postFiltrarDocumentosConBodega");
Route::get("documentos/modal/conectar/{idDocumento}/{idWsDocumento}","DocumentosController@getModalConectarDocumentos");
Route::post("documentos/conectar/","DocumentosController@postConectarDocumentos");

Route::get("documentos/modal/desconectar/{idDocumento}/{idWsDocumento?}","DocumentosController@getModalDesconectarDocumentos");
Route::post("documentos/desconectar/","DocumentosController@postDesconectarDocumentos");

Route::get("documentos/listado/bodega/listos","DocumentosController@getDocumentosConBodegaListos");
Route::post("documentos/listado/bodega/listos/filtrar","DocumentosController@postFiltrarDocumentosConBodegaListos");

Route::get("documentos/num/bodega/por_confirmar","DocumentosController@getNumDocumentosConRecepcionPorConfirmar");
Route::get("documentos/num/rechazo_visto_bueno","DocumentosController@getNumDocumentosRechazoVistoBueno");

Route::get("documentos/listado/sin_bodega/","DocumentosController@getDocumentosSinBodega");
Route::post("documentos/listado/sin_bodega/filtrar","DocumentosController@postFiltrarDocumentosSinBodega");
Route::post("documentos/sin_bodega/excel", "DocumentosController@postSinBodegaExcel");

Route::get("documentos/bodega/intermediacion","DocumentosController@getDocumentosConBodegaIntermediacion");

Route::get("documentos/listado/pendiente_validacion/{filtroHome?}","DocumentosController@geDocumentosPendientesValidacion");
Route::post("documentos/data_table_pendiente_validacion","DocumentosController@dataTablePendientesValidacion");
Route::post("documentos/listado/pendiente_validacion/excel","DocumentosController@postPendienteValidacionExcel");

Route::get("documentos/modal/problema/{id}","DocumentosController@getModalProblemaDocumento");
Route::post("documentos/problema/","DocumentosController@postProblemaDocumento");

Route::get("documentos/listado/con_problemas/","DocumentosController@getDocumentosConProblema");
Route::post("documentos/data_table_con_problemas","DocumentosController@dataTableConProblemas");
Route::post("documentos/listado/con_problemas/excel","DocumentosController@postDocumentosConProblemaExcel");


Route::get("documentos/modal/agregar_observacion_problema/{id}","DocumentosController@getModalAgregarObservacionProblemaDocumento");
Route::post("documentos/agregar_observacion_problema","DocumentosController@postAgregarObservacionProblemaDocumento");

Route::get("documentos/modal/quitar_problema/{id}","DocumentosController@getModalQuitarProblemaDocumento");
Route::post("documentos/quitar_problema","DocumentosController@postQuitarProblema");

Route::get("documentos/listado/validados/","DocumentosController@geDocumentosValidados");
Route::post("documentos/listado/validados/filtrar/","DocumentosController@postFiltrarDocumentosValidados");
Route::post("documentos/listado/validados/excel","DocumentosController@postValidadosExcel");

Route::get("documentos/listado/rechazo_visto_bueno/","DocumentosController@geDocumentosRechazoVistoBueno");
Route::post("documentos/listado/rechazo_visto_bueno/filtrar/","DocumentosController@postFiltrarDocumentosRechazoVistoBueno");

Route::get("documentos/get_items_presupuestarios_recepcion/{ids_items}/{idDocumentoRecepcion}","DocumentosController@getItemsPresupuestariosDocRecepcion");

Route::get("documentos/modal/imprimir_documento/{id}","DocumentosController@getModalImprimirDocumento");

/**
 * Devengo
 */
Route::get('devengar/test_doc','DevengoController@test');

Route::get('devengar','DevengoController@getDevengarDocumentos');
Route::post("devengar/data_table_devengar","DevengoController@dataTableDevengar");

Route::post("devengar/filtrar/","DevengoController@postFiltrarDocumentos");
Route::post("devengar/excel/","DevengoController@postDevengarDocumentosExcel");
Route::get('devengar/modal/{idDocumento}/{idFolioSigfe?}','DevengoController@getModalDevengar');
Route::post("devengar/devengar_documento","DevengoController@postDevengarDocumento");
Route::get('devengar/devengados','DevengoController@getDocumentosDevengados');
Route::post("devengar/data_table_devengados","DevengoController@dataTableDocumentosDevengados");
Route::post("devengar/devengados/excel/","DevengoController@postGenerarExcel");

Route::get("devengar/upload/masivo","DevengoController@getUpload");
Route::post("devengar/upload/masivo","DevengoController@postUpload");

Route::get("devengar/upload/sigfe","DevengoController@getCargaSigfe");
Route::post("devengar/upload/sigfe","DevengoController@postCargaSigfe");

Route::get("devengar/upload/items_sigfe","DevengoController@getCargaItemsSigfe");
Route::post("devengar/upload/items_sigfe","DevengoController@postCargaItemsSigfe");

Route::get("devengar/modal/carga_sigfe/{idProveedor}/{idTipoDoc}/{numeroDoc}/{totalDoc}/{idSigfe}/{fechaSigfe}/{keyTr}",
            "DevengoController@getModalCargaSigfe");
Route::post("devengar/carga_sigfe_a_sistema","DevengoController@postIngresarDocumentoDesdeSigfe");

// Devengo 2.0
Route::get('devengar/version_2','DevengoController@getDevengarDocumentosDos');
Route::post("devengar/data_table_por_devengar_2","DevengoController@dataTablePorDevengarDos");

Route::post("devengar/version_2/excel/","DevengoController@postDevengarDocumentosExcelDos");
Route::get("devengar/home/excel/{year}","DevengoController@getDevengarDocumentosExcelHome");

Route::get('devengar/modal_eliminar_devengo/{id}','DevengoController@getModalEliminarDevengo');
Route::post("devengar/eliminar_devengo","DevengoController@postEliminarDevengo");

/**
 * Recepcion de documentos en bodega
 */
// Route::resource('recepcion_bodega','DocumentoBodegaController');

Route::get("obtener_codigo_pedido","DocumentoBodegaController@getCodigoPedido");

Route::get("recepcion_bodega","DocumentoBodegaController@index");
Route::post("recepcion_bodega/data_table_index","DocumentoBodegaController@dataTableIndex");
Route::post("recepcion_bodega/index/excel","DocumentoBodegaController@postIndexExcel");

Route::get("recepcion_bodega/match","DocumentoBodegaController@getListadoMatch");
Route::post("recepcion_bodega/data_table_match","DocumentoBodegaController@dataTableMatch");
Route::post("recepcion_bodega/match/excel","DocumentoBodegaController@postMatchExcel");

Route::get("recepcion_bodega/match_pendiente","DocumentoBodegaController@getListadoMatchPendiente");
Route::post("recepcion_bodega/data_table_match_pendiente","DocumentoBodegaController@dataTableMatchPendiente");
Route::post("recepcion_bodega/match_pendiente/excel","DocumentoBodegaController@postMatchPendienteExcel");

Route::post("recepcion_bodega/filtrar","DocumentoBodegaController@postFiltrarDocumentosBodega");
Route::get("recepcion_bodega/modal/ver/{id}","DocumentoBodegaController@getModalVer");
Route::get("recepcion_bodega/modal/errores/{id}","DocumentoBodegaController@getModalErrores");
Route::post("recepcion_bodega/modal/errores","DocumentoBodegaController@postModalErrores");

Route::get("recepcion_bodega/listado/sin_documento/","DocumentoBodegaController@getListadoSinDocumento");
Route::post("recepcion_bodega/data_table_sin_documento","DocumentoBodegaController@dataTableSinDocumento");
Route::post("recepcion_bodega/sin_documento/excel", "DocumentoBodegaController@postSinDocumentoExcel");

Route::get("recepcion_bodega/listado/oc/","DocumentoBodegaController@getListadoOrdenCompra");


Route::get("recepcion_bodega/upload/contrato/masivo","DocumentoBodegaController@getUploadContrato");
Route::post("recepcion_bodega/upload/contrato/masivo","DocumentoBodegaController@postUploadContrato");

Route::get("recepcion_bodega/upload/orden_compra/masivo","DocumentoBodegaController@getUploadOrdenCompra");
Route::post("recepcion_bodega/upload/orden_compra/masivo","DocumentoBodegaController@postUploadOrdenCompra");

Route::get("recepcion_bodega/upload/documento/masivo","DocumentoBodegaController@getUploadDocumento");
Route::post("recepcion_bodega/upload/documento/masivo","DocumentoBodegaController@postUploadDocumento");

Route::get("recepcion_bodega/upload/item/masivo","DocumentoBodegaController@getUploadItem");
Route::post("recepcion_bodega/upload/item/masivo","DocumentoBodegaController@postUploadItem");

Route::get("recepcion_bodega/upload/archivo/masivo","DocumentoBodegaController@getUploadArchivo");
Route::post("recepcion_bodega/upload/archivo/masivo","DocumentoBodegaController@postUploadArchivo");

Route::get("buscador_recepcion","DocumentoBodegaController@getBuscadorRecepcion");
Route::post("buscador_recepcion","DocumentoBodegaController@postBuscadorRecepcion");

/**
 * Archios Acepta
 */
// Route::resource('archivos_acepta','ArchivoAceptaController');

Route::get("archivos_acepta/test/cambiar_recepciones","ArchivoAceptaController@cambiarRecepciones");

Route::get("archivos_acepta/test/recepciones_hermes","ArchivoAceptaController@recepcionesHermes");

Route::get("archivos_acepta/test/contador_recepciones","ArchivoAceptaController@contadorRecepciones");

Route::get("archivos_acepta/repartir_carga","ArchivoAceptaController@getRepartirCarga");
Route::post("archivos_acepta/filtrar/repartir_carga", "ArchivoAceptaController@postFiltrarRepartirCarga");
Route::post("archivos_acepta/repartir_carga/guardar", "ArchivoAceptaController@postRepartirCargaGuardar");

Route::get("archivos_acepta/reasignar_carga","ArchivoAceptaController@getReasignarCarga");
Route::post("archivos_acepta/filtrar/reasignar_carga", "ArchivoAceptaController@postFiltrarReasignarCarga");
Route::post("archivos_acepta/reasignar_carga/guardar", "ArchivoAceptaController@postReasignarCargaGuardar");

Route::get("archivos_acepta","ArchivoAceptaController@index");
Route::post("archivos_acepta/carga_archivo/","ArchivoAceptaController@postCargaArchivo");
Route::get("archivos_acepta/sin_recepcion","ArchivoAceptaController@getDocumentosSinRecepcion");
Route::post("archivos_acepta/data_table_sin_recepcion","ArchivoAceptaController@dataTableSinRecepcion");

Route::post("archivos_acepta/sin_recepcion/excel","ArchivoAceptaController@postSinRecepcionExcel");

Route::get("archivos_acepta/grilla/modal/carga/{id_archivo_acepta}","ArchivoAceptaController@getModalCarga");
Route::post("archivos_acepta/grilla/carga_archivo/","ArchivoAceptaController@postCargaArchivoAceptaToDocumento");

Route::get("archivos_acepta/listado/bodega","ArchivoAceptaController@getArchivosConBodega");
Route::post("archivos_acepta/data_table_con_recepcion","ArchivoAceptaController@dataTableConRecepcion");
Route::post("archivos_acepta/con_recepcion/excel", "ArchivoAceptaController@postExcelConRecepcion");

Route::get("archivos_acepta/modal/posible_conexion/{idArchivoAcepta}/{idWsDocumento}","ArchivoAceptaController@getModalPosibleConexion");
Route::get("archivos_acepta/num/bodega","ArchivoAceptaController@getNumDocumentosConRecepcionPorConfirmar");

Route::get("archivos_acepta/modal/rechazar/{id}","ArchivoAceptaController@getModalRechazar");
Route::post("archivos_acepta/rechazar","ArchivoAceptaController@postRechazar");

Route::get("archivos_acepta/modal/rezagar/{id}","ArchivoAceptaController@getModalRezagar");
Route::post("archivos_acepta/rezagar","ArchivoAceptaController@postRezagar");


Route::get("archivos_acepta/listado/rechazados","ArchivoAceptaController@getArchivosRechazados");
Route::post("archivos_acepta/data_table_rechazados","ArchivoAceptaController@dataTableRechazados");
Route::post("archivos_acepta/filtrar/listado/rechazados","ArchivoAceptaController@postFiltrarRechazados");
Route::post("archivos_acepta/rechazados/excel","ArchivoAceptaController@postRechazadosExcel");


Route::get("archivos_acepta/listado/rezagados","ArchivoAceptaController@getArchivosRezagados");
Route::post("archivos_acepta/data_table_rezagados","ArchivoAceptaController@dataTableRezagados");
Route::post("archivos_acepta/rezagados/excel","ArchivoAceptaController@postRezagadosExcel");

Route::get("archivos_acepta/modal/quitar_rechazo/{id}","ArchivoAceptaController@getQuitarRechazo");
Route::post("archivos_acepta/quitar_rechazo/","ArchivoAceptaController@postQuitarRechazo");

Route::get("archivos_acepta/varios/upload/masivo","ArchivoAceptaController@getUpload");
Route::post("archivos_acepta/varios/upload/masivo","ArchivoAceptaController@postUpload");

Route::get("archivos_acepta/excels/upload/masivo","ArchivoAceptaController@getUploadExcel");
Route::post("archivos_acepta/excels/upload/masivo","ArchivoAceptaController@postUploadExcel");

Route::get("archivos_acepta/excels/archivos/upload/masivo","ArchivoAceptaController@getUploadExcelArchivo");
Route::post("archivos_acepta/excels/archivos/upload/masivo","ArchivoAceptaController@postUploadExcelArchivo");

Route::get("archivos_acepta/carga_archivos_en_documentos","ArchivoAceptaController@cargaArchivoEnDocumentos");

Route::get("archivos_acepta/test_acepta","ArchivoAceptaController@testAcepta");

Route::get("archivos_acepta/carga_reclamados", "ArchivoAceptaController@getCargaReclamados");
Route::post("archivos_acepta/carga_reclamados", "ArchivoAceptaController@postCargaReclamados");

Route::get("archivos_acepta/home/excel/{year}","ArchivoAceptaController@getHomeExcel");

Route::get("archivos_acepta/modal/pasar_a_revision_bodega/{id}","ArchivoAceptaController@getModalToRevisionBodega");
Route::post("archivos_acepta/to_revision_bodega", "ArchivoAceptaController@postToRevisionBodega");

Route::get("archivos_acepta/listado/revision_bodega","ArchivoAceptaController@getArchivosRevisionBodega");
Route::post("archivos_acepta/data_table_revision_bodega","ArchivoAceptaController@dataTableRevisionBodega");
Route::post("archivos_acepta/revision_bodega/excel","ArchivoAceptaController@postRevisionBodegaExcel");

Route::get("archivos_acepta/modal/revision_bodega/{id}","ArchivoAceptaController@getModalRevisionBodega");
Route::post("archivos_acepta/revision_bodega", "ArchivoAceptaController@postRevisionBodega");

Route::get("archivos_acepta/modal/traza/{id}","ArchivoAceptaController@getModalTraza");

Route::get("archivos_acepta/modal/quitar_reclamo/{id}","ArchivoAceptaController@getModalQuitaReclamo");
Route::post("archivos_acepta/quitar_reclamo", "ArchivoAceptaController@postQuitarReclamo");
/**
 * Factoring
 */

// Route::resource('factoring','FactoringController');
Route::get("factoring/carga/individual","FactoringController@getCargaIndividual");
Route::post("factoring/carga/individual","FactoringController@postCargaIndividual");

Route::get("factoring/grilla","FactoringController@getGrilla");
Route::post("factoring/grilla/filtrar","FactoringController@postGrilla");
Route::get("factoring/eliminar_factoring/{id}","FactoringController@getEliminarFactoring");
Route::get("factoring/get_facturas_proveedor/{id}","FactoringController@getFacturasProveedor");

Route::get("factoring/carga/masiva","FactoringController@getCargaMasiva");
Route::post("factoring/carga/masiva","FactoringController@postCargaMasiva");

Route::get("factoring/grilla/no_factura","FactoringController@getGrillaNoFactura");
Route::post("factoring/grilla/no_factura/filtrar","FactoringController@postGrillaNoFactura");

Route::get("factoring/grilla/pagado","FactoringController@getGrillaPagado");

Route::get("factoring/modal/ver/{id}","FactoringController@getModalVer");
Route::get("factoring/modal/editar/{id}","FactoringController@getModalEditar");
Route::post("factoring/modal/editar_sii","FactoringController@postModalEditar");
/**
 * Mantenedores
 */
/**
 * Perfiles
 */
Route::get("perfiles","PerfilesController@index");
Route::get("perfiles/modal/editar/{id}","PerfilesController@getModalEditar");
Route::post("perfiles/editar","PerfilesController@postEditar");

Route::get("perfiles/modal/ver/{id}","PerfilesController@getModalVer");

Route::get("perfiles/create","PerfilesController@getCreate");
Route::post("perfiles/create","PerfilesController@postCreate");

Route::get("perfiles/modal/eliminar/{id}","PerfilesController@getEliminar");
Route::post("perfiles/eliminar","PerfilesController@postEliminar");

/**
 * Permisos
 */
Route::get("permisos","PermisosController@index");

Route::get("permisos/modal/editar/{id}","PermisosController@getModalEditar");
Route::post("permisos/editar","PermisosController@postEditar");

Route::get("permisos/create","PermisosController@getCreate");
Route::post("permisos/create","PermisosController@postCreate");

Route::get("permisos/modal/eliminar/{id}","PermisosController@getEliminar");
Route::post("permisos/eliminar","PermisosController@postEliminar");

/**
 * Usuarios
 */
Route::resource('usuarios','UserController');
Route::get("usuarios/modal/ver/{id}","UserController@getModalVer");
Route::get("usuarios/modal/editar/{id}","UserController@getModalEditar");
Route::post("usuarios/editar/","UserController@postEditar");

Route::get("usuarios/modal/eliminar/{id}","UserController@getModalEliminar");
Route::post("usuarios/eliminar/","UserController@postEliminar");

Route::get("usuarios/modal/editar_perfil/","UserController@getModalEditarPerfil");
Route::post("usuarios/editar_perfil/","UserController@postEditarPerfil");

// Test Entrust
Route::get("usuarios/test/entrust","UserController@getTestEntrust");

/**
 * Proveedores
 */
Route::resource('proveedores','ProveedorController');
Route::get('proveedores/test/test', 'ProveedorController@test');
Route::post("proveedores/data_table/index","ProveedorController@dataTableIndex");

Route::get("proveedores/seeds/help","ProveedorController@getSeeds");
Route::get("proveedores/modal/ver_proveedor/{id_proveedor}","ProveedorController@getModalVerProveedor");
Route::get("proveedores/modal/editar_proveedor/{id_proveedor}","ProveedorController@getModalEditarProveedor");
Route::post("proveedores/editar_proveedor/","ProveedorController@postEditarProveedor");
Route::get("proveedores/modal/eliminar_proveedor/{id_proveedor}","ProveedorController@getModalEliminarProveedor");
Route::post("proveedores/eliminar_proveedor/","ProveedorController@postEliminarProveedor");

Route::get("proveedores/upload/masivo","ProveedorController@getUpload");
Route::post("proveedores/upload/masivo","ProveedorController@postUpload");

/**
 * Tipos de Documento
 */
Route::resource('tipos_documento','TipoDocumentoController');
Route::get("tipos_documento/modal/ver_tipo_documento/{id_tipo_documento}","TipoDocumentoController@getModalVerTipoDocumento");
Route::get("tipos_documento/modal/editar_tipo_documento/{id_tipo_documento}","TipoDocumentoController@getModalEditarTipoDocumento");
Route::post("tipos_documento/editar_tipo_documento/","TipoDocumentoController@postEditarTipoDocumento");

Route::get("tipos_documento/modal/eliminar_tipo_documento/{id_tipo_documento}","TipoDocumentoController@getModalEliminarTipoDocumento");
Route::post("tipos_documento/eliminar_tipo_documento/","TipoDocumentoController@postEliminarTipoDocumento");

/**
 * Items Presupuestarios
 */
Route::resource('item','ItemPresupuestarioController');
Route::get("item/modal/ver/{id_item}","ItemPresupuestarioController@getModalVer");
Route::get("item/modal/editar/{id_item}","ItemPresupuestarioController@getModalEditar");
Route::post("item/editar_item/","ItemPresupuestarioController@postEditar");

Route::get("item/modal/eliminar/{id_item}","ItemPresupuestarioController@getModalEliminar");
Route::post("item/eliminar/","ItemPresupuestarioController@postEliminar");

Route::get("item/seeds/help","ItemPresupuestarioController@getSeeds");
Route::get("item/seeds/help/clasificacion","ItemPresupuestarioController@getSeedsClasificacion");

Route::get("item/upload/masivo","ItemPresupuestarioController@getUpload");
Route::post("item/upload/masivo","ItemPresupuestarioController@postUpload");

/**
 * Modalidades Compra
 */

Route::resource('modalidad_compra','ModalidadCompraController');
Route::get("modalidad_compra/modal/ver/{id_modalidad_compra}","ModalidadCompraController@getModalVer");
Route::get("modalidad_compra/modal/editar/{id_modalidad_compra}","ModalidadCompraController@getModalEditar");
Route::post("modalidad_compra/editar_modalidad/","ModalidadCompraController@postEditar");

Route::get("modalidad_compra/modal/eliminar/{id_modalidad_compra}","ModalidadCompraController@getModalEliminar");
Route::post("modalidad_compra/eliminar/","ModalidadCompraController@postEliminar");

/**
 * Tipos Adjudicacion
 */
Route::resource('tipo_adjudicacion','TipoAdjudicacionController');
Route::get("tipo_adjudicacion/modal/ver/{id}","TipoAdjudicacionController@getModalVer");
Route::get("tipo_adjudicacion/modal/editar/{id}","TipoAdjudicacionController@getModalEditar");
Route::post("tipo_adjudicacion/editar/","TipoAdjudicacionController@postEditar");

Route::get("tipo_adjudicacion/modal/eliminar/{id}","TipoAdjudicacionController@getModalEliminar");
Route::post("tipo_adjudicacion/eliminar/","TipoAdjudicacionController@postEliminar");

/**
 * Tipos de Informe
 */
Route::resource('tipo_informe','TipoInformeController');
Route::get("tipo_informe/modal/ver/{id}","TipoInformeController@getModalVer");
Route::get("tipo_informe/modal/editar/{id}","TipoInformeController@getModalEditar");
Route::post("tipo_informe/editar/","TipoInformeController@postEditar");

Route::get("tipo_informe/modal/eliminar/{id}","TipoInformeController@getModalEliminar");
Route::post("tipo_informe/eliminar/","TipoInformeController@postEliminar");

/**
 * Tipos de Archivo
 */
Route::resource('tipo_archivo','TipoArchivoController');
Route::get("tipo_archivo/modal/ver/{id}","TipoArchivoController@getModalVer");
Route::get("tipo_archivo/modal/editar/{id}","TipoArchivoController@getModalEditar");
Route::post("tipo_archivo/editar/","TipoArchivoController@postEditar");

Route::get("tipo_archivo/modal/eliminar/{id}","TipoArchivoController@getModalEliminar");
Route::post("tipo_archivo/eliminar/","TipoArchivoController@postEliminar");

/**
 * Motivos de Rechazo
 */
Route::get("motivo_rechazo","MotivoRechazoController@index");
Route::get("motivo_rechazo/create","MotivoRechazoController@create");
Route::post("motivo_rechazo","MotivoRechazoController@store");
Route::get("motivo_rechazo/modal/ver/{id}","MotivoRechazoController@show");
Route::get("motivo_rechazo/modal/editar/{id}","MotivoRechazoController@getModalEditar");
Route::post("motivo_rechazo/editar/","MotivoRechazoController@postEditar");
Route::get("motivo_rechazo/modal/eliminar/{id}","MotivoRechazoController@getModalEliminar");
Route::post("motivo_rechazo/eliminar/","MotivoRechazoController@postEliminar");

/**
 * Motivos Rezagar
 */
Route::get("motivo_rezagar","MotivoRezagarController@index");
Route::get("motivo_rezagar/create","MotivoRezagarController@create");
Route::post("motivo_rezagar","MotivoRezagarController@store");
Route::get("motivo_rezagar/modal/ver/{id}","MotivoRezagarController@show");
Route::get("motivo_rezagar/modal/editar/{id}","MotivoRezagarController@getModalEditar");
Route::post("motivo_rezagar/editar/","MotivoRezagarController@postEditar");
Route::get("motivo_rezagar/modal/eliminar/{id}","MotivoRezagarController@getModalEliminar");
Route::post("motivo_rezagar/eliminar/","MotivoRezagarController@postEliminar");

/**
 * Motivos de Problema
 */
Route::get("motivo_problema","MotivoProblemaController@index");
Route::get("motivo_problema/create","MotivoProblemaController@create");
Route::post("motivo_problema","MotivoProblemaController@store");
Route::get("motivo_problema/modal/ver/{id}","MotivoProblemaController@show");
Route::get("motivo_problema/modal/editar/{id}","MotivoProblemaController@getModalEditar");
Route::post("motivo_problema/editar/","MotivoProblemaController@postEditar");
Route::get("motivo_problema/modal/eliminar/{id}","MotivoProblemaController@getModalEliminar");
Route::post("motivo_problema/eliminar/","MotivoProblemaController@postEliminar");


/**
 * Medios de Pago
 */
Route::get("medio_pago","MedioPagoController@index");
Route::get("medio_pago/create","MedioPagoController@create");
Route::post("medio_pago","MedioPagoController@store");
Route::get("medio_pago/modal/ver/{id}","MedioPagoController@show");
Route::get("medio_pago/modal/editar/{id}","MedioPagoController@getModalEditar");
Route::post("medio_pago/editar/","MedioPagoController@postEditar");
Route::get("medio_pago/modal/eliminar/{id}","MedioPagoController@getModalEliminar");
Route::post("medio_pago/eliminar/","MedioPagoController@postEliminar");


/**
 * Cuentas Contables
 */
Route::get("cuenta_contable","CuentaContableController@index");
Route::get("cuenta_contable/create","CuentaContableController@create");
Route::post("cuenta_contable","CuentaContableController@store");
Route::get("cuenta_contable/modal/ver/{id}","CuentaContableController@show");
Route::get("cuenta_contable/modal/editar/{id}","CuentaContableController@getModalEditar");
Route::post("cuenta_contable/editar/","CuentaContableController@postEditar");
Route::get("cuenta_contable/modal/eliminar/{id}","CuentaContableController@getModalEliminar");
Route::post("cuenta_contable/eliminar/","CuentaContableController@postEliminar");

/**
 * Cuentas bancarias
 */
Route::get("cuenta_bancaria","CuentaBancariaController@index");
Route::get("cuenta_bancaria/create","CuentaBancariaController@create");
Route::post("cuenta_bancaria","CuentaBancariaController@store");
Route::get("cuenta_bancaria/modal/ver/{id}","CuentaBancariaController@show");
Route::get("cuenta_bancaria/modal/editar/{id}","CuentaBancariaController@getModalEditar");
Route::post("cuenta_bancaria/editar/","CuentaBancariaController@postEditar");
Route::get("cuenta_bancaria/modal/eliminar/{id}","CuentaBancariaController@getModalEliminar");
Route::post("cuenta_bancaria/eliminar/","CuentaBancariaController@postEliminar");

/**
 * Unidad
 */
Route::get("unidad","UnidadController@index");
Route::get("unidad/create","UnidadController@create");
Route::post("unidad","UnidadController@store");
Route::get("unidad/modal/ver/{id}","UnidadController@show");
Route::get("unidad/modal/editar/{id}","UnidadController@getModalEditar");
Route::post("unidad/editar/","UnidadController@postEditar");
Route::get("unidad/modal/eliminar/{id}","UnidadController@getModalEliminar");
Route::post("unidad/eliminar/","UnidadController@postEliminar");

/**
 * Profesion
 */
Route::get("profesion","ProfesionController@index");
Route::get("profesion/create","ProfesionController@create");
Route::post("profesion","ProfesionController@store");
Route::get("profesion/modal/ver/{id}","ProfesionController@show");
Route::get("profesion/modal/editar/{id}","ProfesionController@getModalEditar");
Route::post("profesion/editar/","ProfesionController@postEditar");
Route::get("profesion/modal/eliminar/{id}","ProfesionController@getModalEliminar");
Route::post("profesion/eliminar/","ProfesionController@postEliminar");

/**
 * Tipo Prestacion
 */
Route::get("tipo_prestacion","TipoPrestacionController@index");
Route::get("tipo_prestacion/create","TipoPrestacionController@create");
Route::post("tipo_prestacion","TipoPrestacionController@store");
Route::get("tipo_prestacion/modal/ver/{id}","TipoPrestacionController@show");
Route::get("tipo_prestacion/modal/editar/{id}","TipoPrestacionController@getModalEditar");
Route::post("tipo_prestacion/editar/","TipoPrestacionController@postEditar");
Route::get("tipo_prestacion/modal/eliminar/{id}","TipoPrestacionController@getModalEliminar");
Route::post("tipo_prestacion/eliminar/","TipoPrestacionController@postEliminar");

/**
 * Categoría
 */
Route::get("categoria","CategoriaController@index");
Route::get("categoria/create","CategoriaController@create");
Route::post("categoria","CategoriaController@store");
Route::get("categoria/modal/ver/{id}","CategoriaController@show");
Route::get("categoria/modal/editar/{id}","CategoriaController@getModalEditar");
Route::post("categoria/editar/","CategoriaController@postEditar");
Route::get("categoria/modal/eliminar/{id}","CategoriaController@getModalEliminar");
Route::post("categoria/eliminar/","CategoriaController@postEliminar");



/**
 * Referente técnico - Ex servicios
 */
Route::resource('referente_tecnico','ReferenteTecnicoController');
Route::get("referente_tecnico/modal/ver/{id}","ReferenteTecnicoController@getModalVer");
Route::get("referente_tecnico/modal/editar/{id}","ReferenteTecnicoController@getModalEditar");
Route::post("referente_tecnico/editar/","ReferenteTecnicoController@postEditar");

Route::get("referente_tecnico/modal/eliminar/{id}","ReferenteTecnicoController@getModalEliminar");
Route::post("referente_tecnico/eliminar/","ReferenteTecnicoController@postEliminar");
/**
 * Opciones del referente técnico
 */
Route::get("referente_tecnico/vistobueno/index","ReferenteTecnicoController@getIndexVistoBueno");
Route::post("referente_tecnico/data_table_index_solicitud_visto_bueno","ReferenteTecnicoController@dataTableSolicitudVistoBueno");

Route::get("referente_tecnico/modal/generar_memo/{id}","ReferenteTecnicoController@getModalGenerarMemo");
Route::post("referente_tecnico/generar_memo/","ReferenteTecnicoController@postGenerarMemo");

Route::get("referente_tecnico/modal/generar_memo_masivo/{ids}","ReferenteTecnicoController@getModalGenerarMemoMasivo");
Route::post("referente_tecnico/generar_memo_masivo/","ReferenteTecnicoController@postGenerarMemoMasivo");
// Recepción Memos, vistos buenos
Route::get("referente_tecnico/memo/index","ReferenteTecnicoController@getIndexMemo");
Route::post("referente_tecnico/data_table_index_memo_visto_bueno","ReferenteTecnicoController@dataTableMemoVistoBueno");

Route::get("referente_tecnico/modal/eliminar_solicitud_visto_bueno/{id}","ReferenteTecnicoController@getModalEliminarSolicitudVistoBueno");
Route::post("referente_tecnico/eliminar_solicitud_visto_bueno","ReferenteTecnicoController@postEliminarSolicitudVistoBueno");

Route::get("referente_tecnico/modal/editar_memo/{id}","ReferenteTecnicoController@getModalEditarMemo");
Route::post("referente_tecnico/editar_memo/","ReferenteTecnicoController@postEditarMemo");

Route::get("referente_tecnico/modal/registrar_memo_visto_bueno/{id}","ReferenteTecnicoController@getModalRegistrarMemoVistoBueno");
Route::post("referente_tecnico/registrar_memo_visto_bueno/","ReferenteTecnicoController@postRegistrarMemoVistoBueno");
// Historial Memos, son los memos listos
Route::get("referente_tecnico/memo/historial","ReferenteTecnicoController@getIndexHistorial");
Route::get("referente_tecnico/modal/ver/memo/{id}","ReferenteTecnicoController@getModalVerMemo");

Route::get("referente_tecnico/modal/editar_visto_bueno/{id}","ReferenteTecnicoController@getModalEditarVistoBueno");
Route::post("referente_tecnico/editar_visto_bueno/","ReferenteTecnicoController@postEditarVistoBueno");

// Genera PDF del memo para visto Bueno
Route::get("referente_tecnico/generar/pdf_solicitud_visto_bueno/{id}","ReferenteTecnicoController@getGenerarPDFSolicitudVistoBueno");
Route::get("referente_tecnico/generar/pdf_visto_bueno/{id}","ReferenteTecnicoController@getGenerarPDFVistoBueno");

Route::get("referente_tecnico/seeds/help","ReferenteTecnicoController@getSeeds");

// Para notificar al referente tecnico de los vistos buenos pendientes
Route::get("referente_tecnico/num/vistos_buenos_pendientes","ReferenteTecnicoController@getNumVistosBuenosPendientes");

Route::get("referente_tecnico/vistobueno/upload/masivo","ReferenteTecnicoController@getUpload");
Route::post("referente_tecnico/vistobueno/upload/masivo","ReferenteTecnicoController@postUpload");

/**
 * Libro Compras
 */
Route::get("libro_compra/upload/masivo","LibroCompraController@getUpload");
Route::post("libro_compra/upload/masivo","LibroCompraController@postUpload");

/**
 * Consumir API's
 */
Route::get("consume_api/test","ConsumeApiController@test");
Route::get("consume_api/enviar_recepciones","ConsumeApiController@enviarRecepciones");


Route::get("consume_api/eliminar_recepciones","ConsumeApiController@deleteRecepciones");

/**
 * Reportes
 */
Route::get("reportes/libro_de_compras","ReporteController@getLibroDeCompras");
Route::post("reportes/libro_de_compras/excel","ReporteController@postLibroDeCompras");

Route::get("reportes/libro_de_honorarios","ReporteController@getLibroDeHonorarios");
Route::post("reportes/libro_de_honorarios/excel","ReporteController@postLibroDeHonorarios");

Route::get("reportes/libro_de_deudas","ReporteController@getLibroDeDeudas");
Route::post("reportes/libro_de_deudas/excel","ReporteController@postLibroDeDeudas");

/**
 * Tesoreria / Deuda Flotante / Nominas de Pago / Comprobantes / Docs. Pagados
 */

 /**
  * Deuda Flotante
  */
Route::get("deuda_flotante","DeudaFlotanteController@index");
Route::post("deuda_flotante/data_table_index","DeudaFlotanteController@dataTableDeudaFlotanteIndex");
Route::post("deuda_flotante/excel","DeudaFlotanteController@postDeudaExcel");

/**
 * Nominas de Pago
 */
Route::get("nomina_pago","NominaPagoController@index");
Route::post("nomina_pago/saldo_disponible","NominaPagoController@postGuardarSaldoDisponible");

Route::post("nomina_pago/data_table_index","NominaPagoController@dataTableIndex");

Route::get("nomina_pago/modal/nuevo","NominaPagoController@getModalNuevaNomina");
Route::post("nomina_pago/store","NominaPagoController@store");

Route::get("nomina_pago/edit/{id}/{nuevo?}","NominaPagoController@getEdit");

Route::get("nomina_pago/modal/seleccionar_documentos/{idNomina}","NominaPagoController@getSeleccionarDocumentos");
Route::post("nomina_pago/documentos_para_nomina","NominaPagoController@dataTableDocumentosParaNomina");

Route::post("nomina_pago/agregar/documentos/", "NominaPagoController@postAgregarDocumentos");

Route::get("nomina_pago/documentos_nomina/{idNomina}","NominaPagoController@getDocumentosNomina");

Route::get("nomina_pago/modal/quitar_doc/{idDoc}","NominaPagoController@getModalQuitarDocumento");
Route::post("nomina_pago/quitar_documento","NominaPagoController@postQuitarDocumento");

Route::get("nomina_pago/modal/eliminar_nomina/{idNomina}","NominaPagoController@getModalEliminarNomina");
Route::post("nomina_pago/eliminar_nomina","NominaPagoController@postEliminarNomina");

Route::get('nomina_pago/imprimir/{id}',"NominaPagoController@getImprimirNomina");

/**
 * Comprobantes contables
 */

 Route::get("comprobantes/crear_tgr", "ComprobanteContableController@crearComprobantesTGR");
 
Route::get("comprobantes/test","ComprobanteContableController@test");

Route::get("comprobantes/test_2","ComprobanteContableController@test2");
Route::get("comprobantes/test_3","ComprobanteContableController@test3");

Route::get("comprobantes","ComprobanteContableController@index");
Route::post("comprobantes/data_table_index","ComprobanteContableController@dataTableIndex");

Route::get("comprobantes/modal/nuevo","ComprobanteContableController@getModalNuevoComprobante");
Route::post("comprobantes/store","ComprobanteContableController@store");

Route::get("comprobantes/edit/{id}/{nuevo?}","ComprobanteContableController@getEdit");

Route::get("comprobantes/modal/seleccionar_documentos/{idComprobante}","ComprobanteContableController@getSeleccionarDocumentos");
Route::post("comprobantes/documentos_para_comprobante","ComprobanteContableController@dataTableDocumentosParaComprobante");
Route::post("comprobantes/agregar/documentos/", "ComprobanteContableController@postAgregarDocumentos");

Route::get("comprobantes/documentos_comprobante/{idComprobante}","ComprobanteContableController@getDocumentosComprobante");

Route::get("comprobantes/modal/quitar_doc/{idDoc}","ComprobanteContableController@getModalQuitarDocumento");
Route::post("comprobantes/quitar_documento","ComprobanteContableController@postQuitarDocumento");

Route::get("comprobantes/modal/eliminar_comprobante/{idComprobante}","ComprobanteContableController@getModalEliminarComprobante");
Route::post("comprobantes/eliminar_comprobante","ComprobanteContableController@postEliminarComprobante");

Route::get("comprobantes/carga/sigfe","ComprobanteContableController@getCargaSigfe");
Route::post("comprobantes/carga/sigfe","ComprobanteContableController@postCargaSigfe");

Route::get("comprobantes/carga/pagos_sigfe","ComprobanteContableController@getCargaPagosSigfe");
Route::post("comprobantes/carga/pagos_sigfe","ComprobanteContableController@postCargaPagosSigfe");

Route::get("comprobantes/modal/generar_pdf/{id}","ComprobanteContableController@getModalGenerarPDF");
Route::post("comprobantes/generar_pdf","ComprobanteContableController@postGenerarPDFComprobante");

/**
 * Documentos pagados
 */
Route::get("documentos_pagados","DocumentosPagadosController@index");
Route::post("documentos_pagados/data_table_index","DocumentosPagadosController@dataTableDocumentosPagados");

Route::post("documentos_pagados/excel","DocumentosPagadosController@documentosPagadosExcel");


/**
 * Nube
 */

 /**
  * Nube Libro de Compras
  */
  Route::get("nube/libro_de_compras","NubeController@getIndexLibroDeCompras");
  Route::post("nube/libro_de_compras","NubeController@postUploadLibroDeCompras");
  Route::get("nube/libro_de_compras/modal_eliminar/{idLibro}","NubeController@getModalEliminarLibroDeCompras");
  Route::post("nube/libro_de_compras/eliminar","NubeController@postEliminarLibroDeCompras");

  /**
   * Nube Libro de Honorarios
   */
  Route::get("nube/libro_de_honorarios","NubeController@getIndexLibroDeHonorarios");
  Route::post("nube/libro_de_honorarios","NubeController@postUploadLibroDeHonorarios");
  Route::get("nube/libro_de_honorarios/modal_eliminar/{idLibro}","NubeController@getModalEliminarLibroDeHonorarios");
  Route::post("nube/libro_de_honorarios/eliminar","NubeController@postEliminarLibroDeHonorarios");

  /**
   * Nube Libro de Deudas
   */
  Route::get("nube/libro_de_deudas","NubeController@getIndexLibroDeDeudas");
  Route::post("nube/libro_de_deudas","NubeController@postUploadLibroDeDeudas");
  Route::get("nube/libro_de_deudas/modal_eliminar/{idLibro}","NubeController@getModalEliminarLibroDeDeudas");
  Route::post("nube/libro_de_deudas/eliminar","NubeController@postEliminarLibroDeDeudas");

/**
 * Graficos
 */
Route::get("graficos/documentos_ingresados","GraficosController@getDocumentosIngresados");
Route::post("graficos/documentos_ingresados","GraficosController@postDocumentosIngresados");

Route::get("graficos/items_devengados","GraficosController@getItemsDevengados");
Route::post("graficos/items_devengados","GraficosController@postItemsDevengados");

Route::get("graficos/usuarios_acepta","GraficosController@getUsuariosAcepta");
Route::post("graficos/usuarios_acepta","GraficosController@postUsuariosAcepta");

Route::get("graficos/usuarios_documentos","GraficosController@getUsuariosDocumentos");
Route::post("graficos/usuarios_documentos","GraficosController@postUsuariosDocumentos");

/**
 * Gestion Contratos
 */
Route::get("contratos","ContratosController@index");

Route::post("contratos/data_table_index", "ContratosController@dataTableIndex");

Route::get("contratos/modal/nuevo", "ContratosController@create");
Route::post("contratos", "ContratosController@store");

Route::get("contratos/modal/editar/{id}", "ContratosController@getModalEditar");
Route::post("contratos/editar", "ContratosController@postEditar");

Route::get("contratos/modal/eliminar/{id}", "ContratosController@getModalEliminar");
Route::post("contratos/eliminar", "ContratosController@postEliminar");

Route::get("contratos/modal/terminar/{id}", "ContratosController@getModalTerminar");
Route::post("contratos/terminar", "ContratosController@postTerminar");

Route::get("contratos/modal/ver/{id}", "ContratosController@getModalVer");

Route::get("contratos/modal/consumir/{id}", "ContratosController@getModalConsumir");
Route::post("contratos/consumir", "ContratosController@postConsumir");

Route::get("contratos/get_contratos_para_documentos/{id}","ContratosController@getContratosParaDocumentos");

Route::get("contratos/pdf_saldo_contrato/{id}","ContratosController@getGenerarPDFSaldoConstrato");

Route::get("contratos/upload","ContratosController@getUpload");
Route::post("contratos/upload","ContratosController@postUpload");

/**
 * Ordenes Compra
 */
Route::get("ordenes_compra","OrdenCompraController@index");

Route::post("ordenes_compra/data_table_index", "OrdenCompraController@dataTableIndex");

Route::get("ordenes_compra/modal/nuevo", "OrdenCompraController@create");
Route::post("ordenes_compra", "OrdenCompraController@store");

Route::get("ordenes_compra/modal/editar/{id}", "OrdenCompraController@getModalEditar");
Route::post("ordenes_compra/editar", "OrdenCompraController@postEditar");

Route::get("ordenes_compra/modal/eliminar/{id}", "OrdenCompraController@getModalEliminar");
Route::post("ordenes_compra/eliminar", "OrdenCompraController@postEliminar");

Route::get("ordenes_compra/get_contratos/{id}", "OrdenCompraController@getContratosProveedor");
Route::get("ordenes_compra/get_oc_para_boleta/{idProveedor}/{idContrato}","OrdenCompraController@getOcParaBoleta");

Route::get("ordenes_compra/modal/consumir/{id}", "OrdenCompraController@getModalConsumir");
Route::post("ordenes_compra/consumir", "OrdenCompraController@postConsumir");

Route::get("ordenes_compra/obtener_pdf/{id}", "OrdenCompraController@getObtenerPdfOc");