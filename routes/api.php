<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});

/*
Route::post('/register','UserController@register');
Route::post('/login','UserController@login');

Route::resource('acepta_carga','ArchivoAceptaController');
*/

Route::post('/facturas/agregar','apiFacturasController@agregarFacturasHermes');
Route::get('/facturas/test','apiFacturasController@test');

Route::put('/oc/actualizar','apiFacturasController@actualizarOc');
Route::post('/facturas/ver','apiFacturasController@ver');