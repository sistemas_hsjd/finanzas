<?php namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class RelacionDocumentoWsDocumento extends Model
{
    use SoftDeletes;
    protected $table = 'relacion_documentos_ws_documentos';

    public function getUsuarioResponsable()
    {
        return $this->belongsTo('App\User','id_user_relacion');
    }

    public function getDocumento()
    {
        return $this->belongsTo('App\Documento','id_documento');
    }

    public function getWsDocumento()
    {
        return $this->belongsTo('App\WsDocumento','id_ws_documento');
    }

    public function getColorPorDias()
    {
        $dias = diferencia_dias($this->created_at,date('Y-m-d'));
        $color = '';
        if ($dias <= 4) {
            $color = 'green';
        } elseif ( $dias == 5 || $dias == 6 ) {
            $color = '#e2e200f5';
        } elseif ( $dias == 7 || $dias == 8 ) {
            $color = '#ff8300';
        } else {
            $color = 'red';
        }
        
        return $color;
    }
}
