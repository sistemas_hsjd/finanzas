<?php namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class TipoDocumento extends Model
{
    use SoftDeletes;
    protected $table = 'tipos_documento';

    public function __construct( $request = null, array $attributes = array())
    {
        parent::__construct($attributes);
        if ($request != null ) {
            $this->nombre = $request->input('nombre');
            $this->sigla = $request->input('sigla');
            $this->id_user_created = \Auth::user()->id;
            $this->updated_at = NULL;
            $this->save();
        }
    }

    public function editTipoDocumento($request)
    {
        $this->nombre = $request->input('nombre');
        $this->sigla = $request->input('sigla');
        $this->id_user_updated = \Auth::user()->id;
        $this->save();
    }

}
