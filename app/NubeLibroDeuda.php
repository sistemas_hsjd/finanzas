<?php namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class NubeLibroDeuda extends Model
{
    use SoftDeletes;
    protected $table = 'nube_libros_deudas';

    public function getUser()
    {
        return $this->belongsTo('App\User','id_usuario_crea');
    }
}
