<?php namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class RechazoVistoBueno extends Model
{
    use SoftDeletes;
    protected $table = 'rechazos_vistos_buenos';

    public function getVistoBueno()
    {
        
        return $this->belongsTo('App\VistoBueno','id_visto_bueno')->withTrashed();
    }

}
