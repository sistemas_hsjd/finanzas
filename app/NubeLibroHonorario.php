<?php namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class NubeLibroHonorario extends Model
{
    use SoftDeletes;
    protected $table = 'nube_libros_honorarios';

    public function getUser()
    {
        return $this->belongsTo('App\User','id_usuario_crea');
    }
}
