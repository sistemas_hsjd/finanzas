<?php namespace App;

use Illuminate\Database\Eloquent\Model;

class ArchivoAceptaExcel extends Model
{
    protected $table = 'archivo_acepta_excel';

    public function getUser()
    {
        return $this->belongsTo('App\User','id_usuario')->withTrashed();
    }

}
