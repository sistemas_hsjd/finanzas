<?php namespace App;

use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Support\Facades\Hash;

use Zizaco\Entrust\Traits\EntrustUserTrait;
use App\Role;

class User extends Authenticatable
{

    use Notifiable;
    use SoftDeletes,EntrustUserTrait { // add this trait to your user model

        SoftDeletes::restore insteadof EntrustUserTrait;
        EntrustUserTrait::restore insteadof SoftDeletes;
    }
    
    // protected $table = 'ft_usuario'; // para generar seed a partir de la base de datos de finanzas

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'email', 'password', 'rut',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];


    public function __construct( $request = null, array $attributes = array())
    {
        parent::__construct($attributes);
        if ($request != null) {
            $this->rut = str_replace( '.', '', trim($request->input('rut') ) );
            $this->password = Hash::make( substr($this->rut,0,-2) );
            $this->name = trim($request->input('nombre'));

            if ($request->input('email') == null) {
                $this->email = null;
            } else {
                $this->email = trim($request->input('email'));
            }

            $this->iniciales = trim($request->input('iniciales'));

            if ($request->input('responsable') == 1) {
                $this->responsable = 1;
            } else {
                $this->responsable = 0;
            }

            if ($request->input('suspendido') == 1) {
                $this->suspendido = 1;
            } else {
                $this->suspendido = 0;
            }
            $this->updated_at = NULL;

            $this->save();

            $perfil = Role::findOrFail($request->input('perfil'));
            $this->attachRole($perfil);
        }
    }

    public function editUser($request)
    {
        $this->rut = str_replace('.','',trim($request->input('rut')));
        $this->name = trim($request->input('nombre'));

        if ($request->input('email') == null) {
            $this->email = null;
        } else {
            $this->email = trim($request->input('email'));
        }

        $this->iniciales = trim($request->input('iniciales'));

        if ($request->input('responsable') == 1) {
            $this->responsable = 1;
        } else {
            $this->responsable = 0;
        }

        if ($request->input('suspendido') == 1) {
            $this->suspendido = 1;
        } else {
            $this->suspendido = 0;
        }

        //Se elimina el perfil que tiene el usuario
        if ( $this->roles ) {
            $this->roles()->sync([]);
        }
        

        $perfil = Role::findOrFail($request->input('perfil'));
        $this->attachRole($perfil);

        $this->save();
    }
    
    public function editPerfil($request)
    {
        $this->rut = str_replace('.','',trim($request->input('rut')));
        $this->name = trim($request->input('nombre'));

        if ($request->input('email') == null) {
            $this->email = null;
        } else {
            $this->email = trim($request->input('email'));
        }

        if ( $request->input('nueva_clave') != null ) {
            $this->password = Hash::make( trim($request->input('nueva_clave')) );
        }

        $this->iniciales = trim($request->input('iniciales'));

        $this->save();
    }
    
    public function getArchivosAceptaExel()
    {
        return $this->hasMany('App\ArchivoAceptaExcel','id_usuario','id');
    }

    public function formatRut()
    {
        if ($this->rut != '' && $this->rut != null) {
            return \Rut::parse($this->rut)->format();
        } else {
            return '';
        }
    }

    public function getReferenteTecnico()
    {
        return $this->hasOne('App\ReferenteTecnico','id_user','id');
    }

    public function getDocumentos()
    {
        return $this->hasMany('App\Documento','id_digitador','id');
    }

    public function getDocumentosPorDevengar()
    {
        return $this->hasMany('App\Documento','id_user_para_devengar','id');
    }

    public function getDocumentosCuadrados()
    {
        return $this->hasMany('App\Documento','id_user_validado','id');
    }

    public function getDocumentosResponsable()
    {
        return $this->hasMany('App\Documento','id_responsable','id');
    }

    public function suspendido()
    {
        if ($this->suspendido == 0) {
            return "No";
        } else {
            return "Si";
        }
    }

    public function responsable()
    {
        if ($this->responsable == 0) {
            return "No";
        } else {
            return "Si";
        }
    }

    public function getNombre()
    {
        $nombreAux = explode(' ', $this->name);

        $nombre = $nombreAux[0];

        if ( array_key_exists(2, $nombreAux) ) {
            $nombre .= ' '.$nombreAux[2];
        }
        return $nombre;
    }

    public function getArchivosAcepta()
    {
        return $this->hasMany('App\ArchivoAcepta','id_user_responsable','id');
    }
    
}
