<?php namespace App;

use Illuminate\Database\Eloquent\Model;

class SiiFactoringInfo extends Model
{
    protected $table = 'sii_factoring_info';

    public function __construct( $itemSii = null, array $attributes = array())
    {
        parent::__construct($attributes);
        if ( $itemSii != null ) {
            $this->vendedor = strtoupper( utf8_encode( $itemSii[0] ) );
            $this->estado_cesion = strtoupper( utf8_encode( $itemSii[1] ) );
            $this->deudor = strtoupper( utf8_encode( $itemSii[2] ) );
            $this->mail_deudor = strtoupper( utf8_encode( $itemSii[3] ) );
            $this->tipo_doc = strtoupper( utf8_encode( $itemSii[4] ) );
            $this->nombre_doc = strtoupper( utf8_encode( $itemSii[5] ) );
            $this->folio_doc = strtoupper( utf8_encode( $itemSii[6] ) );
            $this->fecha_emis_dte = strtoupper( utf8_encode( $itemSii[7] ) );
            $this->mnt_total = strtoupper( utf8_encode( $itemSii[8] ) );
            $this->cedente = strtoupper( utf8_encode( $itemSii[9] ) );
            $this->rz_cedente = strtoupper( utf8_encode( $itemSii[10] ) );
            $this->mail_cedente = strtoupper( utf8_encode( $itemSii[11] ) );
            $this->cesionario = strtoupper( utf8_encode( $itemSii[12] ) );
            $this->razon_cesionario = strtoupper( utf8_encode( $itemSii[13] ) );
            $this->mail_cesionario = strtoupper( utf8_encode( $itemSii[14] ) );
            $this->fecha_cesion = strtoupper( utf8_encode( $itemSii[15] ) );
            $this->mnt_cesion = strtoupper( utf8_encode( $itemSii[16] ) );
            $this->fecha_vencimiento = strtoupper( utf8_encode( $itemSii[17] ) );
            $this->pagado = 0;
            $this->save();
        }
    }

    public function editSiiFactoring($request = null)
    {
        if ( $request != null ) {
            $this->vendedor = strtoupper( utf8_encode( $request->input('vendedor') ) );
            $this->estado_cesion = strtoupper( utf8_encode( $request->input('estado_cesion') ) );
            $this->deudor = strtoupper( utf8_encode( $request->input('deudor') ) );
            if ( $request->input('mail_deudor') == null ) {
                $this->mail_deudor = '';
            } else {
                $this->mail_deudor = strtoupper( utf8_encode( $request->input('mail_deudor') ) );
            }
            
            $this->tipo_doc = strtoupper( utf8_encode( $request->input('tipo_doc') ) );
            $this->nombre_doc = strtoupper( utf8_encode( $request->input('nombre_doc') ) );
            $this->folio_doc = strtoupper( utf8_encode( $request->input('folio_doc') ) );
            $this->fecha_emis_dte = fecha_Y_m_d( $request->input('fecha_emision_dte') );
            $this->mnt_total = strtoupper( $request->input('monto_total') );
            $this->cedente = strtoupper( utf8_encode( $request->input('cedente') ) );
            $this->rz_cedente = strtoupper( utf8_encode( $request->input('rz_cedente') ) );
            $this->mail_cedente = strtoupper( utf8_encode( $request->input('mail_cedente') ) );
            $this->cesionario = strtoupper( utf8_encode( $request->input('cesionario') ) );
            $this->razon_cesionario = strtoupper( utf8_encode( $request->input('rz_cesionario') ) );
            $this->mail_cesionario = strtoupper( utf8_encode( $request->input('mail_cesionario') ) );

            $this->fecha_cesion = fecha_Y_m_d_h_m_s( $request->input('fecha_cesion') );
            
            $this->mnt_cesion = strtoupper( utf8_encode( $request->input('monto_cesion') ) );
            $this->fecha_vencimiento = fecha_Y_m_d( $request->input('fecha_vencimiento') );
            if ( $request->input('pagado') != null ) {
                $this->pagado = 1;
            } else {
                $this->pagado = 0;
            }
            
            $this->save();
        }
    }
}
