<?php namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests;
use Auth;
use App\Devengo;
use App\Documento;
use App\TipoDocumento;
use App\Proveedor;
use App\ModalidadCompra;
use App\TipoAdjudicacion;
use App\TipoInforme;
use App\User;
use App\ReferenteTecnico;
use App\TipoArchivo;
use App\ItemPresupuestario;
use App\WsDocumento;
use App\ArchivoAcepta;
use App\FolioSigfe;
use DB;
use App\Contrato;
use App\OrdenCompra;

class DevengoController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
    */
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function test()
    {
        set_time_limit ( 0 );
        ini_set('max_execution_time', 0);

        $documentos = Documento::with('getDevengos')
                               ->where('id_devengador','<>', null)
                               ->where('id_sigfe', '<>', null)
                               ->where('fecha_sigfe', '<>', null)
                            //    ->where('detalle', '<>', null)
                               ->select(
                                   'id','id_devengador','id_sigfe','fecha_sigfe','detalle'
                               )
                               ->orderBy('id','ASC')
                               ->get(); 

        $devengosCreados = 0;
        $devengosUpdate = 0;
        $foliosCreados = 0;
        foreach ( $documentos as $doc) {

            $folioSigfeAux = FolioSigfe::where('year_sigfe', fecha_Y( $doc->fecha_sigfe ))
                                       ->where('id_sigfe', $doc->id_sigfe)
                                       ->first();

            if ( ! is_object($folioSigfeAux) ) {

                $folioSigfe = new FolioSigfe();
                $folioSigfe->id_documento = $doc->id;
                $folioSigfe->id_sigfe = $doc->id_sigfe;
                $folioSigfe->fecha_sigfe = fecha_Y_m_d( $doc->fecha_sigfe );
                $folioSigfe->dia_sigfe = fecha_D( $doc->fecha_sigfe );
                $folioSigfe->mes_sigfe = fecha_M( $doc->fecha_sigfe );
                $folioSigfe->year_sigfe = fecha_Y( $doc->fecha_sigfe );

                if ( $doc->detalle != null ) {
                    $folioSigfe->detalle_sigfe = $doc->detalle;
                } else {
                    $folioSigfe->detalle_sigfe = '-';
                }

                $folioSigfe->save();

                $foliosCreados++;

                foreach ( $doc->getDevengos as $devengo ) {
                    
                    if ( $devengo->id_folio_sigfe == null ) {

                        $devengo->id_folio_sigfe = $folioSigfe->id;
                        $devengo->save();
                        $devengosUpdate++;

                    } else {

                        $devengoNew = new Devengo();
                        $devengoNew->id_folio_sigfe = $folioSigfe->id;
                        $devengoNew->id_item_presupuestario = $devengo->id_item_presupuestario;
                        $devengoNew->monto = formato_entero($devengo->monto);
                        $devengoNew->save();
                        $devengosCreados++;

                    }

                }

            }

        }

        
        return "Se han creado {$foliosCreados} documentos. <br> Devengos creados: {$devengosCreados}.<br> Devengos actualizados: {$devengosUpdate}";
    }

    public function dataTableDevengar(Request $request)
    {
        set_time_limit ( 0 );
        ini_set('max_execution_time', 0);

        $search = $request->get('search')['value'];
        $order_column = $request->get('order')[0]['column'];
        $order_dir = $request->get('order')[0]['dir'];

        $formJSON = json_decode($request->get('form'));
        $form = construyeFormulario($formJSON);

        $fechaInicio = fecha_Y_m_d($form->filtro_fecha_inicio);
        $fechaTermino = fecha_Y_m_d($form->filtro_fecha_termino);

        $documentos = Documento::with(['getTipoInforme','getProveedor','getTipoDocumento','getModalidadCompra',
                                       'getDevengos.getItemPresupuestario'])
                               ->where('id_devengador',null)
                               ->where('reclamado', null);

        if ( $form->filtro_numero_documento == null ) {
            // Se filtra por las fechas

            if ( $form->filtro_fecha_inicio != null ) {

                if ( $form->filtro_fecha_a_utilizar == 'Ingreso' ) {
                    $documentos = $documentos->where('fecha_ingreso','>=',$fechaInicio.' 00:00:00');
                } elseif ( $form->filtro_fecha_a_utilizar == 'Recepción' ) {
                    $documentos = $documentos->where('fecha_recepcion','>=',$fechaInicio);
                }

            }

            if ( $form->filtro_fecha_termino != null ) {
                if ( $form->filtro_fecha_a_utilizar == 'Ingreso' ) {
                    $documentos = $documentos->where('fecha_ingreso','<=',$fechaTermino.' 23:59:59');
                } elseif ( $form->filtro_fecha_a_utilizar == 'Recepción' ) {
                    $documentos = $documentos->where('fecha_recepcion','<=',$fechaTermino);
                }
            }

        } else {
            // no se consideran las fechas, y se busca el numero del documento
            $documentos = $documentos->where('numero_documento', trim($form->filtro_numero_documento) );
        }

        if ( isset($form->filtro_proveedor) ) {
            $documentos = $documentos->WhereIn('id_proveedor', $form->filtro_proveedor);
        }

        if ( isset($form->filtro_tipo_documento) ) {
            $documentos = $documentos->WhereIn('id_tipo_documento',$form->filtro_tipo_documento);
        }

        if ( isset($form->filtro_modalidad_compra) ) {
            $documentos = $documentos->WhereIn('id_modalidad_compra',$form->filtro_modalidad_compra);
        }

        if ( isset($form->filtro_tipo_adjudicacion) ) {
            $documentos = $documentos->WhereIn('id_tipo_adjudicacion',$form->filtro_tipo_adjudicacion);
        }

        if ( isset($form->filtro_tipo_informe) ) {
            $documentos = $documentos->WhereIn('id_tipo_informe',$form->filtro_tipo_informe);
        }

        if ( isset($form->filtro_responsable) ) {
            $documentos = $documentos->WhereIn('id_responsable',$form->filtro_responsable);
        }

        if ( isset($form->filtro_referente_tecnico) ) {
            $documentos = $documentos->WhereIn('id_referente_tecnico',$form->filtro_referente_tecnico);
        }

        if ( isset($form->filtro_digitador) ) {
            $documentos = $documentos->WhereIn('id_digitador',$form->filtro_digitador);
        }

        if ( isset($form->filtro_item_presupuestario) ) {
            $documentos = $documentos->whereHas('getDevengos', function ($query) use ($form) {
                $query->WhereIn('id_item_presupuestario',$form->filtro_item_presupuestario);
            });
        }

        $documentos = $documentos->get();

        $data = [];

        if ( $request->get('length') != -1 ) {
            $hasta = $request->get('start') + $request->get('length');
        } else {
            // Se quieren ver todos los datos
            $hasta = $request->get('start') + $documentos->count();
        }
    
        for( $i = $request->get('start') ; $i < $hasta ; $i++ ) {
            if ( isset($documentos[$i]) ) {
                // if ( isset($form->filtro_item_presupuestario) ) {
                //     $data[] = $documentos[$i]->getDatosPorDevengar($form->filtro_item_presupuestario);
                // } else {
                //     $data[] = $documentos[$i]->getDatosPorDevengar();
                // }
                if ( isset($form->filtro_item_presupuestario) ) {

                    $docJson = $documentos[$i]->getDatosPorDevengar($form->filtro_item_presupuestario);
                    if ( $docJson != '' ) {
                        $data[] = $docJson;
                    } else {
                        $documentos->forget($i);
                        $i--;
                    }
                    
                } else {

                    $docJson = $documentos[$i]->getDatosPorDevengar();
                    if ( $docJson != '' ) {
                        $data[] = $docJson;
                    } else {
                        $documentos->forget($i);
                        $i--;
                    }

                }
                
            }
        }

         // Se realiza la busqueda y se ordenan los datos
         if ( $documentos->count() > 0 ) {

            foreach ($data as $clave => $fila) {

                if ( $search != '' ) {
                    if ( in_array_r($search,$fila) ) {
                        // dd("lo encuentra",$fila,$search);
                        $columna[$clave] = $fila[$order_column];
                    } else {
                        // dd("NO lo encuentra",$fila,$search);
                        unset( $data[$clave] ); // quita el elemento del data
                    }
                } else {
                    $columna[$clave] = $fila[$order_column];
                }
                
            }

            
            // Se ordenan los datos, segun la columna seleccionada
            if ( $order_dir == 'asc' ) {
                array_multisort($columna, SORT_ASC, $data);
            } elseif ( $order_dir == 'desc' ) {
                array_multisort($columna, SORT_DESC, $data);
            }

        }

        $totalPorDevengar = 0;
        foreach ( $documentos as $index => $doc ) {
            $totalPorDevengarDocumento = 0;
            foreach ( $doc->getDevengos as $devengo) {
                $totalPorDevengarDocumento += $devengo->monto;
            }
            if ($doc->id_tipo_documento == 4 || $doc->id_tipo_documento == 10) {
                $totalPorDevengar = $totalPorDevengar - $totalPorDevengarDocumento;
            } else {
                $totalPorDevengar = $totalPorDevengar + $totalPorDevengarDocumento;
            }
        }

        $respuesta = array(
            "draw" => $request->get('draw'),
            "recordsTotal" => count($documentos),
            "recordsFiltered"=> count($documentos),
            "data" => $data,
            "totalPorDevengar" => '$ '.formatoMiles($totalPorDevengar),
        );

        return json_encode($respuesta);

    }

    /**
     * Muestra los documentos que aún no se han devengado
     */
    public function getDevengarDocumentos()
    {

        return \Redirect::to('home');

        // if (!\Entrust::can(['listado-por-devengar','devengar-documento'])) {
        //     return \Redirect::to('home');
        // }        
        
        $tiposDocumento = TipoDocumento::whereNotIn('id',[2,8])->get();
        $modalidadesCompra = ModalidadCompra::all();
        $tiposAdjudicacion = TipoAdjudicacion::all();
        $tiposInforme = TipoInforme::all();
        $usuariosResponsables = User::where('responsable',1)->get();
        $referentesTecnicos = ReferenteTecnico::all();
        $usuariosDigitadores = User::has('getDocumentos')->get();
        $usuariosPorDevengar = User::has('getDocumentosPorDevengar')->get();
        $proveedores = Proveedor::all();
        $itemsPresupuestarios = ItemPresupuestario::all();

        // Select para Fechas a utilizar en el filtro
        $fechaUtilizar1 = new \stdClass;
        $fechaUtilizar1->opcion = 'Ingreso';
        $fechaUtilizar2 = new \stdClass;
        $fechaUtilizar2->opcion = 'Recepción';

        $fechasUtilizar[] = $fechaUtilizar2;
        $fechasUtilizar[] = $fechaUtilizar1;
        

        return view('devengo.index')->with('tiposDocumento',$tiposDocumento)
                                    ->with('modalidadesCompra',$modalidadesCompra)
                                    ->with('tiposAdjudicacion',$tiposAdjudicacion)
                                    ->with('tiposInforme',$tiposInforme)
                                    ->with('usuariosResponsables',$usuariosResponsables)
                                    ->with('referentesTecnicos',$referentesTecnicos)
                                    ->with('usuariosDigitadores',$usuariosDigitadores)
                                    ->with('proveedores',$proveedores)
                                    ->with('itemsPresupuestarios',$itemsPresupuestarios)
                                    ->with('usuariosPorDevengar',$usuariosPorDevengar)
                                    ->with('fechasUtilizar',$fechasUtilizar);
    }

    /**
     * Filtro los documentos que aún no se han devengado
     */
    public function postFiltrarDocumentos(Request $request)
    {
        set_time_limit ( 0 );
        ini_set('max_execution_time', 0);
        
        $fechaInicio = fecha_Y_m_d($request->input('filtro_fecha_inicio'));
        $fechaTermino = fecha_Y_m_d($request->input('filtro_fecha_termino'));
        //dd($fechaInicio,$fechaTermino);
        
        $documentos = Documento::with([
                                    'getTipoInforme','getProveedor','getTipoDocumento','getModalidadCompra',
                                    'getDevengos.getItemPresupuestario','getRelacionRecepcionesValidadas.getWsDocumento'
                                ])
                               ->where('id_devengador',null)
                               ->where('reclamado', null);

        if ( $request->input('filtro_numero_documento') == null ) {
            if ( $request->input('filtro_fecha_a_utilizar') == 'Ingreso' ) {
                $documentos = $documentos->where('fecha_ingreso','>=',$fechaInicio.' 00:00:00');
            } elseif ( $request->input('filtro_fecha_a_utilizar') == 'Recepción' ) {
                $documentos = $documentos->where('fecha_recepcion','>=',$fechaInicio);
            }

            if ( $request->input('filtro_fecha_a_utilizar') == 'Ingreso' ) {
                $documentos = $documentos->where('fecha_ingreso','<=',$fechaTermino.' 23:59:59');
            } elseif ( $request->input('filtro_fecha_a_utilizar') == 'Recepción' ) {
                $documentos = $documentos->where('fecha_recepcion','<=',$fechaTermino);
            }
        } else {
            $documentos = $documentos->where('numero_documento', trim($request->input('filtro_numero_documento')) );
        }
        

        if ( $request->input('filtro_proveedor') != null ) {
            $documentos = $documentos->WhereIn('id_proveedor',$request->input('filtro_proveedor'));
        }

        if ( $request->input('filtro_tipo_documento') != null ) {
            $documentos = $documentos->WhereIn('id_tipo_documento',$request->input('filtro_tipo_documento'));
        }

        if ( $request->input('filtro_modalidad_compra') != null ) {
            $documentos = $documentos->WhereIn('id_modalidad_compra',$request->input('filtro_modalidad_compra'));
        }

        if ( $request->input('filtro_tipo_adjudicacion') != null ) {
            $documentos = $documentos->WhereIn('id_tipo_adjudicacion',$request->input('filtro_tipo_adjudicacion'));
        }

        if ( $request->input('filtro_tipo_informe') != null ) {
            $documentos = $documentos->WhereIn('id_tipo_informe',$request->input('filtro_tipo_informe'));
        }

        if ( $request->input('filtro_responsable') != null ) {
            $documentos = $documentos->WhereIn('id_responsable',$request->input('filtro_responsable'));
        }

        if ( $request->input('filtro_referente_tecnico') != null ) {
            $documentos = $documentos->WhereIn('id_referente_tecnico',$request->input('filtro_referente_tecnico'));
        }

        if ( $request->input('filtro_digitador') != null ) {
            $documentos = $documentos->WhereIn('id_digitador',$request->input('filtro_digitador'));
        }

        if ( $request->input('filtro_item_presupuestario') != null) {
            $documentos = $documentos->whereHas('getDevengos',function ($query) use ($request) {
                $query->WhereIn('id_item_presupuestario',$request->input('filtro_item_presupuestario'));
            });
        }

        $documentos = $documentos->get();

        $totalPorDevengar = 0;
        $datos = array();
        foreach ( $documentos as $index => $doc ) {
            // Se trabajan los datos para mostrar en la dataTable
            $datos[] = $doc->getDatosPorDevengar($request->input('filtro_item_presupuestario'), $totalPorDevengar);
        }
        
        $tiposDocumento = TipoDocumento::whereNotIn('id',[2,8])->get();
        $modalidadesCompra = ModalidadCompra::all();
        $tiposAdjudicacion = TipoAdjudicacion::all();
        $tiposInforme = TipoInforme::all();
        $usuariosResponsables = User::where('responsable',1)->get();
        $referentesTecnicos = ReferenteTecnico::all();
        $usuariosDigitadores = User::has('getDocumentos')->get();
        $usuariosPorDevengar = User::has('getDocumentosPorDevengar')->get();
        $proveedores = Proveedor::all();
        $itemsPresupuestarios = ItemPresupuestario::all();

        // Select para Fechas a utilizar en el filtro
        $fechaUtilizar1 = new \stdClass;
        $fechaUtilizar1->opcion = 'Ingreso';
        $fechaUtilizar2 = new \stdClass;
        $fechaUtilizar2->opcion = 'Recepción';

        $fechasUtilizar[] = $fechaUtilizar2;
        $fechasUtilizar[] = $fechaUtilizar1;
        
        return view('devengo.index')->with('documentos',$documentos)
                                    ->with('tiposDocumento',$tiposDocumento)
                                    ->with('modalidadesCompra',$modalidadesCompra)
                                    ->with('tiposAdjudicacion',$tiposAdjudicacion)
                                    ->with('tiposInforme',$tiposInforme)
                                    ->with('usuariosResponsables',$usuariosResponsables)
                                    ->with('referentesTecnicos',$referentesTecnicos)
                                    ->with('usuariosDigitadores',$usuariosDigitadores)
                                    ->with('usuariosPorDevengar',$usuariosPorDevengar)
                                    ->with('proveedores',$proveedores)
                                    ->with('itemsPresupuestarios',$itemsPresupuestarios)
                                    ->with('totalPorDevengar',$totalPorDevengar)
                                    ->with('fechasUtilizar',$fechasUtilizar)
                                    //filtros
                                    ->with('filtroFechaInicio',$request->input('filtro_fecha_inicio'))
                                    ->with('filtroFechaTermino',$request->input('filtro_fecha_termino'))
                                    ->with('filtroProveedor',$request->input('filtro_proveedor'))
                                    ->with('filtroTipoDocumento',$request->input('filtro_tipo_documento'))
                                    ->with('filtroModalidadCompra',$request->input('filtro_modalidad_compra'))
                                    ->with('filtroTipoAdjudicacion',$request->input('filtro_tipo_adjudicacion'))
                                    ->with('filtroTipoInforme',$request->input('filtro_tipo_informe'))
                                    ->with('filtroResponsable',$request->input('filtro_responsable'))
                                    ->with('filtroReferenteTecnico',$request->input('filtro_referente_tecnico'))
                                    ->with('filtroDigitador',$request->input('filtro_digitador'))
                                    ->with('filtroUsersPorDevengar',$request->input('filtro_users_para_devengar'))
                                    ->with('filtroItems',$request->input('filtro_item_presupuestario'))
                                    ->with('filtroFechasUtilizar',$request->input('filtro_fecha_a_utilizar'))
                                    ->with('filtroNumeroDocumento',$request->input('filtro_numero_documento'))

                                    ->with('dataTable',json_encode($datos));
    }

    public function dataTablePorDevengarDos(Request $request)
    {
        // dd($request->all());

        set_time_limit ( 0 );
        ini_set('max_execution_time', 0);

        $search = $request->get('search')['value'];
        $order_column = $request->get('order')[0]['column'];
        $order_dir = $request->get('order')[0]['dir'];

        $formJSON = json_decode($request->get('form'));
        $form = construyeFormulario($formJSON);

        $fechaInicio = fecha_Y_m_d($form->filtro_fecha_inicio);
        $fechaTermino = fecha_Y_m_d($form->filtro_fecha_termino);

        $documentos = Documento::porDevengar();

        if ( $form->filtro_numero_documento == null ) {
            // Se filtra por las fechas

            if ( $form->filtro_fecha_inicio != null ) {

                if ( $form->filtro_fecha_a_utilizar == 'Ingreso' ) {
                    $documentos = $documentos->where('fecha_ingreso','>=',$fechaInicio.' 00:00:00');
                } elseif ( $form->filtro_fecha_a_utilizar == 'Recepción' ) {
                    $documentos = $documentos->where('fecha_recepcion','>=',$fechaInicio);
                }

            }

            if ( $form->filtro_fecha_termino != null ) {
                if ( $form->filtro_fecha_a_utilizar == 'Ingreso' ) {
                    $documentos = $documentos->where('fecha_ingreso','<=',$fechaTermino.' 23:59:59');
                } elseif ( $form->filtro_fecha_a_utilizar == 'Recepción' ) {
                    $documentos = $documentos->where('fecha_recepcion','<=',$fechaTermino);
                }
            }

        } else {
            // no se consideran las fechas, y se busca el numero del documento
            $documentos = $documentos->where('numero_documento', trim($form->filtro_numero_documento) );
        }

        if ( isset($form->filtro_proveedor) ) {
            $documentos = $documentos->WhereIn('id_proveedor', $form->filtro_proveedor);
        }

        if ( isset($form->filtro_modalidad_compra) ) {
            $documentos = $documentos->WhereIn('id_modalidad_compra',$form->filtro_modalidad_compra);
        }

        if ( isset($form->filtro_tipo_adjudicacion) ) {
            $documentos = $documentos->WhereIn('id_tipo_adjudicacion',$form->filtro_tipo_adjudicacion);
        }

        if ( isset($form->filtro_tipo_informe) ) {
            $documentos = $documentos->WhereIn('id_tipo_informe',$form->filtro_tipo_informe);
        }

        if ( isset($form->filtro_responsable) ) {
            $documentos = $documentos->WhereIn('id_responsable',$form->filtro_responsable);
        }

        if ( isset($form->filtro_referente_tecnico) ) {
            $documentos = $documentos->WhereIn('id_referente_tecnico',$form->filtro_referente_tecnico);
        }

        if ( isset($form->filtro_digitador) ) {
            $documentos = $documentos->WhereIn('id_digitador',$form->filtro_digitador);
        }

        if ( isset($form->filtro_item_presupuestario) ) {
            $documentos = $documentos->whereHas('getDevengos',function ($query) use ($form) {
                $query->WhereIn('id_item_presupuestario',$form->filtro_item_presupuestario);
            });
        }

        if ( \Entrust::hasRole('usuario-ingreso') ) {
            $documentos = $documentos->where(function ($query){
                                        $query->where('id_user_responsable', \Auth::user()->id)
                                              ->orWhere('id_user_responsable', null);
                                    });
        }

        if ( isset($form->filtro_tipo_documento) ) {
            
            if ( in_array( '4', $form->filtro_tipo_documento) || in_array( '5', $form->filtro_tipo_documento) ) {
                if ( in_array( '4', $form->filtro_tipo_documento) ) {
                    $documentos = $documentos->WhereIn('id_tipo_documento', [4]);
                                             
                }

                if ( in_array( '5', $form->filtro_tipo_documento) ) {
                    $documentos = $documentos->WhereIn('id_tipo_documento', [5]);
                }

                $documentos = $documentos->whereHas('getDocumentoRelacionado', function ($query) {
                                                // $query->where('id_estado', 2)
                                                //       ->where('fecha_devengado', '<>', null);
                                                $query->has('getFoliosSigfe'); 
                                           })
                                         ->get();
                
            } else {
                $documentos = $documentos->WhereIn('id_tipo_documento',$form->filtro_tipo_documento);

                $documentos = $documentos
                        // ->where(function ($query) {
                        //     $query->where('id_relacion', '<>', null)
                        //           ->orWhere('validado', '=', 1)
                        //           ->orWhere('directo_por_devengar', 1)
                        //           ->orWhereHas('getVistoBueno', function ($queryTwo){
                        //             $queryTwo->where('id_registrador_memo_respuesta','<>',null);
                        //           });
                        // })
                        ->get();
            }

        } else {
            $documentos = $documentos
                        // ->where(function ($query) {
                        //     $query->where('id_relacion', '<>', null)
                        //           ->orWhere('validado', '=', 1)
                        //           ->orWhere('directo_por_devengar', 1)
                        //           ->orWhereHas('getVistoBueno', function ($queryTwo){
                        //             $queryTwo->where('id_registrador_memo_respuesta','<>',null);
                        //           });
                        // })
                        ->get();
                        // $documentos = $documentos->get();
        }

        $data = [];

        if ( $request->get('length') != -1 ) {
            $hasta = $request->get('start') + $request->get('length');
        } else {
            // Se quieren ver todos los datos
            $hasta = $request->get('start') + $documentos->count();
        }
    
        for( $i = $request->get('start') ; $i < $hasta ; $i++ ) {
            if ( isset($documentos[$i]) ) {
                if ( isset($form->filtro_item_presupuestario) ) {
                    $data[] = $documentos[$i]->getDatosPorDevengar2($form->filtro_item_presupuestario);
                } else {
                    $data[] = $documentos[$i]->getDatosPorDevengar2();
                }
                
            }
        }

        // Se realiza la busqueda y se ordenan los datos
        if ( $documentos->count() > 0 ) {

            foreach ($data as $clave => $fila) {

                if ( $search != '' ) {
                    if ( in_array_r($search,$fila) ) {
                        // dd("lo encuentra",$fila,$search);
                        $columna[$clave] = $fila[$order_column];
                    } else {
                        // dd("NO lo encuentra",$fila,$search);
                        unset( $data[$clave] ); // quita el elemento del data
                    }
                } else {
                    $columna[$clave] = $fila[$order_column];
                }
                
            }

            
            // Se ordenan los datos, segun la columna seleccionada
            if ( $order_dir == 'asc' ) {
                array_multisort($columna, SORT_ASC, $data);
            } elseif ( $order_dir == 'desc' ) {
                array_multisort($columna, SORT_DESC, $data);
            }

        }

        $totalPorDevengar = 0;
        foreach ( $documentos as $index => $doc ) {
            $totalPorDevengarDocumento = 0;
            foreach ( $doc->getDevengos as $devengo) {
                $totalPorDevengarDocumento += $devengo->monto;
            }
            if ($doc->id_tipo_documento == 4 || $doc->id_tipo_documento == 10) {
                $totalPorDevengar = $totalPorDevengar - $totalPorDevengarDocumento;
            } else {
                $totalPorDevengar = $totalPorDevengar + $totalPorDevengarDocumento;
            }
        }

        $respuesta = array(
            "draw" => $request->get('draw'),
            "recordsTotal" => count($documentos),
            "recordsFiltered"=> count($documentos),
            "data" => $data,
            "totalPorDevengar" => '$ '.formatoMiles($totalPorDevengar),
        );

        return json_encode($respuesta);
    }

    public function dataTableDocumentosDevengados(Request $request )
    {

        set_time_limit ( 0 );
        ini_set('max_execution_time', 0);

        $search = $request->get('search')['value'];
        $order_column = $request->get('order')[0]['column'];
        $order_dir = $request->get('order')[0]['dir'];

        $formJSON = json_decode($request->get('form'));
        $form = construyeFormulario($formJSON);

        $fechaInicio = '';
        if ( $form->filtro_fecha_inicio != '' ) {
            $fechaInicio = fecha_Y_m_d($form->filtro_fecha_inicio);
        }
        
        $fechaTermino = '';
        if ( $form->filtro_fecha_termino != '' ) {
            $fechaTermino = fecha_Y_m_d($form->filtro_fecha_termino);
        }
        
        $foliosSigfe = FolioSigfe::with(['getDevengos','getDevengos.getItemPresupuestario','getDocumento',
                                         'getDocumento.getTipoInforme','getDocumento.getProveedor',
                                         'getDocumento.getTipoDocumento','getDocumento.getModalidadCompra',
                                         'getDocumento.getDevengos','getDocumento.getDevengos.getItemPresupuestario']);
                                //  ->whereHas('getDocumento', function($query){
                                //         $query->where('id_devengador', '<>', null);
                                //   });

                            
        if ( $form->filtro_numero_documento == null ) {
            // Se filtra por las fechas

            if ( $form->filtro_fecha_a_utilizar == 'Ingreso' ) {

                $foliosSigfe = $foliosSigfe->whereHas('getDocumento', function($query) use( $fechaInicio, $fechaTermino ) {
                    
                                                if ( $fechaInicio != '' ) {
                                                    $query->where('fecha_ingreso','>=', $fechaInicio.' 00:00:00');
                                                }
                                                
                                                if ( $fechaTermino != '' ) {
                                                    $query->where('fecha_ingreso','<=', $fechaTermino.' 23:59:59');
                                                }
                                                    
                                            });

            } elseif ( $form->filtro_fecha_a_utilizar == 'Recepción' ) {

                $foliosSigfe = $foliosSigfe->whereHas('getDocumento', function($query) use( $fechaInicio, $fechaTermino ) {
                                                if ( $fechaInicio != '' ) {
                                                    $query->where('fecha_recepcion','>=', $fechaInicio);
                                                }

                                                if ( $fechaTermino != '' ) {
                                                    $query->where('fecha_recepcion','<=', $fechaTermino);
                                                }
                                                      
                                            });

            } elseif ( $form->filtro_fecha_a_utilizar == 'SIGFE' ) {
                
                if ( $fechaInicio != '' ) {
                    $foliosSigfe = $foliosSigfe->where('fecha_sigfe','>=', $fechaInicio);
                }

                if ( $fechaTermino != '' ) {
                    $foliosSigfe = $foliosSigfe->where('fecha_sigfe','<=', $fechaTermino);
                }                       

            }

        } else {
            // no se consideran las fechas, y se busca el numero del documento
            $foliosSigfe = $foliosSigfe->whereHas('getDocumento', function($query) use( $form ){
                                        $query->where('numero_documento', trim($form->filtro_numero_documento) );
                                    });
        }

        if ( isset($form->filtro_proveedor) ) {

            $foliosSigfe = $foliosSigfe->whereHas('getDocumento', function($query) use( $form ){
                                            $query->WhereIn('id_proveedor', $form->filtro_proveedor);
                                        });
        }

        if ( isset($form->filtro_tipo_documento) ) {

            $foliosSigfe = $foliosSigfe->whereHas('getDocumento', function($query) use( $form ){
                                            $query->WhereIn('id_tipo_documento', $form->filtro_tipo_documento);
                                        });
        }

        if ( isset($form->filtro_modalidad_compra) ) {

            $foliosSigfe = $foliosSigfe->whereHas('getDocumento', function($query) use( $form ){
                                            $query->WhereIn('id_modalidad_compra', $form->filtro_modalidad_compra);
                                        });
        }

        if ( isset($form->filtro_tipo_adjudicacion) ) {

            $foliosSigfe = $foliosSigfe->whereHas('getDocumento', function($query) use( $form ){
                                            $query->WhereIn('id_tipo_adjudicacion', $form->filtro_tipo_adjudicacion);
                                        });
        }

        if ( isset($form->filtro_tipo_informe) ) {

            $foliosSigfe = $foliosSigfe->whereHas('getDocumento', function($query) use( $form ){
                                            $query->WhereIn('id_tipo_informe', $form->filtro_tipo_informe);
                                        });
        }

        if ( isset($form->filtro_responsable) ) {

            $foliosSigfe = $foliosSigfe->whereHas('getDocumento', function($query) use( $form ){
                                            $query->WhereIn('id_responsable', $form->filtro_responsable);
                                        });
        }

        if ( isset($form->filtro_referente_tecnico) ) {

            $foliosSigfe = $foliosSigfe->whereHas('getDocumento', function($query) use( $form ){
                                            $query->WhereIn('id_referente_tecnico', $form->filtro_referente_tecnico);
                                        });
        }

        if ( isset($form->filtro_digitador) ) {

            $foliosSigfe = $foliosSigfe->whereHas('getDocumento', function($query) use( $form ){
                                            $query->WhereIn('id_digitador', $form->filtro_digitador);
                                        });
        }

        if ( isset($form->filtro_item_presupuestario) ) {

            $foliosSigfe = $foliosSigfe->whereHas('getDevengos', function($query) use( $form ){
                                            $query->WhereIn('id_item_presupuestario', $form->filtro_item_presupuestario);
                                        });
        }

        if ( $form->filtro_id_sigfe != null ) {
            $foliosSigfe = $foliosSigfe->where('id_sigfe', $form->filtro_id_sigfe);
        }

        $foliosSigfe = $foliosSigfe->get();

        $data = [];

        if ( $request->get('length') != -1 ) {
            $hasta = $request->get('start') + $request->get('length');
        } else {
            // Se quieren ver todos los datos
            $hasta = $request->get('start') + $foliosSigfe->count();
        }

        for( $i = $request->get('start') ; $i < $hasta ; $i++ ) {
            if ( isset($foliosSigfe[$i]) ) {
                
                $foliosSigfe[$i]->getDocumento->idFolioSigfe       = $foliosSigfe[$i]->id;
                $foliosSigfe[$i]->getDocumento->fechaSigfe       = $foliosSigfe[$i]->fecha_sigfe;
                $foliosSigfe[$i]->getDocumento->idSigfe            = $foliosSigfe[$i]->id_sigfe;
                $foliosSigfe[$i]->getDocumento->detalleSigfe       = $foliosSigfe[$i]->detalle_sigfe;
                $foliosSigfe[$i]->getDocumento->devengosFolioSigfe = $foliosSigfe[$i]->getDevengos;

                if ( isset($form->filtro_item_presupuestario) ) {
                    $data[] = $foliosSigfe[$i]->getDocumento->getDatosDevengados($form->filtro_item_presupuestario);
                } else {
                    $data[] = $foliosSigfe[$i]->getDocumento->getDatosDevengados();
                }
                
            }
        }

        // Se realiza la busqueda y se ordenan los datos
        if ( $foliosSigfe->count() > 0 ) {

            foreach ($data as $clave => $fila) {

                if ( $search != '' ) {
                    if ( in_array_r($search,$fila) ) {
                        // dd("lo encuentra",$fila,$search);
                        $columna[$clave] = $fila[$order_column];
                    } else {
                        // dd("NO lo encuentra",$fila,$search);
                        unset( $data[$clave] ); // quita el elemento del data
                    }
                } else {
                    $columna[$clave] = $fila[$order_column];
                }
                
            }

            
            // Se ordenan los datos, segun la columna seleccionada
            if ( $order_dir == 'asc' ) {
                array_multisort($columna, SORT_ASC, $data);
            } elseif ( $order_dir == 'desc' ) {
                array_multisort($columna, SORT_DESC, $data);
            }

        }

        $totalDevengado = 0;
        foreach ( $foliosSigfe as $index => $folioSigfe ) {
            $totalDevengadoFolio = 0;
            foreach ( $folioSigfe->getDevengos as $devengo) {

                if ( isset($form->filtro_item_presupuestario) ) {

                    if ( in_array($devengo->id_item_presupuestario, $form->filtro_item_presupuestario) ) {
                        $totalDevengadoFolio += $devengo->monto;
                    }
                    

                } else {
                    $totalDevengadoFolio += $devengo->monto;
                }
                
            }



            if ($folioSigfe->getDocumento->id_tipo_documento == 4 || $folioSigfe->getDocumento->id_tipo_documento == 10) {
                $totalDevengado = $totalDevengado - $totalDevengadoFolio;
            } else {
                $totalDevengado = $totalDevengado + $totalDevengadoFolio;
            }
        }

        $respuesta = array(
            "draw" => $request->get('draw'),
            "recordsTotal" => $foliosSigfe->count(),
            "recordsFiltered"=> $foliosSigfe->count(),
            "data" => $data,
            "totalDevengado" => '$ '.formatoMiles($totalDevengado),
        );

        return json_encode($respuesta);

    }

    /**
     * Muestra los documentos que aún no se han devengado
     */
    public function getDevengarDocumentosDos()
    {
        if (!\Entrust::can(['listado-por-devengar','devengar-documento'])) {
            return \Redirect::to('home');
        }

        $tiposDocumento = TipoDocumento::whereNotIn('id',[2,8])->get();
        $modalidadesCompra = ModalidadCompra::all();
        $tiposAdjudicacion = TipoAdjudicacion::all();
        $tiposInforme = TipoInforme::all();
        $usuariosResponsables = User::where('responsable',1)->get();
        $referentesTecnicos = ReferenteTecnico::all();
        $usuariosDigitadores = User::has('getDocumentos')->get();
        $proveedores = Proveedor::all();
        $itemsPresupuestarios = ItemPresupuestario::all();

        // Select para Fechas a utilizar en el filtro
        $fechaUtilizar1 = new \stdClass;
        $fechaUtilizar1->opcion = 'Ingreso';
        $fechaUtilizar2 = new \stdClass;
        $fechaUtilizar2->opcion = 'Recepción';

        $fechasUtilizar[] = $fechaUtilizar2;
        $fechasUtilizar[] = $fechaUtilizar1;

        return view('devengo.por_devengar')->with('tiposDocumento',$tiposDocumento)
                                           ->with('modalidadesCompra',$modalidadesCompra)
                                           ->with('tiposAdjudicacion',$tiposAdjudicacion)
                                           ->with('tiposInforme',$tiposInforme)
                                           ->with('usuariosResponsables',$usuariosResponsables)
                                           ->with('referentesTecnicos',$referentesTecnicos)
                                           ->with('usuariosDigitadores',$usuariosDigitadores)
                                           ->with('proveedores',$proveedores)
                                           ->with('itemsPresupuestarios',$itemsPresupuestarios)
                                           ->with('fechasUtilizar',$fechasUtilizar);
    }

    /**
     * Se trabaja una descripción dependiendo del tipo de documento y el documento relacionado
     */
    public function getDescripcion($documento)
    {
        $descripcion = '';
        if ( $documento->id_devengador == null ) {
            if ( $documento->id_tipo_documento == 4 || $documento->id_tipo_documento == 10 || $documento->id_tipo_documento == 5 || $documento->id_tipo_documento == 11 ) {
                if ( $documento->getDocumentoRelacionado ) {

                    $montoDocumentoRelacionado = $documento->getDocumentoRelacionado->total_documento;
                    foreach ( $documento->getDocumentoRelacionado->getDevengos as $key => $devengo ) {

                        if ( $documento->getDocumentoRelacionado->id_tipo_documento == 4 || $documento->getDocumentoRelacionado->id_tipo_documento == 10 ) {
                            $montoDocumentoRelacionado = $montoDocumentoRelacionado - $devengo->monto;
                        } elseif ( $documento->getDocumentoRelacionado->id_tipo_documento == 5 || $documento->getDocumentoRelacionado->id_tipo_documento == 11 ) {
                            $montoDocumentoRelacionado = $montoDocumentoRelacionado + $devengo->monto;
                        }
                        
                    }
                    
                    $glosaDocumento = '';
                    if ( $documento->id_tipo_documento == 10 || $documento->id_tipo_documento == 11 ) {
                        $glosaDocumento = 'Carta Certificada ';
                    }

                    if ( $documento->id_tipo_documento == 4 || $documento->id_tipo_documento == 10 ) {
                        if ( $montoDocumentoRelacionado <= 0 ) {
                            if ($documento->getDocumentoRelacionado->id == $documento->id) {
                                $descripcion = $glosaDocumento.'Anula Fact. '.$documento->getDocumentoRelacionado->numero_documento;
                            } else {
                                $descripcion = $glosaDocumento.'Rebaja Fact. '.$documento->getDocumentoRelacionado->numero_documento;
                            }
                        } else {
                            $descripcion = $glosaDocumento.'Rebaja Fact. '.$documento->getDocumentoRelacionado->numero_documento;
                        }
                    } else if ( $documento->id_tipo_documento == 5 || $documento->id_tipo_documento == 11 ) {
                        $descripcion = $glosaDocumento.'Aumenta Fact. '.$documento->getDocumentoRelacionado->numero_documento;
                    }

                }
            }
        }
            
        return $descripcion;
    }

    public function getModalDevengar($id, $idFolioSigfe = null)
    {
        $documento = Documento::with(['getProveedor','getTipoDocumento','getModalidadCompra',
                                      'getTipoAdjudicacion','getTipoInforme','getResponsable',
                                      'getReferenteTecnico','getDigitador','getDevengador',
                                      'getArchivos','getArchivos.getTipoArchivo','getDevengos',
                                      'getDevengos.getItemPresupuestario','getRelacionRecepcionesValidadas.getUsuarioResponsable',
                                      'getRelacionRecepcionesValidadas.getWsDocumento.getWsArchivos'])
                                ->findOrfail($id);
                                //dd($documento);
        $anho_limite = date('Y',strtotime ( '-1 year' , strtotime ( date("Y") ) ) );
        
        $descripcion = $this->getDescripcion($documento); // pasar funcion al modelo

        $folioSigfe = null;
        if ( $idFolioSigfe ) {
            $folioSigfe = FolioSigfe::findOrfail($idFolioSigfe);
        }
        
        $tiposDocumento = TipoDocumento::whereNotIn('id',[2,8])->get();
        $modalidadesCompra = ModalidadCompra::all();
        $tiposAdjudicacion = TipoAdjudicacion::all();
        $tiposInforme = TipoInforme::all();
        $usuarios = User::where('responsable',1)->get();
        $referentesTecnicos = ReferenteTecnico::all();
        $tiposArchivo = TipoArchivo::all();

        // Para obtener documentos relacionados dependiendo del tipo de documento
        $documentosProveedor = null;
        $docsRechazadosAceptaProv = null;
        if ( $documento->id_tipo_documento == 1 ) {
            // Solo Facturas con NC al 100%
            // Que no tengan Facturas relacionadas, si la tienen es porque ya se refacturo
            $documentosProveedor = Documento::with(['getTipoDocumento', 'getDocumentosRelacionados'])
                                            ->where('id_proveedor',$documento->id_proveedor)
                                            ->whereHas('getDocumentosRelacionados', function($query) {
                                                $query->where('id_tipo_documento', 4); // NC
                                            })
                                            ->whereDoesntHave('getDocumentosRelacionados', function ($query) {
                                                $query->where('id_tipo_documento', 1); // FC
                                            })
                                            ->where('total_documento_actualizado' , 0) // significa que la factura esta anulada
                                            ->where('id','<>',$documento->id)
                                            ->get();

             // Solo facturas con NC por el 100% y que no tengan refactura (getDocumentoFactura)
             $docsRechazadosAceptaProv = ArchivoAcepta::with(['getProveedor','getTipoDocumento'])
                                                      ->where('cargado', 0)
                                                      ->where('id_user_rechazo','<>',null)
                                                      ->where('id_proveedor', $documento->id_proveedor)
                                                      ->where('id_tipo_documento', 1)
                                                      ->whereHas('getAceptaMotivoRechazo', function ($query){
                                                          $query->where('id_motivo_rechazo', 5); // Anula Documento
                                                        })
                                                      ->where('observacion_rechazo', 'LIKE', '%Tiene una Nota de Crédito por el total del monto%')
                                                      ->doesntHave('getDocumentoFactura') // Con esto, no sale el archivo acepta que tiene el documento( como refactura)
                                                      ->get();

        } elseif ( $documento->id_tipo_documento == 4 || $documento->id_tipo_documento == 5 || $documento->id_tipo_documento == 10 || $documento->id_tipo_documento == 11 ) {

            $documentosProveedor = Documento::with(['getDevengos.getItemPresupuestario','getTipoDocumento'])
                                            ->where('id_proveedor',$documento->id_proveedor)
                                            ->whereIn('id_tipo_documento',[1,2,8]);
            if ( $documento->id_relacionado != null ) {
                $documentosProveedor = $documentosProveedor->whereNotIn('id',[$documento->id,$documento->id_relacionado])->get();
            } else {
                $documentosProveedor = $documentosProveedor->where('id','<>',$documento->id)->get();
            }

        } elseif ($documento->id_tipo_documento == 8 ) {
            $documentosProveedor = Documento::with(['getDevengos.getItemPresupuestario','getTipoDocumento'])
                                            ->where('id_proveedor',$documento->id_proveedor)
                                            ->where('id_tipo_documento',4);
            if ( $documento->id_relacionado != null ) {
                $documentosProveedor = $documentosProveedor->whereNotIn('id',[$documento->id,$documento->id_relacionado])->get();
            } else {
                $documentosProveedor = $documentosProveedor->where('id','<>',$documento->id)->get();
            }
        }

        $detalleRecepcion = '';
        if ( $documento->getRelacionRecepcionesValidadas->count() > 0 ) {
            foreach ($documento->getRelacionRecepcionesValidadas as $relacion) {
                $len = count($relacion->getWsDocumento->getWsItemsPresupuestarios);
                foreach ($relacion->getWsDocumento->getWsItemsPresupuestarios as $key => $item) {

                    if ( $key != $len - 1 ) {
                        $detalleRecepcion .= ''.$item->descripcion_articulo.PHP_EOL;
                    } else {
                        $detalleRecepcion .= ''.$item->descripcion_articulo;
                    }

                }

            }
        }


        return view('devengo.modal_devengar_documento')->with('documento',$documento)
                                                       ->with('anho_limite',$anho_limite)
                                                       ->with('descripcion',$descripcion)
                                                       ->with('tiposDocumento',$tiposDocumento)
                                                       ->with('modalidadesCompra',$modalidadesCompra)
                                                       ->with('tiposAdjudicacion',$tiposAdjudicacion)
                                                       ->with('tiposInforme',$tiposInforme)
                                                       ->with('usuarios',$usuarios)
                                                       ->with('referentesTecnicos',$referentesTecnicos)
                                                       ->with('tiposArchivo',$tiposArchivo)
                                                       ->with('documentosProveedor',$documentosProveedor)
                                                       ->with('docsRechazadosAceptaProv', $docsRechazadosAceptaProv)
                                                       ->with('detalleRecepcion',$detalleRecepcion)
                                                       ->with('folioSigfe', $folioSigfe);
    }

    /**
     * Funcion que valida el numero de orden de compra y el numero licitacion, segun los formatos establecidos.
     * Retorna el array datos que se envia por json a las vistas
    */
    public function validarNumOrdenCompraAndNumLicitacion($request)
    {
        $datos = array(
            'mensaje_documento' => 'Todo bien',
            'estado_documento' => 'success',
        );
        // Validar N°Documento Compra o N° Orden Compra y N° Licitación
        if ( $request->input('modalidad_compra') == 1 && substr( $request->input('numero_orden_compra'), 0, 5) != "1641-") {
            $datos = array(
                'mensaje_documento' => 'El "N° Orden de Compra" debe iniciar con "1641-"',
                'estado_documento' => 'error',
            );
        }

        if ( $request->input('numero_licitacion') != '' && substr( $request->input('numero_licitacion'), 0, 5) != "1641-") {
            $datos = array(
                'mensaje_documento' => 'El "N° Licitación" debe iniciar con "1641-"',
                'estado_documento' => 'error',
            );
        }

        return $datos;
    }

    public function postDevengarDocumento(Request $request)
    {
        // sleep(5);
        // dd('devengar documento',$request->all());
        //mensajes de los validadores
        $messages = [
            'required'    => 'Debe ingresar el :attribute',
            'numeric' => 'El :attribute debe solo contener números',
            'max' => 'El :attribute no debe exeder los :max caracteres',
            'min' => 'El :attribute debe tener minimo :min caracteres',
        ];

        //validador de los input del formulario
        $validator = \Validator::make($request->all(), [
            // requeridos para devengar el documento
            'id_sigfe' => 'required|numeric',
            'fecha_id' => 'required',
            'detalle_documento' => 'required',
            'valor_item' => 'required',
            // requeridos para editar el documento
            'nombre_proveedor' => 'required|numeric',
            'tipo_documento' => 'required|numeric',
            'numero_documento' => 'required',
            'modalidad_compra' => 'required|numeric',
            'tipo_adjudicacion' => 'required|numeric',
            'numero_orden_compra' => 'required',
            'fecha_documento' => 'required|date_format:"d/m/Y"',
            'tipo_informe' => 'required|numeric',
            // 'responsable' => 'required|numeric',
            'fecha_recepcion' => 'required|date_format:"d/m/Y"',
            'valor_total_documento' => 'required'
        ], $messages);

        //Si contiene errores se devuelven todos los errores, de lo contrario guarda en la base de datos
        if ( $validator->fails() ) {            
            return response()->json($validator->errors(),400);
        } else {

            $datos = $this->validarSigfe($request);
            if ( $datos['estado_documento'] == 'success' ) {

                $datos = $this->validarNumOrdenCompraAndNumLicitacion($request);
                if ( $datos['estado_documento'] == 'success' ) {
                    // Se valida que para el proveedor y el tipo de documento, no se repita el numero documento
                    // Se agrega a la validacion los documentos eliminados, ya que pueden ser recuperados.
                    $documentoAux = Documento::withTrashed()
                                             ->where('id_proveedor', $request->input('nombre_proveedor'))
                                             ->where('id_tipo_documento', $request->input('tipo_documento'))
                                             ->where('numero_documento', $request->input('numero_documento'))
                                             ->where('id','<>',$request->input('id_documento'))
                                             ->first();

                    if ( !is_object($documentoAux) ) {

                        $devengarDocumento = Documento::findOrFail($request->input('id_documento'));
                        
                        /**
                         * el valor datatable de la variable $datos es para filtros en la vista, para modificar
                         * la tabla principal de la vista.
                         */
                        if ($devengarDocumento->id_devengador == null) {
                            // Significa que se esta devengado por primera vez documento
                            $datos['datatable'] ='devengar_listo_documento';
                        } else {
                            // Significa que se esta editando el documento, el "devengador"
                            $datos['datatable'] ='devengar_editado_documento';
                        }

                        $devengarDocumento->editDocumento($request);
                        $folioSigfe = $devengarDocumento->editDocumentoDevengar($request);
                        // dd($folioSigfe);
                        // Se crean los devengos, contienen el id del item presupuestario, id doc & id folio sigfe
                        if ( $request->input('valor_item') ) {

                            $devengarDocumento->devengarItemsPresupuestarios($request, $folioSigfe);
                            
                        } else {
                            // Si no vienen items(devengo es la tabla que guarda los items), se deben borrar los que pertenecen al documento.
                            // Siempre debe venir al menos un item para devengar, por lo tanto nunca se ingresa acá
                            dd('No hay items');
                            foreach ($devengarDocumento->getDevengos as $devengo) {
                                $devengo->delete();
                            }

                        }

                        // Eliminar los archivos seleccionados para eliminar
                        if ( $request->input('delete_list') ) {
                            foreach ($request->input('delete_list') as $idArchivo) {
                                $archivoDelete = Archivo::findOrfail($idArchivo);
                                $archivoDelete->delete();
                            }
                        }

                        $archivo = $request->file('archivo');
                        if ( $archivo != null ) {
                            $nombreOriginalArchivo = str_replace('.pdf','',$archivo->getClientOriginalName() );
                            $arreglo_nombre = explode(".", $archivo->getClientOriginalName() );
                            $extension = strtolower( end( $arreglo_nombre ) );

                            if ($extension != 'pdf') {
                                $datos['mensaje_archivo'] = 'Extension incorrecta del archivo';
                                $datos['estado_archivo'] = 'error';
                            } else {
                                // Se trabajan las rutas del archivo, se guarda en el proyecto al final de la funcion
                                $nombre = $nombreOriginalArchivo.'_'.date('Y_m_d_G_i_s');
                                $rutaArchivo = '/documento/'.$devengarDocumento->id.'/'.$nombre.'.pdf';
                                $pathArchivo = public_path().'/documento/'.$devengarDocumento->id.'/';

                                $newArchivo = new Archivo();
                                $newArchivo->id_documento = $devengarDocumento->id;
                                $newArchivo->id_tipo_archivo = $request->input('tipo_archivo');

                                $newArchivo->nombre = $nombre;
                                $newArchivo->nombre_original = $nombreOriginalArchivo;
                                $newArchivo->ubicacion = "documento/".$devengarDocumento->id."/".$newArchivo->nombre.".pdf";
                                $newArchivo->extension = "pdf";
                                $newArchivo->peso = $archivo->getSize();
                                $newArchivo->save();

                                $datos['mensaje_archivo'] = 'Se ha guardado correctamente el archivo';
                                $datos['estado_archivo'] = 'success';

                                $archivo->move( $pathArchivo , $nombre.'.pdf' ); // Se guarda el archivo
                            }
                            
                        } else {
                            $datos['mensaje_archivo'] = '';
                            $datos['estado_archivo'] = '';
                        }

                        // Mensajes para la vista.
                        $datos['mensaje_documento'] = 'Devengado exitoso del documento.';
                        $datos['estado_documento'] = 'success';
                        $datos['id'] = $devengarDocumento->id;
                        
                    } else {
                        $datos = array(
                            'mensaje_documento' => 'N° Documento repetido para el Proveedor y Tipo Documento seleccionados',
                            'estado_documento' => 'error',
                        );

                        if ( $documentoAux->trashed() ) {
                            $datos['mensaje_documento'] = 'N° Documento repetido para el Proveedor y Tipo Documento seleccionados.<br><br> El Documento se encuentra eliminado.<br> Observaciones : <br>'.$documentoAux->observacion;
                        }
                    }
                }

            }
            

        }

        return response()->json($datos,200);
    }

    /**
     * Funcion que valida el id_sigfe y el año del sigfe sean unicos.
     * Retorna el array datos que se envia por json a las vistas
    */
    public function validarSigfe($request)
    {
        $datos = array(
            'mensaje_documento' => 'Todo bien',
            'estado_documento'  => 'success',
        );

        // Nuevo
        /**
         * Se valida que el id_sigfe no este ingresado en el sistema, para otro documento.
         */
        $folioSigfe = FolioSigfe::with(['getDocumento','getDocumento.getTipoDocumento','getDocumento.getProveedor'])
                                ->where('id_documento','<>', $request->input('id_documento'))
                                ->where('id_sigfe', $request->input('id_sigfe'))
                                ->where('year_sigfe', fecha_Y($request->input('fecha_id')))
                                ->first();

        if ( is_object($folioSigfe) ) {
            $datos = array(
                'mensaje_documento' => 'El ID SIGFE ya se encuentra en el sistema para el año '.fecha_Y($request->input('fecha_id')).'
                                        <br><br><p style="color:black">N° Documento: '.$folioSigfe->getDocumento->numero_documento.'
                                        <br>Tipo: '.$folioSigfe->getDocumento->getTipoDocumento->nombre.'
                                        <br>Proveedor: '.$folioSigfe->getDocumento->getProveedor->rut.'</p>',
                'estado_documento' => 'error',
            );
        }

        return $datos;
    }

    /**
     * Muestra todos los documentos devengados del mes actual
     */
    public function getDocumentosDevengados()
    {
        if ( ! \Entrust::can(['listado-devengado','editar-devengo']) ) {
            return \Redirect::to('home');
        }
        
        $tiposDocumento = TipoDocumento::whereNotIn('id',[2,8])->get();
        $modalidadesCompra = ModalidadCompra::all();
        $tiposAdjudicacion = TipoAdjudicacion::all();
        $tiposInforme = TipoInforme::all();
        $usuariosResponsables = User::where('responsable',1)->get();
        $referentesTecnicos = ReferenteTecnico::all();
        $usuariosDigitadores = User::has('getDocumentos')->get();
        $proveedores = Proveedor::all();
        $itemsPresupuestarios = ItemPresupuestario::all();

        // Select para Fechas a utilizar en el filtro
        $fechaUtilizar1 = new \stdClass;
        $fechaUtilizar1->opcion = 'Ingreso';
        $fechaUtilizar2 = new \stdClass;
        $fechaUtilizar2->opcion = 'Recepción';
        $fechaUtilizar3 = new \stdClass;
        $fechaUtilizar3->opcion = 'SIGFE';

        $fechasUtilizar[] = $fechaUtilizar3;
        $fechasUtilizar[] = $fechaUtilizar2;
        $fechasUtilizar[] = $fechaUtilizar1;

        return view('devengo.devengados')->with('tiposDocumento',$tiposDocumento)
                                         ->with('modalidadesCompra',$modalidadesCompra)
                                         ->with('tiposAdjudicacion',$tiposAdjudicacion)
                                         ->with('tiposInforme',$tiposInforme)
                                         ->with('usuariosResponsables',$usuariosResponsables)
                                         ->with('referentesTecnicos',$referentesTecnicos)
                                         ->with('usuariosDigitadores',$usuariosDigitadores)
                                         ->with('proveedores',$proveedores)
                                         ->with('itemsPresupuestarios',$itemsPresupuestarios)
                                         ->with('fechasUtilizar',$fechasUtilizar);
    }

    /**
     * Para generar excel de los documentos devengados
     */
    public function postGenerarExcel(Request $request)
    {
        set_time_limit ( 0 );
        ini_set('max_execution_time', 0);
        // dd($request->all());
        
        \Excel::create('Informe Devengados '.date('d-m-Y').'', function($excel) use($request) {
        
            $fechaInicio = '';
            if ( $request->input('filtro_fecha_inicio') != '' ) {
                $fechaInicio = fecha_Y_m_d($request->input('filtro_fecha_inicio'));
            }
            
            $fechaTermino = '';
            if ( $request->input('filtro_fecha_termino') != '' ) {
                $fechaTermino = fecha_Y_m_d($request->input('filtro_fecha_termino'));
            }

            $foliosSigfe = FolioSigfe::with(['getDevengos','getDevengos.getItemPresupuestario','getDocumento',
                                             'getDocumento.getTipoInforme','getDocumento.getProveedor',
                                             'getDocumento.getTipoDocumento','getDocumento.getModalidadCompra',
                                             'getDocumento.getDevengos','getDocumento.getDevengos.getItemPresupuestario',
                                             'getDocumento.getRelacionRecepcionesNoValidadas'])
                                        ->whereHas('getDocumento', function($query){
                                            $query->where('id_devengador', '<>', null);
                                        });


            if ( $request->input('filtro_numero_documento') == null ) {

                if ( $request->input('filtro_fecha_a_utilizar') == 'Ingreso' ) {
                    
                    $foliosSigfe = $foliosSigfe->whereHas('getDocumento', function($query) use( $fechaInicio, $fechaTermino ) {
                    
                                                    if ( $fechaInicio != '' ) {
                                                        $query->where('fecha_ingreso','>=', $fechaInicio.' 00:00:00');
                                                    }
                                                    
                                                    if ( $fechaTermino != '' ) {
                                                        $query->where('fecha_ingreso','<=', $fechaTermino.' 23:59:59');
                                                    }
                                                        
                                                });

                } elseif ( $request->input('filtro_fecha_a_utilizar') == 'Recepción' ) {

                    $foliosSigfe = $foliosSigfe->whereHas('getDocumento', function($query) use( $fechaInicio, $fechaTermino ) {

                                                    if ( $fechaInicio != '' ) {
                                                        $query->where('fecha_recepcion','>=', $fechaInicio);
                                                    }

                                                    if ( $fechaTermino != '' ) {
                                                        $query->where('fecha_recepcion','<=', $fechaTermino);
                                                    }
                                                        
                                                });

                } elseif ( $request->input('filtro_fecha_a_utilizar') == 'SIGFE' ) {

                    if ( $fechaInicio != '' ) {
                        $foliosSigfe = $foliosSigfe->where('fecha_sigfe','>=', $fechaInicio);
                    }
    
                    if ( $fechaTermino != '' ) {
                        $foliosSigfe = $foliosSigfe->where('fecha_sigfe','<=', $fechaTermino);
                    }

                }

            } else {
                $foliosSigfe = $foliosSigfe->whereHas('getDocumento', function($query) use( $request ){
                                                $query->where('numero_documento', trim( $request->input('filtro_numero_documento') ) );
                                            });
            }

            if ( $request->input('filtro_proveedor') != null ) {

                $foliosSigfe = $foliosSigfe->whereHas('getDocumento', function($query) use( $request ){
                                                $query->WhereIn('id_proveedor', $request->input('filtro_proveedor') );
                                            });
            }

            if ( $request->input('filtro_tipo_documento') != null ) {

                $foliosSigfe = $foliosSigfe->whereHas('getDocumento', function($query) use( $request ){
                                                $query->WhereIn('id_tipo_documento', $request->input('filtro_tipo_documento') );
                                            });

            }

            if ( $request->input('filtro_modalidad_compra') != null ) {

                $foliosSigfe = $foliosSigfe->whereHas('getDocumento', function($query) use( $request ){
                                                $query->WhereIn('id_modalidad_compra', $request->input('filtro_modalidad_compra') );
                                            });

            }

            if ( $request->input('filtro_tipo_adjudicacion') != null ) {

                $foliosSigfe = $foliosSigfe->whereHas('getDocumento', function($query) use( $request ){
                                                $query->WhereIn('id_tipo_adjudicacion', $request->input('filtro_tipo_adjudicacion') );
                                            });

            }

            if ( $request->input('filtro_tipo_informe') != null ) {

                $foliosSigfe = $foliosSigfe->whereHas('getDocumento', function($query) use( $request ){
                                                $query->WhereIn('id_tipo_informe', $request->input('filtro_tipo_informe'));
                                            });

            }

            if ( $request->input('filtro_responsable') != null ) {

                $foliosSigfe = $foliosSigfe->whereHas('getDocumento', function($query) use( $request ){
                                                $query->WhereIn('id_responsable', $request->input('filtro_responsable') );
                                            });

            }

            if ( $request->input('filtro_referente_tecnico') != null ) {

                $foliosSigfe = $foliosSigfe->whereHas('getDocumento', function($query) use( $request ){
                                                $query->WhereIn('id_referente_tecnico', $request->input('filtro_referente_tecnico') );
                                            });

            }

            if ( $request->input('filtro_digitador') != null ) {;

                $foliosSigfe = $foliosSigfe->whereHas('getDocumento', function($query) use( $request ){
                                                $query->WhereIn('id_digitador', $request->input('filtro_digitador') );
                                            });

            }

            if ( $request->input('filtro_item_presupuestario') != null ) {

                $foliosSigfe = $foliosSigfe->whereHas('getDevengos', function($query) use( $request ){
                                                $query->WhereIn('id_item_presupuestario', $request->input('filtro_item_presupuestario') );
                                            });

            }

            if ( $request->input('filtro_id_sigfe') != null ) {
                $foliosSigfe = $foliosSigfe->where('id_sigfe', $request->input('filtro_id_sigfe') );
            }

            $foliosSigfe = $foliosSigfe->get();
            
            $excel->sheet('Documentos', function($sheet) use($foliosSigfe, $request) {
            
            $sheet->row(1, [
                'Estado', 
                // 'Responsable', 
                'Factoring',
                'Chile-Compra',
                'M/Dev',
                'Proveedor',
                'RUT',
                'Tipo Dcto.',
                'N° Dcto.',
                'F/Doc.',
                'M/Doc.',
                'Fecha RD',
                'Mes RD',
                'Item',
                'ID SIGFE',
                'Fecha ID',
                'Devengado',
                'Mecanismo de Adjudicación',
                'Observación',
                'Detalle'
            ]);

            $totalDevengadoExcel = 0;
            $cuentaLineas = 1;
            foreach ( $foliosSigfe as $indexAux => $folioSigfe ) {
                $items = '';
                $totalDevengadoPorDocumento = 0;

                foreach ( $folioSigfe->getDevengos as $index => $devengo) {
                    // comprobar si el item se encuentra dentro del filtro
                    if ( $request->input('filtro_item_presupuestario') != null &&  ! in_array($devengo->id_item_presupuestario, $request->input('filtro_item_presupuestario')) ) {
                        // si el item no se encuentra se salta a la proxima iteración
                        continue;
                    }

                    $cuentaLineas++;
                    $items = $devengo->getItemPresupuestario->codigo();
                    $totalDevengadoPorDocumento = $devengo->monto;
                    // para columna "devengado"
                    $devengado = '';
                    if ( $folioSigfe->getDocumento->id_tipo_documento == 4 || $folioSigfe->getDocumento->id_tipo_documento == 10 ) {
                        $devengado .= '-'.$totalDevengadoPorDocumento;
                        $totalDevengadoExcel = $totalDevengadoExcel - $totalDevengadoPorDocumento;
                    } else {
                        $devengado .= $totalDevengadoPorDocumento;
                        $totalDevengadoExcel = $totalDevengadoExcel + $totalDevengadoPorDocumento;
                    }

                    // para columna factoring
                    $factoring = '';
                    if ( $folioSigfe->getDocumento->getProveedorFactoring ) {
                        $factoring .= $folioSigfe->getDocumento->getProveedorFactoring->nombre.' '.$folioSigfe->getDocumento->getProveedorFactoring->rut;
                    }
                    

                    $sheet->row($cuentaLineas, [
                        $folioSigfe->getDocumento->getEstadoDocumento('excel'),
                        // $folioSigfe->getDocumento->getResponsable->name, 
                        $factoring,
                        $folioSigfe->getDocumento->documento_compra,
                        explode("-",$folioSigfe->fecha_sigfe)[1], // mes de la fecha sigfe
                        $folioSigfe->getDocumento->getProveedor->nombre,
                        $folioSigfe->getDocumento->getProveedor->rut,
                        $folioSigfe->getDocumento->getTipoDocumento->nombre,
                        $folioSigfe->getDocumento->numero_documento,
                        fecha_dmY($folioSigfe->getDocumento->fecha_documento),
                        explode("-",$folioSigfe->getDocumento->fecha_documento)[1], // mes de la fecha del documento
                        fecha_dmY($folioSigfe->getDocumento->fecha_recepcion),
                        explode("-",$folioSigfe->getDocumento->fecha_recepcion)[1], // mes de la fecha de recepcion del documento
                        $items,
                        $folioSigfe->id_sigfe,
                        fecha_dmY($folioSigfe->fecha_sigfe),
                        $devengado,
                        $folioSigfe->getDocumento->getTipoAdjudicacion->nombre,
                        $folioSigfe->getDocumento->observacion,
                        $folioSigfe->detalle_sigfe
                    ]);
                }

            }

            $sheet->row( $cuentaLineas+1 , [
                '', 
                // '', 
                '',
                '',
                '', 
                '',
                '',
                '',
                '',
                '',
                '', 
                '',
                '',
                '',
                '',
                'TOTAL',
                $totalDevengadoExcel,
                '',
                '',
                ''
            ]);

            //$sheet->fromArray($documentos);
        
        });
        
        })->export('xlsx');
    }

    /**
     * Genera excel de los documentos por devengar, antiguos
     */
    public function postDevengarDocumentosExcel(Request $request)
    {

        set_time_limit ( 0 );
        ini_set('max_execution_time', 0);
        
        \Excel::create('Informe Por Devengar '.date('d-m-Y').'', function($excel) use($request) {
        
            $fechaInicio = fecha_Y_m_d($request->input('filtro_fecha_inicio'));
            $fechaTermino = fecha_Y_m_d($request->input('filtro_fecha_termino'));
            
            $documentos = Documento::with(['getTipoInforme','getProveedor','getProveedorFactoring','getTipoDocumento','getModalidadCompra','getDevengos.getItemPresupuestario'])
                                // ->whereNotIn('id_tipo_documento',[3,9])
                                ->where('id_devengador', null);

            if ( $request->input('filtro_numero_documento') == null ) {
                if ( $request->input('filtro_fecha_a_utilizar') == 'Ingreso' ) {
                    $documentos = $documentos->where('fecha_ingreso','>=',$fechaInicio.' 00:00:00');
                } elseif ( $request->input('filtro_fecha_a_utilizar') == 'Recepción' ) {
                    $documentos = $documentos->where('fecha_recepcion','>=',$fechaInicio);
                }
        
                if ( $request->input('filtro_fecha_a_utilizar') == 'Ingreso' ) {
                    $documentos = $documentos->where('fecha_ingreso','<=',$fechaTermino.' 23:59:59');
                } elseif ( $request->input('filtro_fecha_a_utilizar') == 'Recepción' ) {
                    $documentos = $documentos->where('fecha_recepcion','<=',$fechaTermino);
                }
            } else {
                $documentos = $documentos->where('numero_documento', trim($request->input('filtro_numero_documento')) );
            }

            if ( $request->input('filtro_proveedor') != null ) {
                $documentos = $documentos->WhereIn('id_proveedor',$request->input('filtro_proveedor'));
            }

            if ( $request->input('filtro_tipo_documento') != null ) {
                $documentos = $documentos->WhereIn('id_tipo_documento',$request->input('filtro_tipo_documento'));
            }

            if ( $request->input('filtro_modalidad_compra') != null ) {
                $documentos = $documentos->WhereIn('id_modalidad_compra',$request->input('filtro_modalidad_compra'));
            }

            if ( $request->input('filtro_tipo_adjudicacion') != null ) {
                $documentos = $documentos->WhereIn('id_tipo_adjudicacion',$request->input('filtro_tipo_adjudicacion'));
            }

            if ( $request->input('filtro_tipo_informe') != null ) {
                $documentos = $documentos->WhereIn('id_tipo_informe',$request->input('filtro_tipo_informe'));
            }

            if ( $request->input('filtro_responsable') != null ) {
                $documentos = $documentos->WhereIn('id_responsable',$request->input('filtro_responsable'));
            }

            if ( $request->input('filtro_referente_tecnico') != null ) {
                $documentos = $documentos->WhereIn('id_referente_tecnico',$request->input('filtro_referente_tecnico'));
            }

            if ( $request->input('filtro_digitador') != null ) {
                $documentos = $documentos->WhereIn('id_digitador',$request->input('filtro_digitador'));
            }

            if ( $request->input('filtro_item_presupuestario') != null) {
                $documentos = $documentos->whereHas('getDevengos',function ($query) use ($request) {
                    $query->WhereIn('id_item_presupuestario',$request->input('filtro_item_presupuestario'));
                });
            }

            if ( \Entrust::hasRole('usuario-general') ) {
                $documentos = $documentos->where('id_user_para_devengar',\Auth::user()->id)->get();
            } else {
                $documentos = $documentos->get();
            }
        
            $excel->sheet('Documentos', function($sheet) use($documentos, $request) {
            
                $sheet->row(1, [
                    'Estado', 
                    // 'Responsable', 
                    'Usuario Por Devengar',
                    'Factoring',
                    'Chile-Compra',
                    // 'M/Dev',
                    'Proveedor',
                    'RUT',
                    'Tipo Dcto.',
                    'N° Dcto.',
                    'F/Doc.',
                    'M/Doc.',
                    'Fecha RD',
                    'Mes RD',
                    'Item',
                    // 'ID SIGFE',
                    // 'Fecha ID',
                    'Monto',
                    'Mecanismo de Adjudicación',
                    'Observación',
                    // 'Detalle'
                ]);

                $totalPorDevengarExcel = 0;
                $cuentaLineas = 1;
                foreach ( $documentos as $index => $documento ) {
                    $items = '';
                    $totalPorDevengarDocumento = 0;

                    foreach ( $documento->getDevengos as $devengo) {
                        // comprobar si el item se encuentra dentro del filtro
                        if ( $request->input('filtro_item_presupuestario') != null &&  ! in_array($devengo->id_item_presupuestario, $request->input('filtro_item_presupuestario')) ) {
                            // si el item no se encuentra se salta a la proxima iteración
                            continue;
                        }

                        $cuentaLineas++;
                        $items = $devengo->getItemPresupuestario->codigo();
                        $totalPorDevengarDocumento = $devengo->monto;

                        // para columna "monto"
                        $monto = '';
                        if ($documento->id_tipo_documento == 4 || $documento->id_tipo_documento == 10) {
                            $monto .= '-'.$totalPorDevengarDocumento;
                            $totalPorDevengarExcel = $totalPorDevengarExcel - $totalPorDevengarDocumento;
                        } else {
                            $monto .= $totalPorDevengarDocumento;
                            $totalPorDevengarExcel = $totalPorDevengarExcel + $totalPorDevengarDocumento;
                        }

                        // para columna factoring
                        $factoring = '';
                        if ($documento->getProveedorFactoring) {
                            $factoring .= $documento->getProveedorFactoring->nombre.' '.$documento->getProveedorFactoring->rut;
                        }

                        $userPorDevengar = '';
                        if ($documento->getUserParaDevengar) {
                            $userPorDevengar = $documento->getUserParaDevengar->name;
                        }

                        $sheet->row($cuentaLineas, [
                            'DESC.', 
                            // $documento->getResponsable->name,
                            $userPorDevengar,
                            $factoring,
                            $documento->documento_compra,
                            //explode("-",$documento->fecha_sigfe)[1], // mes de la fecha sigfe
                            $documento->getProveedor->nombre,
                            $documento->getProveedor->rut,
                            $documento->getTipoDocumento->nombre,
                            $documento->numero_documento,
                            fecha_dmY($documento->fecha_documento),
                            explode("-",$documento->fecha_documento)[1], // mes de la fecha del documento
                            fecha_dmY($documento->fecha_recepcion),
                            explode("-",$documento->fecha_recepcion)[1], // mes de la fecha de recepcion del documento
                            $items,
                            // $documento->id_sigfe,
                            // fecha_dmY($documento->fecha_sigfe),
                            $monto,
                            $documento->getTipoAdjudicacion->nombre,
                            $documento->observacion,
                            // $documento->detalle
                        ]);
                    }
                    
                }

                $sheet->row( $cuentaLineas+1 , [
                    '', 
                    // '',
                    '',
                    '',
                    '',
                    '', 
                    '',
                    '',
                    '',
                    '',
                    '',
                    '', 
                    '',
                    'TOTAL',
                    $totalPorDevengarExcel,
                    '',
                    '',
                    ''
                ]);

                //$sheet->fromArray($documentos);
        
            });
        
        })->export('xlsx');
    }


    /**
     * Genera excel de los documentos por devengar, nuevos
     */
    public function postDevengarDocumentosExcelDos(Request $request)
    {
        set_time_limit ( 0 );
        ini_set('max_execution_time', 0);

        \Excel::create('Informe Por Devengar '.date('d-m-Y').'', function($excel) use($request) {
        
            $fechaInicio = fecha_Y_m_d($request->input('filtro_fecha_inicio'));
            $fechaTermino = fecha_Y_m_d($request->input('filtro_fecha_termino'));
            
            $documentos = Documento::porDevengar();

            if ( $request->input('filtro_numero_documento') == null ) {
                if ( $request->input('filtro_fecha_a_utilizar') == 'Ingreso' ) {
                    $documentos = $documentos->where('fecha_ingreso','>=',$fechaInicio.' 00:00:00');
                } elseif ( $request->input('filtro_fecha_a_utilizar') == 'Recepción' ) {
                    $documentos = $documentos->where('fecha_recepcion','>=',$fechaInicio);
                }
        
                if ( $request->input('filtro_fecha_a_utilizar') == 'Ingreso' ) {
                    $documentos = $documentos->where('fecha_ingreso','<=',$fechaTermino.' 23:59:59');
                } elseif ( $request->input('filtro_fecha_a_utilizar') == 'Recepción' ) {
                    $documentos = $documentos->where('fecha_recepcion','<=',$fechaTermino);
                }
            } else {
                $documentos = $documentos->where('numero_documento', trim($request->input('filtro_numero_documento')) );
            }

            if ( $request->input('filtro_proveedor') != null ) {
                $documentos = $documentos->WhereIn('id_proveedor',$request->input('filtro_proveedor'));
            }

            if ( $request->input('filtro_tipo_documento') != null ) {
                $documentos = $documentos->WhereIn('id_tipo_documento', $request->input('filtro_tipo_documento'));
            }

            if ( $request->input('filtro_modalidad_compra') != null ) {
                $documentos = $documentos->WhereIn('id_modalidad_compra', $request->input('filtro_modalidad_compra'));
            }

            if ( $request->input('filtro_tipo_adjudicacion') != null ) {
                $documentos = $documentos->WhereIn('id_tipo_adjudicacion', $request->input('filtro_tipo_adjudicacion'));
            }

            if ( $request->input('filtro_tipo_informe') != null ) {
                $documentos = $documentos->WhereIn('id_tipo_informe', $request->input('filtro_tipo_informe'));
            }

            if ( $request->input('filtro_responsable') != null ) {
                $documentos = $documentos->WhereIn('id_responsable', $request->input('filtro_responsable'));
            }

            if ( $request->input('filtro_referente_tecnico') != null ) {
                $documentos = $documentos->WhereIn('id_referente_tecnico', $request->input('filtro_referente_tecnico'));
            }

            if ( $request->input('filtro_digitador') != null ) {
                $documentos = $documentos->WhereIn('id_digitador', $request->input('filtro_digitador'));
            }

            if ( $request->input('filtro_item_presupuestario') != null) {
                $documentos = $documentos->whereHas('getDevengos',function ($query) use ($request) {
                    $query->WhereIn('id_item_presupuestario', $request->input('filtro_item_presupuestario'));
                });
            }

            $documentos = $documentos->get();
        
            $excel->sheet('Documentos', function($sheet) use($documentos, $request) {
            
                $sheet->row(1, [
                    'Estado', 
                    'Responsable', 
                    // 'Usuario Por Devengar',
                    'Factoring',
                    'Chile-Compra',
                    // 'M/Dev',
                    'Proveedor',
                    'RUT',
                    'Informe',
                    'Tipo Dcto.',
                    'N° Dcto.',
                    'N° Dcto. Relacionado',
                    'F/Doc.',
                    'M/Doc.',
                    'Fecha RD',
                    'Mes RD',
                    'Item',
                    // 'ID SIGFE',
                    // 'Fecha ID',
                    'Monto',
                    'Mecanismo de Adjudicación',
                    'Observación',
                    // 'Detalle'
                ]);

                $totalPorDevengarExcel = 0;
                $cuentaLineas = 1;
                foreach ( $documentos as $index => $documento ) {
                    $items = '';
                    $totalPorDevengarDocumento = 0;

                    foreach ( $documento->getDevengos as $devengo) {
                        // comprobar si el item se encuentra dentro del filtro
                        if ( $request->input('filtro_item_presupuestario') != null &&  ! in_array($devengo->id_item_presupuestario, $request->input('filtro_item_presupuestario')) ) {
                            // si el item no se encuentra se salta a la proxima iteración
                            continue;
                        }
                        
                        $cuentaLineas++;
                        $items = $devengo->getItemPresupuestario->codigo();
                        $totalPorDevengarDocumento = $devengo->monto;

                        // para columna "monto"
                        $monto = '';
                        if ($documento->id_tipo_documento == 4 || $documento->id_tipo_documento == 10) {
                            $monto .= '-'.$totalPorDevengarDocumento;
                            $totalPorDevengarExcel = $totalPorDevengarExcel - $totalPorDevengarDocumento;
                        } else {
                            $monto .= $totalPorDevengarDocumento;
                            $totalPorDevengarExcel = $totalPorDevengarExcel + $totalPorDevengarDocumento;
                        }

                        // para columna factoring
                        $factoring = '';
                        if ($documento->getProveedorFactoring) {
                            $factoring .= $documento->getProveedorFactoring->nombre.' '.$documento->getProveedorFactoring->rut;
                        }

                        $userPorDevengar = $documento->getResponsable ? $documento->getResponsable->name : '';

                        $numeroDocumentoRelacionado = '';
                        if ($documento->getDocumentoRelacionado ) {
                            $numeroDocumentoRelacionado = $documento->getDocumentoRelacionado->numero_documento;
                        }
                        $sheet->row($cuentaLineas, [
                            'DESC.', 
                            // $documento->getResponsable->name,
                            $userPorDevengar,
                            $factoring,
                            $documento->documento_compra,
                            //explode("-",$documento->fecha_sigfe)[1], // mes de la fecha sigfe
                            $documento->getProveedor->nombre,
                            $documento->getProveedor->rut,
                            $documento->getTipoInforme->nombre,
                            $documento->getTipoDocumento->nombre,
                            $documento->numero_documento,
                            $numeroDocumentoRelacionado,
                            fecha_dmY($documento->fecha_documento),
                            explode("-",$documento->fecha_documento)[1], // mes de la fecha del documento
                            fecha_dmY($documento->fecha_recepcion),
                            explode("-",$documento->fecha_recepcion)[1], // mes de la fecha de recepcion del documento
                            $items,
                            // $documento->id_sigfe,
                            // fecha_dmY($documento->fecha_sigfe),
                            $monto,
                            $documento->getTipoAdjudicacion->nombre,
                            $documento->observacion,
                            // $documento->detalle
                        ]);
                    }
                    
                }

                $sheet->row( $cuentaLineas+1 , [
                    '', 
                    '',
                    '',
                    '',
                    '',
                    '', 
                    '',
                    '',
                    '',
                    '',
                    '',
                    '',
                    '', 
                    '',
                    'TOTAL',
                    $totalPorDevengarExcel,
                    '',
                    '',
                    ''
                ]);

                //$sheet->fromArray($documentos);
            
            });
        
        })->export('xlsx');
    }

    public function getUpload()
    {
        return view('devengo.index_masivo');
    }

    public function postUpload(Request $request)
    {
        set_time_limit ( 0 );
        ini_set('max_execution_time', 0);

        $archivo = $request->file('archivo');
        $excel = \Excel::selectSheetsByIndex(0)->load($archivo)->get();

        $log = '';
        // $archivoLog = fopen("log-devengo-upload.txt", "w");

        foreach ($excel as $key => $fila) {
            // dd('holo',$fila);

            // se valida que el devengo no se encuentra en la base de datos
            $devengo = Devengo::where('id_documento',$fila->id_factura)
                              ->where('id_item_presupuestario',$fila->id_item_presupuestario)
                              ->first();
            if ( is_object($devengo) ) {
                $log = ($key + 1).'.- YA EXISTE El Devengo : id '.$fila->id.'  .'.PHP_EOL;
                // fwrite($archivoLog, $log);
            } else {
                // se crea el devengo
                $newDevengo = new Devengo();
                $newDevengo->id = $fila->id;
                $newDevengo->id_documento = trim($fila->id_factura);
                $newDevengo->id_item_presupuestario = trim($fila->id_item_presupuestario);
                $newDevengo->monto = $fila->nr_monto;
                $newDevengo->registro = $fila->fc_registro;

                $newDevengo->save();

                $log = ($key + 1).'.- Se registro correctamente El Devengo : id '.$fila->id.' .'.PHP_EOL;
                // fwrite($archivoLog, $log);   
                
            }

        }
        //fclose($archivoLog);
        $datos = array(
            'mensaje' => 'Carga de devengo exitoso.',
            'estado' => 'success',
        );

        return response()->json($datos,200);
    }

    public function getCargaSigfe()
    {
        if (! \Entrust::can(['carga-sigfe'])) {
            return \Redirect::to('home');
        }

        return view('devengo.index_carga_sigfe');
    }

    public function postCargaSigfe(Request $request)
    {
        // \Debugbar::disable();
        set_time_limit ( 0 );
        ini_set('max_execution_time', 0);
        // ini_set('memory_limit','4000M');
        
        $archivo = $request->file('archivo');
        // se selecciona la hoja que se quire utilziar del excel
        $excel = \Excel::selectSheetsByIndex(1)->load($archivo)->get(); 
        

        $numDocEncontrados = 0;
        $numDocNoEncontrados = 0;
        $numSigfeYaAsignado = 0;
        $numSigfeNoActualizado = 0;
        $numSigfeNoAsignados = 0;
        $numSigfeAsignado = 0;
        $numSigfeDiferenciaMontos= 0;
        $datosTablaDiferenciaMontos = array();
        $datosTablaSigfeNoAsignados = array();
        $datosTablaErrorSigfe = array();
        $datosTablaEncontrados = array();
        $datosTablaNoEncontrados = array();
        $datosTablaSigfeNoActualizado = array();
        $datosTablaAsignadoSigfe = array();

        foreach ($excel as $key => $fila) {

            $montoSigfe = 0;

            if ( $fila->haber != null ) {
                try{
                    $montoSigfe = (int) formato_entero( str_replace(['(',')'],'', $fila->haber) );
                } catch ( \ErrorException $e) {
                    dd($e,$montoSigfe,$fila,'errors');
                }
                
            }
            

            if ( $fila->haber != null && $montoSigfe != 0 ) {
                // Variables utilizadas para mostrar en la tabla
                $estadoDoc = '';
                $totalDoc = '$ ';
                $totalDocAct = '$ ';

                // Se trabaja la tabla de factura, se guardan distintos documentos
                $id_tipo_documento = 0;
                switch ( trim($fila->tipo_documento) ) {
                    case 'Rendición de cuentas':
                        $id_tipo_documento = 6; // Rendicion de fondo fijo
                        break;

                    case 'Boleta de Honorarios':
                        $id_tipo_documento = 3; // Boleta de Honorarios Afecta
                        break;

                    case 'Factura Exenta':
                        $id_tipo_documento = 1; // Factura Electronica
                        break;

                    case 'Factura Exenta Electrónica':
                        $id_tipo_documento = 1; // Factura Electronica
                        break;

                    case 'Factura Afecta Electrónica':
                        $id_tipo_documento = 1; // Factura Electronica
                        break;

                    case 'Factura Afecta':
                        $id_tipo_documento = 1; // Factura Electronica
                        break;

                    case 'Nota de Credito':
                        $id_tipo_documento = 4; // Nota de Credito
                        break;

                    case 'Nota de Crédito Electrónica':
                            $id_tipo_documento = 4; // Nota de Credito
                            break;

                    case 'Nota de Debito':
                        $id_tipo_documento = 5; //Nota de Debito
                        break;

                    case 'Nota de Débito Electrónica':
                            $id_tipo_documento = 5; //Nota de Debito
                            break;

                    default:
                        $id_tipo_documento = 0;
                        break;
                }

                // Buscar Documento para pasar los datos del sigfe
                $doc = Documento::with(['getRelacionRecepcionesValidadas.getWsDocumento.getWsItemsPresupuestarios',
                                        'getDevengos', 'getFoliosSigfe', 'getDocumentoRelacionado', 
                                        'getDocumentosRelacionados'
                                    ])
                                // ->where('numero_documento', formato_entero( trim($fila->numero_documento) ))
                                ->whereHas('getProveedor', function ($query) use ($fila) {
                                    $query->where('rut', explode(' ', trim($fila->principal))[0]);
                                })
                                ->where('id_tipo_documento', $id_tipo_documento);
                                // ->where('total_documento', formato_entero( trim($fila->haber)) )
                                // ->first();

                if ( $id_tipo_documento == 4 || $id_tipo_documento == 5 ) {
                    // acá son NC o ND
                    $doc = $doc->where('total_documento', $montoSigfe)
                                ->whereHas('getDocumentoRelacionado', function ($query) use ($fila){
                                    $query->where('numero_documento', formato_entero( trim($fila->numero_documento) ));
                                });

                } else {
                    /**
                     * acá son todos los otros documentos y las cartas certificadas, que son ajustes en sigfe
                     * Los ajustes vienen con el tipo documento 1, factura.
                     * por lo tanto siempre va a encontrar la factura.
                     * se debe realizar valicación para verificar si el folio sigfe es a la factura o a una 
                     * carta certificada
                     */
                     
                    $doc = $doc->where('numero_documento', formato_entero( trim($fila->numero_documento) ));
                }

                $doc = $doc->first();                
                
                if ( is_object($doc) ) {
                    // Se encuentra el documento
                    
                    $numDocEncontrados++;
                    // dd('se encuentra el documento', $fila, formato_entero( $fila->haber ),$doc);

                    /**
                     * Se deben verificar los montos.
                     * 1° Si el documento no tiene algun folio sigfe(para el año actual), se debe verificar por el total_documento, 
                     *    porque la factura se devenga antes de aplicar NC o ND.
                     * 2° Si el documento ya tiene un folio sigfe:
                     *      1° verificar si el folio sigfe que viene es del mismo mes y mayor, para cambiar
                     *      2° se deben verificar las cartas certificadas relacionadas que no tengan folio sigfe.
                     * 3° Si todas las cartas certificadas tienen folio sigfe, se verifica nuevamente con el documento original, pero 
                     *    ahora se verifica con el total_documento_actualizado
                     */
                    $foliosDocAñoActual = FolioSigfe::where('year_sigfe', fecha_Y( trim($fila->fecha) ) )
                                                    ->where('id_documento', $doc->id)
                                                    ->get();

                    // Separar para sub 34 y otros
                    if ( strpos($fila->cuenta_contable, '21534') === false ) {
                        // No es 34
                        
                        if ( $foliosDocAñoActual->count() == 0 ) {
                            /**
                             * Si no tiene folios sigfe para el año, significa que no se ha procesado el primer devengo.
                             * El cual tiene que se por el total del documento
                             */
                            if ( $montoSigfe == $doc->total_documento ) {

                                $this->evaluaFolioSigfe($doc, $fila, $numSigfeYaAsignado, $estadoDoc, $totalDoc, 
                                                        $totalDocAct, $datosTablaErrorSigfe, $numSigfeAsignado, $datosTablaAsignadoSigfe, 
                                                        $numSigfeNoAsignados, $datosTablaSigfeNoAsignados );

                            } else {

                                // Mostrar aviso de no actualización por el monto distinto
                                $numSigfeDiferenciaMontos++;

                                $estadoDoc .= 'Diferencia en montos';
                                $totalDoc .= formatoMiles($doc->total_documento);
                                $totalDocAct .= formatoMiles($doc->total_documento_actualizado);
                                $montoSigfe = formatoMiles($montoSigfe);

                                $docJson = array(
                                    'DT_RowID' => $doc->id,
                                    $doc->getProveedor->rut,
                                    $doc->getProveedor->nombre,
                                    $doc->getTipoDocumento->nombre,
                                    $doc->numero_documento,
                                    $totalDoc,
                                    $totalDocAct,
                                    trim($fila->folio),
                                    trim($fila->fecha),
                                    $fila->haber,
                                );

                                $datosTablaDiferenciaMontos[] = $docJson;
                            }

                        } else {
                            /**
                             * Tiene folio para el año. Por lo tanto el primer devengo debe haber sido realizado.
                             * El folio sigfe que viene puede ser para el documento (factura) o para cartas certificadas(Sifcon),
                             * que son ajustes en sigfe.
                             * La prioridad de busqueda es:
                             * 1° cartas certificadas, sin devengo y con el mismo monto que viene en la fila.
                             * 2° Factura, utilizando el total_documento o el total_documento_actualizado, se debe evaluar si es del mismo mes
                             *             para dejar el último y borrar el anterior. O pueder ser del año y se crea un nuevo id sigfe
                             */

                            // buscar las cartas certificadas
                            $cartaCertificada = Documento::where('id_relacionado', $doc->id)->doesnthave('getFoliosSigfe');

                            if ( strpos($fila->haber,'(') !== false ) {
                                // credito
                                $cartaCertificada = $cartaCertificada->whereIn('id_tipo_documento', [10]);
                            } else {
                                //debito
                                $cartaCertificada = $cartaCertificada->whereIn('id_tipo_documento', [11]);
                            }

                            $cartaCertificada = $cartaCertificada->where('total_documento', $montoSigfe)->first();

                            if ( is_object($cartaCertificada) ) {

                                // pasar el folio sigfe a la carta certificada
                                $this->evaluaFolioSigfe($cartaCertificada, $fila, $numSigfeYaAsignado, $estadoDoc, $totalDoc, 
                                                        $totalDocAct, $datosTablaErrorSigfe, $numSigfeAsignado, $datosTablaAsignadoSigfe, 
                                                        $numSigfeNoAsignados, $datosTablaSigfeNoAsignados );

                            } else {
                                /**
                                 * Se evalua el documento(factura)
                                 * Antes era solo por el total documento actualizado
                                 */
                                // if ( $montoSigfe == $doc->total_documento || $montoSigfe == $doc->total_documento_actualizado  ) {
                                if ( $montoSigfe == $doc->total_documento_actualizado  ) {

                                    /**
                                     * Buscar si el folio sigfe ya se encuentra en el sistema para el documento.
                                     * Si lo encuentra lo actualizad, si no lo evalua
                                     */
                                    
                                    $folioSigfe = FolioSigfe::where('id_sigfe', trim($fila->folio) )
                                                            ->where('year_sigfe', fecha_Y( trim($fila->fecha) ) )
                                                            ->where('id_documento', $doc->id)
                                                            ->first();
                                                            
                                    if ( is_object($folioSigfe) ) {

                                        $this->actualizaFolioSigfe($folioSigfe, $fila, $doc, $estadoDoc, $totalDoc, $totalDocAct, $datosTablaEncontrados);

                                    } else {

                                        // Se debe evaluar con el total documento actualizado                                    
                                        $doc->aux_total_documento = $doc->total_documento; // Se hace para no cambiar demasiado la funcion evaluaFolioSigfe()
                                        $doc->total_documento = $doc->total_documento_actualizado;
                                        $this->evaluaFolioSigfe($doc, $fila, $numSigfeYaAsignado, $estadoDoc, $totalDoc, 
                                                                $totalDocAct, $datosTablaErrorSigfe, $numSigfeAsignado, $datosTablaAsignadoSigfe, 
                                                                $numSigfeNoAsignados, $datosTablaSigfeNoAsignados, true );

                                    }

                                } else {
                                    // Mostrar aviso de no actualización por el monto distinto
                                    $numSigfeDiferenciaMontos++;

                                    $estadoDoc .= 'Diferencia en montos';
                                    $totalDoc .= formatoMiles($doc->total_documento);
                                    $totalDocAct .= formatoMiles($doc->total_documento_actualizado);
                                    $montoSigfe = formatoMiles($montoSigfe);

                                    $docJson = array(
                                        'DT_RowID' => $doc->id,
                                        $doc->getProveedor->rut,
                                        $doc->getProveedor->nombre,
                                        $doc->getTipoDocumento->nombre,
                                        $doc->numero_documento,
                                        $totalDoc,
                                        $totalDocAct,
                                        trim($fila->folio),
                                        trim($fila->fecha),
                                        $fila->haber,
                                    );

                                    $datosTablaDiferenciaMontos[] = $docJson;
                                }

                            }

                        }

                    } else {
                        // Es 34

                        if ( $foliosDocAñoActual->count() == 0 ) {
                            /**
                             * Si no tiene folios sigfe para el año, significa que no se ha procesado el primer devengo.
                             * El cual tiene que se por el total del documento o el total documento actualizado
                             */
                            if ( $montoSigfe == $doc->total_documento ) {

                                $this->evaluaFolioSigfe($doc, $fila, $numSigfeYaAsignado, $estadoDoc, $totalDoc, 
                                                        $totalDocAct, $datosTablaErrorSigfe, $numSigfeAsignado, $datosTablaAsignadoSigfe, 
                                                        $numSigfeNoAsignados, $datosTablaSigfeNoAsignados );

                            } elseif ( $montoSigfe == $doc->total_documento_actualizado ) {

                                // Se debe evaluar con el total documento actualizado                                    
                                $doc->aux_total_documento = $doc->total_documento; // Se hace para no cambiar demasiado la funcion evaluaFolioSigfe()
                                $doc->total_documento = $doc->total_documento_actualizado;
                                $this->evaluaFolioSigfe($doc, $fila, $numSigfeYaAsignado, $estadoDoc, $totalDoc, 
                                                        $totalDocAct, $datosTablaErrorSigfe, $numSigfeAsignado, $datosTablaAsignadoSigfe, 
                                                        $numSigfeNoAsignados, $datosTablaSigfeNoAsignados, true );

                            } else {

                                // Mostrar aviso de no actualización por el monto distinto
                                $numSigfeDiferenciaMontos++;

                                $estadoDoc .= 'Diferencia en montos';
                                $totalDoc .= formatoMiles($doc->total_documento);
                                $totalDocAct .= formatoMiles($doc->total_documento_actualizado);
                                $montoSigfe = formatoMiles($montoSigfe);

                                $docJson = array(
                                    'DT_RowID' => $doc->id,
                                    $doc->getProveedor->rut,
                                    $doc->getProveedor->nombre,
                                    $doc->getTipoDocumento->nombre,
                                    $doc->numero_documento,
                                    $totalDoc,
                                    $totalDocAct,
                                    trim($fila->folio),
                                    trim($fila->fecha),
                                    $fila->haber,
                                );

                                $datosTablaDiferenciaMontos[] = $docJson;
                            }

                        } else {
                            /**
                             * Tiene folio para el año. Por lo tanto el primer devengo debe haber sido realizado.
                             * El folio sigfe que viene puede ser para el documento (factura) o para cartas certificadas(Sifcon),
                             * que son ajustes en sigfe.
                             * La prioridad de busqueda es:
                             * 1° cartas certificadas, sin devengo y con el mismo monto que viene en la fila.
                             * 2° Factura, utilizando el total_documento o el total_documento_actualizado, se debe evaluar si es del mismo mes
                             *             para dejar el último y borrar el anterior. O pueder ser del año y se crea un nuevo id sigfe
                             */

                            // buscar las cartas certificadas
                            $cartaCertificada = Documento::where('id_relacionado', $doc->id)->doesnthave('getFoliosSigfe');

                            if ( strpos($fila->haber,'(') !== false ) {
                                // credito
                                $cartaCertificada = $cartaCertificada->whereIn('id_tipo_documento', [10]);
                            } else {
                                //debito
                                $cartaCertificada = $cartaCertificada->whereIn('id_tipo_documento', [11]);
                            }

                            $cartaCertificada = $cartaCertificada->where('total_documento', $montoSigfe)->first();

                            if ( is_object($cartaCertificada) ) {

                                // pasar el folio sigfe a la carta certificada
                                $this->evaluaFolioSigfe($cartaCertificada, $fila, $numSigfeYaAsignado, $estadoDoc, $totalDoc, 
                                                        $totalDocAct, $datosTablaErrorSigfe, $numSigfeAsignado, $datosTablaAsignadoSigfe, 
                                                        $numSigfeNoAsignados, $datosTablaSigfeNoAsignados );

                            } else {
                                /**
                                 * Se evalua el documento(factura)
                                 * Antes era solo por el total documento actualizado
                                 */
                                if ( $montoSigfe == $doc->total_documento || $montoSigfe == $doc->total_documento_actualizado  ) {

                                    /**
                                     * Buscar si el folio sigfe ya se encuentra en el sistema para el documento.
                                     * Si lo encuentra lo actualizad, si no lo evalua
                                     */
                                    
                                    $folioSigfe = FolioSigfe::where('id_sigfe', trim($fila->folio) )
                                                            ->where('year_sigfe', fecha_Y( trim($fila->fecha) ) )
                                                            ->where('id_documento', $doc->id)
                                                            ->first();
                                                            
                                    if ( is_object($folioSigfe) ) {

                                        $this->actualizaFolioSigfe($folioSigfe, $fila, $doc, $estadoDoc, $totalDoc, $totalDocAct, $datosTablaEncontrados);

                                    } else {

                                        // Se debe evaluar con el total documento actualizado                                    
                                        $doc->aux_total_documento = $doc->total_documento; // Se hace para no cambiar demasiado la funcion evaluaFolioSigfe()
                                        $doc->total_documento = $doc->total_documento_actualizado;
                                        $this->evaluaFolioSigfe($doc, $fila, $numSigfeYaAsignado, $estadoDoc, $totalDoc, 
                                                                $totalDocAct, $datosTablaErrorSigfe, $numSigfeAsignado, $datosTablaAsignadoSigfe, 
                                                                $numSigfeNoAsignados, $datosTablaSigfeNoAsignados, true );

                                    }

                                } else {
                                    // Mostrar aviso de no actualización por el monto distinto
                                    $numSigfeDiferenciaMontos++;

                                    $estadoDoc .= 'Diferencia en montos';
                                    $totalDoc .= formatoMiles($doc->total_documento);
                                    $totalDocAct .= formatoMiles($doc->total_documento_actualizado);
                                    $montoSigfe = formatoMiles($montoSigfe);

                                    $docJson = array(
                                        'DT_RowID' => $doc->id,
                                        $doc->getProveedor->rut,
                                        $doc->getProveedor->nombre,
                                        $doc->getTipoDocumento->nombre,
                                        $doc->numero_documento,
                                        $totalDoc,
                                        $totalDocAct,
                                        trim($fila->folio),
                                        trim($fila->fecha),
                                        $fila->haber,
                                    );

                                    $datosTablaDiferenciaMontos[] = $docJson;
                                }

                            }

                        }
                        
                    }
                    
                } else {

                    // No se encuentra documento con los datos del excel
                    $numDocNoEncontrados++;

                    $estadoDoc .= 'No Encontrado';

                    try{
                        // $montoSigfe = (int) str_replace(['(',')'],'', $fila->haber);
                        $totalDoc .= trim( str_replace(['(',')'],'', $fila->haber) );
                    } catch ( \ErrorException $e) {
                        dd($e,$montoSigfe,$fila,'errors');
                    }

                    // $totalDoc .= formatoMiles( formato_entero( trim($fila->haber) ) );
                    
                    $nombreProveedor = '';
                    $auxProv = explode(' ',trim($fila->principal) );
                    for ( $i = 1; $i < count($auxProv); $i++ ) {
                        $nombreProveedor .= $auxProv[$i].' ';
                    }

                    // Para realizar la carga del documento al sistema
                    $proveedor = Proveedor::where('rut',explode(' ',trim($fila->principal))[0])->first();

                    $idProveedor = 0;
                    if ( is_object($proveedor) ) {
                        $idProveedor = $proveedor->id;
                    }

                    $numeroDocumento = formato_entero( trim($fila->numero_documento) );
                    $totalDocumento = formato_entero( trim($fila->haber));
                    $idSigfe = trim($fila->folio);
                    $fechaSigfe = fecha_Y_m_d(trim($fila->fecha));
                    // $fechaSigfe = trim($fila->fecha);

                    $botonCarga = '<div class="btn-group">';

                    if ( $id_tipo_documento != 0 && $idProveedor != 0 ) {

                        $botonCarga .= '<button type="button" class="btn btn-primary btn-xs" title="Ingresar Documento" ';
                        $botonCarga .= 'onclick="carga('.$idProveedor.','.$id_tipo_documento.','.$numeroDocumento.','.$totalDocumento.','.$idSigfe.',\''.$fechaSigfe.'\','.$key.')" >';
                        $botonCarga .= '<i class="fas fa-file-export fa-lg"></i>&nbsp;<i class="fas fa-file-invoice fa-lg"></i></button>';

                    }

                    $botonCarga .= '</div>';
                    
                    $docJson = array(
                        'DT_RowID' => $key,
                        $botonCarga,
                        $estadoDoc,
                        trim(explode(' ',$fila->principal)[0]),
                        $nombreProveedor,
                        trim($fila->tipo_documento),
                        trim($fila->numero_documento),
                        $fila->haber,
                        trim($fila->folio),
                        trim($fila->fecha)
                    );

                    $datosTablaNoEncontrados[] = $docJson;
                }
                
            }

        }

        $resumen = '<strong>';
        $resumen .= 'Documentos Encontrados : '.formatoMiles($numDocEncontrados).'<br>';
        $resumen .= 'Documentos No Encontrados : '.formatoMiles($numDocNoEncontrados).'<br>';
        $resumen .= "ID SIGFE Asignados : ".formatoMiles($numSigfeAsignado)."<br>";
        $resumen .= "ID SIGFE Asignados a otro Documento, errores solucionados : ".formatoMiles($numSigfeYaAsignado)."<br>";
        $resumen .= "ID SIGFE Asignados a otro Documento (hay diferencia entre el valor total del documento y el monto de SIGFE) : ".formatoMiles($numSigfeNoActualizado)."<br>";
        $resumen .= "ID SIGFE no asignados, hay diferencia entre el valor total del documento y el monto de SIGFE : ".formatoMiles($numSigfeDiferenciaMontos)."<br>";
        $resumen .= "ID SIGFE no encontrados : ".formatoMiles($numSigfeNoAsignados)."<br>";
        $resumen .= '</strong>';
        
        $datos = array(
            'mensaje' => 'Carga SIGFE exitosa.',
            'estado'  => 'success',
            'resumen' => $resumen,
            'datosTablaErrorSigfe'         => $datosTablaErrorSigfe,
            'datosTablaEncontrados'        => $datosTablaEncontrados,
            'datosTablaNoEncontrados'      => $datosTablaNoEncontrados,
            'datosTablaSigfeNoAsignados'   => $datosTablaSigfeNoAsignados,
            'datosTablaSigfeNoActualizado' => $datosTablaSigfeNoActualizado,
            'datosTablaAsignadoSigfe'      => $datosTablaAsignadoSigfe,
            'datosTablaDiferenciaMontos'   => $datosTablaDiferenciaMontos
        );

        return response()->json($datos,200);
    }

    /**
     * Actualiza el folio sigfe con la información que viene del excel cargado
     */
    public function actualizaFolioSigfe(FolioSigfe $folioSigfe, $fila, Documento $doc, &$estadoDoc, &$totalDoc, &$totalDocAct, &$datosTablaEncontrados)
    {

        // Se actualizan los datos
        $folioSigfe->fecha_sigfe = fecha_Y_m_d( trim($fila->fecha) );
        $folioSigfe->dia_sigfe = fecha_D( trim($fila->fecha) );
        $folioSigfe->mes_sigfe = fecha_M( trim($fila->fecha) );

        if ( $doc->id_devengador == null ) {
            $doc->id_devengador = \Auth::user()->id;
        }

        if ( $doc->id_estado == 1 ) {
            $doc->id_estado = 2;
        }

        if ( $doc->fecha_devengado == null ) {
            $doc->fecha_devengado = date('Y-m-d H:i:s');
        }

        // Para detalle de devengo
        $detalleRecepcion = '';
        if ( $doc->getRelacionRecepcionesValidadas->count() > 0 ) {
            foreach ($doc->getRelacionRecepcionesValidadas as $relacion) {

                $len = count($relacion->getWsDocumento->getWsItemsPresupuestarios);
                foreach ($relacion->getWsDocumento->getWsItemsPresupuestarios as $key => $item) {

                    if ( $item->descripcion_articulo != null ) {
                        if ( $key != $len - 1 ) {
                            $detalleRecepcion .= ''.$item->descripcion_articulo.PHP_EOL;
                        } else {
                            $detalleRecepcion .= ''.$item->descripcion_articulo;
                        }
                    }

                }

            }
            
            if ( $detalleRecepcion != '' && strpos($folioSigfe->detalle_sigfe, $detalleRecepcion) === false ) {
                $folioSigfe->detalle_sigfe = $folioSigfe->detalle_sigfe.PHP_EOL.$detalleRecepcion;
            }
        }

        $doc->save();
        $folioSigfe->save();

        $estadoDoc .= 'Actualizado';
        $totalDoc .= formatoMiles($doc->total_documento);
        $totalDocAct .= formatoMiles($doc->total_documento_actualizado);
        $docJson = array(
            'DT_RowID' => $doc->id,
            $estadoDoc,
            $doc->getProveedor->rut,
            $doc->getProveedor->nombre,
            $doc->getTipoDocumento->nombre,
            $doc->numero_documento,
            $totalDoc,
            $totalDocAct,
            $folioSigfe->id_sigfe,
            fecha_dmY($folioSigfe->fecha_sigfe),
            $folioSigfe->detalle_sigfe
        );

        $datosTablaEncontrados[] = $docJson;

    }


    /**
     * Sirve para evaluar el folio sigfe que se quirre asignar al documento.
     * verifica si otro documento lo tiene, si es de otra fecha.
     * Al llamar la funcion se puede escoger entre utilizar el total documento o
     * el total documento actualizado para evaluar.
     */
    public function evaluaFolioSigfe(Documento $doc, $fila, &$numSigfeYaAsignado, &$estadoDoc, &$totalDoc, 
                                     &$totalDocAct, &$datosTablaErrorSigfe, &$numSigfeAsignado, &$datosTablaAsignadoSigfe, 
                                     &$numSigfeNoAsignados, &$datosTablaSigfeNoAsignados, $evaluaTotalDocumentoActualizado = false )
    {

        // Buscar si el folio sigfe ya se encuentra en el sistema,pero para otro documento
        $folioSigfe = FolioSigfe::with(['getDocumento','getDocumento.getDevengos','getDevengos'])
                                ->where('id_sigfe', trim($fila->folio) )
                                ->where('year_sigfe', fecha_Y( trim($fila->fecha) ) )
                                ->where('id_documento', '<>', $doc->id)
                                ->first();
        /**
         * No evaluar para subtitulo 34 (Cuenta Contable 21534), porque muchos documentos pueden tener el mismo id_sigfe
         */

        if ( strpos($fila->cuenta_contable, '21534') === false ) {
            /**
             * No es subtitulo 34
             */
            if ( is_object($folioSigfe) ) {
                /**
                 * Esta el folio sigfe para otro documento.
                 * Se debe cambiar el folio sigfe al documento que pertenece segun sigfe.
                 * Se trabaja el Folio Sigfe solo si concuerdan los montos
                 */

                $montoSigfe = (int) formato_entero( str_replace(['(',')'],'',$fila->haber) );
    
                if ( $montoSigfe == $doc->total_documento ) {
                    // Mostrar alerta de que hay un documento con ese id sigfe para este año
                    $numSigfeYaAsignado++;
    
                    $botones = '<div class="btn-group">';
                    // Ver
                    if ( \Entrust::can('ver-documento') ) {
                        $botones .= '<button class="btn btn-danger btn-xs" title="Ver documento con el ID SIGFE erróneo" onclick="verDocumento('. $folioSigfe->id_documento .');">';
                        $botones .= '<i class="fa fa-eye"></i></button>';
                    }
                    $botones .= '</div>';
    
                    /**
                     * Se trabaja el cambio de folio
                     * Hacer los cambios en los items.
                     * 1° trabajar los items.
                     * 2° trabajar el documento erroneo
                     * 3° trabajar el documento correcto
                     */
    
                    /**
                     * Crear nuevos devengos para el documento, quitar el id documento de los que son del folio sigfe
                     */
                    if ( $folioSigfe->getDevengos->count() > 0 ) {
    
                        foreach ( $folioSigfe->getDevengos as $devengo ) {
    
                            /**
                             * Se quita el documento de los devengo del folio sigfe
                             */
                            $devengo->id_documento = null;
                            $devengo->save();
                            
                            /**
                             * Se evalua si el documento erroneo tiene el item presupuestario o no.
                             * Si no lo tiene se le crea, pero sin folio sigfe, para no dejar el documento sin items presupuestarios
                             */
                            $devengoDoc = Devengo::where('id_documento', $folioSigfe->id_documento)
                                                 ->where('id_item_presupuestario', $devengo->id_item_presupuestario)
                                                 ->first();
    
                            if ( ! is_object($devengoDoc) ) {
    
                                $newDevengo = new Devengo();
                                $newDevengo->id_documento = $folioSigfe->id_documento;
                                $newDevengo->id_item_presupuestario = $devengo->id_item_presupuestario;
                                $newDevengo->monto = $devengo->monto;
                                $newDevengo->save();
    
                            }
                            
                        }
    
                    }
    
                    /**
                     * Se le quita el folio sigfe(devengo) al documento erroneo
                     */
                    $folioSigfe->getDocumento->id_devengador = null;
                    $folioSigfe->getDocumento->id_estado = 1;
                    $folioSigfe->getDocumento->fecha_devengado = null;
                    $folioSigfe->getDocumento->save();
    
                    /**
                     * Trabajar el nuevo folio sigfe para el documento correcto.
                     * Si el documento tiene un folio sigfe asignado para la misma fecha que el que se quiere actualizar,
                     * se le debe eliminar el folio.
                     * El documento siempre queda con el ultimo folio del mes
                     */
    
                    $folioSigfeError = FolioSigfe::with(['getDocumento','getDocumento.getDevengos','getDevengos'])
                                                // ->where('dia_sigfe', '<=', fecha_D( trim($fila->fecha) ) )
                                                 ->where('id_sigfe', '<', trim($fila->folio))
                                                 ->where('mes_sigfe', fecha_M( trim($fila->fecha) ) )
                                                 ->where('year_sigfe', fecha_Y( trim($fila->fecha) ) )
                                                 ->where('id_documento', $doc->id)
                                                 ->first();
    
                    if ( is_object($folioSigfeError) ) {
    
                        /**
                         * Existe un folio.
                         * Se quita el devengo(folio sigfe) de los items del documento, 
                         * que se le actualizara el folio
                         */
    
                        foreach ( $folioSigfeError->getDevengos as $devengo ) {
    
                            $devengo->id_folio_sigfe = null;
                            $devengo->save();
                            if ( $devengo->id_documento == null ) {
                                // Si el devengo solo pertenece al folio sigfe, se elimina
                                $devengo->delete();
                            }
    
                        }
    
                        /**
                         * Se le quita el devengo(folio sigfe) al documento que se le actualizara el folio
                         */
                        $folioSigfeError->getDocumento->id_devengador = null;
                        $folioSigfeError->getDocumento->id_estado = 1;
                        $folioSigfeError->getDocumento->fecha_devengado = null;
                        $folioSigfeError->getDocumento->save();
    
                        $folioSigfeError->delete();
    
                    }
    
                    /**
                     * Se une el folio correcto al documento correcto
                     */                            
                    $folioSigfe->id_documento = $doc->id;
                    $folioSigfe->save();
    
                    $doc->id_devengador = \Auth::user()->id;
                    $doc->id_estado = 2;
                    $doc->fecha_devengado = date('Y-m-d H:i:s');
                    if ( $doc->aux_total_documento ) {
                        unset( $doc->aux_total_documento );
                    }
                    $doc->save();
                    
    
                    $estadoDoc .= 'ID SIGFE asignado';
    
                    if ( $evaluaTotalDocumentoActualizado == false ) {
                        $totalDoc .= formatoMiles($doc->total_documento);
                    } else {
                        $totalDoc .= formatoMiles($doc->aux_total_documento);
                    }
    
                    $totalDocAct .= formatoMiles($doc->total_documento_actualizado);
                    $docJson = array(
                        'DT_RowID' => $doc->id,
                        $estadoDoc,
                        $doc->getProveedor->rut,
                        $doc->getProveedor->nombre,
                        $doc->getTipoDocumento->nombre,
                        $doc->numero_documento,
                        $totalDoc,
                        $totalDocAct,
                        trim($fila->folio),
                        trim($fila->fecha),
                        $botones
                    );
    
                    $datosTablaErrorSigfe[] = $docJson;
                } else {
                    // Mostrar aviso de no actualización por el monto distinto
                    // $numSigfeNoActualizado++;
    
                    // $botones = '<div class="btn-group">';
                    // // Ver
                    // if ( \Entrust::can('ver-documento') ) {
                    //     $botones .= '<button class="btn btn-danger btn-xs" title="Ver documento" onclick="verDocumento('. $folioSigfe->id_documento .');">';
                    //     $botones .= '<i class="fa fa-eye"></i></button>';
                    // }
                    // $botones .= '</div>';
    
                    // $estadoDoc .= 'Diferencia en montos';
                    // if ( $evaluaTotalDocumentoActualizado == false ) {
                    //     $totalDoc .= formatoMiles($doc->total_documento);
                    // } else {
                    //     $totalDoc .= formatoMiles($doc->aux_total_documento);
                    // }
                    // $totalDocAct .= formatoMiles($doc->total_documento_actualizado);
                    // $montoSigfe = formatoMiles($montoSigfe);
    
                    // $docJson = array(
                    //     'DT_RowID' => $doc->id,
                    //     $estadoDoc,
                    //     $doc->getProveedor->rut,
                    //     $doc->getProveedor->nombre,
                    //     $doc->getTipoDocumento->nombre,
                    //     $doc->numero_documento,
                    //     $totalDoc,
                    //     $totalDocAct,
                    //     trim($fila->folio),
                    //     trim($fila->fecha),
                    //     $montoSigfe,
                    //     $botones
                    // );
    
                    // $datosTablaSigfeNoActualizado[] = $docJson;
                    /**
                     * Esto es para que funcione
                     */
                    $numSigfeNoAsignados++;
    
                        $docJson = array(
                            $fila->principal,
                            $fila->tipo_documento,
                            $fila->numero_documento,
                            $fila->haber,
                            trim($fila->folio),
                            trim($fila->fecha)
                        );
    
                    $datosTablaSigfeNoAsignados[] = $docJson;

                }
    
                
    
            } else {
    
                /**
                 * No esta el folio sigfe utilizado en otro documento.
                 * Crear el folio sigfe si el monto es el mismo al monto total del documento
                 * y si no tiene un folio sigfe mayor en el mes
                 */
                $folioSigfe = FolioSigfe::with(['getDocumento','getDocumento.getDevengos','getDevengos'])
                                        ->where('mes_sigfe', fecha_M( trim($fila->fecha) ) )
                                        ->where('year_sigfe', fecha_Y( trim($fila->fecha) ) )
                                        ->where('id_documento', $doc->id)
                                        ->first();
    
                if ( is_object($folioSigfe) ) {
                    if ( $folioSigfe->id_sigfe < trim($fila->folio) ) {
                        // Crear nuevo sigfe y eliminar el anterior
    
                        $montoSigfe = (int) formato_entero( str_replace(['(',')'],'',$fila->haber) );
                        if ( $montoSigfe == $doc->total_documento ) {
    
                            $numSigfeAsignado++;
    
                            $botones = '<div class="btn-group">';
                            // Ver
                            if ( \Entrust::can('ver-documento') ) {
                                $botones .= '<button class="btn btn-success btn-xs" title="Documento con el ID SIGFE asignado" onclick="verDocumento('. $doc->id .');">';
                                $botones .= '<i class="fa fa-eye"></i></button>';
                            }
                            $botones .= '</div>';
    
                            $folioSigfeNew = new FolioSigfe();
                            $folioSigfeNew->id_documento = $doc->id;
                            $folioSigfeNew->id_sigfe = trim( $fila->folio );
                            $folioSigfeNew->fecha_sigfe = fecha_Y_m_d( trim($fila->fecha) );
                            $folioSigfeNew->dia_sigfe = fecha_D( trim($fila->fecha) );
                            $folioSigfeNew->mes_sigfe = fecha_M( trim($fila->fecha) );
                            $folioSigfeNew->year_sigfe = fecha_Y( trim($fila->fecha) );
                            $folioSigfeNew->detalle_sigfe = '-';
                            $folioSigfeNew->save();
    
                            foreach ( $folioSigfe->getDevengos as $devengo ) {
                                $devengo->id_folio_sigfe = $folioSigfeNew->id;
                                $devengo->save();
                            }
    
                            $folioSigfe->delete();
                            
    
                            $estadoDoc .= 'ID SIGFE asignado';
                            if ( $evaluaTotalDocumentoActualizado == false ) {
                                $totalDoc .= formatoMiles($doc->total_documento);
                            } else {
                                $totalDoc .= formatoMiles($doc->aux_total_documento);
                            }
                            $totalDocAct .= formatoMiles($doc->total_documento_actualizado);
                            $docJson = array(
                                'DT_RowID' => $doc->id,
                                $estadoDoc,
                                $doc->getProveedor->rut,
                                $doc->getProveedor->nombre,
                                $doc->getTipoDocumento->nombre,
                                $doc->numero_documento,
                                $totalDoc,
                                $totalDocAct,
                                trim($fila->folio),
                                trim($fila->fecha),
                                $botones
                            );
    
    
                            $datosTablaAsignadoSigfe[] = $docJson;
                        } else {
                            $numSigfeNoAsignados++;
    
                            $docJson = array(
                                $fila->principal,
                                $fila->tipo_documento,
                                $fila->numero_documento,
                                $fila->haber,
                                trim($fila->folio),
                                trim($fila->fecha)
                            );
    
                            $datosTablaSigfeNoAsignados[] = $docJson;
                        }
                    } else {
    
                        $numSigfeNoAsignados++;
    
                        $docJson = array(
                            $fila->principal,
                            $fila->tipo_documento,
                            $fila->numero_documento,
                            $fila->haber,
                            trim($fila->folio),
                            trim($fila->fecha)
                        );
    
                        $datosTablaSigfeNoAsignados[] = $docJson;
    
                    }
    
                } else {

                    // Crear el folio sigfe
                    $montoSigfe = (int) formato_entero( str_replace(['(',')'],'',$fila->haber) );

                    if ( $montoSigfe == $doc->total_documento  ) {
    
                        $numSigfeAsignado++;
    
                        $botones = '<div class="btn-group">';
                        // Ver
                        if ( \Entrust::can('ver-documento') ) {
                            $botones .= '<button class="btn btn-success btn-xs" title="Documento con el ID SIGFE asignado" onclick="verDocumento('. $doc->id .');">';
                            $botones .= '<i class="fa fa-eye"></i></button>';
                        }
                        $botones .= '</div>';
    
                        $folioSigfe = new FolioSigfe();
                        $folioSigfe->id_documento = $doc->id;
                        $folioSigfe->id_sigfe = trim( $fila->folio );
                        $folioSigfe->fecha_sigfe = fecha_Y_m_d( trim($fila->fecha) );
                        $folioSigfe->dia_sigfe = fecha_D( trim($fila->fecha) );
                        $folioSigfe->mes_sigfe = fecha_M( trim($fila->fecha) );
                        $folioSigfe->year_sigfe = fecha_Y( trim($fila->fecha) );
                        $folioSigfe->detalle_sigfe = '-';
                        $folioSigfe->save();

                        /**
                         * Acá se esta duplicando el item, en caso que ya este devengado el doc
                         */
                        foreach ( $doc->getDevengos as $devengo ) {

                            if ( $doc->getFoliosSigfe->count() > 0 ) {
                                $newDevengo = new Devengo();
                                $newDevengo->id_folio_sigfe = $folioSigfe->id;
                                $newDevengo->id_item_presupuestario = $devengo->id_item_presupuestario;
                                $newDevengo->monto = $devengo->monto;
                                $newDevengo->save();
                                if ( $newDevengo->monto == $doc->total_documento ) {
                                    break;
                                }
                            } else {
                                $devengo->id_folio_sigfe = $folioSigfe->id;
                                $devengo->save();
                            }
                            
                        }
    
                        $doc->id_devengador = \Auth::user()->id;
                        $doc->id_estado = 2;
                        $doc->fecha_devengado = date('Y-m-d H:i:s');
                        if ( $doc->aux_total_documento ) {
                            unset( $doc->aux_total_documento );
                        }
                        $doc->save();
    
                        $estadoDoc .= 'ID SIGFE asignado';
                        if ( strpos($fila->cuenta_contable, '21534') !== false ) {
                            $estadoDoc .= '<br>Traspaso deuda realizado';
                        }
                        if ( $evaluaTotalDocumentoActualizado == false ) {
                            $totalDoc .= formatoMiles($doc->total_documento);
                        } else {
                            $totalDoc .= formatoMiles($doc->aux_total_documento);
                        }
                        $totalDocAct .= formatoMiles($doc->total_documento_actualizado);
                        $docJson = array(
                            'DT_RowID' => $doc->id,
                            $estadoDoc,
                            $doc->getProveedor->rut,
                            $doc->getProveedor->nombre,
                            $doc->getTipoDocumento->nombre,
                            $doc->numero_documento,
                            $totalDoc,
                            $totalDocAct,
                            trim($fila->folio),
                            trim($fila->fecha),
                            $botones
                        );
    
    
                        $datosTablaAsignadoSigfe[] = $docJson;
                    } else {
                        $numSigfeNoAsignados++;
    
                        $docJson = array(
                            $fila->principal,
                            $fila->tipo_documento,
                            $fila->numero_documento,
                            $fila->haber,
                            trim($fila->folio),
                            trim($fila->fecha)
                        );
    
                        $datosTablaSigfeNoAsignados[] = $docJson;
                    }
                }
    
            }
        } else {
            /**
             * es subtitulo 34
             */

            /**
             * Crear el folio sigfe si el monto es el mismo al monto total del documento
             * y si no tiene un folio sigfe mayor en el mes
             */
            $folioSigfe = FolioSigfe::with(['getDocumento','getDocumento.getDevengos','getDevengos'])
                                    ->where('mes_sigfe', fecha_M( trim($fila->fecha) ) )
                                    ->where('year_sigfe', fecha_Y( trim($fila->fecha) ) )
                                    ->where('id_documento', $doc->id)
                                    ->first();

            if ( is_object($folioSigfe) ) {
                if ( $folioSigfe->id_sigfe < trim($fila->folio) ) {
                    // Crear nuevo sigfe y eliminar el anterior

                    $montoSigfe = (int) formato_entero( str_replace(['(',')'],'',$fila->haber) );
                    if ( $montoSigfe == $doc->total_documento ) {

                        $numSigfeAsignado++;

                        $botones = '<div class="btn-group">';
                        // Ver
                        if ( \Entrust::can('ver-documento') ) {
                            $botones .= '<button class="btn btn-success btn-xs" title="Documento con el ID SIGFE asignado" onclick="verDocumento('. $doc->id .');">';
                            $botones .= '<i class="fa fa-eye"></i></button>';
                        }
                        $botones .= '</div>';

                        $folioSigfeNew = new FolioSigfe();
                        $folioSigfeNew->id_documento = $doc->id;
                        $folioSigfeNew->id_sigfe = trim( $fila->folio );
                        $folioSigfeNew->fecha_sigfe = fecha_Y_m_d( trim($fila->fecha) );
                        $folioSigfeNew->dia_sigfe = fecha_D( trim($fila->fecha) );
                        $folioSigfeNew->mes_sigfe = fecha_M( trim($fila->fecha) );
                        $folioSigfeNew->year_sigfe = fecha_Y( trim($fila->fecha) );
                        $folioSigfeNew->detalle_sigfe = trim($fila->titulo);
                        $folioSigfeNew->save();

                        /**
                         * Acá no se crean item en el subtitulo 34, porque se esta cambiando el id sigfe.
                         * El primero ya debe tener item en el subtitulo 34
                         */
                        foreach ( $folioSigfe->getDevengos as $devengo ) {
                            $devengo->id_folio_sigfe = $folioSigfeNew->id;
                            $devengo->save();
                        }

                        $folioSigfe->delete();
                        

                        $estadoDoc .= 'ID SIGFE asignado';
                        if ( $evaluaTotalDocumentoActualizado == false ) {
                            $totalDoc .= formatoMiles($doc->total_documento);
                        } else {
                            $totalDoc .= formatoMiles($doc->aux_total_documento);
                        }
                        $totalDocAct .= formatoMiles($doc->total_documento_actualizado);
                        $docJson = array(
                            'DT_RowID' => $doc->id,
                            $estadoDoc,
                            $doc->getProveedor->rut,
                            $doc->getProveedor->nombre,
                            $doc->getTipoDocumento->nombre,
                            $doc->numero_documento,
                            $totalDoc,
                            $totalDocAct,
                            trim($fila->folio),
                            trim($fila->fecha),
                            $botones
                        );


                        $datosTablaAsignadoSigfe[] = $docJson;
                    } else {
                        $numSigfeNoAsignados++;

                        $docJson = array(
                            $fila->principal,
                            $fila->tipo_documento,
                            $fila->numero_documento,
                            $fila->haber,
                            trim($fila->folio),
                            trim($fila->fecha)
                        );

                        $datosTablaSigfeNoAsignados[] = $docJson;
                    }
                } else {

                    $numSigfeNoAsignados++;

                    $docJson = array(
                        $fila->principal,
                        $fila->tipo_documento,
                        $fila->numero_documento,
                        $fila->haber,
                        trim($fila->folio),
                        trim($fila->fecha)
                    );

                    $datosTablaSigfeNoAsignados[] = $docJson;

                }

            } else {

                // Crear el folio
                $montoSigfe = (int) formato_entero( str_replace(['(',')'],'',$fila->haber) );
                if ( $montoSigfe == $doc->total_documento ) {

                    $numSigfeAsignado++;

                    $botones = '<div class="btn-group">';
                    // Ver
                    if ( \Entrust::can('ver-documento') ) {
                        $botones .= '<button class="btn btn-success btn-xs" title="Documento con el ID SIGFE asignado" onclick="verDocumento('. $doc->id .');">';
                        $botones .= '<i class="fa fa-eye"></i></button>';
                    }
                    $botones .= '</div>';

                    $folioSigfe = new FolioSigfe();
                    $folioSigfe->id_documento = $doc->id;
                    $folioSigfe->id_sigfe = trim( $fila->folio );
                    $folioSigfe->fecha_sigfe = fecha_Y_m_d( trim($fila->fecha) );
                    $folioSigfe->dia_sigfe = fecha_D( trim($fila->fecha) );
                    $folioSigfe->mes_sigfe = fecha_M( trim($fila->fecha) );
                    $folioSigfe->year_sigfe = fecha_Y( trim($fila->fecha) );
                    $folioSigfe->detalle_sigfe = trim($fila->titulo);
                    $folioSigfe->save();

                    /**
                     * Para subtitulo 34 (Cuenta Contable 21534), crear devengos nuevos con los item 3407
                     * Por se el primer id sigfe que se crea para el documento.
                     */
                    foreach ( $doc->getDevengos as $devengo ) {

                        $item34 = ItemPresupuestario::where('titulo', 34)->first();

                        $newDevengo = new Devengo();
                        $newDevengo->id_documento = $folioSigfe->id_documento;
                        $newDevengo->id_folio_sigfe = $folioSigfe->id;
                        $newDevengo->id_item_presupuestario = $item34->id;
                        $newDevengo->monto = $devengo->monto;
                        $newDevengo->save();

                    }

                    /**
                     * Se devenga el documento,
                     * si es subtitulo 34 (Cuenta Contable 21534) el documento ya esta devengado
                     */
                    // if ( strpos($fila->cuenta_contable, '21534') === false ) {

                    //     $doc->id_devengador = \Auth::user()->id;
                    //     $doc->id_estado = 2;
                    //     $doc->fecha_devengado = date('Y-m-d H:i:s');
                    //     if ( $doc->aux_total_documento ) {
                    //         unset( $doc->aux_total_documento );
                    //     }
                    //     $doc->save();

                    // }
                         
                    $estadoDoc .= 'ID SIGFE asignado';
                    if ( strpos($fila->cuenta_contable, '21534') !== false ) {
                        $estadoDoc .= '<br>Traspaso deuda realizado';
                    }
                    if ( $evaluaTotalDocumentoActualizado == false ) {
                        $totalDoc .= formatoMiles($doc->total_documento);
                    } else {
                        $totalDoc .= formatoMiles($doc->aux_total_documento);
                    }
                    $totalDocAct .= formatoMiles($doc->total_documento_actualizado);
                    $docJson = array(
                        'DT_RowID' => $doc->id,
                        $estadoDoc,
                        $doc->getProveedor->rut,
                        $doc->getProveedor->nombre,
                        $doc->getTipoDocumento->nombre,
                        $doc->numero_documento,
                        $totalDoc,
                        $totalDocAct,
                        trim($fila->folio),
                        trim($fila->fecha),
                        $botones
                    );


                    $datosTablaAsignadoSigfe[] = $docJson;
                } else {

                    $numSigfeNoAsignados++;

                    $docJson = array(
                        $fila->principal,
                        $fila->tipo_documento,
                        $fila->numero_documento,
                        $fila->haber,
                        trim($fila->folio),
                        trim($fila->fecha)
                    );

                    $datosTablaSigfeNoAsignados[] = $docJson;

                }

            }
            
        }
        
    }

    public function getModalCargaSigfe($idProveedor, $idTipoDoc, $numeroDoc, $totalDoc, $idSigfe, $fechaSigfe, $keyTr)
    {
        $proveedor = Proveedor::findOrFail($idProveedor);
        $tipoDoc = TipoDocumento::findOrFail($idTipoDoc);
        
        $documentosProveedor = null;
        $docsRechazadosAceptaProv = null;
        if ( $tipoDoc->id == 1 ) {
            // Solo facturas con NC por el 100%
            // Que no tengan Facturas relacionadas, si la tienen es porque ya se refacturo
            $documentosProveedor = Documento::with(['getDevengos.getItemPresupuestario','getTipoDocumento','getDocumentosRelacionados'])
                                            ->where('id_proveedor', $proveedor->id)
                                            ->where('id_tipo_documento', 1)
                                            ->whereHas('getDocumentosRelacionados', function($query) {
                                                $query->where('id_tipo_documento', 4); // NC
                                            })
                                            ->whereDoesntHave('getDocumentosRelacionados', function ($query) {
                                                $query->where('id_tipo_documento', 1); // FC
                                            })
                                            ->where('total_documento_actualizado', 0) // significa que la factura esta anulada
                                            ->where('numero_documento', '<>', $numeroDoc)
                                            ->get();

            // Solo facturas con NC por el 100% y que no tengan refactura (getDocumentoFactura)
            $docsRechazadosAceptaProv = ArchivoAcepta::with(['getProveedor','getTipoDocumento'])
                                                     ->where('cargado', 0)
                                                     ->where('id_user_rechazo','<>', null)
                                                     ->where('id_proveedor', $proveedor->id)
                                                     ->where('id_tipo_documento', 1)
                                                     ->whereHas('getAceptaMotivoRechazo', function ($query){
                                                         $query->where('id_motivo_rechazo', 5); // Anula Documento
                                                     })
                                                     ->where('observacion_rechazo', 'LIKE', '%Tiene una Nota de Crédito por el total del monto%')
                                                     ->doesntHave('getDocumentoFactura')
                                                     ->get();

        } elseif ( $tipoDoc->id == 4 || $tipoDoc->is == 5 || $tipoDoc->id == 10 || $tipoDoc->id == 11 ) {

            $documentosProveedor = Documento::with(['getDevengos.getItemPresupuestario','getTipoDocumento'])
                                            ->where('id_proveedor', $proveedor->id)
                                            ->whereIn('id_tipo_documento', [1,2,8])
                                            ->where('numero_documento', '<>', $numeroDoc)
                                            ->get();

        } elseif ($tipoDoc->id == 8 ) {

            $documentosProveedor = Documento::with(['getDevengos.getItemPresupuestario','getTipoDocumento'])
                                            ->where('id_proveedor', $proveedor->id)
                                            ->where('id_tipo_documento', 4)
                                            ->where('numero_documento', '<>', $numeroDoc)
                                            ->get();
        }

        /** 
        * Para los documentos disponibles de recepcion
        * En este caso se busca el documento disponible de recepcion 
        * que concuerde con el documento que viene desde sigfe
        * Si no encuentra recepcion que concuerde, se muestran todas en la modal
        */
        $opciones = '';
        if ( ! ($tipoDoc->id == 4 || $tipoDoc->id == 5 || $tipoDoc->id == 10 || $tipoDoc->id == 11) ) {

            $documentoBodega = WsDocumento::with(['getWsOrdenCompra.getWsContrato'])
                                            ->whereHas('getWsOrdenCompra.getWsContrato',function ($query) use ($proveedor) {
                                                $query->whereIn('rut_proveedor', $proveedor->getTodosLosRut());
                                            })
                                            ->where('id_relacion', null)
                                            ->where('tipo_documento', 'LIKE', $tipoDoc->nombre)
                                            ->where('documento', $numeroDoc)
                                            ->first();
        
            if ( is_object($documentoBodega) ) {

                $opciones .= PHP_EOL.'<option id="docBodega_'.$documentoBodega->id.'" ';
                $opciones .= 'value="'.$documentoBodega->id.'" ';
                $opciones .= 'data-datosrecepcion="'.$documentoBodega->getDatosDocumentoAndOrdenCompra().'" ';
                $opciones .= '>'.$tipoDoc->sigla.' N° '.$documentoBodega->documento.' '; 
                $opciones .= '- '.fecha_dmY($documentoBodega->fecha_carga).' - $'.formatoMiles($documentoBodega->documento_total).'';
                $opciones .= ' | Prov. '.$documentoBodega->getWsOrdenCompra->getWsContrato->rut_proveedor;
                $opciones .= '</option>'.PHP_EOL;

            } else {

                $docBodegaDisponibles = WsDocumento::with(['getWsOrdenCompra.getWsContrato'])
                                            ->whereHas('getWsOrdenCompra.getWsContrato',function ($query) use ($proveedor) {
                                                $query->whereIn('rut_proveedor', $proveedor->getTodosLosRut());
                                            })
                                            ->where('id_relacion', null)
                                            ->where('tipo_documento', 'LIKE', $tipoDoc->nombre)
                                            ->get();
        
                if ( $docBodegaDisponibles->count() > 0 ) {
                    foreach ($docBodegaDisponibles as $key => $documentoBodega) {

                        $opciones .= PHP_EOL.'<option id="docBodega_'.$documentoBodega->id.'" ';
                        $opciones .= 'value="'.$documentoBodega->id.'" ';
                        $opciones .= 'data-datosrecepcion="'.$documentoBodega->getDatosDocumentoAndOrdenCompra().'" ';
                        $opciones .= '>'.$tipoDoc->sigla.' N° '.$documentoBodega->documento.' '; 
                        $opciones .= '- '.fecha_dmY($documentoBodega->fecha_carga).' - $'.formatoMiles($documentoBodega->documento_total).'';
                        $opciones .= ' | Prov. '.$documentoBodega->getWsOrdenCompra->getWsContrato->rut_proveedor;
                        $opciones .= '</option>'.PHP_EOL;

                    }
                } 
            }

        }

        /**
         * Para las recepcion que es del tipo Guia de Despacho Electronica
         */
        $opcionesGuias = '';
        if ( ! ($tipoDoc->id == 4 || $tipoDoc->id == 5 || $tipoDoc->id == 10 || $tipoDoc->id == 11) ) {

            $documentosBodegaDisponibles = WsDocumento::with(['getWsOrdenCompra.getWsContrato'])
                                            ->whereHas('getWsOrdenCompra.getWsContrato',function ($query) use ($proveedor) {
                                                $query->whereIn('rut_proveedor', $proveedor->getTodosLosRut());
                                            })
                                            ->where('id_relacion', null)
                                            ->where('tipo_documento', 'LIKE', '%Guia de Despacho%')
                                            ->get();

            if (count($documentosBodegaDisponibles) > 0 ) {
                foreach ($documentosBodegaDisponibles as $key => $documentoBodega) {
                    $opcionesGuias .= PHP_EOL.'<option id="guiaDespacho_'.$documentoBodega->id.'" ';
                    $opcionesGuias .= 'value="'.$documentoBodega->id.'" data-numdocumento="'.$documentoBodega->documento.'" ';
                    $opcionesGuias .= 'data-valordocumento="'.$documentoBodega->documento_total.'" ';
                    $opcionesGuias .= 'data-numoc="'.$documentoBodega->numero_orden_compra_mercado_publico.'" ';
                    $opcionesGuias .= '>GD N° '.$documentoBodega->documento.' - '.fecha_dmY($documentoBodega->fecha_carga).' ';
                    $opcionesGuias .= '- $'.formatoMiles($documentoBodega->documento_total).'';
                    $opcionesGuias .= ' | Prov. '.$documentoBodega->getWsOrdenCompra->getWsContrato->rut_proveedor;
                    $opcionesGuias .= '</option>'.PHP_EOL;
                }
            }
        }
        
        
        $tiposDocumento = TipoDocumento::whereNotIn('id',[2,8])->get();
        $modalidadesCompra = ModalidadCompra::all();
        $tiposAdjudicacion = TipoAdjudicacion::all();
        $tiposInforme = TipoInforme::all();
        $usuarios = User::where('responsable',1)->get();
        $referentesTecnicos = ReferenteTecnico::all();
        $tiposArchivo = TipoArchivo::all();

        /**
         * Obtener opciones de los contratos del proveedor
         */
        $contratos = Contrato::vigenteConSaldoParaProveedor($proveedor->id)->get();

        $optionsContrato = '';
        if ( $contratos->count() > 0 ) {

            foreach ( $contratos as $contrato ) {
                $optionsContrato .= $contrato->getOpcionesParaSelect();
            }

        }

        /**
         * Obtener opciones de las ordenes de compra del proveedor
         */
        $ordenes = OrdenCompra::where('id_proveedor', $proveedor->id)
                              ->where('saldo_oc', '>', 0)
                              ->get();

        $optionsOc = '';
        if ( $ordenes->count() > 0 ) {

            foreach ( $ordenes as $oc ) {
                $optionsOc .= $oc->getOpcionesParaSelect();
            }

        }

        return view('devengo.modal_carga_sigfe')->with('proveedor', $proveedor)
                                                ->with('tipoDoc', $tipoDoc)
                                                ->with('numeroDoc', $numeroDoc)
                                                ->with('totalDoc', $totalDoc)
                                                ->with('idSigfe', $idSigfe)
                                                ->with('fechaSigfe', $fechaSigfe)
                                                ->with('keyTr', $keyTr)
                                                ->with('opciones',$opciones)
                                                ->with('opcionesGuias', $opcionesGuias)
                                                ->with('docsRechazadosAceptaProv', $docsRechazadosAceptaProv)
                                                
                                                ->with('documentosProveedor', $documentosProveedor)
                                                ->with('tiposDocumento', $tiposDocumento)
                                                ->with('modalidadesCompra', $modalidadesCompra)
                                                ->with('tiposAdjudicacion', $tiposAdjudicacion)
                                                ->with('tiposInforme', $tiposInforme)
                                                ->with('usuarios', $usuarios)
                                                ->with('referentesTecnicos', $referentesTecnicos)
                                                ->with('tiposArchivo', $tiposArchivo)
                                                ->with('optionsContrato', $optionsContrato)
                                                ->with('optionsOc', $optionsOc);

    }

    public function postIngresarDocumentoDesdeSigfe(Request $request)
    {

        $messages = [
            'required'           => 'Debe ingresar el :attribute',
            'numeric'            => 'El :attribute debe solo contener números',
            'max'                => 'El :attribute no debe exeder los :max caracteres',
            'min'                => 'El :attribute debe tener minimo :min caracteres',
            'date_format'        => 'La :attribute no tiene el formato correcto: día/mes/año',
            'reemplaza.required' => 'Debe seleccionar una factura'
        ];


        if ( $request->input('tipo_documento') == 4 || $request->input('tipo_documento') == 5 || $request->input('tipo_documento') == 10 || $request->input('tipo_documento') == 11 || $request->input('tipo_documento') == 8 ) {
            //validador de los input del formulario
            $validator = \Validator::make($request->all(), [
                'nombre_proveedor'      => 'required|numeric',
                'tipo_documento'        => 'required|numeric',
                'numero_documento'      => 'required',
                'modalidad_compra'      => 'required|numeric',
                'tipo_adjudicacion'     => 'required|numeric',
                'numero_orden_compra'   => 'required',
                'fecha_documento'       => 'required|date_format:"d/m/Y"',
                'tipo_informe'          => 'required|numeric',
                'fecha_recepcion'       => 'required|date_format:"d/m/Y"',
                'valor_total_documento' => 'required',
                'reemplaza'             => 'required',
                'periodo'               => 'required|date_format:"m/Y"',
                'id_sigfe'              => 'required|numeric',
                'fecha_id'              => 'required',
                'detalle_documento'     => 'required',
            ], $messages);
        } else {
            //validador de los input del formulario
            $validator = \Validator::make($request->all(), [
                'nombre_proveedor'       => 'required|numeric',
                'tipo_documento'        => 'required|numeric',
                'numero_documento'      => 'required',
                'modalidad_compra'      => 'required|numeric',
                'tipo_adjudicacion'     => 'required|numeric',
                'numero_orden_compra'   => 'required',
                'fecha_documento'       => 'required|date_format:"d/m/Y"',
                'tipo_informe'          => 'required|numeric',
                'fecha_recepcion'       => 'required|date_format:"d/m/Y"',
                'valor_total_documento' => 'required',
                'periodo'               => 'required|date_format:"m/Y"',
                'id_sigfe'              => 'required|numeric',
                'fecha_id'              => 'required',
                'detalle_documento'     => 'required',
            ], $messages);
        }

        if ( $validator->fails() ) {            
            return response()->json($validator->errors(),400);
        } else {

            $datos = $this->validarSigfe($request);
            /**
             * Comprobar si se ha seleccionado contrato y orden de compra
             * comprobrar si el monto del documento es igual o menor a los saldos
             * hacer el resto de validaciones.
             */
            $datosContrato = array();
            $contrato = null;
            $oc = null;
            $totalDocumento = formato_entero($request->input('valor_total_documento'));

            if ( $request->input('orden_compra') != null ) {

                $oc = OrdenCompra::findOrFail( $request->input('orden_compra') );

            }

            if ( $request->input('contrato') != null ) {

                $contrato = Contrato::findOrFail($request->input('contrato'));
            }

            if ( is_object($oc) && is_object($contrato) ) {

                if ( $oc->id_contrato != $contrato->id ) {

                    $datosContrato['mensaje'] = 'La Orden de Compra seleccionada debe pertenecer al Contrato seleccionado.';
                    return response()->json($datosContrato, 400);

                }

            }

            if ( ! is_object($oc) && is_object($contrato) ) {
                // Verificar si el contrato tiene OC disponibles con saldo.
                $ocContrato = OrdenCompra::where('id_contrato', $contrato->id)
                                         ->where('saldo_oc', '>', 0)
                                         ->first();

                if ( is_object($ocContrato) ) {

                    $datosContrato['mensaje'] = 'Debe seleccionar una Orden de Compra del Contrato.';
                    return response()->json($datosContrato, 400);

                }

            }

            if ( is_object($oc) && ! is_object($contrato) ) {
                if ( $oc->id_contrato != null ) {

                    $datosContrato['mensaje'] = 'Debe seleccionar el Contrato de la Orden de Compra seleccionada.';
                    return response()->json($datosContrato, 400);

                }
            }

            /**
             * Validar que el monto del documento no supere los saldos de la OC ni del Contrato.
             */
            // Validar con Contrato
            if ( is_object($contrato) ) {

                if ( $request->input('tipo_documento') == 4 || $request->input('tipo_documento') == 10 ) {
                    // NC y CCC

                    if ( $contrato->saldo_preventivo != null || $contrato->saldo_correctivo != null ) {
                        if ( $request->input('montoOcupar') == 'preventivo' && $totalDocumento + $contrato->saldo_preventivo > $contrato->monto_preventivo ) {

                            $datosContrato['mensaje'] = 'El Valor Total más el Saldo Preventivo no puede superar el Monto Preventivo del Contrato.';
                            return response()->json($datosContrato, 400);
                            
                        }
                        if ( $request->input('montoOcupar') == 'correctivo' && $totalDocumento + $contrato->saldo_correctivo > $contrato->monto_correctivo ) {

                            $datosContrato['mensaje'] = 'El Valor Total más el Saldo Correctivo no puede superar el Monto Correctivo del Contrato.';
                            return response()->json($datosContrato, 400);

                        }
                    }

                    if ( $totalDocumento + $contrato->saldo_contrato > $contrato->monto_contrato ) {

                        $datosContrato['mensaje'] = 'El Valor Total más el Saldo Contrato no puede superar el Monto del Contrato.';
                        return response()->json($datosContrato, 400);

                    }

                } else {

                    if ( $contrato->saldo_preventivo != null || $contrato->saldo_correctivo != null ) {
                        if ( $request->input('montoOcupar') == 'preventivo' && $totalDocumento > $contrato->saldo_preventivo ) {

                            $datosContrato['mensaje'] = 'El Valor Total no puede superar el Saldo Preventivo del Contrato.';
                            return response()->json($datosContrato, 400);

                        }
                        if ( $request->input('montoOcupar') == 'correctivo' && $totalDocumento > $contrato->saldo_correctivo ) {

                            $datosContrato['mensaje'] = 'El Valor Total no puede superar el Saldo Correctivo del Contrato.';
                            return response()->json($datosContrato, 400);

                        }
                    }

                    if ( $totalDocumento > $contrato->saldo_contrato ) {

                        $datosContrato['mensaje'] = 'El Valor Total no puede superar el Saldo Contrato.';
                        return response()->json($datosContrato, 400);

                    }

                }
                
            }

            // Validar OC
            if ( is_object($oc) ) {

                if ( $request->input('tipo_documento') == 4 || $request->input('tipo_documento') == 10 ) {
                    if ( $totalDocumento + $oc->saldo_oc  > $oc->monto_oc ) {
                        
                        $datosContrato['mensaje'] = 'El Valor Total más el Saldo OC no puede superar el Monto OC.';
                        return response()->json($datosContrato, 400);

                    }
                } else {
                    if ( $totalDocumento > $oc->saldo_oc ) {
                        
                        $datosContrato['mensaje'] = 'El Valor Total no puede superar el Saldo OC.';
                        return response()->json($datosContrato, 400);

                    }
                }

            }

            if ( $datos['estado_documento'] == 'success' ) {

                $datos = $this->validarNumOrdenCompraAndNumLicitacion($request);
                if ( $datos['estado_documento'] == 'success' ) {

                    // Se valida que para el proveedor y el tipo de documento, no se repita el numero documento
                    // Se agrega a la validacion los documentos eliminados, ya que pueden ser recuperados.
                    $documentoAux = Documento::withTrashed()
                                             ->where('id_proveedor', $request->input('nombre_proveedor'))
                                             ->where('id_tipo_documento', $request->input('tipo_documento'))
                                             ->where('numero_documento', $request->input('numero_documento'))
                                             ->first();


                    if ( !is_object($documentoAux) ) {
                        $newDocumento = new Documento($request);
                        $mensajeDocumento = 'Ingreso exitoso del documento.';

                        // Se devenga el documento
                        $folioSigfe = $newDocumento->editDocumentoDevengar($request);
                        $mensajeDocumento .= '<br><strong style="colo:black !important;">Se ha devengado el Documento.';

                        // Se crean los items presupuestarios
                        if ( $request->input('valor_item') ) {
                            $newDocumento->devengarItemsPresupuestarios($request, $folioSigfe);
                        }
                    
                        $archivo = $request->file('archivo');
                        if ( $archivo != null ) {
                            $nombreOriginalArchivo = str_replace('.pdf','',$archivo->getClientOriginalName() );
                            $arreglo_nombre = explode(".", $archivo->getClientOriginalName() );
                            $extension = strtolower( end( $arreglo_nombre ) );

                            if ($extension != 'pdf') {
                                $datos['mensaje_archivo'] = 'Extension incorrecta del archivo';
                                $datos['estado_archivo'] = 'error';
                            } else {
                                // Se trabajan las rutas del archivo, se guarda en el proyecto al final de la funcion
                                $nombre = $nombreOriginalArchivo.'_'.date('Y_m_d_G_i_s');
                                $rutaArchivo = '/documento/'.$newDocumento->id.'/'.$nombre.'.pdf';
                                $pathArchivo = public_path().'/documento/'.$newDocumento->id.'/';

                                $newArchivo = new Archivo();
                                $newArchivo->id_documento = $newDocumento->id;
                                $newArchivo->id_tipo_archivo = $request->input('tipo_archivo');

                                $newArchivo->nombre = $nombre;
                                $newArchivo->nombre_original = $nombreOriginalArchivo;
                                $newArchivo->ubicacion = "documento/".$newDocumento->id."/".$newArchivo->nombre.".pdf";
                                $newArchivo->extension = "pdf";
                                $newArchivo->peso = $archivo->getSize();
                                $newArchivo->save();

                                $datos['mensaje_archivo'] = 'Se ha guardado correctamente el archivo';
                                $datos['estado_archivo'] = 'success';

                                $archivo->move( $pathArchivo , $nombre.'.pdf' ); // Se guarda el archivo
                            }
                            
                        } else {
                            $datos['mensaje_archivo'] = '';
                            $datos['estado_archivo'] = '';
                        }

                        /*  Para conectar documentos si se ha seleccinado uno
                            Se valida si el nuevo documento tiene los archivos necesarios,
                            Factura y la Autorización SII
                        */
                        if ( $request->input('doc_disponible_bodega') != null || $request->input('guias_disponible_bodega') != null ) {
                            if ( $newDocumento->tieneLosArchivosNecesarios() ) {
                                if ( $request->input('doc_disponible_bodega') != null ) {
                                    // Un solo documento (No es guia despacho)
                                    $wsDocumento = WsDocumento::findOrFail( $request->input('doc_disponible_bodega') );
                                    // Se crea la relacion
                                    $newRelacion = new RelacionDocumentoWsDocumento();
                                    $newRelacion->id_documento = $newDocumento->id;
                                    $newRelacion->id_ws_documento = $wsDocumento->id;
                                    $newRelacion->id_user_relacion = \Auth::user()->id;
                                    $newRelacion->updated_at = NULL;
                                    $newRelacion->save();
                                    // Se paasa el id correspondiente a cada documento para la conexion
                                    $newDocumento->id_relacion = $newRelacion->id;
                                    $newDocumento->validar();
                                    $newDocumento->updated_at = NULL;
                                    $newDocumento->save();
                                    $wsDocumento->id_relacion = $newRelacion->id;
                                    $wsDocumento->save();

                                    
                                }

                                if ( $request->input('guias_disponible_bodega') != null ) {

                                    foreach ($request->input('guias_disponible_bodega') as $index => $idGuiaBodega) {

                                        $wsDocumento = WsDocumento::findOrFail( $idGuiaBodega );
                                        // Se crea la relacion
                                        $newRelacion = new RelacionDocumentoWsDocumento();
                                        $newRelacion->id_documento = $newDocumento->id;
                                        $newRelacion->id_ws_documento = $wsDocumento->id;
                                        $newRelacion->id_user_relacion = \Auth::user()->id;
                                        $newRelacion->updated_at = NULL;
                                        $newRelacion->save();
                                        // Se paasa el id correspondiente al documento para la conexion
                                        $newDocumento->id_relacion = $newRelacion->id; // Queda con la ultima relacion
                                        $newDocumento->validar();
                                        $newDocumento->updated_at = NULL;
                                        $newDocumento->save();
                                        $wsDocumento->id_relacion = $newRelacion->id;
                                        $wsDocumento->save();

                                    }

                                }

                                $mensajeDocumento .= '<br><strong style="colo:black !important;">Se han conectado exitosamente los Documentos.</strong>';
                            } else {
                                $mensajeDocumento .= '<br><strong style="colo:black !important;">No se han conectado los Documentos.';
                                $mensajeDocumento .= '<br>El nuevo Documento no cuenta con el archivo necesario (Factura).</strong>';
                            }
                        }

                        // Se ingresa el documento validado y devengado.
                        $newDocumento->validar();

                        $newDocumento->actualizaValidacionDocumentosRelacionados();

                        /**
                         * Evaluar la relacion del documento con recepcion.
                         * Para pasar a documento con problema o al pendiente de validación
                         */
                        if ( $newDocumento->setProblemaDocumento() == true ) {
                            $mensajeDocumento .= '<br><strong style="colo:black !important;">El documento a quedado con problema, debido a la diferencia en los montos con la recepción.</strong>';
                        }

                        /**
                         * Conectar con contrato, oc y modificar los montos
                         */
                        $newDocumento->unirConContratoOrdenCompra($contrato, $oc, $request);

                        $datos = array(
                            'mensaje_documento' => $mensajeDocumento,
                            'estado_documento' => 'success',
                            'keyTr' => trim( $request->get('_keyTr') )
                        );

                    } else {

                        $datos = array(
                            'mensaje_documento' => 'N° Documento repetido para el Proveedor y Tipo Documento seleccionados',
                            'estado_documento' => 'error',
                        );

                        if ( $documentoAux->trashed() ) {
                            $datos['mensaje_documento'] = 'N° Documento repetido para el Proveedor y Tipo Documento seleccionados.<br><br> El Documento se encuentra eliminado.<br> Observaciones : <br>'.$documentoAux->observacion;
                        }
                        
                    }

                }

            }

        }

        return response()->json($datos,200);

    }

    public function getCargaItemsSigfe()
    {
        if (! \Entrust::can(['carga-sigfe'])) {
            return \Redirect::to('home');
        }

        return view('devengo.index_carga_items_sigfe');
    }

    public function postCargaItemsSigfe(Request $request)
    {
        set_time_limit ( 0 );
        ini_set('max_execution_time', 0);
        
        $archivo = $request->file('archivo');
        // se selecciona la hoja que se quire utilziar del excel
        // $excel = \Excel::selectSheetsByIndex(1)->load($archivo)->get();
        $excel = \Excel::selectSheetsByIndex(0)->load($archivo)->get();
        // dd($excel);

        $numErrorItem = 0;
        $datosTablaErrorItem = array();

        $numCambioItem = 0;
        $datosTablaCambioItem = array();

        $numFolioNoEncontrado = 0;
        $datosTablaFolioNoEncontrado = array();

        $numDevengoNoEncontrado = 0;
        $datosTablaDevengoNoEncontrado = array();

        $numNuevoItem = 0;
        $datosTablaNuevoItem = array();

        $numItemCorrecto = 0;
        $datosTablaItemCorrecto = array();

        foreach ($excel as $key => $fila) {            

            /********************************
             * Falta tabla de los items encontrados
             */

            if ( $fila->fecha != null && $fila->fecha != '' && $fila->folio != null && $fila->folio != '' && $fila->titulo != null && $fila->titulo != '' ) {
                // dd($fila, fecha_M( trim($fila->fecha) ) , fecha_Y( trim($fila->fecha) ) );
                
                $folioSigfe = FolioSigfe::with(['getDocumento','getDocumento.getProveedor','getDocumento.getTipoDocumento',
                                                'getDevengos.getItemPresupuestario'])
                                        ->where('id_sigfe', trim($fila->folio) )
                                        ->where('mes_sigfe', fecha_M( trim($fila->fecha) ) )
                                        ->where('year_sigfe', fecha_Y( trim($fila->fecha) ) )
                                        ->first();

                if ( is_object($folioSigfe) ) {

                    $fila->monto = str_replace(['(',')'],'',$fila->monto);

                    // Trabajar el codigo del item
                    $codigoItem = explode(' ',trim($fila->concepto))[0];
                    $titulo = null;
                    $subtitulo = null;
                    $item = null;
                    $asignacion = null;
                    $subasignacion = null;

                    for ( $i=0; $i< strlen($codigoItem) ; $i++ ) {
                        
                        if ( $i >= 0 && $i < 2 ) {
                            $titulo .= $codigoItem[$i];
                        } elseif ( $i >= 2 && $i < 4 ) {
                            $subtitulo .= $codigoItem[$i];
                        } elseif ( $i >= 4 && $i < 7 ) {
                            $item .= $codigoItem[$i];
                        } elseif ( $i >= 7 && $i < 10 ) {
                            $asignacion .= $codigoItem[$i];
                        } elseif ( $i >= 10 && $i < 12 ) {
                            $subasignacion .= $codigoItem[$i];
                        }

                    }
                    
                    // Este devengo es para actualizar monto
                    $devengo = Devengo::with(['getItemPresupuestario'])
                                      ->where('id_folio_sigfe', $folioSigfe->id)
                                      ->whereHas('getItemPresupuestario', function ($query) use ($titulo, $subtitulo, $item, $asignacion, $subasignacion) {
                                          $query->where('titulo', $titulo)
                                                ->where('subtitulo', $subtitulo)
                                                ->where('item', $item)
                                                ->where('asignacion', $asignacion)
                                                ->where('subasignacion', $subasignacion);
                                        })
                                      ->first();

                    if ( is_object($devengo) ) {

                        if ( $devengo->monto != (int)formato_entero($fila->monto) ) {
                            $numErrorItem++;

                            $botones = '<div class="btn-group">';
                            // Ver
                            if ( \Entrust::can('ver-documento') ) {
                                $botones .= '<button class="btn btn-danger btn-xs" title="Ver documento con item erróneo" onclick="verDocumento('. $folioSigfe->id_documento .');">';
                                $botones .= '<i class="fa fa-eye"></i></button>';
                            }
                            $botones .= '</div>';

                            $proveedor = $folioSigfe->getDocumento->getProveedor->rut.' '.$folioSigfe->getDocumento->getProveedor->nombre;
                            $docJson = array(
                                'DT_RowID' => $folioSigfe->id_documento,
                                $proveedor,
                                $folioSigfe->getDocumento->getTipoDocumento->nombre,
                                $folioSigfe->getDocumento->numero_documento,
                                trim($fila->folio),
                                trim($fila->fecha),
                                $codigoItem,
                                $fila->monto,
                                formatoMiles($devengo->monto),
                                $botones
                            );

                            $datosTablaErrorItem[] = $docJson;

                            // dd('diferencias en montos', $devengo, $fila);
                        } else {

                            $numItemCorrecto++;

                            $botones = '<div class="btn-group">';
                            // Ver
                            if ( \Entrust::can('ver-documento') ) {
                                $botones .= '<button class="btn btn-info btn-xs" title="Ver documento con item correcto" onclick="verDocumento('. $folioSigfe->id_documento .');">';
                                $botones .= '<i class="fa fa-eye"></i></button>';
                            }
                            $botones .= '</div>';                            

                            $proveedor = $folioSigfe->getDocumento->getProveedor->rut.' '.$folioSigfe->getDocumento->getProveedor->nombre;
                            $docJson = array(
                                'DT_RowID' => $folioSigfe->id_documento,
                                $proveedor,
                                $folioSigfe->getDocumento->getTipoDocumento->nombre,
                                $folioSigfe->getDocumento->numero_documento,
                                trim($fila->folio),
                                trim($fila->fecha),
                                $codigoItem,
                                $botones
                            );

                            $datosTablaItemCorrecto[] = $docJson;

                        }

                    } else {
                        // dd('no encontrado',$codigoItem, $titulo,$subtitulo,$item,$asignacion,$subasignacion , $fila, $folioSigfe->id, $folioSigfe->id_documento);

                        /**
                         * Verificar si el folio sigfe tiene algun item por el mismo monto, 
                         * si lo tiene se elimina.
                         * la suma de los item debe ser igual al monto del documento.
                         */

                        $devengo = Devengo::with(['getItemPresupuestario','getFolioSigfe.getDocumento.getProveedor',
                                                  'getFolioSigfe.getDocumento.getTipoDocumento'])
                                         ->where('id_folio_sigfe', $folioSigfe->id)
                                         ->where('monto', (int)formato_entero($fila->monto))                                      
                                         ->first();

                        // dd('no encontrado',$codigoItem, $titulo,$subtitulo,$item,$asignacion,$subasignacion , $fila, $devengo, $folioSigfe->id, $folioSigfe->id_documento);
                        if ( is_object($devengo) ) {

                            // Buscar si existe el item presupuestario que viene del excel
                            $item = ItemPresupuestario::where('titulo', $titulo)
                                                      ->where('subtitulo', $subtitulo)
                                                      ->where('item', $item)
                                                      ->where('asignacion', $asignacion)
                                                      ->where('subasignacion', $subasignacion)
                                                      ->first();

                            if ( is_object($item) ) {

                                // Se cambia el item presupuestario del devengo
                                $numCambioItem++;

                                $botones = '<div class="btn-group">';
                                // Ver
                                if ( \Entrust::can('ver-documento') ) {
                                    $botones .= '<button class="btn btn-info btn-xs" title="Ver documento con cambio de item" onclick="verDocumento('. $devengo->getFolioSigfe->id_documento .');">';
                                    $botones .= '<i class="fa fa-eye"></i></button>';
                                }
                                $botones .= '</div>';

                                $proveedor = $devengo->getFolioSigfe->getDocumento->getProveedor->rut.' '.$devengo->getFolioSigfe->getDocumento->getProveedor->nombre;
                                $docJson = array(
                                    'DT_RowID' => $devengo->getFolioSigfe->id_documento,
                                    $proveedor,
                                    $devengo->getFolioSigfe->getDocumento->getTipoDocumento->nombre,
                                    $folioSigfe->getDocumento->numero_documento,
                                    trim($fila->folio),
                                    trim($fila->fecha),
                                    $codigoItem,
                                    $devengo->getItemPresupuestario->codigoSinPuntos(),
                                    $botones
                                );

                                // if ( $item->titulo == 34 && $item->subtitulo == 07 ) {
                                    $devengo->id_item_presupuestario_previo = $devengo->id_item_presupuestario;
                                    // dd($devengo,$item,'Cambio item');
                                // }

                                $devengo->id_item_presupuestario = $item->id;
                                $devengo->save();

                                $datosTablaCambioItem[] = $docJson;
                            } else {
                                dd('1° no se encuentra item en el sistema, pero si el devengo por el folio y monto ingresado',$fila,$devengo, $folioSigfe);
                            }
                        } else {
                            // Buscar de nuevo el devengo y agregarlo si lo encuentra, si no tiene devengos el folio sigfe
                            /**
                             * Buscar si existe el item para agregarlo como devengo al folio sigfe.
                             * el monto que viene en el excel no puede superar el monto del documento
                             */
                            // Buscar si existe el item presupuestario que viene del excel
                            $item = ItemPresupuestario::where('titulo', $titulo)
                                                      ->where('subtitulo', $subtitulo)
                                                      ->where('item', $item)
                                                      ->where('asignacion', $asignacion)
                                                      ->where('subasignacion', $subasignacion)
                                                      ->first();

                            if ( is_object($item) && $folioSigfe->getDocumento->total_documento <= (int)formato_entero($fila->monto) ) {

                                // Se crea el devengo
                                $numNuevoItem++;

                                $botones = '<div class="btn-group">';
                                // Ver
                                if ( \Entrust::can('ver-documento') ) {
                                    $botones .= '<button class="btn btn-info btn-xs" title="Ver documento con nuevo item" onclick="verDocumento('. $folioSigfe->id_documento .');">';
                                    $botones .= '<i class="fa fa-eye"></i></button>';
                                }
                                $botones .= '</div>';

                                $devengo = new Devengo();
                                $devengo->id_documento = $folioSigfe->id_documento;
                                $devengo->id_folio_sigfe = $folioSigfe->id;
                                $devengo->id_item_presupuestario = $item->id;
                                $devengo->monto = (int)formato_entero($fila->monto);
                                $devengo->save();
 
                                $proveedor = $folioSigfe->getDocumento->getProveedor->rut.' '.$folioSigfe->getDocumento->getProveedor->nombre;
                                $docJson = array(
                                    'DT_RowID' => $folioSigfe->id_documento,
                                    $proveedor,
                                    $folioSigfe->getDocumento->getTipoDocumento->nombre,
                                    $folioSigfe->getDocumento->numero_documento,
                                    trim($fila->folio),
                                    trim($fila->fecha),
                                    $codigoItem,
                                    $botones
                                );
 
                                
 
                                $datosTablaNuevoItem[] = $docJson;
                            } else {

                                $numDevengoNoEncontrado++;

                                $docJson = array(
                                    trim($fila->folio),
                                    trim($fila->fecha),
                                    $fila->concepto,
                                    $fila->monto,
                                );

                                $datosTablaDevengoNoEncontrado[] = $docJson;

                            }
                            
                        }
                    }                                   

                    // dd('se encuentran todos',$devengo,$codigoItem, $fila, $folioSigfe);
                    // dd($devengo);

                } else {
                    // dd('no se encuentra el folioSigfe', $fila);
                    $numFolioNoEncontrado++;

                    $docJson = array(
                        trim($fila->folio),
                        trim($fila->fecha),
                        $fila->concepto,
                        $fila->monto,
                    );

                    $datosTablaFolioNoEncontrado[] = $docJson;
                }
            }
            
        }
        
        $resumen = '<strong>';
        $resumen .= 'Documentos con error en item : '.formatoMiles($numErrorItem).'<br>';
        $resumen .= 'Documentos con cambio de item : '.formatoMiles($numCambioItem).'<br>';
        $resumen .= 'Documentos con nuevo item : '.formatoMiles($numNuevoItem).'<br>';
        $resumen .= 'Documentos con item correcto : '.formatoMiles($numNuevoItem).'<br>';
        $resumen .= 'Folio SIGFE no encontrado : '.formatoMiles($numFolioNoEncontrado).'<br>';
        $resumen .= 'Folio SIGFE o monto devengado no encontrado : '.formatoMiles($numDevengoNoEncontrado).'<br>';

        $datos = array(
            'mensaje' => "Carga ITEM's SIGFE exitosa.",
            'estado'  => 'success',
            'resumen' => $resumen,
            'datosTablaErrorItem'           => $datosTablaErrorItem,
            'datosTablaCambioItem'          => $datosTablaCambioItem,
            'datosTablaFolioNoEncontrado'   => $datosTablaFolioNoEncontrado,
            'datosTablaDevengoNoEncontrado' => $datosTablaDevengoNoEncontrado,
            'datosTablaNuevoItem'           => $datosTablaNuevoItem,
            'datosTablaItemCorrecto'        => $datosTablaItemCorrecto,
        );

        return response()->json($datos,200);
    }

    public function getModalEliminarDevengo($id)
    {

        $folio = FolioSigfe::with(['getDocumento'])
                           ->findOrFail($id);

        return view('devengo.modal_eliminar_devengo')->with('folio', $folio);

    }

    public function postEliminarDevengo(Request $request)
    {
        $folio = FolioSigfe::with(['getDocumento','getDevengos'])
                           ->find( $request->input('id_folio') );

        if ( ! is_object($folio) ) {

            $datos = array(
                'mensaje' => 'No se encuentra el Folio SIGFE en la Base de Datos',
                'estado' => 'error',
            );

        } else {

            $id_doc = $folio->id_documento;

            foreach ( $folio->getDevengos as $devengo ) {
                $devengo->id_folio_sigfe = null;
                $devengo->save();
                if ( $devengo->id_documento == null ) {
                    $devengo->delete();
                }
            }

            $folio->delete();

            $doc = Documento::with(['getFoliosSigfe'])->findOrFail($id_doc);

            if ( $doc->getFoliosSigfe->count() == 0 ) {
                $doc->id_devengador = null;
                $doc->id_estado = 1;
                $doc->fecha_devengado = null;
                $doc->save();
            }
            

            $datos = array(
                'mensaje' => 'Se ha Eliminado correctamente el Folio SIGFE.',
                'estado' => 'success',
            );

        }

        return response()->json($datos,200);
    }

    public function getDevengarDocumentosExcelHome($year)
    {
        set_time_limit ( 0 );
        ini_set('max_execution_time', 0);

        \Excel::create('SIFCON - Informe Por Devengar '.$year.' - '.date('d-m-Y').'', function($excel) use ($year) {
            
            $documentos = Documento::porDevengar();

            if ( $year == '2019' ) {
                $documentos = $documentos->where('fecha_recepcion' , '>=', '2019-06-22')
                                         ->where('fecha_recepcion' , '<=', '2019-12-31');
            }

            if ( $year == '2020' ) {
                $documentos = $documentos->where('fecha_recepcion' , '>=', '2020-01-01')
                                         ->where('fecha_recepcion' , '<=', '2020-12-31');
            }

                        
            $documentos = $documentos->get();
        
            $excel->sheet('Documentos', function($sheet) use($documentos) {
            
                $sheet->row(1, [
                    'Estado', 
                    'Responsable',
                    'Factoring',
                    'Chile-Compra',
                    'Proveedor',
                    'RUT',
                    'Informe',
                    'Tipo Dcto.',
                    'N° Dcto.',
                    'N° Dcto. Relacionado',
                    'F/Doc.',
                    'M/Doc.',
                    'Fecha RD',
                    'Mes RD',
                    'Item',
                    'Monto',
                    'Mecanismo de Adjudicación',
                    'Observación',
                ]);

                $totalPorDevengarExcel = 0;
                $cuentaLineas = 1;
                foreach ( $documentos as $index => $documento ) {
                    $items = '';
                    $totalPorDevengarDocumento = 0;

                    if ( $documento->getDevengos->count() == 0 ) {
                        
                        $cuentaLineas++;
                        $items = 'Sin items';
                        $totalPorDevengarDocumento = $documento->total_documento_actualizado;

                        // para columna "monto"
                        $monto = '';
                        if ($documento->id_tipo_documento == 4 || $documento->id_tipo_documento == 10) {
                            $monto .= '-'.$totalPorDevengarDocumento;
                            $totalPorDevengarExcel = $totalPorDevengarExcel - $totalPorDevengarDocumento;
                        } else {
                            $monto .= $totalPorDevengarDocumento;
                            $totalPorDevengarExcel = $totalPorDevengarExcel + $totalPorDevengarDocumento;
                        }

                        // para columna factoring
                        $factoring = '';
                        if ($documento->getProveedorFactoring) {
                            $factoring .= $documento->getProveedorFactoring->nombre.' '.$documento->getProveedorFactoring->rut;
                        }

                        $userPorDevengar = $documento->getResponsable ? $documento->getResponsable->name : '';

                        $numeroDocumentoRelacionado = '';
                        if ($documento->getDocumentoRelacionado ) {
                            $numeroDocumentoRelacionado = $documento->getDocumentoRelacionado->numero_documento;
                        }
                        $sheet->row($cuentaLineas, [
                            $documento->getEstadoDocumento('excel'), 
                            $userPorDevengar,
                            $factoring,
                            $documento->documento_compra,
                            $documento->getProveedor->nombre,
                            $documento->getProveedor->rut,
                            $documento->getTipoInforme->nombre,
                            $documento->getTipoDocumento->nombre,
                            $documento->numero_documento,
                            $numeroDocumentoRelacionado,
                            fecha_dmY($documento->fecha_documento),
                            explode("-",$documento->fecha_documento)[1], // mes de la fecha del documento
                            fecha_dmY($documento->fecha_recepcion),
                            explode("-",$documento->fecha_recepcion)[1], // mes de la fecha de recepcion del documento
                            $items,
                            $monto,
                            $documento->getTipoAdjudicacion->nombre,
                            $documento->observacion,
                        ]);

                    } else {

                        foreach ( $documento->getDevengos as $devengo) {
                        
                            $cuentaLineas++;
                            $items = $devengo->getItemPresupuestario->codigo();
                            $totalPorDevengarDocumento = $devengo->monto;
    
                            // para columna "monto"
                            $monto = '';
                            if ($documento->id_tipo_documento == 4 || $documento->id_tipo_documento == 10) {
                                $monto .= '-'.$totalPorDevengarDocumento;
                                $totalPorDevengarExcel = $totalPorDevengarExcel - $totalPorDevengarDocumento;
                            } else {
                                $monto .= $totalPorDevengarDocumento;
                                $totalPorDevengarExcel = $totalPorDevengarExcel + $totalPorDevengarDocumento;
                            }
    
                            // para columna factoring
                            $factoring = '';
                            if ($documento->getProveedorFactoring) {
                                $factoring .= $documento->getProveedorFactoring->nombre.' '.$documento->getProveedorFactoring->rut;
                            }
                            
                            $userPorDevengar = '';
                            if ($documento->getUserParaDevengar) {
                                $userPorDevengar = $documento->getUserParaDevengar->name;
                            }
    
                            $numeroDocumentoRelacionado = '';
                            if ($documento->getDocumentoRelacionado ) {
                                $numeroDocumentoRelacionado = $documento->getDocumentoRelacionado->numero_documento;
                            }
                            $sheet->row($cuentaLineas, [
                                $documento->getEstadoDocumento('excel'), 
                                // $documento->getResponsable->name,
                                $userPorDevengar,
                                $factoring,
                                $documento->documento_compra,
                                //explode("-",$documento->fecha_sigfe)[1], // mes de la fecha sigfe
                                $documento->getProveedor->nombre,
                                $documento->getProveedor->rut,
                                $documento->getTipoInforme->nombre,
                                $documento->getTipoDocumento->nombre,
                                $documento->numero_documento,
                                $numeroDocumentoRelacionado,
                                fecha_dmY($documento->fecha_documento),
                                explode("-",$documento->fecha_documento)[1], // mes de la fecha del documento
                                fecha_dmY($documento->fecha_recepcion),
                                explode("-",$documento->fecha_recepcion)[1], // mes de la fecha de recepcion del documento
                                $items,
                                // $documento->id_sigfe,
                                // fecha_dmY($documento->fecha_sigfe),
                                $monto,
                                $documento->getTipoAdjudicacion->nombre,
                                $documento->observacion,
                                // $documento->detalle
                            ]);
                        }

                    }
                    
                    
                }

                $sheet->row( $cuentaLineas+1 , [
                    '', 
                    '',
                    '',
                    '',
                    '',
                    '', 
                    '',
                    '',
                    '',
                    '',
                    '',
                    '',
                    '', 
                    '',
                    'TOTAL',
                    $totalPorDevengarExcel,
                    '',
                    '',
                    ''
                ]);

                //$sheet->fromArray($documentos);
            
            });
        
        })->export('xlsx');
    }

}
