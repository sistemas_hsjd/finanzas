<?php namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests;
use Auth;
use App\TipoInforme;

class TipoInformeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
    */
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index()
    {
        if (!\Entrust::can(['crear-tipo-informe','editar-tipo-informe','eliminar-tipo-informe','ver-tipo-informe','ver-general-tipo-informe'])) {
            return \Redirect::to('home');
        }
        $tiposInforme = TipoInforme::all();
        return view('tipo_informe.index')->with('tiposInforme',$tiposInforme);
    }

    public function getModalVer($id)
    {
        $tipoInforme = TipoInforme::findOrFail($id);
        return view('tipo_informe.modal_ver_tipo_informe')->with('tipoInforme',$tipoInforme);
    }

    public function getModalEditar($id)
    {
        $tipoInforme = TipoInforme::findOrFail($id);
        return view('tipo_informe.modal_editar_tipo_informe')->with('tipoInforme',$tipoInforme);
    }

    public function postEditar(Request $request)
    {
        // dd('Holo',$request->all());
        // Mensajes del validador
        $messages = [
            'required' => 'Debe ingresar el :attribute',
            'nombre.required' => 'Debe ingresar el Nombre',
            'numeric' => 'El :attribute debe solo contener números',
            'max' => 'El :attribute no debe exeder los :max caracteres',
            'min' => 'El :attribute debe tener minimo :min caracteres',
            'unique' => 'El valor :attribute ya existe en la base de datos',
        ];

        // Reglas del validador
        $rules = [
            'nombre' => 'required',
        ];
        
        // Validador de los input del formulario
        $validator = \Validator::make($request->all(), $rules, $messages);

        //Si contiene errores se devuelven todos los errores, de lo contrario guarda en la base de datos
        if ( $validator->fails() ) {
            return response()->json($validator->errors(),400);
        } else {
            $editTipoInforme = TipoInforme::findOrFail($request->input('_id'));
            $editTipoInforme->editTipoInforme($request);

            $datos = array(
                'mensaje' => 'Edición exitosa del Tipo de Informe',
                'estado' => 'success',
                'nombre' => $editTipoInforme->nombre,
                'id' => $editTipoInforme->id,
            );
        }

        return response()->json($datos,200);
    }

    public function create()
    {
        return view('tipo_informe.modal_crear_tipo_informe');
    }

    public function store(Request $request)
    {
        // Mensajes del validador
        $messages = [
            'required' => 'Debe ingresar el :attribute',
            'nombre.required' => 'Debe ingresar el Nombre',
            'numeric' => 'El :attribute debe solo contener números',
            'max' => 'El :attribute no debe exeder los :max caracteres',
            'min' => 'El :attribute debe tener minimo :min caracteres',
            'unique' => 'El valor :attribute ya existe en la base de datos',
        ];

        // Reglas del validador
        $rules = [
            'nombre' => 'required',
        ];

        // Validador de los input del formulario
        $validator = \Validator::make($request->all(), $rules, $messages);

        //Si contiene errores se devuelven todos los errores, de lo contrario guarda en la base de datos
        if ( $validator->fails() ) {
            return response()->json($validator->errors(),400);
        } else {

            try {
                $newTipoInforme = new TipoInforme($request);
            } catch (QueryException $e) {
                $datos = array(
                    'estado' => 'error',
                    'mensaje' => 'Existen problemas al momento de guardar el Tipo de Informe',
                );
                return response()->json($datos,200);
            }

            $datos = array(
                'mensaje' => 'Se ha creado el Tipo de Informe',
                'estado' => 'success',
                'nombre' => $newTipoInforme->nombre,
                'id' => $newTipoInforme->id,
            );
        }

        return response()->json($datos,200);
    }

    public function getModalEliminar($id)
    {
        $tipoInforme = TipoInforme::findOrFail($id);
        return view('tipo_informe.modal_eliminar_tipo_informe')->with('tipoInforme',$tipoInforme);
    }

    public function postEliminar(Request $request)
    {
        $deleteTipoInforme = TipoInforme::find($request->input('_id'));
        if ( !is_object($deleteTipoInforme) ) {
            $datos = array(
                'mensaje' => 'No se encuentra el Tipo de Informe en la Base de Datos',
                'estado' => 'error',
            );
        } else {
            $deleteTipoInforme->id_user_deleted = Auth::user()->id;
            $deleteTipoInforme->timestamps = false;
            $deleteTipoInforme->save();
            $deleteTipoInforme->delete();

            $datos = array(
                'mensaje' => 'Se ha eliminado correctamente el Tipo de Informe.',
                'estado' => 'success',
                'id' => $deleteTipoInforme->id,
            );
        }

        return response()->json($datos,200);
    }
}
