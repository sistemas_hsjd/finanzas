<?php namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests;
use Auth;
use App\Documento;
use App\TipoArchivo;
use App\Archivo;
use App\TipoDocumento;
use App\Proveedor;
use App\ArchivoSiiTxt;
use App\SiiFactoringInfo;

class FactoringController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
    */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Lleva a la vista para hacer la carga individual del factoring
     */
    public function getCargaIndividual()
    {
        if (!\Entrust::can(['carga-individual-factoring'])) {
            return \Redirect::to('home');
        }
        $tiposArchivo = TipoArchivo::all();
        return view('factoring.carga_individual')->with('tiposArchivo',$tiposArchivo);
    }
    
    /**
     * Obtiene todas las facturas del proveedor, tipo de documento = 1,2,8
     */
    public function getFacturasProveedor($id)
    {
        $facturasProveedor = Documento::where('id_proveedor',$id)
                                      ->whereIn('id_tipo_documento',[1,2,8])
                                      ->where('id_factoring',null)
                                      ->get();

        if ( is_object($facturasProveedor) ) {
            $opciones = '';

            foreach ($facturasProveedor as $key => $documento) {
                $opciones .= PHP_EOL.'<option value="'.$documento->id.'"> N° '.$documento->numero_documento.' - '.fecha_dmY($documento->fecha_documento).' - $'.formatoMiles($documento->total_documento).'</option>'.PHP_EOL;
            }

            $datos = array(
                'opciones' => $opciones,
                'estado' => 'success',
            );

        } else {

            $datos = array(
                'mensaje' => 'No se encuentran Facturas del Proveedor',
                'estado' => 'error',
            );

        }

        return response()->json($datos,200);
    }

    /**
     * Registra la carga individual de factoring
     */
    public function postCargaIndividual(Request $request)
    {
        //mensajes de los validadores
        $messages = [
            'required'    => 'Debe ingresar el :attribute',
            'numeric' => 'El :attribute debe solo contener números',
            'max' => 'El :attribute no debe exeder los :max caracteres',
            'min' => 'El :attribute debe tener minimo :min caracteres',
            'date_format' => 'La :attribute no tiene el formato correcto: día/mes/año',
        ];

        //validador de los input del formulario
        $validator = \Validator::make($request->all(), [
            'nombre_proveedor' => 'required|numeric',
            'rut_proveedor' => 'required|numeric',
            'nombre_factoring' => 'required|numeric',
            'rut_factoring' => 'required|numeric',
            'factura' => 'required'
        ], $messages);

        //Si contiene errores se devuelven todos los errores, de lo contrario guarda en la base de datos
        if ( $validator->fails() ) {
            return response()->json($validator->errors(),400);
        } else {

            $documento = Documento::findOrFail($request->input('factura'));

            // Ambos input traen ID de proveedor y factoring(otro proveedor)
            if ( $request->input('nombre_proveedor') == $request->input('nombre_factoring') ) {
                $documento->id_factoring = null;
                $documento->id_user_updated = \Auth::user()->id;
                $documento->save();
            } else {
                $documento->id_factoring = $request->input('nombre_factoring');
                $documento->id_user_updated = \Auth::user()->id;
                $documento->save();
            }

            $datos = array(
                'mensajeDocumento' => 'Registro exitoso de Factoring.',
                'estadoDocumento' => 'success',
            );

            $archivo = $request->file('archivo');
            if ( $archivo != null ) {
                $nombreOriginalArchivo = str_replace('.pdf','',$archivo->getClientOriginalName() );
                $arreglo_nombre = explode(".", $archivo->getClientOriginalName() );
                $extension = strtolower( end( $arreglo_nombre ) );

                if ($extension != 'pdf') {
                    $datos['mensajeArchivo'] = 'Extension incorrecta del archivo';
                    $datos['estadoArchivo'] = 'error';
                } else {
                    // Se trabajan las rutas del archivo, se guarda en el proyecto al final de la funcion
                    $nombre = $nombreOriginalArchivo.'_'.date('Y_m_d_G_i_s');
                    $rutaArchivo = '/documento/'.$documento->id.'/'.$nombre.'.pdf';
                    $pathArchivo = public_path().'/documento/'.$documento->id.'/';

                    $newArchivo = new Archivo();
                    $newArchivo->id_documento = $documento->id;
                    $newArchivo->id_tipo_archivo = $request->input('tipo_archivo');

                    $newArchivo->nombre = $nombre;
                    $newArchivo->nombre_original = $nombreOriginalArchivo;
                    $newArchivo->ubicacion = "documento/".$documento->id."/".$newArchivo->nombre.".pdf";
                    $newArchivo->extension = "pdf";
                    $newArchivo->peso = $archivo->getSize();
                    $newArchivo->save();

                    $datos['mensajeArchivo'] = 'Se ha guardado correctamente el archivo';
                    $datos['estadoArchivo'] = 'success';

                    $archivo->move( $pathArchivo , $nombre.'.pdf' ); // Se guarda el archivo
                }
                
            } else {
                $datos['mensajeArchivo'] = '';
                $datos['estadoArchivo'] = '';
            }
        }
        return response()->json($datos,200);
    }

    /**
     * Muestra todas las facturas con id_factoring != null
     */
    public function getGrilla()
    {
        if (!\Entrust::can(['grilla-factoring','eliminar-factoring'])) {
            return \Redirect::to('home');
        }
        $documentos = Documento::with(['getProveedor','getTipoDocumento','getProveedorFactoring'])
                              ->where('fecha_documento','>=',date('Y-m').'-01')
                              ->where('id_factoring','<>',null)
                              ->get();
                              
        $tiposDocumento = TipoDocumento::all();
        $proveedores = Proveedor::all();

        return view('factoring.grilla_documentos')->with('documentos',$documentos)
                                                  ->with('tiposDocumento',$tiposDocumento)
                                                  ->with('proveedores',$proveedores);
    }

    /**
     * Filtra los documentos con factoring según los filtros seleccionados
     */
    public function postGrilla(Request $request)
    {
        $fechaInicio = fecha_Y_m_d($request->input('filtro_fecha_inicio'));
        $fechaTermino = fecha_Y_m_d($request->input('filtro_fecha_termino'));

        $documentos = Documento::with(['getProveedor','getTipoDocumento','getProveedorFactoring'])
                              ->where('id_factoring','<>',null)
                              ->where('fecha_documento','>=',$fechaInicio)
                              ->where('fecha_documento','<=',$fechaTermino);

        if ( $request->input('filtro_proveedor') != null ) {
            $documentos = $documentos->WhereIn('id_proveedor',$request->input('filtro_proveedor'));
        }

        if ( $request->input('filtro_tipo_documento') != null ) {
            $documentos = $documentos->WhereIn('id_tipo_documento',$request->input('filtro_tipo_documento'));
        }

        if ( $request->input('filtro_factoring') != null ) {
            $documentos = $documentos->WhereIn('id_factoring',$request->input('filtro_factoring'));
        }

        $documentos = $documentos->get();

        $tiposDocumento = TipoDocumento::all();
        $proveedores = Proveedor::all();
        return view('factoring.grilla_documentos')->with('documentos',$documentos)
                                                  ->with('tiposDocumento',$tiposDocumento)
                                                  ->with('proveedores',$proveedores)
                                                  //filtros
                                                  ->with('filtroFechaInicio',$request->input('filtro_fecha_inicio'))
                                                  ->with('filtroFechaTermino',$request->input('filtro_fecha_termino'))
                                                  ->with('filtroProveedor',$request->input('filtro_proveedor'))
                                                  ->with('filtroTipoDocumento',$request->input('filtro_tipo_documento'))
                                                  ->with('filtroProveedorFactoring',$request->input('filtro_factoring'));
    }

    /**
     * Elimina el Factoring del documento
     */
    public function getEliminarFactoring($id)
    {
        $documento = Documento::findOrFail($id);
        $documento->id_factoring = null;
        $documento->id_user_updated = \Auth::user()->id;
        $documento->save();

        $datos = array(
            'mensaje' => 'Eliminación exitosa de Factoring.',
            'estado' => 'success',
            'id' => $documento->id,
        );

        return response()->json($datos,200);
    }

    /**
     * Lleva a la vista para hacer la carga masiva del factoring
     */
    public function getCargaMasiva()
    {
        if (!\Entrust::can(['carga-masiva-factoring'])) {
            return \Redirect::to('home');
        }
        $archivosSII = ArchivoSiiTxt::all();
        return view('factoring.carga_masiva')->with('archivosSii',$archivosSII);
    }

    /**
     * Registra la carga masiva de factoring
     */
    public function postCargaMasiva(Request $request)
    {
        $archivo = $request->file('archivo');
        
        if ($archivo == null) {
            
            $datos = array(
                'mensaje' => 'No existe archivo',
                'estado' => 'error',   
            );

            return response()->json($datos,200);
        } else {
            $nombreOriginal = str_replace('.txt','',$archivo->getClientOriginalName() );
            $arreglo_nombre = explode(".", $archivo->getClientOriginalName() );
            $extension = strtolower( end( $arreglo_nombre ) );

            if ($extension != 'txt') {
                $datos = array(
                    'mensaje' => 'Extension incorrecta del archivo',
                    'estado' => 'error',
                );
    
                return response()->json($datos,200);
            }
            

            // Se trabajan las rutas del excel, se guarda en el proyecto al final de la funcion
            $nombre = $nombreOriginal.'_SII_'.date('Y_m_d_G_i_s');
            $rutaArchivo = '/factoring/'.date('Y-m-d').'/'.$nombre.'.txt';
            $pathArchivo = public_path().'/factoring/'.date('Y-m-d').'/';
 
            // 1° Crear archivo acepta excel.
            $newArchivoSii = new ArchivoSiiTxt();
            $newArchivoSii->id_usuario = Auth::user()->id;
            //el nombre esta compuesto por = nombreOriginal(sin extension)_A_año_mes_dia_hora_minutos_segundos.
            $newArchivoSii->nombre = $nombre;
            $newArchivoSii->nombre_original = $nombreOriginal;
            $newArchivoSii->ubicacion = $rutaArchivo;
            $newArchivoSii->extension = $extension;
            $newArchivoSii->peso = $archivo->getSize();
            $newArchivoSii->save();

            $archivo->move( $pathArchivo , $nombre.'.txt' ); // Se guarda el archivo

            // Se lee el archivo para guardar sus datos
            $archivoTxt = fopen($pathArchivo.''.$nombre.'.txt','r');
            $arregloSii = array();
            while(!feof($archivoTxt)) {

                $arregloTemporal = explode(';',str_replace("null","",fgets($archivoTxt)));
                if(count($arregloTemporal) > 1){
                    $arregloSii[] = $arregloTemporal;
                }

            }
            
            fclose($archivoTxt);
            
            unset($arregloSii[0]);
            unset($arregloSii[1]);

            $numFactoringNuevo = 0;
            $numFactoringAntiguo = 0;
            $numFactoringNoVigente = 0;
            $numSinFactura = 0;
            $resultado = '';
            foreach ($arregloSii as $itemSii) {
                $proveedor = Proveedor::where( 'rut',str_replace('.','',$itemSii[9]) )->first();
                if ( is_object($proveedor) ) {
                    if ( $proveedor->email == '' ) {
                        $proveedor->email = trim($itemSii[11]);
                        $proveedor->save();
                    }
                } else {
                    $proveedor = new Proveedor();
                    $proveedor->rut = strtoupper(str_replace('.','',$itemSii[9]));
                    $proveedor->nombre = strtoupper($itemSii[10]);
                    $proveedor->email = strtoupper($itemSii[11]);
                    $proveedor->activo = 1;
                    $proveedor->save();
                }

                $proveedorFactoring = Proveedor::where( 'rut',str_replace('.','',$itemSii[12]) )->first();
                if ( is_object($proveedorFactoring) ) {
                    if ( $proveedorFactoring->email == '' ) {
                        $proveedorFactoring->email = trim($itemSii[14]);
                        $proveedorFactoring->save();
                    }
                } else {
                    $proveedorFactoring = new Proveedor();
                    $proveedorFactoring->rut = strtoupper(str_replace('.','',$itemSii[12]));
                    $proveedorFactoring->nombre = strtoupper($itemSii[13]);
                    $proveedorFactoring->email = strtoupper($itemSii[14]);
                    $proveedorFactoring->activo = 1;
                    $proveedorFactoring->save();
                }

                $documentoFactoring = Documento::where('id_proveedor',$proveedor->id)
                                      ->where('id_factoring',$proveedorFactoring->id)
                                      ->where('numero_documento',$itemSii[6])
                                      ->first();

                if ( !is_object($documentoFactoring) ) {
                    $documentoFactura = Documento::where('id_proveedor',$proveedor->id)
                                      ->where('numero_documento',$itemSii[6])
                                      ->first();
                    if ( is_object($documentoFactura) ) {
                        // el documento existe, pero no tiene factoring en el sistema
                        $documentoFactura->id_factoring = $proveedorFactoring->id;
                        $documentoFactura->save();
                        /** INSERTAR SII FACTORING INFO */
                        $newFactorinIngo = new SiiFactoringInfo($itemSii);
                        $newFactorinIngo->id_factura = $documentoFactura->id;
                        $newFactorinIngo->save();

                        $numFactoringNuevo++;

                    } else {
                        //no existe el documento
                        /** INSERTAR SII FACTORING INFO */
                        $newFactorinIngo = new SiiFactoringInfo($itemSii);

                        $numSinFactura++;
                    }
                } else {
                    // ya existe el documento, con el proveedor y el factoring, se crea la info del sii
                    /** INSERTAR SII FACTORING INFO */
                    $newFactorinIngo = new SiiFactoringInfo($itemSii);
                    $newFactorinIngo->id_factura = $documentoFactoring->id;
                    $newFactorinIngo->save();
                    $numFactoringAntiguo++;
                }
            }

            $resultado .= '<strong>';
            $resultado .= 'Factoring agregados : '.$numFactoringNuevo.'<br>';
            $resultado .= 'Factoring ya existentes : '.$numFactoringAntiguo.'<br>';
            $resultado .= 'Facturas no encontradas : '.$numSinFactura.'<br>';
            $resultado .= 'Factoring no vigentes : '.$numFactoringNoVigente.'<br>';
            $resultado .= 'Total de documentos revisados : '.count($arregloSii).'<br></strong>';


            $boton = '<td><div class="btn-group">';
            $boton .= '<a class="btn btn-success btn-xs" href="'.asset( $newArchivoSii->ubicacion ).'" target="_blank" title="Ver Archivo">';
            $boton .= '<i class="fa fa-eye"></i>';
            $boton .= '</a></div></td>';
            
            $datos = array(
                'mensaje' => 'Carga de Factoring exitoso.',
                'estado' => 'success',
                'id_archivo' => $newArchivoSii->id,
                'fechaIngreso' => date('d/m/Y'),
                'ingresadoPor' => $newArchivoSii->getUser->name,
                'nombreDelArchivo' => $newArchivoSii->nombre_original,
                'ubicacion' => $newArchivoSii->ubicacion,
                'extension' => $newArchivoSii->extension,
                'peso' => pesoArchivoEnMB($newArchivoSii->peso),
                'boton' => $boton,
                'resultado' => $resultado,
            );

            return response()->json($datos,200);
        }
    }

    /**
     * Muestra todas las Sii factoring info, que no tengan factura y no esten pagados
     */
    public function getGrillaNoFactura()
    {
        if (!\Entrust::can(['grilla-no-factura','ver-factoring','editar-factoring'])) {
            return \Redirect::to('home');
        }
        $documentos = SiiFactoringInfo::where('pagado',0)
                              ->where('id_factura',0)
                              ->get();

        return view('factoring.grilla_no_factura')->with('documentos',$documentos);
    }

    /**
     * Filtra los documentos con factoring según los filtros seleccionados.
     * No se ha creado la funcionalidad en la vista.
     */
    public function postGrillaNoFactura(Request $request)
    {
        dd('Crea la funcionalidad');
        $fechaInicio = fecha_Y_m_d($request->input('filtro_fecha_inicio'));
        $fechaTermino = fecha_Y_m_d($request->input('filtro_fecha_termino'));

        $documentos = Documento::with(['getProveedor','getTipoDocumento','getProveedorFactoring'])
                              ->where('id_factoring','<>',null)
                              ->where('fecha_documento','>=',$fechaInicio)
                              ->where('fecha_documento','<=',$fechaTermino);

        if ( $request->input('filtro_proveedor') != null ) {
            $documentos = $documentos->WhereIn('id_proveedor',$request->input('filtro_proveedor'));
        }

        if ( $request->input('filtro_tipo_documento') != null ) {
            $documentos = $documentos->WhereIn('id_tipo_documento',$request->input('filtro_tipo_documento'));
        }

        if ( $request->input('filtro_factoring') != null ) {
            $documentos = $documentos->WhereIn('id_factoring',$request->input('filtro_factoring'));
        }

        $documentos = $documentos->get();

        $tiposDocumento = TipoDocumento::all();
        $proveedores = Proveedor::all();
        return view('factoring.grilla_documentos')->with('documentos',$documentos)
                                                  ->with('tiposDocumento',$tiposDocumento)
                                                  ->with('proveedores',$proveedores)
                                                  //filtros
                                                  ->with('filtroFechaInicio',$request->input('filtro_fecha_inicio'))
                                                  ->with('filtroFechaTermino',$request->input('filtro_fecha_termino'))
                                                  ->with('filtroProveedor',$request->input('filtro_proveedor'))
                                                  ->with('filtroTipoDocumento',$request->input('filtro_tipo_documento'))
                                                  ->with('filtroProveedorFactoring',$request->input('filtro_factoring'));
    }

    /**
     * Modal para ver el documento
     */
    public function getModalVer($id)
    {
        $documento = SiiFactoringInfo::findOrFail($id);
        return view('factoring.modal_ver_sii')->with('documento',$documento);
    }

    /**
     * Modal para editar el documento
     */
    public function getModalEditar($id)
    {
        $documento = SiiFactoringInfo::findOrFail($id);
        return view('factoring.modal_editar_sii')->with('documento',$documento);
    }

    /**
     * Guarda los cambios efectuados en el sii factoring info
     */
    public function postModalEditar(Request $request)
    {
        // dd('wololo',$request->all());
        $messages = [
            'required' => 'Debe ingresar el :attribute',
            'numeric' => 'El :attribute debe solo contener números',
            'max' => 'El :attribute no debe exeder los :max caracteres',
            'min' => 'El :attribute debe tener minimo :min caracteres',
            'unique' => 'El valor :attribute ya existe en la base de datos',
            'email' => 'Debe ingresar un E-mail valido',
            'cl_rut' => 'El Rut es incorrecto',
        ];

        // Reglas del validador
        $rules = [
            'estado_cesion' => 'required',
            'deudor' => 'required',
            'mail_deudor' => 'required',
            'tipo_doc' => 'required',
            'nombre_doc' => 'required',
            'folio_doc' => 'required',
            'fecha_emision_dte' => 'required',
            'monto_total' => 'required',
            'cedente' => 'required',
            'rz_cedente' => 'required',
            'mail_cedente' => 'required',
            'cesionario' => 'required',
            'rz_cesionario' => 'required',
            'mail_cesionario' => 'required',
            'fecha_cesion' => 'required',
            'monto_cesion' => 'required',
            'fecha_vencimiento' => 'required',
        ];

        // Validador de los input del formulario
        $validator = \Validator::make($request->all(), $rules, $messages);

        //Si contiene errores se devuelven todos los errores, de lo contrario guarda en la base de datos
        if ( $validator->fails() ) {
            return response()->json($validator->errors(),400);
        } else {

            $proveedor = Proveedor::where( 'rut',$request->input('cedente') )->first();
            $proveedorFactoring = Proveedor::where( 'rut',$request->input('cesionario') )->first();

            if ( is_object($proveedor) ) {

                if ( is_object($proveedorFactoring) ) {

                    $documentoFactoring = Documento::where('id_proveedor',$proveedor->id)
                                                   ->where('id_factoring',$proveedorFactoring->id)
                                                   ->where('numero_documento', trim( $request->input('folio_doc')) )
                                                   ->first();

                    if ( is_object($documentoFactoring) ) {

                        $siiFactoring = SiiFactoringInfo::findOrFail($request->input('_id'));
                        $siiFactoring->editSiiFactoring($request);
                        $siiFactoring->id_factura = $documentoFactoring->id;
                        $siiFactoring->save();

                    } else {
                        $documentoFactura = Documento::where('id_proveedor',$proveedor->id)
                                                     ->where('numero_documento', trim( $request->input('folio_doc')) )
                                                     ->first();
                                            
                        if ( is_object($documentoFactura) ) {
                            $documentoFactura->id_factoring = $proveedorFactoring->id;
                            $documentoFactura->save();

                            $siiFactoring = SiiFactoringInfo::findOrFail($request->input('_id'));
                            $siiFactoring->editSiiFactoring($request);
                            $siiFactoring->id_factura = $documentoFactura->id;
                            $siiFactoring->save();
                        } else {
                            $siiFactoring = SiiFactoringInfo::findOrFail($request->input('_id'));
                            $siiFactoring->editSiiFactoring($request);
                            $siiFactoring->id_factura = 0;
                            $siiFactoring->save();
                        }
                    }

                    $datos = array(
                        'mensaje' => 'Se ha editado correctamente.',
                        'estado' => 'success',
                        'id' => $siiFactoring->id,
                        'estado_cesion' => $siiFactoring->estado_cesion,
                        'nombre_doc' => $siiFactoring->nombre_doc,
                        'folio_doc' => $siiFactoring->folio_doc,
                        'fecha_emis_dte' => fecha_dmY($siiFactoring->fecha_emis_dte),
                        'mnt_total' => '$ '.formatoMiles($siiFactoring->mnt_total),
                        'cedente' => $siiFactoring->cedente,
                        'rz_cedente' => $siiFactoring->rz_cedente,
                        'cesionario' => $siiFactoring->cesionario,
                        'razon_cesionario' => $siiFactoring->razon_cesionario,
                        'fecha_cesion' => fecha_dmY($siiFactoring->fecha_cesion),
                        'mnt_cesion' => '$ '.formatoMiles($siiFactoring->mnt_cesion),
                        'fecha_vencimiento' => fecha_dmY($siiFactoring->fecha_vencimiento),
                        'pagado' => $siiFactoring->pagado
                    );

                } else {
                    // Mostar error del proveedor factoring.
                    $datos = array(
                        'mensaje' => 'No existe el Proveedor (cesionario) en el sistema.',
                        'estado' => 'error',   
                    );
                }

            } else {
                // Mostar error del proveedor.
                $datos = array(
                    'mensaje' => 'No existe el Proveedor (cedente) en el sistema.',
                    'estado' => 'error',   
                );
            }

            
        }

        return response()->json($datos,200);
    }

    /**
     * Muestra todas las Sii factoring info, que no tengan factura y esten pagados
     */
    public function getGrillaPagado()
    {
        if (!\Entrust::can(['grilla-factoring-pagado'])) {
            return \Redirect::to('home');
        }
        $documentos = SiiFactoringInfo::where('pagado',1)
                              ->where('id_factura',0)
                              ->get();

        return view('factoring.grilla_pagados')->with('documentos',$documentos);
    }
}
