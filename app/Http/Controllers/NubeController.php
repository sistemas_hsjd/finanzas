<?php namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Auth;

use App\NubeLibroCompra;
use App\NubeLibroHonorario;
use App\NubeLibroDeuda;

// Para guardar archivos
use Storage;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\File;

class NubeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
    */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Libro de Compras
     */

    public function getIndexLibroDeCompras()
    {

        // if (!\Entrust::can(['ver-general-nube-libro-compra','ver-nube-libro-compra'])) {
        //     return \Redirect::to('home');
        // }

        $nubeLibrosCompras = NubeLibroCompra::all();
        return view('nube.libro_compra')->with('nubeLibrosCompras',$nubeLibrosCompras);
    }

    public function postUploadLibroDeCompras(Request $request)
    {

        set_time_limit(0);
        ini_set('max_execution_time', 0);

        $archivo = $request->file('archivo');
        
        if ($archivo == null) {
            
            $datos = array(
                'mensaje' => 'No existe archivo',
                'estado' => 'error',   
            );

        } else {

            $nombreOriginalExcel = str_replace('.xlsx','',$archivo->getClientOriginalName() );
            $arreglo_nombre = explode(".", $archivo->getClientOriginalName() );
            $extension = strtolower( end( $arreglo_nombre ) );

            if ($extension != 'xlsx') {

                $datos = array(
                    'mensaje' => 'Extension incorrecta del archivo, debe ser "xlsx".',
                    'estado' => 'error',
                );
    
            } else {
                
                // Se trabajan las rutas del excel, se guarda en el proyecto al final de la funcion
                $nombre = $nombreOriginalExcel.'_LC_'.date('Y_m_d_G_i_s');
                $rutaExcel = '/carga/'.date('Y-m-d').'/'.$nombre.'.xlsx';
                $pathExcel = public_path().'/carga/'.date('Y-m-d').'/';

                
                $nubeLibroCompra = new NubeLibroCompra();
                $nubeLibroCompra->id_usuario_crea = Auth::user()->id;
                //el nombre esta compuesto por = nombreOriginalExcel(sin extension)_A_año_mes_dia_hora_minutos_segundos.
                $nubeLibroCompra->nombre = $nombre;
                $nubeLibroCompra->nombre_original = $nombreOriginalExcel;
                $nubeLibroCompra->ubicacion = $rutaExcel;
                $nubeLibroCompra->extension = $extension;
                $nubeLibroCompra->peso = $archivo->getSize();
                $nubeLibroCompra->save();

                $archivo->move( $pathExcel , $nombre.'.xlsx' ); // Se guarda el archivo


                // Se trabaja la respuesta
                $boton = '<td><div class="btn-group">';
                $boton .= '<a class="btn btn-success btn-xs" href="'.asset( $nubeLibroCompra->ubicacion ).'" target="_blank" title="Descargar Archivo">';
                $boton .= '<i class="fas fa-cloud-download-alt"></i>';
                $boton .= '</a></div></td>';

                $datos = array(
                    'mensaje'          => 'Ingreso de libro de compras exitoso.',
                    'estado'           => 'success',
                    'id_archivo'       => $nubeLibroCompra->id,
                    'fechaIngreso'     => date('d/m/Y'),
                    'ingresadoPor'     => $nubeLibroCompra->getUser->name,
                    'nombreDelArchivo' => $nubeLibroCompra->nombre_original,
                    'ubicacion'        => $nubeLibroCompra->ubicacion,
                    'extension'        => $nubeLibroCompra->extension,
                    'peso'             => pesoArchivoEnMB($nubeLibroCompra->peso),
                    'boton'            => $boton,
                );

            }

        }

        return response()->json($datos,200);

    }

    public function getModalEliminarLibroDeCompras($idLibro)
    {
        $libro = NubeLibroCompra::findOrFail($idLibro);
        return view('nube.modal_eliminar_libro_compra', compact('libro'));
    }

    public function postEliminarLibroDeCompras(Request $request)
    {
        $libro = NubeLibroCompra::find($request->input('_id'));

        if ( file_exists(public_path().'/'.$libro->ubicacion) ) {
            unlink(public_path().'/'.$libro->ubicacion); // elimina archivo
        }

        if ( !is_object($libro) ) {
            $datos = array(
                'mensaje' => 'No se encuentra el Libro de Compras en la Base de Datos',
                'estado' => 'error',
            );
        } else {
            $libro->delete();

            $datos = array(
                'mensaje' => 'Se ha eliminado correctamente el Libro de Compras.',
                'estado'  => 'success',
                'id'      => $libro->id,
            );
        }

        return response()->json($datos,200);

    }
    
    /**
     * Libro de Honorarios
     */

    public function getIndexLibroDeHonorarios()
    {

        // if (!\Entrust::can(['ver-general-nube-libro-compra','ver-nube-libro-compra'])) {
        //     return \Redirect::to('home');
        // }

        $nubeLibros = NubeLibroHonorario::all();
        return view('nube.libro_honorario')->with('nubeLibros',$nubeLibros);
    }

    public function postUploadLibroDeHonorarios(Request $request)
    {
        set_time_limit(0);
        ini_set('max_execution_time', 0);

        $archivo = $request->file('archivo');
        
        if ($archivo == null) {
            
            $datos = array(
                'mensaje' => 'No existe archivo',
                'estado' => 'error',   
            );

        } else {

            $nombreOriginalExcel = str_replace('.xlsx','',$archivo->getClientOriginalName() );
            $arreglo_nombre = explode(".", $archivo->getClientOriginalName() );
            $extension = strtolower( end( $arreglo_nombre ) );

            if ($extension != 'xlsx') {

                $datos = array(
                    'mensaje' => 'Extension incorrecta del archivo, debe ser "xlsx".',
                    'estado' => 'error',
                );
    
            } else {
                
                // Se trabajan las rutas del excel, se guarda en el proyecto al final de la funcion
                $nombre = $nombreOriginalExcel.'_LH_'.date('Y_m_d_G_i_s');
                $rutaExcel = '/carga/'.date('Y-m-d').'/'.$nombre.'.xlsx';
                $pathExcel = public_path().'/carga/'.date('Y-m-d').'/';

                
                $nubeLibro = new NubeLibroHonorario();
                $nubeLibro->id_usuario_crea = Auth::user()->id;
                //el nombre esta compuesto por = nombreOriginalExcel(sin extension)_A_año_mes_dia_hora_minutos_segundos.
                $nubeLibro->nombre = $nombre;
                $nubeLibro->nombre_original = $nombreOriginalExcel;
                $nubeLibro->ubicacion = $rutaExcel;
                $nubeLibro->extension = $extension;
                $nubeLibro->peso = $archivo->getSize();
                $nubeLibro->save();

                $archivo->move( $pathExcel , $nombre.'.xlsx' ); // Se guarda el archivo


                // Se trabaja la respuesta
                $boton = '<td><div class="btn-group">';
                $boton .= '<a class="btn btn-success btn-xs" href="'.asset( $nubeLibro->ubicacion ).'" target="_blank" title="Descargar Archivo">';
                $boton .= '<i class="fas fa-cloud-download-alt"></i>';
                $boton .= '</a></div></td>';

                $datos = array(
                    'mensaje'          => 'Ingreso de libro exitoso.',
                    'estado'           => 'success',
                    'id_archivo'       => $nubeLibro->id,
                    'fechaIngreso'     => date('d/m/Y'),
                    'ingresadoPor'     => $nubeLibro->getUser->name,
                    'nombreDelArchivo' => $nubeLibro->nombre_original,
                    'ubicacion'        => $nubeLibro->ubicacion,
                    'extension'        => $nubeLibro->extension,
                    'peso'             => pesoArchivoEnMB($nubeLibro->peso),
                    'boton'            => $boton,
                );

            }

        }

        return response()->json($datos,200);

    }

    public function getModalEliminarLibroDeHonorarios($idLibro)
    {
        $libro = NubeLibroHonorario::findOrFail($idLibro);
        return view('nube.modal_eliminar_libro_honorario', compact('libro'));
    }

    public function postEliminarLibroDeHonorarios(Request $request)
    {
        $libro = NubeLibroHonorario::find($request->input('_id'));

        if ( file_exists(public_path().'/'.$libro->ubicacion) ) {
            unlink(public_path().'/'.$libro->ubicacion); // elimina archivo
        }

        if ( !is_object($libro) ) {
            $datos = array(
                'mensaje' => 'No se encuentra el Libro en la Base de Datos',
                'estado' => 'error',
            );
        } else {
            $libro->delete();

            $datos = array(
                'mensaje' => 'Se ha eliminado correctamente el Libro.',
                'estado'  => 'success',
                'id'      => $libro->id,
            );
        }

        return response()->json($datos,200);

    }

    /**
     * Libro de Deudas
     */

    public function getIndexLibroDeDeudas()
    {

        // if (!\Entrust::can(['ver-general-nube-libro-compra','ver-nube-libro-compra'])) {
        //     return \Redirect::to('home');
        // }

        $nubeLibros = NubeLibroDeuda::all();
        return view('nube.libro_deuda')->with('nubeLibros',$nubeLibros);
    }

    public function postUploadLibroDeDeudas(Request $request)
    {
        set_time_limit(0);
        ini_set('max_execution_time', 0);

        $archivo = $request->file('archivo');
        
        if ($archivo == null) {
            
            $datos = array(
                'mensaje' => 'No existe archivo',
                'estado' => 'error',   
            );

        } else {

            $nombreOriginalExcel = str_replace('.xlsx','',$archivo->getClientOriginalName() );
            $arreglo_nombre = explode(".", $archivo->getClientOriginalName() );
            $extension = strtolower( end( $arreglo_nombre ) );

            if ($extension != 'xlsx') {

                $datos = array(
                    'mensaje' => 'Extension incorrecta del archivo, debe ser "xlsx".',
                    'estado' => 'error',
                );
    
            } else {
                
                // Se trabajan las rutas del excel, se guarda en el proyecto al final de la funcion
                $nombre = $nombreOriginalExcel.'_LD_'.date('Y_m_d_G_i_s');
                $rutaExcel = '/carga/'.date('Y-m-d').'/'.$nombre.'.xlsx';
                $pathExcel = public_path().'/carga/'.date('Y-m-d').'/';

                
                $nubeLibro = new NubeLibroDeuda();
                $nubeLibro->id_usuario_crea = Auth::user()->id;
                //el nombre esta compuesto por = nombreOriginalExcel(sin extension)_A_año_mes_dia_hora_minutos_segundos.
                $nubeLibro->nombre = $nombre;
                $nubeLibro->nombre_original = $nombreOriginalExcel;
                $nubeLibro->ubicacion = $rutaExcel;
                $nubeLibro->extension = $extension;
                $nubeLibro->peso = $archivo->getSize();
                $nubeLibro->save();

                $archivo->move( $pathExcel , $nombre.'.xlsx' ); // Se guarda el archivo


                // Se trabaja la respuesta
                $boton = '<td><div class="btn-group">';
                $boton .= '<a class="btn btn-success btn-xs" href="'.asset( $nubeLibro->ubicacion ).'" target="_blank" title="Descargar Archivo">';
                $boton .= '<i class="fas fa-cloud-download-alt"></i>';
                $boton .= '</a></div></td>';

                $datos = array(
                    'mensaje'          => 'Ingreso de libro exitoso.',
                    'estado'           => 'success',
                    'id_archivo'       => $nubeLibro->id,
                    'fechaIngreso'     => date('d/m/Y'),
                    'ingresadoPor'     => $nubeLibro->getUser->name,
                    'nombreDelArchivo' => $nubeLibro->nombre_original,
                    'ubicacion'        => $nubeLibro->ubicacion,
                    'extension'        => $nubeLibro->extension,
                    'peso'             => pesoArchivoEnMB($nubeLibro->peso),
                    'boton'            => $boton,
                );

            }

        }

        return response()->json($datos,200);

    }

    public function getModalEliminarLibroDeDeudas($idLibro)
    {
        $libro = NubeLibroDeuda::findOrFail($idLibro);
        return view('nube.modal_eliminar_libro_deuda', compact('libro'));
    }

    public function postEliminarLibroDeDeudas(Request $request)
    {
        $libro = NubeLibroDeuda::find($request->input('_id'));

        if ( file_exists(public_path().'/'.$libro->ubicacion) ) {
            unlink(public_path().'/'.$libro->ubicacion); // elimina archivo
        }

        if ( !is_object($libro) ) {
            $datos = array(
                'mensaje' => 'No se encuentra el Libro en la Base de Datos',
                'estado' => 'error',
            );
        } else {
            $libro->delete();

            $datos = array(
                'mensaje' => 'Se ha eliminado correctamente el Libro.',
                'estado'  => 'success',
                'id'      => $libro->id,
            );
        }

        return response()->json($datos,200);

    }
}
