<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\TipoPrestacion;

class TipoPrestacionController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
    */
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index()
    {
        // if (!\Entrust::can(['crear-tipo-prestacion','editar-tipo-prestacion','eliminar-tipo-prestacion','ver-tipo-prestacion','ver-general-tipo-prestacion'])) {
        //     return \Redirect::to('home');
        // }

        $prestaciones = TipoPrestacion::all();
        return view('tipo_prestacion.index')->with('prestaciones',$prestaciones);
    }

    public function create()
    {
        return view('tipo_prestacion.modal_crear_tipo_prestacion');
    }

    
    public function store(Request $request)
    {
        // Mensajes del validador
        $messages = [
            'required' => 'Debe ingresar el :attribute',
            'nombre.required' => 'Debe ingresar el Nombre',
            'numeric' => 'El :attribute debe solo contener números',
            'max' => 'El :attribute no debe exeder los :max caracteres',
            'min' => 'El :attribute debe tener minimo :min caracteres',
            'unique' => 'El valor :attribute ya existe en la base de datos',
        ];

        // Reglas del validador
        $rules = [
            'nombre' => 'required',
        ];

        // Validador de los input del formulario
        $validator = \Validator::make($request->all(), $rules, $messages);

        //Si contiene errores se devuelven todos los errores, de lo contrario guarda en la base de datos
        if ( $validator->fails() ) {
            return response()->json($validator->errors(),400);
        } else {

            try {

                $prestacion = new TipoPrestacion;
                $prestacion->nombre = trim( $request->input('nombre') );
                $prestacion->save();

            } catch (QueryException $e) {

                $datos = array(
                    'estado' => 'error',
                    'mensaje' => 'Existen problemas al momento de guardar el Tipo Prestación.',
                );
                return response()->json($datos,200);

            }

            $datos = array(
                'mensaje' => 'Se ha creado el Tipo Prestación.',
                'estado'  => 'success',
                'nombre'  => $prestacion->nombre,
                'id'      => $prestacion->id,
            );
        }

        return response()->json($datos, 200);
    }

    public function show($id)
    {
        $prestacion = TipoPrestacion::findOrFail($id);
        return view('tipo_prestacion.modal_ver_tipo_prestacion')->with('prestacion',$prestacion);
    }

    public function getModalEditar($id)
    {
        $prestacion = TipoPrestacion::findOrFail($id);
        return view('tipo_prestacion.modal_editar_tipo_prestacion')->with('prestacion',$prestacion);
    }

    public function postEditar(Request $request)
    {
        // dd('Holo',$request->all());
        // Mensajes del validador
        $messages = [
            'required' => 'Debe ingresar el :attribute',
            'nombre.required' => 'Debe ingresar el Nombre',
            'numeric' => 'El :attribute debe solo contener números',
            'max' => 'El :attribute no debe exeder los :max caracteres',
            'min' => 'El :attribute debe tener minimo :min caracteres',
            'unique' => 'El valor :attribute ya existe en la base de datos',
        ];

        // Reglas del validador
        $rules = [
            'nombre' => 'required',
        ];
        
        // Validador de los input del formulario
        $validator = \Validator::make($request->all(), $rules, $messages);

        //Si contiene errores se devuelven todos los errores, de lo contrario guarda en la base de datos
        if ( $validator->fails() ) {
            return response()->json($validator->errors(),400);
        } else {
            $prestacion = TipoPrestacion::findOrFail($request->input('_id'));
            $prestacion->nombre = trim( $request->input('nombre') );
            $prestacion->save();

            $datos = array(
                'mensaje' => 'Edición exitosa de Tipo Prestación',
                'estado'  => 'success',
                'nombre'  => $prestacion->nombre,
                'id'      => $prestacion->id,
            );
        }

        return response()->json($datos,200);
    }

    public function getModalEliminar($id)
    {
        $prestacion = TipoPrestacion::findOrFail($id);
        return view('tipo_prestacion.modal_eliminar_tipo_prestacion')->with('prestacion',$prestacion);
    }

    public function postEliminar(Request $request)
    {
        $prestacion = TipoPrestacion::find($request->input('_id'));

        if ( !is_object($prestacion) ) {

            $datos = array(
                'mensaje' => 'No se encuentra el Tipo Prestación en la Base de Datos',
                'estado' => 'error',
            );

        } else {

            $prestacion->delete();

            $datos = array(
                'mensaje' => 'Se ha eliminado correctamente el Tipo Prestación.',
                'estado'  => 'success',
                'id'      => $prestacion->id,
            );

        }

        return response()->json($datos,200);
    }

}
