<?php namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests;
use Auth;

use GuzzleHttp\Client;
use GuzzleHttp\Exception\RequestException;
use Freshwork\ChileanBundle\Rut;

use App\WsContrato;
use App\WsOrdenCompra;
use App\WsDocumento;
use App\WsItemPresupuestario;
use App\WsArchivo;
use App\Proveedor;
use App\HermesDocumento;

class DocumentoBodegaController extends Controller
{

    public function getCodigoPedido()
    {
        set_time_limit(0);
        ini_set('max_execution_time', 0);

        $recepciones = WsDocumento::with(['getWsOrdenCompra.getWsContrato','getRelacion'])
                                  ->where('codigo_pedido', null)
                                  ->take(5000)
                                  ->get();

        // dd($recepciones->count());
        $sinRecepcionHermes = 0;
        $problemaProveedor = 0;
        $sinContrato = 0;
        foreach ( $recepciones as $recepcion ) {

            if ( $recepcion->getWsOrdenCompra->getWsContrato ) {
                
                $rut = $recepcion->getWsOrdenCompra->getWsContrato->rut_proveedor;
                // dd(
                //     $rut,
                //     explode('-', $rut)[0],
                //     explode('-', $rut)[1]
                // );
                if ( array_key_exists(0, explode('-', $rut)) && array_key_exists(1, explode('-', $rut)) ) {
                    $recepcionHermes = \DB::connection('hermes')
                                    ->table('ACTA_RECEPCION')
                                    ->leftJoin('RESOLUCION', 'ACTA_RECEPCION.ID_RESOLUCION', '=', 'RESOLUCION.ID')
                                    ->leftJoin('PJURIDICA', 'RESOLUCION.ID_PROVEEDOR', '=', 'PJURIDICA.ID')
                                    ->leftJoin('PERSONA', 'PJURIDICA.ID_PERSONA', '=', 'PERSONA.ID')
                                    ->leftJoin('TIPO_DOCUMENTO', 'ACTA_RECEPCION.ID_TIPO_DOCUMENTO', '=', 'TIPO_DOCUMENTO.ID')
                                    
                                    ->where('PERSONA.RUT_PERSONA', '=', explode('-', $rut)[0])
                                    ->where('PERSONA.DIGITO_VERIFICADOR', '=', explode('-', $rut)[1])
                                    ->where('ACTA_RECEPCION.CODIGO_DOCUMENTO', $recepcion->documento )
                                    ->where('TIPO_DOCUMENTO.NOMBRE', $recepcion->tipo_documento)

                                    ->select(
                                        'ACTA_RECEPCION.*',
                                        'ACTA_RECEPCION.CODIGO_DOCUMENTO as FOLIO_DOCUMENTO',
                                        'PERSONA.RUT_PERSONA',
                                        'PERSONA.DIGITO_VERIFICADOR',
                                        'PJURIDICA.RAZON_SOCIAL',
                                        'TIPO_DOCUMENTO.NOMBRE as NOMBRE_DOCUMENTO',
                                        'TIPO_DOCUMENTO.CODIGO_SII as CODIGO_SII_DOCUMENTO',
                                        'RESOLUCION.NUMERO_RESOLUCION as RESOLUCION_CONTRATO'
                                    )
                                    ->first();


                    if( is_object( $recepcionHermes ) ) {
                        $recepcion->codigo_pedido = $recepcionHermes->CODIGO_PEDIDO;
                        $recepcion->save();
                    } else {
                        echo "<br>Sin recepcion en hermes ".$recepcion->id;
                        echo "<br>N° de documento : ".$recepcion->documento;
                        echo " <br>Tiene relacion ?";
                        if ( $recepcion->getRelacion != null) { 
                            
                            echo 'Si'; 
                        }else{ 
                            echo 'No'; 
                            // eliminar el wsDocumento
                            $recepcion->delete();
                        }
                        $sinRecepcionHermes++;
                    }
                } else {
                    echo "<br>Problemas con el rut del provedoor ".$recepcion->id;
                    echo "<br>Rut proveedor : ".$recepcion->getWsOrdenCompra->getWsContrato->rut_proveedor;
                    $prov = Proveedor::where('rut', 'LIKE', '%'.$recepcion->getWsOrdenCompra->getWsContrato->rut_proveedor.'%')->first();
                    if ( is_object($prov) ) {
                        $recepcion->getWsOrdenCompra->getWsContrato->rut_proveedor = $prov->rut;
                        $recepcion->getWsOrdenCompra->getWsContrato->save();

                    }
                    $problemaProveedor++;
                }

            } else {
                echo "<br>Sin contrato: id wsDocumento ".$recepcion->id;
                $sinContrato++;
            }

        }
        echo "<br>";
        return "Listo obtener el codigo de pedido : sinRecepcionHermes ".$sinRecepcionHermes." 
        problemaProveedor ".$problemaProveedor."
        sinContrato ".$sinContrato ;

    }

    /**
     * Create a new controller instance.
     *
     * @return void
    */
    public function __construct()
    {
        $this->middleware('auth');
    }
    
    public function dataTableIndex(Request $request)
    {
        set_time_limit ( 0 );
        ini_set('max_execution_time', 0);

        $search = $request->get('search')['value'];
        $order_column = $request->get('order')[0]['column'];
        $order_dir = $request->get('order')[0]['dir'];

        $formJSON = json_decode($request->get('form'));
        $form = construyeFormulario($formJSON);
        
        $fechaInicio = fecha_Y_m_d($form->filtro_fecha_inicio);
        $fechaTermino = fecha_Y_m_d($form->filtro_fecha_termino);

        $wsDocumentos = WsDocumento::with(['getWsOrdenCompra.getWsContrato','getWsArchivos']);

        if ( $form->filtro_numero_documento == null ) {
            // Se filtra por las fechas
            $wsDocumentos = $wsDocumentos->where('fecha_carga','>=',$fechaInicio.' 00:00:00')
                                         ->where('fecha_carga','<=',$fechaTermino.' 23:59:59');

        } else {
            // no se consideran las fechas, y se busca el numero del documento
            $wsDocumentos = $wsDocumentos->where('documento', trim($form->filtro_numero_documento) );
        }

        if ( isset($form->filtro_proveedor) ) {

            $arregloRutProveedores = Proveedor::WhereIn('id', $form->filtro_proveedor)
                                                ->select('rut')
                                                ->get()
                                                ->pluck('rut')
                                                ->toArray();
            
            $wsDocumentos = $wsDocumentos->whereHas('getWsOrdenCompra',function ($query) use ($arregloRutProveedores) {
                $query->whereHas('getWsContrato',function ($query2) use ($arregloRutProveedores) {
                    $query2->WhereIn('rut_proveedor',$arregloRutProveedores);
                });
            });

        }

        if ( $form->filtro_numero_documento_compra != null ) {

            $wsDocumentos = $wsDocumentos->whereHas('getWsOrdenCompra',function ($query) use ($form) {
                $query->where('numero_orden_compra_mercado_publico', trim($form->filtro_numero_documento_compra) );
            });

        }

        $wsDocumentos = $wsDocumentos->get();

        $data = [];

        if ( $request->get('length') != -1 ) {
            $hasta = $request->get('start') + $request->get('length');
        } else {
            // Se quieren ver todos los datos
            $hasta = $request->get('start') + $wsDocumentos->count();
        }
        
        for( $i = $request->get('start') ; $i < $hasta ; $i++ ) {
            if ( isset($wsDocumentos[$i]) ) {
                $data[] = $wsDocumentos[$i]->getDatosRecepciones();
            }
        }

        // Se realiza la busqueda y se ordenan los datos
        if ( $wsDocumentos->count() > 0 ) {

            foreach ($data as $clave => $fila) {

                if ( $search != '' ) {
                    if ( in_array_r($search,$fila) ) {
                        // dd("lo encuentra",$fila,$search);
                        $columna[$clave] = $fila[$order_column];
                    } else {
                        // dd("NO lo encuentra",$fila,$search);
                        unset( $data[$clave] ); // quita el elemento del data
                    }
                } else {
                    $columna[$clave] = $fila[$order_column];
                }
                
            }

            
            // Se ordenan los datos, segun la columna seleccionada
            if ( $order_dir == 'asc' ) {
                array_multisort($columna, SORT_ASC, $data);
            } elseif ( $order_dir == 'desc' ) {
                array_multisort($columna, SORT_DESC, $data);
            }

        }

        $respuesta = array(
            "draw" => $request->get('draw'),
            "recordsTotal" => $wsDocumentos->count(),
            "recordsFiltered"=> $wsDocumentos->count(),
            "data" => $data
        );

        return json_encode($respuesta);

    }

    public function index()
    {
        if (!\Entrust::can(['ver-general-documento-recepcionado','ver-documentos-recepcionados','ver-archivo-documento'])) {
            return \Redirect::to('home');
        }
                                
        $proveedores = Proveedor::all();
        return view('documento_bodega.index')->with('proveedores',$proveedores);
    }

    /**
     * Excel del listado index
     */
    public function postIndexExcel(Request $request)
    {
        set_time_limit ( 0 );
        ini_set('max_execution_time', 0);

        \Excel::create('SIFCON | Recepción Bodega '.date('d-m-Y').'', function($excel) use($request) {

            $wsDocumentos = WsDocumento::with(['getWsOrdenCompra.getWsContrato','getWsArchivos']);

            $fechaInicio = fecha_Y_m_d($request->input('filtro_fecha_inicio'));
            $fechaTermino = fecha_Y_m_d($request->input('filtro_fecha_termino'));

            if ( $request->input('filtro_numero_documento') == null  ) {

                $wsDocumentos = $wsDocumentos->where('fecha_carga','>=', $fechaInicio.' 00:00:00')
                                             ->where('fecha_carga','<=', $fechaTermino.' 23:59:59');

            } else {
                $wsDocumentos = $wsDocumentos->where('documento', trim($form->filtro_numero_documento) );
            }
            

            if ( $request->input('filtro_proveedor') != null ) {

                $arregloRutProveedores = Proveedor::WhereIn('id', $form->filtro_proveedor)
                                                  ->select('rut')
                                                  ->get()
                                                  ->pluck('rut')
                                                  ->toArray();

                $wsDocumentos = $wsDocumentos->whereHas('getWsOrdenCompra',function ($query) use ($arregloRutProveedores) {
                                                $query->whereHas('getWsContrato',function ($query2) use ($arregloRutProveedores) {
                                                        $query2->WhereIn('rut_proveedor',$arregloRutProveedores);
                                                    });
                                                });
            }

            if ( $request->input('filtro_numero_documento_compra') != null ) {
                $wsDocumentos = $wsDocumentos->whereHas('getWsOrdenCompra',function ($query) use ($form) {
                                                    $query->where('numero_orden_compra_mercado_publico', trim($form->filtro_numero_documento_compra) );
                                                });
            }

            $wsDocumentos = $wsDocumentos->get();

            $excel->sheet('Documentos', function($sheet) use($wsDocumentos) {
            
                $sheet->row(1, [
                    'Tipo Dcto.',
                    'Rut Proveedor',
                    'Proveedor',
                    'N° Dcto.',
                    'Monto',
                    'Orden Compra',
                    'Fecha Carga',
                ]);

                $cuentaLineas = 1;
                foreach ( $wsDocumentos as $k => $documento ) {
                    $cuentaLineas++;

                    $rutProveedor = '';
                    $nombreProveedor = '';

                    if ( $documento->getWsOrdenCompra->getWsContrato ) {
                        $rutProveedor = $documento->getWsOrdenCompra->getWsContrato->rut_proveedor;
                        $nombreProveedor = $documento->getWsOrdenCompra->getWsContrato->nombre_proveedor;
                    }

                    $numOcMercadoPublico = '';
                    if ( $documento->getWsOrdenCompra ) {
                        $numOcMercadoPublico = $documento->getWsOrdenCompra->numero_orden_compra_mercado_publico;
                    }

                    $fechaCarga = fecha_dmY($documento->fecha_carga);

                    $sheet->row($cuentaLineas, [
                        $documento->tipo_documento,
                        $rutProveedor,
                        $nombreProveedor,
                        $documento->documento,
                        formatoMiles($documento->documento_total),
                        $numOcMercadoPublico,
                        $fechaCarga
                    ]);
                }


                // $cuentaLineas++;
                // $sheet->row( $cuentaLineas , [
                //     '',
                //     '',
                //     '',
                //     '',
                //     '',
                //     '',
                //     ''
                // ]);

        });

        })->export('xlsx');
    }

    public function dataTableMatch(Request $request)
    {
        set_time_limit ( 0 );
        ini_set('max_execution_time', 0);

        $search = $request->get('search')['value'];
        $order_column = $request->get('order')[0]['column'];
        $order_dir = $request->get('order')[0]['dir'];

        $formJSON = json_decode($request->get('form'));
        $form = construyeFormulario($formJSON);
        
        $fechaInicio = fecha_Y_m_d($form->filtro_fecha_inicio);
        $fechaTermino = fecha_Y_m_d($form->filtro_fecha_termino);

        $wsDocumentos = WsDocumento::with(['getWsOrdenCompra.getWsContrato','getWsArchivos'])
                                   ->where('id_relacion','<>',null);

        if ( $form->filtro_numero_documento == null ) {
            // Se filtra por las fechas
            $wsDocumentos = $wsDocumentos->where('fecha_carga','>=',$fechaInicio.' 00:00:00')
                                         ->where('fecha_carga','<=',$fechaTermino.' 23:59:59');

        } else {
            // no se consideran las fechas, y se busca el numero del documento
            $wsDocumentos = $wsDocumentos->where('documento', trim($form->filtro_numero_documento) );
        }

        if ( isset($form->filtro_proveedor) ) {

            $arregloRutProveedores = Proveedor::WhereIn('id', $form->filtro_proveedor)
                                                ->select('rut')
                                                ->get()
                                                ->pluck('rut')
                                                ->toArray();
            
            $wsDocumentos = $wsDocumentos->whereHas('getWsOrdenCompra',function ($query) use ($arregloRutProveedores) {
                $query->whereHas('getWsContrato',function ($query2) use ($arregloRutProveedores) {
                    $query2->WhereIn('rut_proveedor',$arregloRutProveedores);
                });
            });

        }

        if ( $form->filtro_numero_documento_compra != null ) {

            $wsDocumentos = $wsDocumentos->whereHas('getWsOrdenCompra',function ($query) use ($form) {
                $query->where('numero_orden_compra_mercado_publico', trim($form->filtro_numero_documento_compra) );
            });

        }

        $wsDocumentos = $wsDocumentos->get();

        $data = [];

        if ( $request->get('length') != -1 ) {
            $hasta = $request->get('start') + $request->get('length');
        } else {
            // Se quieren ver todos los datos
            $hasta = $request->get('start') + $wsDocumentos->count();
        }
        
        for( $i = $request->get('start') ; $i < $hasta ; $i++ ) {
            if ( isset($wsDocumentos[$i]) ) {
                $data[] = $wsDocumentos[$i]->getDatosRecepciones();
            }
        }

        // Se realiza la busqueda y se ordenan los datos
        if ( $wsDocumentos->count() > 0 ) {

            foreach ($data as $clave => $fila) {

                if ( $search != '' ) {
                    if ( in_array_r($search,$fila) ) {
                        // dd("lo encuentra",$fila,$search);
                        $columna[$clave] = $fila[$order_column];
                    } else {
                        // dd("NO lo encuentra",$fila,$search);
                        unset( $data[$clave] ); // quita el elemento del data
                    }
                } else {
                    $columna[$clave] = $fila[$order_column];
                }
                
            }

            
            // Se ordenan los datos, segun la columna seleccionada
            if ( $order_dir == 'asc' ) {
                array_multisort($columna, SORT_ASC, $data);
            } elseif ( $order_dir == 'desc' ) {
                array_multisort($columna, SORT_DESC, $data);
            }

        }

        $respuesta = array(
            "draw" => $request->get('draw'),
            "recordsTotal" => $wsDocumentos->count(),
            "recordsFiltered"=> $wsDocumentos->count(),
            "data" => $data
        );

        return json_encode($respuesta);
    }

    public function getListadoMatch()
    {
        if (!\Entrust::can(['ver-general-documento-recepcionado','ver-documentos-recepcionados','ver-archivo-documento'])) {
            return \Redirect::to('home');
        }

        $proveedores = Proveedor::all();
        return view('documento_bodega.index_match')->with('proveedores',$proveedores);
    }

    /**
     * excel de match listos
     */
    public function postMatchExcel(Request $request)
    {
        \Excel::create('SIFCON | Documentos Recepcionados en Bodega con Match Confirmado '.date('d-m-Y').'', function($excel) use($request) {            

            $wsDocumentos = WsDocumento::with(['getWsOrdenCompra.getWsContrato','getWsArchivos'])
                                       ->where('id_relacion','<>', null);

            $fechaInicio = fecha_Y_m_d($request->input('filtro_fecha_inicio'));
            $fechaTermino = fecha_Y_m_d($request->input('filtro_fecha_termino'));

            if ( $request->input('filtro_numero_documento') == null  ) {

                $wsDocumentos = $wsDocumentos->where('fecha_carga','>=', $fechaInicio.' 00:00:00')
                                             ->where('fecha_carga','<=', $fechaTermino.' 23:59:59');

            } else {
                $wsDocumentos = $wsDocumentos->where('documento', trim($form->filtro_numero_documento) );
            }
            

            if ( $request->input('filtro_proveedor') != null ) {

                $arregloRutProveedores = Proveedor::WhereIn('id', $form->filtro_proveedor)
                                                  ->select('rut')
                                                  ->get()
                                                  ->pluck('rut')
                                                  ->toArray();

                $wsDocumentos = $wsDocumentos->whereHas('getWsOrdenCompra',function ($query) use ($arregloRutProveedores) {
                                                $query->whereHas('getWsContrato',function ($query2) use ($arregloRutProveedores) {
                                                        $query2->WhereIn('rut_proveedor',$arregloRutProveedores);
                                                    });
                                                });
            }

            if ( $request->input('filtro_numero_documento_compra') != null ) {
                $wsDocumentos = $wsDocumentos->whereHas('getWsOrdenCompra',function ($query) use ($form) {
                                                    $query->where('numero_orden_compra_mercado_publico', trim($form->filtro_numero_documento_compra) );
                                                });
            }

            $wsDocumentos = $wsDocumentos->get();

            $excel->sheet('Documentos', function($sheet) use($wsDocumentos) {
            
                $sheet->row(1, [
                    'Tipo Dcto.',
                    'Rut Proveedor',
                    'Proveedor',
                    'N° Dcto.',
                    'Monto',
                    'Orden Compra',
                    'Fecha Carga'
                ]);

                $cuentaLineas = 1;
                foreach ( $wsDocumentos as $k => $documento ) {
                    $cuentaLineas++;

                    $rutProveedor = '';
                    $nombreProveedor = '';

                    if ( $documento->getWsOrdenCompra->getWsContrato ) {
                        $rutProveedor = $documento->getWsOrdenCompra->getWsContrato->rut_proveedor;
                        $nombreProveedor = $documento->getWsOrdenCompra->getWsContrato->nombre_proveedor;
                    }

                    $numOcMercadoPublico = '';
                    if ( $documento->getWsOrdenCompra ) {
                        $numOcMercadoPublico = $documento->getWsOrdenCompra->numero_orden_compra_mercado_publico;
                    }

                    $fechaCarga = fecha_dmY($documento->fecha_carga);

                    $sheet->row($cuentaLineas, [
                        $documento->tipo_documento,
                        $rutProveedor,
                        $nombreProveedor,
                        $documento->documento,
                        formatoMiles($documento->documento_total),
                        $numOcMercadoPublico,
                        $fechaCarga
                    ]);
                }


                // $cuentaLineas++;
                // $sheet->row( $cuentaLineas , [
                //     '',
                //     '',
                //     '',
                //     '',
                //     '',
                //     '',
                //     ''
                // ]);

        });

        })->export('xlsx');
    }

    public function dataTableMatchPendiente(Request $request)
    {
        set_time_limit ( 0 );
        ini_set('max_execution_time', 0);

        $search = $request->get('search')['value'];
        $order_column = $request->get('order')[0]['column'];
        $order_dir = $request->get('order')[0]['dir'];

        $formJSON = json_decode($request->get('form'));
        $form = construyeFormulario($formJSON);
        
        $fechaInicio = fecha_Y_m_d($form->filtro_fecha_inicio);
        $fechaTermino = fecha_Y_m_d($form->filtro_fecha_termino);

        $documentos = \DB::table('documentos')
                          ->join('archivos','documentos.id','=','archivos.id_documento')
                          ->join('proveedores','documentos.id_proveedor','=','proveedores.id')
                          ->join('tipos_documento','documentos.id_tipo_documento','=','tipos_documento.id')
                          ->join('ws_contrato','proveedores.rut','=','ws_contrato.rut_proveedor')
                          ->join('ws_orden_compra','ws_contrato.id','=','ws_orden_compra.id_contrato')
                          ->join('ws_documento','ws_orden_compra.id','=','ws_documento.id_orden_compra')

                          ->whereColumn('tipos_documento.nombre','LIKE','ws_documento.tipo_documento')
                          ->whereColumn('documentos.numero_documento','=','ws_documento.documento')
                          ->whereColumn('documentos.total_documento_actualizado','=','ws_documento.documento_total')
                          ->where('archivos.id_tipo_archivo',1) // Factura
                          ->where('documentos.id_relacion',null)
                          ->where('ws_documento.id_relacion',null)

                          ->select(
                                'ws_documento.id as id_ws_documento'
                            )
                          ->get()
                          ->pluck('id_ws_documento')
                          ->toArray();

        $archivosAcepta = \DB::table('archivos_acepta')
                              ->join('proveedores','archivos_acepta.id_proveedor','=','proveedores.id')
                              ->join('tipos_documento','archivos_acepta.id_tipo_documento','tipos_documento.id')
                              ->join('ws_contrato','proveedores.rut','=','ws_contrato.rut_proveedor')
                              ->join('ws_orden_compra','ws_contrato.id','=','ws_orden_compra.id_contrato')
                              ->join('ws_documento','ws_orden_compra.id','=','ws_documento.id_orden_compra')

                              ->whereColumn('tipos_documento.nombre','LIKE','ws_documento.tipo_documento')
                              ->whereColumn('archivos_acepta.folio','=','ws_documento.documento')
                              ->whereColumn('archivos_acepta.monto_total','=','ws_documento.documento_total')
                                
                              ->where('ws_documento.id_relacion',null)
                              ->where('archivos_acepta.cargado',0)
                              ->where('archivos_acepta.id_user_rechazo', null)

                              ->select(
                                    'ws_documento.id as id_ws_documento'
                                )
                              ->get()
                              ->pluck('id_ws_documento')
                              ->toArray();

        $arregloIdWsDocs = array_merge($documentos, $archivosAcepta);
        
        $wsDocumentos = WsDocumento::with(['getWsOrdenCompra.getWsContrato','getWsArchivos'])
                                   ->whereIn('id',$arregloIdWsDocs)
                                   ->where('id_relacion', null);

        

        if ( $form->filtro_numero_documento == null ) {
            // Se filtra por las fechas
            $wsDocumentos = $wsDocumentos->where('fecha_carga','>=',$fechaInicio.' 00:00:00')
                                         ->where('fecha_carga','<=',$fechaTermino.' 23:59:59');

        } else {
            // no se consideran las fechas, y se busca el numero del documento
            $wsDocumentos = $wsDocumentos->where('documento', trim($form->filtro_numero_documento) );
        }

        if ( isset($form->filtro_proveedor) ) {

            $arregloRutProveedores = Proveedor::WhereIn('id', $form->filtro_proveedor)
                                                ->select('rut')
                                                ->get()
                                                ->pluck('rut')
                                                ->toArray();
            
            $wsDocumentos = $wsDocumentos->whereHas('getWsOrdenCompra',function ($query) use ($arregloRutProveedores) {
                $query->whereHas('getWsContrato',function ($query2) use ($arregloRutProveedores) {
                    $query2->WhereIn('rut_proveedor',$arregloRutProveedores);
                });
            });

        }

        if ( $form->filtro_numero_documento_compra != null ) {

            $wsDocumentos = $wsDocumentos->whereHas('getWsOrdenCompra',function ($query) use ($form) {
                $query->where('numero_orden_compra_mercado_publico', trim($form->filtro_numero_documento_compra) );
            });

        }

        $wsDocumentos = $wsDocumentos->get();

        $data = [];

        if ( $request->get('length') != -1 ) {
            $hasta = $request->get('start') + $request->get('length');
        } else {
            // Se quieren ver todos los datos
            $hasta = $request->get('start') + $wsDocumentos->count();
        }
        
        for( $i = $request->get('start') ; $i < $hasta ; $i++ ) {
            if ( isset($wsDocumentos[$i]) ) {
                $data[] = $wsDocumentos[$i]->getDatosRecepcionesMatchPendiente($documentos, $archivosAcepta);
            }
        }

        // Se realiza la busqueda y se ordenan los datos
        if ( $wsDocumentos->count() > 0 ) {

            foreach ($data as $clave => $fila) {

                if ( $search != '' ) {
                    if ( in_array_r($search,$fila) ) {
                        // dd("lo encuentra",$fila,$search);
                        $columna[$clave] = $fila[$order_column];
                    } else {
                        // dd("NO lo encuentra",$fila,$search);
                        unset( $data[$clave] ); // quita el elemento del data
                    }
                } else {
                    $columna[$clave] = $fila[$order_column];
                }
                
            }

            
            // Se ordenan los datos, segun la columna seleccionada
            if ( $order_dir == 'asc' ) {
                array_multisort($columna, SORT_ASC, $data);
            } elseif ( $order_dir == 'desc' ) {
                array_multisort($columna, SORT_DESC, $data);
            }

        }

        $respuesta = array(
            "draw" => $request->get('draw'),
            "recordsTotal" => $wsDocumentos->count(),
            "recordsFiltered"=> $wsDocumentos->count(),
            "data" => $data
        );

        return json_encode($respuesta);
    }

    public function getListadoMatchPendiente()
    {
        if (!\Entrust::can(['ver-general-documento-recepcionado','ver-documentos-recepcionados','ver-archivo-documento'])) {
            return \Redirect::to('home');
        }

        $proveedores = Proveedor::all();
        return view('documento_bodega.index_match_pendiente')->with('proveedores',$proveedores);
    }

    /**
     * Excel de match pendiente
     */
    public function postMatchPendienteExcel(Request $request)
    {
        \Excel::create('SIFCON | Documentos Recepcionados en Bodega con Match Pendiente '.date('d-m-Y').'', function($excel) use($request) {

            $documentos = \DB::table('documentos')
                          ->join('archivos','documentos.id','=','archivos.id_documento')
                          ->join('proveedores','documentos.id_proveedor','=','proveedores.id')
                          ->join('tipos_documento','documentos.id_tipo_documento','=','tipos_documento.id')
                          ->join('ws_contrato','proveedores.rut','=','ws_contrato.rut_proveedor')
                          ->join('ws_orden_compra','ws_contrato.id','=','ws_orden_compra.id_contrato')
                          ->join('ws_documento','ws_orden_compra.id','=','ws_documento.id_orden_compra')

                          ->whereColumn('tipos_documento.nombre','LIKE','ws_documento.tipo_documento')
                          ->whereColumn('documentos.numero_documento','=','ws_documento.documento')
                          ->whereColumn('documentos.total_documento_actualizado','=','ws_documento.documento_total')
                          ->where('archivos.id_tipo_archivo',1) // Factura
                          ->where('documentos.id_relacion',null)
                          ->where('ws_documento.id_relacion',null)

                          ->select(
                                'ws_documento.id as id_ws_documento'
                            )
                          ->get()
                          ->pluck('id_ws_documento')
                          ->toArray();

            $archivosAcepta = \DB::table('archivos_acepta')
                              ->join('proveedores','archivos_acepta.id_proveedor','=','proveedores.id')
                              ->join('tipos_documento','archivos_acepta.id_tipo_documento','tipos_documento.id')
                              ->join('ws_contrato','proveedores.rut','=','ws_contrato.rut_proveedor')
                              ->join('ws_orden_compra','ws_contrato.id','=','ws_orden_compra.id_contrato')
                              ->join('ws_documento','ws_orden_compra.id','=','ws_documento.id_orden_compra')

                              ->whereColumn('tipos_documento.nombre','LIKE','ws_documento.tipo_documento')
                              ->whereColumn('archivos_acepta.folio','=','ws_documento.documento')
                              ->whereColumn('archivos_acepta.monto_total','=','ws_documento.documento_total')
                                
                              ->where('ws_documento.id_relacion',null)
                              ->where('archivos_acepta.cargado',0)
                              ->where('archivos_acepta.id_user_rechazo', null)

                              ->select(
                                    'ws_documento.id as id_ws_documento'
                                )
                              ->get()
                              ->pluck('id_ws_documento')
                              ->toArray();

            $arregloIdWsDocs = array_merge($documentos, $archivosAcepta);

            $wsDocumentos = WsDocumento::with(['getWsOrdenCompra.getWsContrato','getWsArchivos'])
                                       ->whereIn('id',$arregloIdWsDocs)
                                       ->where('id_relacion', null);

            $fechaInicio = fecha_Y_m_d($request->input('filtro_fecha_inicio'));
            $fechaTermino = fecha_Y_m_d($request->input('filtro_fecha_termino'));

            if ( $request->input('filtro_numero_documento') == null  ) {

                $wsDocumentos = $wsDocumentos->where('fecha_carga','>=', $fechaInicio.' 00:00:00')
                                             ->where('fecha_carga','<=', $fechaTermino.' 23:59:59');

            } else {
                $wsDocumentos = $wsDocumentos->where('documento', trim($form->filtro_numero_documento) );
            }
            

            if ( $request->input('filtro_proveedor') != null ) {

                $arregloRutProveedores = Proveedor::WhereIn('id', $form->filtro_proveedor)
                                                  ->select('rut')
                                                  ->get()
                                                  ->pluck('rut')
                                                  ->toArray();

                $wsDocumentos = $wsDocumentos->whereHas('getWsOrdenCompra',function ($query) use ($arregloRutProveedores) {
                                                $query->whereHas('getWsContrato',function ($query2) use ($arregloRutProveedores) {
                                                        $query2->WhereIn('rut_proveedor',$arregloRutProveedores);
                                                    });
                                                });
            }

            if ( $request->input('filtro_numero_documento_compra') != null ) {
                $wsDocumentos = $wsDocumentos->whereHas('getWsOrdenCompra',function ($query) use ($form) {
                                                    $query->where('numero_orden_compra_mercado_publico', trim($form->filtro_numero_documento_compra) );
                                                });
            }

            $wsDocumentos = $wsDocumentos->get();

            $excel->sheet('Documentos', function($sheet) use($wsDocumentos, $documentos, $archivosAcepta) {
            
                $sheet->row(1, [
                    'Tipo Dcto.',
                    'Rut Proveedor',
                    'Proveedor',
                    'N° Dcto.',
                    'Monto',
                    'Orden Compra',
                    'Fecha Carga',
                    'Ubicación Match'
                ]);

                $cuentaLineas = 1;
                foreach ( $wsDocumentos as $k => $documento ) {
                    $cuentaLineas++;

                    if ( in_array($documento->id, $documentos) ) {
                        $documento->ubicacion = 'Documentos';
                    }
            
                    if ( in_array($documento->id, $archivosAcepta) ) {
                        $documento->ubicacion = 'Archivos Acepta';
                    }

                    $rutProveedor = '';
                    $nombreProveedor = '';

                    if ( $documento->getWsOrdenCompra->getWsContrato ) {
                        $rutProveedor = $documento->getWsOrdenCompra->getWsContrato->rut_proveedor;
                        $nombreProveedor = $documento->getWsOrdenCompra->getWsContrato->nombre_proveedor;
                    }

                    $numOcMercadoPublico = '';
                    if ( $documento->getWsOrdenCompra ) {
                        $numOcMercadoPublico = $documento->getWsOrdenCompra->numero_orden_compra_mercado_publico;
                    }

                    $fechaCarga = fecha_dmY($documento->fecha_carga);

                    $sheet->row($cuentaLineas, [
                        $documento->tipo_documento,
                        $rutProveedor,
                        $nombreProveedor,
                        $documento->documento,
                        formatoMiles($documento->documento_total),
                        $numOcMercadoPublico,
                        $fechaCarga,
                        $documento->ubicacion
                    ]);
                }


                // $cuentaLineas++;
                // $sheet->row( $cuentaLineas , [
                //     '',
                //     '',
                //     '',
                //     '',
                //     '',
                //     '',
                //     ''
                // ]);

        });

        })->export('xlsx');
    }

    public function postFiltrarDocumentosBodega(Request $request)
    {
        $wsDocumentos = WsDocumento::with(['getWsOrdenCompra.getWsContrato','getWsArchivos']);

        $fechaInicio = fecha_Y_m_d($request->input('filtro_fecha_inicio'));
        $fechaTermino = fecha_Y_m_d($request->input('filtro_fecha_termino'));
        
        set_time_limit ( 0 );
        ini_set('max_execution_time', 0);

        if ( $request->input('filtro_fecha_inicio') != null && $request->input('filtro_numero_documento') == null ) {
            $wsDocumentos = $wsDocumentos->where('fecha_carga','>=',$fechaInicio.' 00:00:00');
        }

        if ( $request->input('filtro_fecha_termino') != null && $request->input('filtro_numero_documento') == null ) {
            $wsDocumentos = $wsDocumentos->where('fecha_carga','<=',$fechaTermino.' 23:59:59');
        }
        
        if ( $request->input('filtro_proveedor') != null ) {

            $proveedores = Proveedor::WhereIn('id',$request->input('filtro_proveedor'))->get();
            
            $arregloRutProveedores = array();
            
            foreach ($proveedores as $proveedor) {
                $arregloRutProveedores[] = $proveedor->rut;
            }
            
            $wsDocumentos = $wsDocumentos->whereHas('getWsOrdenCompra',function ($query) use ($arregloRutProveedores) {
                $query->whereHas('getWsContrato',function ($query2) use ($arregloRutProveedores) {
                    $query2->WhereIn('rut_proveedor',$arregloRutProveedores);
                });
            });
            
        }

        if ( $request->input('filtro_numero_documento') != null ) {
            $wsDocumentos = $wsDocumentos->where('documento', trim($request->input('filtro_numero_documento')) );
        }

        if ( $request->input('filtro_numero_documento_compra') != null ) {
            $wsDocumentos = $wsDocumentos->whereHas('getWsOrdenCompra',function ($query) use ($request) {
                $query->where('numero_orden_compra_mercado_publico', trim($request->input('filtro_numero_documento_compra')));
            });
        }

        $wsDocumentos = $wsDocumentos->get();

        $proveedores = Proveedor::all();
        return view('documento_bodega.index')->with('wsDocumentos',$wsDocumentos)
                                             ->with('proveedores',$proveedores)
                                             ->with('filtroFechaInicio',$request->input('filtro_fecha_inicio'))
                                             ->with('filtroFechaTermino',$request->input('filtro_fecha_termino'))
                                             ->with('filtroProveedor',$request->input('filtro_proveedor'))
                                             ->with('filtroNumeroDocumento',$request->input('filtro_numero_documento'))
                                             ->with('filtroNumeroDocumentoCompra',$request->input('filtro_numero_documento_compra'));

    }

    /**
     * Muestra todos los WS Documento que no tiene documento en el sistema para
     * hacer la conexion
     */
    public function getListadoSinDocumento()
    {
        if (!\Entrust::can(['ver-general-recepcion-sin-documento','comentar-error-recepcion'])) {
            return \Redirect::to('home');
        }
        
        $proveedores = Proveedor::all();

        return view('documento_bodega.index_sin_documento')->with('proveedores', $proveedores);
        
    }

    public function dataTableSinDocumento(Request $request)
    {
        set_time_limit ( 0 );
        ini_set('max_execution_time', 0);

        $search = $request->get('search')['value'];
        $order_column = $request->get('order')[0]['column'];
        $order_dir = $request->get('order')[0]['dir'];

        $formJSON = json_decode($request->get('form'));
        $form = construyeFormulario($formJSON);
        
        $fechaInicio = fecha_Y_m_d($form->filtro_fecha_inicio);
        $fechaTermino = fecha_Y_m_d($form->filtro_fecha_termino);

        $documentos = \DB::table('documentos')
                          ->join('archivos','documentos.id','=','archivos.id_documento')
                          ->join('proveedores','documentos.id_proveedor','=','proveedores.id')
                          ->join('tipos_documento','documentos.id_tipo_documento','=','tipos_documento.id')
                          ->join('ws_contrato','proveedores.rut','=','ws_contrato.rut_proveedor')
                          ->join('ws_orden_compra','ws_contrato.id','=','ws_orden_compra.id_contrato')
                          ->join('ws_documento','ws_orden_compra.id','=','ws_documento.id_orden_compra')

                          ->whereColumn('tipos_documento.nombre','LIKE','ws_documento.tipo_documento')
                          ->whereColumn('documentos.numero_documento','=','ws_documento.documento')
                          ->whereColumn('documentos.total_documento_actualizado','=','ws_documento.documento_total')
                          ->where('archivos.id_tipo_archivo',1) // Factura
                          ->where('documentos.id_relacion', null)
                          ->where('ws_documento.id_relacion', null)

                          ->select(
                                'ws_documento.id as id_ws_documento'
                            )
                          ->get()
                          ->pluck('id_ws_documento')
                          ->toArray();

        $archivosAcepta = \DB::table('archivos_acepta')
                              ->join('proveedores','archivos_acepta.id_proveedor','=','proveedores.id')
                              ->join('tipos_documento','archivos_acepta.id_tipo_documento','tipos_documento.id')
                              ->join('ws_contrato','proveedores.rut','=','ws_contrato.rut_proveedor')
                              ->join('ws_orden_compra','ws_contrato.id','=','ws_orden_compra.id_contrato')
                              ->join('ws_documento','ws_orden_compra.id','=','ws_documento.id_orden_compra')

                              ->whereColumn('tipos_documento.nombre','LIKE','ws_documento.tipo_documento')
                              ->whereColumn('archivos_acepta.folio','=','ws_documento.documento')
                              ->whereColumn('archivos_acepta.monto_total','=','ws_documento.documento_total')
                                
                              ->where('ws_documento.id_relacion',null)
                              ->where('archivos_acepta.cargado',0)
                              ->where('archivos_acepta.id_user_rechazo', null)

                              ->select(
                                    'ws_documento.id as id_ws_documento'
                                )
                              ->get()
                              ->pluck('id_ws_documento')
                              ->toArray();

        $arregloIdWsDocs = array_merge($documentos, $archivosAcepta);
        
        $wsDocumentos = WsDocumento::with(['getWsOrdenCompra.getWsContrato','getWsArchivos'])
                                   ->whereNotIn('id',$arregloIdWsDocs)
                                   ->where('id_relacion', null);

        

        if ( $form->filtro_numero_documento == null ) {
            // Se filtra por las fechas
            $wsDocumentos = $wsDocumentos->where('fecha_carga','>=',$fechaInicio.' 00:00:00')
                                         ->where('fecha_carga','<=',$fechaTermino.' 23:59:59');

        } else {
            // no se consideran las fechas, y se busca el numero del documento
            $wsDocumentos = $wsDocumentos->where('documento', trim($form->filtro_numero_documento) );
        }

        if ( isset($form->filtro_proveedor) ) {

            $arregloRutProveedores = Proveedor::WhereIn('id', $form->filtro_proveedor)
                                                ->select('rut')
                                                ->get()
                                                ->pluck('rut')
                                                ->toArray();
            
            $wsDocumentos = $wsDocumentos->whereHas('getWsOrdenCompra',function ($query) use ($arregloRutProveedores) {
                $query->whereHas('getWsContrato',function ($query2) use ($arregloRutProveedores) {
                    $query2->WhereIn('rut_proveedor',$arregloRutProveedores);
                });
            });

        }

        if ( $form->filtro_numero_documento_compra != null ) {

            $wsDocumentos = $wsDocumentos->whereHas('getWsOrdenCompra',function ($query) use ($form) {
                $query->where('numero_orden_compra_mercado_publico', trim($form->filtro_numero_documento_compra) );
            });

        }

        $wsDocumentos = $wsDocumentos->get();

        $data = [];

        if ( $request->get('length') != -1 ) {
            $hasta = $request->get('start') + $request->get('length');
        } else {
            // Se quieren ver todos los datos
            $hasta = $request->get('start') + $wsDocumentos->count();
        }
        
        for( $i = $request->get('start') ; $i < $hasta ; $i++ ) {
            if ( isset($wsDocumentos[$i]) ) {
                $data[] = $wsDocumentos[$i]->getDatosRecepcionesSinDocumento();
            }
        }

        // Se realiza la busqueda y se ordenan los datos
        if ( $wsDocumentos->count() > 0 ) {

            foreach ($data as $clave => $fila) {

                if ( $search != '' ) {
                    if ( in_array_r($search,$fila) ) {
                        // dd("lo encuentra",$fila,$search);
                        $columna[$clave] = $fila[$order_column];
                    } else {
                        // dd("NO lo encuentra",$fila,$search);
                        unset( $data[$clave] ); // quita el elemento del data
                    }
                } else {
                    $columna[$clave] = $fila[$order_column];
                }
                
            }

            
            // Se ordenan los datos, segun la columna seleccionada
            if ( $order_dir == 'asc' ) {
                array_multisort($columna, SORT_ASC, $data);
            } elseif ( $order_dir == 'desc' ) {
                array_multisort($columna, SORT_DESC, $data);
            }

        }

        $respuesta = array(
            "draw" => $request->get('draw'),
            "recordsTotal" => $wsDocumentos->count(),
            "recordsFiltered"=> $wsDocumentos->count(),
            "data" => $data
        );

        return json_encode($respuesta);
    }

    /**
     * Excel de las recepciones sin documento en el sistema
     */
    public function postSinDocumentoExcel(Request $request)
    {
        \Excel::create('SIFCON | Recepción Bodega Sin Documento para Match '.date('d-m-Y').'', function($excel) use($request) {

            $documentos = \DB::table('documentos')
                          ->join('archivos','documentos.id','=','archivos.id_documento')
                          ->join('proveedores','documentos.id_proveedor','=','proveedores.id')
                          ->join('tipos_documento','documentos.id_tipo_documento','=','tipos_documento.id')
                          ->join('ws_contrato','proveedores.rut','=','ws_contrato.rut_proveedor')
                          ->join('ws_orden_compra','ws_contrato.id','=','ws_orden_compra.id_contrato')
                          ->join('ws_documento','ws_orden_compra.id','=','ws_documento.id_orden_compra')

                          ->whereColumn('tipos_documento.nombre','LIKE','ws_documento.tipo_documento')
                          ->whereColumn('documentos.numero_documento','=','ws_documento.documento')
                          ->whereColumn('documentos.total_documento_actualizado','=','ws_documento.documento_total')
                          ->where('archivos.id_tipo_archivo',1) // Factura
                          ->where('documentos.id_relacion', null)
                          ->where('ws_documento.id_relacion', null)

                          ->select(
                                'ws_documento.id as id_ws_documento'
                            )
                          ->get()
                          ->pluck('id_ws_documento')
                          ->toArray();

            $archivosAcepta = \DB::table('archivos_acepta')
                              ->join('proveedores','archivos_acepta.id_proveedor','=','proveedores.id')
                              ->join('tipos_documento','archivos_acepta.id_tipo_documento','tipos_documento.id')
                              ->join('ws_contrato','proveedores.rut','=','ws_contrato.rut_proveedor')
                              ->join('ws_orden_compra','ws_contrato.id','=','ws_orden_compra.id_contrato')
                              ->join('ws_documento','ws_orden_compra.id','=','ws_documento.id_orden_compra')

                              ->whereColumn('tipos_documento.nombre','LIKE','ws_documento.tipo_documento')
                              ->whereColumn('archivos_acepta.folio','=','ws_documento.documento')
                              ->whereColumn('archivos_acepta.monto_total','=','ws_documento.documento_total')
                                
                              ->where('ws_documento.id_relacion',null)
                              ->where('archivos_acepta.cargado',0)
                              ->where('archivos_acepta.id_user_rechazo', null)

                              ->select(
                                    'ws_documento.id as id_ws_documento'
                                )
                              ->get()
                              ->pluck('id_ws_documento')
                              ->toArray();

            $arregloIdWsDocs = array_merge($documentos, $archivosAcepta);

            $wsDocumentos = WsDocumento::with(['getWsOrdenCompra.getWsContrato','getWsArchivos'])
                                       ->whereNotIn('id',$arregloIdWsDocs)
                                       ->where('id_relacion', null);

            $fechaInicio = fecha_Y_m_d($request->input('filtro_fecha_inicio'));
            $fechaTermino = fecha_Y_m_d($request->input('filtro_fecha_termino'));

            if ( $request->input('filtro_numero_documento') == null  ) {

                $wsDocumentos = $wsDocumentos->where('fecha_carga','>=', $fechaInicio.' 00:00:00')
                                             ->where('fecha_carga','<=', $fechaTermino.' 23:59:59');

            } else {
                $wsDocumentos = $wsDocumentos->where('documento', trim($form->filtro_numero_documento) );
            }
            

            if ( $request->input('filtro_proveedor') != null ) {

                $arregloRutProveedores = Proveedor::WhereIn('id', $form->filtro_proveedor)
                                                  ->select('rut')
                                                  ->get()
                                                  ->pluck('rut')
                                                  ->toArray();

                $wsDocumentos = $wsDocumentos->whereHas('getWsOrdenCompra',function ($query) use ($arregloRutProveedores) {
                                                $query->whereHas('getWsContrato',function ($query2) use ($arregloRutProveedores) {
                                                        $query2->WhereIn('rut_proveedor',$arregloRutProveedores);
                                                    });
                                                });
            }

            if ( $request->input('filtro_numero_documento_compra') != null ) {
                $wsDocumentos = $wsDocumentos->whereHas('getWsOrdenCompra',function ($query) use ($form) {
                                                    $query->where('numero_orden_compra_mercado_publico', trim($form->filtro_numero_documento_compra) );
                                                });
            }

            $wsDocumentos = $wsDocumentos->get();

            $excel->sheet('Documentos', function($sheet) use($wsDocumentos) {
            
                $sheet->row(1, [
                    'Tipo Dcto.',
                    'Rut Proveedor',
                    'Proveedor',
                    'N° Dcto.',
                    'Monto',
                    'Orden Compra',
                    'Fecha Carga',
                ]);

                $cuentaLineas = 1;
                foreach ( $wsDocumentos as $k => $documento ) {
                    $cuentaLineas++;

                    $rutProveedor = '';
                    $nombreProveedor = '';

                    if ( $documento->getWsOrdenCompra->getWsContrato ) {
                        $rutProveedor = $documento->getWsOrdenCompra->getWsContrato->rut_proveedor;
                        $nombreProveedor = $documento->getWsOrdenCompra->getWsContrato->nombre_proveedor;
                    }

                    $numOcMercadoPublico = '';
                    if ( $documento->getWsOrdenCompra ) {
                        $numOcMercadoPublico = $documento->getWsOrdenCompra->numero_orden_compra_mercado_publico;
                    }

                    $fechaCarga = fecha_dmY($documento->fecha_carga);

                    $sheet->row($cuentaLineas, [
                        $documento->tipo_documento,
                        $rutProveedor,
                        $nombreProveedor,
                        $documento->documento,
                        formatoMiles($documento->documento_total),
                        $numOcMercadoPublico,
                        $fechaCarga
                    ]);
                }


                // $cuentaLineas++;
                // $sheet->row( $cuentaLineas , [
                //     '',
                //     '',
                //     '',
                //     '',
                //     '',
                //     '',
                //     ''
                // ]);

        });

        })->export('xlsx');
    }

    public function getListadoOrdenCompra()
    {
        $ordenesCompra = WsOrdenCompra::with('getWsArchivos')->where('id_contrato',null)->get();
        return view('documento_bodega.index_oc',compact('ordenesCompra'));
    }

    public function getModalVer($id)
    {
        $wsDocumento = WsDocumento::with(['getWsOrdenCompra','getWsOrdenCompra.getWsContrato',
                                          'getWsArchivos','getWsItemsPresupuestarios'])
                                          ->findOrFail($id);

        return view('documento_bodega.modal_ver_documento_bodega')->with('wsDocumento',$wsDocumento);
    }

    public function getModalErrores($id)
    {
        $wsDocumento = WsDocumento::with(['getWsOrdenCompra','getWsOrdenCompra.getWsContrato',
                                          'getWsArchivos','getWsItemsPresupuestarios'])
                                          ->findOrFail($id);

        return view('documento_bodega.modal_error_documento_bodega')->with('wsDocumento',$wsDocumento);
    }

    /**
     * 
     */
    public function postModalErrores(Request $request)
    {
        // dd($request->all());
        // Mensajes de los validadores
        $messages = [
            'required'    => 'Debe ingresar el  :attribute',
            'numeric' => 'El :attribute debe solo contener números',
            'max' => 'El :attribute no debe exeder los :max caracteres',
            'min' => 'El :attribute debe tener minimo :min caracteres',
            'date_format' => 'La :attribute no tiene el formato correcto: día/mes/año',
        ];

        //validador de los input del formulario
        $validator = \Validator::make($request->all(), [
            '_id' => 'required|numeric',
            'comentario_error' => 'required',
        ], $messages);

        //Si contiene errores se devuelven todos los errores, de lo contrario guarda en la base de datos
        if ( $validator->fails() ) {
            //return redirect()->back()->withInput($request->all)->withErrors($validator);
            return response()->json($validator->errors(),400);
        } else {
            $wsDocumento = WsDocumento::with(['getWsOrdenCompra.getWsContrato','getWsArchivos'])
                                      ->findOrFail($request->input('_id'));
            
            $wsDocumento->comentario_error = trim($request->input('comentario_error'));
            $wsDocumento->save();

            $rutProveedor = '';
            $nombreProveedor = '';
            $fechaResolucion = '';
            $resolucion = '';
            if ( $wsDocumento->getWsOrdenCompra->getWsContrato ) {
                $rutProveedor = $wsDocumento->getWsOrdenCompra->getWsContrato->rut_proveedor;
                $nombreProveedor = $wsDocumento->getWsOrdenCompra->getWsContrato->nombre_proveedor;
                $resolucion = $wsDocumento->getWsOrdenCompra->getWsContrato->resolucion;
                if ($wsDocumento->getWsOrdenCompra->getWsContrato->fecha_resolucion != null) {
                    $fechaResolucion = fecha_dmY($wsDocumento->getWsOrdenCompra->getWsContrato->fecha_resolucion);
                }
            }

            $archivos = array();
            foreach ($wsDocumento->getWsArchivos as $archivo) {
                $archivo->nombre = explode('_',$archivo->nombre_archivo)[0];
                $archivos[] = $archivo;
            }

            $errores = '';
            if ( $wsDocumento->comentario_error != null ) {
                $errores = 'Si';
            } else {
                $errores = 'No';
            }
            
            $datos = array(
                'mensaje' => 'Comentario Error Guardado.',
                'estado' => 'success',
                'id' => $wsDocumento->id,
                'tipoDocumento' => $wsDocumento->tipo_documento,
                'rutProveedor' => $rutProveedor,
                'nombreProveedor' => $nombreProveedor,
                'documento' => $wsDocumento->documento,
                'documentoTotal' => '$ '.formatoMiles($wsDocumento->documento_total),
                'numeroOrdenCompra' => $wsDocumento->getWsOrdenCompra->numero_orden_compra,
                'fechaRecepcionOrdenCompra' => fecha_dmY($wsDocumento->getWsOrdenCompra->fecha_recepcion_orden_compra),
                'resolucion' => $resolucion,
                'fechaResolucion' => $fechaResolucion,
                'archivos' => $archivos,
                'errores' => $errores,
            );

        }

        return response()->json($datos,200);
    }

    /**
     * Muestra la vista para realizar carga de WS contrato
     */
    public function getUploadContrato()
    {
        return view('documento_bodega.index_masivo_contrato');
    }

    /**
     * Guardara los Ws Contrato subidos en el excel
     */
    public function postUploadContrato(Request $request)
    {
        set_time_limit ( 0 );
        ini_set('max_execution_time', 0);

        $archivo = $request->file('archivo');
        $excel = \Excel::selectSheetsByIndex(0)->load($archivo)->get();

        $log = '';
        $archivoLog = fopen("log-ws-contrato.txt", "w");

        foreach ($excel as $key => $fila) {
            // dd('holo',$fila);

            // se crea el archivo
            $newWsContrato = new WsContrato();
            $newWsContrato->id = $fila->id;
            $newWsContrato->fecha_carga = $fila->fecha_carga;
            $newWsContrato->id_licitacion = $fila->id_licitacion;
            $newWsContrato->fecha_ingreso_contrato = $fila->fecha_ingreso_contrato;
            $newWsContrato->resolucion = $fila->resolucion;
            $newWsContrato->fecha_resolucion = $fila->fecha_resolucion;
            $newWsContrato->nombre_proveedor = $fila->nombre_proveedor;
            $newWsContrato->rut_proveedor = $fila->rut_proveedor;
            $newWsContrato->fecha_inicio_contrato = $fila->fecha_inicio_contrato;
            $newWsContrato->fecha_termino_contrato = $fila->fecha_termino_contrato;
            $newWsContrato->monto_maximo = $fila->monto_maximo;
            $newWsContrato->created_at = $fila->created_at;
            $newWsContrato->updated_at = $fila->updated_at;
            
            $newWsContrato->save();

            $log = ($key + 1).'.- Se registro correctamente El Ws Contrato : id '.$fila->id.' .'.PHP_EOL;
            fwrite($archivoLog, $log);    

        }

        fclose($archivoLog);
        $datos = array(
            'mensaje' => 'Carga de WS Contrato exitosa.',
            'estado' => 'success',
        );

        return response()->json($datos,200);
    }

    /**
     * Muestra la vista para realizar carga de WS orden de compra
     */
    public function getUploadOrdenCompra()
    {
        return view('documento_bodega.index_masivo_orden');
    }

    /**
     * Guardara los Ws Orden Compra subidos en el excel
     */
    public function postUploadOrdenCompra(Request $request)
    {
        set_time_limit ( 0 );
        ini_set('max_execution_time', 0);

        $archivo = $request->file('archivo');
        $excel = \Excel::selectSheetsByIndex(0)->load($archivo)->get();

        $log = '';
        $archivoLog = fopen("log-ws-orden-compra.txt", "w");

        foreach ($excel as $key => $fila) {
            // dd('holo',$fila);

            // se crea el archivo
            $newWsOrdenCompra = new WsOrdenCompra();
            $newWsOrdenCompra->id = $fila->id;
            $newWsOrdenCompra->fecha_carga = $fila->fecha_carga;
            $newWsOrdenCompra->id_contrato = $fila->id_contrato;
            $newWsOrdenCompra->id_orden_compra = $fila->id_orden_compra;
            $newWsOrdenCompra->fecha_orden_compra = $fila->fecha_orden_compra;
            $newWsOrdenCompra->fecha_recepcion_orden_compra = $fila->fecha_recepcion_orden_compra;
            $newWsOrdenCompra->tipo_compra = $fila->tipo_compra;
            $newWsOrdenCompra->programa = $fila->programa;
            $newWsOrdenCompra->numero_orden_compra = $fila->numero_orden_compra;
            $newWsOrdenCompra->total_orden_compra = $fila->total_orden_compra;
            $newWsOrdenCompra->created_at = $fila->created_at;
            $newWsOrdenCompra->updated_at = $fila->updated_at;
            
            $newWsOrdenCompra->save();

            $log = ($key + 1).'.- Se registro correctamente La Ws Orden Compra : id '.$fila->id.' .'.PHP_EOL;
            fwrite($archivoLog, $log);    

        }

        fclose($archivoLog);
        $datos = array(
            'mensaje' => 'Carga de WS Orden Compra exitosa.',
            'estado' => 'success',
        );

        return response()->json($datos,200);
    }


    /**
     * Muestra la vista para realizar carga de WS documento
     */
    public function getUploadDocumento()
    {
        return view('documento_bodega.index_masivo_documento');
    }

    /**
     * Guardara los Ws Documento subidos en el excel
     */
    public function postUploadDocumento(Request $request)
    {
        set_time_limit ( 0 );
        ini_set('max_execution_time', 0);

        $archivo = $request->file('archivo');
        $excel = \Excel::selectSheetsByIndex(0)->load($archivo)->get();

        $log = '';
        $archivoLog = fopen("log-ws-orden-compra.txt", "w");

        foreach ($excel as $key => $fila) {
            // dd('holo',$fila);

            // se crea el archivo
            $newWsDocumento = new WsDocumento();
            $newWsDocumento->id = $fila->id;
            $newWsDocumento->fecha_carga = $fila->fecha_carga;
            $newWsDocumento->id_orden_compra = $fila->id_orden_compra;
            $newWsDocumento->documento = $fila->documento;
            $newWsDocumento->tipo_documento = $fila->tipo_documento;
            $newWsDocumento->nombre_usuario_responsable = $fila->nombre_usuario_responsable;
            $newWsDocumento->documento_descuento_total = $fila->documento_descuento_total;
            $newWsDocumento->documento_neto = $fila->documento_neto;
            $newWsDocumento->documento_iva = $fila->documento_iva;
            $newWsDocumento->documento_total = $fila->documento_total;
            $newWsDocumento->created_at = $fila->created_at;
            $newWsDocumento->updated_at = $fila->updated_at;
            
            $newWsDocumento->save();

            $log = ($key + 1).'.- Se registro correctamente El Ws Documento : id '.$fila->id.' .'.PHP_EOL;
            fwrite($archivoLog, $log);    

        }

        fclose($archivoLog);
        $datos = array(
            'mensaje' => 'Carga de WS Documento exitosa.',
            'estado' => 'success',
        );

        return response()->json($datos,200);
    }

    /**
     * Muestra la vista para realizar carga de WS item presupuestarios
     */
    public function getUploadItem()
    {
        return view('documento_bodega.index_masivo_item');
    }

    /**
     * Guardara los Ws Item Presupuestario subidos en el excel
     */
    public function postUploadItem(Request $request)
    {
        set_time_limit ( 0 );
        ini_set('max_execution_time', 0);

        $archivo = $request->file('archivo');
        $excel = \Excel::selectSheetsByIndex(0)->load($archivo)->get();

        $log = '';
        $archivoLog = fopen("log-ws-orden-compra.txt", "w");

        foreach ($excel as $key => $fila) {
            // dd('holo',$fila);

            // se crea el archivo
            $WsItemPresupuestario = new WsItemPresupuestario();
            $WsItemPresupuestario->id = $fila->id;
            $WsItemPresupuestario->fecha_carga = $fila->fecha_carga;
            $WsItemPresupuestario->id_documento = $fila->id_documento;
            $WsItemPresupuestario->id_producto = $fila->id_producto;
            $WsItemPresupuestario->cantidad_recepcionada = $fila->cantidad_recepcionada;
            $WsItemPresupuestario->valor_item_recepcionado = $fila->valor_item_recepcionado;
            $WsItemPresupuestario->item_presupuestario = $fila->item_presupuestario;
            $WsItemPresupuestario->created_at = $fila->created_at;
            $WsItemPresupuestario->updated_at = $fila->updated_at;
            
            $WsItemPresupuestario->save();

            $log = ($key + 1).'.- Se registro correctamente El Ws Item Presupuestario : id '.$fila->id.' .'.PHP_EOL;
            fwrite($archivoLog, $log);    

        }

        fclose($archivoLog);
        $datos = array(
            'mensaje' => 'Carga de WS Documento exitosa.',
            'estado' => 'success',
        );

        return response()->json($datos,200);
    }

    /**
     * Muestra la vista para realizar carga de WS archivo
     */
    public function getUploadArchivo()
    {
        return view('documento_bodega.index_masivo_archivo');
    }

    /**
     * Guardara los Ws archivo subidos en el excel
     */
    public function postUploadArchivo(Request $request)
    {
        set_time_limit ( 0 );
        ini_set('max_execution_time', 0);

        $archivo = $request->file('archivo');
        $excel = \Excel::selectSheetsByIndex(0)->load($archivo)->get();

        $log = '';
        // $archivoLog = fopen("log-ws-archivo.txt", "w");

        foreach ($excel as $key => $fila) {
            // dd('holo',$fila);

            // se crea el archivo
            $wsArchivo = new WsArchivo();
            $wsArchivo->id = $fila->id;
            $wsArchivo->fecha_carga = $fila->fecha_carga;
            $wsArchivo->id_documento = $fila->id_documento;
            $wsArchivo->nombre_archivo = $fila->nombre_archivo;
            $wsArchivo->ubicacion = $fila->ubicacion;
            // $wsArchivo->base64 = $fila->base64;
            $wsArchivo->created_at = $fila->created_at;
            $wsArchivo->updated_at = $fila->updated_at;
            
            $wsArchivo->save();

            // Se trabaja el archivo pdf
            // $base64Descomprimido = gzinflate($fila->base64);
            // $auxData = str_replace(' ','+',$base64Descomprimido);
            $auxData = str_replace(' ','+',$fila->base64);
            $content = base64_decode($auxData);
            if ( !file_exists(public_path().'/archivobodega/'.$wsArchivo->id_documento) ) {
                mkdir(public_path().'/archivobodega/'.$wsArchivo->id_documento, 0775, true);
                chmod(public_path().'/archivobodega/'.$wsArchivo->id_documento, 0775);
            }

            $file = fopen( public_path().'/archivobodega/'.$wsArchivo->id_documento.'/'.$wsArchivo->nombre_archivo.'.pdf', 'a');
            fwrite($file, $content);
            fclose($file);


            $log = ($key + 1).'.- Se registro correctamente El Ws Archivo : id '.$fila->id.' .'.PHP_EOL;
            //fwrite($archivoLog, $log);

        }

        // fclose($archivoLog);
        $datos = array(
            'mensaje' => 'Carga de WS Archivo.',
            'estado' => 'success',
        );

        return response()->json($datos,200);
    }

    public function getBuscadorRecepcion()
    {
        return view('documento_bodega.buscador');
    }

    public function postBuscadorRecepcion(Request $request)
    {
        $messages = [
            'required'    => 'Debe ingresar el  :attribute'
        ];

        //validador de los input del formulario
        $validator = \Validator::make($request->all(), [
            'numero_recepcion' => 'required',
        ], $messages);

        if ( $validator->fails() ) {
            return response()->json($validator->errors(),400);
        } else {
            $codigoPedido = $request->input('numero_recepcion');
            $mensaje = '';
            $error = false; // utilizada cuando se obtiene la rececpcion desde hermes

            /**
             * Se busca en:
             * 1- wsDocumento
             * 2- HermesDocumento
             * 3- base de datos de hermes, si se encuentra, se guarda.
             */
            $recepcionConfirmada = WsDocumento::with('getRelacion')
                                              ->where('codigo_pedido', $codigoPedido)
                                              ->first();

            if ( is_object( $recepcionConfirmada ) ) {
                $mensaje .= 'La recepción se encuentra disponible en el sistema.';
                if ( $recepcionConfirmada->getRelacion != null ) {
                    $mensaje .= '<br>Tiene match confirmado.';
                }
            } else {
                $recepcionSinConfirmar = HermesDocumento::with('getRelacion')
                                                        ->where('codigo_pedido', $codigoPedido)
                                                        ->first();

                if ( is_object( $recepcionSinConfirmar ) ) {
                    $mensaje .= 'La recepción se encuentra disponible en el sistema.';
                    if ( $recepcionSinConfirmar->getRelacion != null ) {
                        $mensaje .= '<br>Tiene match confirmado.';
                    }
                } else {

                    // Se busca en la base de datos de hermes
                    $recepcionHermes = \DB::connection('hermes')
                                            ->table('ACTA_RECEPCION')
                                            ->leftJoin('RESOLUCION', 'ACTA_RECEPCION.ID_RESOLUCION', '=', 'RESOLUCION.ID')
                                            ->leftJoin('PJURIDICA', 'RESOLUCION.ID_PROVEEDOR', '=', 'PJURIDICA.ID')
                                            ->leftJoin('PERSONA', 'PJURIDICA.ID_PERSONA', '=', 'PERSONA.ID')
                                            ->leftJoin('TIPO_DOCUMENTO', 'ACTA_RECEPCION.ID_TIPO_DOCUMENTO', '=', 'TIPO_DOCUMENTO.ID')
                                            
                                            ->where('ACTA_RECEPCION.CODIGO_PEDIDO', $codigoPedido )
                                            // ->where('ACTA_RECEPCION.ES_VALIDADO', 1)

                                            ->select(
                                                'ACTA_RECEPCION.*',
                                                'ACTA_RECEPCION.CODIGO_DOCUMENTO as FOLIO_DOCUMENTO',
                                                'PERSONA.RUT_PERSONA',
                                                'PERSONA.DIGITO_VERIFICADOR',
                                                'PJURIDICA.RAZON_SOCIAL',
                                                'TIPO_DOCUMENTO.NOMBRE as NOMBRE_DOCUMENTO',
                                                'TIPO_DOCUMENTO.CODIGO_SII as CODIGO_SII_DOCUMENTO',
                                                'RESOLUCION.NUMERO_RESOLUCION as RESOLUCION_CONTRATO'
                                            )
                                            ->first();

                    if( ! is_object($recepcionHermes) ) {

                        $mensaje .= 'No logramos encontrar la recepcion.';
                        $mensaje .= '<br>¿Has ingresado correctamente el Número Recepción?';
                        
                    } else {
                        if ( $recepcionHermes->ES_VALIDADO == 1 ) {
                            // Se guarda como wsDocumento
                            try {

                                $client = new Client();
                                $response = $client->request(
                                    'GET',
                                    'http://hsjd.logisticapp.cl/logisticaPublica/integracionFinanzas/integracionFinanzas.php?codigoRecepcion='.$recepcionHermes->CODIGO_PEDIDO.'&token=test_v1'
                                );
                
                                if ( $response->getStatusCode() === 200 ) {
                
                                    $contentResponse = $response->getBody()->getContents();
                                    $jsonResponse = json_decode($contentResponse);
                                    
                                    if ( $jsonResponse != null ) {
                
                                        $wsDocumento = WsDocumento::with(['getWsOrdenCompra.getWsContrato','getWsArchivos','getWsItemsPresupuestarios','getRelacion'])
                                                                ->where('tipo_documento', $jsonResponse->{'documento'}->tipo_documento)
                                                                ->where('documento', $jsonResponse->{'documento'}->documento)
                                                                ->where('documento_total', $jsonResponse->{'documento'}->documento_total)
                                                                ->whereHas('getWsOrdenCompra.getWsContrato',function ($query) use ($jsonResponse) {
                                                                        $query->where('rut_proveedor', $jsonResponse->{'contrato'}->rut_proveedor);
                                                                    })
                                                                ->first();
                
                                        
                                        if ( ! is_object($wsDocumento) ) {
                                            
                                            if ( $jsonResponse->{'contrato'}->nombre_proveedor == null || $jsonResponse->{'contrato'}->rut_proveedor == null ) {
                                                $mensaje .= 'Error en la información del contrato desde Hermes';
                                                $error = true;
                                            }
                                            
                                            if ( $jsonResponse->{'orden_compra'}->fecha_orden_compra == null ||
                                                 $jsonResponse->{'orden_compra'}->fecha_recepcion_orden_compra == null ||
                                                 $jsonResponse->{'orden_compra'}->id_orden_compra == null ||
                                                 $jsonResponse->{'orden_compra'}->numero_orden_compra == null ||
                                                 $jsonResponse->{'orden_compra'}->total_orden_compra == null ||
                                                 $jsonResponse->{'orden_compra'}->tipo_compra == null
                                               ) {
                                                $mensaje .= 'Error en la información de la orden de compra desde Hermes';
                                                $error = true;
                                            }
                
                                            if ( $jsonResponse->{'documento'}->documento == null ||
                                                 $jsonResponse->{'documento'}->tipo_documento == null ||
                                                 $jsonResponse->{'documento'}->nombre_usuario_responsable == null ||
                                                 $jsonResponse->{'documento'}->documento_descuento_total === null ||
                                                 $jsonResponse->{'documento'}->documento_neto === null ||
                                                 $jsonResponse->{'documento'}->documento_iva === null ||
                                                 $jsonResponse->{'documento'}->documento_total === null
                                               ) {
                                                $mensaje .= 'Error en la información del documento desde Hermes';
                                                $error = true;
                                            }
                
                                            $newContrato = new WsContrato( $jsonResponse->{'contrato'} );
                
                                            $newOrdenCompra = new WsOrdenCompra( $jsonResponse->{'orden_compra'}, $newContrato->id);
                
                                            $newDocumento = new WsDocumento( $jsonResponse->{'documento'}, $newOrdenCompra->id, $recepcionHermes->CODIGO_PEDIDO);
                
                                            foreach ( $jsonResponse->{'items_presupuestarios'} as $item ) {
                                                $newItem = new WsItemPresupuestario($item ,$newDocumento->id);
                                            }
                
                                            foreach ( $jsonResponse->{'archivos'} as $archivo) {
                                                $newArchivo = new WsArchivo($archivo, $newDocumento->id, $newOrdenCompra->id);
                                            }
                
                                            $mensaje .= 'La recepción se encuentra disponible en el sistema';
                
                                        } else {
                
                                            $mensaje .= 'La recepción se encuentra disponible en el sistema';
                                            if ( $wsDocumento->getRelacion != null ) {
                                                $mensaje .= '<br>Tiene match confirmado.';
                                            }
                                            
                                        }
                
                                    } else {
                                        $this->info("OBTENER RECEPCIONES | Respuesta null, codigo pedido:  {$recepcionHermes->CODIGO_PEDIDO}");
                                    }
                                    
                                } else {
                                    $mensaje .= 'Existen problemas con obtener la recepción desde hermes.';
                                    $mensaje .= '<br>Por favor intentalo más tarde';
                                    $mensaje .= 'Código Error: 001';
                                    $error = true;
                                }
                
                            } catch (RequestException $e) {
                                $mensaje .= 'Existen problemas con obtener la recepción desde hermes.';
                                $mensaje .= '<br>Por favor intentalo más tarde';
                                $mensaje .= 'Código Error: 002';
                                $error = true;
                            }

                        } else {
                            // Se guarda como HermesDocumento
                            $rut = Rut::parse( $recepcionHermes->RUT_PERSONA.''.$recepcionHermes->DIGITO_VERIFICADOR )->format(Rut::FORMAT_WITH_DASH);
                            $proveedor = Proveedor::where('rut', $rut )->first();
                            try {

                                $hermesDocumento = new HermesDocumento($recepcionHermes, is_object($proveedor) ? $proveedor->id : null, $rut);
                                $mensaje .= 'La recepción se encuentra disponible en el sistema.';

                            } catch (\Exception $e) {
                                $mensaje .= 'Existen problemas con obtener la recepción desde hermes.';
                                $mensaje .= '<br>Por favor intentalo más tarde';
                                $mensaje .= 'Código Error: 003';
                                $error = true;
                            }
                        }
                    }

                }

            }

            $datos = [
                'mensaje' => $mensaje,
                'error'   => $error
            ];

        }

        return response()->json($datos,200);
    }
}
