<?php namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests;
//use App\Helpers\JwtAuth;
use Auth;
use Excel;
use App\Traits\relacionRecepcionesNoValidadas;

use App\ArchivoAceptaExcel;
use App\ArchivoAcepta;
use App\Proveedor;
use App\TipoArchivo;
use App\Documento;
use App\Archivo;
use App\TipoDocumento;
use App\ModalidadCompra;
use App\TipoAdjudicacion;
use App\TipoInforme;
use App\User;
use App\ReferenteTecnico;
use App\Devengo;
use App\WsDocumento;
use App\RelacionDocumentoWsDocumento;
use App\MotivoRechazo;
use App\AceptaMotivoRechazo;
use App\ItemPresupuestario;
use App\DatoGeneral;
use App\Role;
use App\MotivoRezagar;
use App\AceptaMotivoRezagar;
use App\HermesDocumento;
use App\RelacionDocumentoHermesDocumento;
use App\ArchivoAceptaRevisionBodega;
use App\Contrato;
use App\OrdenCompra;

// Para guardar archivos
use Storage;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\File;
use Mpdf\Mpdf;
use Carbon\Carbon;


use GuzzleHttp\Client;
use GuzzleHttp\Exception\RequestException;
use App\WsContrato;
use App\WsOrdenCompra;
// use App\WsDocumento;
use App\WsArchivo;
use App\WsItemPresupuestario;


class ArchivoAceptaController extends Controller
{

    public function cambiarRecepciones()
    {
        set_time_limit(0);
        ini_set('max_execution_time', 0);

        dd('Obtener recepciones no validadas del servidor SIFCON');
        try {

            $client = new Client();
            $response = $client->request(
                'GET',
                // 'http://18.234.47.233/api/recepciones_no_validadas'
                'http://127.0.0.1:8000/api/recepciones_no_validadas'
            );

            if ( $response->getStatusCode() === 200 ) {

                $statusCode = $response->getStatusCode();
                $contentResponse = $response->getBody()->getContents();
                $jsonResponse = json_decode($contentResponse);
                // dd(
                //     $jsonResponse, $contentResponse
                // );
 
                
                if ( $jsonResponse != null ) {

                    foreach ( $jsonResponse as $params ) {

                        $hermesDocumento = HermesDocumento::where('rut_proveedor', $params->rut_proveedor)
                                                          ->where('folio_documento', $params->FOLIO_DOCUMENTO)
                                                          ->where('tipo_documento', $params->NOMBRE_DOCUMENTO)
                                                          ->where('documento_total', $params->TOTAL)
                                                          ->where('codigo_pedido', $params->CODIGO_PEDIDO)
                                                          ->first();

                        if ( is_object($hermesDocumento) ) {
                            echo "<br>Actualizar hermes documento doc: ". $params->FOLIO_DOCUMENTO;
                            
                            $hermesDocumento->rut_proveedor = $params->rut_proveedor;
                            $hermesDocumento->codigo_pedido = $params->CODIGO_PEDIDO;
                            $hermesDocumento->save();
                        
                        } else {
                            echo "<br>Crear hermes documento : ". $params->CODIGO_PEDIDO;
                            $proveedor = Proveedor::where('rut', $params->rut_proveedor )->first();
                            $hermesDocumento = new HermesDocumento($params, is_object($proveedor) ? $proveedor->id : null, $params->rut_proveedor);
                        }

                    }

                    dd(
                        $jsonResponse
                    );


                } else {
                    echo "<br>";
                    echo "Respuesta NULA ";

                }
                
            } else {
                echo "<br>";
                echo "Respuesta diferente a 200 del servidor ". $response->getStatusCode();
            }

        } catch (RequestException $e) {
            echo '<br>';
            dd('Problema con consumir webservices de recepción no validadas: ', $e);
        }

        dd('Funcion para obtener todas las recepciones de SIFCON server');


        $obtener = true;
        while ( $obtener != false ) {
            try {

                $client = new Client();
                $response = $client->request(
                    'GET',
                    'http://18.234.47.233/api/recepciones'
                );
                echo $response->getStatusCode();

                if ( $response->getStatusCode() === 200 ) {

                    $statusCode = $response->getStatusCode();
                    $contentResponse = $response->getBody()->getContents();

                    $jsonResponse = json_decode($contentResponse);
                    
                    // dd($jsonResponse, $jsonResponse->{'contrato'}, $jsonResponse->{'contrato'}->nombre_proveedor, $jsonResponse->enviar);
                    
                    if ( $jsonResponse != null && $jsonResponse->enviar ) {

                        $wsDocumento = WsDocumento::with(['getWsOrdenCompra.getWsContrato','getWsArchivos','getWsItemsPresupuestarios'])
                                            ->where('tipo_documento', $jsonResponse->{'documento'}->tipo_documento)
                                            ->where('documento', $jsonResponse->{'documento'}->documento)
                                            ->where('documento_total', $jsonResponse->{'documento'}->documento_total)
                                            ->whereHas('getWsOrdenCompra.getWsContrato',function ($query) use ($jsonResponse) {
                                                    $query->where('rut_proveedor', $jsonResponse->{'contrato'}->rut_proveedor);
                                                })
                                            ->first();

                        
                        if ( ! is_object($wsDocumento) ) {
                            
                            if ( $jsonResponse->{'contrato'}->nombre_proveedor == null || $jsonResponse->{'contrato'}->rut_proveedor == null ) {
                                var_dump($jsonResponse->{'contrato'});
                                continue;
                            }
                            
                            if ( $jsonResponse->{'orden_compra'}->fecha_orden_compra == null ||
                                 $jsonResponse->{'orden_compra'}->fecha_recepcion_orden_compra == null ||
                                 $jsonResponse->{'orden_compra'}->id_orden_compra == null ||
                                 $jsonResponse->{'orden_compra'}->numero_orden_compra == null ||
                                 $jsonResponse->{'orden_compra'}->total_orden_compra == null ||
                                 $jsonResponse->{'orden_compra'}->tipo_compra == null
                               ) {
                                var_dump($jsonResponse->{'orden_compra'});
                                continue;
                            }

                            if ( $jsonResponse->{'documento'}->documento == null ||
                                 $jsonResponse->{'documento'}->tipo_documento == null ||
                                 $jsonResponse->{'documento'}->nombre_usuario_responsable == null ||
                                 $jsonResponse->{'documento'}->documento_descuento_total === null ||
                                 $jsonResponse->{'documento'}->documento_neto === null ||
                                 $jsonResponse->{'documento'}->documento_iva === null ||
                                 $jsonResponse->{'documento'}->documento_total === null
                               ) {
                                var_dump($jsonResponse->{'documento'});
                                continue;
                            }

                            $newContrato = new WsContrato( $jsonResponse->{'contrato'} );

                            $newOrdenCompra = new WsOrdenCompra( $jsonResponse->{'orden_compra'}, $newContrato->id);

                            $newDocumento = new WsDocumento( $jsonResponse->{'documento'}, $newOrdenCompra->id);

                            foreach ( $jsonResponse->{'items_presupuestarios'} as $item ) {
                                $newItem = new WsItemPresupuestario($item ,$newDocumento->id);
                            }

                            foreach ( $jsonResponse->{'archivos'} as $archivo) {
                                $newArchivo = new WsArchivo($archivo, $newDocumento->id, $newOrdenCompra->id);
                            }

                            echo "<br>";
                            echo "Se creo el newDocumento, id: ".$newDocumento->id." | ".$newDocumento->documento." | ".$newDocumento->tipo_documento;

                        } else {
                            echo "<br>";
                            echo "Esta el wsDocumento, id: ".$wsDocumento->id." | ".$wsDocumento->documento." | ".$wsDocumento->tipo_documento;
                        }

                    } else {
                        $obtener = $jsonResponse == null || $jsonResponse->cantidad_recepciones < 2 ? false : true;
                        echo "<br>";
                        echo "Respuesta NULA ";

                    }
                    
                } else {
                    $obtener = false;
                }

            } catch (RequestException $e) {
                echo '<br>';
                dd('Problema con consumir webservices de recepción: ', $e);
            }
        }


        dd('Funcion para obtener todas las recepciones de hermes');

        $recepcionesSifcon = WsDocumento::all();

        $recepcionesHermes = \DB::connection('hermes')
                                ->table('ACTA_RECEPCION')
                                ->leftJoin('RESOLUCION', 'ACTA_RECEPCION.ID_RESOLUCION', '=', 'RESOLUCION.ID')
                                ->leftJoin('PJURIDICA', 'RESOLUCION.ID_PROVEEDOR', '=', 'PJURIDICA.ID')
                                ->leftJoin('PERSONA', 'PJURIDICA.ID_PERSONA', '=', 'PERSONA.ID')
                                ->leftJoin('TIPO_DOCUMENTO', 'ACTA_RECEPCION.ID_TIPO_DOCUMENTO', '=', 'TIPO_DOCUMENTO.ID')
                                
                                // ->where('PERSONA.RUT_PERSONA', '=', explode('-', $archivoAcepta->getProveedor->rut)[0])
                                // ->where('PERSONA.DIGITO_VERIFICADOR', '=', explode('-', $archivoAcepta->getProveedor->rut)[1])
                                // no considerar las recepciones en el sistema, segun el proveedor.
                                ->whereNotIn('ACTA_RECEPCION.CODIGO_DOCUMENTO', $recepcionesSifcon->pluck('documento')->toArray() )
                                ->where('ACTA_RECEPCION.ES_VALIDADO', 1)
                                ->whereYear( 'ACTA_RECEPCION.FECHA_INGRESO','>=', 2019 )
                                ->select(
                                    'ACTA_RECEPCION.*',
                                    'ACTA_RECEPCION.CODIGO_DOCUMENTO as FOLIO_DOCUMENTO',
                                    'PERSONA.RUT_PERSONA',
                                    'PERSONA.DIGITO_VERIFICADOR',
                                    'PJURIDICA.RAZON_SOCIAL',
                                    'TIPO_DOCUMENTO.NOMBRE as NOMBRE_DOCUMENTO',
                                    'TIPO_DOCUMENTO.CODIGO_SII as CODIGO_SII_DOCUMENTO'
                                )
                                ->get();

        echo '<br>Recepciones encontratas: '.$recepcionesHermes->count();
        foreach ( $recepcionesHermes as $recepcionHermes ) {
            
            // $this->info("Se encuentra la recepcion en hermes :  {$recepcionHermes->CODIGO_PEDIDO}");
            echo 'Se encuentra la recepcion en hermes: '.$recepcionHermes->CODIGO_PEDIDO;
            echo '<br>';
            try {

                $client = new Client();
                $response = $client->request(
                    'GET',
                    'http://hsjd.logisticapp.cl/logisticaPublica/integracionFinanzas/integracionFinanzas.php?codigoRecepcion='.$recepcionHermes->CODIGO_PEDIDO.'&token=test_v1'
                );
                echo $response->getStatusCode();

                if ( $response->getStatusCode() === 200 ) {
                    // dd(
                    //     $hermesDoc,
                    //     $hermesDoc->getProveedor,
                    //     $hermesDoc->getRelacion,
                    //     $hermesDoc->getRelacion->getDocumento
                    // );
                    $statusCode = $response->getStatusCode();
                    $contentResponse = $response->getBody()->getContents();
                    // echo $contentResponse;
                    // echo '<br>';
                    $jsonResponse = json_decode($contentResponse);
                    
                    // dd($jsonResponse, $jsonResponse->{'contrato'}, $jsonResponse->{'contrato'}->nombre_proveedor);
                    // dd($jsonResponse, $response->getBody()->getContents());
                    if ( $jsonResponse != null ) {

                        $wsDocumento = WsDocumento::with(['getWsOrdenCompra.getWsContrato','getWsArchivos','getWsItemsPresupuestarios'])
                                            ->where('tipo_documento', $jsonResponse->{'documento'}->tipo_documento)
                                            ->where('documento', $jsonResponse->{'documento'}->documento)
                                            ->where('documento_total', $jsonResponse->{'documento'}->documento_total)
                                            ->whereHas('getWsOrdenCompra.getWsContrato',function ($query) use ($jsonResponse) {
                                                    $query->where('rut_proveedor', $jsonResponse->{'contrato'}->rut_proveedor);
                                                })
                                            ->first();

                        
                        if ( ! is_object($wsDocumento) ) {

                            // dd( $jsonResponse->{'orden_compra'}, $jsonResponse->{'orden_compra'}->fecha_orden_compra );
                            
                            if ( $jsonResponse->{'contrato'}->nombre_proveedor == null || $jsonResponse->{'contrato'}->rut_proveedor == null ) {
                                var_dump($jsonResponse->{'contrato'});
                                continue;
                            }
                            
                            if ( $jsonResponse->{'orden_compra'}->fecha_orden_compra == null ||
                                 $jsonResponse->{'orden_compra'}->fecha_recepcion_orden_compra == null ||
                                 $jsonResponse->{'orden_compra'}->id_orden_compra == null ||
                                 $jsonResponse->{'orden_compra'}->numero_orden_compra == null ||
                                 $jsonResponse->{'orden_compra'}->total_orden_compra == null ||
                                 $jsonResponse->{'orden_compra'}->tipo_compra == null
                               ) {
                                var_dump($jsonResponse->{'orden_compra'});
                                continue;
                            }

                            if ( $jsonResponse->{'documento'}->documento == null ||
                                 $jsonResponse->{'documento'}->tipo_documento == null ||
                                 $jsonResponse->{'documento'}->nombre_usuario_responsable == null ||
                                 $jsonResponse->{'documento'}->documento_descuento_total === null ||
                                 $jsonResponse->{'documento'}->documento_neto === null ||
                                 $jsonResponse->{'documento'}->documento_iva === null ||
                                 $jsonResponse->{'documento'}->documento_total === null
                               ) {
                                var_dump($jsonResponse->{'documento'});
                                continue;
                            }

                            $newContrato = new WsContrato( $jsonResponse->{'contrato'} );

                            $newOrdenCompra = new WsOrdenCompra( $jsonResponse->{'orden_compra'}, $newContrato->id);

                            $newDocumento = new WsDocumento( $jsonResponse->{'documento'}, $newOrdenCompra->id);

                            foreach ( $jsonResponse->{'items_presupuestarios'} as $item ) {
                                $newItem = new WsItemPresupuestario($item ,$newDocumento->id);
                            }

                            foreach ( $jsonResponse->{'archivos'} as $archivo) {
                                $newArchivo = new WsArchivo($archivo, $newDocumento->id, $newOrdenCompra->id);
                            }

                            // dd(
                            //     $jsonResponse, 
                            //     $jsonResponse->{'contrato'}, 
                            //     $jsonResponse->{'contrato'}->nombre_proveedor,
                            //     $newArchivo,
                            //     $newDocumento,
                            //     $recepcionHermes
                            // );
                            echo "<br>";
                            echo "Se creo el newDocumento, id: ".$newDocumento->id." | ".$newDocumento->documento." | ".$newDocumento->tipo_documento;

                        } else {
                            echo "<br>";
                            echo "Esta el wsDocumento, id: ".$wsDocumento->id." | ".$wsDocumento->documento." | ".$wsDocumento->tipo_documento;
                        }

                    } else {

                        echo "<br>";
                        echo "Respuesta NULA ";

                    }
                    
                }
                
                

            } catch (RequestException $e) {
                echo '<br>';
                echo 'Problema con consumir webservices de recepción: '.$recepcionHermes->CODIGO_PEDIDO;
            }
                
        }

        // dd('cantidad de recepciones validadas y no ingresadas en el sistema', $recepcionesHermes->count());

        dd('test para obtener las recepciones desde hermes');

        $hermesDocumento = HermesDocumento::with(['getRelacion', 'getProveedor'])->get()->take(2);
        // $this->info("Cantidad de hermes documentos :  {$hermesDocumento->count()}");

        foreach ( $hermesDocumento as $hermesDoc ) {

            $recepcionHermes = \DB::connection('hermes')
                                    ->table('ACTA_RECEPCION')
                                    ->leftJoin('RESOLUCION', 'ACTA_RECEPCION.ID_RESOLUCION', '=', 'RESOLUCION.ID')
                                    ->leftJoin('PJURIDICA', 'RESOLUCION.ID_PROVEEDOR', '=', 'PJURIDICA.ID')
                                    ->leftJoin('PERSONA', 'PJURIDICA.ID_PERSONA', '=', 'PERSONA.ID')
                                    ->where('ACTA_RECEPCION.CODIGO_DOCUMENTO', '=', $hermesDoc->folio_documento)
                                    ->where('PERSONA.RUT_PERSONA', '=', explode('-',$hermesDoc->getProveedor->rut)[0])
                                    ->where('PERSONA.DIGITO_VERIFICADOR', '=', explode('-',$hermesDoc->getProveedor->rut)[1])
                                    ->where('ACTA_RECEPCION.ES_VALIDADO', 1)
                                    ->select(
                                        'ACTA_RECEPCION.CODIGO_DOCUMENTO',
                                        'ACTA_RECEPCION.FECHA_INGRESO',
                                        'ACTA_RECEPCION.NETO',
                                        'ACTA_RECEPCION.DESCUENTO',
                                        'ACTA_RECEPCION.TOTAL',
                                        'ACTA_RECEPCION.CODIGO_PEDIDO',
                                        'PERSONA.RUT_PERSONA',
                                        'PERSONA.DIGITO_VERIFICADOR',
                                        'PJURIDICA.RAZON_SOCIAL'
                                    )
                                    ->first();
            if ( is_object($recepcionHermes) ) {
                // $this->info("Se encuentra la recepcion en hermes :  {$recepcionHermes->CODIGO_PEDIDO}");
                echo 'Se encuentra la recepcion en hermes: '.$recepcionHermes->CODIGO_PEDIDO;
                echo '<br>';
                try {

                    $client = new Client();
                    $response = $client->request(
                        'GET',
                        'http://hsjd.logisticapp.cl/logisticaPublica/integracionFinanzas/integracionFinanzas.php?codigoRecepcion='.$recepcionHermes->CODIGO_PEDIDO.'&token=test_v1'
                    );
                    echo $response->getStatusCode();

                    if ( $response->getStatusCode() === 200 ) {
                        // dd(
                        //     $hermesDoc,
                        //     $hermesDoc->getProveedor,
                        //     $hermesDoc->getRelacion,
                        //     $hermesDoc->getRelacion->getDocumento
                        // );
                        echo '<br>';
                        $statusCode = $response->getStatusCode();
                        // $this->info("StatusCode :  {$statusCode}");
                        // echo $response;
                        echo '<br>';

                        $contentResponse = $response->getBody()->getContents();
                        echo $contentResponse;
                        echo '<br>';
                        $jsonResponse = json_decode($contentResponse);
                        
                        // dd($jsonResponse, $jsonResponse->{'contrato'}, $jsonResponse->{'contrato'}->nombre_proveedor);

                        $newContrato = new WsContrato( $jsonResponse->{'contrato'} );

                        $newOrdenCompra = new WsOrdenCompra( $jsonResponse->{'orden_compra'}, $newContrato->id);

                        $newDocumento = new WsDocumento( $jsonResponse->{'documento'}, $newOrdenCompra->id);

                        foreach ( $jsonResponse->{'items_presupuestarios'} as $item ) {
                            $newItem = new WsItemPresupuestario($item ,$newDocumento->id);
                        }

                        foreach ( $jsonResponse->{'archivos'} as $archivo) {
                            $newArchivo = new WsArchivo($archivo, $newDocumento->id, $newOrdenCompra->id);
                        }

                        dd(
                            $jsonResponse, 
                            $jsonResponse->{'contrato'}, 
                            $jsonResponse->{'contrato'}->nombre_proveedor,
                            $newArchivo,
                            $newDocumento,
                            $hermesDoc,
                            $hermesDoc->getRelacion,
                            $hermesDoc->getRelacion->getDocumento
                        );

                    }
                    
                    

                } catch (RequestException $e) {
                    echo '<br>';
                    echo 'Problema con consumir webservices de recepción: '.$recepcionHermes->CODIGO_PEDIDO;
                }
                
            }                                        
            
        }


    }

    public function recepcionesHermes()
    {
        set_time_limit(0);
        ini_set('max_execution_time', 0);

        /**
         * Para cambiar el match con hermes, por el del sistema
         */
        $recepciones = \DB::table('hermes_documentos')
                    ->join('ws_documento','hermes_documentos.folio_documento','=','ws_documento.documento')

                    ->whereColumn('hermes_documentos.tipo_documento','LIKE','ws_documento.tipo_documento')
                    ->whereColumn('hermes_documentos.folio_documento','=','ws_documento.documento')
                    ->whereColumn('hermes_documentos.documento_total','=','ws_documento.documento_total')

                    ->where('ws_documento.id_relacion', null)

                    ->select(
                        'hermes_documentos.id as id_hermes_documento',
                        'hermes_documentos.folio_documento',
                        'hermes_documentos.documento_total as total_hermes',
                        'ws_documento.id as id_ws_documento',
                        'ws_documento.tipo_documento as ws_tipo_documento',
                        'ws_documento.documento as ws_numero_documento',
                        'ws_documento.documento_total'
                    )
                    ->get();

        // dd($recepciones);

        foreach ( $recepciones as $recepcion ) {

            $hermesDocumento = HermesDocumento::with(['getRelacion'])->findOrFail( $recepcion->id_hermes_documento );
            $documento = Documento::findOrFail( $hermesDocumento->getRelacion->id_documento );
            $wsDocumento = WsDocumento::findOrFail( $recepcion->id_ws_documento );

            dd($hermesDocumento, $documento, $wsDocumento);

            // Se crea la relacion
            $relacion = new RelacionDocumentoWsDocumento();
            $relacion->id_documento = $documento->id;
            $relacion->id_ws_documento = $wsDocumento->id;
            $relacion->id_user_relacion = NULL;
            $relacion->updated_at = NULL;
            $relacion->save();

            // Se paasa el id correspondiente a cada documento para la conexion
            $documento->timestamps = false;
            $documento->id_relacion = $relacion->id;
            $documento->save();
            $documento->validar();
            $documento->actualizaValidacionDocumentosRelacionados();

            $wsDocumento->timestamps = false;
            $wsDocumento->id_relacion = $relacion->id;
            $wsDocumento->save();

            $hermesDocumento->getRelacion->delete();
            $hermesDocumento->delete();
            
        }

        /**
         * Para obtener las recepciones de hermes
         */
        // $archivoAcepta = ArchivoAcepta::where('id', 19662)
        //                                 ->first();

        // $query = \DB::connection('hermes')
        //             ->table('ACTA_RECEPCION')
        //             ->leftJoin('RESOLUCION', 'ACTA_RECEPCION.ID_RESOLUCION', '=', 'RESOLUCION.ID')
        //             ->leftJoin('PJURIDICA', 'RESOLUCION.ID_PROVEEDOR', '=', 'PJURIDICA.ID')
        //             ->leftJoin('PERSONA', 'PJURIDICA.ID_PERSONA', '=', 'PERSONA.ID')
        //             // ->where('ACTA_RECEPCION.CODIGO_DOCUMENTO', '=', $archivoAcepta->folio)
        //             ->leftJoin('TIPO_DOCUMENTO', 'ACTA_RECEPCION.ID_TIPO_DOCUMENTO', '=', 'TIPO_DOCUMENTO.ID')

        //             ->where('PERSONA.RUT_PERSONA', '=', explode('-', $archivoAcepta->getProveedor->rut)[0])
        //             ->where('PERSONA.DIGITO_VERIFICADOR', '=', explode('-', $archivoAcepta->getProveedor->rut)[1])
        //             // ->where('ACTA_RECEPCION.TOTAL', '=', $archivoAcepta->monto_total)
        //             ->select(
        //                 // 'ACTA_RECEPCION.CODIGO_DOCUMENTO',
        //                 // 'ACTA_RECEPCION.FECHA_INGRESO',
        //                 // 'ACTA_RECEPCION.NETO',
        //                 // 'ACTA_RECEPCION.DESCUENTO',
        //                 // 'ACTA_RECEPCION.TOTAL',
        //                 'ACTA_RECEPCION.*',
        //                 'ACTA_RECEPCION.CODIGO_DOCUMENTO as FOLIO_DOCUMENTO',
        //                 'PERSONA.RUT_PERSONA',
        //                 'PERSONA.DIGITO_VERIFICADOR',
        //                 'PJURIDICA.RAZON_SOCIAL',
        //                 'TIPO_DOCUMENTO.NOMBRE as NOMBRE_DOCUMENTO',
        //                 'TIPO_DOCUMENTO.CODIGO_SII as CODIGO_SII_DOCUMENTO'
        //             )
        //             ->get();

        // $query2 = \DB::connection('hermes')
        //             ->table('TIPO_DOCUMENTO')
        //             ->get();

        // dd($archivoAcepta, $query, $query2);
    }

    public function testAcepta()
    {
        set_time_limit(0);
        ini_set('max_execution_time', 0);

        $archivosAcepta = ArchivoAcepta::where('cargado', 0)
                                       ->where('id_tipo_documento', null)
                                       ->where('id_proveedor', null)
                                       ->get();

        $contadorProveedoresNuevos = 0;
        $contadorArchivosCargados = 0;
        $contadorArchivosAceptaModificados = 0;

        foreach ( $archivosAcepta as $archivo ) {

            $id_tipo_documento = 0;
            switch ($archivo->tipo) {
                case 33:
                    $id_tipo_documento = 1;
                    break;
                case 34:
                    $id_tipo_documento = 1;
                    break;
                case 39:
                    $id_tipo_documento = 7;
                    break;
                case 46:
                    $id_tipo_documento = 7;
                    break;
                case 56:
                    $id_tipo_documento = 5;
                    break;
                case 61:
                    $id_tipo_documento = 4;
                    break;
                default:
                    $id_tipo_documento = 0;
                    break;
            }

            $proveedor = Proveedor::where('rut', trim( str_replace( ".", "", $archivo->emisor ) ) )
                                  ->first();

            if ( ! is_object($proveedor) ) {

                $proveedor = new Proveedor;
                $proveedor->rut = trim( str_replace( ".", "", $archivo->emisor ) );
                $proveedor->nombre = $archivo->razon_social_emisor;
                $proveedor->activo = 1;
                $proveedor->save();
                $contadorProveedoresNuevos++;

            }

            // dd($proveedor,$id_tipo_documento, $archivo->emisor, $archivo->razon_social_emisor, $archivo->tipo, $archivosAcepta->count() );

            $archivo->id_proveedor = $proveedor->id;
            $archivo->save();

            if ( $id_tipo_documento != 0 ) {

                $archivo->id_tipo_documento = $id_tipo_documento;
                $archivo->save();

                $contadorArchivosAceptaModificados++;

                $documento = Documento::where('id_proveedor', $proveedor->id)
                                      ->where('numero_documento', $archivo->folio ) 
                                      ->where('id_tipo_documento', $id_tipo_documento)
                                      ->first();
                        
                if ( is_object($documento) ){

                    $archivo->cargado = 1;
                    $archivo->save();
                    $contadorArchivosCargados++;

                }
                
            }

        }

        echo "Proveedores nuevos : ".$contadorProveedoresNuevos." | Archivos Cargados : ".$contadorArchivosCargados.
             " | Archivos Modificados : ".$contadorArchivosAceptaModificados;

        // $archivosAcepta = ArchivoAcepta::where('cargado', 0)->where('reclamado', 1)
        //                                ->where('id_user_rechazo', null)->get();
        // dd($archivosAcepta);

        // $archivosAcepta = \DB::table('archivos_acepta')
        //                 ->join('proveedores','archivos_acepta.id_proveedor','=','proveedores.id')
        //                 ->join('tipos_documento','archivos_acepta.id_tipo_documento','tipos_documento.id')
        //                 ->join('ws_contrato','proveedores.rut','=','ws_contrato.rut_proveedor')
        //                 ->join('ws_orden_compra','ws_contrato.id','=','ws_orden_compra.id_contrato')
        //                 ->join('ws_documento','ws_orden_compra.id','=','ws_documento.id_orden_compra')

        //                 ->whereColumn('tipos_documento.nombre','LIKE','ws_documento.tipo_documento')
        //                 ->whereColumn('archivos_acepta.folio','=','ws_documento.documento')
        //                 ->whereColumn('archivos_acepta.monto_total','!=','ws_documento.documento_total')
        //                 // ->where('archivos_acepta.id_relacion',null)
        //                 ->where('ws_documento.id_relacion',null)
        //                 ->where('archivos_acepta.cargado',0)
        //                 ->where('archivos_acepta.id_user_rechazo', null)
        //                 // ->take(5)
        //                 ->get();

        // dd( $archivosAcepta->count(), $archivosAcepta );
    }

    public function contadorRecepciones()
    {
        $archivosAceptaConRecepcion = \DB::table('archivos_acepta')
                                          ->join('proveedores','archivos_acepta.id_proveedor','=','proveedores.id')
                                          ->join('tipos_documento','archivos_acepta.id_tipo_documento','tipos_documento.id')
                                          ->join('ws_contrato','proveedores.rut','=','ws_contrato.rut_proveedor')
                                          ->join('ws_orden_compra','ws_contrato.id','=','ws_orden_compra.id_contrato')
                                          ->join('ws_documento','ws_orden_compra.id','=','ws_documento.id_orden_compra')

                                          ->whereColumn('tipos_documento.nombre','LIKE','ws_documento.tipo_documento')
                                          ->whereColumn('archivos_acepta.folio','=','ws_documento.documento')
                                          ->whereColumn('archivos_acepta.monto_total','=','ws_documento.documento_total')
                                          
                                          ->where('ws_documento.id_relacion',null)
                                          ->where('archivos_acepta.cargado',0)
                                          ->where('archivos_acepta.id_user_rechazo', null)

                                          ->select(
                                              'archivos_acepta.id'
                                            )
                                          ->get();

        $arregloDocumentosId = $archivosAceptaConRecepcion->pluck('id')->toArray();

        $archivosAcepta = ArchivoAcepta::with(['getProveedor','getTipoDocumento'])
                                       ->where('cargado',0)
                                       ->where('id_user_rechazo',null)
                                       ->whereNotIn('id',$arregloDocumentosId)
                                       ->get();


        $contador = 0;

        foreach ( $archivosAcepta as $archivoAcepta ) {
            // $recepcionHermes = \DB::connection('hermes')
            //                     ->table('ACTA_RECEPCION')
            //                     ->leftJoin('RESOLUCION', 'ACTA_RECEPCION.ID_RESOLUCION', '=', 'RESOLUCION.ID')
            //                     ->leftJoin('PJURIDICA', 'RESOLUCION.ID_PROVEEDOR', '=', 'PJURIDICA.ID')
            //                     ->leftJoin('PERSONA', 'PJURIDICA.ID_PERSONA', '=', 'PERSONA.ID')
            //                     ->where('ACTA_RECEPCION.CODIGO_DOCUMENTO', '=', $archivoAcepta->folio)
            //                     ->where('PERSONA.RUT_PERSONA', '=', explode('-',$archivoAcepta->getProveedor->rut)[0])
            //                     ->where('PERSONA.DIGITO_VERIFICADOR', '=', explode('-',$archivoAcepta->getProveedor->rut)[1])
            //                     ->select(
            //                         'ACTA_RECEPCION.CODIGO_DOCUMENTO',
            //                         'ACTA_RECEPCION.FECHA_INGRESO',
            //                         'ACTA_RECEPCION.NETO',
            //                         'ACTA_RECEPCION.DESCUENTO',
            //                         'ACTA_RECEPCION.TOTAL',
            //                         'PERSONA.RUT_PERSONA',
            //                         'PERSONA.DIGITO_VERIFICADOR',
            //                         'PJURIDICA.RAZON_SOCIAL'
            //                     )
            //                     ->first();
            $recepcionHermes = null;

            if ( is_object($recepcionHermes) ) {
                $contador++;
            }
        }

        return "Cantidad sin recepcion : {$contador}";

    }

    /**
     * Create a new controller instance.
     *
     * @return void
    */
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index() {
        if (!\Entrust::can(['ver-general-excel-acepta','ver-excel-acepta'])) {
            return \Redirect::to('home');
        }
        $archivosAceptaExcel = ArchivoAceptaExcel::all();
        return view('archivo_acepta.index')->with('archivosAcepta',$archivosAceptaExcel);
    }

    public function postCargaArchivo(Request $request) {
        //sleep(5);
        $archivo = $request->file('archivo');
        
        if ($archivo == null) {
            
            $datos = array(
                'mensaje' => 'No existe archivo',
                'estado' => 'error',   
            );

            return response()->json($datos,200);
        } else {

            $nombreOriginalExcel = str_replace('.xls','',$archivo->getClientOriginalName() );
            $arreglo_nombre = explode(".", $archivo->getClientOriginalName() );
            $extension = strtolower( end( $arreglo_nombre ) );

            if ($extension != 'xls') {
                $datos = array(
                    'mensaje' => 'Extension incorrecta del archivo',
                    'estado' => 'error',
                );
    
                return response()->json($datos,200);
            }

            // Se trabajan las rutas del excel, se guarda en el proyecto al final de la funcion
            $nombre = $nombreOriginalExcel.'_A_'.date('Y_m_d_G_i_s');
            $rutaExcel = '/carga/'.date('Y-m-d').'/'.$nombre.'.xls';
            $pathExcel = public_path().'/carga/'.date('Y-m-d').'/';

            // 1° Crear archivo acepta excel.
            $newArchivoAceptaExcel = new ArchivoAceptaExcel();
            $newArchivoAceptaExcel->id_usuario = Auth::user()->id;
            //el nombre esta compuesto por = nombreOriginalExcel(sin extension)_A_año_mes_dia_hora_minutos_segundos.
            $newArchivoAceptaExcel->nombre = $nombre;
            $newArchivoAceptaExcel->nombre_original = $nombreOriginalExcel;
            $newArchivoAceptaExcel->ubicacion = $rutaExcel;
            $newArchivoAceptaExcel->extension = $extension;
            $newArchivoAceptaExcel->peso = $archivo->getSize();
            $newArchivoAceptaExcel->save();

            $excel = Excel::selectSheetsByIndex(0)->load($archivo)->get();
            
            // 2° se recorre el excel, por fila. Se crean archivos acepta por cada fila recorrida
            foreach ($excel as $key => $fila) {
                
                // Se trabaja la tabla de factura, se guardan distintos documentos
                $id_tipo_documento = 0;
                switch ($fila->tipo) {
                    case 33: // factura
                        $id_tipo_documento = 1;
                        break;
                    case 34: // factura
                        $id_tipo_documento = 1;
                        break;
                    case 39: // Boleta venta y servicio
                        $id_tipo_documento = 7;
                        break;
                    case 46: // boleta venta y servicio
                        $id_tipo_documento = 7;
                        break;
                    case 56: // nota de debito
                        $id_tipo_documento = 5;
                        break;
                    case 61: // Nota de credito
                        $id_tipo_documento = 4;
                        break;
                    default:
                        $id_tipo_documento = 0;
                        break;
                }

                // se obtiene el proveedor;
                $proveedor = Proveedor::where('rut', trim( str_replace( ".", "", $fila->emisor ) ) )
                                      ->first();  
                                    
                if ( ! is_object($proveedor) ) {

                    $proveedor = new Proveedor;
                    $proveedor->rut = trim( str_replace( ".", "", $fila->emisor ) );
                    $proveedor->nombre = trim($fila->razon_social_emisor);
                    $proveedor->activo = 1;
                    $proveedor->save();
    
                }

                if ( is_object($proveedor) && $id_tipo_documento != 0 ) {

                    $documento = Documento::where('id_proveedor', $proveedor->id)
                                          ->where('numero_documento', trim($fila->folio) ) 
                                          ->where('id_tipo_documento', $id_tipo_documento)
                                          ->first();

                    /** Se verifica si el documento se encuentra en la base de datos
                     * Si se encuentra en la base de datos: 
                     *  - no se guarda el archivo acepta.
                     *  - se busca si tiene el archivo (PDF), de no ser así se extrae de acepta
                     * Si no se encuentra en la base de datos:
                     *  - se guarda el archivo acepta
                     *  */ 
                    
                    if ( is_object($documento) ) {

                        $archivoDocumento = Archivo::where('id_documento', $documento->id)
                                                    ->where('id_tipo_archivo', 1)
                                                    ->first();
                        
                        if ( !is_object($archivoDocumento) ) {
                            // se deben generar 'string' para la ubicacion del archivo
                            $newArchivo = new Archivo();
                            $newArchivo->id_documento = $documento->id;
                            $newArchivo->id_tipo_archivo = 1;
                            $newArchivo->nombre = 'archivoAcepta_'.date('Y_m_d_G_i_s');
                            $newArchivo->nombre_original = $newArchivo->nombre;
                            $newArchivo->ubicacion = "documento/".$documento->id."/".$newArchivo->nombre.".pdf";
                            $newArchivo->extension = "pdf";
                            $newArchivo->peso = 0;
                            $newArchivo->save();

                            // Para guardar el pdf en el proyecto, faltan pruebas y arreglar el path
                            
                            $content = file_get_contents('http://windte1806.acepta.com/ca4webv3/PdfViewMedia?url='.$fila->uri.'&menuTitle=Papel%2520Carta');
                            if ( !file_exists(public_path().'/documento/'.$documento->id) ) {
                                mkdir(public_path().'/documento/'.$documento->id, 0775, true);
                                chmod(public_path().'/documento/'.$documento->id, 0775);
                            }
                            $file = fopen( public_path().'/documento/'.$documento->id.'/'.$newArchivo->nombre.'.pdf', 'a');
                            fwrite($file, $content);
                            fclose($file);
                            
                        }

                    } else {
                        // Se valida que el archivo (de la fila del excel) no se encuentra en la base de datos, en la tabla archivo acepta.
                        $archivoAcepta = ArchivoAcepta::where('tipo',$fila->tipo)
                                                      ->where('folio',$fila->folio)
                                                      ->where('emisor',$fila->emisor)
                                                      ->first();

                        if ( !is_object($archivoAcepta) ) {

                            $archivoAcepta = new ArchivoAcepta();
                            $archivoAcepta->id_archivo_excel = $newArchivoAceptaExcel->id;

                            $archivoAcepta->tipo = ($fila->tipo != null) ? $fila->tipo : '';
                            $archivoAcepta->tipo_documento = ($fila->tipo_documento != null) ? $fila->tipo_documento : '';

                            if ( $id_tipo_documento != 0 ) {
                                $archivoAcepta->id_tipo_documento = $id_tipo_documento;
                            }
                            
                            $archivoAcepta->folio = ($fila->folio != null) ? $fila->folio : 99999999999;
                            $archivoAcepta->emisor = ($fila->emisor != null) ? $fila->emisor : '';
                            $archivoAcepta->razon_social_emisor = ($fila->razon_social_emisor != null) ? $fila->razon_social_emisor : '' ;

                            if ( is_object($proveedor) ) {
                                $archivoAcepta->id_proveedor = $proveedor->id;
                            }

                            $archivoAcepta->receptor = ($fila->receptor != null) ? $fila->receptor : '';
                            $archivoAcepta->publicacion = ($fila->publicacion != null) ? $fila->publicacion : '';
                            $archivoAcepta->emision = ($fila->emision != null) ? $fila->emision : '';
                            // Cambiar tipo de dato de la BD
                            $archivoAcepta->monto_neto = ($fila->monto_neto != null) ? $fila->monto_neto : 0;
                            $archivoAcepta->monto_exento = ($fila->monto_exento != null) ? $fila->monto_exento : 0;
                            $archivoAcepta->monto_iva = ($fila->monto_iva != null) ? $fila->monto_iva : 0;
                            $archivoAcepta->monto_total = ($fila->monto_total != null) ? $fila->monto_total : 0;
                            // Solo cambiar los montos... revisar si se deben cambiar fechas u otros
                            $archivoAcepta->impuestos = $fila->impuestos;
                            $archivoAcepta->estado_acepta = ($fila->estado_acepta != null) ? $fila->estado_acepta : '';
                            $archivoAcepta->estado_sii = ($fila->estado_sii != null) ? $fila->estado_sii : '';
                            $archivoAcepta->estado_intercambio = $fila->estado_intercambio;
                            $archivoAcepta->informacion_intercambio = $fila->informacion_intercambio;
                            $archivoAcepta->uri = ($fila->uri != null) ? $fila->uri : '';
                            $archivoAcepta->referencias = $fila->referencias;
                            $archivoAcepta->mensaje_nar = $fila->mensaje_nar;
                            $archivoAcepta->uri_nar = $fila->uri_nar;
                            $archivoAcepta->uri_arm = $fila->uri_arm;
                            $archivoAcepta->fecha_arm = $fila->fecha_arm;
                            $archivoAcepta->condicion_pago = $fila->condicion_pago;
                            $archivoAcepta->fecha_vencimiento = $fila->fecha_vencimiento;
                            $archivoAcepta->estado_cesion = $fila->estado_cesion;
                            $archivoAcepta->url_correo_cesion = $fila->correo_sesion;
                            $archivoAcepta->fecha_cesion = $fila->fecha_cesion;
                            $archivoAcepta->fecha_recepcion_sii = $fila->fecha_recepcion_sii;
                            $archivoAcepta->estado_reclamo_mercaderia = $fila->estado_reclamo_mercaderia;
                            $archivoAcepta->fecha_reclamo_mercaderia = $fila->fecha_reclamo_mercaderia;
                            $archivoAcepta->estado_reclamo_contenido = $fila->estado_reclamo_contenido;
                            $archivoAcepta->fecha_reclamo_contenido = $fila->fecha_reclamo_contenido;
                            $archivoAcepta->estado_nar = $fila->estado_nar;
                            $archivoAcepta->fecha_nar = $fila->fecha_nar;
                            $archivoAcepta->mensaje_nar_2 = $fila->mensaje_nar_2;

                            $archivoAcepta->fmapago = $fila->fmapago;
                            $archivoAcepta->controller = $fila->controller;
                            $archivoAcepta->estado_reclamo = $fila->estado_reclamo;
                            $archivoAcepta->fecha_reclamo = $fila->fecha_reclamo;
                            $archivoAcepta->mensaje_reclamo = $fila->mensaje_reclamo;
                            $archivoAcepta->estado_devengo = $fila->estado_devengo;
                            $archivoAcepta->codigo_devengo = $fila->codigo_devengo;
                            $archivoAcepta->folio_oc = $fila->folio_oc;
                            $archivoAcepta->fecha_ingreso_oc = $fila->fecha_ingreso_oc;
                            $archivoAcepta->folio_rc = $fila->folio_rc;
                            $archivoAcepta->fecha_ingreso_rc = $fila->fecha_ingreso_rc;
                            $archivoAcepta->ticket_devengo = $fila->ticket_devengo;
                            $archivoAcepta->folio_sigfe = $fila->folio_sigfe;
                            $archivoAcepta->tarea_actual = $fila->tarea_actual;

                            $archivoAcepta->save();

                            if ( $archivoAcepta->id_tipo_documento === 4 && $archivoAcepta->id_proveedor != null ) {
                                $archivoAcepta->rechazarFactura();
                            }

                        } else {

                            $archivoAcepta->tipo = $fila->tipo;
                            $archivoAcepta->tipo_documento = $fila->tipo_documento;

                            $archivoAcepta->folio = $fila->folio;
                            $archivoAcepta->emisor = $fila->emisor;
                            $archivoAcepta->razon_social_emisor = $fila->razon_social_emisor;

                            $archivoAcepta->receptor = $fila->receptor;
                            $archivoAcepta->publicacion = $fila->publicacion;
                            $archivoAcepta->emision = $fila->emision;
                            // Cambiar tipo de dato de la BD
                            $archivoAcepta->monto_neto = ($fila->monto_neto != null) ? $fila->monto_neto : 0;
                            $archivoAcepta->monto_exento = ($fila->monto_exento != null) ? $fila->monto_exento : 0;
                            $archivoAcepta->monto_iva = ($fila->monto_iva != null) ? $fila->monto_iva : 0;
                            $archivoAcepta->monto_total = ($fila->monto_total != null) ? $fila->monto_total : 0;
                            // Solo cambiar los montos... revisar si se deben cambiar fechas u otros
                            $archivoAcepta->impuestos = $fila->impuestos;
                            $archivoAcepta->estado_acepta = $fila->estado_acepta;
                            $archivoAcepta->estado_sii = $fila->estado_sii;
                            $archivoAcepta->estado_intercambio = $fila->estado_intercambio;
                            $archivoAcepta->informacion_intercambio = $fila->informacion_intercambio;
                            $archivoAcepta->uri = $fila->uri;
                            $archivoAcepta->referencias = $fila->referencias;
                            $archivoAcepta->mensaje_nar = $fila->mensaje_nar;
                            $archivoAcepta->uri_nar = $fila->uri_nar;
                            $archivoAcepta->uri_arm = $fila->uri_arm;
                            $archivoAcepta->fecha_arm = $fila->fecha_arm;
                            $archivoAcepta->condicion_pago = $fila->condicion_pago;
                            $archivoAcepta->fecha_vencimiento = $fila->fecha_vencimiento;
                            $archivoAcepta->estado_cesion = $fila->estado_cesion;
                            $archivoAcepta->url_correo_cesion = $fila->correo_sesion;
                            $archivoAcepta->fecha_cesion = $fila->fecha_cesion;
                            $archivoAcepta->fecha_recepcion_sii = $fila->fecha_recepcion_sii;
                            $archivoAcepta->estado_reclamo_mercaderia = $fila->estado_reclamo_mercaderia;
                            $archivoAcepta->fecha_reclamo_mercaderia = $fila->fecha_reclamo_mercaderia;
                            $archivoAcepta->estado_reclamo_contenido = $fila->estado_reclamo_contenido;
                            $archivoAcepta->fecha_reclamo_contenido = $fila->fecha_reclamo_contenido;
                            $archivoAcepta->estado_nar = $fila->estado_nar;
                            $archivoAcepta->fecha_nar = $fila->fecha_nar;
                            $archivoAcepta->mensaje_nar_2 = $fila->mensaje_nar_2;

                            $archivoAcepta->fmapago = $fila->fmapago;
                            $archivoAcepta->controller = $fila->controller;
                            $archivoAcepta->estado_reclamo = $fila->estado_reclamo;
                            $archivoAcepta->fecha_reclamo = $fila->fecha_reclamo;
                            $archivoAcepta->mensaje_reclamo = $fila->mensaje_reclamo;
                            $archivoAcepta->estado_devengo = $fila->estado_devengo;
                            $archivoAcepta->codigo_devengo = $fila->codigo_devengo;
                            $archivoAcepta->folio_oc = $fila->folio_oc;
                            $archivoAcepta->fecha_ingreso_oc = $fila->fecha_ingreso_oc;
                            $archivoAcepta->folio_rc = $fila->folio_rc;
                            $archivoAcepta->fecha_ingreso_rc = $fila->fecha_ingreso_rc;
                            $archivoAcepta->ticket_devengo = $fila->ticket_devengo;
                            $archivoAcepta->folio_sigfe = $fila->folio_sigfe;
                            $archivoAcepta->tarea_actual = $fila->tarea_actual;

                            $archivoAcepta->save();

                            if ( $archivoAcepta->id_tipo_documento === 4 && $archivoAcepta->id_proveedor != null ) {
                                $archivoAcepta->rechazarFactura();
                            }
                            
                        }
                    }
                } else {
                    // Se valida que el archivo (de la fila del excel) no se encuentra en la base de datos, en la tabla archivo acepta.
                    $archivoAcepta = ArchivoAcepta::where('tipo',$fila->tipo)
                                                  ->where('folio',$fila->folio)
                                                  ->where('emisor',$fila->emisor)
                                                  ->first();

                    if ( !is_object($archivoAcepta) && $fila->tipo != 52  ) {
                        $archivoAcepta = new ArchivoAcepta();
                        $archivoAcepta->id_archivo_excel = $newArchivoAceptaExcel->id;

                        $archivoAcepta->tipo = $fila->tipo;
                        $archivoAcepta->tipo_documento = $fila->tipo_documento;

                        $archivoAcepta->folio = $fila->folio;
                        $archivoAcepta->emisor = $fila->emisor;
                        $archivoAcepta->razon_social_emisor = $fila->razon_social_emisor;

                        $archivoAcepta->receptor = $fila->receptor;
                        $archivoAcepta->publicacion = $fila->publicacion;
                        $archivoAcepta->emision = $fila->emision;
                        // Cambiar tipo de dato de la BD
                        $archivoAcepta->monto_neto = $fila->monto_neto;
                        $archivoAcepta->monto_exento = $fila->monto_exento;
                        $archivoAcepta->monto_iva = $fila->monto_iva;
                        $archivoAcepta->monto_total = $fila->monto_total;
                        // Solo cambiar los montos... revisar si se deben cambiar fechas u otros
                        $archivoAcepta->impuestos = $fila->impuestos;
                        $archivoAcepta->estado_acepta = $fila->estado_acepta;
                        $archivoAcepta->estado_sii = $fila->estado_sii;
                        $archivoAcepta->estado_intercambio = $fila->estado_intercambio;
                        $archivoAcepta->informacion_intercambio = $fila->informacion_intercambio;
                        $archivoAcepta->uri = $fila->uri;
                        $archivoAcepta->referencias = $fila->referencias;
                        $archivoAcepta->mensaje_nar = $fila->mensaje_nar;
                        $archivoAcepta->uri_nar = $fila->uri_nar;
                        $archivoAcepta->uri_arm = $fila->uri_arm;
                        $archivoAcepta->fecha_arm = $fila->fecha_arm;
                        $archivoAcepta->condicion_pago = $fila->condicion_pago;
                        $archivoAcepta->fecha_vencimiento = $fila->fecha_vencimiento;
                        $archivoAcepta->estado_cesion = $fila->estado_cesion;
                        $archivoAcepta->url_correo_cesion = $fila->correo_sesion;
                        $archivoAcepta->fecha_cesion = $fila->fecha_cesion;
                        $archivoAcepta->fecha_recepcion_sii = $fila->fecha_recepcion_sii;
                        $archivoAcepta->estado_reclamo_mercaderia = $fila->estado_reclamo_mercaderia;
                        $archivoAcepta->fecha_reclamo_mercaderia = $fila->fecha_reclamo_mercaderia;
                        $archivoAcepta->estado_reclamo_contenido = $fila->estado_reclamo_contenido;
                        $archivoAcepta->fecha_reclamo_contenido = $fila->fecha_reclamo_contenido;
                        $archivoAcepta->estado_nar = $fila->estado_nar;
                        $archivoAcepta->fecha_nar = $fila->fecha_nar;
                        $archivoAcepta->mensaje_nar_2 = $fila->mensaje_nar_2;

                        $archivoAcepta->fmapago = $fila->fmapago;
                        $archivoAcepta->controller = $fila->controller;
                        $archivoAcepta->estado_reclamo = $fila->estado_reclamo;
                        $archivoAcepta->fecha_reclamo = $fila->fecha_reclamo;
                        $archivoAcepta->mensaje_reclamo = $fila->mensaje_reclamo;
                        $archivoAcepta->estado_devengo = $fila->estado_devengo;
                        $archivoAcepta->codigo_devengo = $fila->codigo_devengo;
                        $archivoAcepta->folio_oc = $fila->folio_oc;
                        $archivoAcepta->fecha_ingreso_oc = $fila->fecha_ingreso_oc;
                        $archivoAcepta->folio_rc = $fila->folio_rc;
                        $archivoAcepta->fecha_ingreso_rc = $fila->fecha_ingreso_rc;
                        $archivoAcepta->ticket_devengo = $fila->ticket_devengo;
                        $archivoAcepta->folio_sigfe = $fila->folio_sigfe;
                        $archivoAcepta->tarea_actual = $fila->tarea_actual;

                        $archivoAcepta->save();

                        if ( $archivoAcepta->id_tipo_documento === 4 && $archivoAcepta->id_proveedor != null ) {
                            $archivoAcepta->rechazarFactura();
                        }
                    } else {
                        if ( is_object($archivoAcepta) ) {

                            $archivoAcepta->tipo = $fila->tipo;
                            $archivoAcepta->tipo_documento = $fila->tipo_documento;

                            $archivoAcepta->folio = $fila->folio;
                            $archivoAcepta->emisor = $fila->emisor;
                            $archivoAcepta->razon_social_emisor = $fila->razon_social_emisor;

                            $archivoAcepta->receptor = $fila->receptor;
                            $archivoAcepta->publicacion = $fila->publicacion;
                            $archivoAcepta->emision = $fila->emision;
                            // Cambiar tipo de dato de la BD
                            $archivoAcepta->monto_neto = $fila->monto_neto;
                            $archivoAcepta->monto_exento = $fila->monto_exento;
                            $archivoAcepta->monto_iva = $fila->monto_iva;
                            $archivoAcepta->monto_total = $fila->monto_total;
                            // Solo cambiar los montos... revisar si se deben cambiar fechas u otros
                            $archivoAcepta->impuestos = $fila->impuestos;
                            $archivoAcepta->estado_acepta = $fila->estado_acepta;
                            $archivoAcepta->estado_sii = $fila->estado_sii;
                            $archivoAcepta->estado_intercambio = $fila->estado_intercambio;
                            $archivoAcepta->informacion_intercambio = $fila->informacion_intercambio;
                            $archivoAcepta->uri = $fila->uri;
                            $archivoAcepta->referencias = $fila->referencias;
                            $archivoAcepta->mensaje_nar = $fila->mensaje_nar;
                            $archivoAcepta->uri_nar = $fila->uri_nar;
                            $archivoAcepta->uri_arm = $fila->uri_arm;
                            $archivoAcepta->fecha_arm = $fila->fecha_arm;
                            $archivoAcepta->condicion_pago = $fila->condicion_pago;
                            $archivoAcepta->fecha_vencimiento = $fila->fecha_vencimiento;
                            $archivoAcepta->estado_cesion = $fila->estado_cesion;
                            $archivoAcepta->url_correo_cesion = $fila->correo_sesion;
                            $archivoAcepta->fecha_cesion = $fila->fecha_cesion;
                            $archivoAcepta->fecha_recepcion_sii = $fila->fecha_recepcion_sii;
                            $archivoAcepta->estado_reclamo_mercaderia = $fila->estado_reclamo_mercaderia;
                            $archivoAcepta->fecha_reclamo_mercaderia = $fila->fecha_reclamo_mercaderia;
                            $archivoAcepta->estado_reclamo_contenido = $fila->estado_reclamo_contenido;
                            $archivoAcepta->fecha_reclamo_contenido = $fila->fecha_reclamo_contenido;
                            $archivoAcepta->estado_nar = $fila->estado_nar;
                            $archivoAcepta->fecha_nar = $fila->fecha_nar;
                            $archivoAcepta->mensaje_nar_2 = $fila->mensaje_nar_2;

                            $archivoAcepta->fmapago = $fila->fmapago;
                            $archivoAcepta->controller = $fila->controller;
                            $archivoAcepta->estado_reclamo = $fila->estado_reclamo;
                            $archivoAcepta->fecha_reclamo = $fila->fecha_reclamo;
                            $archivoAcepta->mensaje_reclamo = $fila->mensaje_reclamo;
                            $archivoAcepta->estado_devengo = $fila->estado_devengo;
                            $archivoAcepta->codigo_devengo = $fila->codigo_devengo;
                            $archivoAcepta->folio_oc = $fila->folio_oc;
                            $archivoAcepta->fecha_ingreso_oc = $fila->fecha_ingreso_oc;
                            $archivoAcepta->folio_rc = $fila->folio_rc;
                            $archivoAcepta->fecha_ingreso_rc = $fila->fecha_ingreso_rc;
                            $archivoAcepta->ticket_devengo = $fila->ticket_devengo;
                            $archivoAcepta->folio_sigfe = $fila->folio_sigfe;
                            $archivoAcepta->tarea_actual = $fila->tarea_actual;

                            $archivoAcepta->save();

                            if ( $archivoAcepta->id_tipo_documento === 4 && $archivoAcepta->id_proveedor != null ) {
                                $archivoAcepta->rechazarFactura();
                            }

                        }
                    }
                }
            }
        }

        $archivo->move( $pathExcel , $nombre.'.xls' ); // Se guarda el archivo

        $boton = '<td><div class="btn-group">';
        $boton .= '<a class="btn btn-success btn-xs" href="'.asset( $newArchivoAceptaExcel->ubicacion ).'" target="_blank" title="Descargar Archivo">';
        $boton .= '<i class="fa fa-eye"></i>';
        $boton .= '</a></div></td>';
        
        $datos = array(
            'mensaje'          => 'Ingreso de documento exitoso.',
            'estado'           => 'success',
            'id_archivo'       => $newArchivoAceptaExcel->id,
            'fechaIngreso'     => date('d/m/Y'),
            'ingresadoPor'     => $newArchivoAceptaExcel->getUser->name,
            'nombreDelArchivo' => $newArchivoAceptaExcel->nombre_original,
            'ubicacion'        => $newArchivoAceptaExcel->ubicacion,
            'extension'        => $newArchivoAceptaExcel->extension,
            'peso'             => pesoArchivoEnMB($newArchivoAceptaExcel->peso),
            'boton'            => $boton,
        );

        return response()->json($datos,200);
    }

    /**
     * Función que busca las facturas que tienen notas de credito
     * pero por un monto total menor al 100% de la factura.
     * se cambia el valor de dos columnas, para mostrar dicha informacion
     * en el listado de documentos sin recepcion
     */
    public function setFacturasConNotasCreditoMenorAlCienPorciento()
    {
        // Para dar aviso de las facturas que tienen nota de credito no por el 100%
        $notasCreditoAcepta = ArchivoAcepta::where('cargado', 0)
                                           ->where('id_tipo_documento', 4)
                                           ->where('referencias','<>', null)
                                           ->where('id_user_rechazo', null)
                                           ->select('id','referencias','monto_total','id_proveedor','folio')
                                           ->orderBy('created_at','desc')
                                           ->get();

        foreach ( $notasCreditoAcepta as $ncA ) {
            $numero_documento = null;

            if ($ncA->referencias != "" && $ncA->referencias != null) {

                $arrayJson = json_decode($ncA->referencias, true);
                if ($arrayJson != null) {
    
                    foreach ($arrayJson as $itemJson) {
    
                        if ( $itemJson['Tipo'] == 30 || $itemJson['Tipo'] == 32 || $itemJson['Tipo'] == 33 || $itemJson['Tipo'] == 34 ) { // Factura

                            $numero_documento = $itemJson['Folio'];

                            // Buscar la Factura
                            $facturaAcepta = ArchivoAcepta::where('cargado', 0)
                                                          ->where('id_proveedor', $ncA->id_proveedor)
                                                          ->whereIn('id_tipo_documento', [1,2,8]) // Factura y Re-factura
                                                          ->where('folio', $numero_documento)
                                                          ->where('id_user_rechazo', null)
                                                          ->where('monto_total','>', $ncA->monto_total)
                                                          ->where('nota_credito_menor_100_porciento', 0)
                                                          ->select('id','id_proveedor','id_tipo_documento','folio','monto_total','emision')
                                                          ->first();

                            if ( is_object($facturaAcepta) ) {
                                
                                // Factura tiene nc asociada y con un monto menor al 100% de su valor
                                $facturaAcepta->nota_credito_menor_100_porciento = 1;
                                $facturaAcepta->id_nota_credito_menor_100_porciento = $ncA->id;
                                $facturaAcepta->save();
                                break;

                            }
                                
                        }
    
                    }
                }
    
            }

        }

    }

    /**
     * Funcion que rechaza automaticamente Facturas y notas de credito.
     * Deben hacer match, la nota de credito debe ser por el monto total de la factura.
     * Deben ser del mismo proveedor y ambos documentos no deben estar cargados en el sistema
     */
    public function rechazoAutomaticoFacturasAndNotasCredito()
    {
        /**
         * Para rechazar Facturas con Notas de credito al 100% 
         */
        $notasCreditoAcepta = ArchivoAcepta::where('cargado', 0)
                                           ->where('id_tipo_documento', 4)
                                           ->where('referencias','<>', null)
                                           ->where('id_user_rechazo', null)
                                           ->select('id','referencias','monto_total','id_proveedor','folio')
                                           ->get();

        foreach ( $notasCreditoAcepta as $ncA ) {
            $numero_documento = null;

            if ($ncA->referencias != "" && $ncA->referencias != null) {

                $arrayJson = json_decode($ncA->referencias, true);
                if ($arrayJson != null) {
    
                    foreach ($arrayJson as $itemJson) {
    
                        if ( $itemJson['Tipo'] == 30 || $itemJson['Tipo'] == 32 || $itemJson['Tipo'] == 33 || $itemJson['Tipo'] == 34 ) { // Factura

                            $numero_documento = $itemJson['Folio'];

                            // Buscar la Factura
                            $facturaAcepta = ArchivoAcepta::where('cargado', 0)
                                                          ->where('id_proveedor', $ncA->id_proveedor)
                                                          ->whereIn('id_tipo_documento', [1,2,8]) // Factura y Re-factura
                                                          ->where('folio', $numero_documento)
                                                          ->where('id_user_rechazo', null)
                                                          ->where('monto_total', $ncA->monto_total)
                                                          ->select('id','id_proveedor','id_tipo_documento','folio','monto_total','emision')
                                                          ->first();

                            if ( is_object($facturaAcepta) ) {

                                // Rechazar Factura
                                $facturaAcepta->observacion_rechazo = 'Documento rechazado automáticamente por el sistema.';
                                $facturaAcepta->observacion_rechazo .= PHP_EOL.'Tiene una Nota de Crédito por el total del monto.';
                                $facturaAcepta->observacion_rechazo .= PHP_EOL.'Folio de la Nota de Crédito : '.$ncA->folio;
                                $facturaAcepta->id_user_rechazo = Auth::user()->id;
                                $facturaAcepta->save();

                                $aceptaMotivo = new AceptaMotivoRechazo();
                                $aceptaMotivo->id_archivo_acepta = $facturaAcepta->id;
                                $aceptaMotivo->id_motivo_rechazo = 5;
                                $aceptaMotivo->save();

                                // Rechazar Nota de Credito
                                $ncA->observacion_rechazo = 'Documento rechazado automáticamente por el sistema.';
                                $ncA->observacion_rechazo .= PHP_EOL.'Anula una Factura, la Nota de Crédito es por el total del monto.';
                                $ncA->observacion_rechazo .= PHP_EOL.'Folio de la Factura : '.$facturaAcepta->folio;
                                $ncA->id_user_rechazo = Auth::user()->id;
                                $ncA->save();

                                $aceptaMotivo = new AceptaMotivoRechazo();
                                $aceptaMotivo->id_archivo_acepta = $ncA->id;
                                $aceptaMotivo->id_motivo_rechazo = 5;
                                $aceptaMotivo->save();

                                // dd($facturaAcepta,$ncA, $numero_documento);
                                break;

                            } else {

                                /**
                                 * Para rechazar la nota de credito en caso de que la factura esté rechazada
                                 */

                                $facturaAcepta = ArchivoAcepta::where('cargado', 0)
                                                              ->where('id_proveedor', $ncA->id_proveedor)
                                                              ->whereIn('id_tipo_documento', [1,2,8]) // Factura y Re-factura
                                                              ->where('folio', $numero_documento)
                                                              ->where('id_user_rechazo','<>' ,null)
                                                            //   ->where('monto_total', $ncA->monto_total)
                                                              ->select('id','id_proveedor','id_tipo_documento','folio','monto_total','emision')
                                                              ->first();

                                if ( is_object($facturaAcepta) ) {

                                    // Rechazar Nota de Credito
                                    $ncA->observacion_rechazo = 'Documento rechazado automáticamente por el sistema.';
                                    $ncA->observacion_rechazo .= PHP_EOL.'Aplica a una Factura que ya esta rechazada.';
                                    $ncA->observacion_rechazo .= PHP_EOL.'Folio de la Factura : '.$facturaAcepta->folio;
                                    $ncA->id_user_rechazo = Auth::user()->id;
                                    $ncA->save();

                                    $aceptaMotivo = new AceptaMotivoRechazo();
                                    $aceptaMotivo->id_archivo_acepta = $ncA->id;
                                    $aceptaMotivo->id_motivo_rechazo = 4;
                                    $aceptaMotivo->save();

                                }
                                
                            }
                              
                        }
    
                    }
                }
    
            }

        }
        
    }

    /**
     * Esta funcion carga en los documentos el archivo de tipo factura,
     * en caso de no tenerlo
     */
    public function cargaArchivoEnDocumentos()
    {
        set_time_limit(0);
        ini_set('max_execution_time', 0);

        $documentos = Documento::with([
                                    'getArchivoAcepta' => function ($query) {
                                        $query->select(
                                                'id','folio','id_proveedor',
                                                'id_tipo_documento','monto_total',
                                                'uri',
                                                'created_at'
                                            );
                                    },
                                ])
                               ->whereHas('getArchivoAcepta')
                               ->whereDoesntHave('getArchivos', function ($query) {
                                   $query->where('id_tipo_archivo', 1);
                               })
                            //    ->where('created_at', '>=', '2019-06-23 00:00:00')
                               ->select(
                                   'id','numero_documento','id_proveedor',
                                   'id_tipo_documento','total_documento',
                                   'created_at'
                               )
                               ->orderBy('created_at','desc')
                               ->get();
        
        foreach ($documentos as $key => $documento) {

            // Se guarda la Factura (pdf) en el sistema, para el documento
            $newArchivo = new Archivo();
            $newArchivo->id_documento = $documento->id;
            $newArchivo->id_tipo_archivo = 1;
            $newArchivo->nombre = 'archivoAcepta_'.$documento->getArchivoAcepta->id.'_'.date('Y_m_d_G_i_s');
            $newArchivo->nombre_original = $newArchivo->nombre;
            $newArchivo->ubicacion = "documento/".$documento->id."/".$newArchivo->nombre.".pdf";
            $newArchivo->extension = "pdf";
            $newArchivo->peso = 0;
            $newArchivo->save();
                    
            $content = file_get_contents('http://windte1806.acepta.com/ca4webv3/PdfViewMedia?url='.$documento->getArchivoAcepta->uri.'&menuTitle=Papel%2520Carta');
            if ( !file_exists(public_path().'/documento/'.$documento->id) ) {
                mkdir(public_path().'/documento/'.$documento->id, 0775, true);
                chmod(public_path().'/documento/'.$documento->id, 0775);
            }
            $file = fopen( public_path().'/documento/'.$documento->id.'/'.$newArchivo->nombre.'.pdf', 'a');
            fwrite($file, $content);
            fclose($file);

        }

        $datos = array (
            'estado' => 'success',
            'cantidadCargas' => $documentos->count(),
        );

        return response()->json($datos,200);

    }

    /**
     * Funcion que busca documentos en el sistema en base a los documentos de acepta.
     * Si encuentra en el sistema guarda los archivos de acepta en el documento del sistema
     * y deja el "documento acepta" como cargado para quitarlo de la grilla
     */
    public function cargaAuxiliarDeAceptaAlSistema()
    {
        set_time_limit(0);
        ini_set('max_execution_time', 0);

        $archivosAcepta = ArchivoAcepta::with(['getProveedor','getTipoDocumento'])
                                       ->where('cargado',0)
                                       // ->whereMonth('created_at', date('m'))
                                       ->where('created_at', '>=', '2019-06-23 00:00:00')
                                       ->get();
        $contaDeletedNull = 0;
        $contaDeleted = 0;
        $arrayDeletedNull = array();
        $arrayDeleted = array();
        foreach ($archivosAcepta as $key => $archivoAcepta) {
            $documento = Documento::where('id_proveedor',$archivoAcepta->getProveedor->id)
                                  // ->where('deleted_at',null)
                                  ->where('numero_documento', trim($archivoAcepta->folio) ) 
                                  ->where('id_tipo_documento',$archivoAcepta->getTipoDocumento->id)
                                  //->select('id_tipo_documento','deleted_at','numero_documento','id_proveedor','fecha_ingreso')
                                  ->first();

            if ( is_object($documento) ) {
                $contaDeletedNull++;
                $arrayDeletedNull[] = 'N° Doc. '.$documento->numero_documento.' rutProveedor '.$documento->getProveedor->rut.' fechaIngreso '.$documento->fecha_ingreso.'';

                $archivoFacturaDelDocumento = Archivo::where('id_documento',$documento->id)
                                                     ->where('id_tipo_archivo',1)
                                                     ->first();
                
                if ( !is_object($archivoFacturaDelDocumento) ) {
                    // Se guarda la Factura (pdf) en el sistema, para el documento
                    $newArchivo = new Archivo();
                    $newArchivo->id_documento = $documento->id;
                    $newArchivo->id_tipo_archivo = 1;
                    $newArchivo->nombre = 'archivoAcepta_'.$archivoAcepta->id.'_'.date('Y_m_d_G_i_s');
                    $newArchivo->nombre_original = $newArchivo->nombre;
                    $newArchivo->ubicacion = "documento/".$documento->id."/".$newArchivo->nombre.".pdf";
                    $newArchivo->extension = "pdf";
                    $newArchivo->peso = 0;
                    $newArchivo->save();
                    
                    $content = file_get_contents('http://windte1806.acepta.com/ca4webv3/PdfViewMedia?url='.$archivoAcepta->uri.'&menuTitle=Papel%2520Carta');
                    if ( !file_exists(public_path().'/documento/'.$documento->id) ) {
                        mkdir(public_path().'/documento/'.$documento->id, 0775, true);
                        chmod(public_path().'/documento/'.$documento->id, 0775);
                    }
                    $file = fopen( public_path().'/documento/'.$documento->id.'/'.$newArchivo->nombre.'.pdf', 'a');
                    fwrite($file, $content);
                    fclose($file);
                    
                }

                // $archivoAutorizacionSiiDelDocumento = Archivo::where('id_documento',$documento->id)
                //                            ->where('id_tipo_archivo', 15)
                //                            ->first();

                // if ( !is_object($archivoAutorizacionSiiDelDocumento) ) {
                //     //Autorizacion SII
                //     $newArchivo = new Archivo();
                //     $newArchivo->id_documento = $documento->id;
                //     $newArchivo->id_tipo_archivo = 15;
                //     $newArchivo->nombre = 'archivoAcepta_'.$archivoAcepta->id.'_'.date('Y_m_d_G_i_s').'_sii';
                //     $newArchivo->nombre_original = $newArchivo->nombre;
                //     $newArchivo->ubicacion = "documento/".$documento->id."/".$newArchivo->nombre.".pdf";
                //     $newArchivo->extension = "pdf";
                //     $newArchivo->peso = 0;
                //     $newArchivo->save();

                //     // Se obtiene la información desde SII y se genera un pdf para guardarlo en el sistema
                //     $gl_url = 'https://palena.sii.cl/cgi_dte/UPL/QEstadoDTE?rutQuery=61608204&dvQuery=3&rutCompany='.str_replace(".", "", trim( explode("-",$archivoAcepta->emisor)[0] ) ).'&dvCompany='.explode("-", $archivoAcepta->emisor )[1].'&rutReceiver=61608204&dvReceiver=3&tipoDTE='.$archivoAcepta->tipo.'&folioDTE='.$archivoAcepta->folio.'&fechaDTE='.fecha_dmY_sinSeparador($archivoAcepta->emision).'&montoDTE='.$archivoAcepta->monto_total;
                //     $content = file_get_contents($gl_url);
                //     //str_replace(public_path().'VisBue.gif',"https://palena.sii.cl/dte/UPL/VisBue.gif",$content);
                //     str_replace('',"https://palena.sii.cl/dte/UPL/VisBue.gif",$content);
                    
                //     // Quita espacios
                    
                //     $buscar = array('/\>[^\S ]+/s','/[^\S ]+\</s','/(\s)+/s');
                //     $reemplazar = array('>','<','\\1');
                //     $content = preg_replace($buscar, $reemplazar, $content);
                //     $content = str_replace("> <", "><", $content);
                
                //     // Arregla etiquetas
                    
                //     $content = str_replace('</table></td></tr><tr><td><font face="Arial" size="2"><strong>Documento consultado:</strong></font>','</table><table><tr><td><font face="Arial" size="2"><strong>Documento consultado:</strong></font>',$content);
                //     $content = utf8_encode($content);
                //     $content = mb_convert_encoding($content, 'UTF-8', 'UTF-8');

                //     $mpdf = new Mpdf(['mode' => 'c','tempDir' => public_path() . '/tempMpdf']);
                //     $mpdf->SetDisplayMode('fullpage');
                //     $mpdf->keep_table_proportions = true;
                //     $mpdf->WriteHTML($content);
                //     $mpdf->Output(public_path().'/documento/'.$documento->id.'/'.$newArchivo->nombre.'.pdf','F');
                // }

                $archivoAcepta->cargado = 1;
                $archivoAcepta->save();

            } 
        }

        // dd('No eliminados',$contaDeletedNull,$arrayDeletedNull,'Eliminados',$contaDeleted,$arrayDeleted);

    }

    public function dataTableSinRecepcion(Request $request)
    {
        set_time_limit(0);
        ini_set('max_execution_time', 0);

        $search = $request->get('search')['value'];
        $order_column = $request->get('order')[0]['column'];
        $order_dir = $request->get('order')[0]['dir'];

        $formJSON = json_decode($request->get('form'));
        $form = construyeFormulario($formJSON);
        
        $fechaInicio = fecha_Y_m_d($form->filtro_fecha_inicio);
        $fechaTermino = fecha_Y_m_d($form->filtro_fecha_termino);

        $archivosAceptaConRecepcion = \DB::table('archivos_acepta')
                                          ->join('proveedores','archivos_acepta.id_proveedor','=','proveedores.id')
                                          ->join('tipos_documento','archivos_acepta.id_tipo_documento','tipos_documento.id')
                                          ->join('ws_contrato','proveedores.rut','=','ws_contrato.rut_proveedor')
                                          ->join('ws_orden_compra','ws_contrato.id','=','ws_orden_compra.id_contrato')
                                          ->join('ws_documento','ws_orden_compra.id','=','ws_documento.id_orden_compra')

                                          ->whereColumn('tipos_documento.nombre','LIKE','ws_documento.tipo_documento')
                                          ->whereColumn('archivos_acepta.folio','=','ws_documento.documento')
                                          ->whereColumn('archivos_acepta.monto_total','=','ws_documento.documento_total')
                                          
                                          ->where('ws_documento.id_relacion',null)
                                          ->where('archivos_acepta.cargado',0)
                                          ->where('archivos_acepta.id_user_rechazo', null)
                                          ->where('archivos_acepta.id_user_rezagar', null)

                                          ->select(
                                              'archivos_acepta.id'
                                            )
                                          ->get()->pluck('id')->toArray();

        $archivosAcepta = ArchivoAcepta::with([
                                            'getProveedor',
                                            'getTipoDocumento',
                                            'getUsuarioResponsable'
                                        ])
                                       ->where('cargado', 0)
                                       ->where('id_user_rechazo', null)
                                       ->where('id_user_rezagar', null)
                                       ->doesnthave('getArchivoAceptaRevisionBodega')
                                       ->whereNotIn('id', $archivosAceptaConRecepcion);

        if ( $form->filtro_numero_documento == null ) {
            // Se filtra por las fechas

            $archivosAcepta = $archivosAcepta->where('created_at','>=',$fechaInicio.' 00:00:00')
                                             ->where('created_at','<=',$fechaTermino.' 23:59:59');

        } else {
            // no se consideran las fechas, y se busca el numero del documento
            $archivosAcepta = $archivosAcepta->where('folio', trim($form->filtro_numero_documento) );
        }

        if ( $form->filtro_consumo_basico == "No" ) {
            if ( isset($form->filtro_proveedor) ) {
                $archivosAcepta = $archivosAcepta->WhereIn('id_proveedor', $form->filtro_proveedor);
            }
        } else {
            /**
             * Filtro según proveedores de consumo basico
             * ID     Prov
             * 240    Empresa de correos chile
             * 250    Aguas Andinas
             * 437    Costanera Norte
             * 595    Esmax distribucion
             * 723    Gasco GLP
             * 750    Metrogas
             * 754    Chilexpress
             * 761    Enel distribucion chile
             * 763    Entel PCS telecomunicaciones
             * 779    Vespucio norte autopista
             * 782    S. CONCENSONARIA AUTOPISTA CENTRAL
             * 788    Vespucio norte express
             */

            $archivosAcepta = $archivosAcepta->WhereIn('id_proveedor', [ 240, 250, 437, 595, 
                                                                         723, 750, 754, 761, 
                                                                         763, 779, 782, 788 ]);
        }
        

        if ( isset($form->filtro_tipo_documento) ) {
            $archivosAcepta = $archivosAcepta->WhereIn('id_tipo_documento', $form->filtro_tipo_documento);
        }

        if ( \Entrust::hasRole('usuario-ingreso') ) {
            $archivosAcepta = $archivosAcepta->where(function ($query){
                                                $query->where('id_user_responsable', \Auth::user()->id)
                                                      ->orWhere('id_user_responsable', null);
                                            });
        }

        $archivosAcepta = $archivosAcepta->get();

        $data = [];
        
        $verFacturaDocumentoAcepta = \Entrust::can('ver-factura-documento-acepta'); // \Auth::user()->can('ver-factura-documento-acepta')
        $cargaDocumentoAceptaAlSistema = \Entrust::can('carga-documento-acepta-al-sistema');
        $rechazarDocumentoAcepta = \Entrust::can('rechazar-documento-acepta');
        $isAdmin = \Entrust::hasRole(['propietario','administrador']);
        foreach ( $archivosAcepta as $archivoAcepta ) {
            $data[] = $archivoAcepta->getDatosSinRecepcion( $verFacturaDocumentoAcepta, $cargaDocumentoAceptaAlSistema, $rechazarDocumentoAcepta, $isAdmin );
        }

        $respuesta = array(
            "data" => $data
        );

        return json_encode($respuesta);

    }

    public function getDocumentosSinRecepcion()
    {
        if (!\Entrust::can(['ver-sin-recepcion-acepta','ver-factura-documento-acepta','carga-documento-acepta-al-sistema','rechazar-documento-acepta'])) {
            return \Redirect::to('home');
        }

        $tiposDocumento = TipoDocumento::whereNotIn('id',[2,8])->get();
        $proveedores = Proveedor::all();

        return view('archivo_acepta.index_sin_recepcion')->with('tiposDocumento',$tiposDocumento)
                                                         ->with('proveedores',$proveedores);
    }

    public function postSinRecepcionExcel(Request $request)
    {
        \Excel::create('SIFCON | Informe Sin Recepción Acepta '.date('d-m-Y').'', function($excel) use($request) {
            $fechaInicio = fecha_Y_m_d($request->input('filtro_fecha_inicio'));
            $fechaTermino = fecha_Y_m_d($request->input('filtro_fecha_termino'));
            
            $archivosAceptaConRecepcion = \DB::table('archivos_acepta')
                                          ->join('proveedores','archivos_acepta.id_proveedor','=','proveedores.id')
                                          ->join('tipos_documento','archivos_acepta.id_tipo_documento','tipos_documento.id')
                                          ->join('ws_contrato','proveedores.rut','=','ws_contrato.rut_proveedor')
                                          ->join('ws_orden_compra','ws_contrato.id','=','ws_orden_compra.id_contrato')
                                          ->join('ws_documento','ws_orden_compra.id','=','ws_documento.id_orden_compra')

                                          ->whereColumn('tipos_documento.nombre','LIKE','ws_documento.tipo_documento')
                                          ->whereColumn('archivos_acepta.folio','=','ws_documento.documento')
                                          ->whereColumn('archivos_acepta.monto_total','=','ws_documento.documento_total')
                                          
                                          ->where('ws_documento.id_relacion',null)
                                          ->where('archivos_acepta.cargado',0)
                                          ->where('archivos_acepta.id_user_rechazo', null)
                                          ->where('archivos_acepta.id_user_rezagar', null)

                                          ->select(
                                              'archivos_acepta.id'
                                            )
                                          ->get()->pluck('id')->toArray();
                                          
            $documentosAcepta = ArchivoAcepta::with(['getProveedor','getTipoDocumento', 'getUsuarioResponsable'])
                                            ->where('cargado', 0)
                                            ->where('id_user_rechazo', null)
                                            ->where('id_user_rezagar', null)
                                            ->doesnthave('getArchivoAceptaRevisionBodega')
                                            ->where('created_at', '>=', $fechaInicio.' 00:00:00')
                                            ->where('created_at', '<=', $fechaTermino.' 23:59:59')
                                            ->whereNotIn('id', $archivosAceptaConRecepcion);

            if ( $request->input('filtro_consumo_basico') == "No" ) {
                if ( $request->input('filtro_proveedor') != null ) {
                    $documentosAcepta = $documentosAcepta->WhereIn('id_proveedor',$request->input('filtro_proveedor'));
                }
            } else {

                 /**
                 * Filtro según proveedores de consumo basico
                 * ID     Prov
                 * 240    Empresa de correos chile
                 * 250    Aguas Andinas
                 * 437    Costanera Norte
                 * 595    Esmax distribucion
                 * 723    Gasco GLP
                 * 750    Metrogas
                 * 754    Chilexpress
                 * 761    Enel distribucion chile
                 * 763    Entel PCS telecomunicaciones
                 * 779    Vespucio norte autopista
                 * 782    S. CONCENSONARIA AUTOPISTA CENTRAL
                 * 788    Vespucio norte express
                 */

                $documentosAcepta = $documentosAcepta->WhereIn('id_proveedor', [ 240, 250, 437, 595, 
                                                                             723, 750, 754, 761, 
                                                                             763, 779, 782, 788 ]);
            }
            

            if ( $request->input('filtro_tipo_documento') != null ) {
                $archivosAcepta = $documentosAcepta->WhereIn('id_tipo_documento',$request->input('filtro_tipo_documento'));
            }

            $documentosAcepta = $documentosAcepta->get();

            $excel->sheet('Documentos Acepta', function($sheet) use($documentosAcepta) {

                $sheet->row(1, [
                    'Estado',
                    'Responsable',
                    'Chile-Compra',
                    'Proveedor',
                    'RUT',
                    'Tipo Dcto.',
                    'N° Dcto.',
                    'F/Doc.',
                    'M/Doc.',
                    'Fecha RD',
                    'Mes RD',
                    'Monto',
                    'URI',
                ]);

                foreach ( $documentosAcepta as $index => $documento ) {

                    // Se trabaja el N° de orden compra.
                    $numOrdenCompra = '';
                    if ($documento->referencias != "" && $documento->referencias != null) {

                        $arrayJson = json_decode($documento->referencias, true);
                        if ($arrayJson != null) {
                            foreach ($arrayJson as $itemJson) {
                                if ($itemJson['Tipo'] == 801) {
                                    $numOrdenCompra = $itemJson['Folio'];
                                }
                            }
                        }

                    }

                    $tipoDocumento = '';    
                    if ( $documento->getTipoDocumento ) {
                        $tipoDocumento .= $documento->getTipoDocumento->nombre;
                    } else {
                        $tipoDocumento .= $documento->tipo_documento;
                    }
                    
                    $rutProveedor = '';
                    $nombreProveedor = '';
                    if ( $documento->getProveedor ) {
                        $rutProveedor .= $documento->getProveedor->rut;
                        $nombreProveedor .= $documento->getProveedor->nombre;
                    } else {
                        $rutProveedor .= $documento->emisor;
                        $nombreProveedor .= $documento->razon_social_emisor;
                    }

                    $sheet->row($index+2, [
                        $documento->getEstadoAcepta(),
                        ($documento->getUsuarioResponsable) ? $documento->getUsuarioResponsable->name : '',
                        $numOrdenCompra,
                        //explode("-",$documento->fecha_sigfe)[1], // mes de la fecha sigfe
                        $nombreProveedor,
                        $rutProveedor,
                        $tipoDocumento,
                        $documento->folio,
                        fecha_dmY($documento->emision),
                        explode("-",$documento->emision)[1], // mes de la fecha del documento
                        fecha_dmY($documento->created_at),
                        explode("-",$documento->created_at)[1], // mes de la fecha de recepcion del documento
                        $documento->monto_total,
                        $documento->uri,
                    ]);

                }

            });

        })->export('xlsx');
        
    }

    public function getModalCarga($idArchivoAcepta)
    {
        
        $archivoAcepta = ArchivoAcepta::with(['getProveedor','getTipoDocumento'])->findOrFail($idArchivoAcepta);
        $tiposDocumento = TipoDocumento::whereNotIn('id',[2,8])->get();
        $modalidadesCompra = ModalidadCompra::all();
        $tiposAdjudicacion = TipoAdjudicacion::all();
        $tiposInforme = TipoInforme::all();
        $usuarios = User::where('responsable',1)->get();
        $referentesTecnicos = ReferenteTecnico::all();
        $tiposArchivo = TipoArchivo::all();
        $tipoDoc = TipoDocumento::findOrFail($archivoAcepta->id_tipo_documento);

        // Se trabaja el N° de orden compra.
        $numOrdenCompra = '';
        $numero_documento = null; // guarda el numero del documento para buscarlo en el sistema, en el caso de NC y ND
        if ($archivoAcepta->referencias != "" && $archivoAcepta->referencias != null) {
            
            $arrayJson = json_decode($archivoAcepta->referencias, true);
            
            if ($arrayJson != null) {
                
                foreach ($arrayJson as $itemJson) {

                    if ($itemJson['Tipo'] == 801) {
                        $numOrdenCompra = $itemJson['Folio'];
                    }
                    
                    if ( ( $itemJson['Tipo'] == 30 || $itemJson['Tipo'] == 32 || $itemJson['Tipo'] == 33 || $itemJson['Tipo'] == 34 ) && $itemJson['Folio'] != 0 ) { // Factura
                        $numero_documento = $itemJson['Folio'];
                    }

                }

            }

        } else {
            $numOrdenCompra = '1641-';
        }

        $modalidadCompraDefecto = null;
        // Solo se ingresa si el proveedor es cenabast
        if ( $archivoAcepta->id_proveedor == 249 ) {

            $modalidadCompraDefecto = ModalidadCompra::findOrFail(2); // Cenabast
            $modalidadCompraDefecto->id_tipo_adjudicacion = 2; // Convenio Cenabast
            $modalidadCompraDefecto->id_user_responsable = 17; // Pablo Osorio
            $modalidadCompraDefecto->id_referente_tecnico = 13; //Jefe de bodega
            $modalidadCompraDefecto->id_tipo_informe = 1; // Programa
            

            // Ver si se puede obtener información del PDF, al final se obtiene desde el visor XML
            $content = file_get_contents('http://dipres1908.acepta.com/ca4webv3/XmlView?url='.$archivoAcepta->uri);
            // dd( $content );

            if ( strpos($content,'INTERMEDIACION') !== false ) {

                $numOrdenCompra = 'Intermediación';
                $datos = array(
                    'estado' => 'error',
                );
                if ( strpos($content,'Segun&nbsp;detalle&nbsp;anexo&nbsp;') !== false ) {
                    // es comisión intermediación
                    $modalidadCompraDefecto->id_referente_tecnico = 33; //Jefe de abastecimiento
                    // dd('comisión intermediacion',$archivoAcepta->uri);
                    $item = ItemPresupuestario::findOrFail(117);
                    
                    $datos = array(
                        'estado' => 'success',
                        'id_item' => $item->id,
                        'codigo' => $item->codigo(),
                        'clasificacionPresupuestario' => $item->clasificador_presupuestario,
                    );
                } else {
                    //es farmacía intermediación
                    // dd('farmacía intermediacion',$archivoAcepta->uri);
                    $item = ItemPresupuestario::findOrFail(84);

                    $datos = array(
                        'estado' => 'success',
                        'id_item' => $item->id,
                        'codigo' => $item->codigo(),
                        'clasificacionPresupuestario' => $item->clasificador_presupuestario,
                    );
                }

                $modalidadCompraDefecto->item = json_encode($datos, true);

            } else if ( strpos($content, 'Ministeriales') !== false ) {
                $numOrdenCompra = 'Programa Ministerial';

                $item = ItemPresupuestario::findOrFail(84);

                $datos = array(
                    'estado' => 'success',
                    'id_item' => $item->id,
                    'codigo' => $item->codigo(),
                    'clasificacionPresupuestario' => $item->clasificador_presupuestario,
                );

                $modalidadCompraDefecto->item = json_encode($datos, true);

            }
            
        }

        // Para obtener documentos relacionados dependiendo del tipo de documento
        $documentosProveedor = null;
        $docsRechazadosAceptaProv = null;
        if ( $archivoAcepta->id_tipo_documento == 1 ) {
            // Solo Facturas con NC al 100%
            // Que no tengan Facturas relacionadas, si la tienen es porque ya se refacturo
            $documentosProveedor = Documento::with(['getDevengos.getItemPresupuestario','getTipoDocumento','getDocumentosRelacionados'])
                                            ->where('id_proveedor',$archivoAcepta->id_proveedor)
                                            ->where('id_tipo_documento', 1)
                                            ->whereHas('getDocumentosRelacionados', function($query) {
                                                $query->where('id_tipo_documento', 4); // NC
                                            })
                                            ->whereDoesntHave('getDocumentosRelacionados', function ($query) {
                                                $query->where('id_tipo_documento', 1); // FC
                                            })
                                            ->where('total_documento_actualizado', 0) // significa que la factura esta anulada
                                            ->where('numero_documento','<>',$archivoAcepta->folio)
                                            ->get();

            // Solo facturas con NC por el 100% y que no tengan refactura (getDocumentoFactura)
            $docsRechazadosAceptaProv = ArchivoAcepta::with(['getProveedor','getTipoDocumento'])
                                                     ->where('cargado', 0)
                                                     ->where('id_user_rechazo','<>',null)
                                                     ->where('id_proveedor', $archivoAcepta->id_proveedor)
                                                     ->where('id_tipo_documento', 1)
                                                     ->whereHas('getAceptaMotivoRechazo', function ($query){
                                                         $query->where('id_motivo_rechazo', 5); // Anula Documento
                                                     })
                                                     ->where('observacion_rechazo', 'LIKE', '%Tiene una Nota de Crédito por el total del monto%')
                                                     ->doesntHave('getDocumentoFactura')
                                                     ->get();

        } elseif ( $archivoAcepta->id_tipo_documento == 4 || $archivoAcepta->id_tipo_documento == 5 || $archivoAcepta->id_tipo_documento == 10 || $archivoAcepta->id_tipo_documento == 11 ) {

            $documentosProveedor = Documento::with(['getDevengos.getItemPresupuestario','getTipoDocumento'])
                                            ->where('id_proveedor', $archivoAcepta->id_proveedor)
                                            ->whereIn('id_tipo_documento', [1,2,8]);
            if ( $numero_documento == null ) {
                $documentosProveedor = $documentosProveedor->where('numero_documento', '<>', $archivoAcepta->folio)->get();
            } else {
                $documentosProveedor = $documentosProveedor->where('numero_documento', '=', $numero_documento)->get();
            }

        } elseif ($archivoAcepta->id_tipo_documento == 8 ) {

            $documentosProveedor = Documento::with(['getDevengos.getItemPresupuestario','getTipoDocumento'])
                                            ->where('id_proveedor',$archivoAcepta->id_proveedor)
                                            ->where('id_tipo_documento',4)
                                            ->where('numero_documento','<>',$archivoAcepta->folio)->get();
        }


        /** 
        * Para los documentos disponibles de recepcion
        * En este caso se busca el documento disponible de recepcion 
        * que concuerde con el documento acepta (segun el tipo de documento)
        * Si no encuentra recepcion que concuerde, se muestran todas en la modal
        */
        $recepcionDisponible = false;
        $opciones = '';
        if ( ! ($archivoAcepta->id_tipo_documento == 4 || $archivoAcepta->id_tipo_documento == 5 || $archivoAcepta->id_tipo_documento == 10 || $archivoAcepta->id_tipo_documento == 11) ) {

            $documentoBodega = WsDocumento::with(['getWsOrdenCompra.getWsContrato'])
                                            ->whereHas('getWsOrdenCompra.getWsContrato',function ($query) use ($archivoAcepta) {
                                                $query->whereIn('rut_proveedor', $archivoAcepta->getProveedor->getTodosLosRut());
                                            })
                                            ->where('id_relacion', null)
                                            ->where('tipo_documento', 'LIKE', $archivoAcepta->getTipoDocumento->nombre)
                                            ->where('documento', $archivoAcepta->folio)
                                            ->first();
        
            if ( is_object($documentoBodega) ) {
                
                $opciones .= PHP_EOL.'<option id="docBodega_'.$documentoBodega->id.'" ';
                $opciones .= 'value="'.$documentoBodega->id.'" ';
                $opciones .= 'data-datosrecepcion="'.$documentoBodega->getDatosDocumentoAndOrdenCompra().'" ';
                $opciones .= '>'.$archivoAcepta->getTipoDocumento->sigla.' N° '.$documentoBodega->documento.' '; 
                $opciones .= '- '.fecha_dmY($documentoBodega->fecha_carga).' - $'.formatoMiles($documentoBodega->documento_total).'';
                $opciones .= ' | Prov. '.$documentoBodega->getWsOrdenCompra->getWsContrato->rut_proveedor;
                $opciones .= '</option>'.PHP_EOL;

                $recepcionDisponible = true;
                
            } else {

                $docBodegaDisponibles = WsDocumento::with(['getWsOrdenCompra.getWsContrato'])
                                                //    ->whereHas('getWsOrdenCompra.getWsContrato',function ($query) use ($archivoAcepta) {
                                                //         $query->whereIn('rut_proveedor', $archivoAcepta->getProveedor->getTodosLosRut());
                                                //     })
                                                   ->where('id_relacion', null)
                                                   ->where('tipo_documento', 'LIKE', $archivoAcepta->getTipoDocumento->nombre);
                                                   
                // Solo se ingresa si el proveedor no es cenabast
                if ( $archivoAcepta->id_proveedor != 249 ) {

                    $docBodegaDisponibles = $docBodegaDisponibles->whereHas('getWsOrdenCompra.getWsContrato',function ($query) use ($archivoAcepta) {
                                                                        $query->whereIn('rut_proveedor', $archivoAcepta->getProveedor->getTodosLosRut());
                                                                    });

                }

                $docBodegaDisponibles =$docBodegaDisponibles->get();
                                                
                if ( $docBodegaDisponibles->count() > 0 ) {
                    foreach ($docBodegaDisponibles as $key => $documentoBodega) {

                        $opciones .= PHP_EOL.'<option id="docBodega_'.$documentoBodega->id.'" ';
                        $opciones .= 'value="'.$documentoBodega->id.'" ';
                        $opciones .= 'data-datosrecepcion="'.$documentoBodega->getDatosDocumentoAndOrdenCompra().'" ';
                        $opciones .= '>'.$archivoAcepta->getTipoDocumento->sigla.' N° '.$documentoBodega->documento.' '; 
                        $opciones .= '- '.fecha_dmY($documentoBodega->fecha_carga).' - $'.formatoMiles($documentoBodega->documento_total).'';
                        $opciones .= ' | Prov. '.$documentoBodega->getWsOrdenCompra->getWsContrato->rut_proveedor;
                        $opciones .= '</option>'.PHP_EOL;

                    }
                }

            }

        }
        

        /**
         * Para las recepcion que es del tipo Guia de Despacho Electronica
         */
        $opcionesGuias = '';
        if ( ! ($archivoAcepta->id_tipo_documento == 4 || $archivoAcepta->id_tipo_documento == 5 || $archivoAcepta->id_tipo_documento == 10 || $archivoAcepta->id_tipo_documento == 11) ) {

            $documentosBodegaDisponibles = WsDocumento::with(['getWsOrdenCompra.getWsContrato'])
                                                    //   ->whereHas('getWsOrdenCompra.getWsContrato',function ($query) use ($archivoAcepta) {
                                                    //         $query->whereIn('rut_proveedor', $archivoAcepta->getProveedor->getTodosLosRut());
                                                    //     })
                                                      ->where('id_relacion', null)
                                                      ->where('tipo_documento', 'LIKE', '%Guia de Despacho%');
                                                    //   ->get();

            // Solo se ingresa si el proveedor no es cenabast
            if ( $archivoAcepta->id_proveedor != 249 ) {
                $documentosBodegaDisponibles = $documentosBodegaDisponibles->whereHas('getWsOrdenCompra.getWsContrato',function ($query) use ($archivoAcepta) {
                                                                                $query->whereIn('rut_proveedor', $archivoAcepta->getProveedor->getTodosLosRut());
                                                                            });
            }

            $documentosBodegaDisponibles = $documentosBodegaDisponibles->get();

            if (count($documentosBodegaDisponibles) > 0 ) {
                foreach ($documentosBodegaDisponibles as $key => $documentoBodega) {
                    $opcionesGuias .= PHP_EOL.'<option id="guiaDespacho_'.$documentoBodega->id.'" ';
                    $opcionesGuias .= 'value="'.$documentoBodega->id.'" data-numdocumento="'.$documentoBodega->documento.'" ';
                    $opcionesGuias .= 'data-valordocumento="'.$documentoBodega->documento_total.'" ';
                    $opcionesGuias .= 'data-numoc="'.$documentoBodega->numero_orden_compra_mercado_publico.'" ';
                    $opcionesGuias .= '>GD N° '.$documentoBodega->documento.' - '.fecha_dmY($documentoBodega->fecha_carga).' ';
                    $opcionesGuias .= '- $'.formatoMiles($documentoBodega->documento_total).'';
                    $opcionesGuias .= ' | Prov. '.$documentoBodega->getWsOrdenCompra->getWsContrato->rut_proveedor;
                    $opcionesGuias .= '</option>'.PHP_EOL;
                }
            }
        }


        $recepcionHermes = null;
        $opcionesFacturasHermes = '';
        $opcionesGuiasHermes = '';
        /**
         * Solo buscar en hermes si no esta la recepcion disponible en el sistema
         */
        if ( $recepcionDisponible == false ) {

            // $recepcionHermes = \DB::connection('hermes')
            //                         ->table('ACTA_RECEPCION')
            //                         ->leftJoin('RESOLUCION', 'ACTA_RECEPCION.ID_RESOLUCION', '=', 'RESOLUCION.ID')
            //                         ->leftJoin('PJURIDICA', 'RESOLUCION.ID_PROVEEDOR', '=', 'PJURIDICA.ID')
            //                         ->leftJoin('PERSONA', 'PJURIDICA.ID_PERSONA', '=', 'PERSONA.ID')
            //                         ->where('ACTA_RECEPCION.CODIGO_DOCUMENTO', '=', $archivoAcepta->folio)
            //                         ->where('PERSONA.RUT_PERSONA', '=', explode('-',$archivoAcepta->getProveedor->rut)[0])
            //                         ->where('PERSONA.DIGITO_VERIFICADOR', '=', explode('-',$archivoAcepta->getProveedor->rut)[1])
            //                         ->where('ACTA_RECEPCION.ES_VALIDADO', 1)
            //                         ->select(
            //                             'ACTA_RECEPCION.CODIGO_DOCUMENTO',
            //                             'ACTA_RECEPCION.FECHA_INGRESO',
            //                             'ACTA_RECEPCION.NETO',
            //                             'ACTA_RECEPCION.DESCUENTO',
            //                             'ACTA_RECEPCION.TOTAL',
            //                             'ACTA_RECEPCION.CODIGO_PEDIDO',
            //                             'PERSONA.RUT_PERSONA',
            //                             'PERSONA.DIGITO_VERIFICADOR',
            //                             'PJURIDICA.RAZON_SOCIAL'
            //                         )
            //                         ->first();

            $recepcionHermes = HermesDocumento::where('rut_proveedor', $archivoAcepta->getProveedor->rut )
                                              ->where('folio_documento', $archivoAcepta->folio )
                                            //   ->where('documento_total', '!=', $this->monto_total )
                                              ->doesnthave('getRelacion')
                                              ->first();

            // if ( $recepcionHermes != null ) {
            if ( is_object($recepcionHermes) ) {

                $recepcionHermes->extra = null;
                if ( $archivoAcepta->monto_total > $recepcionHermes->documento_total ) {

                    $dif = $archivoAcepta->monto_total - $recepcionHermes->documento_total;
                    if ( $dif > 100 ) {
                        $recepcionHermes->extra = 'Es necesaria una NC por $ '.formatoMiles($dif);
                    }

                } else {

                    $dif = $recepcionHermes->documento_total - $archivoAcepta->monto_total;
                    if ( $dif > 100 ) {
                        $recepcionHermes->extra = 'Es necesaria una ND por $ '.formatoMiles($dif);
                    }
                    
                }

            }

            /**
             * Se trabajan las recepciones disponibles en hermes
             */
            if ( $archivoAcepta->id_proveedor != 249 ) {

                $recepcionesSifcon = WsDocumento::whereHas('getWsOrdenCompra.getWsContrato',function ($query) use ($archivoAcepta) {
                                                    $query->whereIn('rut_proveedor', $archivoAcepta->getProveedor->getTodosLosRut());
                                                });

            } else {
                $recepcionesSifcon = WsDocumento::all();
            }

            // se buscan recepciones en hermes que no esten en el sifcon
            // $recepcionesHermes = \DB::connection('hermes')
            //                         ->table('ACTA_RECEPCION')
            //                         ->leftJoin('RESOLUCION', 'ACTA_RECEPCION.ID_RESOLUCION', '=', 'RESOLUCION.ID')
            //                         ->leftJoin('PJURIDICA', 'RESOLUCION.ID_PROVEEDOR', '=', 'PJURIDICA.ID')
            //                         ->leftJoin('PERSONA', 'PJURIDICA.ID_PERSONA', '=', 'PERSONA.ID')
            //                         ->leftJoin('TIPO_DOCUMENTO', 'ACTA_RECEPCION.ID_TIPO_DOCUMENTO', '=', 'TIPO_DOCUMENTO.ID')
                                    
            //                         ->where('PERSONA.RUT_PERSONA', '=', explode('-', $archivoAcepta->getProveedor->rut)[0])
            //                         ->where('PERSONA.DIGITO_VERIFICADOR', '=', explode('-', $archivoAcepta->getProveedor->rut)[1])
            //                         // no considerar las recepciones en el sistema, segun el proveedor.
            //                         ->whereNotIn('ACTA_RECEPCION.CODIGO_DOCUMENTO', $recepcionesSifcon->pluck('documento')->toArray() )
            //                         ->where('ACTA_RECEPCION.ES_VALIDADO', 1)
            //                         ->whereYear( 'ACTA_RECEPCION.FECHA_INGRESO','>=', 2019 )
            //                         ->select(
            //                             'ACTA_RECEPCION.*',
            //                             'ACTA_RECEPCION.CODIGO_DOCUMENTO as FOLIO_DOCUMENTO',
            //                             'PERSONA.RUT_PERSONA',
            //                             'PERSONA.DIGITO_VERIFICADOR',
            //                             'PJURIDICA.RAZON_SOCIAL',
            //                             'TIPO_DOCUMENTO.NOMBRE as NOMBRE_DOCUMENTO',
            //                             'TIPO_DOCUMENTO.CODIGO_SII as CODIGO_SII_DOCUMENTO'
            //                         )
            //                         ->get();

            // if ( $recepcionesHermes->count() > 0 ) {

                $recepcionesHermes = HermesDocumento::where('rut_proveedor', $archivoAcepta->getProveedor->rut )
                                                    ->doesnthave('getRelacion')
                                                    ->get();

                foreach($recepcionesHermes as $recepcion) {

                    // $hermesDocumentoAux = HermesDocumento::where('rut_proveedor', $archivoAcepta->getProveedor->rut )
                    //                                      ->where('folio_documento', $recepcion->FOLIO_DOCUMENTO )
                    //                                      ->where('tipo_documento', $recepcion->NOMBRE_DOCUMENTO )
                    //                                      ->where('documento_neto', formato_entero($recepcion->NETO) )
                    //                                      ->where('documento_total', formato_entero($recepcion->TOTAL) )
                    //                                      ->where('fecha_ingreso_recepcion', $recepcion->FECHA_INGRESO )
                    //                                      ->doesnthave('getRelacion')
                    //                                      ->first();

                    // if ( ! is_object($hermesDocumentoAux) ) {

                        $datosRecepcion = array(
                            'numeroDocumento' => $recepcion->folio_documento,
                            'valorDocumento'  => formato_entero(formatoMiles( $recepcion->documento_total )),
                        );

                        $datosRecepcion = htmlspecialchars(json_encode($datosRecepcion), ENT_QUOTES, 'UTF-8');

                        if ( strpos($recepcion->tipo_documento, 'Factura') !== false ) {

                            $opcionesFacturasHermes .= PHP_EOL.'<option id="recepcionHermes_'.$recepcion->folio_documento.'" ';
                            $opcionesFacturasHermes .= 'value="'.$recepcion->folio_documento.'" ';
                            $opcionesFacturasHermes .= 'data-datosrecepcion="'.$datosRecepcion.'" ';
                            $opcionesFacturasHermes .= '>'.$archivoAcepta->getTipoDocumento->sigla.' N° '.$recepcion->folio_documento.' '; 
                            $opcionesFacturasHermes .= '- '.fecha_dmY($recepcion->fecha_ingreso_recepcion).' - $'.formatoMiles($recepcion->documento_total).'';
                            $opcionesFacturasHermes .= ' | Prov. '.$recepcion->rut_proveedor;
                            $opcionesFacturasHermes .= '</option>'.PHP_EOL;

                        } elseif ( strpos($recepcion->tipo_documento, 'Guia') !== false ) {

                            $opcionesGuiasHermes .= PHP_EOL.'<option id="guiaDespachoHermes_'.$recepcion->folio_documento.'" ';
                            $opcionesGuiasHermes .= 'value="'.$recepcion->folio_documento.'" data-numdocumento="'.$recepcion->folio_documento.'" ';
                            $opcionesGuiasHermes .= 'data-valordocumento="'.formato_entero(formatoMiles($recepcion->documento_total)).'" ';
                            $opcionesGuiasHermes .= '>GD N° '.$recepcion->folio_documento.' - '.fecha_dmY($recepcion->fecha_ingreso_recepcion).' ';
                            $opcionesGuiasHermes .= '- $'.formatoMiles($recepcion->documento_total).'';
                            $opcionesGuiasHermes .= ' | Prov. '.$recepcion->rut_proveedor;
                            $opcionesGuiasHermes .= '</option>'.PHP_EOL;

                        }

                    // }

                }

            // }
        }

        /**
         * Obtener opciones de los contratos del proveedor
         */
        $contratos = Contrato::vigenteConSaldoParaProveedor($archivoAcepta->id_proveedor)->get();

        $optionsContrato = '';
        if ( $contratos->count() > 0 ) {

            foreach ( $contratos as $contrato ) {
                $optionsContrato .= $contrato->getOpcionesParaSelect();
            }

        }

        /**
         * Obtener opciones de las ordenes de compra del proveedor
         */
        $ordenes = OrdenCompra::where('id_proveedor', $archivoAcepta->id_proveedor)
                              ->where('saldo_oc', '>', 0)
                              ->get();

        $optionsOc = '';
        if ( $ordenes->count() > 0 ) {

            foreach ( $ordenes as $oc ) {
                $optionsOc .= $oc->getOpcionesParaSelect();
            }

        }

                                
        return view('archivo_acepta.modal_carga_archivo')->with('archivoAcepta',$archivoAcepta)
                                                         ->with('numOrdenCompra',$numOrdenCompra)
                                                         ->with('tiposDocumento',$tiposDocumento)
                                                         ->with('modalidadesCompra',$modalidadesCompra)
                                                         ->with('tiposAdjudicacion',$tiposAdjudicacion)
                                                         ->with('tiposInforme',$tiposInforme)
                                                         ->with('usuarios',$usuarios)
                                                         ->with('referentesTecnicos',$referentesTecnicos)
                                                         ->with('tiposArchivo',$tiposArchivo)
                                                         ->with('documentosProveedor',$documentosProveedor)
                                                         ->with('opciones',$opciones)
                                                         ->with('opcionesGuias',$opcionesGuias)
                                                         ->with('modalidadCompraDefecto',$modalidadCompraDefecto)
                                                         ->with('tipoDoc', $tipoDoc)
                                                         ->with('docsRechazadosAceptaProv',$docsRechazadosAceptaProv)
                                                         ->with('recepcionHermes', $recepcionHermes)
                                                         ->with('opcionesFacturasHermes', $opcionesFacturasHermes)
                                                         ->with('opcionesGuiasHermes', $opcionesGuiasHermes)
                                                         ->with('optionsContrato', $optionsContrato)
                                                         ->with('optionsOc', $optionsOc);
    }

    public function postCargaArchivoAceptaToDocumento(Request $request)
    {   
        // sleep(5);
        // dd('Hola',$request->all(), $request->input('directo_por_devengar'));

        set_time_limit ( 0 );
        ini_set('max_execution_time', 0);

        // Mensajes de los validadores
        $messages = [
            'required'           => 'Debe ingresar el :attribute',
            'numeric'            => 'El :attribute debe solo contener números',
            'max'                => 'El :attribute no debe exeder los :max caracteres',
            'min'                => 'El :attribute debe tener minimo :min caracteres',
            'date_format'        => 'La :attribute no tiene el formato correcto: día/mes/año',
            'reemplaza.required' => 'Debe seleccionar una factura'
        ];

        if ( $request->input('tipo_documento') == 4 || $request->input('tipo_documento') == 5 || $request->input('tipo_documento') == 10 || $request->input('tipo_documento') == 11 || $request->input('tipo_documento') == 8 ) {
            //validador de los input del formulario
            $validator = \Validator::make($request->all(), [
                'nombre_proveedor'      => 'required|numeric',
                'tipo_documento'        => 'required|numeric',
                'numero_documento'      => 'required',
                'modalidad_compra'      => 'required|numeric',
                'tipo_adjudicacion'     => 'required|numeric',
                'numero_orden_compra'   => 'required',
                'fecha_documento'       => 'required|date_format:"d/m/Y"',
                'tipo_informe'          => 'required|numeric',
                'fecha_recepcion'       => 'required|date_format:"d/m/Y"',
                'valor_total_documento' => 'required',
                'reemplaza'             => 'required',
                'periodo'               => 'required|date_format:"m/Y"',
            ], $messages);

        } else {
            //validador de los input del formulario
            $validator = \Validator::make($request->all(), [
                'nombre_proveedor'      => 'required|numeric',
                'tipo_documento'        => 'required|numeric',
                'numero_documento'      => 'required',
                'modalidad_compra'      => 'required|numeric',
                'tipo_adjudicacion'     => 'required|numeric',
                'numero_orden_compra'   => 'required',
                'fecha_documento'       => 'required|date_format:"d/m/Y"',
                'tipo_informe'          => 'required|numeric',
                'fecha_recepcion'       => 'required|date_format:"d/m/Y"',
                'valor_total_documento' => 'required',
                'periodo'               => 'required|date_format:"m/Y"',
            ], $messages);
        }

        //Si contiene errores se devuelven todos los errores, de lo contrario guarda en la base de datos
        if ( $validator->fails() ) {
            return response()->json($validator->errors(),400);
        } else {

            $datos = $this->validarNumOrdenCompraAndNumLicitacion($request);

            /**
             * Comprobar si se ha seleccionado contrato y orden de compra
             * comprobrar si el monto del documento es igual o menor a los saldos
             * hacer el resto de validaciones.
             */
            $datosContrato = array();
            $contrato = null;
            $oc = null;
            $totalDocumento = formato_entero($request->input('valor_total_documento'));

            if ( $request->input('orden_compra') != null ) {

                $oc = OrdenCompra::findOrFail( $request->input('orden_compra') );

            }

            if ( $request->input('contrato') != null ) {

                $contrato = Contrato::findOrFail($request->input('contrato'));
            }

            if ( is_object($oc) && is_object($contrato) ) {

                if ( $oc->id_contrato != $contrato->id ) {

                    $datosContrato['mensaje'] = 'La Orden de Compra seleccionada debe pertenecer al Contrato seleccionado.';
                    return response()->json($datosContrato, 400);

                }

            }

            if ( ! is_object($oc) && is_object($contrato) ) {
                // Verificar si el contrato tiene OC disponibles con saldo.
                $ocContrato = OrdenCompra::where('id_contrato', $contrato->id)
                                         ->where('saldo_oc', '>', 0)
                                         ->first();

                if ( is_object($ocContrato) ) {

                    $datosContrato['mensaje'] = 'Debe seleccionar una Orden de Compra del Contrato.';
                    return response()->json($datosContrato, 400);

                }

            }

            if ( is_object($oc) && ! is_object($contrato) ) {
                if ( $oc->id_contrato != null ) {

                    $datosContrato['mensaje'] = 'Debe seleccionar el Contrato de la Orden de Compra seleccionada.';
                    return response()->json($datosContrato, 400);

                }
            }

            /**
             * Validar que el monto del documento no supere los saldos de la OC ni del Contrato.
             */
            // Validar con Contrato
            if ( is_object($contrato) ) {

                if ( $request->input('tipo_documento') == 4 || $request->input('tipo_documento') == 10 ) {
                    // NC y CCC

                    if ( $contrato->saldo_preventivo != null || $contrato->saldo_correctivo != null ) {
                        if ( $request->input('montoOcupar') == 'preventivo' && $totalDocumento + $contrato->saldo_preventivo > $contrato->monto_preventivo ) {

                            $datosContrato['mensaje'] = 'El Valor Total más el Saldo Preventivo no puede superar el Monto Preventivo del Contrato.';
                            return response()->json($datosContrato, 400);
                            
                        }
                        if ( $request->input('montoOcupar') == 'correctivo' && $totalDocumento + $contrato->saldo_correctivo > $contrato->monto_correctivo ) {

                            $datosContrato['mensaje'] = 'El Valor Total más el Saldo Correctivo no puede superar el Monto Correctivo del Contrato.';
                            return response()->json($datosContrato, 400);

                        }
                    }

                    if ( $totalDocumento + $contrato->saldo_contrato > $contrato->monto_contrato ) {

                        $datosContrato['mensaje'] = 'El Valor Total más el Saldo Contrato no puede superar el Monto del Contrato.';
                        return response()->json($datosContrato, 400);

                    }

                } else {

                    if ( $contrato->saldo_preventivo != null || $contrato->saldo_correctivo != null ) {
                        if ( $request->input('montoOcupar') == 'preventivo' && $totalDocumento > $contrato->saldo_preventivo ) {

                            $datosContrato['mensaje'] = 'El Valor Total no puede superar el Saldo Preventivo del Contrato.';
                            return response()->json($datosContrato, 400);

                        }
                        if ( $request->input('montoOcupar') == 'correctivo' && $totalDocumento > $contrato->saldo_correctivo ) {

                            $datosContrato['mensaje'] = 'El Valor Total no puede superar el Saldo Correctivo del Contrato.';
                            return response()->json($datosContrato, 400);

                        }
                    }

                    if ( $totalDocumento > $contrato->saldo_contrato ) {

                        $datosContrato['mensaje'] = 'El Valor Total no puede superar el Saldo Contrato.';
                        return response()->json($datosContrato, 400);

                    }

                }
                
            }

            // Validar OC
            if ( is_object($oc) ) {

                if ( $request->input('tipo_documento') == 4 || $request->input('tipo_documento') == 10 ) {
                    if ( $totalDocumento + $oc->saldo_oc  > $oc->monto_oc ) {
                        
                        $datosContrato['mensaje'] = 'El Valor Total más el Saldo OC no puede superar el Monto OC.';
                        return response()->json($datosContrato, 400);

                    }
                } else {
                    if ( $totalDocumento > $oc->saldo_oc ) {
                        
                        $datosContrato['mensaje'] = 'El Valor Total no puede superar el Saldo OC.';
                        return response()->json($datosContrato, 400);

                    }
                }

            }

            
            if ( $datos['estado_documento'] == 'success' ) {

                // Se valida que para el proveedor y el tipo de documento, no se repita el numero documento
                // Se agrega a la validacion los documentos eliminados, ya que pueden ser recuperados.
                $documentoAux = Documento::withTrashed()
                                         ->where('id_proveedor', $request->input('nombre_proveedor'))
                                         ->where('id_tipo_documento', $request->input('tipo_documento'))
                                         ->where('numero_documento', $request->input('numero_documento'))
                                         ->first();

                if ( !is_object($documentoAux) ) {

                    $newDocumento = new Documento($request);
                    $mensajeDocumento = 'Carga exitosa del Documento al Sistema.';

                    // Se crean los items presupuestarios
                    if ( $request->input('valor_item') ) {

                        foreach ($request->input('valor_item') as $id_item => $valor) {

                            if ( formato_entero($valor) > 0 ) {

                                $newDevengo = new Devengo();
                                $newDevengo->id_documento = $newDocumento->id;
                                $newDevengo->id_item_presupuestario = $id_item;
                                $newDevengo->monto = formato_entero($valor);
                                $newDevengo->save();

                            }

                        }

                    }

                    // Se cambia el estado del archivoacepta
                    $archivoAcepta = ArchivoAcepta::findOrFail($request->input('id_archivo_acepta'));
                    $archivoAcepta->cargado = 1;
                    $archivoAcepta->save();

                    if ( $archivoAcepta->id_user_responsable != null ) {

                        $newDocumento->id_responsable = $archivoAcepta->id_user_responsable;
                        $newDocumento->save();

                    }

                    $datos['idArchivoAcepta'] = $archivoAcepta->id;

                    $archivo = $request->file('archivo');
                    if ( $archivo != null ) {

                        $nombreOriginalArchivo = str_replace('.pdf','',$archivo->getClientOriginalName() );
                        $arreglo_nombre = explode(".", $archivo->getClientOriginalName() );
                        $extension = strtolower( end( $arreglo_nombre ) );

                        if ($extension != 'pdf') {

                            $datos['mensaje_archivo'] = 'Extension incorrecta del archivo';
                            $datos['estado_archivo'] = 'error';

                        } else {

                            // Se trabajan las rutas del archivo, se guarda en el proyecto al final de la funcion
                            $nombre = $nombreOriginalArchivo.'_'.date('Y_m_d_G_i_s');
                            $rutaArchivo = '/documento/'.$newDocumento->id.'/'.$nombre.'.pdf';
                            $pathArchivo = public_path().'/documento/'.$newDocumento->id.'/';

                            $newArchivo = new Archivo();
                            $newArchivo->id_documento = $newDocumento->id;
                            $newArchivo->id_tipo_archivo = $request->input('tipo_archivo');

                            $newArchivo->nombre = $nombre;
                            $newArchivo->nombre_original = $nombreOriginalArchivo;
                            $newArchivo->ubicacion = "documento/".$newDocumento->id."/".$newArchivo->nombre.".pdf";
                            $newArchivo->extension = "pdf";
                            $newArchivo->peso = $archivo->getSize();
                            $newArchivo->save();

                            $datos['mensaje_archivo'] = 'Se ha guardado correctamente el archivo';
                            $datos['estado_archivo'] = 'success';

                            $archivo->move( $pathArchivo , $nombre.'.pdf' ); // Se guarda el archivo
                            
                        }
                        
                    } else {
                        $datos['mensaje_archivo'] = '';
                        $datos['estado_archivo'] = '';
                    }

                    /**
                     * Para conectar con documentos de recepcion, si se ha seleccinado uno
                     */
                    if ( $request->input('doc_disponible_bodega') != null || $request->input('guias_disponible_bodega') != null ) {
                        
                        if ( $request->input('doc_disponible_bodega') != null ) {

                            // Un solo documento (No es guia despacho)
                            $wsDocumento = WsDocumento::findOrFail( $request->input('doc_disponible_bodega') );
                            $this->guardarRelaciones($newDocumento, $wsDocumento);

                        }

                        if ( $request->input('guias_disponible_bodega') != null ) {

                            foreach ($request->input('guias_disponible_bodega') as $index => $idGuiaBodega) {

                                $wsDocumento = WsDocumento::findOrFail( $idGuiaBodega );
                                $this->guardarRelaciones($newDocumento, $wsDocumento);

                            }

                        }

                        $newDocumento->actualizaValidacionDocumentosRelacionados();    
                        $mensajeDocumento .= '<br><strong style="colo:black !important;">Se han conectado exitosamente los Documentos.</strong>';                        
                    }
                    
                    /**
                     * Para conectar con recepciones de hermes.
                     * No debe tener relacion con recepciones del sistema.
                     */
                    if ( $newDocumento->getUnaRelacionRecepcionValidada == null ) {
                        // dd('no tiene relacion', $request->all());

                        if ( $request->input('factura_recepcion_hermes') != null || $request->input('guias_disponible_hermes') != null ) {
                            /**
                             * Se deben crear los documento hermes correspondiente y luego las relaciones
                             */

                            $observacionMatchHermes = '';
                            if ( $request->input('factura_recepcion_hermes') != null ) {
                                relacionRecepcionesNoValidadas::validarGuardarRelacionesHermes($request->input('factura_recepcion_hermes'), $newDocumento, $mensajeDocumento, $observacionMatchHermes);
                            }
    
                            if ( $request->input('guias_disponible_hermes') != null ) {

                                foreach ( $request->input('guias_disponible_hermes') as $index => $folioDoc ) {
                                    relacionRecepcionesNoValidadas::validarGuardarRelacionesHermes($folioDoc, $newDocumento, $mensajeDocumento, $observacionMatchHermes);
                                } 

                            }

                            /**
                             * Se agrega informacion a la observacion del documento, respecto a la relacon creada con las recepciones de hermes
                             */
                            if ( $observacionMatchHermes != '' ) {

                                if ( $newDocumento->getRelacionRecepcionesNoValidadas->count() == 1 ) {

                                    $observacion = 'Documento relacionado con recepcion de Hermes, esperando a que este disponible en el sistema'.PHP_EOL;
                                    $observacion .= 'N° del documento recepcion de Hermes: '.$observacionMatchHermes;

                                } elseif ( $newDocumento->getRelacionRecepcionesNoValidadas->count() > 1 ) {

                                    $observacion = 'Documento relacionado con recepciones de Hermes, esperando a que esten disponibles en el sistema'.PHP_EOL;
                                    $observacion .= 'N° de los documentos recepciones de Hermes: '.$observacionMatchHermes;

                                }
                                
                                if ( $newDocumento->observacion == null || $newDocumento->observacion == '' ) {
                                    $newDocumento->observacion .= $observacion;
                                } else {
                                    $newDocumento->observacion .= PHP_EOL.$observacion;
                                }
                                
                                $newDocumento->save();

                            }
                            
                        } // if facturas recepcion hermes && guias disponibles hermes != null

                    } // if $newDocumento->getUnaRelacionRecepcionValidada == null

                    /**
                     * Evaluar la relacion del documento con recepcion.
                     * Para pasar a documento con problema o al pendiente de validación
                     */
                    if ( $newDocumento->setProblemaDocumento() == true ) {
                        $mensajeDocumento .= '<br><strong style="colo:black !important;">El documento a quedado con problema, debido a la diferencia en los montos con la recepción.</strong>';
                    }

                    /**
                     * Conectar con contrato, oc y modificar los montos
                     */
                    $newDocumento->unirConContratoOrdenCompra($contrato, $oc, $request);

                    $datos['mensaje_documento'] = $mensajeDocumento;
                    $datos['estado_documento'] = 'success';

                } else {

                    $datos['mensaje_documento'] = 'N° Documento repetido para el Proveedor y Tipo Documento seleccionados';
                    $datos['estado_documento'] = 'error';

                    if ( $documentoAux->trashed() ) {
                        $datos['mensaje_documento'] = 'N° Documento repetido para el Proveedor y Tipo Documento seleccionados.<br><br> El Documento se encuentra eliminado.<br> Observaciones : <br>'.$documentoAux->observacion;
                    }
                    
                }
            }
        }

        return response()->json($datos,200);

    }

    /**
     * Guarda las relaciones con las recepciones disponibles en el sistema
     */
    public function guardarRelaciones(Documento $newDocumento, WsDocumento $wsDocumento)
    {
        
        // Se crea la relacion
        $newRelacion = new RelacionDocumentoWsDocumento();
        $newRelacion->id_documento = $newDocumento->id;
        $newRelacion->id_ws_documento = $wsDocumento->id;
        $newRelacion->id_user_relacion = \Auth::user()->id;
        $newRelacion->updated_at = NULL;
        $newRelacion->save();
        // Se paasa el id correspondiente a cada documento para la conexion
        $newDocumento->id_relacion = $newRelacion->id;
        $newDocumento->validar();
        $newDocumento->updated_at = NULL;
        $newDocumento->save();
        $wsDocumento->id_relacion = $newRelacion->id;
        $wsDocumento->save();

    }

    /**
     * Funcion que valia el numero de orden de compra y el numero licitacion, segun los formatos establecidos.
     * Retorna el array datos que se envia por json a las vistas
    */
    public function validarNumOrdenCompraAndNumLicitacion($request)
    {
        $datos = array(
            'mensaje_documento' => 'Todo bien',
            'estado_documento' => 'success',
        );
        // Validar N°Documento Compra o N° Orden Compra y N° Licitación
        if ( $request->input('modalidad_compra') == 1 && substr( $request->input('numero_orden_compra'), 0, 5) != "1641-") {
            $datos = array(
                'mensaje_documento' => 'El "N° Orden de Compra" debe iniciar con "1641-"',
                'estado_documento' => 'error',
            );
        }

        if ( $request->input('numero_licitacion') != '' && substr( $request->input('numero_licitacion'), 0, 5) != "1641-") {
            $datos = array(
                'mensaje_documento' => 'El "N° Licitación" debe iniciar con "1641-"',
                'estado_documento' => 'error',
            );
        }

        return $datos;
    }

    public function getUpload()
    {
        return view('archivo_acepta.index_masivo');
    }

    public function postUpload(Request $request)
    {
        set_time_limit ( 0 );
        ini_set('max_execution_time', 0);

        $archivo = $request->file('archivo');
        $excel = Excel::selectSheetsByIndex(0)->load($archivo)->get();

        $log = '';
        // $archivoLog = fopen("log-archivos-upload.txt", "w");

        foreach ($excel as $key => $fila) {
            // dd('holo',$fila);

            // se crea el archivo
            $newArchivo = new Archivo();
            $newArchivo->id = $fila->id;
            $newArchivo->id_tipo_ingreso = trim($fila->id_tipo_ingreso);
            $newArchivo->id_documento = trim($fila->id_factura);
            $newArchivo->id_tipo_archivo = trim($fila->id_tipo_archivo);
            $newArchivo->nombre = trim($fila->gl_nombre);
            $newArchivo->nombre_original = trim($fila->gl_nombre_original);
            $newArchivo->ubicacion = '10.6.3.43/factura_/app/file/'.trim($fila->gl_ubicacion);
            $newArchivo->extension = trim($fila->gl_extension);
            $newArchivo->peso = trim($fila->nr_peso);
            $newArchivo->cargado = 1;

            $newArchivo->save();

            if ($fila->bo_eliminado == 1) {
                $newArchivo->delete();
            }

            $log = ($key + 1).'.- Se registro correctamente El Archivo : id '.$fila->id.' .'.PHP_EOL;
            // fwrite($archivoLog, $log);    

        }

        // fclose($archivoLog);
        $datos = array(
            'mensaje' => 'Carga de archivos exitosa.',
            'estado' => 'success',
        );

        return response()->json($datos,200);
    }

    public function getUploadExcel()
    {
        return view('archivo_acepta.index_masivo_excel');
    }

    public function postUploadExcel(Request $request)
    {
        set_time_limit ( 0 );
        ini_set('max_execution_time', 0);

        $archivo = $request->file('archivo');
        $excel = Excel::selectSheetsByIndex(0)->load($archivo)->get();

        $log = '';
        // $archivoLog = fopen("log-archivos-acepta-excel-upload.txt", "w");

        foreach ($excel as $key => $fila) {
             

            // se crea el archivo
            $newArchivoAceptaExcel = new ArchivoAceptaExcel();
            $newArchivoAceptaExcel->id = $fila->id;
            $newArchivoAceptaExcel->ingreso = $fila->fc_ingreso;
            $newArchivoAceptaExcel->id_usuario = trim($fila->id_usuario);
            $newArchivoAceptaExcel->nombre = trim($fila->gl_nombre);
            $newArchivoAceptaExcel->nombre_original = trim($fila->gl_nombre_original);
            $newArchivoAceptaExcel->ubicacion = '10.6.3.43/factura_/app/file/'.trim($fila->gl_ubicacion);
            $newArchivoAceptaExcel->extension = trim($fila->gl_extension);
            $newArchivoAceptaExcel->peso = trim($fila->nr_peso);
            $newArchivoAceptaExcel->cargado = 1;
            
            // dd('holo',$fila, $newArchivoAceptaExcel );
            $newArchivoAceptaExcel->save();

            if ($fila->bo_eliminado == 1) {
                $newArchivoAceptaExcel->delete();
            }

            $log = ($key + 1).'.- Se registro correctamente El Archivo Acepta Excel : '.$fila->gl_nombre.' .'.PHP_EOL;
            // fwrite($archivoLog, $log);    

        }

        // fclose($archivoLog);
        $datos = array(
            'mensaje' => 'Carga de Archivos Acepta Excel exitosa.',
            'estado' => 'success',
        );

        return response()->json($datos,200);
    }


    public function getUploadExcelArchivo()
    {
        return view('archivo_acepta.index_masivo_excel_archivo');
    }

    public function postUploadExcelArchivo(Request $request)
    {
        set_time_limit ( 0 );
        ini_set('max_execution_time', 0);

        $archivo = $request->file('archivo');
        $excel = Excel::selectSheetsByIndex(0)->load($archivo)->get();

        $log = '';
        // $archivoLog = fopen("log-archivos-acepta-upload.txt", "w");

        foreach ($excel as $key => $fila) {

            // Se trabaja la tabla de factura, se guardan distintos documentos
            $id_tipo_documento = 0;
            switch ($fila->gl_tipo) {
                case 33:
                    $id_tipo_documento = 1;
                    break;
                case 34:
                    $id_tipo_documento = 1;
                    break;
                case 39:
                    $id_tipo_documento = 7;
                    break;
                case 46:
                    $id_tipo_documento = 7;
                    break;
                case 56:
                    $id_tipo_documento = 5;
                    break;
                case 61:
                    $id_tipo_documento = 4;
                    break;
                default:
                    $id_tipo_documento = 0;
                    break;
            }

            // se obtiene el proveedor;
            $proveedor = Proveedor::where('rut', trim( str_replace( ".", "", $fila->gl_emisor ) ) )
                                  ->first();
            

            if ( is_object($proveedor) && $id_tipo_documento != 0 ) {
                $documento = Documento::where('id_proveedor',$proveedor->id)
                                    ->where('numero_documento', trim($fila->gl_folio) ) 
                                    ->where('id_tipo_documento',$id_tipo_documento)
                                    ->first();
                /** Se verifica si el documento se encuentra en la base de datos
                 * Si se encuentra en la base de datos: 
                 *  - no se guarda el archivo acepta.
                 *  - se busca si tiene el archivo (PDF), de no ser así se extrae de acepta
                 * Si no se encuentra en la base de datos:
                 *  - se guarda el archivo acepta
                 *  */ 
            
                if ( is_object($documento) ) {

                    $archivo = Archivo::where('id_documento',$documento->id)
                                        ->where('id_tipo_archivo',1)
                                        ->first();
                    
                    if ( !is_object($archivo) ) {
                        // se deben generar 'string' para la ubicacion del archivo
                        $newArchivo = new Archivo();
                        $newArchivo->id_documento = $documento->id;
                        $newArchivo->id_tipo_archivo = 1;
                        $newArchivo->nombre = 'archivoAcepta_'.date('Y_m_d_G_i_s');
                        $newArchivo->nombre_original = $newArchivo->nombre;
                        $newArchivo->ubicacion = "documento/".$documento->id."/".$newArchivo->nombre.".pdf";
                        $newArchivo->extension = "pdf";
                        $newArchivo->peso = 0;
                        $newArchivo->save();

                        // Para guardar el pdf en el proyecto, faltan pruebas y arreglar el path
                        
                        $content = file_get_contents('http://windte1806.acepta.com/ca4webv3/PdfViewMedia?url='.$fila->gl_uri.'&menuTitle=Papel%2520Carta');
                        if ( !file_exists(public_path().'/documento/'.$documento->id) ) {
                            mkdir(public_path().'/documento/'.$documento->id, 0775, true);
                            chmod(public_path().'/documento/'.$documento->id, 0775);
                        }
                        $file = fopen( public_path().'/documento/'.$documento->id.'/'.$newArchivo->nombre.'.pdf', 'a');
                        fwrite($file, $content);
                        fclose($file);
                    }
                        
                } else {
                    // Se valida que el archivo (de la fila del excel) no se encuentra en la base de datos, en la tabla archivo acepta.
                    $archivoAcepta = ArchivoAcepta::where('tipo',$fila->gl_tipo)
                                                ->where('folio',$fila->gl_folio)
                                                ->where('emisor',$fila->gl_emisor)
                                                ->first();
                    //dd($archivoAcepta,$fila);
                    if (!is_object($archivoAcepta)) {
                        // se crea el archivo
                        $newArchivoAcepta = new ArchivoAcepta();

                        $newArchivoAcepta->id = $fila->id;
                        $newArchivoAcepta->id_archivo_excel = $fila->id_archivo_excel;
                        $newArchivoAcepta->tipo = $fila->gl_tipo;
                        $newArchivoAcepta->tipo_documento = $fila->gl_tipo_documento;

                        if ( $id_tipo_documento != 0 ) {
                            $newArchivoAcepta->id_tipo_documento = $id_tipo_documento;
                        }
                        
                        $newArchivoAcepta->folio = $fila->gl_folio;
                        $newArchivoAcepta->emisor = $fila->gl_emisor;
                        $newArchivoAcepta->razon_social_emisor = $fila->gl_razon_social_emisor;

                        if ( is_object($proveedor) ) {
                            $newArchivoAcepta->id_proveedor = $proveedor->id;
                        }

                        $newArchivoAcepta->receptor = $fila->gl_receptor;
                        $newArchivoAcepta->publicacion = $fila->gl_publicacion;
                        $newArchivoAcepta->emision = $fila->gl_emision;
                        // Cambiar tipo de dato de la BD
                        $newArchivoAcepta->monto_neto = $fila->gl_monto_neto;
                        $newArchivoAcepta->monto_exento = $fila->gl_monto_exento;
                        $newArchivoAcepta->monto_iva = $fila->gl_monto_iva;
                        $newArchivoAcepta->monto_total = $fila->gl_monto_total;
                        // Solo cambiar los montos... revisar si se deben cambiar fechas u otros
                        $newArchivoAcepta->impuestos = $fila->gl_impuestos;
                        $newArchivoAcepta->estado_acepta = $fila->gl_estado_acepta;
                        if ( $fila->gl_estado_sii == '') {
                            $newArchivoAcepta->estado_sii = '  ';
                        } else {
                            $newArchivoAcepta->estado_sii = $fila->gl_estado_sii;
                        }
                        
                        $newArchivoAcepta->estado_intercambio = $fila->gl_estado_intercambio;
                        $newArchivoAcepta->informacion_intercambio = $fila->gl_informacion_intercambio;
                        $newArchivoAcepta->uri = $fila->gl_uri;
                        $newArchivoAcepta->referencias = $fila->gl_referencias;
                        $newArchivoAcepta->mensaje_nar = $fila->gl_mensaje_nar;
                        $newArchivoAcepta->uri_nar = $fila->gl_uri_nar;
                        $newArchivoAcepta->uri_arm = $fila->gl_uri_arm;
                        $newArchivoAcepta->fecha_arm = $fila->gl_fecha_arm;
                        $newArchivoAcepta->condicion_pago = $fila->gl_condicion_pago;
                        $newArchivoAcepta->fecha_vencimiento = $fila->gl_fecha_vencimiento;
                        $newArchivoAcepta->estado_cesion = $fila->gl_estado_cesion;
                        $newArchivoAcepta->url_correo_cesion = $fila->gl_url_correo_cesion;
                        $newArchivoAcepta->fecha_cesion = $fila->gl_fecha_cesion;
                        $newArchivoAcepta->fecha_recepcion_sii = $fila->gl_fecha_recepcion_sii;
                        $newArchivoAcepta->estado_reclamo_mercaderia = $fila->gl_estado_reclamo_mercaderia;
                        $newArchivoAcepta->fecha_reclamo_mercaderia = $fila->gl_fecha_reclamo_mercaderia;
                        $newArchivoAcepta->estado_reclamo_contenido = $fila->gl_estado_reclamo_contenido;
                        $newArchivoAcepta->fecha_reclamo_contenido = $fila->gl_fecha_reclamo_contenido;
                        $newArchivoAcepta->estado_nar = $fila->gl_estado_nar;
                        $newArchivoAcepta->fecha_nar = $fila->gl_fecha_nar;
                        $newArchivoAcepta->mensaje_nar_2 = $fila->gl_mensaje_nar_2;
                        $newArchivoAcepta->cargado = $fila->bo_cargado;
                        $newArchivoAcepta->save();

                        // dd($proveedor,$fila,$id_tipo_documento,$newArchivoAcepta);

                        $log = ($key + 1).'.- Se registro correctamente El Archivo Acepta : '.$fila->gl_folio.' .'.PHP_EOL;
                        // fwrite($archivoLog, $log);
                    }
                }

            }
        }

        // fclose($archivoLog);
        $datos = array(
            'mensaje' => 'Carga de Archivos Acepta Excel exitosa.',
            'estado' => 'success',
        );

        return response()->json($datos,200);
    }

    /**
     * Obtiene el numero de documentos acepta que esta asociado a una recepcion en bodega
     * Es utilizada para las notificaciones de la cabecera del sistema
     */
    public function getNumDocumentosConRecepcionPorConfirmar()
    {
        $datoGeneral = DatoGeneral::find(1);
        $numDoc = 0;
        
        if ( is_object($datoGeneral)  ) {
            $numDoc = $datoGeneral->archivos_acepta_con_recepcion;
        }

        $liReemplaza = '<a href="'.asset('archivos_acepta/listado/bodega').'">';
        $liReemplaza .= '<i class="fas fa-eye fa-bounce fa-lg" style="color: #f2002b;"></i>';
        $liReemplaza .= 'Hay '.$numDoc.' Documentos Acepta con &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Recepción';
        $liReemplaza .= '</a>';

        $datos = array (
            'estado' => 'success',
            'numDoc' => $numDoc,
            'liReemplaza' => $liReemplaza,
        );

        return response()->json($datos,200);
    }

    /**
     * Muestra los archivos acepta que tienen conexion con documentos recepcionado
     * en bodega.
     * Se puede visualizar la posible conexion, pero es necesario pasar el documenco de
     * acepta a documento en el sistema para concretarla.
     * Ademas se dan las mismas opciones que en el listado(grilla) normal
     */
    public function getArchivosConBodega()
    {
        if (!\Entrust::can(['ver-listado-acepta-con-recepcion','ver-posible-conexion-acepta-recepcion','rechazar-documento-acepta'])) {
            return \Redirect::to('home');
        }

        $tiposDocumento = TipoDocumento::whereNotIn('id',[2,8])->get();
        $proveedores = Proveedor::all();
        
        return view('archivo_acepta.index_listado_por_confirmar')->with('tiposDocumento',$tiposDocumento)
                                                                 ->with('proveedores',$proveedores);
    }

    public function dataTableConRecepcion(Request $request)
    {

        set_time_limit(0);
        ini_set('max_execution_time', 0);

        $formJSON = json_decode($request->get('form'));
        $form = construyeFormulario($formJSON);
        
        $fechaInicio = fecha_Y_m_d($form->filtro_fecha_inicio);
        $fechaTermino = fecha_Y_m_d($form->filtro_fecha_termino);

        $archivosAcepta = ArchivoAcepta::join('proveedores','archivos_acepta.id_proveedor','=','proveedores.id')
                        ->join('tipos_documento','archivos_acepta.id_tipo_documento','tipos_documento.id')
                        ->join('ws_contrato','proveedores.rut','=','ws_contrato.rut_proveedor')
                        ->join('ws_orden_compra','ws_contrato.id','=','ws_orden_compra.id_contrato')
                        ->join('ws_documento','ws_orden_compra.id','=','ws_documento.id_orden_compra')

                        ->whereColumn('tipos_documento.nombre','LIKE','ws_documento.tipo_documento')
                        ->whereColumn('archivos_acepta.folio','=','ws_documento.documento')
                        ->whereColumn('archivos_acepta.monto_total','=','ws_documento.documento_total')
                        // ->where('archivos_acepta.id_relacion',null)
                        ->where('ws_documento.id_relacion', null)
                        ->where('archivos_acepta.cargado', 0)
                        ->where('archivos_acepta.id_user_rechazo', null);
                        

        if ( $form->filtro_numero_documento == null ) {

            $archivosAcepta = $archivosAcepta->where('archivos_acepta.created_at','>=',$fechaInicio.' 00:00:00')
                                             ->where('archivos_acepta.created_at','<=',$fechaTermino.' 23:59:59');

        } else {

            $archivosAcepta = $archivosAcepta->where('archivos_acepta.folio', trim($form->filtro_numero_documento) );

        }

        if ( isset($form->filtro_proveedor) ) {
            $archivosAcepta = $archivosAcepta->WhereIn('archivos_acepta.id_proveedor', $form->filtro_proveedor );
        }

        if ( isset($form->filtro_tipo_documento) ) {
            $archivosAcepta = $archivosAcepta->WhereIn('archivos_acepta.id_tipo_documento', $form->filtro_tipo_documento);
        }

        if ( \Entrust::hasRole('usuario-ingreso') ) {
            $archivosAcepta = $archivosAcepta->where(function ($query){
                                                $query->where('archivos_acepta.id_user_responsable', \Auth::user()->id)
                                                      ->orWhere('archivos_acepta.id_user_responsable', null);
                                            });
        }

        $archivosAcepta = $archivosAcepta->select(
                            'archivos_acepta.id as id_archivo_acepta',
                            'archivos_acepta.folio',
                            'archivos_acepta.uri',
                            'archivos_acepta.emision',
                            'archivos_acepta.created_at',
                            'archivos_acepta.id_user_responsable',
                            'archivos_acepta.estado_reclamo_contenido',
                            'archivos_acepta.reclamado',
                            'archivos_acepta.publicacion',
                            'proveedores.rut as rut_proveedor',
                            'proveedores.nombre as nombre_proveedor',
                            'tipos_documento.nombre as nombre_tipo_documento',
                            'ws_contrato.nombre_proveedor as ws_nombre_proveedor',
                            'ws_contrato.rut_proveedor as ws_rut_proveedor',
                            'ws_contrato.id as id_ws_contrato',
                            'ws_orden_compra.id as id_ws_orden_compra',
                            'ws_documento.id as id_ws_documento',
                            'ws_documento.tipo_documento as ws_tipo_documento',
                            'ws_documento.documento as ws_numero_documento'
                        )
                        ->get();

        $data = [];
        $verFacturaDocumentoAcepta = \Entrust::can('ver-factura-documento-acepta');
        $verPosibleConexionAceptaRecepcion = \Entrust::can('ver-posible-conexion-acepta-recepcion');
        $cargaDocumentoAceptaAlSistema = \Entrust::can('carga-documento-acepta-al-sistema');
        $rechazarDocumentoAcepta = \Entrust::can('rechazar-documento-acepta');
        $isAdmin = \Entrust::hasRole(['propietario','administrador']);

        foreach ( $archivosAcepta as $archivo ) {
            $data[] = $archivo->getDatosConRecepcion(
                $verPosibleConexionAceptaRecepcion, $verFacturaDocumentoAcepta, $cargaDocumentoAceptaAlSistema, $rechazarDocumentoAcepta, $isAdmin
            );
        }

        $respuesta = array(
            "data" => $data
        );

        return json_encode($respuesta);

    }

    public function postFiltroConRecepcion(Request $request)
    {
        // dd($request->all(), "con recepcion");

        if (!\Entrust::can(['ver-listado-acepta-con-recepcion','ver-posible-conexion-acepta-recepcion','rechazar-documento-acepta'])) {
            return \Redirect::to('home');
        }
        
        set_time_limit(0);
        ini_set('max_execution_time', 0);

        $fechaInicio = fecha_Y_m_d($request->input('filtro_fecha_inicio'));
        $fechaTermino = fecha_Y_m_d($request->input('filtro_fecha_termino'));

        $archivosAcepta = \DB::table('archivos_acepta')
                        ->join('proveedores','archivos_acepta.id_proveedor','=','proveedores.id')
                        ->join('tipos_documento','archivos_acepta.id_tipo_documento','tipos_documento.id')
                        ->join('ws_contrato','proveedores.rut','=','ws_contrato.rut_proveedor')
                        ->join('ws_orden_compra','ws_contrato.id','=','ws_orden_compra.id_contrato')
                        ->join('ws_documento','ws_orden_compra.id','=','ws_documento.id_orden_compra')

                        ->whereColumn('tipos_documento.nombre','LIKE','ws_documento.tipo_documento')
                        ->whereColumn('archivos_acepta.folio','=','ws_documento.documento')
                        ->whereColumn('archivos_acepta.monto_total','=','ws_documento.documento_total')
                        // ->where('archivos_acepta.id_relacion',null)
                        ->where('ws_documento.id_relacion',null)
                        ->where('archivos_acepta.cargado',0)
                        ->where('archivos_acepta.id_user_rechazo', null);


        if ( $request->get('filtro_numero_documento') == null ) {
            $archivosAcepta = $archivosAcepta->where('archivos_acepta.created_at','>=',$fechaInicio.' 00:00:00')
                                             ->where('archivos_acepta.created_at','<=',$fechaTermino.' 23:59:59');
        } else {
            $archivosAcepta = $archivosAcepta->where('archivos_acepta.folio', trim($request->input('filtro_numero_documento')) );
        }
                        
        if ( $request->input('filtro_proveedor') != null ) {
            $archivosAcepta = $archivosAcepta->WhereIn('archivos_acepta.id_proveedor',$request->input('filtro_proveedor'));
        }

        if ( $request->input('filtro_tipo_documento') != null ) {
            $archivosAcepta = $archivosAcepta->WhereIn('archivos_acepta.id_tipo_documento',$request->input('filtro_tipo_documento'));
        }


        if ( \Entrust::hasRole('usuario-ingreso') ) {
            $archivosAcepta = $archivosAcepta->where(function ($query){
                                                $query->where('archivos_acepta.id_user_responsable', \Auth::user()->id)
                                                     ->orWhere('archivos_acepta.id_user_responsable', null);
                                            });
        }


        $archivosAcepta = $archivosAcepta->select(
                                'archivos_acepta.id as id_archivo_acepta',
                                'archivos_acepta.folio',
                                'archivos_acepta.uri',
                                'archivos_acepta.emision',
                                'archivos_acepta.created_at',
                                'archivos_acepta.id_proveedor',
                                'archivos_acepta.id_tipo_documento',
                                'archivos_acepta.id_user_responsable',
                                'archivos_acepta.estado_reclamo_contenido',
                                'archivos_acepta.reclamado',
                                'archivos_acepta.publicacion',
                                'proveedores.rut as rut_proveedor',
                                'proveedores.nombre as nombre_proveedor',
                                'tipos_documento.nombre as nombre_tipo_documento',
                                'ws_contrato.nombre_proveedor as ws_nombre_proveedor',
                                'ws_contrato.rut_proveedor as ws_rut_proveedor',
                                'ws_contrato.id as id_ws_contrato',
                                'ws_orden_compra.id as id_ws_orden_compra',
                                'ws_documento.id as id_ws_documento',
                                'ws_documento.tipo_documento as ws_tipo_documento',
                                'ws_documento.documento as ws_numero_documento'
                            )
                            ->get();

        foreach ($archivosAcepta as $archivo) {
            $botones = '';
            $style = '';
            if ( $archivo->estado_reclamo_contenido != '' ) {

                $botones .= '<button class="btn btn-danger btn-xs" title="Documento con Reclamo de Contenido: '.$archivo->estado_reclamo_contenido.' " >';
                $botones .= '<i class="fas fa-exclamation-circle fa-lg"></i></button>';
                $style   .= ' style="background-color: black;border-color: black;" ';

            }
            
            if ( $archivo->estado_reclamo_contenido == null && $archivo->reclamado == 1 ) {
                $botones .= '<button class="btn btn-danger btn-xs" title="Documento Reclamado" >';
                $botones .= '<i class="fas fa-exclamation-circle fa-lg"></i></button>';
                $style   .= ' style="background-color: black;border-color: black;" ';
            }

            $archivo->botones = $botones;
            $archivo->style = $style;

        }

        $tiposDocumento = TipoDocumento::whereNotIn('id',[2,8])->get();
        $proveedores = Proveedor::all();
        
        return view('archivo_acepta.index_listado_por_confirmar')->with('archivosAcepta',$archivosAcepta)
                                                                 ->with('tiposDocumento',$tiposDocumento)
                                                                 ->with('proveedores',$proveedores)
                                                                 // filtros
                                                                ->with('filtroFechaInicio',$request->input('filtro_fecha_inicio'))
                                                                ->with('filtroFechaTermino',$request->input('filtro_fecha_termino'))
                                                                ->with('filtroProveedor',$request->input('filtro_proveedor'))
                                                                ->with('filtroTipoDocumento',$request->input('filtro_tipo_documento'))
                                                                ->with('filtroNumeroDocumento',$request->input('filtro_numero_documento'));
    }

    public function postExcelConRecepcion(Request $request)
    {
        \Excel::create('SIFCON - Informe Con Recepción Acepta '.date('d-m-Y').'', function($excel) use($request) {
            $fechaInicio = fecha_Y_m_d($request->input('filtro_fecha_inicio'));
            $fechaTermino = fecha_Y_m_d($request->input('filtro_fecha_termino'));
           
            
            $documentosAcepta = \DB::table('archivos_acepta')
                        ->join('proveedores','archivos_acepta.id_proveedor','=','proveedores.id')
                        ->join('tipos_documento','archivos_acepta.id_tipo_documento','tipos_documento.id')
                        ->join('ws_contrato','proveedores.rut','=','ws_contrato.rut_proveedor')
                        ->join('ws_orden_compra','ws_contrato.id','=','ws_orden_compra.id_contrato')
                        ->join('ws_documento','ws_orden_compra.id','=','ws_documento.id_orden_compra')

                        ->whereColumn('tipos_documento.nombre','LIKE','ws_documento.tipo_documento')
                        ->whereColumn('archivos_acepta.folio','=','ws_documento.documento')
                        ->whereColumn('archivos_acepta.monto_total','=','ws_documento.documento_total')
                        // ->where('archivos_acepta.id_relacion',null)
                        ->where('ws_documento.id_relacion',null)
                        ->where('archivos_acepta.cargado',0)
                        ->where('archivos_acepta.id_user_rechazo', null);


            if ( $request->get('filtro_numero_documento') == null ) {
                $documentosAcepta = $documentosAcepta->where('archivos_acepta.created_at','>=',$fechaInicio.' 00:00:00')
                                                ->where('archivos_acepta.created_at','<=',$fechaTermino.' 23:59:59');
            } else {
                $documentosAcepta = $documentosAcepta->where('archivos_acepta.folio', trim($request->input('filtro_numero_documento')) );
            }
                            
            if ( $request->input('filtro_proveedor') != null ) {
                $documentosAcepta = $documentosAcepta->WhereIn('archivos_acepta.id_proveedor',$request->input('filtro_proveedor'));
            }

            if ( $request->input('filtro_tipo_documento') != null ) {
                $documentosAcepta = $documentosAcepta->WhereIn('archivos_acepta.id_tipo_documento',$request->input('filtro_tipo_documento'));
            }

            if ( \Entrust::hasRole('usuario-ingreso') ) {
                $documentosAcepta = $documentosAcepta->where(function ($query){
                                                    $query->where('archivos_acepta.id_user_responsable', \Auth::user()->id)
                                                        ->orWhere('archivos_acepta.id_user_responsable', null);
                                                });
            }

            $documentosAcepta = $documentosAcepta->get();

            $excel->sheet('Documentos Acepta', function($sheet) use($documentosAcepta) {

                $sheet->row(1, [
                    'Chile-Compra',
                    'Proveedor',
                    'RUT',
                    'Tipo Dcto.',
                    'N° Dcto.',
                    'F/Doc.',
                    'M/Doc.',
                    'Fecha RD',
                    'Mes RD',
                    'Monto',
                    'URI',
                ]);

                foreach ( $documentosAcepta as $index => $documento ) {

                    // Se trabaja el N° de orden compra.
                    $numOrdenCompra = '';
                    if ($documento->referencias != "" && $documento->referencias != null) {

                        $arrayJson = json_decode($documento->referencias, true);
                        if ($arrayJson != null) {
                            foreach ($arrayJson as $itemJson) {
                                if ($itemJson['Tipo'] == 801) {
                                    $numOrdenCompra = $itemJson['Folio'];
                                }
                            }
                        }

                    }

                    // dd($documento);
                    $tipoDocumento = $documento->tipo_documento;    
                    
                    
                    $rutProveedor = $documento->rut_proveedor;
                    $nombreProveedor = $documento->nombre_proveedor;

                    $sheet->row($index+2, [
                        $numOrdenCompra,
                        //explode("-",$documento->fecha_sigfe)[1], // mes de la fecha sigfe
                        $nombreProveedor,
                        $rutProveedor,
                        $tipoDocumento,
                        $documento->folio,
                        fecha_dmY($documento->emision),
                        explode("-",$documento->emision)[1], // mes de la fecha del documento
                        fecha_dmY($documento->created_at),
                        explode("-",$documento->created_at)[1], // mes de la fecha de recepcion del documento
                        $documento->monto_total,
                        $documento->uri,
                    ]);
                }

               
            });

        })->export('xlsx');
    }
    
    /**
     * Muestra modal para mostrar posible conexion entre el archivo acepta(documento) y
     * documento recepcionado en bodega
     */
    public function getModalPosibleConexion($idArchivoAcepta,$idWsDocumento)
    {
        $wsDocumento = WsDocumento::with(['getWsOrdenCompra','getWsOrdenCompra.getWsContrato',
                                      'getWsArchivos','getWsItemsPresupuestarios'])
                                      ->findOrFail($idWsDocumento);
                
        $archivoAcepta = ArchivoAcepta::with(['getProveedor','getTipoDocumento'])
                                      ->findOrFail($idArchivoAcepta);
                                    
        // Se trabaja el N° de orden compra.
        $numOrdenCompra = '';
        if ($archivoAcepta->referencias != "" && $archivoAcepta->referencias != null) {

            $arrayJson = json_decode($archivoAcepta->referencias, true);
            if ( $arrayJson != null ) {
                foreach ($arrayJson as $itemJson) {
                    if ($itemJson['Tipo'] == 801) {
                        $numOrdenCompra = $itemJson['Folio'];
                    }
                } 
            }
            

        } else {
            $numOrdenCompra = '1641-';
        }
                            
        return view('archivo_acepta.modal_posible_conexion')->with('wsDocumento',$wsDocumento)
                                                            ->with('archivoAcepta',$archivoAcepta)
                                                            ->with('numOrdenCompra',$numOrdenCompra);
    }

    /**
     * Funcion que muestra modal para rechazar el documento
     */
    public function getModalRechazar($id)
    {
        $archivoAcepta = ArchivoAcepta::with(['getProveedor','getTipoDocumento'])
                                      ->findOrFail($id);

        $motivos = MotivoRechazo::all();

        return view('archivo_acepta.modal_rechazo',compact('archivoAcepta','motivos'));
    }

    /**
     * Funcion que guarda el rechazo del archivo acepta.
     * debe guardar los motivos del rechazo
     */
    public function postRechazar(Request $request)
    {
        // dd('rechazando',$request->all());

        // Mensajes del validador
        $messages = [
            'required' => 'Debe ingresar el :attribute',
            'motivos.required' => 'Debe seleccionar al menos un Motivo de Rechazo',
            'numeric' => 'El :attribute debe solo contener números',
            'max' => 'El :attribute no debe exeder los :max caracteres',
            'min' => 'El :attribute debe tener minimo :min caracteres',
            'unique' => 'El valor :attribute ya existe en la base de datos',
        ];

        // Reglas del validador
        $rules = [
            'motivos' => 'required',
            '_id' => 'required',
        ];
        
        // Validador de los input del formulario
        $validator = \Validator::make($request->all(), $rules, $messages);

        //Si contiene errores se devuelven todos los errores, de lo contrario guarda en la base de datos
        if ( $validator->fails() ) {
            return response()->json($validator->errors(),400);
        } else {
            //dd('todo bien',$request->all());

            $archivoRechazo = ArchivoAcepta::findOrFail($request->input('_id'));
            $archivoRechazo->observacion_rechazo = trim($request->get('observacion_rechazo'));
            $archivoRechazo->id_user_rechazo = Auth::user()->id;
            $archivoRechazo->save();

            foreach ( $request->get('motivos') as $i => $idMotivo ) {

                $aceptaMotivo = new AceptaMotivoRechazo();
                $aceptaMotivo->id_archivo_acepta = $archivoRechazo->id;
                $aceptaMotivo->id_motivo_rechazo = $idMotivo;
                $aceptaMotivo->save();

            }

            $datos = array(
                'mensaje' => 'Se ha rechazado el Documento',
                'estado' => 'success',
                'id' => $archivoRechazo->id,
            );
        }

        return response()->json($datos,200);
    }

    /**
     * Funcion que muestra modal para quitar el rechazo del documento
     */
    public function getQuitarRechazo($id)
    {
        $archivoAcepta = ArchivoAcepta::with(['getProveedor','getTipoDocumento','getAceptaMotivoRechazo.getMotivoRechazo'])
                                      ->findOrFail($id);

        return view('archivo_acepta.modal_quitar_rechazo',compact('archivoAcepta'));
    }

    /**
     * Funcion que quita el rechazo del documento acepta(archivo)
     */
    public function postQuitarRechazo(Request $request)
    {

        // Mensajes del validador
        $messages = [
            'required' => 'Debe ingresar el :attribute',
            'motivos.required' => 'Debe seleccionar al menos un Motivo de Rechazo',
            'numeric' => 'El :attribute debe solo contener números',
            'max' => 'El :attribute no debe exeder los :max caracteres',
            'min' => 'El :attribute debe tener minimo :min caracteres',
            'unique' => 'El valor :attribute ya existe en la base de datos',
        ];

        // Reglas del validador
        $rules = [
            '_id' => 'required',
        ];
        
        // Validador de los input del formulario
        $validator = \Validator::make($request->all(), $rules, $messages);

        //Si contiene errores se devuelven todos los errores, de lo contrario guarda en la base de datos
        if ( $validator->fails() ) {
            return response()->json($validator->errors(),400);
        } else {
            //dd('todo bien',$request->all());

            $archivoQuitarRechazo = ArchivoAcepta::findOrFail($request->input('_id'));
            $archivoQuitarRechazo->observacion_rechazo = null;
            $archivoQuitarRechazo->id_user_rechazo = null;
            $archivoQuitarRechazo->id_user_quita_rechazo = Auth::user()->id;
            $archivoQuitarRechazo->save();

            foreach ($archivoQuitarRechazo->getAceptaMotivoRechazo as $aceptaMotivoRechazo) {
                $aceptaMotivoRechazo->delete();
            }

            $datos = array(
                'mensaje' => 'Se ha quitado el rechazo del Documento',
                'estado' => 'success',
                'id' => $archivoQuitarRechazo->id,
            );
        }

        return response()->json($datos,200);
    }

    /**
     * Muestra el listado de archivos rechazados
     */
    public function getArchivosRechazados()
    {
        if (!\Entrust::can(['ver-factura-documento-acepta','quitar-rechazo-documento-acepta'])) {
            return \Redirect::to('home');
        }
        set_time_limit(0);
        ini_set('max_execution_time', 0);

        $archivosAcepta = ArchivoAcepta::with(['getProveedor','getTipoDocumento','getAceptaMotivoRechazo.getMotivoRechazo'])
                                       ->where('cargado',0)
                                       ->where('id_user_rechazo','<>',null)
                                       ->whereMonth('created_at', date('m'))
                                       ->get();

        // dd($archivosAcepta);

        $tiposDocumento = TipoDocumento::whereNotIn('id',[2,8])->get();
        $proveedores = Proveedor::all();

        return view('archivo_acepta.rechazados')->with('archivosAcepta',$archivosAcepta)
                                                ->with('tiposDocumento',$tiposDocumento)
                                                ->with('proveedores',$proveedores);
    }

    public function dataTableRechazados(Request $request)
    {
        set_time_limit(0);
        ini_set('max_execution_time', 0);

        $formJSON = json_decode($request->get('form'));
        $form = construyeFormulario($formJSON);
        
        $fechaInicio = fecha_Y_m_d($form->filtro_fecha_inicio);
        $fechaTermino = fecha_Y_m_d($form->filtro_fecha_termino);

        $archivosAcepta = ArchivoAcepta::with([
                                        'getProveedor',
                                        'getTipoDocumento',
                                        'getAceptaMotivoRechazo.getMotivoRechazo'
                                        ])
                                       ->where('cargado', 0)
                                       ->where('id_user_rechazo','<>',null);

        if ( $form->filtro_numero_documento == null ) {

            $archivosAcepta = $archivosAcepta->where('created_at','>=',$fechaInicio.' 00:00:00')
                                             ->where('created_at','<=',$fechaTermino.' 23:59:59');

        } else {

            $archivosAcepta = $archivosAcepta->where('folio', trim($form->filtro_numero_documento) );

        }

        if ( isset($form->filtro_proveedor) ) {
            $archivosAcepta = $archivosAcepta->WhereIn('id_proveedor', $form->filtro_proveedor );
        }

        if ( isset($form->filtro_tipo_documento) ) {
            $archivosAcepta = $archivosAcepta->WhereIn('id_tipo_documento', $form->filtro_tipo_documento);
        }

        $archivosAcepta = $archivosAcepta->get();
        $data = [];

        $verFacturaDocumentoAcepta = \Entrust::can('ver-factura-documento-acepta'); // \Auth::user()->can('ver-factura-documento-acepta')
        $quitarRechazoDocumentoAcepta = \Entrust::can('quitar-rechazo-documento-acepta');
        foreach ( $archivosAcepta as $archivo ) {
            $data[] = $archivo->getDatosRechazados($verFacturaDocumentoAcepta, $quitarRechazoDocumentoAcepta);
        }

        $respuesta = array(
            "data" => $data
        );

        return json_encode($respuesta);

    }

    /**
     * Filtro de los documentos rechazados
     */
    public function postFiltrarRechazados(Request $request)
    {
        $fechaInicio = fecha_Y_m_d($request->input('filtro_fecha_inicio'));
        $fechaTermino = fecha_Y_m_d($request->input('filtro_fecha_termino'));
        // dd($fechaInicio,$fechaTermino);
        // dd($request->all());
        set_time_limit(0);
        $archivosAcepta = ArchivoAcepta::with(['getProveedor','getTipoDocumento'])
                                       ->where('cargado',0)
                                       ->where('id_user_rechazo','<>',null);

        // if ( $request->input('filtro_numero_documento') == null ) {
        //     $archivosAcepta = $archivosAcepta->where('created_at','>=',$fechaInicio.' 00:00:00')
        //                                      ->where('created_at','<=',$fechaTermino.' 23:59:59');
        // }
                                    
        // if ( $request->input('filtro_proveedor') != null ) {
        //     $archivosAcepta = $archivosAcepta->WhereIn('id_proveedor',$request->input('filtro_proveedor'));
        // }

        // if ( $request->input('filtro_tipo_documento') != null ) {
        //     $archivosAcepta = $archivosAcepta->WhereIn('id_tipo_documento',$request->input('filtro_tipo_documento'));
        // }

        // if ( $request->input('filtro_numero_documento') != null ) {
        //     $archivosAcepta = $archivosAcepta->where('folio', trim($request->input('filtro_numero_documento')) );
        // }

        $archivosAcepta = $archivosAcepta->get();

        $tiposDocumento = TipoDocumento::whereNotIn('id',[2,8])->get();
        $proveedores = Proveedor::all();

        return view('archivo_acepta.rechazados')->with('archivosAcepta',$archivosAcepta)
                                                ->with('tiposDocumento',$tiposDocumento)
                                                ->with('proveedores',$proveedores)
                                                // filtros
                                                ->with('filtroFechaInicio',$request->input('filtro_fecha_inicio'))
                                                ->with('filtroFechaTermino',$request->input('filtro_fecha_termino'))
                                                ->with('filtroProveedor',$request->input('filtro_proveedor'))
                                                ->with('filtroTipoDocumento',$request->input('filtro_tipo_documento'))
                                                ->with('filtroNumeroDocumento',$request->input('filtro_numero_documento'));
    }

    /**
     * Genera excel del listado de rechazados
     */
    public function postRechazadosExcel(Request $request)
    {
        \Excel::create('Informe Rechazados Acepta '.date('d-m-Y').'', function($excel) use($request) {
            $fechaInicio = fecha_Y_m_d($request->input('filtro_fecha_inicio'));
            $fechaTermino = fecha_Y_m_d($request->input('filtro_fecha_termino'));
            // dd($fechaInicio,$fechaTermino);
            // dd($request->all());
            set_time_limit(0);
            $archivosAcepta = ArchivoAcepta::with(['getProveedor','getTipoDocumento'])
                                        ->where('cargado',0)
                                        ->where('id_user_rechazo','<>',null);

            if ( $request->input('filtro_numero_documento') == null ) {
                $archivosAcepta = $archivosAcepta->where('created_at','>=',$fechaInicio.' 00:00:00')
                                                ->where('created_at','<=',$fechaTermino.' 23:59:59');
            }
                                        
            if ( $request->input('filtro_proveedor') != null ) {
                $archivosAcepta = $archivosAcepta->WhereIn('id_proveedor',$request->input('filtro_proveedor'));
            }

            if ( $request->input('filtro_tipo_documento') != null ) {
                $archivosAcepta = $archivosAcepta->WhereIn('id_tipo_documento',$request->input('filtro_tipo_documento'));
            }

            if ( $request->input('filtro_numero_documento') != null ) {
                $archivosAcepta = $archivosAcepta->where('folio', trim($request->input('filtro_numero_documento')) );
            }

            $archivosAcepta = $archivosAcepta->get();

            $excel->sheet('Documentos Acepta', function($sheet) use($archivosAcepta) {

                $sheet->row(1, [
                    'Estado',
                    'Chile-Compra',
                    'Proveedor',
                    'RUT',
                    'Tipo Dcto.',
                    'N° Dcto.',
                    'F/Doc.',
                    'M/Doc.',
                    'Fecha RD',
                    'Mes RD',
                    'Monto',
                    'Motivo Rechazo',
                    'Observación Rechazo',
                    'URI',
                ]);

                foreach ( $archivosAcepta as $index => $documento ) {

                    // Se trabaja el N° de orden compra.
                    $numOrdenCompra = '';
                    if ($documento->referencias != "" && $documento->referencias != null) {

                        $arrayJson = json_decode($documento->referencias, true);
                        foreach ($arrayJson as $itemJson) {
                            if ($itemJson['Tipo'] == 801) {
                                $numOrdenCompra = $itemJson['Folio'];
                            }
                        }

                    }

                    $motivos = "";
                    foreach ($documento->getAceptaMotivoRechazo as $key => $aceptaMotivoRechazo) {
                        $motivos .= $aceptaMotivoRechazo->getMotivoRechazo->nombre.PHP_EOL;
                    }

                    $sheet->row($index+2, [
                        'Rechazado',
                        $numOrdenCompra,
                        //explode("-",$documento->fecha_sigfe)[1], // mes de la fecha sigfe
                        $documento->getProveedor->nombre,
                        $documento->getProveedor->rut,
                        $documento->getTipoDocumento->nombre,
                        $documento->folio,
                        ( strpos($documento->emision,'-') !== false ) ? fecha_dmY($documento->emision) : '',
                        ( strpos($documento->emision,'-') !== false ) ? explode("-",$documento->emision)[1] : '', // mes de la fecha del documento
                        fecha_dmY($documento->created_at),
                        explode("-",$documento->created_at)[1], // mes de la fecha de recepcion del documento
                        $documento->monto_total,
                        $motivos,
                        $documento->observacion_rechazo,
                        $documento->uri,
                    ]);
                }

               
            })->getDefaultStyle()
            ->getAlignment()
            ->applyFromArray(array(
                'horizontal'   	=> \PHPExcel_Style_Alignment::HORIZONTAL_LEFT,
                'vertical'     	=> \PHPExcel_Style_Alignment::VERTICAL_TOP,
                'wrap'	 	=> TRUE
            ));

        })->export('xlsx');
    }

    /*
    Funciones Api

    public function store(Request $request) {
        //Autenticación del usuarios
    	$hash = $request->header('Authorization', null);

    	$jwtAuth = new JwtAuth();
        $checkToken =$jwtAuth->checkToken($hash);
        
        if (!$checkToken) {
            // Devolver error
            $data = array(
                'message' => 'Login Incorrecto',
                'status' => 'error',
                'code' => 300,
            );
        } else {
            // Recoger datos por post
            $json = $request->input('json',null);
    		$params = json_decode($json);
            $params_array = json_decode($json, true);

    		// Conseguir el ususario identificado
            $user = $jwtAuth->checkToken($hash,true);

            $data = array(
                'message' => 'Estas en Store de archivo acepta',
                'status' => 'success',
                'params' => $params,
                'request_file' => $request->file(),
                'username' => $user->name,
                'code' => 200,
            );
        }

        return response()->json($data, 200);
    }
    */

    public function getReasignarCarga()
    {
        $tiposDocumento = TipoDocumento::whereNotIn('id',[2,8])->get();
        $proveedores = Proveedor::all();

        $usuariosIngreso = Role::where('name','usuario-ingreso')->first()->users()->get();
        
        
        return view('archivo_acepta.reasignar_carga')->with('tiposDocumento',$tiposDocumento)
                                                    ->with('proveedores', $proveedores)
                                                    ->with('usuariosIngreso', $usuariosIngreso);
    }

    public function postFiltrarReasignarCarga(Request $request)
    {

        set_time_limit(0);
        ini_set('max_execution_time', 0);
        

        // Mensajes del validador
        $messages = [
            'required' => 'Debe ingresar el :attribute',
            'filtro_usuario_reasignar.required' => 'Debe seleccionar el usuario para reasignar',
            'filtro_usuarios.required' => 'Debe seleccionar usuarios de ingreso',
            'numeric' => 'El :attribute debe solo contener números',
            'max' => 'El :attribute no debe exeder los :max caracteres',
            'min' => 'El :attribute debe tener minimo :min caracteres',
            'unique' => 'El valor :attribute ya existe en la base de datos',
        ];

        // Reglas del validador
        $rules = [
            'filtro_usuario_reasignar' => 'required',
            'filtro_usuarios' => 'required'
        ];
        
        // Validador de los input del formulario
        $validator = \Validator::make($request->all(), $rules, $messages);

        //Si contiene errores se devuelven todos los errores, de lo contrario guarda en la base de datos
        if ( $validator->fails() ) {
            return response()->json($validator->errors(),400);
        } else {

            $fechaInicio = fecha_Y_m_d($request->input('filtro_fecha_inicio'));
            $fechaTermino = fecha_Y_m_d($request->input('filtro_fecha_termino'));
            $usuariosIngreso = count($request->get('filtro_usuarios'));
            $usuarioReasignar = $request->get('filtro_usuario_reasignar');

            if ( $request->get('filtro_archivos_repartir') == 'Sin Recepción' ) {

                $archivosAceptaConRecepcion = \DB::table('archivos_acepta')
                                            ->join('proveedores','archivos_acepta.id_proveedor','=','proveedores.id')
                                            ->join('tipos_documento','archivos_acepta.id_tipo_documento','tipos_documento.id')
                                            ->join('ws_contrato','proveedores.rut','=','ws_contrato.rut_proveedor')
                                            ->join('ws_orden_compra','ws_contrato.id','=','ws_orden_compra.id_contrato')
                                            ->join('ws_documento','ws_orden_compra.id','=','ws_documento.id_orden_compra')

                                            ->whereColumn('tipos_documento.nombre','LIKE','ws_documento.tipo_documento')
                                            ->whereColumn('archivos_acepta.folio','=','ws_documento.documento')
                                            ->whereColumn('archivos_acepta.monto_total','=','ws_documento.documento_total')
                                            
                                            ->where('ws_documento.id_relacion', null)
                                            // ->where('archivos_acepta.cargado', 0)
                                            ->where('archivos_acepta.id_user_rechazo', null)

                                            ->select(
                                                'archivos_acepta.id'
                                                )
                                            ->get();

                $arregloDocumentosId = $archivosAceptaConRecepcion->pluck('id')->toArray();

                $archivosAcepta = ArchivoAcepta::with(['getProveedor','getTipoDocumento'])
                                            // ->where('cargado', 0)
                                            ->where('id_user_rechazo', null)
                                            ->where('id_user_rezagar', null)
                                            ->where('id_user_responsable', $usuarioReasignar)
                                            ->whereNotIn('id', $arregloDocumentosId);

                if ( $request->input('filtro_numero_documento') == null ) {
                    $archivosAcepta = $archivosAcepta->where('created_at','>=', $fechaInicio.' 00:00:00')
                                                    ->where('created_at','<=', $fechaTermino.' 23:59:59');
                }
                                            
                if ( $request->input('filtro_proveedor') != null ) {
                    $archivosAcepta = $archivosAcepta->WhereIn('id_proveedor', $request->input('filtro_proveedor'));
                }
        
                if ( $request->input('filtro_tipo_documento') != null ) {
                    $archivosAcepta = $archivosAcepta->WhereIn('id_tipo_documento', $request->input('filtro_tipo_documento'));
                }
        
                if ( $request->input('filtro_numero_documento') != null ) {
                    $archivosAcepta = $archivosAcepta->where('folio', trim($request->input('filtro_numero_documento')) );
                }

                

                $archivosAcepta = $archivosAcepta->get();

                $arregloDocumentosId = $archivosAcepta->pluck('id')->toArray();

                $archivosAceptaCargados = ArchivoAcepta::where('cargado', '<>', 0)
                                                       ->whereIn('id', $arregloDocumentosId)
                                                       ->get()->count();

                $archivosAceptaRepartir = ArchivoAcepta::where('cargado', 0)
                                                       ->whereIn('id', $arregloDocumentosId)
                                                       ->get()->count();

            } elseif ( $request->get('filtro_archivos_repartir') == 'Con Recepción' ) {

                $archivosAcepta = \DB::table('archivos_acepta')
                            ->join('proveedores','archivos_acepta.id_proveedor','=','proveedores.id')
                            ->join('tipos_documento','archivos_acepta.id_tipo_documento','tipos_documento.id')
                            ->join('ws_contrato','proveedores.rut','=','ws_contrato.rut_proveedor')
                            ->join('ws_orden_compra','ws_contrato.id','=','ws_orden_compra.id_contrato')
                            ->join('ws_documento','ws_orden_compra.id','=','ws_documento.id_orden_compra')

                            ->whereColumn('tipos_documento.nombre','LIKE','ws_documento.tipo_documento')
                            ->whereColumn('archivos_acepta.folio','=','ws_documento.documento')
                            ->whereColumn('archivos_acepta.monto_total','=','ws_documento.documento_total')
                            // ->where('archivos_acepta.id_relacion',null)
                            ->where('ws_documento.id_relacion', null)
                            // ->where('archivos_acepta.cargado', 0)
                            ->where('id_user_responsable', $usuarioReasignar)
                            ->where('archivos_acepta.id_user_rechazo', null);


                if ( $request->get('filtro_numero_documento') == null ) {
                    $archivosAcepta = $archivosAcepta->where('archivos_acepta.created_at','>=', $fechaInicio.' 00:00:00')
                                                    ->where('archivos_acepta.created_at','<=', $fechaTermino.' 23:59:59');
                } else {
                    $archivosAcepta = $archivosAcepta->where('archivos_acepta.folio', trim($request->input('filtro_numero_documento')) );
                }
                                
                if ( $request->input('filtro_proveedor') != null ) {
                    $archivosAcepta = $archivosAcepta->WhereIn('archivos_acepta.id_proveedor', $request->input('filtro_proveedor'));
                }

                if ( $request->input('filtro_tipo_documento') != null ) {
                    $archivosAcepta = $archivosAcepta->WhereIn('archivos_acepta.id_tipo_documento', $request->input('filtro_tipo_documento'));
                }

                $archivosAcepta = $archivosAcepta->select(
                                    'archivos_acepta.id as id_archivo_acepta',
                                    'archivos_acepta.folio',
                                    'archivos_acepta.uri',
                                    'archivos_acepta.emision',
                                    'archivos_acepta.created_at',
                                    'archivos_acepta.id_proveedor',
                                    'archivos_acepta.id_tipo_documento',
                                    'proveedores.rut as rut_proveedor',
                                    'proveedores.nombre as nombre_proveedor',
                                    'tipos_documento.nombre as nombre_tipo_documento',
                                    'ws_contrato.nombre_proveedor as ws_nombre_proveedor',
                                    'ws_contrato.rut_proveedor as ws_rut_proveedor',
                                    'ws_contrato.id as id_ws_contrato',
                                    'ws_orden_compra.id as id_ws_orden_compra',
                                    'ws_documento.id as id_ws_documento',
                                    'ws_documento.tipo_documento as ws_tipo_documento',
                                    'ws_documento.documento as ws_numero_documento'
                                )
                                ->get();

                $arregloDocumentosId = $archivosAcepta->pluck('id_archivo_acepta')->toArray();

                $archivosAceptaCargados = ArchivoAcepta::where('cargado', '<>', 0)
                                                        ->whereIn('id', $arregloDocumentosId)
                                                        ->get()->count();

                $archivosAceptaRepartir = ArchivoAcepta::where('cargado', 0)
                                                        ->whereIn('id', $arregloDocumentosId)
                                                        ->get()->count();

            }

            // Para pruebas
            // $conta = 0;
            // foreach ( $archivosAcepta as $archivo ) {
            //     $archivo->cargado = 0;
            //     $archivo->save();
            //     $conta++;

            //     if ( $conta == 4 ) {
            //         break;
            //     }
            // }

            $user = User::findorfail($usuarioReasignar);

            $cincuentaPorCiento = round( $archivosAcepta->count() / 2 );
            $resto = $archivosAcepta->count() % 2;
            $aRepartir = $cincuentaPorCiento - ($archivosAceptaCargados - $cincuentaPorCiento);

            $resumen = '<strong>';

            $resumen .= 'Documentos asignados al usuario '.$user->name.' : '. formatoMiles($archivosAcepta->count()).'<br>';
            $resumen .= 'Documentos ingresados por el usuario : '. formatoMiles( $archivosAceptaCargados ).'<br>';
            $resumen .= 'Documentos a reasignar : '. formatoMiles( $aRepartir - $resto ).'<br>';
            
            $resumen .= '<br>Presione el botón " <i class="fas fa-exchange-alt"></i> Reasignar " para reasignar la carga.<br>';

            $resumen .= '</strong>';

            // calcular cuantos documentos se reparten, hay que dejarle al ususario los que estan cargados.
            // se reparte el 50% de lo asignado
            
            // dd('todo bien', $request->all(), $resumen , $archivosAceptaCargados, $archivosAcepta->count(), $archivosAceptaRepartir, $cincuentaPorCiento, $resto);
        }

        $datos = array(
            'mensaje' => 'Todo bien',
            'estado'  => 'success',
            'resumen' => $resumen
        );

        return response()->json($datos,200);

    }

    public function postReasignarCargaGuardar(Request $request)
    {

        set_time_limit(0);
        ini_set('max_execution_time', 0);
        

        // Mensajes del validador
        $messages = [
            'required' => 'Debe ingresar el :attribute',
            'filtro_usuario_reasignar.required' => 'Debe seleccionar el usuario para reasignar',
            'filtro_usuarios.required' => 'Debe seleccionar usuarios de ingreso',
            'numeric' => 'El :attribute debe solo contener números',
            'max' => 'El :attribute no debe exeder los :max caracteres',
            'min' => 'El :attribute debe tener minimo :min caracteres',
            'unique' => 'El valor :attribute ya existe en la base de datos',
        ];

        // Reglas del validador
        $rules = [
            'filtro_usuario_reasignar' => 'required',
            'filtro_usuarios' => 'required'
        ];
        
        // Validador de los input del formulario
        $validator = \Validator::make($request->all(), $rules, $messages);

        //Si contiene errores se devuelven todos los errores, de lo contrario guarda en la base de datos
        if ( $validator->fails() ) {
            return response()->json($validator->errors(),400);
        } else {

            $fechaInicio = fecha_Y_m_d($request->input('filtro_fecha_inicio'));
            $fechaTermino = fecha_Y_m_d($request->input('filtro_fecha_termino'));
            $usuariosIngreso = User::whereIn('id', $request->get('filtro_usuarios'))->get();
            $usuarioReasignar = $request->get('filtro_usuario_reasignar');

            if ( $request->get('filtro_archivos_repartir') == 'Sin Recepción' ) {

                $archivosAceptaConRecepcion = \DB::table('archivos_acepta')
                                            ->join('proveedores','archivos_acepta.id_proveedor','=','proveedores.id')
                                            ->join('tipos_documento','archivos_acepta.id_tipo_documento','tipos_documento.id')
                                            ->join('ws_contrato','proveedores.rut','=','ws_contrato.rut_proveedor')
                                            ->join('ws_orden_compra','ws_contrato.id','=','ws_orden_compra.id_contrato')
                                            ->join('ws_documento','ws_orden_compra.id','=','ws_documento.id_orden_compra')

                                            ->whereColumn('tipos_documento.nombre','LIKE','ws_documento.tipo_documento')
                                            ->whereColumn('archivos_acepta.folio','=','ws_documento.documento')
                                            ->whereColumn('archivos_acepta.monto_total','=','ws_documento.documento_total')
                                            
                                            ->where('ws_documento.id_relacion', null)
                                            // ->where('archivos_acepta.cargado', 0)
                                            ->where('archivos_acepta.id_user_rechazo', null)

                                            ->select(
                                                'archivos_acepta.id'
                                                )
                                            ->get();

                $arregloDocumentosId = $archivosAceptaConRecepcion->pluck('id')->toArray();

                $archivosAcepta = ArchivoAcepta::with(['getProveedor','getTipoDocumento'])
                                            // ->where('cargado', 0)
                                            ->where('id_user_rechazo', null)
                                            ->where('id_user_rezagar', null)
                                            ->where('id_user_responsable', $usuarioReasignar)
                                            ->whereNotIn('id', $arregloDocumentosId);

                if ( $request->input('filtro_numero_documento') == null ) {
                    $archivosAcepta = $archivosAcepta->where('created_at','>=', $fechaInicio.' 00:00:00')
                                                    ->where('created_at','<=', $fechaTermino.' 23:59:59');
                }
                                            
                if ( $request->input('filtro_proveedor') != null ) {
                    $archivosAcepta = $archivosAcepta->WhereIn('id_proveedor', $request->input('filtro_proveedor'));
                }
        
                if ( $request->input('filtro_tipo_documento') != null ) {
                    $archivosAcepta = $archivosAcepta->WhereIn('id_tipo_documento', $request->input('filtro_tipo_documento'));
                }
        
                if ( $request->input('filtro_numero_documento') != null ) {
                    $archivosAcepta = $archivosAcepta->where('folio', trim($request->input('filtro_numero_documento')) );
                }

                

                $archivosAcepta = $archivosAcepta->get();

                $arregloDocumentosId = $archivosAcepta->pluck('id')->toArray();

                $archivosAceptaCargados = ArchivoAcepta::where('cargado', '<>', 0)
                                                       ->whereIn('id', $arregloDocumentosId)
                                                       ->get()->count();

                $archivosAceptaRepartir = ArchivoAcepta::where('cargado', 0)
                                                       ->whereIn('id', $arregloDocumentosId)
                                                       ->get();

            } elseif ( $request->get('filtro_archivos_repartir') == 'Con Recepción' ) {

                $archivosAcepta = \DB::table('archivos_acepta')
                            ->join('proveedores','archivos_acepta.id_proveedor','=','proveedores.id')
                            ->join('tipos_documento','archivos_acepta.id_tipo_documento','tipos_documento.id')
                            ->join('ws_contrato','proveedores.rut','=','ws_contrato.rut_proveedor')
                            ->join('ws_orden_compra','ws_contrato.id','=','ws_orden_compra.id_contrato')
                            ->join('ws_documento','ws_orden_compra.id','=','ws_documento.id_orden_compra')

                            ->whereColumn('tipos_documento.nombre','LIKE','ws_documento.tipo_documento')
                            ->whereColumn('archivos_acepta.folio','=','ws_documento.documento')
                            ->whereColumn('archivos_acepta.monto_total','=','ws_documento.documento_total')
                            // ->where('archivos_acepta.id_relacion',null)
                            ->where('ws_documento.id_relacion', null)
                            // ->where('archivos_acepta.cargado', 0)
                            ->where('id_user_responsable', $usuarioReasignar)
                            ->where('archivos_acepta.id_user_rechazo', null);


                if ( $request->get('filtro_numero_documento') == null ) {
                    $archivosAcepta = $archivosAcepta->where('archivos_acepta.created_at','>=', $fechaInicio.' 00:00:00')
                                                    ->where('archivos_acepta.created_at','<=', $fechaTermino.' 23:59:59');
                } else {
                    $archivosAcepta = $archivosAcepta->where('archivos_acepta.folio', trim($request->input('filtro_numero_documento')) );
                }
                                
                if ( $request->input('filtro_proveedor') != null ) {
                    $archivosAcepta = $archivosAcepta->WhereIn('archivos_acepta.id_proveedor', $request->input('filtro_proveedor'));
                }

                if ( $request->input('filtro_tipo_documento') != null ) {
                    $archivosAcepta = $archivosAcepta->WhereIn('archivos_acepta.id_tipo_documento', $request->input('filtro_tipo_documento'));
                }

                $archivosAcepta = $archivosAcepta->select(
                                    'archivos_acepta.id as id_archivo_acepta',
                                    'archivos_acepta.folio',
                                    'archivos_acepta.uri',
                                    'archivos_acepta.emision',
                                    'archivos_acepta.created_at',
                                    'archivos_acepta.id_proveedor',
                                    'archivos_acepta.id_tipo_documento',
                                    'proveedores.rut as rut_proveedor',
                                    'proveedores.nombre as nombre_proveedor',
                                    'tipos_documento.nombre as nombre_tipo_documento',
                                    'ws_contrato.nombre_proveedor as ws_nombre_proveedor',
                                    'ws_contrato.rut_proveedor as ws_rut_proveedor',
                                    'ws_contrato.id as id_ws_contrato',
                                    'ws_orden_compra.id as id_ws_orden_compra',
                                    'ws_documento.id as id_ws_documento',
                                    'ws_documento.tipo_documento as ws_tipo_documento',
                                    'ws_documento.documento as ws_numero_documento'
                                )
                                ->get();

                $arregloDocumentosId = $archivosAcepta->pluck('id_archivo_acepta')->toArray();

                $archivosAceptaCargados = ArchivoAcepta::where('cargado', '<>', 0)
                                                        ->whereIn('id', $arregloDocumentosId)
                                                        ->get()->count();

                $archivosAceptaRepartir = ArchivoAcepta::where('cargado', 0)
                                                        ->whereIn('id', $arregloDocumentosId)
                                                        ->get();

            }

            // Trabajar con archivosAceptaRepartir
            // Repartir la carga

            $repartidos = $archivosAceptaRepartir->count();
            $division = round($repartidos / $usuariosIngreso->count());
            $resto = $repartidos % $usuariosIngreso->count();

            foreach ( $usuariosIngreso as $key => $usuario  ) {
            
                for ( $i = ( $division * $key ) ; $i < ( $division * ( $key +1 ) ) ; $i++ ) {
    
                    $archivosAceptaAux = ArchivoAcepta::findOrfail($archivosAceptaRepartir[$i]->id);
                    $archivosAceptaAux->id_user_responsable = $usuario->id;
                    $archivosAceptaAux->save();
                    
                    $archivosAceptaRepartir->forget($i);
    
                }
    
            }

            $resumenExtra = '';
            foreach ( $archivosAceptaRepartir as $archivoAcepta ) {
                
                $random = rand( 0, ($usuariosIngreso->count() - 1) );

                $archivosAceptaAux = ArchivoAcepta::findOrfail($archivoAcepta->id);
                $archivosAceptaAux->id_user_responsable = $usuariosIngreso[ $random ]->id;
                $archivosAceptaAux->save();

                $resumenExtra .= 'El usuario '.$usuariosIngreso[ $random ]->name.' tiene 1 documento más. <br>';

            }

            $resumen = '<strong>';
            $resumen .= 'Documentos reasignados : '.formatoMiles($repartidos).'<br>';
            $resumen .= 'Documentos por usuario : '.formatoMiles($division).'<br>';
            if ( $resto != 0 ) {
                $resumen .= $resumenExtra;
            }
            $resumen .= '<br>Se logro reasignar la carga correctamente.<br>';
    
            $resumen .= '</strong>';

        }

        $datos = array(
            'mensaje' => 'Todo bien',
            'estado'  => 'success',
            'resumen' => $resumen
        );

        return response()->json($datos,200);
    }

    public function getRepartirCarga()
    {
        $tiposDocumento = TipoDocumento::whereNotIn('id',[2,8])->get();
        $proveedores = Proveedor::all();

        $usuariosIngreso = Role::where('name','usuario-ingreso')->first()->users()->get();
        // dd($usuariosIngreso);
        
        return view('archivo_acepta.repartir_carga')->with('tiposDocumento',$tiposDocumento)
                                                    ->with('proveedores', $proveedores)
                                                    ->with('usuariosIngreso', $usuariosIngreso);
    }

    public function postFiltrarRepartirCarga(Request $request)
    {
        // dd($request->all());

        set_time_limit(0);
        ini_set('max_execution_time', 0);

        $fechaInicio = fecha_Y_m_d($request->input('filtro_fecha_inicio'));
        $fechaTermino = fecha_Y_m_d($request->input('filtro_fecha_termino'));
        $usuariosIngreso = count($request->get('filtro_usuarios'));
        // $usuariosIngreso = Role::where('name','usuario-ingreso')->first()->users()->get()->count();

        if ( $request->get('filtro_archivos_repartir') == 'Sin Recepción' ) {

            $archivosAceptaConRecepcion = \DB::table('archivos_acepta')
                                          ->join('proveedores','archivos_acepta.id_proveedor','=','proveedores.id')
                                          ->join('tipos_documento','archivos_acepta.id_tipo_documento','tipos_documento.id')
                                          ->join('ws_contrato','proveedores.rut','=','ws_contrato.rut_proveedor')
                                          ->join('ws_orden_compra','ws_contrato.id','=','ws_orden_compra.id_contrato')
                                          ->join('ws_documento','ws_orden_compra.id','=','ws_documento.id_orden_compra')

                                          ->whereColumn('tipos_documento.nombre','LIKE','ws_documento.tipo_documento')
                                          ->whereColumn('archivos_acepta.folio','=','ws_documento.documento')
                                          ->whereColumn('archivos_acepta.monto_total','=','ws_documento.documento_total')
                                          
                                          ->where('ws_documento.id_relacion',null)
                                          ->where('archivos_acepta.cargado',0)
                                          ->where('archivos_acepta.id_user_rechazo', null)

                                          ->select(
                                              'archivos_acepta.id'
                                            )
                                          ->get();

            $arregloDocumentosId = $archivosAceptaConRecepcion->pluck('id')->toArray();

            $archivosAcepta = ArchivoAcepta::with(['getProveedor','getTipoDocumento'])
                                          ->where('cargado',0)
                                          ->where('id_user_rechazo', null)
                                          ->where('id_user_rezagar', null)
                                          ->doesnthave('getArchivoAceptaRevisionBodega')
                                          ->whereNotIn('id',$arregloDocumentosId);

            if ( $request->input('filtro_numero_documento') == null ) {
                $archivosAcepta = $archivosAcepta->where('created_at','>=', $fechaInicio.' 00:00:00')
                                                 ->where('created_at','<=', $fechaTermino.' 23:59:59');
            }
                                        
            if ( $request->input('filtro_proveedor') != null ) {
                $archivosAcepta = $archivosAcepta->WhereIn('id_proveedor', $request->input('filtro_proveedor'));
            }
    
            if ( $request->input('filtro_tipo_documento') != null ) {
                $archivosAcepta = $archivosAcepta->WhereIn('id_tipo_documento', $request->input('filtro_tipo_documento'));
            }
    
            if ( $request->input('filtro_numero_documento') != null ) {
                $archivosAcepta = $archivosAcepta->where('folio', trim($request->input('filtro_numero_documento')) );
            }
    
            $archivosAcepta = $archivosAcepta->get()->count();

        } elseif ( $request->get('filtro_archivos_repartir') == 'Con Recepción' ) {

            $archivosAcepta = \DB::table('archivos_acepta')
                        ->join('proveedores','archivos_acepta.id_proveedor','=','proveedores.id')
                        ->join('tipos_documento','archivos_acepta.id_tipo_documento','tipos_documento.id')
                        ->join('ws_contrato','proveedores.rut','=','ws_contrato.rut_proveedor')
                        ->join('ws_orden_compra','ws_contrato.id','=','ws_orden_compra.id_contrato')
                        ->join('ws_documento','ws_orden_compra.id','=','ws_documento.id_orden_compra')

                        ->whereColumn('tipos_documento.nombre','LIKE','ws_documento.tipo_documento')
                        ->whereColumn('archivos_acepta.folio','=','ws_documento.documento')
                        ->whereColumn('archivos_acepta.monto_total','=','ws_documento.documento_total')
                        // ->where('archivos_acepta.id_relacion',null)
                        ->where('ws_documento.id_relacion',null)
                        ->where('archivos_acepta.cargado',0)
                        ->where('archivos_acepta.id_user_rechazo', null);


            if ( $request->get('filtro_numero_documento') == null ) {
                $archivosAcepta = $archivosAcepta->where('archivos_acepta.created_at','>=', $fechaInicio.' 00:00:00')
                                                 ->where('archivos_acepta.created_at','<=', $fechaTermino.' 23:59:59');
            } else {
                $archivosAcepta = $archivosAcepta->where('archivos_acepta.folio', trim($request->input('filtro_numero_documento')) );
            }
                            
            if ( $request->input('filtro_proveedor') != null ) {
                $archivosAcepta = $archivosAcepta->WhereIn('archivos_acepta.id_proveedor', $request->input('filtro_proveedor'));
            }

            if ( $request->input('filtro_tipo_documento') != null ) {
                $archivosAcepta = $archivosAcepta->WhereIn('archivos_acepta.id_tipo_documento', $request->input('filtro_tipo_documento'));
            }

            $archivosAcepta = $archivosAcepta->select(
                                'archivos_acepta.id as id_archivo_acepta',
                                'archivos_acepta.folio',
                                'archivos_acepta.uri',
                                'archivos_acepta.emision',
                                'archivos_acepta.created_at',
                                'archivos_acepta.id_proveedor',
                                'archivos_acepta.id_tipo_documento',
                                'proveedores.rut as rut_proveedor',
                                'proveedores.nombre as nombre_proveedor',
                                'tipos_documento.nombre as nombre_tipo_documento',
                                'ws_contrato.nombre_proveedor as ws_nombre_proveedor',
                                'ws_contrato.rut_proveedor as ws_rut_proveedor',
                                'ws_contrato.id as id_ws_contrato',
                                'ws_orden_compra.id as id_ws_orden_compra',
                                'ws_documento.id as id_ws_documento',
                                'ws_documento.tipo_documento as ws_tipo_documento',
                                'ws_documento.documento as ws_numero_documento'
                            )
                            ->get()->count();

        }

        $division = (int) ( $archivosAcepta / $usuariosIngreso );
        $resto = $archivosAcepta % $usuariosIngreso;
        // $division = round($division);

        $resumen = '<strong>';
        $resumen .= 'Documentos encontrados : '.formatoMiles($archivosAcepta).'<br>';
        $resumen .= 'Documentos por usuario: '.formatoMiles($division).'<br>';
        if ( $resto != 0 ) {
            $resumen .= 'Se repartira(n) '.$resto.' documento(s) entre los usuarios de ingreso. <br>';
        }
        $resumen .= '<br>Presione el botón " <i class="fas fa-random fa-pulso"></i> Repartir " para repartir la carga.<br>';

        $resumen .= '</strong>';

        $datos = array(
            'mensaje' => 'Todo bien',
            'estado'  => 'success',
            'resumen' => $resumen
        );

        return response()->json($datos,200);
        // dd($request->all(), $archivosAcepta, $usuariosIngreso, $division, $resto, round($division), $resumen);
    }

    public function postRepartirCargaGuardar(Request $request)
    {

        set_time_limit(0);
        ini_set('max_execution_time', 0);

        $fechaInicio = fecha_Y_m_d($request->input('filtro_fecha_inicio'));
        $fechaTermino = fecha_Y_m_d($request->input('filtro_fecha_termino'));
        // $usuariosIngreso = Role::where('name','usuario-ingreso')->first()->users()->get();
        // $usuariosIngreso = count($request->get('filtro_usuarios'));
        $usuariosIngreso = User::whereIn('id', $request->get('filtro_usuarios'))->get();
        // dd($request->all(), $usuariosIngreso);

        if ( $request->get('filtro_archivos_repartir') == 'Sin Recepción' ) {

            $archivosAceptaConRecepcion = \DB::table('archivos_acepta')
                                          ->join('proveedores','archivos_acepta.id_proveedor','=','proveedores.id')
                                          ->join('tipos_documento','archivos_acepta.id_tipo_documento','tipos_documento.id')
                                          ->join('ws_contrato','proveedores.rut','=','ws_contrato.rut_proveedor')
                                          ->join('ws_orden_compra','ws_contrato.id','=','ws_orden_compra.id_contrato')
                                          ->join('ws_documento','ws_orden_compra.id','=','ws_documento.id_orden_compra')

                                          ->whereColumn('tipos_documento.nombre','LIKE','ws_documento.tipo_documento')
                                          ->whereColumn('archivos_acepta.folio','=','ws_documento.documento')
                                          ->whereColumn('archivos_acepta.monto_total','=','ws_documento.documento_total')
                                          
                                          ->where('ws_documento.id_relacion',null)
                                          ->where('archivos_acepta.cargado',0)
                                          ->where('archivos_acepta.id_user_rechazo', null)

                                          ->select(
                                              'archivos_acepta.id'
                                            )
                                          ->get();

            $arregloDocumentosId = $archivosAceptaConRecepcion->pluck('id')->toArray();

            $archivosAcepta = ArchivoAcepta::with(['getProveedor','getTipoDocumento'])
                                          ->where('cargado',0)
                                          ->where('id_user_rechazo', null)
                                          ->where('id_user_rezagar', null)
                                          ->doesnthave('getArchivoAceptaRevisionBodega')
                                          ->whereNotIn('id', $arregloDocumentosId);

            if ( $request->input('filtro_numero_documento') == null ) {
                $archivosAcepta = $archivosAcepta->where('created_at','>=', $fechaInicio.' 00:00:00')
                                                 ->where('created_at','<=', $fechaTermino.' 23:59:59');
            }
                                        
            if ( $request->input('filtro_proveedor') != null ) {
                $archivosAcepta = $archivosAcepta->WhereIn('id_proveedor', $request->input('filtro_proveedor'));
            }
    
            if ( $request->input('filtro_tipo_documento') != null ) {
                $archivosAcepta = $archivosAcepta->WhereIn('id_tipo_documento', $request->input('filtro_tipo_documento'));
            }
    
            if ( $request->input('filtro_numero_documento') != null ) {
                $archivosAcepta = $archivosAcepta->where('folio', trim($request->input('filtro_numero_documento')) );
            }
    
            $archivosAcepta = $archivosAcepta->get();

        } elseif ( $request->get('filtro_archivos_repartir') == 'Con Recepción' ) {

            $archivosAcepta = \DB::table('archivos_acepta')
                        ->join('proveedores','archivos_acepta.id_proveedor','=','proveedores.id')
                        ->join('tipos_documento','archivos_acepta.id_tipo_documento','tipos_documento.id')
                        ->join('ws_contrato','proveedores.rut','=','ws_contrato.rut_proveedor')
                        ->join('ws_orden_compra','ws_contrato.id','=','ws_orden_compra.id_contrato')
                        ->join('ws_documento','ws_orden_compra.id','=','ws_documento.id_orden_compra')

                        ->whereColumn('tipos_documento.nombre','LIKE','ws_documento.tipo_documento')
                        ->whereColumn('archivos_acepta.folio','=','ws_documento.documento')
                        ->whereColumn('archivos_acepta.monto_total','=','ws_documento.documento_total')
                        // ->where('archivos_acepta.id_relacion',null)
                        ->where('ws_documento.id_relacion',null)
                        ->where('archivos_acepta.cargado',0)
                        ->where('archivos_acepta.id_user_rechazo', null);


            if ( $request->get('filtro_numero_documento') == null ) {
                $archivosAcepta = $archivosAcepta->where('archivos_acepta.created_at','>=', $fechaInicio.' 00:00:00')
                                                 ->where('archivos_acepta.created_at','<=', $fechaTermino.' 23:59:59');
            } else {
                $archivosAcepta = $archivosAcepta->where('archivos_acepta.folio', trim($request->input('filtro_numero_documento')) );
            }
                            
            if ( $request->input('filtro_proveedor') != null ) {
                $archivosAcepta = $archivosAcepta->WhereIn('archivos_acepta.id_proveedor', $request->input('filtro_proveedor'));
            }

            if ( $request->input('filtro_tipo_documento') != null ) {
                $archivosAcepta = $archivosAcepta->WhereIn('archivos_acepta.id_tipo_documento', $request->input('filtro_tipo_documento'));
            }

            $archivosAcepta = $archivosAcepta->select(
                                'archivos_acepta.id as id',
                                'archivos_acepta.folio',
                                'archivos_acepta.uri',
                                'archivos_acepta.emision',
                                'archivos_acepta.created_at',
                                'archivos_acepta.id_proveedor',
                                'archivos_acepta.id_tipo_documento',
                                'proveedores.rut as rut_proveedor',
                                'proveedores.nombre as nombre_proveedor',
                                'tipos_documento.nombre as nombre_tipo_documento',
                                'ws_contrato.nombre_proveedor as ws_nombre_proveedor',
                                'ws_contrato.rut_proveedor as ws_rut_proveedor',
                                'ws_contrato.id as id_ws_contrato',
                                'ws_orden_compra.id as id_ws_orden_compra',
                                'ws_documento.id as id_ws_documento',
                                'ws_documento.tipo_documento as ws_tipo_documento',
                                'ws_documento.documento as ws_numero_documento'
                            )
                            ->get();

        }

        $division = (int) ( $archivosAcepta->count() / $usuariosIngreso->count() );
        $resto = $archivosAcepta->count() % $usuariosIngreso->count();
        // $division = round($division); // 63.5 queda en 64
        $repartidos = $archivosAcepta->count();

        $resumenUsuarios = '';
        foreach ( $usuariosIngreso as $key => $usuario  ) {
            $usuario->repartidos = 0;
            
            for ( $i = ( $division * $key ) ; $i < ( $division * ( $key +1 ) ) ; $i++ ) {

                $archivosAceptaAux = ArchivoAcepta::findOrfail($archivosAcepta[$i]->id);
                $archivosAceptaAux->id_user_responsable = $usuario->id;
                $archivosAceptaAux->save();
                $usuario->repartidos++;
                
                $archivosAcepta->forget($i);

            }

            $resumenUsuarios .= 'El usuario '.$usuario->name.' tiene '.$usuario->repartidos.' documentos asignados. <br>';

        }

        $resumenExtra = '';
        foreach ( $archivosAcepta as $archivoAcepta ) {
            
            $random = rand( 0, ($usuariosIngreso->count() - 1) );

            $archivosAceptaAux = ArchivoAcepta::findOrfail($archivoAcepta->id);
            $archivosAceptaAux->id_user_responsable = $usuariosIngreso[ $random ]->id;
            $archivosAceptaAux->save();

            $resumenExtra .= 'El usuario '.$usuariosIngreso[ $random ]->name.' tiene 1 documento más. <br>';

        }



        $resumen = '<strong>';
        $resumen .= 'Documentos repartidos : '.formatoMiles($repartidos).'<br>';
        $resumen .= 'Documentos por usuario : '.formatoMiles($division).'<br>';
        $resumen .= '<br>'.$resumenUsuarios;
        if ( $resto != 0 ) {
            $resumen .= $resumenExtra;
        }
        $resumen .= '<br>Se logro repartir la carga correctamente.<br>';

        $resumen .= '</strong>';

        $datos = array(
            'mensaje' => 'Todo bien',
            'estado'  => 'success',
            'resumen' => $resumen
        );

        return response()->json($datos,200);

    }

    /**
     * Funcion que muestra modal para rezagar el documento
     */
    public function getModalRezagar($id)
    {
        $archivoAcepta = ArchivoAcepta::with(['getProveedor','getTipoDocumento'])
                                      ->findOrFail($id);

        $motivos = MotivoRezagar::all();

        return view('archivo_acepta.modal_rezagar',compact('archivoAcepta','motivos'));
    }

    /**
     * Funcion que guarda el regazar del archivo acepta.
     * debe guardar los motivos para rezagar
     */
    public function postRezagar(Request $request)
    {
        // dd('rezagando', $request->all());

        // Mensajes del validador
        $messages = [
            'required' => 'Debe ingresar el :attribute',
            'motivos.required' => 'Debe seleccionar al menos un Motivo para Rezagar',
            'numeric' => 'El :attribute debe solo contener números',
            'max' => 'El :attribute no debe exeder los :max caracteres',
            'min' => 'El :attribute debe tener minimo :min caracteres',
            'unique' => 'El valor :attribute ya existe en la base de datos',
        ];

        // Reglas del validador
        $rules = [
            'motivos' => 'required',
            '_id' => 'required',
        ];
        
        // Validador de los input del formulario
        $validator = \Validator::make($request->all(), $rules, $messages);

        //Si contiene errores se devuelven todos los errores, de lo contrario guarda en la base de datos
        if ( $validator->fails() ) {
            return response()->json($validator->errors(),400);
        } else {
            //dd('todo bien',$request->all());

            $archivoAcepta = ArchivoAcepta::findOrFail($request->input('_id'));
            $archivoAcepta->observacion_rezagar = trim($request->get('observacion_rezagar'));
            $archivoAcepta->id_user_rezagar = Auth::user()->id;
            $archivoAcepta->save();

            foreach ( $request->get('motivos') as $i => $idMotivo ) {

                $aceptaMotivo = new AceptaMotivoRezagar();
                $aceptaMotivo->id_archivo_acepta = $archivoAcepta->id;
                $aceptaMotivo->id_motivo_rezagar = $idMotivo;
                $aceptaMotivo->save();

            }

            $datos = array(
                'mensaje' => 'Se ha rezagado el Documento',
                'estado'  => 'success',
                'id'      => $archivoAcepta->id,
            );
        }

        return response()->json($datos,200);
    }

    /**
     * Muestra el listado de archivos rezagados
     */
    public function getArchivosRezagados()
    {
        // if (!\Entrust::can(['ver-factura-documento-acepta','quitar-rezago-documento-acepta'])) {
        //     return \Redirect::to('home');
        // }
        set_time_limit(0);
        ini_set('max_execution_time', 0);

        $tiposDocumento = TipoDocumento::whereNotIn('id',[2,8])->get();
        $proveedores = Proveedor::all();

        return view('archivo_acepta.rezagados')->with('tiposDocumento',$tiposDocumento)
                                                ->with('proveedores',$proveedores);
    }

    public function dataTableRezagados(Request $request)
    {
        set_time_limit(0);
        ini_set('max_execution_time', 0);

        $formJSON = json_decode($request->get('form'));
        $form = construyeFormulario($formJSON);
        
        $fechaInicio = fecha_Y_m_d($form->filtro_fecha_inicio);
        $fechaTermino = fecha_Y_m_d($form->filtro_fecha_termino);

        $archivosAcepta = ArchivoAcepta::with(['getProveedor','getTipoDocumento','getAceptaMotivoRezagar.getMotivoRezagar'])
                                       ->where('cargado', 0)
                                       ->doesnthave('getArchivoAceptaRevisionBodega')
                                       ->where('id_user_rechazo', null)
                                       ->where('id_user_rezagar', '<>', null);

        if ( $form->filtro_limite_rechazo == "No" ) {

            if ( $form->filtro_numero_documento == null ) {
                $archivosAcepta = $archivosAcepta->where('created_at','>=', $fechaInicio.' 00:00:00')
                                                ->where('created_at','<=', $fechaTermino.' 23:59:59');
            } else {
                $archivosAcepta = $archivosAcepta->where('folio',  trim($form->filtro_numero_documento) );
            }
                            
            if ( isset($form->filtro_proveedor) ) {
                $archivosAcepta = $archivosAcepta->WhereIn('id_proveedor', $form->filtro_proveedor );
            }

            if ( isset($form->filtro_tipo_documento) ) {
                $archivosAcepta = $archivosAcepta->WhereIn('id_tipo_documento', $form->filtro_tipo_documento);
            }

        } else {

            $expDate = Carbon::now()->subDays(8);
            $expDate2 = Carbon::now()->subDays(7);

            $archivosAcepta = $archivosAcepta->where('publicacion', '>=', fecha_Y_m_d($expDate).' 00:00:00' )
                                             ->where('publicacion', '<', fecha_Y_m_d($expDate2).' 00:00:00' );

        }

        $archivosAcepta = $archivosAcepta->get();

        $data = [];

        foreach ( $archivosAcepta as $archivo ) {
            $data[] = $archivo->getDatosRezagados();
        }

        $respuesta = array(
            "data" => $data
        );

        return json_encode($respuesta);
    }

    public function postRezagadosExcel(Request $request)
    {
        set_time_limit(0);
        ini_set('max_execution_time', 0);

        \Excel::create('SIFCON | Informe Rezagados Acepta '.date('d-m-Y').'', function($excel) use($request) {
            $fechaInicio = fecha_Y_m_d($request->input('filtro_fecha_inicio'));
            $fechaTermino = fecha_Y_m_d($request->input('filtro_fecha_termino'));

            $archivosAcepta = ArchivoAcepta::with(['getProveedor','getTipoDocumento','getAceptaMotivoRezagar.getMotivoRezagar'])
                                        ->where('cargado', 0)
                                        ->doesnthave('getArchivoAceptaRevisionBodega')
                                        ->where('id_user_rechazo', null)
                                        ->where('id_user_rezagar', '<>', null);


            if ( $request->get('filtro_numero_documento') == null ) {
                $archivosAcepta = $archivosAcepta->where('created_at','>=',$fechaInicio.' 00:00:00')
                                                ->where('created_at','<=',$fechaTermino.' 23:59:59');
            } else {
                $archivosAcepta = $archivosAcepta->where('folio', trim($request->input('filtro_numero_documento')) );
            }
                        
            if ( $request->input('filtro_proveedor') != null ) {
                $archivosAcepta = $archivosAcepta->WhereIn('id_proveedor',$request->input('filtro_proveedor'));
            }

            if ( $request->input('filtro_tipo_documento') != null ) {
                $archivosAcepta = $archivosAcepta->WhereIn('id_tipo_documento',$request->input('filtro_tipo_documento'));
            }

            $archivosAcepta = $archivosAcepta->get();

            $excel->sheet('Documentos Acepta', function($sheet) use ($archivosAcepta) {

                $sheet->row(1, [
                    'Estado',
                    'Chile-Compra',
                    'Proveedor',
                    'RUT',
                    'Tipo Dcto.',
                    'N° Dcto.',
                    'F/Doc.',
                    'M/Doc.',
                    'Fecha RD',
                    'Mes RD',
                    'Monto',
                    'URI',
                ]);

                foreach ($archivosAcepta as $index => $archivo) {
                    // Se trabaja el N° de orden compra.
                    $numOrdenCompra = '';
                    if ($archivo->referencias != "" && $archivo->referencias != null) {

                        $arrayJson = json_decode($archivo->referencias, true);
                        if ($arrayJson != null) {
                            foreach ($arrayJson as $itemJson) {
                                if ($itemJson['Tipo'] == 801) {
                                    $numOrdenCompra = $itemJson['Folio'];
                                }
                            }
                        }

                    }

                    $tipoDocumento = '';    
                    if ( $archivo->getTipoDocumento ) {
                        $tipoDocumento .= $archivo->getTipoDocumento->nombre;
                    } else {
                        $tipoDocumento .= $archivo->tipo_documento;
                    }
                    
                    $rutProveedor = '';
                    $nombreProveedor = '';
                    if ( $archivo->getProveedor ) {
                        $rutProveedor .= $archivo->getProveedor->rut;
                        $nombreProveedor .= $archivo->getProveedor->nombre;
                    } else {
                        $rutProveedor .= $archivo->emisor;
                        $nombreProveedor .= $archivo->razon_social_emisor;
                    }

                    $sheet->row($index+2, [
                        $archivo->getEstadoAcepta(),
                        $numOrdenCompra,
                        //explode("-",$documento->fecha_sigfe)[1], // mes de la fecha sigfe
                        $nombreProveedor,
                        $rutProveedor,
                        $tipoDocumento,
                        $archivo->folio,
                        fecha_dmY($archivo->emision),
                        explode("-",$archivo->emision)[1], // mes de la fecha del documento
                        fecha_dmY($archivo->created_at),
                        explode("-",$archivo->created_at)[1], // mes de la fecha de recepcion del documento
                        $archivo->monto_total,
                        $archivo->uri,
                    ]);
                }

               
            });

        })->export('xlsx');

    }

    public function getCargaReclamados()
    {
        return view('archivo_acepta.carga_reclamados');
    }

    public function postCargaReclamados(Request $request)
    {
        // dd('Cargando reclamados', $request->all());

        $archivo = $request->file('archivo');
        
        if ($archivo == null) {
            
            $datos = array(
                'mensaje' => 'No existe archivo',
                'estado' => 'error',   
            );

            return response()->json($datos,200);
        } else {

            $nombreOriginalExcel = str_replace('.xlsx','',$archivo->getClientOriginalName() );
            $arreglo_nombre = explode(".", $archivo->getClientOriginalName() );
            $extension = strtolower( end( $arreglo_nombre ) );

            if ($extension != 'xlsx') {
                $datos = array(
                    'mensaje' => 'Extension incorrecta del archivo',
                    'estado' => 'error',
                );
    
                return response()->json($datos,200);
            }

            $excel = Excel::selectSheetsByIndex(0)->load($archivo)->get();
            $resumen = '';
            $contadorDoc = 0;
            $contadorAcepta = 0;
            $contadorNoEncontrado = 0;

            foreach ($excel as $key => $fila) {
                

                $id_tipo_documento = 0;
                switch ($fila->tipo_doc) {
                    case 33:
                        $id_tipo_documento = 1;
                        break;
                    case 34:
                        $id_tipo_documento = 1;
                        break;
                    case 39:
                        $id_tipo_documento = 7;
                        break;
                    case 46:
                        $id_tipo_documento = 7;
                        break;
                    case 56:
                        $id_tipo_documento = 5;
                        break;
                    case 61:
                        $id_tipo_documento = 4;
                        break;
                    default:
                        $id_tipo_documento = 0;
                        break;
                }

                $proveedor = Proveedor::where('rut', trim( $fila->rut_proveedor ) )->first(); 

                // dd($id_tipo_documento, $proveedor, $fila);

                if ( is_object($proveedor) ) {

                    $documento = Documento::where('id_proveedor', $proveedor->id)
                                          ->where('numero_documento', trim($fila->folio) ) 
                                          ->where('id_tipo_documento', $id_tipo_documento)
                                          ->first();

                    if ( is_object($documento) ) {

                        $contadorDoc++;
                        $documento->reclamado = 1;
                        $documento->save();

                    } else {

                        $archivoAcepta = ArchivoAcepta::where('id_tipo_documento', $id_tipo_documento)
                                                      ->where('folio', trim($fila->folio))
                                                      ->where('id_proveedor', $proveedor->id)
                                                      ->first();
                                        
                        if ( is_object($archivoAcepta) ) {

                            $contadorAcepta++;
                            $archivoAcepta->reclamado = 1;
                            $archivoAcepta->save();

                        } else {
                            $contadorNoEncontrado++;
                        }

                    }

                } else {
                    $contadorNoEncontrado++;
                }

            }

        }

        $resumen .= '<strong>';
        $resumen .= 'Documentos encontrados : '.formatoMiles($contadorDoc).'<br>';
        $resumen .= 'Archivos Acepta encontrados : '.formatoMiles($contadorAcepta).'<br>';
        $resumen .= 'Documentos no encontrados : '.formatoMiles($contadorNoEncontrado).'<br>';

        $resumen .= '</strong>';

        $datos = array(
            'mensaje' => 'Carga exitosa.',
            'estado'  => 'success',
            'resumen' => $resumen,
        );

        return response()->json($datos,200);
    }

    public function getHomeExcel($year)
    {
        \Excel::create('SIFCON | Informe Acepta por ingresar '.$year.' - '.date('d-m-Y').'', function($excel) use ($year) {
           
            $documentosAcepta = ArchivoAcepta::with(['getProveedor','getTipoDocumento','getUsuarioResponsable'])
                                            ->where('cargado', 0)
                                            // ->where('created_at','>=','2019-06-22 00:00:00')
                                            ->where('id_user_rechazo', null);
            if ( $year == '2019' ) {
                $documentosAcepta = $documentosAcepta->where('emision','>=','2019-06-22 00:00:00')
                                                     ->where('emision','<=','2019-12-31 23:59:59');
            }

            if ( $year == '2020' ) {
                $documentosAcepta = $documentosAcepta->where('emision','>=','2020-01-01 00:00:00')
                                                     ->where('emision','<=','2020-12-31 23:59:59');
            }
                                            
            $documentosAcepta = $documentosAcepta->get();


            $excel->sheet('Documentos Acepta', function($sheet) use($documentosAcepta) {

                $sheet->row(1, [
                    'Estado',
                    'Responsable',
                    'Chile-Compra',
                    'Proveedor',
                    'RUT',
                    'Tipo Dcto.',
                    'N° Dcto.',
                    'F/Doc.',
                    'M/Doc.',
                    'Fecha carga desde excel acepta',
                    'Mes carga desde excel acepta',
                    'Monto',
                    'URI',
                ]);
                
                foreach ( $documentosAcepta as $index => $documento ) {

                    // Se trabaja el N° de orden compra.
                    $numOrdenCompra = '';
                    if ($documento->referencias != "" && $documento->referencias != null) {

                        $arrayJson = json_decode($documento->referencias, true);
                        if ($arrayJson != null) {
                            foreach ($arrayJson as $itemJson) {
                                if ($itemJson['Tipo'] == 801) {
                                    $numOrdenCompra = $itemJson['Folio'];
                                }
                            }
                        }
                    }

                    $tipoDocumento = ''; 
                    if ( $documento->getTipoDocumento ) {
                        $tipoDocumento .= $documento->getTipoDocumento->nombre;
                    } else {
                        $tipoDocumento .= $documento->tipo_documento;
                    }

                    $rutProveedor = '';
                    $nombreProveedor = '';
                    if ( $documento->getProveedor ) {
                        $rutProveedor .= $documento->getProveedor->rut;
                        $nombreProveedor .= $documento->getProveedor->nombre;
                    } else {
                        $rutProveedor .= $documento->emisor;
                        $nombreProveedor .= $documento->razon_social_emisor;
                    }

                    $monto = '';
                    if ( $documento->id_tipo_documento == 4 || $documento->id_tipo_documento == 10 ) {
                        $monto .= '-'.$documento->monto_total;
                    } else {
                        $monto .= $documento->monto_total;
                    }

                    $sheet->row( $index+2 , [
                        $documento->getEstadoAcepta(),
                        ($documento->getUsuarioResponsable) ? $documento->getUsuarioResponsable->name : '',
                        $numOrdenCompra,
                        $nombreProveedor,
                        $rutProveedor,
                        $tipoDocumento,
                        $documento->folio,
                        fecha_dmY($documento->emision),
                        explode("-",$documento->emision)[1], // mes de la fecha del documento
                        fecha_dmY($documento->created_at),
                        explode("-",$documento->created_at)[1], // mes de la fecha de recepcion del documento
                        $monto,
                        $documento->uri,
                    ]);

                }

            });

        })->export('xlsx');
        
    }
    
    public function getModalToRevisionBodega($id)
    {
        $archivoAcepta = ArchivoAcepta::with(['getProveedor','getTipoDocumento','getAceptaMotivoRezagar'])
                                      ->findOrFail($id);

        return view('archivo_acepta.modal_to_revision_bodega', compact('archivoAcepta'));
    }

    public function postToRevisionBodega(Request $request)
    {
        // dd('Pasando a revision de bodega :', $request->all());
        // Mensajes del validador
        $messages = [
            'required' => 'Debe ingresar el :attribute',
            'numeric'  => 'El :attribute debe solo contener números',
            'max'      => 'El :attribute no debe exeder los :max caracteres',
            'min'      => 'El :attribute debe tener minimo :min caracteres',
            'unique'   => 'El valor :attribute ya existe en la base de datos',
        ];

        // Reglas del validador
        $rules = [
            '_id' => 'required',
        ];
        
        // Validador de los input del formulario
        $validator = \Validator::make($request->all(), $rules, $messages);

        //Si contiene errores se devuelven todos los errores, de lo contrario guarda en la base de datos
        if ( $validator->fails() ) {
            return response()->json($validator->errors(),400);
        } else {

            $archivoAcepta = ArchivoAcepta::findOrFail($request->input('_id'));
            $aceptaRevisionBodega = new ArchivoAceptaRevisionBodega;
            $aceptaRevisionBodega->id_user_created = Auth::user()->id;
            $aceptaRevisionBodega->id_archivo_acepta = $archivoAcepta->id;
            $aceptaRevisionBodega->save();

            $datos = array(
                'mensaje' => 'El Documento se encuentra pendiente de revisión de bodega',
                'estado'  => 'success',
                'id'      => $archivoAcepta->id,
            );
        }

        return response()->json($datos,200);

    }

    public function getArchivosRevisionBodega()
    {
        set_time_limit(0);
        ini_set('max_execution_time', 0);

        $tiposDocumento = TipoDocumento::whereNotIn('id',[2,8])->get();
        $proveedores = Proveedor::all();

        return view('archivo_acepta.revision_bodega')->with('tiposDocumento', $tiposDocumento)
                                                     ->with('proveedores', $proveedores);
    }

    public function dataTableRevisionBodega(Request $request)
    {
        // \Debugbar::disable();
        
        set_time_limit(0);
        ini_set('max_execution_time', 0);

        $formJSON = json_decode($request->get('form'));
        $form = construyeFormulario($formJSON);
        
        $fechaInicio = fecha_Y_m_d($form->filtro_fecha_inicio);
        $fechaTermino = fecha_Y_m_d($form->filtro_fecha_termino);

        $archivosAcepta = ArchivoAcepta::with([
                                               'getProveedor',
                                               'getTipoDocumento',
                                               'getAceptaMotivoRezagar.getMotivoRezagar',
                                               'getArchivoAceptaRevisionBodega'
                                              ])
                                       ->where('cargado', 0)
                                       ->has('getArchivoAceptaRevisionBodega')
                                       ->where('id_user_rechazo', null);

        if ( $form->filtro_limite_rechazo == "No" ) {

            if ( $form->filtro_numero_documento == null ) {
                $archivosAcepta = $archivosAcepta->where('created_at','>=', $fechaInicio.' 00:00:00')
                                                ->where('created_at','<=', $fechaTermino.' 23:59:59');
            } else {
                $archivosAcepta = $archivosAcepta->where('folio',  trim($form->filtro_numero_documento) );
            }
                            
            if ( isset($form->filtro_proveedor) ) {
                $archivosAcepta = $archivosAcepta->WhereIn('id_proveedor', $form->filtro_proveedor );
            }

            if ( isset($form->filtro_tipo_documento) ) {
                $archivosAcepta = $archivosAcepta->WhereIn('id_tipo_documento', $form->filtro_tipo_documento);
            }

        } else {

            $expDate = Carbon::now()->subDays(8);
            $expDate2 = Carbon::now()->subDays(7);

            $archivosAcepta = $archivosAcepta->where('publicacion', '>=', fecha_Y_m_d($expDate).' 00:00:00' )
                                             ->where('publicacion', '<', fecha_Y_m_d($expDate2).' 00:00:00' );

        }

        if ( $form->filtro_respuesta_bodega == "Con Respuesta"  ) {
            $archivosAcepta = $archivosAcepta->whereHas('getArchivoAceptaRevisionBodega', function($query) {
                                                    $query->where('revisado', 1);
                                                });
        }

        if ( $form->filtro_respuesta_bodega == "Sin Respuesta"  ) {
            $archivosAcepta = $archivosAcepta->whereHas('getArchivoAceptaRevisionBodega', function($query) {
                                                    $query->where('revisado', 0);
                                                });
        }

        if ( \Entrust::hasRole('usuario-ingreso') ) {
            $archivosAcepta = $archivosAcepta->where(function ($query){
                                                $query->where('id_user_responsable', \Auth::user()->id)
                                                      ->orWhere('id_user_responsable', null);
                                            });
        }

        $archivosAcepta = $archivosAcepta->get();
        
        $esReferente = \Auth::user()->getReferenteTecnico;
        $esReferenteBodega = $esReferente == true && \Auth::user()->getReferenteTecnico->id == 13 ? true : false;
        // Variables para los botones de los documentos acepta
        $verFacturaDocumentoAcepta = \Entrust::can('ver-factura-documento-acepta'); // \Auth::user()->can('ver-factura-documento-acepta')
        $cargaDocumentoAceptaAlSistema = \Entrust::can('carga-documento-acepta-al-sistema');
        $rechazarDocumentoAcepta = \Entrust::can('rechazar-documento-acepta');
        $data = [];
        foreach ( $archivosAcepta as $archivo ) {
            $data[] = $archivo->getDatosRevisionBodega( $esReferente, $esReferenteBodega, $verFacturaDocumentoAcepta, $cargaDocumentoAceptaAlSistema, $rechazarDocumentoAcepta);
        }

        $respuesta = array(
            "data" => $data
        );

        return json_encode($respuesta);
    }

    public function getModalRevisionBodega($id)
    {
        $archivoAcepta = ArchivoAcepta::with(['getProveedor','getTipoDocumento','getAceptaMotivoRezagar'])
                                      ->findOrFail($id);

        return view('archivo_acepta.modal_revision_bodega', compact('archivoAcepta'));
    }

    public function postRevisionBodega(Request $request)
    {
        // dd('revisión de bodega', $request->all());

        // Mensajes del validador
        $messages = [
            'required' => 'Debe ingresar el :attribute',
            'numeric'  => 'El :attribute debe solo contener números',
            'max'      => 'El :attribute no debe exeder los :max caracteres',
            'min'      => 'El :attribute debe tener minimo :min caracteres',
            'unique'   => 'El valor :attribute ya existe en la base de datos',
        ];

        // Reglas del validador
        $rules = [
            '_id'       => 'required',
            'respuesta' => 'required',
        ];
        
        // Validador de los input del formulario
        $validator = \Validator::make($request->all(), $rules, $messages);

        //Si contiene errores se devuelven todos los errores, de lo contrario guarda en la base de datos
        if ( $validator->fails() ) {
            return response()->json($validator->errors(),400);
        } else {

            $archivoAcepta = ArchivoAcepta::with(['getArchivoAceptaRevisionBodega'])
                                          ->findOrFail($request->input('_id'));

            $archivoAcepta->getArchivoAceptaRevisionBodega->revisado = 1;
            $archivoAcepta->getArchivoAceptaRevisionBodega->comentario = trim($request->get('comentario'));
            $archivoAcepta->getArchivoAceptaRevisionBodega->id_user_respuesta = Auth::user()->id;

            if ( $request->get('respuesta') == 'Con Recepción Total' ) {
                $archivoAcepta->getArchivoAceptaRevisionBodega->recepcion_total = 1;
            }

            if ( $request->get('respuesta') == 'Con Recepción Parcial' ) {
                $archivoAcepta->getArchivoAceptaRevisionBodega->recepcion_parcial = 1;
            }

            if ( $request->get('respuesta') == 'Sin Recepción' ) {
                $archivoAcepta->getArchivoAceptaRevisionBodega->sin_recepcion = 1;
            }

            $archivoAcepta->getArchivoAceptaRevisionBodega->save();

            $datos = array(
                'mensaje' => 'Se ha guardado correctamente la respuesta',
                'estado'  => 'success',
                'id'      => $archivoAcepta->id,
            );

        }

        return response()->json($datos, 200);

    }

    public function postRevisionBodegaExcel(Request $request)
    {
        set_time_limit(0);
        ini_set('max_execution_time', 0);

        \Excel::create('SIFCON | Informe Revisión por Bodega '.date('d-m-Y').'', function($excel) use($request) {
            $fechaInicio = fecha_Y_m_d($request->input('filtro_fecha_inicio'));
            $fechaTermino = fecha_Y_m_d($request->input('filtro_fecha_termino'));

            $archivosAcepta = ArchivoAcepta::with([
                                                   'getProveedor','getTipoDocumento','getAceptaMotivoRezagar.getMotivoRezagar',
                                                   'getArchivoAceptaRevisionBodega','getUsuarioResponsable'
                                                   ])
                                        ->where('cargado', 0)
                                        ->has('getArchivoAceptaRevisionBodega')
                                        ->where('id_user_rechazo', null);


            if ( $request->get('filtro_numero_documento') == null ) {
                $archivosAcepta = $archivosAcepta->where('created_at','>=',$fechaInicio.' 00:00:00')
                                                ->where('created_at','<=',$fechaTermino.' 23:59:59');
            } else {
                $archivosAcepta = $archivosAcepta->where('folio', trim($request->input('filtro_numero_documento')) );
            }
                        
            if ( $request->input('filtro_proveedor') != null ) {
                $archivosAcepta = $archivosAcepta->WhereIn('id_proveedor',$request->input('filtro_proveedor'));
            }

            if ( $request->input('filtro_tipo_documento') != null ) {
                $archivosAcepta = $archivosAcepta->WhereIn('id_tipo_documento',$request->input('filtro_tipo_documento'));
            }

            if ( $request->input('filtro_respuesta_bodega') == "Con Respuesta"  ) {
                $archivosAcepta = $archivosAcepta->whereHas('getArchivoAceptaRevisionBodega', function($query) {
                                                        $query->where('revisado', 1);
                                                    });
            }
    
            if ( $request->input('filtro_respuesta_bodega') == "Sin Respuesta"  ) {
                $archivosAcepta = $archivosAcepta->whereHas('getArchivoAceptaRevisionBodega', function($query) {
                                                        $query->where('revisado', 0);
                                                    });
            }

            $archivosAcepta = $archivosAcepta->get();

            $excel->sheet('Documentos Acepta', function($sheet) use ($archivosAcepta) {

                $sheet->row(1, [
                    'Estado',
                    'Responsable',
                    'Chile-Compra',
                    'Proveedor',
                    'RUT',
                    'Tipo Dcto.',
                    'N° Dcto.',
                    'F/Doc.',
                    'M/Doc.',
                    'Fecha RD',
                    'Mes RD',
                    'Monto',
                    'Respuesta',
                    'Comentario',
                    'URI',
                ]);

                foreach ($archivosAcepta as $index => $archivo) {
                    // Se trabaja el N° de orden compra.
                    $numOrdenCompra = '';
                    if ($archivo->referencias != "" && $archivo->referencias != null) {

                        $arrayJson = json_decode($archivo->referencias, true);
                        if ($arrayJson != null) {
                            foreach ($arrayJson as $itemJson) {
                                if ($itemJson['Tipo'] == 801) {
                                    $numOrdenCompra = $itemJson['Folio'];
                                }
                            }
                        }

                    }

                    $tipoDocumento = '';
                    if ( $archivo->getTipoDocumento ) {
                        $tipoDocumento .= $archivo->getTipoDocumento->nombre;
                    } else {
                        $tipoDocumento .= $archivo->tipo_documento;
                    }
                    
                    $rutProveedor = '';
                    $nombreProveedor = '';
                    if ( $archivo->getProveedor ) {
                        $rutProveedor .= $archivo->getProveedor->rut;
                        $nombreProveedor .= $archivo->getProveedor->nombre;
                    } else {
                        $rutProveedor .= $archivo->emisor;
                        $nombreProveedor .= $archivo->razon_social_emisor;
                    }

                    $respuesta = '';
                    if ( $archivo->getArchivoAceptaRevisionBodega->recepcion_total == 1 ) {
                        $respuesta = 'Recepción Total';
                    }

                    if ( $archivo->getArchivoAceptaRevisionBodega->recepcion_parcial == 1 ) {
                        $respuesta = 'Recepción Parcial';
                    }

                    if ( $archivo->getArchivoAceptaRevisionBodega->sin_recepcion == 1 ) {
                        $respuesta = 'Sin Recepción';
                    }

                    $sheet->row($index+2, [
                        $archivo->getEstadoAcepta(),
                        ($archivo->getUsuarioResponsable) ? $archivo->getUsuarioResponsable->name : '',
                        $numOrdenCompra,
                        $nombreProveedor,
                        $rutProveedor,
                        $tipoDocumento,
                        $archivo->folio,
                        ( strpos($archivo->emision,'-') !== false ) ? fecha_dmY($archivo->emision) : '',
                        ( strpos($archivo->emision,'-') !== false ) ? explode("-",$archivo->emision)[1] : '', // mes de la fecha del documento
                        fecha_dmY($archivo->created_at),
                        explode("-",$archivo->created_at)[1], // mes de la fecha de recepcion del documento
                        $archivo->monto_total,
                        $respuesta,
                        $archivo->getArchivoAceptaRevisionBodega->comentario,
                        $archivo->uri,
                    ]);

                }

            });

        })->export('xlsx');

    }

    public function getModalTraza($id)
    {
        $archivoAcepta = ArchivoAcepta::findOrFail($id);

        return view('archivo_acepta.modal_traza_acepta')->with('archivoAcepta', $archivoAcepta);
    }

    public function getModalQuitaReclamo($id)
    {
        $archivoAcepta = ArchivoAcepta::findOrFail($id);

        return view('archivo_acepta.modal_quita_reclamo')->with('archivoAcepta', $archivoAcepta);
    }

    public function postQuitarReclamo(Request $request)
    {
        $archivoAcepta = ArchivoAcepta::findOrFail($request->input('_id'));
        $archivoAcepta->reclamado = null;
        $archivoAcepta->save();

        $datos = array(
            'mensaje' => 'Se ha quitado correctamente el reclamo.<br>Se actualizará  la información de la tabla.',
            'estado'  => 'success',
            'id'      => $archivoAcepta->id,
        );

        return response()->json($datos, 200);
    }


}
