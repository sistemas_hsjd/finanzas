<?php namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests;
use Auth;
use App\TipoArchivo;

class TipoArchivoController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
    */
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index()
    {
        if (!\Entrust::can(['crear-tipo-archivo','editar-tipo-archivo','eliminar-tipo-archivo','ver-tipo-archivo','ver-general-tipo-archivo'])) {
            return \Redirect::to('home');
        }
        $tiposArchivo = TipoArchivo::all();
        return view('tipo_archivo.index')->with('tiposArchivo',$tiposArchivo);
    }

    public function getModalVer($id)
    {
        $tipoArchivo = TipoArchivo::findOrFail($id);
        return view('tipo_archivo.modal_ver_tipo_archivo')->with('tipoArchivo',$tipoArchivo);
    }

    public function getModalEditar($id)
    {
        $tipoArchivo = TipoArchivo::findOrFail($id);
        return view('tipo_archivo.modal_editar_tipo_archivo')->with('tipoArchivo',$tipoArchivo);
    }

    public function postEditar(Request $request)
    {
        // dd('Holo',$request->all());
        // Mensajes del validador
        $messages = [
            'required' => 'Debe ingresar el :attribute',
            'nombre.required' => 'Debe ingresar el Nombre',
            'numeric' => 'El :attribute debe solo contener números',
            'max' => 'El :attribute no debe exeder los :max caracteres',
            'min' => 'El :attribute debe tener minimo :min caracteres',
            'unique' => 'El valor :attribute ya existe en la base de datos',
        ];

        // Reglas del validador
        $rules = [
            'nombre' => 'required',
        ];
        
        // Validador de los input del formulario
        $validator = \Validator::make($request->all(), $rules, $messages);

        //Si contiene errores se devuelven todos los errores, de lo contrario guarda en la base de datos
        if ( $validator->fails() ) {
            return response()->json($validator->errors(),400);
        } else {
            $editTipoArchivo = TipoArchivo::findOrFail($request->input('_id'));
            $editTipoArchivo->editTipoArchivo($request);

            $datos = array(
                'mensaje' => 'Edición exitosa del Tipo de Archivo',
                'estado' => 'success',
                'nombre' => $editTipoArchivo->nombre,
                'id' => $editTipoArchivo->id,
            );
        }

        return response()->json($datos,200);
    }

    public function create()
    {
        return view('tipo_archivo.modal_crear_tipo_archivo');
    }

    public function store(Request $request)
    {
        // Mensajes del validador
        $messages = [
            'required' => 'Debe ingresar el :attribute',
            'nombre.required' => 'Debe ingresar el Nombre',
            'numeric' => 'El :attribute debe solo contener números',
            'max' => 'El :attribute no debe exeder los :max caracteres',
            'min' => 'El :attribute debe tener minimo :min caracteres',
            'unique' => 'El valor :attribute ya existe en la base de datos',
        ];

        // Reglas del validador
        $rules = [
            'nombre' => 'required',
        ];

        // Validador de los input del formulario
        $validator = \Validator::make($request->all(), $rules, $messages);

        //Si contiene errores se devuelven todos los errores, de lo contrario guarda en la base de datos
        if ( $validator->fails() ) {
            return response()->json($validator->errors(),400);
        } else {

            try {
                $newTipoArchivo = new TipoArchivo($request);
            } catch (QueryException $e) {
                $datos = array(
                    'estado' => 'error',
                    'mensaje' => 'Existen problemas al momento de guardar el Tipo de Archivo',
                );
                return response()->json($datos,200);
            }

            $datos = array(
                'mensaje' => 'Se ha creado el Tipo de Archivo',
                'estado' => 'success',
                'nombre' => $newTipoArchivo->nombre,
                'id' => $newTipoArchivo->id,
            );
        }

        return response()->json($datos,200);
    }

    public function getModalEliminar($id)
    {
        $tipoArchivo = TipoArchivo::findOrFail($id);
        return view('tipo_archivo.modal_eliminar_tipo_archivo')->with('tipoArchivo',$tipoArchivo);
    }

    public function postEliminar(Request $request)
    {
        $deleteTipoArchivo = TipoArchivo::find($request->input('_id'));
        if ( !is_object($deleteTipoArchivo) ) {
            $datos = array(
                'mensaje' => 'No se encuentra el Tipo de Archivo en la Base de Datos',
                'estado' => 'error',
            );
        } else {
            $deleteTipoArchivo->id_user_deleted = Auth::user()->id;
            $deleteTipoArchivo->timestamps = false;
            $deleteTipoArchivo->save();
            $deleteTipoArchivo->delete();

            $datos = array(
                'mensaje' => 'Se ha eliminado correctamente el Tipo de Archivo.',
                'estado' => 'success',
                'id' => $deleteTipoArchivo->id,
            );
        }

        return response()->json($datos,200);
    }
}
