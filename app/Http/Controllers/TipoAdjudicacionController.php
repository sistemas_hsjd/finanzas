<?php namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests;
use Auth;
use App\TipoAdjudicacion;

class TipoAdjudicacionController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
    */
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index()
    {
        if (!\Entrust::can(['crear-tipo-adjudicacion','editar-tipo-adjudicacion','eliminar-tipo-adjudicacion','ver-tipo-adjudicacion','ver-general-tipo-adjudicacion'])) {
            return \Redirect::to('home');
        }
        $tiposAdjudicacion = TipoAdjudicacion::all();
        return view('tipo_adjudicacion.index')->with('tiposAdjudicacion',$tiposAdjudicacion);
    }

    public function getModalVer($id)
    {
        $tipoAdjudicacion = TipoAdjudicacion::findOrFail($id);
        return view('tipo_adjudicacion.modal_ver_tipo_adjudicacion')->with('tipoAdjudicacion',$tipoAdjudicacion);
    }

    public function getModalEditar($id)
    {
        $tipoAdjudicacion = TipoAdjudicacion::findOrFail($id);
        return view('tipo_adjudicacion.modal_editar_tipo_adjudicacion')->with('tipoAdjudicacion',$tipoAdjudicacion);
    }

    public function postEditar(Request $request)
    {
        // dd('Holo',$request->all());
        // Mensajes del validador
        $messages = [
            'required' => 'Debe ingresar el :attribute',
            'nombre.required' => 'Debe ingresar el Nombre',
            'numeric' => 'El :attribute debe solo contener números',
            'max' => 'El :attribute no debe exeder los :max caracteres',
            'min' => 'El :attribute debe tener minimo :min caracteres',
            'unique' => 'El valor :attribute ya existe en la base de datos',
        ];

        // Reglas del validador
        $rules = [
            'nombre' => 'required',
        ];
        
        // Validador de los input del formulario
        $validator = \Validator::make($request->all(), $rules, $messages);

        //Si contiene errores se devuelven todos los errores, de lo contrario guarda en la base de datos
        if ( $validator->fails() ) {
            return response()->json($validator->errors(),400);
        } else {
            $editTipoAdjudicacion = TipoAdjudicacion::findOrFail($request->input('_id'));
            $editTipoAdjudicacion->editTipoAdjudicacion($request);

            $datos = array(
                'mensaje' => 'Edición exitosa del Tipo de Adjudicación',
                'estado'  => 'success',
                'nombre'  => $editTipoAdjudicacion->nombre,
                'id'      => $editTipoAdjudicacion->id,
            );
        }

        return response()->json($datos,200);
    }
    
    public function create()
    {
        return view('tipo_adjudicacion.modal_crear_tipo_adjudicacion');
    }

    public function store(Request $request)
    {
        // Mensajes del validador
        $messages = [
            'required' => 'Debe ingresar el :attribute',
            'nombre.required' => 'Debe ingresar el Nombre',
            'numeric' => 'El :attribute debe solo contener números',
            'max' => 'El :attribute no debe exeder los :max caracteres',
            'min' => 'El :attribute debe tener minimo :min caracteres',
            'unique' => 'El valor :attribute ya existe en la base de datos',
        ];

        // Reglas del validador
        $rules = [
            'nombre' => 'required',
        ];

        // Validador de los input del formulario
        $validator = \Validator::make($request->all(), $rules, $messages);

        //Si contiene errores se devuelven todos los errores, de lo contrario guarda en la base de datos
        if ( $validator->fails() ) {
            return response()->json($validator->errors(),400);
        } else {

            try {
                $newTipoAdjudicacion = new TipoAdjudicacion($request);
            } catch (QueryException $e) {
                $datos = array(
                    'estado' => 'error',
                    'mensaje' => 'Existen problemas al momento de guardar el Tipo de Adjudicación',
                );
                return response()->json($datos,200);
            }

            $datos = array(
                'mensaje' => 'Se ha creado el Tipo de Adjudicación',
                'estado' => 'success',
                'nombre' => $newTipoAdjudicacion->nombre,
                'id' => $newTipoAdjudicacion->id,
            );
        }

        return response()->json($datos,200);
    }

    public function getModalEliminar($id)
    {
        $tipoAdjudicacion = TipoAdjudicacion::findOrFail($id);
        return view('tipo_adjudicacion.modal_eliminar_tipo_adjudicacion')->with('tipoAdjudicacion',$tipoAdjudicacion);
    }

    public function postEliminar(Request $request)
    {
        $deleteTipoAdjudicacion = TipoAdjudicacion::find($request->input('_id'));
        if ( !is_object($deleteTipoAdjudicacion) ) {
            $datos = array(
                'mensaje' => 'No se encuentra el Tipo de Adjudicación en la Base de Datos',
                'estado' => 'error',
            );
        } else {
            $deleteTipoAdjudicacion->id_user_deleted = Auth::user()->id;
            $deleteTipoAdjudicacion->timestamps = false;
            $deleteTipoAdjudicacion->save();
            $deleteTipoAdjudicacion->delete();

            $datos = array(
                'mensaje' => 'Se ha eliminado correctamente el Tipo de Adjudicación.',
                'estado' => 'success',
                'id' => $deleteTipoAdjudicacion->id,
            );
        }

        return response()->json($datos,200);
    }
}
