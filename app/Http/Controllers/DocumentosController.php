<?php namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests;
use Auth;
use Excel;
use Mpdf\Mpdf;
use iio\libmergepdf\Merger;
use iio\libmergepdf\Driver\TcpdiDriver;
use App\Traits\concatenarArchivosDocumento;
use App\Traits\relacionRecepcionesNoValidadas;

use App\TipoDocumento;
use App\Proveedor;
use App\ModalidadCompra;
use App\TipoAdjudicacion;
use App\TipoInforme;
use App\User;
use App\ReferenteTecnico;
use App\TipoArchivo;
use App\ItemPresupuestario;
use App\Documento;
use App\Devengo;
use App\Archivo;
use App\RelacionDocumentoWsDocumento;
use App\ArchivoAcepta;
use App\WsDocumento;
use App\WsContrato;
use App\WsOrdenCompra;
use App\WsArchivo;
use App\MotivoProblema;
use App\DocumentoMotivoProblema;
use App\FolioSigfe;
use App\Contrato;
use App\OrdenCompra;
use App\DocumentoContratoOrdenCompra;
use App\HermesDocumento;


class DocumentosController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
    */
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function getOrdenCompra()
    {
        set_time_limit ( 0 );
        ini_set('max_execution_time', 0);

        $docs = Documento::with(['getArchivos.getTipoArchivo'])
                            ->whereHas('getArchivos.getTipoArchivo', function ($q){
                                $q->whereNotIn('id',[3]);
                            })
                            ->where('id_tipo_documento', 1)
                            ->where('fecha_ingreso','>=','2020-01-01 00:00:00')
                            ->where('documento_compra','LIKE','%1641-%')
                            ->take(3)->get();

        foreach ( $docs as $key => $doc ) {

            $content = file_get_contents('http://www.mercadopublico.cl/PurchaseOrder/Modules/PO/DetailsPurchaseOrder.aspx?codigooc='.$doc->documento_compra);
            if ( strpos( $content,'Número de orden de compra no es válido') === false ) {

                $posicionInicial = strpos($content, 'PDFReport.aspx?qs');
                $content2 = substr($content, $posicionInicial);
                $posicionFinal = strpos($content2, "==");
                $content3 = substr($content2, 0, $posicionFinal);

                $pdfOC = file_get_contents('http://www.mercadopublico.cl/PurchaseOrder/Modules/PO/'.$content3.'==');

                if ( !file_exists( public_path().'/testOC') ) {
                    mkdir(public_path().'/testOC', 0775, true);
                    chmod(public_path().'/testOC', 0775);
                }
                if ( !file_exists( public_path().'/testOC/1') ) {
                    mkdir(public_path().'/testOC/1', 0755);
                    chmod(public_path().'/testOC/1', 0775);
                }

                file_put_contents( public_path().'/testOC/1/'.$doc->documento_compra.'.pdf' , $pdfOC);

                dd('listo');
            }

        }

    }

    public function getActualizaValidacion()
    {
        set_time_limit ( 0 );
        ini_set('max_execution_time', 0);
        // ini_set('memory_limit','4000M');

         // Arreglar documentos con relaciones con recepciones validadas repetidas
        //  $docsRecepcionesValidadas = Documento::with(['getRelacionRecepcionesValidadas.getWsDocumento'])
        //                                         ->has('getRelacionRecepcionesValidadas')
        //                                         ->where('validado', null)
        //                                         ->where('fecha_validado', null)
        //                                         ->get();


        // $contaRelacionesEliminadas = 0;
        // $contaDocumentosArreglados = 0;
        // foreach ( $docsRecepcionesValidadas as $doc ) {
        //     if ( $doc->getRelacionRecepcionesValidadas->count() > 1 ) {

        //         $monto = 0;
        //         $numeroDoc = '';
        //         $tipoDoc = '';
        //         $sumaMontoRecepciones = 0;
        //         foreach ( $doc->getRelacionRecepcionesValidadas as $i => $relacion ) {

        //             if ( $i == 0 ) {
        //                 if ( $monto == 0 ) {
        //                     $monto = $relacion->getWsDocumento->documento_total;
        //                     $sumaMontoRecepciones += $relacion->getWsDocumento->documento_total;
        //                 }

        //                 if ( $numeroDoc == '') {
        //                     $numeroDoc = $relacion->getWsDocumento->documento;
        //                 }

        //                 if ( $tipoDoc == '' ) {
        //                     $tipoDoc = $relacion->getWsDocumento->tipo_documento;
        //                 }
        //             } else {
        //                 if ( $monto == $relacion->getWsDocumento->documento_total &&
        //                      $numeroDoc == $relacion->getWsDocumento->documento &&
        //                      $tipoDoc == $relacion->getWsDocumento->tipo_documento &&
        //                      $sumaMontoRecepciones >= $doc->total_documento
        //                    ) {
        //                         $relacion->delete();
        //                         $contaRelacionesEliminadas++;
        //                    }
        //             }
                
        //         }

        //     }
        // }


        // dd(
        //     'relaciones eliminadas : '.$contaRelacionesEliminadas,
        //     'documentos encontratos : '.$docsRecepcionesValidadas->count()
        // );

        // 1° Validar los que tienen memos visto bueno, invalidar los que tienen rechazo
        // Invalidar los documentos que tienen visto bueno solicitado
        
        // $documentosVistoBueno = Documento::whereHas('getVistoBueno', function ($q) {
        //                             $q->where('id_registrador_memo_respuesta', null)
        //                               ->where('created_at','>=', '2020-04-29 15:00:00');
        //                         })
        //                         ->doesnthave('getRelacionRecepcionesValidadas')
        //                         ->where('fecha_ingreso', '>=', '2019-06-22 00:00:00')
        //                         ->where('validado', 1)
        //                         ->where('fecha_validado', '>=', '2020-04-29 15:00:00')
        //                         ->get();

        
        
        // foreach ( $documentosVistoBueno as $doc ) {
        //     $doc->invalidar();
        //     $doc->actualizaValidacionDocumentosRelacionados();
        // }
        // dd('Documentos a quitar visto bueno: '.$documentosVistoBueno->count());
        
        
        
        // dd(' lo siguiente funciona.');
        // 2° validar las que tienen recepciones
        $docsRecepcionesValidadas = Documento::with(['getRelacionRecepcionesValidadas','getUnaRelacionRecepcionValidada'])
                                            ->has('getRelacionRecepcionesValidadas')
                                            // ->where('fecha_ingreso', '>=', '2019-06-22 00:00:00')
                                            ->where('validado', null)
                                            ->where('fecha_validado', null)
                                            ->get();


        $contaRecepcionesCuadradas = 0;
        $contaRecepcionesNoCuadrados = 0;
        foreach ( $docsRecepcionesValidadas as $doc ) {
            if ( $doc->diferenciaConRecepciones() > 100 || $doc->diferenciaConRecepciones() < -100 ) {
                // no cuadrar
                $contaRecepcionesNoCuadrados++;
                // dd('no cuadrar', $doc->total_documento_actualizado, $doc->diferenciaConRecepciones());
            } else {
                // Cuadrar
                // dd('cuadrar', $doc->total_documento_actualizado, $doc->diferenciaConRecepciones(), $doc->getUnaRelacionRecepcionValidada);

                echo "<br>Num doc a cuadrar: ". $doc->numero_documento;
                $doc->validado = 1;
                $doc->fecha_validado = $doc->getUnaRelacionRecepcionValidada->created_at;
                $doc->id_user_validado = $doc->getUnaRelacionRecepcionValidada->id_user_relacion;
                $doc->save();

                // $doc->actualizaValidacionDocumentosRelacionados();
                $contaRecepcionesCuadradas++;
            }
        }
        dd(
                $docsRecepcionesValidadas->count(),
                'Recepciones cuadradas : '.$contaRecepcionesCuadradas,
                'Reepciones noCuadradas : '.$contaRecepcionesNoCuadrados
            );

        // $docsRecepcionesNoValidadas = Documento::with(['getRelacionRecepcionesNoValidadas','getUnaRelacionRecepcionNoValidada'])
        //                                        ->has('getRelacionRecepcionesNoValidadas')
        //                                     //    ->where('fecha_ingreso', '>=', '2019-06-22 00:00:00')
        //                                        ->where('validado', null)
        //                                        ->where('fecha_validado', null)
        //                                        ->get();

        // $contaHermesCuadrados = 0;
        // $contaHermesNoCuadrados = 0;
        // foreach ( $docsRecepcionesNoValidadas as $doc ) {
        //     if ( $doc->diferenciaConRecepciones() > 100 || $doc->diferenciaConRecepciones() < -100 ) {
        //         // no cuadrar
        //         $contaHermesNoCuadrados++;
        //         // dd('no cuadrar', $doc->total_documento_actualizado, $doc->diferenciaConRecepciones());
        //     } else {
        //         // Cuadrar
        //         // dd('cuadrar', $doc->total_documento_actualizado, $doc->diferenciaConRecepciones(), $doc->getUnaRelacionRecepcionNoValidada);
        //         $doc->validado = 1;
        //         $doc->fecha_validado = $doc->getUnaRelacionRecepcionNoValidada->created_at;
        //         $doc->id_user_validado = $doc->getUnaRelacionRecepcionNoValidada->id_user_relacion;
        //         $doc->save();

        //         $doc->actualizaValidacionDocumentosRelacionados();
        //         $contaHermesCuadrados++;
        //     }
        // }

        // dd(
        //     $docsRecepcionesValidadas->count(),
        //     'Recepciones cuadradas : '.$contaRecepcionesCuadradas,
        //     'Reepciones noCuadradas : '.$contaRecepcionesNoCuadrados,
        //     $docsRecepcionesNoValidadas->count(),
        //     'Hermes cuadradas :'.$contaHermesCuadrados,
        //     'Hermes noCuadradas :'.$contaHermesNoCuadrados
        // );

        // Quitar validacion a documento con una recepcion con una firma
        dd('no hacer lo de abajo, ya se realizo una ves');
        $docsRecepcionesNoValidadas = Documento::with(['getRelacionRecepcionesNoValidadas','getUnaRelacionRecepcionNoValidada'])
                                               ->has('getRelacionRecepcionesNoValidadas')
                                               ->where('validado', 1)
                                               ->get();

        $contaHermesDescuadrados = 0;
        foreach ( $docsRecepcionesNoValidadas as $doc ) {
            $doc->invalidar();
            $doc->actualizaValidacionDocumentosRelacionados();
            $contaHermesDescuadrados++;
        }
                                

        dd(
            $docsRecepcionesValidadas->count(),
            'Recepciones cuadradas : '.$contaRecepcionesCuadradas,
            'Reepciones noCuadradas : '.$contaRecepcionesNoCuadrados,
            $docsRecepcionesNoValidadas->count(),
            'Hermes descuadradas :'.$contaHermesDescuadrados
        );


    }

    public function getActualizaMontos($year, $trimestre)
    {
        // dd('hola', $year);
        set_time_limit ( 0 );
        ini_set('max_execution_time', 0);
        $idDocs = [];
        $contadorActualizado = 0;
        switch ($trimestre) {
            case 1:
                $mes = 1;
                break;
            case 2:
                $mes = 4;
                break;
            case 3:
                $mes = 7;
                break;
            case 4:
                $mes = 10;
                break;
        }
        // $mes = $trimestre == 1 ? 1: 7;
        $first = true;
        CambiaMes : {
            if ( $first != true ) {
                $mes++;
            }       
        }
        Inicio: {

            Documento::disableAuditing();

            $documentos = Documento::with(['getDocumentosRelacionados'])
                                    //->has('getDocumentosRelacionados')
                                    ->whereYear('fecha_ingreso', '=', $year)
                                    ->whereMonth('fecha_ingreso', '=', $mes)
                                    ->whereNotIn('id', $idDocs)
                                    ->take(10)
                                    ->get();
            // dd($documentos->count());
            if ( $documentos->count() != 0 ) {
                foreach ( $documentos as $doc ) {
                
                    $idDocs[] = $doc->id;
                    
                    $doc->total_documento_actualizado = $doc->total_documento;
                    if ( $doc->getDocumentosRelacionados->count() > 0 ) {
    
                        foreach ( $doc->getDocumentosRelacionados as $docRelacionado ) {
                            if ( $docRelacionado->id_tipo_documento == 4 || $docRelacionado->id_tipo_documento == 10 ) {
                                // NC o CC, resta al total
                                $doc->total_documento_actualizado -= $docRelacionado->total_documento;
                            }
                    
                            if ( $docRelacionado->id_tipo_documento == 5 || $docRelacionado->id_tipo_documento == 11 ) {
                                // ND o CD, suma al total
                                $doc->total_documento_actualizado += $docRelacionado->total_documento;
                            }
                        }
                        $contadorActualizado++;
                    }
    
                    $doc->save();
                    
                }
                // dd($contadorActualizado, $idDocs);
                echo "<br>A inicio";
                goto Inicio;

            } else {
                $first = false;
                if ( $trimestre == 1 && $mes < 4) {
                    echo "<br>A cambiar mes Listo, mes :".$mes. " , contador: ".formatoMiles($contadorActualizado);
                    goto CambiaMes;
                } elseif ( $trimestre == 2 && $mes < 7 ) {
                    echo "<br>A cambiar mes Listo, mes :".$mes. " , contador: ".formatoMiles($contadorActualizado);
                    goto CambiaMes;
                } elseif ( $trimestre == 3 && $mes < 10 ) {
                    echo "<br>A cambiar mes Listo, mes :".$mes. " , contador: ".formatoMiles($contadorActualizado);
                    goto CambiaMes;
                } elseif ( $trimestre == 4 && $mes < 13 ) {
                    echo "<br>A cambiar mes Listo, mes :".$mes. " , contador: ".formatoMiles($contadorActualizado);
                    goto CambiaMes;
                } else {
                    echo "<br> Mes: ".$mes;
                }
                
            }
            
        }

        return "sali de todo";
        
    }

    public function getArchivosDocumentos()
    {
        set_time_limit(0);
        ini_set('max_execution_time', 0);

        $archivosAcepta = ArchivoAcepta::with(['getProveedor','getTipoDocumento'])
                                       ->where('id_proveedor', '<>', null)
                                       ->where('id_tipo_documento', '<>', null)
                                       ->orderBy('created_at', 'desc')
                                    //    ->where('cargado',0)
                                       // ->whereMonth('created_at', date('m'))
                                       ->where('created_at', '>=', '2019-12-01 00:00:00')
                                       ->get();
        $contador = 0;
        // \Log::info("Archivos Acepta (PDF), para actualizar archivo del documento  : {$archivosAcepta->count()}");
        echo "<br> Archivos acepta : ".$archivosAcepta->count();
        foreach ($archivosAcepta as $key => $archivoAcepta) {

            $documento = Documento::where('id_proveedor', $archivoAcepta->getProveedor->id)
                                  ->where('numero_documento', trim($archivoAcepta->folio) ) 
                                  ->where('id_tipo_documento', $archivoAcepta->getTipoDocumento->id)
                                  //->select('id_tipo_documento','deleted_at','numero_documento','id_proveedor','fecha_ingreso')
                                  ->first();
            echo "<br> ingrso al loop";
            if ( is_object($documento) ) {
                echo "<br> encuentra el documento id:".$documento->id;
                $archivoFacturaDelDocumento = Archivo::where('id_documento', $documento->id)
                                                     ->where('id_tipo_archivo', 1)
                                                     ->first();
                
                if ( !is_object($archivoFacturaDelDocumento) ) {
                    echo "<br> entrando a buscar una factura del documento id: ".$documento->id." N° doc : ".$documento->numero_documento;
                    // Se guarda la Factura (pdf) en el sistema, para el documento
                    $newArchivo = new Archivo();
                    $newArchivo->id_documento = $documento->id;
                    $newArchivo->id_tipo_archivo = 1;
                    $newArchivo->nombre = 'archivoAcepta_'.$archivoAcepta->id.'_'.date('Y_m_d_G_i_s');
                    $newArchivo->nombre_original = $newArchivo->nombre;
                    $newArchivo->ubicacion = "documento/".$documento->id."/".$newArchivo->nombre.".pdf";
                    $newArchivo->extension = "pdf";
                    $newArchivo->peso = 0;
                    $newArchivo->save();

                    $content = file_get_contents('http://windte1806.acepta.com/ca4webv3/PdfViewMedia?url='.$archivoAcepta->uri.'&menuTitle=Papel%2520Carta');
                    if ( !file_exists(public_path().'/documento/'.$documento->id) ) {
                        mkdir(public_path().'/documento/'.$documento->id, 0775, true);
                        chmod(public_path().'/documento/'.$documento->id, 0775);
                    }
                    $file = fopen( public_path().'/documento/'.$documento->id.'/'.$newArchivo->nombre.'.pdf', 'a');
                    fwrite($file, $content);
                    fclose($file);

                    $contador++;

                } else {
                    echo "<br>encuentra la factura";
                }

                $archivoAcepta->cargado = 1;
                $archivoAcepta->save();

            } else {
                echo"<br>no encuentra el documento : ".$archivoAcepta->folio;
            }
        }

        // \Log::info("Archivos Acepta (PDF) guardados en el sistema : {$contador}");

        // $this->info('carga-auxiliar-de-acepta-al-sistema ejecutado correctamente, pdfs: '. $contador);
    }
    
    public function getRelacionesDuplicadas()
    {
        set_time_limit ( 0 );
        ini_set('max_execution_time', 0);
        // buscar registros duplibados
        $countRelacionesEliminadas = 0;
        $idAux = null;
        Inicio: {

            if ( $idAux == null ) {
                $relaciones = RelacionDocumentoWsDocumento::all();
            } else {
                $relaciones = RelacionDocumentoWsDocumento::where('id', '>', $idAux)->get();
            }
            
            foreach ( $relaciones as $relacion ) {
                $relacionesAux = RelacionDocumentoWsDocumento::where('id_documento', $relacion->id_documento)
                                                            ->where('id_ws_documento', $relacion->id_ws_documento)
                                                            ->whereNotIn('id', [$relacion->id])
                                                            ->get();

                if ( $relacionesAux->count() > 0 ) {
                    
                    foreach ( $relacionesAux as $relacionAux ) {
                        $relacionAux->delete();
                        $countRelacionesEliminadas++;
                    }

                    // dd($relacion, $relacionesAux);
                    $idAux = $relacion->id;
                    echo "<br>, salto, id = ". $idAux;
                    goto Inicio;

                }
                
            }

        }
        

        return 'Listo , relaciones eliminadas :'. $countRelacionesEliminadas;
    }

    /**
     * Funcion que dirige a la vista para ingresar documentos
     */
    public function create()
    {
        if (!\Entrust::can(['crear-documento'])) {
            return \Redirect::to('home');
        }

        $tiposDocumento = TipoDocumento::whereNotIn('id',[2,8])->get();
        $modalidadesCompra = ModalidadCompra::all();
        $tiposAdjudicacion = TipoAdjudicacion::all();
        $tiposInforme = TipoInforme::all();
        $usuarios = User::where('responsable',1)->get();
        $referentesTecnicos = ReferenteTecnico::all();
        $tiposArchivo = TipoArchivo::all();
        return view('documento.create')->with('tiposDocumento',$tiposDocumento)
                                     ->with('modalidadesCompra',$modalidadesCompra)
                                     ->with('tiposAdjudicacion',$tiposAdjudicacion)
                                     ->with('tiposInforme',$tiposInforme)
                                     ->with('usuarios',$usuarios)
                                     ->with('referentesTecnicos',$referentesTecnicos)
                                     ->with('tiposArchivo',$tiposArchivo);
    }

    /**
     * Esta funcion guarda el documento ingresado en la vista create.
     */
    public function store(Request $request)
    {
        // sleep(5);
        // dd('creando documento',$request->all());
        //mensajes de los validadores
        $messages = [
            'required'    => 'Debe ingresar el :attribute',
            'numeric'     => 'El :attribute debe solo contener números',
            'max' => 'El :attribute no debe exeder los :max caracteres',
            'min' => 'El :attribute debe tener minimo :min caracteres',
            'date_format'        => 'La :attribute no tiene el formato correcto: día/mes/año',
            'reemplaza.required' => 'Debe seleccionar una factura'
        ];

        if ( $request->input('tipo_documento') == 4 || $request->input('tipo_documento') == 5 || $request->input('tipo_documento') == 10 || $request->input('tipo_documento') == 11 || $request->input('tipo_documento') == 8 ) {
            //validador de los input del formulario
            $validator = \Validator::make($request->all(), [
                'nombre_proveedor'      => 'required|numeric',
                'tipo_documento'        => 'required|numeric',
                'numero_documento'      => 'required',
                'modalidad_compra'      => 'required|numeric',
                'tipo_adjudicacion'     => 'required|numeric',
                'numero_orden_compra'   => 'required',
                'fecha_documento'       => 'required|date_format:"d/m/Y"',
                'tipo_informe'          => 'required|numeric',
                'fecha_recepcion'       => 'required|date_format:"d/m/Y"',
                'reemplaza'             => 'required',
                'periodo'               => 'required|date_format:"m/Y"',
                'valor_total_documento' => 'required'
            ], $messages);

        } else {
            //validador de los input del formulario
            $validator = \Validator::make($request->all(), [
                'nombre_proveedor'      => 'required|numeric',
                'tipo_documento'        => 'required|numeric',
                'numero_documento'      => 'required',
                'modalidad_compra'      => 'required|numeric',
                'tipo_adjudicacion'     => 'required|numeric',
                'numero_orden_compra'   => 'required',
                'fecha_documento'       => 'required|date_format:"d/m/Y"',
                'tipo_informe'          => 'required|numeric',
                'fecha_recepcion'       => 'required|date_format:"d/m/Y"',
                'periodo'               => 'required|date_format:"m/Y"',
                'valor_total_documento' => 'required'
            ], $messages);
        }
        
        
        //Si contiene errores se devuelven todos los errores, de lo contrario guarda en la base de datos
        if ( $validator->fails() ) {
            return response()->json($validator->errors(), 400);
        } else {
            
            $datos = $this->validarNumOrdenCompraAndNumLicitacion($request);
            /**
             * Comprobar si se ha seleccionado contrato y orden de compra
             * comprobrar si el monto del documento es igual o menor a los saldos
             * hacer el resto de validaciones.
             */
            $datosContrato = array();
            $contrato = null;
            $oc = null;
            $totalDocumento = formato_entero($request->input('valor_total_documento'));

            if ( $request->input('orden_compra') != null ) {

                $oc = OrdenCompra::findOrFail( $request->input('orden_compra') );

            }

            if ( $request->input('contrato') != null ) {

                $contrato = Contrato::findOrFail($request->input('contrato'));
            }

            if ( is_object($oc) && is_object($contrato) ) {

                if ( $oc->id_contrato != $contrato->id ) {

                    $datosContrato['mensaje'] = 'La Orden de Compra seleccionada debe pertenecer al Contrato seleccionado.';
                    return response()->json($datosContrato, 400);

                }

            }

            if ( ! is_object($oc) && is_object($contrato) ) {
                // Verificar si el contrato tiene OC disponibles con saldo.
                $ocContrato = OrdenCompra::where('id_contrato', $contrato->id)
                                         ->where('saldo_oc', '>', 0)
                                         ->first();

                if ( is_object($ocContrato) ) {

                    $datosContrato['mensaje'] = 'Debe seleccionar una Orden de Compra del Contrato.';
                    return response()->json($datosContrato, 400);

                }

            }

            if ( is_object($oc) && ! is_object($contrato) ) {
                if ( $oc->id_contrato != null ) {

                    $datosContrato['mensaje'] = 'Debe seleccionar el Contrato de la Orden de Compra seleccionada.';
                    return response()->json($datosContrato, 400);

                }
            }

            /**
             * Validar que el monto del documento no supere los saldos de la OC ni del Contrato.
             */
            // Validar con Contrato
            if ( is_object($contrato) ) {

                if ( $request->input('tipo_documento') == 4 || $request->input('tipo_documento') == 10 ) {
                    // NC y CCC

                    if ( $contrato->saldo_preventivo != null || $contrato->saldo_correctivo != null ) {
                        if ( $request->input('montoOcupar') == 'preventivo' && $totalDocumento + $contrato->saldo_preventivo > $contrato->monto_preventivo ) {

                            $datosContrato['mensaje'] = 'El Valor Total más el Saldo Preventivo no puede superar el Monto Preventivo del Contrato.';
                            return response()->json($datosContrato, 400);
                            
                        }
                        if ( $request->input('montoOcupar') == 'correctivo' && $totalDocumento + $contrato->saldo_correctivo > $contrato->monto_correctivo ) {

                            $datosContrato['mensaje'] = 'El Valor Total más el Saldo Correctivo no puede superar el Monto Correctivo del Contrato.';
                            return response()->json($datosContrato, 400);

                        }
                    }

                    if ( $totalDocumento + $contrato->saldo_contrato > $contrato->monto_contrato ) {

                        $datosContrato['mensaje'] = 'El Valor Total más el Saldo Contrato no puede superar el Monto del Contrato.';
                        return response()->json($datosContrato, 400);

                    }

                } else {

                    if ( $contrato->saldo_preventivo != null || $contrato->saldo_correctivo != null ) {
                        if ( $request->input('montoOcupar') == 'preventivo' && $totalDocumento > $contrato->saldo_preventivo ) {

                            $datosContrato['mensaje'] = 'El Valor Total no puede superar el Saldo Preventivo del Contrato.';
                            return response()->json($datosContrato, 400);

                        }
                        if ( $request->input('montoOcupar') == 'correctivo' && $totalDocumento > $contrato->saldo_correctivo ) {

                            $datosContrato['mensaje'] = 'El Valor Total no puede superar el Saldo Correctivo del Contrato.';
                            return response()->json($datosContrato, 400);

                        }
                    }

                    if ( $totalDocumento > $contrato->saldo_contrato ) {

                        $datosContrato['mensaje'] = 'El Valor Total no puede superar el Saldo Contrato.';
                        return response()->json($datosContrato, 400);

                    }

                }
                
            }

            // Validar OC
            if ( is_object($oc) ) {

                if ( $request->input('tipo_documento') == 4 || $request->input('tipo_documento') == 10 ) {
                    if ( $totalDocumento + $oc->saldo_oc  > $oc->monto_oc ) {
                        
                        $datosContrato['mensaje'] = 'El Valor Total más el Saldo OC no puede superar el Monto OC.';
                        return response()->json($datosContrato, 400);

                    }
                } else {
                    if ( $totalDocumento > $oc->saldo_oc ) {
                        
                        $datosContrato['mensaje'] = 'El Valor Total no puede superar el Saldo OC.';
                        return response()->json($datosContrato, 400);

                    }
                }

            }

            if ( $datos['estado_documento'] == 'success' ) {

                // Se valida que para el proveedor y el tipo de documento, no se repita el numero documento
                // Se agrega a la validacion los documentos eliminados, ya que pueden ser recuperados.
                $documentoAux = Documento::withTrashed()
                                         ->where('id_proveedor', $request->input('nombre_proveedor'))
                                         ->where('id_tipo_documento', $request->input('tipo_documento'))
                                         ->where('numero_documento', $request->input('numero_documento'))
                                         ->first();

                if ( !is_object($documentoAux) ) {
                    $newDocumento = new Documento($request);
                    $mensajeDocumento = 'Ingreso exitoso del documento.';

                    // Se crean los items presupuestarios
                    if ( $request->input('valor_item') ) {

                        foreach ($request->input('valor_item') as $id_item => $valor) {

                            if ( formato_entero($valor) > 0 ) {

                                $newDevengo = new Devengo();
                                $newDevengo->id_documento = $newDocumento->id;
                                $newDevengo->id_item_presupuestario = $id_item;
                                $newDevengo->monto = formato_entero($valor);
                                $newDevengo->save();

                            }

                        }

                    }
                    
                    $archivo = $request->file('archivo');
                    if ( $archivo != null ) {

                        $nombreOriginalArchivo = str_replace('.pdf','',$archivo->getClientOriginalName() );
                        $arreglo_nombre = explode(".", $archivo->getClientOriginalName() );
                        $extension = strtolower( end( $arreglo_nombre ) );

                        if ($extension != 'pdf') {

                            $datos['mensaje_archivo'] = 'Extension incorrecta del archivo';
                            $datos['estado_archivo'] = 'error';

                        } else {

                            // Se trabajan las rutas del archivo, se guarda en el proyecto al final de la funcion
                            $nombre = $nombreOriginalArchivo.'_'.date('Y_m_d_G_i_s');
                            $rutaArchivo = '/documento/'.$newDocumento->id.'/'.$nombre.'.pdf';
                            $pathArchivo = public_path().'/documento/'.$newDocumento->id.'/';

                            $newArchivo = new Archivo();
                            $newArchivo->id_documento = $newDocumento->id;
                            $newArchivo->id_tipo_archivo = $request->input('tipo_archivo');

                            $newArchivo->nombre = $nombre;
                            $newArchivo->nombre_original = $nombreOriginalArchivo;
                            $newArchivo->ubicacion = "documento/".$newDocumento->id."/".$newArchivo->nombre.".pdf";
                            $newArchivo->extension = "pdf";
                            $newArchivo->peso = $archivo->getSize();
                            $newArchivo->save();

                            $datos['mensaje_archivo'] = 'Se ha guardado correctamente el archivo';
                            $datos['estado_archivo'] = 'success';

                            $archivo->move( $pathArchivo , $nombre.'.pdf' ); // Se guarda el archivo

                        }
                        
                    } else {
                        $datos['mensaje_archivo'] = '';
                        $datos['estado_archivo'] = '';
                    }

                    /*  Para conectar documentos si se ha seleccinado uno
                        Se valida si el nuevo documento tiene los archivos necesarios
                    */
                    if ( $request->input('doc_disponible_bodega') != null || $request->input('guias_disponible_bodega') != null ) {
                        if ( $newDocumento->tieneLosArchivosNecesarios() ) {
                            if ( $request->input('doc_disponible_bodega') != null ) {
                                // Un solo documento (No es guia despacho)
                                $wsDocumento = WsDocumento::findOrFail( $request->input('doc_disponible_bodega') );
                                // Se crea la relacion
                                $newRelacion = new RelacionDocumentoWsDocumento();
                                $newRelacion->id_documento = $newDocumento->id;
                                $newRelacion->id_ws_documento = $wsDocumento->id;
                                $newRelacion->id_user_relacion = \Auth::user()->id;
                                $newRelacion->updated_at = NULL;
                                $newRelacion->save();
                                // Se paasa el id correspondiente a cada documento para la conexion
                                $newDocumento->id_relacion = $newRelacion->id;
                                $newDocumento->validar();
                                $newDocumento->updated_at = NULL;
                                $newDocumento->save();
                                $wsDocumento->id_relacion = $newRelacion->id;
                                $wsDocumento->save();

                                
                            }

                            if ( $request->input('guias_disponible_bodega') != null ) {

                                foreach ($request->input('guias_disponible_bodega') as $index => $idGuiaBodega) {

                                    $wsDocumento = WsDocumento::findOrFail( $idGuiaBodega );
                                    // Se crea la relacion
                                    $newRelacion = new RelacionDocumentoWsDocumento();
                                    $newRelacion->id_documento = $newDocumento->id;
                                    $newRelacion->id_ws_documento = $wsDocumento->id;
                                    $newRelacion->id_user_relacion = \Auth::user()->id;
                                    $newRelacion->updated_at = NULL;
                                    $newRelacion->save();
                                    // Se paasa el id correspondiente al documento para la conexion
                                    $newDocumento->id_relacion = $newRelacion->id; // Queda con la ultima relacion
                                    $newDocumento->validar();
                                    $newDocumento->updated_at = NULL;
                                    $newDocumento->save();
                                    $wsDocumento->id_relacion = $newRelacion->id;
                                    $wsDocumento->save();

                                }

                            }

                            $newDocumento->actualizaValidacionDocumentosRelacionados();
                            $mensajeDocumento .= '<br><strong style="colo:black !important;">Se han conectado exitosamente los Documentos.</strong>';
                        } else {
                            $mensajeDocumento .= '<br><strong style="colo:black !important;">No se han conectado los Documentos.';
                            $mensajeDocumento .= '<br>El nuevo Documento no cuenta con el archivo necesario (Factura).</strong>';
                        }
                    }
                    // por si se valida con las recepciones
                    $newDocumento->actualizaValidacionDocumentosRelacionados();

                    /**
                     * Evaluar la relacion del documento con recepcion.
                     * Para pasar a documento con problema o al pendiente de validación
                     */
                    if ( $newDocumento->setProblemaDocumento() == true ) {
                        $mensajeDocumento .= '<br><strong style="colo:black !important;">El documento a quedado con problema, debido a la diferencia en los montos con la recepción.</strong>';
                    }

                    /**
                     * Conectar con contrato, oc y modificar los montos
                     */
                    $newDocumento->unirConContratoOrdenCompra($contrato, $oc, $request);

                    $datos = array(
                        'mensaje_documento' => $mensajeDocumento,
                        'estado_documento'  => 'success',
                    );

                } else {

                    $datos = array(
                        'mensaje_documento' => 'N° Documento repetido para el Proveedor y Tipo Documento seleccionados',
                        'estado_documento' => 'error',
                    );

                    if ( $documentoAux->trashed() ) {
                        $datos['mensaje_documento'] = 'N° Documento repetido para el Proveedor y Tipo Documento seleccionados.<br><br> El Documento se encuentra eliminado.<br> Observaciones : <br>'.$documentoAux->observacion;
                    }
                    
                }
            }
            
        }
    
        return response()->json($datos,200);
    }

    /**
     * Funcion para obtener el proveedor segun el select 
     */
    public function getProveedor($idProveedor)
    {
        if ( $idProveedor != null && $idProveedor != '') {
            $proveedor = Proveedor::findOrFail($idProveedor);
            $proveedor->rut = $proveedor->formatRut();
        } else {
            $proveedor = new Proveedor();
            $proveedor->id = '';
            $proveedor->nombre = '';
            $proveedor->rut = '';
        }
        
        return $proveedor;
    }

    /**
     * Funcion para obtener última OC del proveedor
     */
    public function getOcProveedor($idProveedor)
    {
        if ( $idProveedor != null && $idProveedor != '') {

            $proveedor = Proveedor::findOrFail($idProveedor);
            
            $ultimaOc = \DB::table('ws_contrato')
                       ->join('ws_orden_compra','ws_contrato.id','=','ws_orden_compra.id_contrato')

                       ->where('ws_contrato.rut_proveedor',$proveedor->rut)
                       ->orderby('ws_orden_compra.fecha_orden_compra','desc')
                       ->select(
                           'ws_orden_compra.fecha_orden_compra_mercado_publico',
                           'ws_orden_compra.numero_orden_compra_mercado_publico',
                           'ws_orden_compra.total_orden_compra',
                           'ws_orden_compra.saldo_orden_compra',
                           'ws_contrato.rut_proveedor'
                        )
                       ->first();
            
            if ( $ultimaOc != null ) {

                $datos = array (
                    'estado' => 'success',
                    'mensaje' => 'Última OC encontrada',
                    'ultimaOc' => $ultimaOc->numero_orden_compra_mercado_publico,
                    'fechaOc' => fecha_dmY($ultimaOc->fecha_orden_compra_mercado_publico),
                    'montoMaximoOc' => formatoMiles($ultimaOc->total_orden_compra),
                    'saldoOc' => formatoMiles($ultimaOc->saldo_orden_compra),
                );

            } else {

                $datos = array (
                    'estado' => 'problem',
                    'mensaje' => 'No hay registro de OC para el proveedor',
                );

            }
            
        } else {

            $datos = array (
                'estado' => 'error',
                'mensaje' => 'No es posible buscar la última OC.',
            );
            
        }

        return response()->json($datos,200);
    }

    public function getFacturasRechazadasArchivosAceptaProveedor($idProveedor)
    {
        // Solo facturas con NC por el 100% y que no tengan refactura (getDocumentoFactura)
        $rechazadosProveedor = ArchivoAcepta::with(['getProveedor','getTipoDocumento'])
                                            ->where('cargado',0)
                                            ->where('id_user_rechazo','<>',null)
                                            ->where('id_proveedor', $idProveedor)
                                            ->where('id_tipo_documento', 1)
                                            ->whereHas('getAceptaMotivoRechazo', function ($query){
                                                $query->where('id_motivo_rechazo', 5); // Anula Documento
                                            })
                                            ->where('observacion_rechazo', 'LIKE', '%Tiene una Nota de Crédito por el total del monto%')
                                            ->doesntHave('getDocumentoFactura')
                                            ->get();

        if ( $rechazadosProveedor->count() > 0 ) {

            $opciones = '';

            foreach ($rechazadosProveedor as $key => $rechazado) {
                $opciones .= PHP_EOL.'<option id="docRechazadoProv_'.$rechazado->id.'" value="'.$rechazado->id.'" ';
                $opciones .= ' data-datosdocumento="'.htmlspecialchars(json_encode($rechazado->getDatosDocumentosProveedores()), ENT_QUOTES, 'UTF-8').'" >';
                $opciones .= $rechazado->getTipoDocumento->sigla.' N° '.$rechazado->folio.' - '.fecha_dmY($rechazado->emision).' - $'.formatoMiles($rechazado->monto_total).'</option>'.PHP_EOL;
                $opciones .= '';
            }

            $datos = array(
                'mensaje' => $opciones,
                'estado' => 'success',
            );

        } else {

            $datos = array(
                'mensaje' => 'No se encuentran Documentos Rechazados del Proveedor',
                'estado' => 'error',
            );

        }

        return response()->json($datos,200);
    }

    /**
     * Funcion para obtener los documentos del proveedor seleccionado
     */
    public function getDocumentosProveedor($idProveedor, $idTipoDocumento, $idDocumento = 0)
    {   
        $documentosProveedor = 0;
        $numCarta = '';
        if ( $idTipoDocumento == 1 ) {
            // Solo Facturas con NC al 100%
            // Que no tengan Facturas relacionadas, si la tienen es porque ya se refacturo
            $documentosProveedor = Documento::with(['getDevengos.getItemPresupuestario','getTipoDocumento','getDocumentosRelacionados'])
                                            ->where('id_tipo_documento', 1)
                                            ->where('id_proveedor', $idProveedor)
                                            ->whereHas('getDocumentosRelacionados', function($query) {
                                                $query->where('id_tipo_documento', 4); // NC
                                            })
                                            ->whereDoesntHave('getDocumentosRelacionados', function ($query) {
                                                $query->where('id_tipo_documento', 1); // FC
                                            })
                                            ->where('total_documento_actualizado' , 0); // significa que la factura esta anulada

        } elseif ( $idTipoDocumento == 4 || $idTipoDocumento == 5 || $idTipoDocumento == 10 || $idTipoDocumento == 11 ) {

            $documentosProveedor = Documento::with(['getDevengos.getItemPresupuestario','getTipoDocumento'])
                                            ->where('id_proveedor',$idProveedor)
                                            ->whereIn('id_tipo_documento',[1,2,8]);

            if ( $idTipoDocumento == 10 || $idTipoDocumento == 11 ) {
                $lastCarta = Documento::whereIn('id_tipo_documento', [10,11])->orderBy('created_at','desc')->first();
                $numCarta = $lastCarta->numero_documento + 10000; 
            }

        } elseif ($idTipoDocumento == 8 ) {
            $documentosProveedor = Documento::with(['getDevengos.getItemPresupuestario','getTipoDocumento'])
                                            ->where('id_proveedor',$idProveedor)
                                            ->where('id_tipo_documento',4);
        }

        if ( $idDocumento != 0 ) {
            
            if ($idTipoDocumento != 8) {
                $documentosProveedor = $documentosProveedor->where('id','<>',$idDocumento)->get();
            } else {
                $documentosProveedor = $documentosProveedor->where('id_relacionado',$idDocumento)->get();
            }

        } else {

            $documentosProveedor = $documentosProveedor->get();

        }
        
        if ( count($documentosProveedor) > 0 ) {
            $opciones = '';

            if ( $idTipoDocumento == 1 ||$idTipoDocumento == 4 || $idTipoDocumento == 5 || $idTipoDocumento == 10 || $idTipoDocumento == 11 ) {

                foreach ($documentosProveedor as $key => $documento) {
                    $opciones .= PHP_EOL.'<option id="docProv_'.$documento->id.'" value="'.$documento->id.'" ';
                    $opciones .= ' data-datosdocumento="'.htmlspecialchars(json_encode($documento->getDatosDocumentosProveedores()), ENT_QUOTES, 'UTF-8').'" >';
                    $opciones .= $documento->getTipoDocumento->sigla.' N° '.$documento->numero_documento.' - '.fecha_dmY($documento->fecha_documento).' - $'.formatoMiles($documento->total_documento).'</option>'.PHP_EOL;
                }

            } elseif ( $idTipoDocumento == 8 ) {

                foreach ($documentosProveedor as $key => $documento) {
                    
                    $opciones .= PHP_EOL.'<option id="docProv_'.$documento->id.'" value="'.$documento->id.'">'.$documento->getTipoDocumento->sigla.' N° '.$documento->numero_documento.' - '.fecha_dmY($documento->fecha_documento).' - $'.formatoMiles($documento->total_documento).'</option>'.PHP_EOL;
                }

            }
            
            $datos = array(
                'mensaje'  => $opciones,
                'estado'   => 'success',
                'numCarta' => $numCarta
            );

        } else {

            $datos = array(
                'mensaje' => 'No se encuentran Documentos del Proveedor',
                'estado'  => 'error',
            );

        }

        return response()->json($datos,200);
    }

    /**
     * Funcion para obtener los documentos de la recepcion boddega 
     * del proveedor seleccionado, no deben estar conectadas a algun documento del sistema.
     */
    public function getDocumentosBodegaProveedor($idProveedor, $idTipoDocumento)
    {

        $proveedor = Proveedor::findOrFail($idProveedor);
        $tipoDocumento = TipoDocumento::findOrFail($idTipoDocumento);

        $documentos = WsDocumento::with(['getWsOrdenCompra.getWsContrato'])
                                 ->where('id_relacion', null)
                                 ->where('tipo_documento', 'LIKE', $tipoDocumento->nombre);
        
        // Solo se ingresa si el proveedor no es cenabast
        if ( $idProveedor != 249 ) {
            $documentos = $documentos->whereHas('getWsOrdenCompra.getWsContrato',function ($query) use ($proveedor) {
                                            $query->whereIn('rut_proveedor', $proveedor->getTodosLosRut());
                                       });
        }

        $documentos = $documentos->get();
                      
        if ( count($documentos) > 0 ) {
            
            $opciones = '';
            foreach ($documentos as $key => $documento) {

                $opciones .= PHP_EOL.'<option id="docBodega_'.$documento->id.'" ';
                $opciones .= 'value="'.$documento->id.'" ';
                $opciones .= 'data-datosrecepcion="'.$documento->getDatosDocumentoAndOrdenCompra().'" ';
                $opciones .= '>'.$tipoDocumento->sigla.' N° '.$documento->documento.' '; 
                $opciones .= '- '.fecha_dmY($documento->fecha_carga).' - $'.formatoMiles($documento->documento_total).'';
                $opciones .= ' | Prov. '.$documento->getWsOrdenCompra->getWsContrato->rut_proveedor;
                $opciones .= '</option>'.PHP_EOL;
            }

            $datos = array(
                'mensaje' => $opciones,
                'estado' => 'success',
            );

        } else {

            $datos = array(
                'mensaje' => 'No se encuentran Documentos del Proveedor en Recepcionados de Bodega ',
                'estado' => 'error',
            );

        }

        return response()->json($datos,200);
    }

    /**
     * Funcion para obtener las guias de despacho recepcionadas en bodega
     * del proveedor seleccionado, no deben estar conectadas a algun documento del sistema
     */
    public function getGuiasDespachoBodegaProveedor($idProveedor)
    {
        $proveedor = Proveedor::findOrFail($idProveedor);

        $documentos = WsDocumento::with(['getWsOrdenCompra.getWsContrato'])
                                 ->where('id_relacion', null)
                                 ->where('tipo_documento', 'LIKE', '%Guia de Despacho%');

        // Solo se ingresa si el proveedor no es cenabast
        if ( $idProveedor != 249 ) {
            $documentos = $documentos->whereHas('getWsOrdenCompra.getWsContrato',function ($query) use ($proveedor) {
                                        $query->whereIn('rut_proveedor', $proveedor->getTodosLosRut());
                                      });
        }

        $documentos = $documentos->get();
                      
        if ( count($documentos) > 0 ) {
            
            $opcionesGuias = '';
            foreach ($documentos as $key => $documentoBodega) {
                $opcionesGuias .= PHP_EOL.'<option id="guiaDespacho_'.$documentoBodega->id.'"';
                $opcionesGuias .= ' value="'.$documentoBodega->id.'"';
                $opcionesGuias .= ' data-numdocumento="'.$documentoBodega->documento.'"';
                $opcionesGuias .= ' data-valordocumento="'.$documentoBodega->documento_total.'"';
                $opcionesGuias .= ' data-numoc="'.$documentoBodega->numero_orden_compra_mercado_publico.'"';
                $opcionesGuias .= ' >GD N° '.$documentoBodega->documento.' - '.fecha_dmY($documentoBodega->fecha_carga).'';
                $opcionesGuias .= ' - $'.formatoMiles($documentoBodega->documento_total);
                $opcionesGuias .= ' | Prov. '.$documentoBodega->getWsOrdenCompra->getWsContrato->rut_proveedor;
                $opcionesGuias .= '</option>'.PHP_EOL;
            }

            $datos = array(
                'mensaje' => $opcionesGuias,
                'estado' => 'success',
            );

        } else {

            $datos = array(
                'mensaje' => 'No se encuentran Guias de Despacho del Proveedor en Recepción ',
                'estado' => 'error',
            );

        }

        return response()->json($datos,200);
    }

    /**
     * Funcion para mostrar la modal con los item presupuestarios, en la cual se permite la seleccion.
     * no muestra los item ya seleccionados
     */
    public function getModalAgregarItem($arregloIdsItems, $idDocBodega = null)
    {
        $idsItem = json_decode($arregloIdsItems);
        $itemsPresupuestarios = ItemPresupuestario::whereNotIn('id', $idsItem)->get();
        $docBodega = null;
        if ( $idDocBodega != null && $idDocBodega != 'null' ) {
            
            $docBodega = WsDocumento::with(['getWsItemsPresupuestarios','getWsArchivos'])->findOrFail($idDocBodega);
            if ( count($docBodega->getWsArchivos) > 0 ) {
                
                $docBodega->tieneArchivos = true;
                foreach ( $docBodega->getWsArchivos as $archivo ) {

                    if ( strpos($archivo->nombre_archivo,'ActaRecepcion') !== false ) {
                        $docBodega->ubicacionArchivoRecepcion = $archivo->ubicacion;
                        $docBodega->archivoRecepcionCargado = $archivo->cargado;
                    }

                }

            } else {
                $docBodega->tieneArchivos = false;
            }
            
        }

        return view('documento.modal_agregar_item')->with('itemsPresupuestarios',$itemsPresupuestarios)->with('docBodega',$docBodega);
    }

    /**
     * Funcion que obtiene el item seleccionado y lo envia a la vista
     */
    public function getItem($idItem)
    {
        try {
            $itemParaTabla = ItemPresupuestario::findOrFail($idItem);
            $datos = array(
                'mensaje' => 'Se Encuentra el Item Presupuestario',
                'estado' => 'success',
                'id_item' => $itemParaTabla->id,
                'codigo' => $itemParaTabla->codigo(),
                'clasificacionPresupuestario' => $itemParaTabla->clasificador_presupuestario,
            );

        } catch (\ModelNotFoundException $e) {
            $datos = array(
                'mensaje' => 'No se encuentra el Item Presupuestario',
                'estado' => 'error',
            );
        }

        return response()->json($datos,200);
    }

    /**
     * Funcion que une automaticamente los documentos :
     * Deben ser con modalidad compra: Cenabast = id_modalidad_compra:2
     * Y N° Orden de Compra = Intermediación = documento_compra
     */
    public function getDocumentosConBodegaIntermediacion()
    {
        
        $documentos = \DB::table('documentos')
                    ->join('proveedores','documentos.id_proveedor','=','proveedores.id')
                    ->join('tipos_documento','documentos.id_tipo_documento','=','tipos_documento.id')
                    ->join('ws_contrato','proveedores.rut','=','ws_contrato.rut_proveedor')
                    ->join('ws_orden_compra','ws_contrato.id','=','ws_orden_compra.id_contrato')
                    ->join('ws_documento','ws_orden_compra.id','=','ws_documento.id_orden_compra')

                    ->whereColumn('tipos_documento.nombre','LIKE','ws_documento.tipo_documento')
                    ->whereColumn('documentos.numero_documento','=','ws_documento.documento')
                    ->whereColumn('documentos.total_documento_actualizado','=','ws_documento.documento_total')
                    ->where('documentos.id_relacion',null)
                    ->where('ws_documento.id_relacion',null)

                    ->where('documentos.id_modalidad_compra',2)
                    ->where('documentos.documento_compra','LIKE','INTERMEDIACIóN')
                    //->take(20)
                    ->select(
                        'documentos.id as id_documento',
                        'documentos.numero_documento',
                        'documentos.total_documento_actualizado',
                        'documentos.documento_compra',
                        'proveedores.rut as rut_proveedor',
                        'proveedores.nombre as nombre_proveedor',
                        'tipos_documento.nombre as nombre_tipo_documento',
                        'ws_contrato.nombre_proveedor as ws_nombre_proveedor',
                        'ws_contrato.rut_proveedor as ws_rut_proveedor',
                        'ws_contrato.id as id_ws_contrato',
                        'ws_orden_compra.id as id_ws_orden_compra',
                        'ws_documento.id as id_ws_documento',
                        'ws_documento.tipo_documento as ws_tipo_documento',
                        'ws_documento.documento as ws_numero_documento',
                        'ws_documento.documento_total'
                    )
                    ->get();

        $numDoc = count( $documentos );
        if ( $numDoc > 0 ) {

            // Se crean las relaciones
            foreach ($documentos as $documentoConectar) {
                $documento = Documento::findOrFail( $documentoConectar->id_documento );
                $wsDocumento = WsDocumento::findOrFail( $documentoConectar->id_ws_documento );

                $relacionAux = RelacionDocumentoWsDocumento::where('id_documento', $documentoConectar->id_documento)
                                                           ->where('id_ws_documento', $documentoConectar->id_ws_documento)
                                                           ->first();

                if ( ! is_object($relacionAux) ) {

                    // Se crea la relacion
                    $newRelacion = new RelacionDocumentoWsDocumento();
                    $newRelacion->id_documento = $documento->id;
                    $newRelacion->id_ws_documento = $wsDocumento->id;
                    $newRelacion->id_user_relacion = NULL;
                    $newRelacion->updated_at = NULL;
                    $newRelacion->save();
                    // Se paasa el id correspondiente a cada documento para la conexion
                    $documento->timestamps = false;
                    $documento->id_relacion = $newRelacion->id;
                    $documento->validar();
                    $documento->save();
                    $documento->actualizaValidacionDocumentosRelacionados();
                    $wsDocumento->timestamps = false;
                    $wsDocumento->id_relacion = $newRelacion->id;
                    $wsDocumento->save();
                    
                }
                
            }

        }

        $liReemplaza = '<a href="#">';
        $liReemplaza .= '<i class="fas fa-link fa-lg fa-bounce" style="color: #3cc051;"></i>';
        $liReemplaza .= 'Se han conectado '.$numDoc.' Documentos (Intermediación) con &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Recepción ';
        $liReemplaza .= '</a>';

        $datos = array (
            'estado' => 'success',
            'numDoc' => $numDoc,
            'liReemplaza' => $liReemplaza,
        );

        return response()->json($datos,200);
    }

    /**
     * Obtiene el numero de documentos que esta asociado a una recepcion en bodega y
     * que no ha sido confirmada.
     * Es utilizada para las notificaciones de la cabecera del sistema
     */
    public function getNumDocumentosConRecepcionPorConfirmar()
    {
        $documentos = \DB::table('documentos')
                    ->join('archivos','documentos.id','=','archivos.id_documento')
                    ->join('proveedores','documentos.id_proveedor','=','proveedores.id')
                    ->join('tipos_documento','documentos.id_tipo_documento','=','tipos_documento.id')
                    ->join('ws_contrato','proveedores.rut','=','ws_contrato.rut_proveedor')
                    ->join('ws_orden_compra','ws_contrato.id','=','ws_orden_compra.id_contrato')
                    ->join('ws_documento','ws_orden_compra.id','=','ws_documento.id_orden_compra')

                    ->whereColumn('tipos_documento.nombre','LIKE','ws_documento.tipo_documento')
                    ->whereColumn('documentos.numero_documento','=','ws_documento.documento')
                    ->whereColumn('documentos.total_documento_actualizado','=','ws_documento.documento_total')

                    ->where('archivos.id_tipo_archivo',1) // Factura
                    ->where('documentos.id_relacion',null)
                    ->where('ws_documento.id_relacion',null)
                    //->take(20)
                    ->get();
        $numDoc = count($documentos);

        $liReemplaza = '<a href="'.asset('documentos/listado/pendiente_validacion/').'">';
        // $liReemplaza .= '<span class="label label-sm label-icon label-warning"><i class="fas fa-link fa-bounce"></i></span>';
        $liReemplaza .= '<i class="fas fa-link fa-lg fa-bounce" style="color: #280bd2;"></i>';
        $liReemplaza .= 'Hay '.$numDoc.' Documentos con &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Recepción por confirmar';
        $liReemplaza .= '</a>';

        $datos = array (
            'estado' => 'success',
            'numDoc' => $numDoc,
            'liReemplaza' => $liReemplaza,
        );

        return response()->json($datos,200);
    }

    /**
     * Obtiene el numero de documentos con rechazo de visto bueno (de los referentes tecnicos)
     * Es utilizada para las notificaciones de la cabecera del sistema
     */
    public function getNumDocumentosRechazoVistoBueno()
    {
        $numDoc = Documento::where('id_rechazo_visto_bueno','<>',null)->count();

        $liReemplaza = '<a href="'.asset('documentos/listado/rechazo_visto_bueno/').'">';
        $liReemplaza .= '<i class="fas fa-id-card-alt fa-lg fa-bounce" style="color: #d2d200"></i>';
        $liReemplaza .= 'Hay '.$numDoc.' Documentos con Rechazo &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Visto Bueno';
        $liReemplaza .= '</a>';

        $datos = array (
            'estado' => 'success',
            'numDoc' => $numDoc,
            'liReemplaza' => $liReemplaza,
        );

        return response()->json($datos,200);
    }

    /**
     * Muestra los documentos que tienen conexion con documento recepcionado en bodega.
     * La conexion debe ser confirmada, y se crea un registro en tabla pivote.
     */
    public function geDocumentosConBodega()
    {
        if (!\Entrust::can(['ver-documentos-con-recepcion-por-confirmar','match-documentos-con-recepcion'])) {
            return \Redirect::to('home');
        }

        $documentos = \DB::table('documentos')
                    ->join('archivos','documentos.id','=','archivos.id_documento')
                    ->join('proveedores','documentos.id_proveedor','=','proveedores.id')
                    ->join('tipos_documento','documentos.id_tipo_documento','=','tipos_documento.id')
                    ->join('ws_contrato','proveedores.rut','=','ws_contrato.rut_proveedor')
                    ->join('ws_orden_compra','ws_contrato.id','=','ws_orden_compra.id_contrato')
                    ->join('ws_documento','ws_orden_compra.id','=','ws_documento.id_orden_compra')

                    ->whereColumn('tipos_documento.nombre','LIKE','ws_documento.tipo_documento')
                    ->whereColumn('documentos.numero_documento','=','ws_documento.documento')
                    ->whereColumn('documentos.total_documento_actualizado','=','ws_documento.documento_total')

                    ->where('archivos.id_tipo_archivo',1) // Factura
                    ->where('documentos.id_relacion',null)
                    ->where('ws_documento.id_relacion',null)

                    ->select(
                        'documentos.id as id_documento',
                        'documentos.numero_documento',
                        'documentos.fecha_ingreso',
                        'proveedores.rut as rut_proveedor',
                        'proveedores.nombre as nombre_proveedor',
                        'tipos_documento.nombre as nombre_tipo_documento',
                        'ws_contrato.nombre_proveedor as ws_nombre_proveedor',
                        'ws_contrato.rut_proveedor as ws_rut_proveedor',
                        'ws_contrato.id as id_ws_contrato',
                        'ws_orden_compra.id as id_ws_orden_compra',
                        'ws_documento.id as id_ws_documento',
                        'ws_documento.tipo_documento as ws_tipo_documento',
                        'ws_documento.documento as ws_numero_documento'
                    )
                    //->take(20)
                    ->get();
                    //->toSql();
        
        $tiposDocumento = TipoDocumento::whereNotIn('id',[2,8])->get();
        $modalidadesCompra = ModalidadCompra::all();
        $tiposAdjudicacion = TipoAdjudicacion::all();
        $tiposInforme = TipoInforme::all();
        $usuariosResponsables = User::where('responsable',1)->get();
        $referentesTecnicos = ReferenteTecnico::all();
        $usuariosDigitadores = User::has('getDocumentos')->get();
        $proveedores = Proveedor::all();
        $itemsPresupuestarios = ItemPresupuestario::all();

        //dd($documentos);
        return view('documento.index_listado_por_confirmar')->with('documentos',$documentos)
                                                            ->with('tiposDocumento',$tiposDocumento)
                                                            ->with('modalidadesCompra',$modalidadesCompra)
                                                            ->with('tiposAdjudicacion',$tiposAdjudicacion)
                                                            ->with('tiposInforme',$tiposInforme)
                                                            ->with('usuariosResponsables',$usuariosResponsables)
                                                            ->with('referentesTecnicos',$referentesTecnicos)
                                                            ->with('usuariosDigitadores',$usuariosDigitadores)
                                                            ->with('proveedores',$proveedores)
                                                            ->with('itemsPresupuestarios',$itemsPresupuestarios);
    }

    public function postFiltrarDocumentosConBodega(Request $request)
    {
        $fechaInicio = fecha_Y_m_d($request->input('filtro_fecha_inicio'));
        $fechaTermino = fecha_Y_m_d($request->input('filtro_fecha_termino'));

        $documentos = \DB::table('documentos')
                    ->join('archivos','documentos.id','=','archivos.id_documento')
                    ->join('proveedores','documentos.id_proveedor','=','proveedores.id')
                    ->join('tipos_documento','documentos.id_tipo_documento','=','tipos_documento.id')
                    ->join('ws_contrato','proveedores.rut','=','ws_contrato.rut_proveedor')
                    ->join('ws_orden_compra','ws_contrato.id','=','ws_orden_compra.id_contrato')
                    ->join('ws_documento','ws_orden_compra.id','=','ws_documento.id_orden_compra')

                    ->whereColumn('tipos_documento.nombre','LIKE','ws_documento.tipo_documento')
                    ->whereColumn('documentos.numero_documento','=','ws_documento.documento')
                    ->whereColumn('documentos.total_documento_actualizado','=','ws_documento.documento_total')

                    ->where('archivos.id_tipo_archivo',1) // Factura
                    ->where('documentos.id_relacion',null)
                    ->where('ws_documento.id_relacion',null);


        if ( $request->input('filtro_fecha_inicio') != null ) {
            // $documentos = $documentos->where('documentos.fecha_ingreso','>=',$fechaInicio.' 00:00:00');
            $documentos = $documentos->where('documentos.fecha_recepcion','>=',$fechaInicio);
        }

        if ( $request->input('filtro_fecha_termino') != null ) {
            // $documentos = $documentos->where('documentos.fecha_ingreso','<=',$fechaTermino.' 23:59:59');
            $documentos = $documentos->where('documentos.fecha_recepcion','<=',$fechaTermino);
        }

        if ( $request->input('filtro_proveedor') != null ) {
            $documentos = $documentos->WhereIn('documentos.id_proveedor',$request->input('filtro_proveedor'));
        }

        if ( $request->input('filtro_tipo_documento') != null ) {
            $documentos = $documentos->WhereIn('documentos.id_tipo_documento',$request->input('filtro_tipo_documento'));
        }

        if ( $request->input('filtro_modalidad_compra') != null ) {
            $documentos = $documentos->WhereIn('documentos.id_modalidad_compra',$request->input('filtro_modalidad_compra'));
        }

        if ( $request->input('filtro_tipo_adjudicacion') != null ) {
            $documentos = $documentos->WhereIn('documentos.id_tipo_adjudicacion',$request->input('filtro_tipo_adjudicacion'));
        }

        if ( $request->input('filtro_tipo_informe') != null ) {
            $documentos = $documentos->WhereIn('documentos.id_tipo_informe',$request->input('filtro_tipo_informe'));
        }

        if ( $request->input('filtro_responsable') != null ) {
            $documentos = $documentos->WhereIn('documentos.id_responsable',$request->input('filtro_responsable'));
        }

        if ( $request->input('filtro_referente_tecnico') != null ) {
            $documentos = $documentos->WhereIn('documentos.id_referente_tecnico',$request->input('filtro_referente_tecnico'));
        }

        if ( $request->input('filtro_digitador') != null ) {
            $documentos = $documentos->WhereIn('documentos.id_digitador',$request->input('filtro_digitador'));
        }

        if ( $request->input('filtro_numero_documento') != null ) {
            $documentos = $documentos->where('documentos.numero_documento', trim($request->input('filtro_numero_documento')) );
        }

        if ( $request->input('filtro_numero_documento_compra') != null ) {
            $documentos = $documentos->where('documentos.documento_compra', trim($request->input('filtro_numero_documento_compra')) );
        }

        $documentos = $documentos->select(
                        'documentos.id as id_documento',
                        'documentos.numero_documento',
                        'documentos.fecha_ingreso',
                        'proveedores.rut as rut_proveedor',
                        'proveedores.nombre as nombre_proveedor',
                        'tipos_documento.nombre as nombre_tipo_documento',
                        'ws_contrato.nombre_proveedor as ws_nombre_proveedor',
                        'ws_contrato.rut_proveedor as ws_rut_proveedor',
                        'ws_contrato.id as id_ws_contrato',
                        'ws_orden_compra.id as id_ws_orden_compra',
                        'ws_documento.id as id_ws_documento',
                        'ws_documento.tipo_documento as ws_tipo_documento',
                        'ws_documento.documento as ws_numero_documento'
                    )
                    //->take(20)
                    ->get();
                    //->toSql();

        $tiposDocumento = TipoDocumento::whereNotIn('id',[2,8])->get();
        $modalidadesCompra = ModalidadCompra::all();
        $tiposAdjudicacion = TipoAdjudicacion::all();
        $tiposInforme = TipoInforme::all();
        $usuariosResponsables = User::where('responsable',1)->get();
        $referentesTecnicos = ReferenteTecnico::all();
        $usuariosDigitadores = User::has('getDocumentos')->get();
        $proveedores = Proveedor::all();
        $itemsPresupuestarios = ItemPresupuestario::all();

        //dd($documentos);
        return view('documento.index_listado_por_confirmar')->with('documentos',$documentos)
                                                            ->with('tiposDocumento',$tiposDocumento)
                                                            ->with('modalidadesCompra',$modalidadesCompra)
                                                            ->with('tiposAdjudicacion',$tiposAdjudicacion)
                                                            ->with('tiposInforme',$tiposInforme)
                                                            ->with('usuariosResponsables',$usuariosResponsables)
                                                            ->with('referentesTecnicos',$referentesTecnicos)
                                                            ->with('usuariosDigitadores',$usuariosDigitadores)
                                                            ->with('proveedores',$proveedores)
                                                            ->with('itemsPresupuestarios',$itemsPresupuestarios)
                                                            //filtros
                                                            ->with('filtroFechaInicio',$request->input('filtro_fecha_inicio'))
                                                            ->with('filtroFechaTermino',$request->input('filtro_fecha_termino'))
                                                            ->with('filtroProveedor',$request->input('filtro_proveedor'))
                                                            ->with('filtroTipoDocumento',$request->input('filtro_tipo_documento'))
                                                            ->with('filtroModalidadCompra',$request->input('filtro_modalidad_compra'))
                                                            ->with('filtroTipoAdjudicacion',$request->input('filtro_tipo_adjudicacion'))
                                                            ->with('filtroTipoInforme',$request->input('filtro_tipo_informe'))
                                                            ->with('filtroResponsable',$request->input('filtro_responsable'))
                                                            ->with('filtroReferenteTecnico',$request->input('filtro_referente_tecnico'))
                                                            ->with('filtroDigitador',$request->input('filtro_digitador'))
                                                            ->with('filtroItems',$request->input('filtro_item_presupuestario'))
                                                            ->with('filtroNumeroDocumento',$request->input('filtro_numero_documento'))
                                                            ->with('filtroNumeroDocumentoCompra',$request->input('filtro_numero_documento_compra'));
    }

    /**
     * Muestra los documentos que no tienen una conexion con los documentos
     * recepcionados en bodega, "sin recepcion"
     */
    public function getDocumentosSinBodega()
    {
        if (!\Entrust::can(['ver-documentos-sin-recepcion'])) {
            return \Redirect::to('home');
        }
        $documentosConBodega = \DB::table('documentos')
                    ->join('proveedores','documentos.id_proveedor','=','proveedores.id')
                    ->join('tipos_documento','documentos.id_tipo_documento','=','tipos_documento.id')
                    ->join('ws_contrato','proveedores.rut','=','ws_contrato.rut_proveedor')
                    ->join('ws_orden_compra','ws_contrato.id','=','ws_orden_compra.id_contrato')
                    ->join('ws_documento','ws_orden_compra.id','=','ws_documento.id_orden_compra')

                    ->whereColumn('tipos_documento.nombre','LIKE','ws_documento.tipo_documento')
                    ->whereColumn('documentos.numero_documento','=','ws_documento.documento')
                    ->whereColumn('documentos.total_documento_actualizado','=','ws_documento.documento_total')
                    ->where('documentos.id_relacion',null)
                    ->where('ws_documento.id_relacion',null)

                    ->select(
                        'documentos.id as id'
                    )
                    //->take(20)
                    ->get();
                    //->toSql();

        $arregloDocumentosId = $documentosConBodega->pluck('id')->toArray();;

        // $arregloDocumentosId = array();
        // foreach($documentosConBodega as $documento) {
        //     $arregloDocumentosId[] = $documento->id;
        // }

        $documentosSinBodega = Documento::with(['getProveedor','getTipoDocumento','getDevengos.getItemPresupuestario'])
                                        // ->where('fecha_ingreso','>=',date('Y-m').'-01 00:00:00')
                                        ->where('fecha_recepcion','>=',date('Y-m').'-01')
                                        ->where('id_relacion', null)
                                        ->where('id_tipo_documento',1)
                                        ->whereNotIn('id',$arregloDocumentosId)->get();
                                        
        $tiposDocumento = TipoDocumento::whereNotIn('id',[2,8])->get();
        $modalidadesCompra = ModalidadCompra::all();
        $tiposAdjudicacion = TipoAdjudicacion::all();
        $tiposInforme = TipoInforme::all();
        $usuariosResponsables = User::where('responsable',1)->get();
        $referentesTecnicos = ReferenteTecnico::all();
        $usuariosDigitadores = User::has('getDocumentos')->get();
        $proveedores = Proveedor::all();
        $itemsPresupuestarios = ItemPresupuestario::all();

        return view('documento.index_listado_sin_bodega')->with('documentos',$documentosSinBodega)
                                                         ->with('tiposDocumento',$tiposDocumento)
                                                         ->with('modalidadesCompra',$modalidadesCompra)
                                                         ->with('tiposAdjudicacion',$tiposAdjudicacion)
                                                         ->with('tiposInforme',$tiposInforme)
                                                         ->with('usuariosResponsables',$usuariosResponsables)
                                                         ->with('referentesTecnicos',$referentesTecnicos)
                                                         ->with('usuariosDigitadores',$usuariosDigitadores)
                                                         ->with('proveedores',$proveedores)
                                                         ->with('itemsPresupuestarios',$itemsPresupuestarios);

    }

    public function postFiltrarDocumentosSinBodega(Request $request)
    {
        set_time_limit(0);
        ini_set('max_execution_time', 0);
        
        $documentosConBodega = \DB::table('documentos')
                    ->join('proveedores','documentos.id_proveedor','=','proveedores.id')
                    ->join('tipos_documento','documentos.id_tipo_documento','=','tipos_documento.id')
                    ->join('ws_contrato','proveedores.rut','=','ws_contrato.rut_proveedor')
                    ->join('ws_orden_compra','ws_contrato.id','=','ws_orden_compra.id_contrato')
                    ->join('ws_documento','ws_orden_compra.id','=','ws_documento.id_orden_compra')

                    ->whereColumn('tipos_documento.nombre','LIKE','ws_documento.tipo_documento')
                    ->whereColumn('documentos.numero_documento','=','ws_documento.documento')
                    ->whereColumn('documentos.total_documento_actualizado','=','ws_documento.documento_total')
                    ->where('documentos.id_relacion',null)
                    ->where('ws_documento.id_relacion',null)

                    ->select(
                        'documentos.id as id'
                    )
                    //->take(20)
                    ->get();
                    //->toSql();

        $arregloDocumentosId = array();
        foreach($documentosConBodega as $documento) {
            $arregloDocumentosId[] = $documento->id;
        }

        $documentos = Documento::with(['getProveedor','getTipoDocumento','getDevengos.getItemPresupuestario'])
                               ->where('id_relacion', null)
                               ->whereNotIn('id',$arregloDocumentosId);

        $fechaInicio = fecha_Y_m_d($request->input('filtro_fecha_inicio'));
        $fechaTermino = fecha_Y_m_d($request->input('filtro_fecha_termino'));

        if ( $request->input('filtro_fecha_inicio') != null ) {
            // $documentos = $documentos->where('fecha_ingreso','>=',$fechaInicio.' 00:00:00');
            $documentos = $documentos->where('fecha_recepcion','>=',$fechaInicio);
        } else {
            // $documentos = $documentos->where('fecha_ingreso','>=','01/06/2019 00:00:00');
            $documentos = $documentos->where('fecha_recepcion','>=', '2019-06-01');
        }

        if ( $request->input('filtro_fecha_termino') != null ) {
            // $documentos = $documentos->where('fecha_ingreso','<=',$fechaTermino.' 23:59:59');
            $documentos = $documentos->where('fecha_recepcion','<=',$fechaTermino);
        }

        if ( $request->input('filtro_proveedor') != null ) {
            $documentos = $documentos->WhereIn('id_proveedor',$request->input('filtro_proveedor'));
        }

        if ( $request->input('filtro_tipo_documento') != null ) {
            $documentos = $documentos->WhereIn('id_tipo_documento',$request->input('filtro_tipo_documento'));
        } else {
            $documentos = $documentos->where('id_tipo_documento',1);
        }

        if ( $request->input('filtro_modalidad_compra') != null ) {
            $documentos = $documentos->WhereIn('id_modalidad_compra',$request->input('filtro_modalidad_compra'));
        }

        if ( $request->input('filtro_tipo_adjudicacion') != null ) {
            $documentos = $documentos->WhereIn('id_tipo_adjudicacion',$request->input('filtro_tipo_adjudicacion'));
        }

        if ( $request->input('filtro_tipo_informe') != null ) {
            $documentos = $documentos->WhereIn('id_tipo_informe',$request->input('filtro_tipo_informe'));
        }

        if ( $request->input('filtro_responsable') != null ) {
            $documentos = $documentos->WhereIn('id_responsable',$request->input('filtro_responsable'));
        }

        if ( $request->input('filtro_referente_tecnico') != null ) {
            $documentos = $documentos->WhereIn('id_referente_tecnico',$request->input('filtro_referente_tecnico'));
        }

        if ( $request->input('filtro_digitador') != null ) {
            $documentos = $documentos->WhereIn('id_digitador',$request->input('filtro_digitador'));
        }

        if ( $request->input('filtro_item_presupuestario') != null) {
            $documentos = $documentos->whereHas('getDevengos',function ($query) use ($request) {
                $query->WhereIn('id_item_presupuestario',$request->input('filtro_item_presupuestario'));
            });
        }

        if ( $request->input('filtro_numero_documento') != null ) {
            $documentos = $documentos->where('numero_documento', trim($request->input('filtro_numero_documento')) );
        }

        if ( $request->input('filtro_numero_documento_compra') != null ) {
            $documentos = $documentos->where('documento_compra', trim($request->input('filtro_numero_documento_compra')) );
        }

        $documentos = $documentos->get();

        $tiposDocumento = TipoDocumento::whereNotIn('id',[2,8])->get();
        $modalidadesCompra = ModalidadCompra::all();
        $tiposAdjudicacion = TipoAdjudicacion::all();
        $tiposInforme = TipoInforme::all();
        $usuariosResponsables = User::where('responsable',1)->get();
        $referentesTecnicos = ReferenteTecnico::all();
        $usuariosDigitadores = User::has('getDocumentos')->get();
        $proveedores = Proveedor::all();
        $itemsPresupuestarios = ItemPresupuestario::all();

        return view('documento.index_listado_sin_bodega')->with('documentos',$documentos)
                                                         ->with('tiposDocumento',$tiposDocumento)
                                                         ->with('modalidadesCompra',$modalidadesCompra)
                                                         ->with('tiposAdjudicacion',$tiposAdjudicacion)
                                                         ->with('tiposInforme',$tiposInforme)
                                                         ->with('usuariosResponsables',$usuariosResponsables)
                                                         ->with('referentesTecnicos',$referentesTecnicos)
                                                         ->with('usuariosDigitadores',$usuariosDigitadores)
                                                         ->with('proveedores',$proveedores)
                                                         ->with('itemsPresupuestarios',$itemsPresupuestarios)
                                                         //filtros
                                                         ->with('filtroFechaInicio',$request->input('filtro_fecha_inicio'))
                                                         ->with('filtroFechaTermino',$request->input('filtro_fecha_termino'))
                                                         ->with('filtroProveedor',$request->input('filtro_proveedor'))
                                                         ->with('filtroTipoDocumento',$request->input('filtro_tipo_documento'))
                                                         ->with('filtroModalidadCompra',$request->input('filtro_modalidad_compra'))
                                                         ->with('filtroTipoAdjudicacion',$request->input('filtro_tipo_adjudicacion'))
                                                         ->with('filtroTipoInforme',$request->input('filtro_tipo_informe'))
                                                         ->with('filtroResponsable',$request->input('filtro_responsable'))
                                                         ->with('filtroReferenteTecnico',$request->input('filtro_referente_tecnico'))
                                                         ->with('filtroDigitador',$request->input('filtro_digitador'))
                                                         ->with('filtroItems',$request->input('filtro_item_presupuestario'))
                                                         ->with('filtroNumeroDocumento',$request->input('filtro_numero_documento'))
                                                         ->with('filtroNumeroDocumentoCompra',$request->input('filtro_numero_documento_compra'));

    }

    /**
     * Excel de documentos sin match con las recepciones
     */
    public function postSinBodegaExcel(Request $request)
    {
        set_time_limit ( 0 );
        ini_set('max_execution_time', 0);

        \Excel::create('SIFCON | Documentos Sin Recepción '.date('d-m-Y').'', function($excel) use($request) {

            $documentosConBodega = \DB::table('documentos')
                                      ->join('proveedores','documentos.id_proveedor','=','proveedores.id')
                                      ->join('tipos_documento','documentos.id_tipo_documento','=','tipos_documento.id')
                                      ->join('ws_contrato','proveedores.rut','=','ws_contrato.rut_proveedor')
                                      ->join('ws_orden_compra','ws_contrato.id','=','ws_orden_compra.id_contrato')
                                      ->join('ws_documento','ws_orden_compra.id','=','ws_documento.id_orden_compra')

                                      ->whereColumn('tipos_documento.nombre','LIKE','ws_documento.tipo_documento')
                                      ->whereColumn('documentos.numero_documento','=','ws_documento.documento')
                                      ->whereColumn('documentos.total_documento_actualizado','=','ws_documento.documento_total')
                                      ->where('documentos.id_relacion',null)
                                      ->where('ws_documento.id_relacion',null)

                                      ->select(
                                        'documentos.id as id'
                                      )
                                      ->get();

            $arregloDocumentosId = $documentosConBodega->pluck('id')->toArray();

            $documentos = Documento::with(['getProveedor','getTipoDocumento',
                                           'getDevengos.getItemPresupuestario',
                                           'getDevengos.getFolioSigfe',
                                           'getRelacionRecepcionesNoValidadas'
                                           ])
                                   ->where('id_relacion', null)
                                   ->whereNotIn('id', $arregloDocumentosId);

            $fechaInicio = fecha_Y_m_d($request->input('filtro_fecha_inicio'));
            $fechaTermino = fecha_Y_m_d($request->input('filtro_fecha_termino'));

            if ( $request->input('filtro_numero_documento') == null  ) {

                if ( $request->input('filtro_fecha_inicio') != null ) {

                    // if ( $request->input('filtro_fecha_a_utilizar') == 'Ingreso' ) {
                        // $documentos = $documentos->where('fecha_ingreso','>=', $fechaInicio.' 00:00:00');
                    // } elseif ( $request->input('filtro_fecha_a_utilizar') == 'Recepción' ) {
                        $documentos = $documentos->where('fecha_recepcion','>=', $fechaInicio);
                    // }

                } else {
                    $documentos = $documentos->where('fecha_recepcion','>=', '2019-06-01');
                }

                if ( $request->input('filtro_fecha_termino') != null ) {
                    
                    // if ( $request->input('filtro_fecha_a_utilizar') == 'Ingreso' ) {
                        // $documentos = $documentos->where('fecha_ingreso','<=',$fechaTermino.' 23:59:59');
                    // } elseif ( $request->input('filtro_fecha_a_utilizar') == 'Recepción' ) {
                        $documentos = $documentos->where('fecha_recepcion','<=', $fechaTermino);
                    // }

                }

            } else {
                $documentos = $documentos->where('numero_documento', trim($request->input('filtro_numero_documento')) );
            }
            

            if ( $request->input('filtro_proveedor') != null ) {
                $documentos = $documentos->WhereIn('id_proveedor',$request->input('filtro_proveedor'));
            }

            if ( $request->input('filtro_tipo_documento') != null ) {
                $documentos = $documentos->WhereIn('id_tipo_documento',$request->input('filtro_tipo_documento'));
            }

            if ( $request->input('filtro_modalidad_compra') != null ) {
                $documentos = $documentos->WhereIn('id_modalidad_compra',$request->input('filtro_modalidad_compra'));
            }

            if ( $request->input('filtro_tipo_adjudicacion') != null ) {
                $documentos = $documentos->WhereIn('id_tipo_adjudicacion',$request->input('filtro_tipo_adjudicacion'));
            }

            if ( $request->input('filtro_tipo_informe') != null ) {
                $documentos = $documentos->WhereIn('id_tipo_informe',$request->input('filtro_tipo_informe'));
            }

            if ( $request->input('filtro_responsable') != null ) {
                $documentos = $documentos->WhereIn('id_responsable',$request->input('filtro_responsable'));
            }

            if ( $request->input('filtro_referente_tecnico') != null ) {
                $documentos = $documentos->WhereIn('id_referente_tecnico',$request->input('filtro_referente_tecnico'));
            }

            if ( $request->input('filtro_digitador') != null ) {
                $documentos = $documentos->WhereIn('id_digitador',$request->input('filtro_digitador'));
            }

            if ( $request->input('filtro_item_presupuestario') != null) {
                $documentos = $documentos->whereHas('getDevengos',function ($query) use ($request) {
                    $query->WhereIn('id_item_presupuestario',$request->input('filtro_item_presupuestario'));
                });
            }


            if ( $request->input('filtro_numero_documento_compra') != null ) {
                $documentos = $documentos->where('documento_compra', trim($request->input('filtro_numero_documento_compra')) );
            }

            $documentos = $documentos->get();

            $excel->sheet('Documentos', function($sheet) use($documentos) {
            
                $sheet->row(1, [
                    'Estado', 
                    // 'Responsable', 
                    'Factoring',
                    'Chile-Compra',
                    'M/Dev',
                    'Proveedor',
                    'RUT',
                    'Tipo Dcto.',
                    'N° Dcto.',
                    'F/Doc.',
                    'M/Doc.',
                    'Fecha RD',
                    'Mes RD',
                    'Item',
                    'ID SIGFE',
                    'Fecha ID',
                    'Devengado',
                    'Mecanismo de Adjudicación',
                    'Observación',
                    'Detalle',
                    'URL Factura',
                    'URL Recepción'
                ]);

                $totalDevengadoExcel = 0;
                $cuentaLineas = 1;
                foreach ( $documentos as $indexAux => $documento ) {
                    $items = '';
                    $totalDevengadoPorDocumento = 0;

                    foreach ( $documento->getDevengos as $index => $devengo) {
                        $cuentaLineas++;
                        $items = $devengo->getItemPresupuestario->codigo();
                        $totalDevengadoPorDocumento = $devengo->monto;
                        // para columna "devengado"
                        $devengado = '';
                        if ($documento->id_tipo_documento == 4 || $documento->id_tipo_documento == 10) {
                            $devengado .= '-'.$totalDevengadoPorDocumento;
                            $totalDevengadoExcel = $totalDevengadoExcel - $totalDevengadoPorDocumento;
                        } else {
                            $devengado .= $totalDevengadoPorDocumento;
                            $totalDevengadoExcel = $totalDevengadoExcel + $totalDevengadoPorDocumento;
                        }

                        // para columna factoring
                        $factoring = '';
                        if ($documento->getProveedorFactoring) {
                            $factoring .= $documento->getProveedorFactoring->nombre.' '.$documento->getProveedorFactoring->rut;
                        }

                        $mesSigfe = '';
                        $fechaSigfe = '';
                        $idSigfe = '';
                        $detalleSigfe = '';
                        if ($devengo->id_folio_sigfe != null) {
                            $mesSigfe = (int) explode("-",$devengo->getFolioSigfe->fecha_sigfe)[1];
                            $fechaSigfe = fecha_dmY($devengo->getFolioSigfe->fecha_sigfe);
                            $idSigfe = $devengo->getFolioSigfe->id_sigfe;
                            $detalleSigfe = $devengo->getFolioSigfe->detalle_sigfe;
                        }

                        $urlRecepcion = '';
                        if ( $documento->getUnaRelacionRecepcionValidada ) {
                            foreach ( $documento->getRelacionRecepcionesValidadas as $relacion ) {
                                if ( $relacion->getWsDocumento ) {
                                    if ( $relacion->getWsDocumento->getWsArchivos ) {
                                        foreach ($relacion->getWsDocumento->getWsArchivos as $archivo) {
                                            $urlRecepcion .= 'http://10.4.237.28/finanzas/'.$archivo->ubicacion.PHP_EOL;
                                        }
                                    }
                                }
                            }
                            
                        }

                        $sheet->row($cuentaLineas, [
                            $documento->getEstadoDocumento('excel'),
                            // $documento->getResponsable->name, 
                            $factoring,
                            $documento->documento_compra,
                            $mesSigfe, // mes de la fecha sigfe
                            $documento->getProveedor->nombre,
                            $documento->getProveedor->rut,
                            $documento->getTipoDocumento->nombre,
                            $documento->numero_documento,
                            fecha_dmY($documento->fecha_documento),
                            (int) explode("-",$documento->fecha_documento)[1], // mes de la fecha del documento
                            fecha_dmY($documento->fecha_recepcion),
                            (int) explode("-",$documento->fecha_recepcion)[1], // mes de la fecha de recepcion del documento
                            $items,
                            $idSigfe,
                            $fechaSigfe,
                            $devengado,
                            $documento->getTipoAdjudicacion->nombre,
                            $documento->observacion,
                            $detalleSigfe,
                            $documento->getUrlFactura(),
                            $urlRecepcion
                        ]);
                    }
                }


                $cuentaLineas++;
                $sheet->row( $cuentaLineas , [
                    '', 
                    '', 
                    '',
                    '',
                    '', 
                    '',
                    '',
                    '',
                    '',
                    '',
                    '', 
                    '',
                    '',
                    '',
                    '',
                    'TOTAL',
                    $totalDevengadoExcel,
                    '',
                    '',
                    '',
                    '',
                    ''
                ]);

        });

        })->export('xlsx');
    }

    /**
     * Muestra los documentos que tienen una conexion confirmada con los documentos
     * recepcionados en bodega.
     */
    public function getDocumentosConBodegaListos()
    {
        if (!\Entrust::can(['ver-documentos-con-recepcion-confirmada','desvincular-documentos-con-recepcion'])) {
            return \Redirect::to('home');
        }
        $documentos = Documento::with(['getProveedor','getTipoDocumento','getRelacionRecepcionesValidadas.getWsDocumento'])
                               ->where('id_relacion','<>',null)
                               ->where('fecha_recepcion','>=',date('Y-m').'-01')
                               ->get();

        $tiposDocumento = TipoDocumento::whereNotIn('id',[2,8])->get();
        $modalidadesCompra = ModalidadCompra::all();
        $tiposAdjudicacion = TipoAdjudicacion::all();
        $tiposInforme = TipoInforme::all();
        $usuariosResponsables = User::where('responsable',1)->get();
        $referentesTecnicos = ReferenteTecnico::all();
        $usuariosDigitadores = User::has('getDocumentos')->get();
        $proveedores = Proveedor::all();
        $itemsPresupuestarios = ItemPresupuestario::all();

        return view('documento.index_listado_bodega_confirmados')->with('documentos',$documentos)
                                                                 ->with('tiposDocumento',$tiposDocumento)
                                                                 ->with('modalidadesCompra',$modalidadesCompra)
                                                                 ->with('tiposAdjudicacion',$tiposAdjudicacion)
                                                                 ->with('tiposInforme',$tiposInforme)
                                                                 ->with('usuariosResponsables',$usuariosResponsables)
                                                                 ->with('referentesTecnicos',$referentesTecnicos)
                                                                 ->with('usuariosDigitadores',$usuariosDigitadores)
                                                                 ->with('proveedores',$proveedores)
                                                                 ->with('itemsPresupuestarios',$itemsPresupuestarios);
    }

    public function postFiltrarDocumentosConBodegaListos(Request $request)
    {
        $fechaInicio = fecha_Y_m_d($request->input('filtro_fecha_inicio'));
        $fechaTermino = fecha_Y_m_d($request->input('filtro_fecha_termino'));

        $documentos = Documento::with(['getProveedor','getTipoDocumento','getRelacionRecepcionesValidadas.getWsDocumento'])
                              ->where('id_relacion','<>',null);
        
        if ( $request->input('filtro_fecha_inicio') != null ) {
            // $documentos = $documentos->where('fecha_ingreso','>=',$fechaInicio.' 00:00:00');
            $documentos = $documentos->where('fecha_recepcion','>=',$fechaInicio);
        }

        if ( $request->input('filtro_fecha_termino') != null ) {
            // $documentos = $documentos->where('fecha_ingreso','<=',$fechaTermino.' 23:59:59');
            $documentos = $documentos->where('fecha_recepcion','<=',$fechaTermino);
        }

        if ( $request->input('filtro_proveedor') != null ) {
            $documentos = $documentos->WhereIn('id_proveedor',$request->input('filtro_proveedor'));
        }

        if ( $request->input('filtro_tipo_documento') != null ) {
            $documentos = $documentos->WhereIn('id_tipo_documento',$request->input('filtro_tipo_documento'));
        }

        if ( $request->input('filtro_modalidad_compra') != null ) {
            $documentos = $documentos->WhereIn('id_modalidad_compra',$request->input('filtro_modalidad_compra'));
        }

        if ( $request->input('filtro_tipo_adjudicacion') != null ) {
            $documentos = $documentos->WhereIn('id_tipo_adjudicacion',$request->input('filtro_tipo_adjudicacion'));
        }

        if ( $request->input('filtro_tipo_informe') != null ) {
            $documentos = $documentos->WhereIn('id_tipo_informe',$request->input('filtro_tipo_informe'));
        }

        if ( $request->input('filtro_responsable') != null ) {
            $documentos = $documentos->WhereIn('id_responsable',$request->input('filtro_responsable'));
        }

        if ( $request->input('filtro_referente_tecnico') != null ) {
            $documentos = $documentos->WhereIn('id_referente_tecnico',$request->input('filtro_referente_tecnico'));
        }

        if ( $request->input('filtro_digitador') != null ) {
            $documentos = $documentos->WhereIn('id_digitador',$request->input('filtro_digitador'));
        }

        if ( $request->input('filtro_item_presupuestario') != null) {
            $documentos = $documentos->whereHas('getDevengos',function ($query) use ($request) {
                $query->WhereIn('id_item_presupuestario',$request->input('filtro_item_presupuestario'));
            });
        }

        if ( $request->input('filtro_numero_documento') != null ) {
            $documentos = $documentos->where('numero_documento', trim($request->input('filtro_numero_documento')) );
        }

        if ( $request->input('filtro_numero_documento_compra') != null ) {
            $documentos = $documentos->where('documento_compra', trim($request->input('filtro_numero_documento_compra')) );
        }

        $documentos = $documentos->get();

        $tiposDocumento = TipoDocumento::whereNotIn('id',[2,8])->get();
        $modalidadesCompra = ModalidadCompra::all();
        $tiposAdjudicacion = TipoAdjudicacion::all();
        $tiposInforme = TipoInforme::all();
        $usuariosResponsables = User::where('responsable',1)->get();
        $referentesTecnicos = ReferenteTecnico::all();
        $usuariosDigitadores = User::has('getDocumentos')->get();
        $proveedores = Proveedor::all();
        $itemsPresupuestarios = ItemPresupuestario::all();

        return view('documento.index_listado_bodega_confirmados')->with('documentos',$documentos)
                                                                 ->with('tiposDocumento',$tiposDocumento)
                                                                 ->with('modalidadesCompra',$modalidadesCompra)
                                                                 ->with('tiposAdjudicacion',$tiposAdjudicacion)
                                                                 ->with('tiposInforme',$tiposInforme)
                                                                 ->with('usuariosResponsables',$usuariosResponsables)
                                                                 ->with('referentesTecnicos',$referentesTecnicos)
                                                                 ->with('usuariosDigitadores',$usuariosDigitadores)
                                                                 ->with('proveedores',$proveedores)
                                                                 ->with('itemsPresupuestarios',$itemsPresupuestarios)
                                                                 //filtros
                                                                ->with('filtroFechaInicio',$request->input('filtro_fecha_inicio'))
                                                                ->with('filtroFechaTermino',$request->input('filtro_fecha_termino'))
                                                                ->with('filtroProveedor',$request->input('filtro_proveedor'))
                                                                ->with('filtroTipoDocumento',$request->input('filtro_tipo_documento'))
                                                                ->with('filtroModalidadCompra',$request->input('filtro_modalidad_compra'))
                                                                ->with('filtroTipoAdjudicacion',$request->input('filtro_tipo_adjudicacion'))
                                                                ->with('filtroTipoInforme',$request->input('filtro_tipo_informe'))
                                                                ->with('filtroResponsable',$request->input('filtro_responsable'))
                                                                ->with('filtroReferenteTecnico',$request->input('filtro_referente_tecnico'))
                                                                ->with('filtroDigitador',$request->input('filtro_digitador'))
                                                                ->with('filtroItems',$request->input('filtro_item_presupuestario'))
                                                                ->with('filtroNumeroDocumento',$request->input('filtro_numero_documento'))
                                                                ->with('filtroNumeroDocumentoCompra',$request->input('filtro_numero_documento_compra'));
    }


    /**
     * Datatable Index
     */
    public function dataTableIndex(Request $request)
    {
        // dd($request->all());

        set_time_limit ( 0 );
        ini_set('max_execution_time', 0);

        $search = $request->get('search')['value'];
        $order_column = $request->get('order')[0]['column'];
        $order_dir = $request->get('order')[0]['dir'];
        
        // Por si es necesario arreglar el search
        // if ( $search == '' ) {
        //     $documentos = Documento::with(['getProveedor','getTipoDocumento','getDevengos.getItemPresupuestario',
        //                                'audits.user'])
        //                            ->where('fecha_ingreso','>=',date('Y-m').'-9 00:00:00');
        // } else {

        // }

        // $documentos = $documentos->get();

        // Para filtrar los datos enviados a la vista
        $dataTable = $request->get('dataTable');
        
        $formJSON = json_decode($request->get('form'));
        $form = construyeFormulario($formJSON);
        
        $fechaInicio = fecha_Y_m_d($form->filtro_fecha_inicio);
        $fechaTermino = fecha_Y_m_d($form->filtro_fecha_termino);

        $documentos = Documento::with([
                                        'getProveedor' => function ($query) {
                                            $query->select('id', 'nombre', 'rut');
                                        },
                                        'getTipoDocumento' => function ($query) {
                                            $query->select('id', 'nombre');
                                        }
                                        ,'getDevengos.getItemPresupuestario',
                                        'getVistoBueno','getUnaRelacionRecepcionValidada',
                                        'getRelacionRecepcionesNoValidadas'
                                    ]);
                            
        if ( $form->filtro_numero_documento == null ) {
            // Se filtra por las fechas

            if ( $form->filtro_fecha_inicio != null ) {

                if ( $form->filtro_fecha_a_utilizar == 'Ingreso' ) {
                    $documentos = $documentos->where('fecha_ingreso','>=',$fechaInicio.' 00:00:00');
                } elseif ( $form->filtro_fecha_a_utilizar == 'Recepción' ) {
                    $documentos = $documentos->where('fecha_recepcion','>=',$fechaInicio);
                }

            }

            if ( $form->filtro_fecha_termino != null ) {
                if ( $form->filtro_fecha_a_utilizar == 'Ingreso' ) {
                    $documentos = $documentos->where('fecha_ingreso','<=',$fechaTermino.' 23:59:59');
                } elseif ( $form->filtro_fecha_a_utilizar == 'Recepción' ) {
                    $documentos = $documentos->where('fecha_recepcion','<=',$fechaTermino);
                }
            }

        } else {
            // no se consideran las fechas, y se busca el numero del documento
            $documentos = $documentos->where('numero_documento', trim($form->filtro_numero_documento) );
        }
        
        if ( isset($form->filtro_proveedor) ) {
            $documentos = $documentos->WhereIn('id_proveedor',$form->filtro_proveedor);
        }

        if ( isset($form->filtro_tipo_documento) ) {
            $documentos = $documentos->WhereIn('id_tipo_documento',$form->filtro_tipo_documento);
        }

        if ( isset($form->filtro_modalidad_compra) ) {
            $documentos = $documentos->WhereIn('id_modalidad_compra',$form->filtro_modalidad_compra);
        }

        if ( isset($form->filtro_tipo_adjudicacion) ) {
            $documentos = $documentos->WhereIn('id_tipo_adjudicacion',$form->filtro_tipo_adjudicacion);
        }

        if ( isset($form->filtro_tipo_informe) ) {
            $documentos = $documentos->WhereIn('id_tipo_informe',$form->filtro_tipo_informe);
        }

        if ( isset($form->filtro_responsable) ) {
            $documentos = $documentos->WhereIn('id_responsable',$form->filtro_responsable);
        }

        if ( isset($form->filtro_referente_tecnico) ) {
            $documentos = $documentos->WhereIn('id_referente_tecnico',$form->filtro_referente_tecnico);
        }

        if ( isset($form->filtro_digitador) ) {
            $documentos = $documentos->WhereIn('id_digitador',$form->filtro_digitador);
        }

        if ( isset($form->filtro_item_presupuestario) ) {
            $documentos = $documentos->whereHas('getDevengos',function ($query) use ($form) {
                $query->WhereIn('id_item_presupuestario',$form->filtro_item_presupuestario);
            });
        }

        if ( $form->filtro_numero_documento_compra != null ) {
            $documentos = $documentos->where('documento_compra', trim($form->filtro_numero_documento_compra) );
        }

        if ( $form->filtro_visto_bueno != null ) {

            if ( $form->filtro_visto_bueno == 'Solicitado' ) {
                //Solicitado
                $documentos = $documentos->where( 'id_visto_bueno', '<>', null );
            }

            if ( $form->filtro_visto_bueno == 'Rechazado' ) {
                //Rechazado
                $documentos = $documentos->where( 'id_rechazo_visto_bueno', '<>', null );
            }

            if ( $form->filtro_visto_bueno == 'Confirmado' ) {
                //Confirmado
                $documentos = $documentos->whereHas( 'getVistoBueno' , function ( $queryTwo ) {
                    $queryTwo->where( 'id_registrador_memo_respuesta', '<>', null );
                });
            }

        }

        $documentos = $documentos->select(
            'fecha_recepcion', 'fecha_documento', 'total_documento','id',
            'numero_documento', 'documento_compra','id_proveedor','id_tipo_documento',
            'fecha_ingreso', 'id_relacion', 'validado', 'id_visto_bueno',
            'id_rechazo_visto_bueno', 'id_estado', 'id_devengador','total_documento_actualizado',
            'id_user_problema', 'reclamado'
        );

        $documentos = $documentos->get();

        $data = [];

        if ( $request->get('length') != -1 ) {
            $hasta = $request->get('start') + $request->get('length');
        } else {
            // Se quieren ver todos los datos
            $hasta = $request->get('start') + $documentos->count();
        }
        
        for( $i = $request->get('start') ; $i < $hasta ; $i++ ) {
            if ( isset($documentos[$i]) ) {
                $data[] = $documentos[$i]->getDatosGeneral($dataTable);
            }
        }

        // Se realiza la busqueda y se ordenan los datos
        if ( $documentos->count() > 0 ) {

            foreach ($data as $clave => $fila) {

                if ( $search != '' ) {
                    if ( in_array_r($search,$fila) ) {
                        // dd("lo encuentra",$fila,$search);
                        $columna[$clave] = $fila[$order_column];
                    } else {
                        // dd("NO lo encuentra",$fila,$search);
                        unset( $data[$clave] ); // quita el elemento del data
                    }
                } else {
                    $columna[$clave] = $fila[$order_column];
                }
                
            }

            
            // Se ordenan los datos, segun la columna seleccionada
            if ( $order_dir == 'asc' ) {
                array_multisort($columna, SORT_ASC, $data);
            } elseif ( $order_dir == 'desc' ) {
                array_multisort($columna, SORT_DESC, $data);
            }

        }
            

        $respuesta = array(
            "draw" => $request->get('draw'),
            "recordsTotal" => count($documentos),
            "recordsFiltered"=> count($documentos),
            "data" => $data
        );

        if ( $dataTable == 'Buscador' && $request->get('draw') == 1 ) {
            return '';
        } else {
            return json_encode($respuesta);
        }

    }

    /**
     * Muestra un listado general de los documentos ingresados en el sistema
     */
    public function index()
    {
        // if (!\Entrust::can(['editar-documento','eliminar-documento','ver-documento','ver-general-documento'])) {
        if (!\Entrust::can(['editar-documento','ver-documento','ver-general-documento'])) {
            return \Redirect::to('home');
        }

        $tiposDocumento = TipoDocumento::whereNotIn('id',[2,8])->get();
        $modalidadesCompra = ModalidadCompra::all();
        $tiposAdjudicacion = TipoAdjudicacion::all();
        $tiposInforme = TipoInforme::all();
        $usuariosResponsables = User::where('responsable',1)->get();
        $referentesTecnicos = ReferenteTecnico::all();
        $usuariosDigitadores = User::has('getDocumentos')->get();
        $proveedores = Proveedor::all();
        $itemsPresupuestarios = ItemPresupuestario::all();

        // Select para Vistos Buenos
        $vistoBueno1 = new \stdClass;
        $vistoBueno1->opcion = "Solicitado";
        $vistoBueno2 = new \stdClass;
        $vistoBueno2->opcion = "Rechazado";
        $vistoBueno3 = new \stdClass;
        $vistoBueno3->opcion = "Confirmado";

        $vistosBuenos[] = $vistoBueno1;
        $vistosBuenos[] = $vistoBueno2;
        $vistosBuenos[] = $vistoBueno3;

        // Select para Fechas a utilizar en el filtro
        $fechaUtilizar1 = new \stdClass;
        $fechaUtilizar1->opcion = 'Ingreso';
        $fechaUtilizar2 = new \stdClass;
        $fechaUtilizar2->opcion = 'Recepción';

        $fechasUtilizar[] = $fechaUtilizar1;
        $fechasUtilizar[] = $fechaUtilizar2;

        $filtroFechaInicio = date("d/m/Y");
        $filtroFechaTermino = date("d/m/Y");
        
        return view('documento.index')->with('tiposDocumento',$tiposDocumento)
                                      ->with('modalidadesCompra',$modalidadesCompra)
                                      ->with('tiposAdjudicacion',$tiposAdjudicacion)
                                      ->with('tiposInforme',$tiposInforme)
                                      ->with('usuariosResponsables',$usuariosResponsables)
                                      ->with('referentesTecnicos',$referentesTecnicos)
                                      ->with('usuariosDigitadores',$usuariosDigitadores)
                                      ->with('proveedores',$proveedores)
                                      ->with('itemsPresupuestarios',$itemsPresupuestarios)
                                      ->with('vistosBuenos',$vistosBuenos)
                                      ->with('fechasUtilizar',$fechasUtilizar);

    }

    public function getBuscadorDocumentos()
    {
        if (!\Entrust::can(['ver-documento'])) {
            return \Redirect::to('home');
        }

        $tiposDocumento = TipoDocumento::whereNotIn('id',[2,8])->get();
        $modalidadesCompra = ModalidadCompra::all();
        $tiposAdjudicacion = TipoAdjudicacion::all();
        $tiposInforme = TipoInforme::all();
        $usuariosResponsables = User::where('responsable',1)->get();
        $referentesTecnicos = ReferenteTecnico::all();
        $usuariosDigitadores = User::has('getDocumentos')->get();
        $proveedores = Proveedor::all();
        $itemsPresupuestarios = ItemPresupuestario::all();

        // Select para Vistos Buenos
        $vistoBueno1 = new \stdClass;
        $vistoBueno1->opcion = "Solicitado";
        $vistoBueno2 = new \stdClass;
        $vistoBueno2->opcion = "Rechazado";
        $vistoBueno3 = new \stdClass;
        $vistoBueno3->opcion = "Confirmado";

        $vistosBuenos[] = $vistoBueno1;
        $vistosBuenos[] = $vistoBueno2;
        $vistosBuenos[] = $vistoBueno3;

        // Select para Fechas a utilizar en el filtro
        $fechaUtilizar1 = new \stdClass;
        $fechaUtilizar1->opcion = 'Ingreso';
        $fechaUtilizar2 = new \stdClass;
        $fechaUtilizar2->opcion = 'Recepción';

        $fechasUtilizar[] = $fechaUtilizar1;
        $fechasUtilizar[] = $fechaUtilizar2;

        $filtroFechaInicio = date("d/m/Y");
        $filtroFechaTermino = date("d/m/Y");

        $verDocumento = \Auth::user()->can('ver-documento');
        $verFacturaDocumentoAcepta = \Auth::user()->can('ver-factura-documento-acepta');

        return view('documento.buscador')->with('tiposDocumento', $tiposDocumento)
                                         ->with('modalidadesCompra', $modalidadesCompra)
                                         ->with('tiposAdjudicacion', $tiposAdjudicacion)
                                         ->with('tiposInforme', $tiposInforme)
                                         ->with('usuariosResponsables', $usuariosResponsables)
                                         ->with('referentesTecnicos', $referentesTecnicos)
                                         ->with('usuariosDigitadores', $usuariosDigitadores)
                                         ->with('proveedores', $proveedores)
                                         ->with('itemsPresupuestarios', $itemsPresupuestarios)
                                         ->with('vistosBuenos', $vistosBuenos)
                                         ->with('fechasUtilizar', $fechasUtilizar)
                                         ->with('verDocumento', $verDocumento)
                                         ->with('verFacturaDocumentoAcepta', $verFacturaDocumentoAcepta);
    }

    /**
     * Datatable del buscador
     */
    public function dataTableBuscador(Request $request)
    {
        // dd($request->all());

        set_time_limit ( 0 );
        ini_set('max_execution_time', 0);

        $search = $request->get('search')['value'];
        $order_column = $request->get('order')[0]['column'];
        $order_dir = $request->get('order')[0]['dir'];
        
        // Por si es necesario arreglar el search
        // if ( $search == '' ) {
        //     $documentos = Documento::with(['getProveedor','getTipoDocumento','getDevengos.getItemPresupuestario',
        //                                'audits.user'])
        //                            ->where('fecha_ingreso','>=',date('Y-m').'-9 00:00:00');
        // } else {

        // }

        // $documentos = $documentos->get();
        
        $formJSON = json_decode($request->get('form'));
        $form = construyeFormulario($formJSON);
        
        $fechaInicio = fecha_Y_m_d($form->filtro_fecha_inicio);
        $fechaTermino = fecha_Y_m_d($form->filtro_fecha_termino);

        $documentos = Documento::withTrashed()
                               ->with([
                                    'getProveedor' => function ($query) {
                                        $query->select('id', 'nombre', 'rut');
                                    },
                                    'getTipoDocumento' => function ($query) {
                                        $query->select('id', 'nombre');
                                    }
                                    ,'getDevengos.getItemPresupuestario',
                                    'getVistoBueno','getUnaRelacionRecepcionValidada',
                                    'getRelacionRecepcionesNoValidadas'
                                ]);
                            
        if ( $form->filtro_numero_documento == null && $form->filtro_numero_documento_compra == null ) {
            // Se filtra por las fechas

            if ( $form->filtro_fecha_inicio != null ) {

                if ( $form->filtro_fecha_a_utilizar == 'Ingreso' ) {
                    $documentos = $documentos->where('fecha_ingreso','>=',$fechaInicio.' 00:00:00');
                } elseif ( $form->filtro_fecha_a_utilizar == 'Recepción' ) {
                    $documentos = $documentos->where('fecha_recepcion','>=',$fechaInicio);
                }

            }

            if ( $form->filtro_fecha_termino != null ) {
                if ( $form->filtro_fecha_a_utilizar == 'Ingreso' ) {
                    $documentos = $documentos->where('fecha_ingreso','<=',$fechaTermino.' 23:59:59');
                } elseif ( $form->filtro_fecha_a_utilizar == 'Recepción' ) {
                    $documentos = $documentos->where('fecha_recepcion','<=',$fechaTermino);
                }
            }

        } else {
            // no se consideran las fechas, y se busca el numero del documento
            if ( $form->filtro_numero_documento != null ) {
                $documentos = $documentos->where('numero_documento', trim($form->filtro_numero_documento) );
            }
            
            if ( $form->filtro_numero_documento_compra != null ) {
                $documentos = $documentos->where('documento_compra', trim($form->filtro_numero_documento_compra) );
            }

        }

        
        
        if ( isset($form->filtro_proveedor) ) {
            $documentos = $documentos->WhereIn('id_proveedor',$form->filtro_proveedor);
        }

        if ( isset($form->filtro_tipo_documento) ) {
            $documentos = $documentos->WhereIn('id_tipo_documento',$form->filtro_tipo_documento);
        }

        if ( isset($form->filtro_modalidad_compra) ) {
            $documentos = $documentos->WhereIn('id_modalidad_compra',$form->filtro_modalidad_compra);
        }

        if ( isset($form->filtro_tipo_adjudicacion) ) {
            $documentos = $documentos->WhereIn('id_tipo_adjudicacion',$form->filtro_tipo_adjudicacion);
        }

        if ( isset($form->filtro_tipo_informe) ) {
            $documentos = $documentos->WhereIn('id_tipo_informe',$form->filtro_tipo_informe);
        }

        if ( isset($form->filtro_responsable) ) {
            $documentos = $documentos->WhereIn('id_responsable',$form->filtro_responsable);
        }

        if ( isset($form->filtro_referente_tecnico) ) {
            $documentos = $documentos->WhereIn('id_referente_tecnico',$form->filtro_referente_tecnico);
        }

        if ( isset($form->filtro_digitador) ) {
            $documentos = $documentos->WhereIn('id_digitador',$form->filtro_digitador);
        }

        if ( isset($form->filtro_item_presupuestario) ) {
            $documentos = $documentos->whereHas('getDevengos',function ($query) use ($form) {
                $query->WhereIn('id_item_presupuestario',$form->filtro_item_presupuestario);
            });
        }

        

        if ( $form->filtro_visto_bueno != null ) {

            if ( $form->filtro_visto_bueno == 'Solicitado' ) {
                //Solicitado
                $documentos = $documentos->where( 'id_visto_bueno', '<>', null );
            }

            if ( $form->filtro_visto_bueno == 'Rechazado' ) {
                //Rechazado
                $documentos = $documentos->where( 'id_rechazo_visto_bueno', '<>', null );
            }

            if ( $form->filtro_visto_bueno == 'Confirmado' ) {
                //Confirmado
                $documentos = $documentos->whereHas( 'getVistoBueno' , function ( $queryTwo ) {
                    $queryTwo->where( 'id_registrador_memo_respuesta', '<>', null );
                });
            }

        }

        $documentos = $documentos->get();

        $verDocumento = $form->permiso_ver_documento;
        $data = [];

        foreach ( $documentos as $doc ) {
            $data[] = $doc->getDatosBuscador($verDocumento);
        }

        /**
         * Se trabajan los documentos de ACEPTA
         */
        $archivosAcepta = ArchivoAcepta::with(['getProveedor','getTipoDocumento'])
                                       ->where('cargado', 0);

        if ( $form->filtro_numero_documento == null && $form->filtro_numero_documento_compra == null ) {

            $archivosAcepta = $archivosAcepta->where('created_at','>=',$fechaInicio.' 00:00:00')
                                             ->where('created_at','<=',$fechaTermino.' 23:59:59');

        } else {
            if ( $form->filtro_numero_documento != null ) {
                $archivosAcepta = $archivosAcepta->where('folio', trim($form->filtro_numero_documento) );
            }
        }

        if ( isset($form->filtro_proveedor) ) {
            $archivosAcepta = $archivosAcepta->WhereIn('id_proveedor', $form->filtro_proveedor);
        }

        if ( isset($form->filtro_tipo_documento) ) {
            $archivosAcepta = $archivosAcepta->WhereIn('id_tipo_documento', $form->filtro_tipo_documento);
        }

        $archivosAcepta = $archivosAcepta->get();

        if ( $form->filtro_numero_documento_compra == null ) {
            $verFacturaDocumentoAcepta = $form->permiso_ver_factura_documento_acepta;
            foreach ( $archivosAcepta as $archivoAcepta ) {
                $data[] = $archivoAcepta->getDatosBuscador($verFacturaDocumentoAcepta);
            }
        }
            

        $respuesta = array(
            "data" => $data
        );

        return json_encode($respuesta);

    }

    public function postBuscadorExcel(Request $request)
    {
        set_time_limit ( 0 );
        ini_set('max_execution_time', 0);

        \Excel::create('SIFCON | Buscador '.date('d-m-Y').'', function($excel) use($request) {
            $fechaInicio = fecha_Y_m_d($request->input('filtro_fecha_inicio'));
            $fechaTermino = fecha_Y_m_d($request->input('filtro_fecha_termino'));

            $documentos = Documento::with([
                                           'getProveedor','getTipoDocumento',
                                           'getDevengos.getItemPresupuestario',
                                           'getDevengos.getFolioSigfe',
                                           'getRelacionRecepcionesValidadas', 'getVistoBueno',
                                           'getRelacionRecepcionesNoValidadas'
                                        ]);

            if ( $request->input('filtro_numero_documento') == null  ) {

                if ( $request->input('filtro_fecha_inicio') != null ) {

                    if ( $request->input('filtro_fecha_a_utilizar') == 'Ingreso' ) {
                        $documentos = $documentos->where('fecha_ingreso','>=', $fechaInicio.' 00:00:00');
                    } elseif ( $request->input('filtro_fecha_a_utilizar') == 'Recepción' ) {
                        $documentos = $documentos->where('fecha_recepcion','>=', $fechaInicio);
                    }

                }

                if ( $request->input('filtro_fecha_termino') != null ) {
                    
                    if ( $request->input('filtro_fecha_a_utilizar') == 'Ingreso' ) {
                        $documentos = $documentos->where('fecha_ingreso','<=',$fechaTermino.' 23:59:59');
                    } elseif ( $request->input('filtro_fecha_a_utilizar') == 'Recepción' ) {
                        $documentos = $documentos->where('fecha_recepcion','<=',$fechaTermino);
                    }

                }

            } else {
                $documentos = $documentos->where('numero_documento', trim($request->input('filtro_numero_documento')) );
            }
            

            if ( $request->input('filtro_proveedor') != null ) {
                $documentos = $documentos->WhereIn('id_proveedor',$request->input('filtro_proveedor'));
            }

            if ( $request->input('filtro_tipo_documento') != null ) {
                $documentos = $documentos->WhereIn('id_tipo_documento',$request->input('filtro_tipo_documento'));
            }

            if ( $request->input('filtro_modalidad_compra') != null ) {
                $documentos = $documentos->WhereIn('id_modalidad_compra',$request->input('filtro_modalidad_compra'));
            }

            if ( $request->input('filtro_tipo_adjudicacion') != null ) {
                $documentos = $documentos->WhereIn('id_tipo_adjudicacion',$request->input('filtro_tipo_adjudicacion'));
            }

            if ( $request->input('filtro_tipo_informe') != null ) {
                $documentos = $documentos->WhereIn('id_tipo_informe',$request->input('filtro_tipo_informe'));
            }

            if ( $request->input('filtro_responsable') != null ) {
                $documentos = $documentos->WhereIn('id_responsable',$request->input('filtro_responsable'));
            }

            if ( $request->input('filtro_referente_tecnico') != null ) {
                $documentos = $documentos->WhereIn('id_referente_tecnico',$request->input('filtro_referente_tecnico'));
            }

            if ( $request->input('filtro_digitador') != null ) {
                $documentos = $documentos->WhereIn('id_digitador',$request->input('filtro_digitador'));
            }

            if ( $request->input('filtro_item_presupuestario') != null) {
                $documentos = $documentos->whereHas('getDevengos',function ($query) use ($request) {
                    $query->WhereIn('id_item_presupuestario',$request->input('filtro_item_presupuestario'));
                });
            }


            if ( $request->input('filtro_numero_documento_compra') != null ) {
                $documentos = $documentos->where('documento_compra', trim($request->input('filtro_numero_documento_compra')) );
            }

            if ( $request->input('filtro_visto_bueno') != null ) {

                if ( $request->input('filtro_visto_bueno') == 'Solicitado' ) {
                    //Solicitado
                    $documentos = $documentos->where( 'id_visto_bueno', '<>', null );
                }
    
                if ( $request->input('filtro_visto_bueno') == 'Rechazado' ) {
                    //Rechazado
                    $documentos = $documentos->where( 'id_rechazo_visto_bueno', '<>', null );
                }
    
                if ( $request->input('filtro_visto_bueno') == 'Confirmado' ) {
                    //Confirmado
                    $documentos = $documentos->whereHas( 'getVistoBueno' , function ( $queryTwo ) {
                        $queryTwo->where( 'id_registrador_memo_respuesta', '<>', null );
                    });
                }

            }

            $documentos = $documentos->get();

            /**
             * Se trabajan los documentos de ACEPTA
             */
            $archivosAcepta = ArchivoAcepta::with(['getProveedor','getTipoDocumento'])
                                           ->where('cargado', 0);

            if ( $request->input('filtro_numero_documento') == null ) {

                $archivosAcepta = $archivosAcepta->where('created_at','>=', $fechaInicio.' 00:00:00')
                                                 ->where('created_at','<=', $fechaTermino.' 23:59:59');
    
            } else {
    
                $archivosAcepta = $archivosAcepta->where('folio', trim($request->input('filtro_numero_documento')) );
    
            }
    
            if ( $request->input('filtro_proveedor') != null ) {
                $archivosAcepta = $archivosAcepta->WhereIn('id_proveedor', $request->input('filtro_proveedor'));
            }
    
            if ( $request->input('filtro_tipo_documento') != null ) {
                $archivosAcepta = $archivosAcepta->WhereIn('id_tipo_documento', $request->input('filtro_tipo_documento'));
            }
    
            $archivosAcepta = $archivosAcepta->get();

            // dd($archivosAcepta,'holo');

            // dd('hola');

            $excel->sheet('Documentos', function($sheet) use($documentos, $archivosAcepta) {
            
                $sheet->row(1, [
                    'Estado', 
                    // 'Responsable', 
                    'Factoring',
                    'Chile-Compra',
                    'M/Dev',
                    'Proveedor',
                    'RUT',
                    'Tipo Dcto.',
                    'N° Dcto.',
                    'F/Doc.',
                    'M/Doc.',
                    'Fecha RD',
                    'Mes RD',
                    'Item',
                    'ID SIGFE',
                    'Fecha ID',
                    'Devengado',
                    'Mecanismo de Adjudicación',
                    'Observación',
                    'Detalle',
                    'URL Factura',
                    'URL Recepción'
                ]);

                $totalDevengadoExcel = 0;
                $cuentaLineas = 1;
                foreach ( $documentos as $indexAux => $documento ) {
                    $items = '';
                    $totalDevengadoPorDocumento = 0;

                    foreach ( $documento->getDevengos as $index => $devengo) {
                        $cuentaLineas++;
                        $items = $devengo->getItemPresupuestario->codigo();
                        $totalDevengadoPorDocumento = $devengo->monto;
                        // para columna "devengado"
                        $devengado = '';
                        if ($documento->id_tipo_documento == 4 || $documento->id_tipo_documento == 10) {
                            $devengado .= '-'.$totalDevengadoPorDocumento;
                            $totalDevengadoExcel = $totalDevengadoExcel - $totalDevengadoPorDocumento;
                        } else {
                            $devengado .= $totalDevengadoPorDocumento;
                            $totalDevengadoExcel = $totalDevengadoExcel + $totalDevengadoPorDocumento;
                        }

                        // para columna factoring
                        $factoring = '';
                        if ($documento->getProveedorFactoring) {
                            $factoring .= $documento->getProveedorFactoring->nombre.' '.$documento->getProveedorFactoring->rut;
                        }

                        $mesSigfe = '';
                        $fechaSigfe = '';
                        $idSigfe = '';
                        $detalleSigfe = '';
                        if ($devengo->id_folio_sigfe != null) {
                            $mesSigfe = (int) explode("-",$devengo->getFolioSigfe->fecha_sigfe)[1];
                            $fechaSigfe = fecha_dmY($devengo->getFolioSigfe->fecha_sigfe);
                            $idSigfe = $devengo->getFolioSigfe->id_sigfe;
                            $detalleSigfe = $devengo->getFolioSigfe->detalle_sigfe;
                        }

                        $urlRecepcion = '';
                        if ( $documento->getUnaRelacionRecepcionValidada ) {
                            foreach ( $documento->getRelacionRecepcionesValidadas as $relacion ) {
                                if ( $relacion->getWsDocumento ) {
                                    if ( $relacion->getWsDocumento->getWsArchivos ) {
                                        foreach ($relacion->getWsDocumento->getWsArchivos as $archivo) {
                                            $urlRecepcion .= 'http://10.4.237.28/finanzas/'.$archivo->ubicacion.PHP_EOL;
                                        }
                                    }
                                }
                            }
                            
                        }

                        $sheet->row($cuentaLineas, [
                            $documento->getEstadoDocumento('excel'),
                            // $documento->getResponsable->name, 
                            $factoring,
                            $documento->documento_compra,
                            $mesSigfe, // mes de la fecha sigfe
                            $documento->getProveedor->nombre,
                            $documento->getProveedor->rut,
                            $documento->getTipoDocumento->nombre,
                            $documento->numero_documento,
                            fecha_dmY($documento->fecha_documento),
                            (int) explode("-",$documento->fecha_documento)[1], // mes de la fecha del documento
                            fecha_dmY($documento->fecha_recepcion),
                            (int) explode("-",$documento->fecha_recepcion)[1], // mes de la fecha de recepcion del documento
                            $items,
                            $idSigfe,
                            $fechaSigfe,
                            $devengado,
                            $documento->getTipoAdjudicacion->nombre,
                            $documento->observacion,
                            $detalleSigfe,
                            $documento->getUrlFactura(),
                            $urlRecepcion
                        ]);
                    }
                }

                $cuentaLineas++;

                $sheet->row( $cuentaLineas , [
                    '', 
                    '', 
                    // '',
                    '',
                    '', 
                    '',
                    '',
                    '',
                    '',
                    '',
                    '', 
                    '',
                    '',
                    '',
                    '',
                    'TOTAL',
                    $totalDevengadoExcel,
                    '',
                    '',
                    '',
                    '',
                    ''
                ]);
                
                // Trabajar archivos de Acepta.
                foreach ( $archivosAcepta as $archivoAcepta ) {

                    $cuentaLineas++;

                    $sheet->row($cuentaLineas, [
                        $archivoAcepta->getEstadoAcepta('excel'),
                        '', 
                        // '',
                        '',
                        '', // mes de la fecha sigfe
                        $archivoAcepta->getProveedor->nombre,
                        $archivoAcepta->getProveedor->rut,
                        $archivoAcepta->getTipoDocumento->nombre,
                        $archivoAcepta->folio,
                        ( strpos($archivoAcepta->emision,'-') !== false ) ? fecha_dmY($archivoAcepta->emision) : '',
                        ( strpos($archivoAcepta->emision,'-') !== false ) ? (int) explode("-",$archivoAcepta->emision)[1] : '', // mes de la fecha del documento
                        '',
                        '', // mes de la fecha de recepcion del documento
                        '',
                        '',
                        '',
                        $archivoAcepta->monto_total, // total
                        '',
                        '',
                        '',
                        '',
                        ''
                    ]);

                }


               

        });

        })->export('xlsx');
    }

    public function postGenerarExcel(Request $request)
    {
        set_time_limit ( 0 );
        ini_set('max_execution_time', 0);
        
        \Excel::create('SIFCON | Documentos General '.date('d-m-Y').'', function($excel) use($request) {
            $fechaInicio = fecha_Y_m_d($request->input('filtro_fecha_inicio'));
            $fechaTermino = fecha_Y_m_d($request->input('filtro_fecha_termino'));

            $documentos = Documento::with(['getProveedor','getTipoDocumento',
                                           'getDevengos.getItemPresupuestario',
                                           'getDevengos.getFolioSigfe',
                                           'getRelacionRecepcionesNoValidadas'
                                           ]);

            if ( $request->input('filtro_fecha_inicio') != null && $request->input('filtro_numero_documento') == null ) {
                if ( $request->input('filtro_fecha_a_utilizar') == 'Ingreso' ) {
                    $documentos = $documentos->where('fecha_ingreso','>=',$fechaInicio.' 00:00:00');
                } elseif ( $request->input('filtro_fecha_a_utilizar') == 'Recepción' ) {
                    $documentos = $documentos->where('fecha_recepcion','>=',$fechaInicio);
                }        
            }

            if ( $request->input('filtro_fecha_termino') != null && $request->input('filtro_numero_documento') == null ) {
                if ( $request->input('filtro_fecha_a_utilizar') == 'Ingreso' ) {
                    $documentos = $documentos->where('fecha_ingreso','<=',$fechaTermino.' 23:59:59');
                } elseif ( $request->input('filtro_fecha_a_utilizar') == 'Recepción' ) {
                    $documentos = $documentos->where('fecha_recepcion','<=',$fechaTermino);
                }
            }

            if ( $request->input('filtro_proveedor') != null ) {
                $documentos = $documentos->WhereIn('id_proveedor',$request->input('filtro_proveedor'));
            }

            if ( $request->input('filtro_tipo_documento') != null ) {
                $documentos = $documentos->WhereIn('id_tipo_documento',$request->input('filtro_tipo_documento'));
            }

            if ( $request->input('filtro_modalidad_compra') != null ) {
                $documentos = $documentos->WhereIn('id_modalidad_compra',$request->input('filtro_modalidad_compra'));
            }

            if ( $request->input('filtro_tipo_adjudicacion') != null ) {
                $documentos = $documentos->WhereIn('id_tipo_adjudicacion',$request->input('filtro_tipo_adjudicacion'));
            }

            if ( $request->input('filtro_tipo_informe') != null ) {
                $documentos = $documentos->WhereIn('id_tipo_informe',$request->input('filtro_tipo_informe'));
            }

            if ( $request->input('filtro_responsable') != null ) {
                $documentos = $documentos->WhereIn('id_responsable',$request->input('filtro_responsable'));
            }

            if ( $request->input('filtro_referente_tecnico') != null ) {
                $documentos = $documentos->WhereIn('id_referente_tecnico',$request->input('filtro_referente_tecnico'));
            }

            if ( $request->input('filtro_digitador') != null ) {
                $documentos = $documentos->WhereIn('id_digitador',$request->input('filtro_digitador'));
            }

            if ( $request->input('filtro_item_presupuestario') != null) {
                $documentos = $documentos->whereHas('getDevengos',function ($query) use ($request) {
                    $query->WhereIn('id_item_presupuestario',$request->input('filtro_item_presupuestario'));
                });
            }

            if ( $request->input('filtro_numero_documento') != null ) {
                $documentos = $documentos->where('numero_documento', trim($request->input('filtro_numero_documento')) );
            }

            if ( $request->input('filtro_numero_documento_compra') != null ) {
                $documentos = $documentos->where('documento_compra', trim($request->input('filtro_numero_documento_compra')) );
            }

            $documentos = $documentos->get();

            $excel->sheet('Documentos', function($sheet) use($documentos) {
            
                $sheet->row(1, [
                    'Estado', 
                    // 'Responsable', 
                    'Factoring',
                    'Chile-Compra',
                    'M/Dev',
                    'Proveedor',
                    'RUT',
                    'Tipo Dcto.',
                    'N° Dcto.',
                    'F/Doc.',
                    'M/Doc.',
                    'Fecha RD',
                    'Mes RD',
                    'Item',
                    'ID SIGFE',
                    'Fecha ID',
                    'Devengado',
                    'Mecanismo de Adjudicación',
                    'Observación',
                    'Detalle',
                    'URL Factura',
                    'URL Recepción'
                ]);

                $totalDevengadoExcel = 0;
                $cuentaLineas = 1;
                foreach ( $documentos as $indexAux => $documento ) {
                    $items = '';
                    $totalDevengadoPorDocumento = 0;

                    foreach ( $documento->getDevengos as $index => $devengo) {
                        $cuentaLineas++;
                        $items = $devengo->getItemPresupuestario->codigo();
                        $totalDevengadoPorDocumento = $devengo->monto;
                        // para columna "devengado"
                        $devengado = '';
                        if ($documento->id_tipo_documento == 4 || $documento->id_tipo_documento == 10) {
                            $devengado .= '-'.$totalDevengadoPorDocumento;
                            $totalDevengadoExcel = $totalDevengadoExcel - $totalDevengadoPorDocumento;
                        } else {
                            $devengado .= $totalDevengadoPorDocumento;
                            $totalDevengadoExcel = $totalDevengadoExcel + $totalDevengadoPorDocumento;
                        }

                        // para columna factoring
                        $factoring = '';
                        if ($documento->getProveedorFactoring) {
                            $factoring .= $documento->getProveedorFactoring->nombre.' '.$documento->getProveedorFactoring->rut;
                        }

                        $estado = str_replace('<br>',' ',$documento->getEstadoDocumento());

                        $mesSigfe = '';
                        $fechaSigfe = '';
                        $idSigfe = '';
                        $detalleSigfe = '';
                        if ($devengo->id_folio_sigfe != null) {
                            $mesSigfe = (int) explode("-",$devengo->getFolioSigfe->fecha_sigfe)[1];
                            $fechaSigfe = fecha_dmY($devengo->getFolioSigfe->fecha_sigfe);
                            $idSigfe = $devengo->getFolioSigfe->id_sigfe;
                            $detalleSigfe = $devengo->getFolioSigfe->detalle_sigfe;
                        }

                        $urlFactura = '';
                        if ( $documento->getArchivos ) {
                            foreach ($documento->getArchivos as $archivo) {
                                if ( $archivo->id_tipo_archivo == 1 ) {
                                    if ($archivo->cargado == 1) {
                                        $urlFactura = 'http://'.$archivo->ubicacion;
                                    } else {
                                        $urlFactura = 'http://10.4.237.28/finanzas/'.$archivo->ubicacion;
                                    }
                                    
                                }
                            }
                        }

                        $urlRecepcion = '';
                        if ( $documento->getUnaRelacionRecepcionValidada ) {
                            foreach ( $documento->getRelacionRecepcionesValidadas as $relacion ) {
                                if ( $relacion->getWsDocumento ) {
                                    if ( $relacion->getWsDocumento->getWsArchivos ) {
                                        foreach ($relacion->getWsDocumento->getWsArchivos as $archivo) {
                                            $urlRecepcion .= 'http://10.4.237.28/finanzas/'.$archivo->ubicacion.PHP_EOL;
                                        }
                                    }
                                }
                            }
                            
                        }

                        $sheet->row($cuentaLineas, [
                            $estado, 
                            // $documento->getResponsable->name, 
                            $factoring,
                            $documento->documento_compra,
                            $mesSigfe, // mes de la fecha sigfe
                            $documento->getProveedor->nombre,
                            $documento->getProveedor->rut,
                            $documento->getTipoDocumento->nombre,
                            $documento->numero_documento,
                            fecha_dmY($documento->fecha_documento),
                            (int) explode("-",$documento->fecha_documento)[1], // mes de la fecha del documento
                            fecha_dmY($documento->fecha_recepcion),
                            (int) explode("-",$documento->fecha_recepcion)[1], // mes de la fecha de recepcion del documento
                            $items,
                            $idSigfe,
                            $fechaSigfe,
                            $devengado,
                            $documento->getTipoAdjudicacion->nombre,
                            $documento->observacion,
                            $detalleSigfe,
                            $urlFactura,
                            $urlRecepcion
                        ]);
                    }
                }


                $sheet->row( $cuentaLineas+1 , [
                    '', 
                    '', 
                    // '',
                    '',
                    '', 
                    '',
                    '',
                    '',
                    '',
                    '',
                    '', 
                    '',
                    '',
                    '',
                    '',
                    'TOTAL',
                    $totalDevengadoExcel,
                    '',
                    '',
                    '',
                    '',
                    ''
                ]);

        });

        })->export('xlsx');
    }

    /**
     * Funcion que muestra la modal con la informacion del documento
     */
    public function getModalVerDocumento($idDocumento, $eliminado = false)
    {
        
        $documento = Documento::with(['getProveedor','getTipoDocumento','getModalidadCompra',
                                      'getTipoAdjudicacion','getTipoInforme','getResponsable',
                                      'getReferenteTecnico','getDigitador','getDevengador',
                                      'getArchivos','getArchivos.getTipoArchivo','getDevengos',
                                      'getDevengos.getItemPresupuestario','getRelacionRecepcionesValidadas.getUsuarioResponsable']);
                            
        if ( $eliminado ) {
            $documento = $documento->onlyTrashed();
        }

        $documento = $documento->findOrFail($idDocumento);

        return view('documento.modal_ver_documento')->with('documento',$documento);
    }

    /**
     * Muestra modal para realizar conexion entre documento ingresado y documento recepcionado de bodega
     */
    public function getModalConectarDocumentos( $idDocumento, $idWsDocumento)
    {
        $documento = Documento::with(['getProveedor','getTipoDocumento','getModalidadCompra',
                                      'getTipoAdjudicacion','getTipoInforme','getResponsable',
                                      'getReferenteTecnico','getDigitador','getDevengador',
                                      'getArchivos','getArchivos.getTipoArchivo','getDevengos',  
                                      'getDevengos.getItemPresupuestario','getRelacionRecepcionesValidadas.getUsuarioResponsable'])
                                      ->findOrFail($idDocumento);
                                    
        $wsDocumento = WsDocumento::with(['getWsOrdenCompra','getWsOrdenCompra.getWsContrato',
                                      'getWsArchivos','getWsItemsPresupuestarios'])
                                      ->findOrFail($idWsDocumento);

        $tiposArchivo = TipoArchivo::all();

        $archivoAcepta = ArchivoAcepta::where('id_proveedor',$documento->id_proveedor)
                                      ->where('id_tipo_documento',$documento->id_tipo_documento)
                                      ->where('folio',$documento->numero_documento)
                                      ->first();

        return view('documento.modal_conectar_documentos')->with('documento',$documento)
                                                          ->with('wsDocumento',$wsDocumento)
                                                          ->with('tiposArchivo',$tiposArchivo)
                                                          ->with('archivoAcepta',$archivoAcepta);
    }

    /**
     * Funcion que conecta los documentos (ingresado y de bodega)
     */
    public function postConectarDocumentos (Request $request)
    {
        // dd($request->all());
        // Mensajes de los validadores
        $messages = [
            'required'    => 'Debe ingresar el  :attribute',
            'numeric' => 'El :attribute debe solo contener números',
            'max' => 'El :attribute no debe exeder los :max caracteres',
            'min' => 'El :attribute debe tener minimo :min caracteres',
            'date_format' => 'La :attribute no tiene el formato correcto: día/mes/año',
        ];

        //validador de los input del formulario
        $validator = \Validator::make($request->all(), [
            'idDocumento' => 'required|numeric',
            'idWsDocumento' => 'required|numeric'
        ], $messages);

        //Si contiene errores se devuelven todos los errores, de lo contrario guarda en la base de datos
        if ( $validator->fails() ) {
            return response()->json($validator->errors(),400);
        } else {
            $documento = Documento::findOrFail($request->input('idDocumento'));
            $wsDocumento = WsDocumento::findOrFail($request->input('idWsDocumento'));
            // Se crea la relacion
            $newRelacion = new RelacionDocumentoWsDocumento();
            $newRelacion->id_documento = $documento->id;
            $newRelacion->id_ws_documento = $wsDocumento->id;
            $newRelacion->id_user_relacion = \Auth::user()->id;
            $newRelacion->updated_at = NULL;
            $newRelacion->save();
            // Se paasa el id correspondiente a cada documento para la conexion
            $documento->id_relacion = $newRelacion->id;
            $documento->validar();
            $documento->save();
            $documento->actualizaValidacionDocumentosRelacionados();
            $wsDocumento->id_relacion = $newRelacion->id;
            $wsDocumento->save();

            $datos = array(
                'mensajeDocumento' => 'Se han conectado exitosamente los Documentos.',
                'estadoDocumento' => 'success',
                'id' => $documento->id,
            );

            $archivo = $request->file('archivo');
            if ( $archivo != null ) {
                $nombreOriginalArchivo = str_replace('.pdf','',$archivo->getClientOriginalName() );
                $arreglo_nombre = explode(".", $archivo->getClientOriginalName() );
                $extension = strtolower( end( $arreglo_nombre ) );

                if ($extension != 'pdf') {
                    $datos['mensaje_archivo'] = 'Extension incorrecta del archivo';
                    $datos['estado_archivo'] = 'error';
                } else {
                    // Se trabajan las rutas del archivo, se guarda en el proyecto al final de la funcion
                    $nombre = $nombreOriginalArchivo.'_'.date('Y_m_d_G_i_s');
                    $rutaArchivo = '/documento/'.$documento->id.'/'.$nombre.'.pdf';
                    $pathArchivo = public_path().'/documento/'.$documento->id.'/';

                    $newArchivo = new Archivo();
                    $newArchivo->id_documento = $documento->id;
                    $newArchivo->id_tipo_archivo = $request->input('tipo_archivo');

                    $newArchivo->nombre = $nombre;
                    $newArchivo->nombre_original = $nombreOriginalArchivo;
                    $newArchivo->ubicacion = "documento/".$documento->id."/".$newArchivo->nombre.".pdf";
                    $newArchivo->extension = "pdf";
                    $newArchivo->peso = $archivo->getSize();
                    $newArchivo->save();

                    $datos['mensaje_archivo'] = 'Se ha guardado correctamente el archivo';
                    $datos['estado_archivo'] = 'success';

                    $archivo->move( $pathArchivo , $nombre.'.pdf' ); // Se guarda el archivo
                }
                
            }
        }

        return response()->json($datos,200);
    }
    
    /**
     * Muestra modal para realizar conexion entre documento ingresado y documento recepcionado de bodega
     */
    public function getModalDesconectarDocumentos( $idDocumento, $idWsDocumento = null)
    {
        $documento = Documento::with(['getProveedor','getTipoDocumento','getModalidadCompra',
                                      'getTipoAdjudicacion','getTipoInforme','getResponsable',
                                      'getReferenteTecnico','getDigitador','getDevengador',
                                      'getArchivos','getArchivos.getTipoArchivo','getDevengos',  
                                      'getDevengos.getItemPresupuestario'])
                                      ->findOrFail($idDocumento);
        if ( $idWsDocumento != null ) {
            $wsDocumento = WsDocumento::with(['getWsOrdenCompra','getWsOrdenCompra.getWsContrato',
                                      'getWsArchivos','getWsItemsPresupuestarios'])
                                      ->findOrFail($idWsDocumento);
        } else {
            $wsDocumento = WsDocumento::with(['getWsOrdenCompra','getWsOrdenCompra.getWsContrato',
                                            'getWsArchivos','getWsItemsPresupuestarios'])
                                            ->findOrFail($documento->getUnaRelacionRecepcionValidada->getWsDocumento->id);
        }                
        

        return view('documento.modal_desconectar_documentos')->with('documento',$documento)
                                                             ->with('wsDocumento',$wsDocumento);
    }

    /**
     * Funcion que conecta los documentos (ingresado y de bodega)
     */
    public function postDesconectarDocumentos (Request $request)
    {
        // Mensajes de los validadores
        $messages = [
            'required'    => 'Debe ingresar el  :attribute',
            'numeric' => 'El :attribute debe solo contener números',
            'max' => 'El :attribute no debe exeder los :max caracteres',
            'min' => 'El :attribute debe tener minimo :min caracteres',
            'date_format' => 'La :attribute no tiene el formato correcto: día/mes/año',
        ];

        //validador de los input del formulario
        $validator = \Validator::make($request->all(), [
            'idDocumento' => 'required|numeric',
            'idWsDocumento' => 'required|numeric'
        ], $messages);

        //Si contiene errores se devuelven todos los errores, de lo contrario guarda en la base de datos
        if ( $validator->fails() ) {
            return response()->json($validator->errors(),400);
        } else {
            $documento = Documento::findOrFail($request->input('idDocumento'));
            foreach ( $documento->getRelacionRecepcionesValidadas as $relacion ) {
                // Se elimiman las relaciones
                $relacion->getWsDocumento->id_relacion = null;
                $relacion->getWsDocumento->save();

                $relacion->delete();
            }

            $documento->id_relacion = null;
            $documento->invalidar();
            $documento->save();
            $documento->actualizaValidacionDocumentosRelacionados();
            $datos = array(
                'mensajeDocumento' => 'Se han Desvinculado exitosamente los Documentos.',
                'estadoDocumento' => 'success',
                'id' => $documento->id,
            );
        }

        return response()->json($datos,200);
    }

    /**
     * Funcion que muestra la modal para editar el documento
     */
    public function getModalEditarDocumento($idDocumento,$datatable = null)
    {
        $documento = Documento::with([
                                      'getProveedor','getTipoDocumento','getDevengos.getItemPresupuestario',
                                      'getArchivos.getTipoArchivo','getModalidadCompra',
                                      'getRelacionRecepcionesValidadas.getWsDocumento',
                                      'getDocumentosRelacionados','getDocumentosRelacionados.getTipoDocumento',
                                      'getDocumentosRelacionados.getArchivos',
                                      'getDocumentosRelacionados.getArchivos.getTipoArchivo',
                                      'getFoliosSigfe','getFoliosSigfe.getDevengos',
                                      'getFoliosSigfe.getDevengos.getItemPresupuestario', 'getContratoOrdenCompra',
                                      'getContratoOrdenCompra.getContrato', 'getContratoOrdenCompra.getOrdenCompra'
                                    ])
                              ->findOrFail($idDocumento);
        
        $tiposDocumento = TipoDocumento::whereNotIn('id',[2,8])->get();
        $modalidadesCompra = ModalidadCompra::all();
        $tiposAdjudicacion = TipoAdjudicacion::all();
        $tiposInforme = TipoInforme::all();
        $usuarios = User::where('responsable',1)->get();
        $referentesTecnicos = ReferenteTecnico::all();
        $tiposArchivo = TipoArchivo::all();
        $datatable = $datatable;

        // Para obtener documentos relacionados dependiendo del tipo de documento
        $documentosProveedor = null;
        $docsRechazadosAceptaProv = null;
        if ( $documento->id_tipo_documento == 1 ) {
            // Solo Facturas con NC al 100%
            // Que no tengan Facturas relacionadas, si la tienen es porque ya se refacturo
            $documentosProveedor = Documento::with(['getTipoDocumento', 'getDocumentosRelacionados'])
                                            ->where('id_tipo_documento', 1)
                                            ->where('id_proveedor', $documento->id_proveedor)
                                            ->whereHas('getDocumentosRelacionados', function($query) {
                                                $query->where('id_tipo_documento', 4); // NC
                                            })
                                            ->whereDoesntHave('getDocumentosRelacionados', function ($query) {
                                                $query->where('id_tipo_documento', 1); // FC
                                            })
                                            ->where('total_documento_actualizado' , 0) // significa que la factura esta anulada
                                            ->where('id','<>',$documento->id)
                                            ->get();

            // Solo facturas con NC por el 100% y que no tengan refactura (getDocumentoFactura)
            $docsRechazadosAceptaProv = ArchivoAcepta::with(['getProveedor','getTipoDocumento'])
                                                     ->where('cargado', 0)
                                                     ->where('id_user_rechazo','<>',null)
                                                     ->where('id_proveedor', $documento->id_proveedor)
                                                     ->where('id_tipo_documento', 1)
                                                     ->whereHas('getAceptaMotivoRechazo', function ($query){
                                                         $query->where('id_motivo_rechazo', 5); // Anula Documento
                                                     })
                                                     ->where('observacion_rechazo', 'LIKE', '%Tiene una Nota de Crédito por el total del monto%')
                                                     ->doesntHave('getDocumentoFactura') // Con esto, no sale el archivo acepta que tiene el documento( como refactura)
                                                     ->get();

        } elseif ( $documento->id_tipo_documento == 4 || $documento->id_tipo_documento == 5 || $documento->id_tipo_documento == 10 || $documento->id_tipo_documento == 11 ) {

            $documentosProveedor = Documento::with(['getDevengos.getItemPresupuestario','getTipoDocumento'])
                                            ->where('id_proveedor',$documento->id_proveedor)
                                            ->whereIn('id_tipo_documento',[1,2,8]);
            if ( $documento->id_relacionado != null ) {
                $documentosProveedor = $documentosProveedor->whereNotIn('id',[$documento->id,$documento->id_relacionado])->get();
            } else {
                $documentosProveedor = $documentosProveedor->where('id','<>',$documento->id)->get();
            }

        } elseif ($documento->id_tipo_documento == 8 ) {
            $documentosProveedor = Documento::with(['getDevengos.getItemPresupuestario','getTipoDocumento'])
                                            ->where('id_proveedor',$documento->id_proveedor)
                                            ->where('id_tipo_documento',4);
            if ( $documento->id_relacionado != null ) {
                $documentosProveedor = $documentosProveedor->whereNotIn('id',[$documento->id,$documento->id_relacionado])->get();
            } else {
                $documentosProveedor = $documentosProveedor->where('id','<>',$documento->id)->get();
            }
        }
        
        //Para obtener las recepciones disponibles en el sistema, con las dos firmas
        $opciones = '';
        if ( ! ($documento->id_tipo_documento == 4 || $documento->id_tipo_documento == 5 || $documento->id_tipo_documento == 10 || $documento->id_tipo_documento == 11) ) {

            $docBodegaDisponibles = WsDocumento::with(['getWsOrdenCompra.getWsContrato'])
                                                ->whereHas('getWsOrdenCompra.getWsContrato',function ($query) use ($documento) {
                                                    $query->whereIn('rut_proveedor', $documento->getProveedor->getTodosLosRut());
                                                })
                                                ->where('id_relacion', null)
                                                ->where('tipo_documento', 'LIKE', $documento->getTipoDocumento->nombre)
                                                ->get();
                                            
                                            
            if ( $documento->getUnaRelacionRecepcionValidada && ! $documento->getRelacionGuiasDespacho() ) {
                
                $opciones .= PHP_EOL.'<option selected id="docBodega_'.$documento->getUnaRelacionRecepcionValidada->getWsDocumento->id.'"';
                $opciones .= ' value="'.$documento->getUnaRelacionRecepcionValidada->getWsDocumento->id.'"';
                $opciones .= 'data-datosrecepcion="'.$documento->getUnaRelacionRecepcionValidada->getWsDocumento->getDatosDocumentoAndOrdenCompra().'" ';
                $opciones .= ' >'.$documento->getTipoDocumento->sigla.' N° '.$documento->getUnaRelacionRecepcionValidada->getWsDocumento->documento.' - '.fecha_dmY($documento->getUnaRelacionRecepcionValidada->getWsDocumento->fecha_carga).' ';
                $opciones .= '- $'.formatoMiles($documento->getUnaRelacionRecepcionValidada->getWsDocumento->documento_total);
                $opciones .= ' | Prov. '.$documento->getUnaRelacionRecepcionValidada->getWsDocumento->getWsOrdenCompra->getWsContrato->rut_proveedor;
                $opciones .= '</option>'.PHP_EOL;

            }


            if (count($docBodegaDisponibles) > 0 ) {
                foreach ($docBodegaDisponibles as $key => $documentoBodega) {

                    $opciones .= PHP_EOL.'<option id="docBodega_'.$documentoBodega->id.'" ';
                    $opciones .= 'value="'.$documentoBodega->id.'" ';
                    $opciones .= 'data-datosrecepcion="'.$documentoBodega->getDatosDocumentoAndOrdenCompra().'" ';
                    $opciones .= '>'.$documento->getTipoDocumento->sigla.' N° '.$documentoBodega->documento.' '; 
                    $opciones .= '- '.fecha_dmY($documentoBodega->fecha_carga).' - $'.formatoMiles($documentoBodega->documento_total).'';
                    $opciones .= ' | Prov. '.$documentoBodega->getWsOrdenCompra->getWsContrato->rut_proveedor;

                    $opciones .= '</option>'.PHP_EOL;

                }
            }
        }

        /**
         * Para las recepcion que es del tipo Guia de Despacho Electronica
         */
        $opcionesGuias = '';
        $totalGuias = 0;
        if ( ! ($documento->id_tipo_documento == 4 || $documento->id_tipo_documento == 5 || $documento->id_tipo_documento == 10 || $documento->id_tipo_documento == 11) ) {

            $documentosBodegaDisponibles = WsDocumento::with(['getWsOrdenCompra.getWsContrato'])
                                                ->whereHas('getWsOrdenCompra.getWsContrato',function ($query) use ($documento) {
                                                    $query->whereIn('rut_proveedor', $documento->getProveedor->getTodosLosRut());
                                                })
                                                ->where('id_relacion', null)
                                                ->where('tipo_documento', 'LIKE', '%Guia de Despacho%')
                                                ->get();
        
            if ($documento->getUnaRelacionRecepcionValidada && $documento->getRelacionGuiasDespacho()) {
                foreach ($documento->getRelacionRecepcionesValidadas as $key => $relacion) {

                    $totalGuias = $totalGuias + $relacion->getWsDocumento->documento_total;
                    $opcionesGuias .= PHP_EOL.'<option selected id="guiaDespacho_'.$relacion->getWsDocumento->id.'" ';
                    $opcionesGuias .= 'value="'.$relacion->getWsDocumento->id.'" ';
                    $opcionesGuias .= 'data-numdocumento="'.$relacion->getWsDocumento->documento.'"';
                    $opcionesGuias .= 'data-valordocumento="'.$relacion->getWsDocumento->documento_total.'"';
                    $opcionesGuias .= 'data-numoc="'.$relacion->getWsDocumento->getWsOrdenCompra->numero_orden_compra_mercado_publico.'"';
                    $opcionesGuias .= ' >GD N° '.$relacion->getWsDocumento->documento.' - '.fecha_dmY($relacion->getWsDocumento->fecha_carga).' - $'.formatoMiles($relacion->getWsDocumento->documento_total);
                    $opcionesGuias .= ' | Prov. '.$relacion->getWsDocumento->getWsOrdenCompra->getWsContrato->rut_proveedor;
                    $opcionesGuias .= '</option>'.PHP_EOL;

                }
                
            }

            if (count($documentosBodegaDisponibles) > 0 ) {
                foreach ($documentosBodegaDisponibles as $key => $documentoBodega) {

                    $opcionesGuias .= PHP_EOL.'<option id="guiaDespacho_'.$documentoBodega->id.'"';
                    $opcionesGuias .= 'value="'.$documentoBodega->id.'"';
                    $opcionesGuias .= 'data-numdocumento="'.$documentoBodega->documento.'"';
                    $opcionesGuias .= 'data-valordocumento="'.$documentoBodega->documento_total.'"';
                    $opcionesGuias .= 'data-numoc="'.$documentoBodega->numero_orden_compra_mercado_publico.'"';
                    $opcionesGuias .= ' >GD N° '.$documentoBodega->documento.' - '.fecha_dmY($documentoBodega->fecha_carga).' - $'.formatoMiles($documentoBodega->documento_total);
                    $opcionesGuias .= ' | Prov. '.$documentoBodega->getWsOrdenCompra->getWsContrato->rut_proveedor;
                    $opcionesGuias .= '</option>'.PHP_EOL;
                    
                }
            }

        }

        /**
         * Para obtener las recepciones que tienen solo una firma
         */
        $opcionesFacturasHermes = '';
        $opcionesGuiasHermes = '';
        $totalGuiasHermes = 0;
        if ( ! ($documento->id_tipo_documento == 4 || $documento->id_tipo_documento == 5 || $documento->id_tipo_documento == 10 || $documento->id_tipo_documento == 11) ) {

            /**
             * Se obtienen las recepciones con una sola firma que tienen match con el documento
             */
            if ( $documento->getUnaRelacionRecepcionNoValidada && ! $documento->getRelacionGuiasDespachoHermes() ) {

                $datosRecepcion = array(
                    'numeroDocumento' => $documento->getUnaRelacionRecepcionNoValidada->getHermesDocumento->folio_documento,
                    'valorDocumento'  => formato_entero(formatoMiles( $documento->getUnaRelacionRecepcionNoValidada->getHermesDocumento->documento_total )),
                );

                $datosRecepcion = htmlspecialchars(json_encode($datosRecepcion), ENT_QUOTES, 'UTF-8');
                
                $opcionesFacturasHermes .= PHP_EOL.'<option selected id="recepcionHermes_'.$documento->getUnaRelacionRecepcionNoValidada->getHermesDocumento->folio_documento.'" ';
                $opcionesFacturasHermes .= 'value="'.$documento->getUnaRelacionRecepcionNoValidada->getHermesDocumento->folio_documento.'" ';
                $opcionesFacturasHermes .= 'data-datosrecepcion="'.$datosRecepcion.'" ';
                $opcionesFacturasHermes .= '>'.$documento->getTipoDocumento->sigla.' N° '.$documento->getUnaRelacionRecepcionNoValidada->getHermesDocumento->folio_documento.' '; 
                $opcionesFacturasHermes .= '- '.fecha_dmY($documento->getUnaRelacionRecepcionNoValidada->getHermesDocumento->fecha_ingreso_recepcion).' - ';
                $opcionesFacturasHermes .= '$'.formatoMiles($documento->getUnaRelacionRecepcionNoValidada->getHermesDocumento->documento_total).'';
                $opcionesFacturasHermes .= ' | Prov. '.$documento->getUnaRelacionRecepcionNoValidada->getHermesDocumento->rut_proveedor;
                $opcionesFacturasHermes .= '</option>'.PHP_EOL;

            }

            if ($documento->getUnaRelacionRecepcionNoValidada && $documento->getRelacionGuiasDespachoHermes()) {
                foreach ($documento->getRelacionRecepcionesNoValidadas as $key => $relacion) {

                    $totalGuiasHermes = $totalGuiasHermes + $relacion->getHermesDocumento->documento_total;

                    $opcionesGuiasHermes .= PHP_EOL.'<option selected id="guiaDespachoHermes_'.$relacion->getHermesDocumento->folio_documento.'" ';
                    $opcionesGuiasHermes .= 'value="'.$relacion->getHermesDocumento->folio_documento.'" data-numdocumento="'.$relacion->getHermesDocumento->folio_documento.'" ';
                    $opcionesGuiasHermes .= 'data-valordocumento="'.formato_entero(formatoMiles($relacion->getHermesDocumento->documento_total)).'" ';
                    $opcionesGuiasHermes .= '>GD N° '.$relacion->getHermesDocumento->folio_documento.' - '.fecha_dmY($relacion->getHermesDocumento->fecha_ingreso_recepcion).' ';
                    $opcionesGuiasHermes .= '- $'.formatoMiles($relacion->getHermesDocumento->documento_total).'';
                    $opcionesGuiasHermes .= ' | Prov. '.$relacion->getHermesDocumento->rut_proveedor;
                    $opcionesGuiasHermes .= '</option>'.PHP_EOL;

                }
                
            }


            /**
             * Se obtienen las recepciones con una firma disponibles para match
             */
            $recepcionesHermes = HermesDocumento::where('rut_proveedor', $documento->getProveedor->rut )
                                              ->doesnthave('getRelacion')
                                              ->get();

            foreach($recepcionesHermes as $recepcion) {

                $datosRecepcion = array(
                    'numeroDocumento' => $recepcion->folio_documento,
                    'valorDocumento'  => formato_entero(formatoMiles( $recepcion->documento_total )),
                );

                $datosRecepcion = htmlspecialchars(json_encode($datosRecepcion), ENT_QUOTES, 'UTF-8');

                if ( strpos($recepcion->tipo_documento, 'Factura') !== false ) {

                    $opcionesFacturasHermes .= PHP_EOL.'<option id="recepcionHermes_'.$recepcion->folio_documento.'" ';
                    $opcionesFacturasHermes .= 'value="'.$recepcion->folio_documento.'" ';
                    $opcionesFacturasHermes .= 'data-datosrecepcion="'.$datosRecepcion.'" ';
                    $opcionesFacturasHermes .= '>'.$documento->getTipoDocumento->sigla.' N° '.$recepcion->folio_documento.' '; 
                    $opcionesFacturasHermes .= '- '.fecha_dmY($recepcion->fecha_ingreso_recepcion).' - $'.formatoMiles($recepcion->documento_total).'';
                    $opcionesFacturasHermes .= ' | Prov. '.$recepcion->rut_proveedor;
                    $opcionesFacturasHermes .= '</option>'.PHP_EOL;

                } elseif ( strpos($recepcion->tipo_documento, 'Guia') !== false ) {

                    $opcionesGuiasHermes .= PHP_EOL.'<option id="guiaDespachoHermes_'.$recepcion->folio_documento.'" ';
                    $opcionesGuiasHermes .= 'value="'.$recepcion->folio_documento.'" data-numdocumento="'.$recepcion->folio_documento.'" ';
                    $opcionesGuiasHermes .= 'data-valordocumento="'.formato_entero(formatoMiles($recepcion->documento_total)).'" ';
                    $opcionesGuiasHermes .= '>GD N° '.$recepcion->folio_documento.' - '.fecha_dmY($recepcion->fecha_ingreso_recepcion).' ';
                    $opcionesGuiasHermes .= '- $'.formatoMiles($recepcion->documento_total).'';
                    $opcionesGuiasHermes .= ' | Prov. '.$recepcion->rut_proveedor;
                    $opcionesGuiasHermes .= '</option>'.PHP_EOL;

                }

            }

        }

        /**
         * Obtener opciones de los contratos del proveedor
         */
        $contratos = Contrato::vigenteConSaldoParaProveedor($documento->id_proveedor)->get();

        $optionsContrato = '';
        if ( $contratos->count() > 0 ) {

            foreach ( $contratos as $contrato ) {
                $optionsContrato .= $contrato->getOpcionesParaSelect($documento);
            }

        }

        if ( $documento->getContratoOrdenCompra && $documento->getContratoOrdenCompra->id_contrato != null ) {
            $contratoDoc = Contrato::findOrFail( $documento->getContratoOrdenCompra->id_contrato );
            $optionsContrato .= $contratoDoc->getOpcionesParaSelect($documento);
        }

        /**
         * Obtener opciones de las ordenes de compra del proveedor
         */
        $ordenes = OrdenCompra::where('id_proveedor', $documento->id_proveedor)
                              ->where('saldo_oc', '>', 0)
                              ->get();

        $optionsOc = '';
        if ( $ordenes->count() > 0 ) {
            foreach ( $ordenes as $oc ) {
                $optionsOc .= $oc->getOpcionesParaSelect( $documento );
            }
        }

        if ( $documento->getContratoOrdenCompra && $documento->getContratoOrdenCompra->id_orden_compra != null ) {
            $ocDoc = OrdenCompra::findOrFail( $documento->getContratoOrdenCompra->id_orden_compra );
            $optionsOc .= $ocDoc->getOpcionesParaSelect( $documento );
        }
        
        return view('documento.modal_editar_documento')->with('documento',$documento)
                                                       ->with('tiposDocumento',$tiposDocumento)
                                                       ->with('modalidadesCompra',$modalidadesCompra)
                                                       ->with('tiposAdjudicacion',$tiposAdjudicacion)
                                                       ->with('tiposInforme',$tiposInforme)
                                                       ->with('usuarios',$usuarios)
                                                       ->with('referentesTecnicos',$referentesTecnicos)
                                                       ->with('tiposArchivo',$tiposArchivo)
                                                       ->with('datatable', $datatable)
                                                       ->with('documentosProveedor',$documentosProveedor)
                                                       ->with('docsRechazadosAceptaProv', $docsRechazadosAceptaProv)
                                                       ->with('opciones',$opciones)
                                                       ->with('opcionesGuias',$opcionesGuias)
                                                       ->with('totalGuias',$totalGuias)
                                                       ->with('optionsContrato', $optionsContrato)
                                                       ->with('optionsOc', $optionsOc)
                                                       ->with('opcionesFacturasHermes', $opcionesFacturasHermes)
                                                       ->with('opcionesGuiasHermes', $opcionesGuiasHermes)
                                                       ->with('totalGuiasHermes', $totalGuiasHermes);
    }

    /**
     * Está Función guarda la edicion del documento
     */
    public function postEditarDocumento(Request $request)
    {
        // sleep(5);
        // dd('editando documento',$request->all());

        // Mensajes de los validadores
        $messages = [
            'required'    => 'Debe ingresar el :attribute',
            'numeric'     => 'El :attribute debe solo contener números',
            'max' => 'El :attribute no debe exeder los :max caracteres',
            'min' => 'El :attribute debe tener minimo :min caracteres',
            'date_format'        => 'La :attribute no tiene el formato correcto: día/mes/año',
            'reemplaza.required' => 'Debe seleccionar una factura'
        ];

        if ( $request->input('tipo_documento') == 4 || $request->input('tipo_documento') == 5 || $request->input('tipo_documento') == 10 || $request->input('tipo_documento') == 11 || $request->input('tipo_documento') == 8 ) {
            //validador de los input del formulario
            $validator = \Validator::make($request->all(), [
                'nombre_proveedor'      => 'required|numeric',
                'tipo_documento'        => 'required|numeric',
                'numero_documento'      => 'required',
                'modalidad_compra'      => 'required|numeric',
                'tipo_adjudicacion'     => 'required|numeric',
                'numero_orden_compra'   => 'required',
                'fecha_documento'       => 'required|date_format:"d/m/Y"',
                'tipo_informe'          => 'required|numeric',
                'fecha_recepcion'       => 'required|date_format:"d/m/Y"',
                'valor_total_documento' => 'required',
                'reemplaza'             => 'required',
                'periodo'               => 'required|date_format:"m/Y"',
            ], $messages);

        } else {
            //validador de los input del formulario
            $validator = \Validator::make($request->all(), [
                'nombre_proveedor'      => 'required|numeric',
                'tipo_documento'        => 'required|numeric',
                'numero_documento'      => 'required',
                'modalidad_compra'      => 'required|numeric',
                'tipo_adjudicacion'     => 'required|numeric',
                'numero_orden_compra'   => 'required',
                'fecha_documento'       => 'required|date_format:"d/m/Y"',
                'tipo_informe'          => 'required|numeric',
                'fecha_recepcion'       => 'required|date_format:"d/m/Y"',
                'valor_total_documento' => 'required',
                'periodo'               => 'required|date_format:"m/Y"',
            ], $messages);
        }

        //Si contiene errores se devuelven todos los errores, de lo contrario guarda en la base de datos
        if ( $validator->fails() ) {
            //return redirect()->back()->withInput($request->all)->withErrors($validator);
            return response()->json($validator->errors(),400);
        } else {

            $datos = $this->validarNumOrdenCompraAndNumLicitacion($request);

            /**
             * Comprobar si se ha seleccionado contrato y orden de compra
             * comprobrar si el monto del documento es igual o menor a los saldos
             * hacer el resto de validaciones.
             */
            $datosContrato = array();
            $contrato = null;
            $oc = null;
            $totalDocumento = formato_entero($request->input('valor_total_documento'));
            $documentoValidacionContratoOC = Documento::with([
                                                'getContratoOrdenCompra',
                                                'getContratoOrdenCompra.getContrato',
                                                'getContratoOrdenCompra.getOrdenCompra'
                                            ])->findOrfail($request->input('id_documento'));

            if ( $request->input('orden_compra') != null ) {

                $oc = OrdenCompra::findOrFail( $request->input('orden_compra') );

            }

            if ( $request->input('contrato') != null ) {

                $contrato = Contrato::findOrFail($request->input('contrato'));
            }

            if ( is_object($oc) && is_object($contrato) ) {

                if ( $oc->id_contrato != $contrato->id ) {

                    $datosContrato['mensaje'] = 'La Orden de Compra seleccionada debe pertenecer al Contrato seleccionado.';
                    return response()->json($datosContrato, 400);

                }

            }

            if ( is_object($oc) && ! is_object($contrato) ) {
                if ( $oc->id_contrato != null ) {

                    $datosContrato['mensaje'] = 'Debe seleccionar el Contrato de la Orden de Compra seleccionada.';
                    return response()->json($datosContrato, 400);

                }
            }

            /**
             * Validar que el monto del documento no supere los saldos de la OC ni del Contrato.
             * las validaciones dependen si el contrato u la OC cambio (respecto al que tenia previamente)
             */
            // Validar con Contrato
            if ( is_object($contrato) ) {                    

                if ( $request->input('tipo_documento') == 4 || $request->input('tipo_documento') == 10 ) {
                    // NC y CCC

                    if ( $contrato->saldo_preventivo != null || $contrato->saldo_correctivo != null ) {

                        // if ( $documentoValidacionContratoOC->getContratoOrdenCompra && $contrato->id == $documentoValidacionContratoOC->getContratoOrdenCompra->id_contrato ) {
                        //     $paraEvaluar = $contrato->saldo_preventivo - $documentoValidacionContratoOC->total_documento;
                        // } else {
                        //     $paraEvaluar = $contrato->saldo_preventivo;
                        // }

                        $paraEvaluar = ( $documentoValidacionContratoOC->getContratoOrdenCompra && $contrato->id == $documentoValidacionContratoOC->getContratoOrdenCompra->id_contrato ) ? 
                                        $contrato->saldo_preventivo - $documentoValidacionContratoOC->total_documento : $contrato->saldo_preventivo;

                        if ( $request->input('montoOcupar') == 'preventivo' && $totalDocumento + $paraEvaluar > $contrato->monto_preventivo ) {

                            $datosContrato['mensaje'] = 'El Valor Total más el Saldo Preventivo no puede superar el Monto Preventivo del Contrato.';
                            return response()->json($datosContrato, 400);
                            
                        }

                        $paraEvaluar = ( $documentoValidacionContratoOC->getContratoOrdenCompra && $contrato->id == $documentoValidacionContratoOC->getContratoOrdenCompra->id_contrato ) ? 
                                        $contrato->saldo_correctivo - $documentoValidacionContratoOC->total_documento : $contrato->saldo_correctivo;

                        if ( $request->input('montoOcupar') == 'correctivo' && $totalDocumento + $paraEvaluar > $contrato->monto_correctivo ) {

                            $datosContrato['mensaje'] = 'El Valor Total más el Saldo Correctivo no puede superar el Monto Correctivo del Contrato.';
                            return response()->json($datosContrato, 400);

                        }

                    }

                    $paraEvaluar = ( $documentoValidacionContratoOC->getContratoOrdenCompra && $contrato->id == $documentoValidacionContratoOC->getContratoOrdenCompra->id_contrato ) ? 
                                        $contrato->saldo_contrato - $documentoValidacionContratoOC->total_documento : $contrato->saldo_contrato;

                    if ( $totalDocumento + $paraEvaluar > $contrato->monto_contrato ) {

                        $datosContrato['mensaje'] = 'El Valor Total más el Saldo Contrato no puede superar el Monto del Contrato.';
                        return response()->json($datosContrato, 400);

                    }

                } else {

                    if ( $contrato->saldo_preventivo != null || $contrato->saldo_correctivo != null ) {

                        $paraEvaluar = ( $documentoValidacionContratoOC->getContratoOrdenCompra && $contrato->id == $documentoValidacionContratoOC->getContratoOrdenCompra->id_contrato ) ? 
                                        $contrato->saldo_preventivo + $documentoValidacionContratoOC->total_documento : $contrato->saldo_preventivo;

                        if ( $request->input('montoOcupar') == 'preventivo' && $totalDocumento > $paraEvaluar ) {

                            $datosContrato['mensaje'] = 'El Valor Total no puede superar el Saldo Preventivo del Contrato.';
                            return response()->json($datosContrato, 400);

                        }

                        $paraEvaluar = ( $documentoValidacionContratoOC->getContratoOrdenCompra && $contrato->id == $documentoValidacionContratoOC->getContratoOrdenCompra->id_contrato ) ? 
                                        $contrato->saldo_correctivo + $documentoValidacionContratoOC->total_documento : $contrato->saldo_correctivo;

                        if ( $request->input('montoOcupar') == 'correctivo' && $totalDocumento > $paraEvaluar ) {

                            $datosContrato['mensaje'] = 'El Valor Total no puede superar el Saldo Correctivo del Contrato.';
                            return response()->json($datosContrato, 400);

                        }

                    }

                    $paraEvaluar = ( $documentoValidacionContratoOC->getContratoOrdenCompra && $contrato->id == $documentoValidacionContratoOC->getContratoOrdenCompra->id_contrato ) ? 
                                        $contrato->saldo_contrato + $documentoValidacionContratoOC->total_documento : $contrato->saldo_contrato;

                    if ( $totalDocumento > $paraEvaluar ) {

                        $datosContrato['mensaje'] = 'El Valor Total no puede superar el Saldo Contrato.';
                        return response()->json($datosContrato, 400);

                    }

                }
                
            }

            // Validar OC
            if ( is_object($oc) ) {

                if ( $request->input('tipo_documento') == 4 || $request->input('tipo_documento') == 10 ) {

                    $paraEvaluar = ( $documentoValidacionContratoOC->getContratoOrdenCompra && $oc->id == $documentoValidacionContratoOC->getContratoOrdenCompra->id_orden_compra ) ? 
                                    $oc->saldo_oc - $documentoValidacionContratoOC->total_documento : $oc->saldo_oc;

                    if ( $totalDocumento + $paraEvaluar  > $oc->monto_oc ) {
                        
                        $datosContrato['mensaje'] = 'El Valor Total más el Saldo OC no puede superar el Monto OC.';
                        return response()->json($datosContrato, 400);

                    }

                } else {

                    $paraEvaluar = ( $documentoValidacionContratoOC->getContratoOrdenCompra && $oc->id == $documentoValidacionContratoOC->getContratoOrdenCompra->id_orden_compra ) ? 
                                    $oc->saldo_oc + $documentoValidacionContratoOC->total_documento : $oc->saldo_oc;

                    if ( $totalDocumento > $paraEvaluar ) {
                        
                        $datosContrato['mensaje'] = 'El Valor Total no puede superar el Saldo OC.';
                        return response()->json($datosContrato, 400);

                    }

                }

            }

            // dd('editando documento, pasa validaciones con el contrato',$request->all());

            if ( $datos['estado_documento'] == 'success' ) {
                $mensajeMatch = "";
                
                // Se valida que para el proveedor y el tipo de documento, no se repita el numero documento
                // Se agrega a la validacion los documentos eliminados, ya que pueden ser recuperados.
                $documentoAux = Documento::withTrashed()
                                         ->where('id_proveedor', $request->input('nombre_proveedor'))
                                         ->where('id_tipo_documento', $request->input('tipo_documento'))
                                         ->where('numero_documento', $request->input('numero_documento'))
                                         ->where('id','<>',$request->input('id_documento'))
                                         ->first();

                if ( !is_object($documentoAux) ) {
                    $editDocumento = Documento::with(['getContratoOrdenCompra'])->findOrfail($request->input('id_documento'));
                    $editDocumento->editDocumento($request);

                    if ( $editDocumento->getFoliosSigfe->count() == 0 ) {
                        // Se crean los items presupuestarios
                        if ( $request->input('valor_item') ) {

                            // Se guardaran los id_item que vienen, para eliminar los quitados en la vista
                            $arregloAux = [];
                            foreach ($request->input('valor_item') as $id_item => $valor) {

                                $arregloAux[] = $id_item;
                                $devengoAux = Devengo::where('id_documento',$editDocumento->id)
                                                    ->where('id_item_presupuestario',$id_item)
                                                    ->first();
                                
                                if (is_object($devengoAux)) {

                                    $devengoAux->monto = formato_entero($valor);
                                    $devengoAux->save();

                                } else {
                                    if ( formato_entero($valor) > 0 ) {
                                        $newDevengo = new Devengo();
                                        $newDevengo->id_documento = $editDocumento->id;
                                        $newDevengo->id_item_presupuestario = $id_item;
                                        $newDevengo->monto = formato_entero($valor);
                                        $newDevengo->save();
                                    }
                                }
                                
                            }
                            
                            // Se buscan los devengo que pertenecen a la factura, pero que no estan en el arregloAux
                            $devengosDelete = Devengo::where('id_documento','=',$editDocumento->id)
                                                    ->whereNotIn('id_item_presupuestario',$arregloAux)
                                                    ->get();
                                                    
                            foreach ($devengosDelete as $devengoDelete){
                                $devengoDelete->delete();
                            }
                            
                        } else {
                            // Si no vienen items(devengo es la tabla que guarda los items), se deben borrar los que pertenecen al documento.
                            foreach ($editDocumento->getDevengos as $devengo) {
                                $devengo->delete();
                            }
                        }
                    }
                    
                    // Eliminar los archivos seleccionados para eliminar
                    if ( $request->input('delete_list') ) {
                        foreach ($request->input('delete_list') as $idArchivo) {
                            $archivoDelete = Archivo::findOrfail($idArchivo);
                            $archivoDelete->delete();
                        }
                    }
                    
                    // Se trabaja el archivo subido
                    $archivo = $request->file('archivo');
                    if ( $archivo != null ) {
                        $nombreOriginalArchivo = str_replace('.pdf','',$archivo->getClientOriginalName() );
                        $arreglo_nombre = explode(".", $archivo->getClientOriginalName() );
                        $extension = strtolower( end( $arreglo_nombre ) );

                        if ($extension != 'pdf') {
                            $datos['mensaje_archivo'] = 'Extension incorrecta del archivo';
                            $datos['estado_archivo'] = 'error';
                        } else {
                            // Se trabajan las rutas del archivo, se guarda en el proyecto al final de la funcion
                            $nombre = $nombreOriginalArchivo.'_'.date('Y_m_d_G_i_s');
                            $rutaArchivo = '/documento/'.$editDocumento->id.'/'.$nombre.'.pdf';
                            $pathArchivo = public_path().'/documento/'.$editDocumento->id.'/';

                            $newArchivo = new Archivo();
                            $newArchivo->id_documento = $editDocumento->id;
                            $newArchivo->id_tipo_archivo = $request->input('tipo_archivo');

                            $newArchivo->nombre = $nombre;
                            $newArchivo->nombre_original = $nombreOriginalArchivo;
                            $newArchivo->ubicacion = "documento/".$editDocumento->id."/".$newArchivo->nombre.".pdf";
                            $newArchivo->extension = "pdf";
                            $newArchivo->peso = $archivo->getSize();
                            $newArchivo->save();

                            $datos['mensaje_archivo'] = 'Se ha guardado correctamente el archivo';
                            $datos['estado_archivo'] = 'success';

                            $archivo->move( $pathArchivo , $nombre.'.pdf' ); // Se guarda el archivo
                        }
                        
                    } else {
                        $datos['mensaje_archivo'] = '';
                        $datos['estado_archivo'] = '';
                    }

                    /**
                     * Para conectar documento con recepciones con dos firmas,
                     * en caso que se seleccione una recepcion.
                     * Se valida si el documento tiene el archivo DTE que es necesario
                     */
                    // Se trabaja el match con documentos de recepcion bodega
                    if ( $request->input('doc_disponible_bodega') != null || $request->input('guias_disponible_bodega') != null ) {
                        /**
                         * ¿tiene al menos una relacion con documentos de bodega?
                         * De tener relacion, es porque cuando se creo el documento
                         * tenia los archivos necesarios para poder crear la relacion.
                         */ 

                        if ( $editDocumento->getUnaRelacionRecepcionValidada ) {

                            // Se revisa si el documento estaba conectado con guias de despacho, pueden ser N guias. o solo un documento de bodega
                            if ( $request->input('doc_disponible_bodega') != null && ! $editDocumento->getRelacionGuiasDespacho() ) {

                                // No estaba conectado con guias de despacho, tenia relacion 1 a 1
                                if ( $editDocumento->getUnaRelacionRecepcionValidada->getWsDocumento->id != $request->input('doc_disponible_bodega') ) {

                                    $editDocumento->getUnaRelacionRecepcionValidada->getWsDocumento->id_relacion = null;
                                    $editDocumento->getUnaRelacionRecepcionValidada->getWsDocumento->save();

                                    $editDocumento->id_relacion = null;
                                    $editDocumento->invalidar();
                                    $editDocumento->save();
                                    $editDocumento->actualizaValidacionDocumentosRelacionados();
                                    $editDocumento->getUnaRelacionRecepcionValidada->delete();

                                    // Se crea la relacion
                                    $wsDocumento = WsDocumento::findOrFail( $request->input('doc_disponible_bodega') );
                                    // Se crea la relacion solo si el total_documento_actualizado es igual al monto de la recepcion
                                    if ( $editDocumento->total_documento_actualizado == $wsDocumento->documento_total ) {
                                        $newRelacion = new RelacionDocumentoWsDocumento();
                                        $newRelacion->id_documento = $editDocumento->id;
                                        $newRelacion->id_ws_documento = $wsDocumento->id;
                                        $newRelacion->id_user_relacion = \Auth::user()->id;
                                        $newRelacion->updated_at = NULL;
                                        $newRelacion->save();
                                        // Se paasa el id correspondiente a cada documento para la conexion
                                        $editDocumento->id_relacion = $newRelacion->id;
                                        $editDocumento->validar();
                                        $editDocumento->save();
                                        $editDocumento->actualizaValidacionDocumentosRelacionados();
                                        $wsDocumento->id_relacion = $newRelacion->id;
                                        $wsDocumento->save();
                                    } else {
                                        $mensajeMatch .= "No se crea el Match con Recepción, el documento tiene Asociadas NC o ND que modifican su valor a ";
                                        $mensajeMatch .= ": $ ".formatoMiles($editDocumento->total_documento_actualizado);
                                    }
                                    

                                }

                            } elseif ( $request->input('doc_disponible_bodega') != null && $editDocumento->getRelacionGuiasDespacho() ) {

                                /** 
                                * Estaba conectado con guias de despacho, tenia relacion 1 a N
                                * Se ha conectado con un doc de bodega (no guia de despacho).
                                * Por lo tanto es necesario eliminar todas las relaciones y 
                                * crear solo una nueva relacion
                                */

                                // El documento guarda la ultima relacion
                                $editDocumento->id_relacion = null;
                                $editDocumento->invalidar();
                                $editDocumento->save();
                                $editDocumento->actualizaValidacionDocumentosRelacionados();

                                foreach ( $editDocumento->getRelacionRecepcionesValidadas as $key => $relacion) {
                                    $relacion->getWsDocumento->id_relacion = null;
                                    $relacion->getWsDocumento->save();
                                    $relacion->delete();
                                }

                                // La relaciones solo se crean si el documento tenia los archivos necesarios, por lo tanto no se valida si ahora los tiene
                                $wsDocumento = WsDocumento::findOrFail( $request->input('doc_disponible_bodega') );
                                // Se crea la relacion solo si el total_documento_actualizado es igual al monto de la recepcion
                                if ( $editDocumento->total_documento_actualizado == $wsDocumento->documento_total ) {
                                    $newRelacion = new RelacionDocumentoWsDocumento();
                                    $newRelacion->id_documento = $editDocumento->id;
                                    $newRelacion->id_ws_documento = $wsDocumento->id;
                                    $newRelacion->id_user_relacion = \Auth::user()->id;
                                    $newRelacion->updated_at = NULL;
                                    $newRelacion->save();
                                    // Se paasa el id correspondiente a cada documento para la conexion
                                    $editDocumento->id_relacion = $newRelacion->id;
                                    $editDocumento->validar();
                                    $editDocumento->save();
                                    $editDocumento->actualizaValidacionDocumentosRelacionados();
                                    $wsDocumento->id_relacion = $newRelacion->id;
                                    $wsDocumento->save();
                                } else {
                                    $mensajeMatch .= "No se crea el Match con Recepción, el documento tiene Asociadas NC o ND que modifican su valor a ";
                                    $mensajeMatch .= ": $ ".formatoMiles($editDocumento->total_documento_actualizado);
                                }


                            } elseif ( $request->input('guias_disponible_bodega') != null && ! $editDocumento->getRelacionGuiasDespacho() ) {

                                // No estaba conectado con guias de despacho, tenia relacion 1 a 1
                                // Se elimina la relacion
                                $editDocumento->getUnaRelacionRecepcionValidada->getWsDocumento->id_relacion = null;
                                $editDocumento->getUnaRelacionRecepcionValidada->getWsDocumento->save();

                                $editDocumento->id_relacion = null;
                                $editDocumento->invalidar();
                                $editDocumento->save();
                                $editDocumento->actualizaValidacionDocumentosRelacionados();

                                $editDocumento->getUnaRelacionRecepcionValidada->delete();
                                // Se crean las relaciones
                                foreach ($request->input('guias_disponible_bodega') as $index => $idGuiaBodega) {

                                    $wsDocumento = WsDocumento::findOrFail( $idGuiaBodega );
                                    // Se crea la relacion
                                    $newRelacion = new RelacionDocumentoWsDocumento;
                                    $newRelacion->id_documento = $editDocumento->id;
                                    $newRelacion->id_ws_documento = $wsDocumento->id;
                                    $newRelacion->id_user_relacion = \Auth::user()->id;
                                    $newRelacion->updated_at = NULL;
                                    $newRelacion->save();
                                    // Se paasa el id correspondiente al documento para la conexion
                                    $editDocumento->id_relacion = $newRelacion->id; // Queda con la ultima relacion
                                    $editDocumento->validar();
                                    $editDocumento->save();
                                    $editDocumento->actualizaValidacionDocumentosRelacionados();

                                    $wsDocumento->id_relacion = $newRelacion->id;
                                    $wsDocumento->save();

                                }
                            } elseif ( $request->input('guias_disponible_bodega') != null && $editDocumento->getRelacionGuiasDespacho() )  {

                                /** 
                                * Estaba conectado con guias de despacho, tenia relacion 1 a N
                                * Se ha conectado con una o mas guias de despacho
                                * Por lo tanto es necesario crear las nuevas relaciones y eliminar las que fueron quitadas
                                */

                                // Eliminar las relaciones quitadas en la modal
                                $relacionesEliminar = RelacionDocumentoWsDocumento::where('id_documento', $editDocumento->id)
                                                                                  ->WhereNotIn('id_ws_documento', $request->input('guias_disponible_bodega'))
                                                                                  ->get();

                                foreach ($relacionesEliminar as $relacionEliminar) {
                                    $relacionEliminar->getWsDocumento->id_relacion = null;
                                    $relacionEliminar->getWsDocumento->save();
                                    $relacionEliminar->delete();
                                }

                                // Crear las nuevas relaciones
                                foreach ($request->input('guias_disponible_bodega') as $index => $idGuiaBodega) {

                                    $existeRelacion = RelacionDocumentoWsDocumento::where('id_documento', $editDocumento->id)
                                                                                  ->where('id_ws_documento', $idGuiaBodega)
                                                                                  ->first();
                                    if ( ! is_object( $existeRelacion ) ) {

                                        $wsDocumento = WsDocumento::findOrFail( $idGuiaBodega );
                                        // Se crea la relacion
                                        $newRelacion = new RelacionDocumentoWsDocumento;
                                        $newRelacion->id_documento = $editDocumento->id;
                                        $newRelacion->id_ws_documento = $wsDocumento->id;
                                        $newRelacion->id_user_relacion = \Auth::user()->id;
                                        $newRelacion->updated_at = NULL;
                                        $newRelacion->save();
                                        // Se pasa el id correspondiente al documento para la conexion
                                        $editDocumento->id_relacion = $newRelacion->id; // Queda con la ultima relacion
                                        $editDocumento->validar();
                                        $editDocumento->save();
                                        $editDocumento->actualizaValidacionDocumentosRelacionados();

                                        $wsDocumento->id_relacion = $newRelacion->id;
                                        $wsDocumento->save();

                                    }

                                }

                            }
                            
                        } else {

                            /**
                             * El documento no tenia relacion anteriormente.
                             * Se crean las relaciones si el documento tiene los archivos necesarios
                             */
                            if ( $editDocumento->tieneLosArchivosNecesarios() ) {

                                if ( $request->input('doc_disponible_bodega') != null ) {

                                    // Se crea la relacion
                                    $wsDocumento = WsDocumento::findOrFail( $request->input('doc_disponible_bodega') );
                                    $newRelacion = new RelacionDocumentoWsDocumento();
                                    $newRelacion->id_documento = $editDocumento->id;
                                    $newRelacion->id_ws_documento = $wsDocumento->id;
                                    $newRelacion->id_user_relacion = \Auth::user()->id;
                                    $newRelacion->updated_at = NULL;
                                    $newRelacion->save();
                                    // Se paasa el id correspondiente a cada documento para la conexion
                                    $editDocumento->id_relacion = $newRelacion->id;
                                    $editDocumento->validar();
                                    $editDocumento->save();
                                    $editDocumento->actualizaValidacionDocumentosRelacionados();

                                    $wsDocumento->id_relacion = $newRelacion->id;
                                    $wsDocumento->save();

                                }

                                if ( $request->input('guias_disponible_bodega') != null ) {

                                    foreach ($request->input('guias_disponible_bodega') as $index => $idGuiaBodega) {

                                        $wsDocumento = WsDocumento::findOrFail( $idGuiaBodega );
                                        // Se crea la relacion
                                        $newRelacion = new RelacionDocumentoWsDocumento();
                                        $newRelacion->id_documento = $editDocumento->id;
                                        $newRelacion->id_ws_documento = $wsDocumento->id;
                                        $newRelacion->id_user_relacion = \Auth::user()->id;
                                        $newRelacion->updated_at = NULL;
                                        $newRelacion->save();
                                        // Se paasa el id correspondiente al documento para la conexion
                                        $editDocumento->id_relacion = $newRelacion->id; // Queda con la ultima relacion
                                        $editDocumento->validar();
                                        $editDocumento->save();
                                        $editDocumento->actualizaValidacionDocumentosRelacionados();
                                        $wsDocumento->id_relacion = $newRelacion->id;
                                        $wsDocumento->save();

                                    }

                                }
                                
                            } else {
                                $mensajeMatch .= "No se crea el Match con Recepción, el documento no tiene el archivo Factura.";
                            }
                            
                        }
                    } else {
                        // Se verifica si existia al menos una relacion en el documento, de ser así se eliminan todas.
                        if ( $editDocumento->getUnaRelacionRecepcionValidada ) {

                            foreach ($editDocumento->getRelacionRecepcionesValidadas as $key => $relacion) {

                                $relacion->getWsDocumento->id_relacion = null;
                                $relacion->getWsDocumento->save();
                                $relacion->delete();

                            }

                            $editDocumento->id_relacion = null;
                            $editDocumento->invalidar();
                            $editDocumento->save();
                            $editDocumento->actualizaValidacionDocumentosRelacionados();

                        }
                    }

                    /**
                     * Para conectar documento con recepciones con una firma,
                     * en caso que se seleccione una recepcion.
                     */
                    $editDocumento->fresh();
                    if ( $editDocumento->getUnaRelacionRecepcionValidada == null ) {
                        if ( $request->input('factura_recepcion_hermes') != null || $request->input('guias_disponible_hermes') != null ) {
                            
                            if ( $editDocumento->getUnaRelacionRecepcionNoValidada ) {
                                // Tiene al menos una relacion con hermes.
                                foreach ( $editDocumento->getRelacionRecepcionesNoValidadas as $relacionHermes ) {
                                    $relacionHermes->delete(); // se eliminan las relaciones
                                }
                                // $editDocumento->invalidar();

                            }

                            // Se crean las relaciones
                            if ( $request->input('factura_recepcion_hermes') != null ) {
                                relacionRecepcionesNoValidadas::validarGuardarRelacionesHermes( $request->input('factura_recepcion_hermes'), $editDocumento);
                            }

                            if ($request->input('guias_disponible_hermes') != null ) {
                                foreach ( $request->input('guias_disponible_hermes') as $index => $folioDoc ) {
                                    relacionRecepcionesNoValidadas::validarGuardarRelacionesHermes($folioDoc, $editDocumento);
                                } 
                            }

                        } else {
                             /**
                             * hay que invalidar el documento, porque no tiene recepciones validadas
                             * y se quitaran las relaciones con las recepciones de una firma
                             */
                            // if ( $editDocumento->getRelacionRecepcionesNoValidadas->count() > 0 ) {
                            //     $editDocumento->invalidar();
                            // }
                            // Eliminar las relaciones con hermes,
                            foreach ( $editDocumento->getRelacionRecepcionesNoValidadas as $relacionHermes ) {
                                $relacionHermes->delete(); // se eliminan las relaciones
                            }                            
                            
                        }
                    } else {
                        // Deberian eliminarse las relaciones con hermes
                        foreach ( $editDocumento->getRelacionRecepcionesNoValidadas as $relacionHermes ) {
                            $relacionHermes->delete(); // se eliminan las relaciones
                        }
                        // y no invalidar el doc. ya que debe validarse con las recepciones confirmadas, previamente
                    }
                    
                    /**
                     * Hasta acá llega el match con hermes
                     */


                    $items = '';
                    foreach($editDocumento->getDevengos as $devengo) {
                        $items .= $devengo->getItemPresupuestario->codigo();
                    }

                    $mensajeDocumento = '';
                    if ( $request->input('datatable') == 'pendientes_validacion' ) {
                        $mensajeDocumento .= 'Validación exitosa del documento.';
                    } elseif ( $request->input('datatable') == 'validados' ){
                        $mensajeDocumento .= 'Se ha invalidado el documento exitosamente.';
                    } else {
                        $mensajeDocumento .= 'Edición exitosa del documento.';
                    }

                    // Para index de pendientes validación
                    $diferenciaDias = '<i class="fas fa-clipboard-check fa-lg fa-bounce" style="color:'. $editDocumento->getColorPorDiasCuadratura() .' !important"></i>';
                    $diferenciaDias .= '&nbsp;&nbsp;'.diferencia_dias_cuadratura($editDocumento->fecha_ingreso,date('Y-m-d'));

                    $tieneRelacion = 0;
                    if ($editDocumento->id_relacion != null) {
                        $tieneRelacion = 1;

                        if ( $editDocumento->id_user_problema != null ) {
                            // Se quita el problema del documento
                            $editDocumento->observacion_problema = $editDocumento->observacion_problema.PHP_EOL.Auth::user()->name.': Quita el documento del listado "Con Problemas"';
                            $editDocumento->id_user_problema = null;
                            $editDocumento->id_user_quita_problema = Auth::user()->id;
                            $editDocumento->save();
                            $mensajeDocumento .= '<br> Se ha retirado el documentos del listado de "Con Problemas"';
                        }
                    }

                    // Se verifica si el documento tiene un posible match directo con recepcion
                    $tieneMatch = 'No';
                    if ( $editDocumento->tieneLosArchivosNecesarios() ) {

                        // Buscar si tiene una Recepción con la cual hacer match
                        $match = WsDocumento::where('id_relacion', null)
                                            ->where('tipo_documento', 'LIKE', $editDocumento->getTipoDocumento->nombre)
                                            ->where('documento', $editDocumento->numero_documento)
                                            ->where('documento_total', $editDocumento->total_documento_actualizado)
                                            ->whereHas('getWsOrdenCompra.getWsContrato', function ($query) use ($editDocumento) {
                                                $query->where('rut_proveedor', $editDocumento->getProveedor->rut);
                                            })
                                            ->first();
                        if ( is_object($match) ) {
                            $tieneMatch = 'Si';
                        }

                    }

                    /**
                     * Conectar con contrato, oc y modificar los montos
                     */
                    $editDocumento->unirConContratoOrdenCompra($contrato, $oc, $request, $documentoValidacionContratoOC);
                    
                    $datos = array(
                        'mensaje_documento' => $mensajeDocumento,
                        'estado_documento'  => 'success',
                        'id'                => $editDocumento->id,
                        'rutProveedor'      => $editDocumento->getProveedor->formatRut(),
                        'nombreProveedor'   => $editDocumento->getProveedor->nombre,
                        'tipoDocumento'     => $editDocumento->getTipoDocumento->nombre,
                        'numeroDocumento'   => $editDocumento->numero_documento,
                        'documentoCompra'   => $editDocumento->documento_compra,
                        'fechaRecepcion'    => fecha_dmY($editDocumento->fecha_recepcion),
                        'fechaDocumento'    => fecha_dmY($editDocumento->fecha_documento),
                        'items'             => $items,
                        'totalDocumento'    => '$ '.formatoMiles($editDocumento->total_documento),
                        'tipoInforme'       => $editDocumento->getTipoInforme->nombre,
                        'modalidadCompra'   => $editDocumento->getModalidadCompra->nombre,
                        'diferenciaDias'    => $diferenciaDias,
                        'fechaIngreso'      => fecha_dmY($editDocumento->fecha_ingreso),
                        // para saber si tiene realacion, por lo tanto esta cuadrara
                        'tieneRelacion'     => $tieneRelacion,
                        'estadoDocumento'   => "<strong>".$editDocumento->getEstadoDocumento()."</strong>",
                        //Para saber si tiene match directo
                        'posibleMatch'      => $tieneMatch,
                        'mensajeMatch'      => $mensajeMatch,
                    );

                    
                    if ( $request->input('datatable') == 'validados' ) {
                        $editDocumento->invalidar();
                    }
                    $editDocumento->actualizaValidacionDocumentosRelacionados();
                } else {
                    $datos = array(
                        'mensaje_documento' => 'N° Documento repetido para el Proveedor y Tipo Documento seleccionados',
                        'estado_documento'  => 'error',
                    );

                    if ( $documentoAux->trashed() ) {
                        $datos['mensaje_documento'] = 'N° Documento repetido para el Proveedor y Tipo Documento seleccionados.<br><br> El Documento se encuentra eliminado.<br> Observaciones : <br>'.$documentoAux->observacion;
                    }
                }

            }

        }

        // Para filtros en la modal de editar el documento, segun esto se actualiza la vista principal(index)
        $datos['datatable'] = $request->input('datatable');
        return response()->json($datos,200);
    }

    public function getPredeterminadoModalidadCompra( $idModalidadCompra )
    {
        $modalidadCompra = ModalidadCompra::find($idModalidadCompra);

        if ( !is_object($modalidadCompra) ) {
            $datos = array(
                'mensaje' => 'No se ha encontrado la Modalidad de Compra',
                'estado' => 'error',
            );

        } else {

            $html = '<label for="numero_orden_compra" class="label-form">N° Orden de Compra <span class="span-label">*</span></label>';
            
            if ( $modalidadCompra->predeterminado == null) {

                $html .= '<input type="text" class="form-control" id="numero_orden_compra" name="numero_orden_compra" required >';
                
            } else {

                $html .= '<select name="numero_orden_compra" id="numero_orden_compra" class="form-control select2" required>';
                $html .= '<option value="">Seleccione</option>';

                foreach (explode( ';', $modalidadCompra->predeterminado ) as $predeterminado) {
                    $html .= '<option value="'.$predeterminado.'">'. $predeterminado .'</option>';
                }

                $html .= '</select>';
                $modalidadCompra->predeterminado = true;

            }

            $datos = array(
                'mensaje' => $html,
                'predeterminado' => $modalidadCompra->predeterminado,
                'estado' => 'success',
            );

        }

        return response()->json($datos,200);
    }

    /**
     * Funcion que valida el numero de orden de compra y el numero licitacion, segun los formatos establecidos.
     * Retorna el array datos que se envia por json a las vistas
    */
    public function validarNumOrdenCompraAndNumLicitacion($request)
    {
        $datos = array(
            'mensaje_documento' => 'Todo bien',
            'estado_documento' => 'success',
        );
        // Validar N°Documento Compra o N° Orden Compra y N° Licitación
        if ( $request->input('modalidad_compra') == 1 && substr( $request->input('numero_orden_compra'), 0, 5) != "1641-") {
            $datos = array(
                'mensaje_documento' => 'El "N° Orden de Compra" debe iniciar con "1641-"',
                'estado_documento' => 'error',
            );
        }

        if ( $request->input('numero_licitacion') != '' && substr( $request->input('numero_licitacion'), 0, 5) != "1641-") {
            $datos = array(
                'mensaje_documento' => 'El "N° Licitación" debe iniciar con "1641-"',
                'estado_documento' => 'error',
            );
        }

        return $datos;
    }

    /**
     * Funcion que muestra la modal para eliminar el documento
     */
    public function getModalEliminarDocumento($idDocumento)
    {
        $documento = Documento::with([
                                    'getProveedor', 'getTipoDocumento', 'getDocumentoRelacionado', 'getModalidadCompra',
                                    'getModalidadCompra', 'getTipoAdjudicacion', 'getTipoInforme', 'getResponsable',
                                    'getDigitador', 'getDevengador', 'getArchivos', 'getArchivos.getTipoArchivo',
                                    'getDocumentosRelacionados', 'getDocumentosRelacionados.getTipoDocumento',
                                    'getDocumentosRelacionados.getArchivos', 'getDocumentosRelacionados.getArchivos.getTipoArchivo',
                                    'getDevengos', 'getDevengos.getItemPresupuestario'
                                ])
                              ->findOrFail($idDocumento);
        return view('documento.modal_eliminar_documento')->with('documento',$documento);
    }

    public function postEliminarDocumento(Request $request)
    {
        $deleteDocumento = Documento::find($request->input('id_documento'));
        if ( !is_object($deleteDocumento) ) {
            $datos = array(
                'mensaje_documento' => 'No se encuentra el Documento en la Base de Datos',
                'estado_documento' => 'error',
            );
        } else {

            // No se eliminan todos los elementos relacionados, porque el documento puede ser recuperado

            // Eliminar devengos asociados
            // foreach ($deleteDocumento->getDevengos as $devengo) {
            //     $devengo->delete();
            // }

            // Eliminar archivos asociados
            // foreach ($deleteDocumento->getArchivos as $archivo) {
            //     $archivo->delete();
            // }

            $deleteDocumento->actualizarTotalDocumentoActualizadoDelDocumentoRelacionado();
            // $deleteDocumento->id_user_deleted = \Auth::user()->id;
            // $deleteDocumento->save();
            $deleteDocumento->delete();

            /**
             * Si se eliminar una nc,nd,ccc o ccd, 
             * verificar si se puede validar el documento relacionado
            */
            if ( $deleteDocumento->id_tipo_documento == 4 || $deleteDocumento->id_tipo_documento == 10 || $deleteDocumento->id_tipo_documento == 5 || $deleteDocumento->id_tipo_documento == 11 ) {
                $doc = Documento::find($deleteDocumento->id_relacionado);
                if ( is_object($doc) ) {
                    $doc->validar();
                    $doc->actualizaValidacionDocumentosRelacionados();
                }
            }

            $datos = array(
                'mensaje_documento' => 'Se ha Eliminado correctamente el documento.',
                'estado_documento' => 'success',
            );
        }

        return response()->json($datos,200);
    }

    public function dataTablePendientesValidacion(Request $request)
    {
        // dd($request->all());

        set_time_limit ( 0 );
        ini_set('max_execution_time', 0);

        $search = $request->get('search')['value'];
        $order_column = $request->get('order')[0]['column'];
        $order_dir = $request->get('order')[0]['dir'];

        $formJSON = json_decode($request->get('form'));
        $form = construyeFormulario($formJSON);
        
        $fechaInicio = fecha_Y_m_d($form->filtro_fecha_inicio);
        $fechaTermino = fecha_Y_m_d($form->filtro_fecha_termino);

        $documentos = Documento::pendientesValidacion();

        $totalDocumentosPorValidar = Documento::where('fecha_ingreso','>=','2019-06-22 00:00:00')
                                              ->where('validado', null)
                                              ->where('id_user_problema', null);

        if ( $form->filtro_numero_documento == null ) {
            // Se filtra por las fechas

            if ( $form->filtro_fecha_inicio != null ) {

                if ( $form->filtro_fecha_a_utilizar == 'Ingreso' ) {
                    $documentos = $documentos->where('fecha_ingreso','>=',$fechaInicio.' 00:00:00');
                } elseif ( $form->filtro_fecha_a_utilizar == 'Recepción' ) {
                    $documentos = $documentos->where('fecha_recepcion','>=',$fechaInicio);
                }

            }

            if ( $form->filtro_fecha_termino != null ) {
                if ( $form->filtro_fecha_a_utilizar == 'Ingreso' ) {
                    $documentos = $documentos->where('fecha_ingreso','<=',$fechaTermino.' 23:59:59');
                } elseif ( $form->filtro_fecha_a_utilizar == 'Recepción' ) {
                    $documentos = $documentos->where('fecha_recepcion','<=',$fechaTermino);
                }

                $totalDocumentosPorValidar = $totalDocumentosPorValidar->where('fecha_ingreso','<=',$fechaTermino.' 23:59:59');
            }

        } else {
            // no se consideran las fechas, y se busca el numero del documento
            $documentos = $documentos->where('numero_documento', trim($form->filtro_numero_documento) );
        }

        if ( isset($form->filtro_proveedor) ) {
            $documentos = $documentos->WhereIn('id_proveedor' ,$form->filtro_proveedor);
            $totalDocumentosPorValidar = $totalDocumentosPorValidar->WhereIn('id_proveedor' ,$form->filtro_proveedor);
        }

        if ( isset($form->filtro_tipo_documento) ) {
            $documentos = $documentos->WhereIn('id_tipo_documento', $form->filtro_tipo_documento);
            $totalDocumentosPorValidar = $totalDocumentosPorValidar->WhereIn('id_tipo_documento', $form->filtro_tipo_documento);
        } else {
            
            // if ( ! \Entrust::hasRole(['propietario','administrador']) ) {
            if ( ! \Entrust::hasRole(['propietario']) ) {

                // $documentos = $documentos->whereIn('id_tipo_documento', [1,2,3,6,7,9]);
                // $totalDocumentosPorValidar = $totalDocumentosPorValidar->whereIn('id_tipo_documento', [1,2,3,6,7,9]);

                // Se quitan las boletas
                $documentos = $documentos->whereIn('id_tipo_documento', [1,2,6,7,9]);
                $totalDocumentosPorValidar = $totalDocumentosPorValidar->whereIn('id_tipo_documento', [1,2,6,7,9]);

            }

        }

        if ( isset($form->filtro_modalidad_compra) ) {
            $documentos = $documentos->WhereIn('id_modalidad_compra', $form->filtro_modalidad_compra);
            $totalDocumentosPorValidar = $totalDocumentosPorValidar->WhereIn('id_modalidad_compra', $form->filtro_modalidad_compra);
        }

        if ( isset($form->filtro_tipo_adjudicacion) ) {
            $documentos = $documentos->WhereIn('id_tipo_adjudicacion', $form->filtro_tipo_adjudicacion);
            $totalDocumentosPorValidar = $totalDocumentosPorValidar->WhereIn('id_tipo_adjudicacion', $form->filtro_tipo_adjudicacion);
        }

        if ( isset($form->filtro_tipo_informe) ) {
            $documentos = $documentos->WhereIn('id_tipo_informe', $form->filtro_tipo_informe);
            $totalDocumentosPorValidar = $totalDocumentosPorValidar->WhereIn('id_tipo_informe', $form->filtro_tipo_informe);
        }

        if ( isset($form->filtro_responsable) ) {
            $documentos = $documentos->WhereIn('id_responsable', $form->filtro_responsable);
            $totalDocumentosPorValidar = $totalDocumentosPorValidar->WhereIn('id_responsable', $form->filtro_responsable);
        }

        if ( isset($form->filtro_referente_tecnico) ) {
            $documentos = $documentos->WhereIn('id_referente_tecnico', $form->filtro_referente_tecnico);
            $totalDocumentosPorValidar = $totalDocumentosPorValidar->WhereIn('id_referente_tecnico', $form->filtro_referente_tecnico);
        }

        if ( isset($form->filtro_digitador) ) {
            $documentos = $documentos->WhereIn('id_digitador', $form->filtro_digitador);
            $totalDocumentosPorValidar = $totalDocumentosPorValidar->WhereIn('id_digitador', $form->filtro_digitador);
        }

        if ( isset($form->filtro_item_presupuestario) ) {
            $documentos = $documentos->whereHas('getDevengos',function ($query) use ($form) {
                $query->WhereIn('id_item_presupuestario', $form->filtro_item_presupuestario);
            });
            $totalDocumentosPorValidar = $totalDocumentosPorValidar->whereHas('getDevengos',function ($query) use ($form) {
                $query->WhereIn('id_item_presupuestario', $form->filtro_item_presupuestario);
            });
        }

        if ( $form->filtro_numero_documento_compra != null ) {
            $documentos = $documentos->where('documento_compra', trim($form->filtro_numero_documento_compra) );
            $totalDocumentosPorValidar = $totalDocumentosPorValidar->where('documento_compra', trim($form->filtro_numero_documento_compra) );
        }

        $documentos = $documentos->get();

        $totalDocumentosPorValidar = $totalDocumentosPorValidar->get()->count();

        $data = [];

        if ( $request->get('length') != -1 ) {
            $hasta = $request->get('start') + $request->get('length');
        } else {
            // Se quieren ver todos los datos
            $hasta = $request->get('start') + $documentos->count();
        }
        
        for( $i = $request->get('start') ; $i < $hasta ; $i++ ) {
            if ( isset($documentos[$i]) ) {
                $data[] = $documentos[$i]->getDatosPendientesValidacion();
            }
        }

        // Se realiza la busqueda y se ordenan los datos
        if ( $documentos->count() > 0 ) {

            foreach ($data as $clave => $fila) {

                if ( $search != '' ) {
                    if ( in_array_r($search,$fila) ) {
                        // dd("lo encuentra",$fila,$search);
                        $columna[$clave] = $fila[$order_column];
                    } else {
                        // dd("NO lo encuentra",$fila,$search);
                        unset( $data[$clave] ); // quita el elemento del data
                    }
                } else {
                    $columna[$clave] = $fila[$order_column];
                }
                
            }

            
            // Se ordenan los datos, segun la columna seleccionada
            if ( $order_dir == 'asc' ) {
                array_multisort($columna, SORT_ASC, $data);
            } elseif ( $order_dir == 'desc' ) {
                array_multisort($columna, SORT_DESC, $data);
            }

        }
            

        $respuesta = array(
            "draw" => $request->get('draw'),
            "recordsTotal" => count($documentos),
            "recordsFiltered"=> count($documentos),
            "data" => $data,
            "totalDocumentosPorValidar" => formatoMiles($totalDocumentosPorValidar),
        );

        return json_encode($respuesta);

    }

    /**
     * Funcion para mostar documentos pendientes de validacion.
     * Esta opcion se añade para los documentos desde el 22-junio-2019.
     * Es necesario validar los documentos para pasar a "por devengar",
     * esto quiere decir que el documento está cuadrado
     */
    public function geDocumentosPendientesValidacion($filtroHome = null)
    {

        if ( !\Entrust::can(['ver-documentos-pendiente-cuadratura','cuadrar-documento','editar-documento','eliminar-documento','match-documentos-con-recepcion'])) {
            return \Redirect::to('home');
        }


        if ( \Entrust::hasRole(['propietario','administrador']) ) {
            $tiposDocumento = TipoDocumento::all();
        } else {
            $tiposDocumento = TipoDocumento::whereIn('id',[1,2,3,6,7,9])->get(); 
        }

        $modalidadesCompra = ModalidadCompra::all();
        $tiposAdjudicacion = TipoAdjudicacion::all();
        $tiposInforme = TipoInforme::all();
        $usuariosResponsables = User::where('responsable', 1)->get();
        $referentesTecnicos = ReferenteTecnico::all();
        $usuariosDigitadores = User::has('getDocumentos')->get();
        $proveedores = Proveedor::all();
        $itemsPresupuestarios = ItemPresupuestario::all();

        // Select para Fechas a utilizar en el filtro
        $fechaUtilizar1 = new \stdClass;
        $fechaUtilizar1->opcion = 'Ingreso';
        $fechaUtilizar2 = new \stdClass;
        $fechaUtilizar2->opcion = 'Recepción';

        $fechasUtilizar[] = $fechaUtilizar1;
        $fechasUtilizar[] = $fechaUtilizar2;
        
        return view('documento.index_pendiente_validacion')->with('tiposDocumento',$tiposDocumento)
                                                           ->with('modalidadesCompra',$modalidadesCompra)
                                                           ->with('tiposAdjudicacion',$tiposAdjudicacion)
                                                           ->with('tiposInforme',$tiposInforme)
                                                           ->with('usuariosResponsables',$usuariosResponsables)
                                                           ->with('referentesTecnicos',$referentesTecnicos)
                                                           ->with('usuariosDigitadores',$usuariosDigitadores)
                                                           ->with('proveedores',$proveedores)
                                                           ->with('itemsPresupuestarios',$itemsPresupuestarios)
                                                           ->with('fechasUtilizar',$fechasUtilizar)
                                                           ->with('filtroHome', $filtroHome);
    }

    public function postPendienteValidacionExcel(Request $request)
    {
        set_time_limit ( 0 );
        ini_set('max_execution_time', 0);
        
        \Excel::create('SIFCON - Documentos Pendientes Validación '.date('d-m-Y').'', function($excel) use($request) {

            $fechaInicio = fecha_Y_m_d($request->input('filtro_fecha_inicio'));
            $fechaTermino = fecha_Y_m_d($request->input('filtro_fecha_termino'));
            // dd($fechaInicio,$fechaTermino);
            // dd($fechaInicio,$fechaTermino,$request->input('filtro_fecha_inicio'));

            $documentos = Documento::pendientesValidacion();

            if ( $request->input('filtro_fecha_inicio') != null && $request->input('filtro_numero_documento') == null) {

                if ( $request->input('filtro_fecha_a_utilizar') == 'Ingreso' ) {
                    $documentos = $documentos->where('fecha_ingreso','>=',$fechaInicio.' 00:00:00');
                } elseif ( $request->input('filtro_fecha_a_utilizar') == 'Recepción' ) {
                    $documentos = $documentos->where('fecha_recepcion','>=',$fechaInicio);
                }

            }

            if ( $request->input('filtro_fecha_termino') != null && $request->input('filtro_numero_documento') == null ) {
                if ( $request->input('filtro_fecha_a_utilizar') == 'Ingreso' ) {
                    $documentos = $documentos->where('fecha_ingreso','<=',$fechaTermino.' 23:59:59');
                } elseif ( $request->input('filtro_fecha_a_utilizar') == 'Recepción' ) {
                    $documentos = $documentos->where('fecha_recepcion','<=',$fechaTermino);
                }
            }

            if ( $request->input('filtro_proveedor') != null ) {
                $documentos = $documentos->WhereIn('id_proveedor',$request->input('filtro_proveedor'));
            }

            if ( $request->input('filtro_tipo_documento') != null ) {
                $documentos = $documentos->WhereIn('id_tipo_documento',$request->input('filtro_tipo_documento'));
            } else {
                if ( ! \Entrust::hasRole(['propietario']) ) {
                    // $documentos = $documentos->whereIn('id_tipo_documento', [1,2,3,6,7,9]);

                    // Se quitan las boletas
                    $documentos = $documentos->whereIn('id_tipo_documento', [1,2,6,7,9]);
                }
            }

            if ( $request->input('filtro_modalidad_compra') != null ) {
                $documentos = $documentos->WhereIn('id_modalidad_compra',$request->input('filtro_modalidad_compra'));
            }

            if ( $request->input('filtro_tipo_adjudicacion') != null ) {
                $documentos = $documentos->WhereIn('id_tipo_adjudicacion',$request->input('filtro_tipo_adjudicacion'));
            }

            if ( $request->input('filtro_tipo_informe') != null ) {
                $documentos = $documentos->WhereIn('id_tipo_informe',$request->input('filtro_tipo_informe'));
            }

            if ( $request->input('filtro_responsable') != null ) {
                $documentos = $documentos->WhereIn('id_responsable',$request->input('filtro_responsable'));
            }

            if ( $request->input('filtro_referente_tecnico') != null ) {
                $documentos = $documentos->WhereIn('id_referente_tecnico',$request->input('filtro_referente_tecnico'));
            }

            if ( $request->input('filtro_digitador') != null ) {
                $documentos = $documentos->WhereIn('id_digitador',$request->input('filtro_digitador'));
            }

            if ( $request->input('filtro_item_presupuestario') != null) {
                $documentos = $documentos->whereHas('getDevengos',function ($query) use ($request) {
                    $query->WhereIn('id_item_presupuestario',$request->input('filtro_item_presupuestario'));
                });
            }

            if ( $request->input('filtro_numero_documento') != null ) {
                $documentos = $documentos->where('numero_documento', trim($request->input('filtro_numero_documento')) );
            }

            if ( $request->input('filtro_numero_documento_compra') != null ) {
                $documentos = $documentos->where('documento_compra', trim($request->input('filtro_numero_documento_compra')) );
            }

            $documentos = $documentos->get();

            $excel->sheet('Documentos', function($sheet) use($documentos) {
            
                $sheet->row(1, [
                    'Estado', 
                    // 'Responsable',
                    'Factoring',
                    'Chile-Compra',
                    'M/Dev',
                    'Proveedor',
                    'RUT',
                    'Tipo Dcto.',
                    'N° Dcto.',
                    'F/Doc.',
                    'M/Doc.',
                    'Fecha RD',
                    'Mes RD',
                    'Item',
                    'ID SIGFE',
                    'Fecha ID',
                    'Devengado',
                    'Mecanismo de Adjudicación',
                    'Observación',
                    'Detalle',
                    'URL Factura'
                ]);

                $totalDevengadoExcel = 0;
                $cuentaLineas = 1;
                foreach ( $documentos as $indexAux => $documento ) {
                    $items = '';
                    $totalDevengadoPorDocumento = 0;

                    foreach ( $documento->getDevengos as $index => $devengo) {
                        $cuentaLineas++;
                        $items = $devengo->getItemPresupuestario->codigo();
                        $totalDevengadoPorDocumento = $devengo->monto;
                        // para columna "devengado"
                        $devengado = '';
                        if ($documento->id_tipo_documento == 4 || $documento->id_tipo_documento == 10) {
                            $devengado .= '-'.$totalDevengadoPorDocumento;
                            $totalDevengadoExcel = $totalDevengadoExcel - $totalDevengadoPorDocumento;
                        } else {
                            $devengado .= $totalDevengadoPorDocumento;
                            $totalDevengadoExcel = $totalDevengadoExcel + $totalDevengadoPorDocumento;
                        }

                        // para columna factoring
                        $factoring = '';
                        if ($documento->getProveedorFactoring) {
                            $factoring .= $documento->getProveedorFactoring->nombre.' '.$documento->getProveedorFactoring->rut;
                        }

                        $mesSigfe = '';
                        $fechaSigfe = '';
                        $folioSigfe = '';
                        $detalleSigfe = '';
                        if ($documento->getFoliosSigfe->count() == 1) {
                            $mesSigfe = explode("-",$documento->getFoliosSigfe->first()->fecha_sigfe)[1];
                            $fechaSigfe = fecha_dmY($documento->getFoliosSigfe->first()->fecha_sigfe);
                            $folioSigfe = $documento->getFoliosSigfe->first()->id_sigfe;
                            $detalleSigfe = $documento->getFoliosSigfe->first()->detalle_sigfe;
                        }

                        if ($documento->getFoliosSigfe->count() > 1) {
                            $mesSigfe = 'VARIOS FOLIOS';
                            $fechaSigfe = 'VARIOS FOLIOS';
                            $folioSigfe = 'VARIOS FOLIOS';
                            $detalleSigfe = 'VARIOS FOLIOS';
                        }

                        $sheet->row($cuentaLineas, [
                            $documento->getEstadoDocumento('excel'),
                            // $documento->getResponsable->name,
                            $factoring,
                            $documento->documento_compra,
                            $mesSigfe, // mes de la fecha sigfe
                            $documento->getProveedor->nombre,
                            $documento->getProveedor->rut,
                            $documento->getTipoDocumento->nombre,
                            $documento->numero_documento,
                            fecha_dmY($documento->fecha_documento),
                            explode("-",$documento->fecha_documento)[1], // mes de la fecha del documento
                            fecha_dmY($documento->fecha_recepcion),
                            explode("-",$documento->fecha_recepcion)[1], // mes de la fecha de recepcion del documento
                            $items,
                            $folioSigfe,
                            $fechaSigfe,
                            $devengado,
                            $documento->getTipoAdjudicacion->nombre,
                            $documento->observacion,
                            $detalleSigfe,
                            $documento->getUrlFactura()
                        ]);
                    }
                }


                $sheet->row( $cuentaLineas+1 , [
                    '', 
                    // '', 
                    '',
                    '',
                    '', 
                    '',
                    '',
                    '',
                    '',
                    '',
                    '', 
                    '',
                    '',
                    '',
                    '',
                    'TOTAL',
                    $totalDevengadoExcel,
                    '',
                    '',
                    '',
                    ''
                ]);

        });

        })->export('xlsx');
    }

    /**
     * Muestra modal para pasar documento al listado de documentos con problemas
     */
    public function getModalProblemaDocumento($id)
    {
        $documento = Documento::with(['getProveedor','getTipoDocumento'])
                              ->findOrFail($id);

        $motivos = MotivoProblema::all();

        return view('documento.modal_problema',compact('documento', 'motivos'));
    }

    /**
     * Guarda el problema del documento y lo saca del listado.
     */
    public function postProblemaDocumento(Request $request)
    {
         // Mensajes del validador
         $messages = [
            'required'         => 'Debe ingresar el :attribute',
            'motivos.required' => 'Debe seleccionar al menos un Motivo de Problema',
            'numeric' => 'El :attribute debe solo contener números',
            'max'     => 'El :attribute no debe exeder los :max caracteres',
            'min'    => 'El :attribute debe tener minimo :min caracteres',
            'unique' => 'El valor :attribute ya existe en la base de datos',
        ];

        // Reglas del validador
        $rules = [
            'motivos' => 'required',
            '_id' => 'required',
        ];

        // Validador de los input del formulario
        $validator = \Validator::make($request->all(), $rules, $messages);

        //Si contiene errores se devuelven todos los errores, de lo contrario guarda en la base de datos
        if ( $validator->fails() ) {
            return response()->json($validator->errors(),400);
        } else {

            $documento = Documento::findOrFail( $request->get('_id') );
            $documento->observacion_problema = Auth::user()->name.': '.trim( $request->get('observacion_problema') );
            if ( $request->get('proceso_solucion_problema') ) {
                $documento->proceso_solucion_problema = 1;
            }
            $documento->id_user_problema = Auth::user()->id;
            $documento->save();

            foreach ( $request->get('motivos') as $i => $idMotivo ) {

                $documentoMotivoProblema = new DocumentoMotivoProblema();
                $documentoMotivoProblema->id_documento = $documento->id;
                $documentoMotivoProblema->id_motivo_problema = $idMotivo;
                $documentoMotivoProblema->save();

            }

            /**
             * Quitar validacion del documento.
             */
            $quitaValidacion = false;
            if ( $documento->validado != null ) {

                $documento->invalidar();
                $documento->actualizaValidacionDocumentosRelacionados();
                $quitaValidacion = true;

            }

            $mensaje = 'Se han guardado correctamente los problemas.';
            $mensaje .= $quitaValidacion ? '<br>El documento ya no está cuadraro.' : '';
            $mensaje .= '<br>Puede encontrarlo en el listado de documentos: "Con problema"';

            $datos = array(
                'mensaje' => $mensaje,
                'estado'  => 'success',
                'id'      => $documento->id,
            );
        }

        return response()->json($datos,200);
    }

    

    /**
     * Funcion para mostrar todos los documentos validados.
     * Esta opcion se añade para los documentos desde el 22-junio-2019.
     */
    public function geDocumentosValidados()
    {
        if (!\Entrust::can(['ver-documentos-cuadrados','invalidar-documento','desvincular-documentos-con-recepcion'])) {
            return \Redirect::to('home');
        }

        $documentos = Documento::validados()
                               ->where('fecha_ingreso','>=',date('Y-m-d').' 00:00:00')
                               ->get();

        $tiposDocumento = TipoDocumento::whereNotIn('id',[2,8])->get();
        $modalidadesCompra = ModalidadCompra::all();
        $tiposAdjudicacion = TipoAdjudicacion::all();
        $tiposInforme = TipoInforme::all();
        $usuariosResponsables = User::where('responsable',1)->get();
        $usuariosCuadratura = User::whereHas('getDocumentosCuadrados')->get();
        $referentesTecnicos = ReferenteTecnico::all();
        $usuariosDigitadores = User::has('getDocumentos')->get();
        $proveedores = Proveedor::all();
        $itemsPresupuestarios = ItemPresupuestario::all();
        
        return view('documento.index_validados')->with('documentos',$documentos)
                                                ->with('tiposDocumento',$tiposDocumento)
                                                ->with('modalidadesCompra',$modalidadesCompra)
                                                ->with('tiposAdjudicacion',$tiposAdjudicacion)
                                                ->with('tiposInforme',$tiposInforme)
                                                ->with('usuariosResponsables',$usuariosResponsables)
                                                ->with('usuariosCuadratura',$usuariosCuadratura)
                                                ->with('referentesTecnicos',$referentesTecnicos)
                                                ->with('usuariosDigitadores',$usuariosDigitadores)
                                                ->with('proveedores',$proveedores)
                                                ->with('itemsPresupuestarios',$itemsPresupuestarios);
    }

    /**
     * Filtra los documentos validados, según los filtros seleccionados
     */
    public function postFiltrarDocumentosValidados(Request $request)
    {
        // \Debugbar::disable();
        set_time_limit ( 0 );
        ini_set('max_execution_time', 0);

        $fechaInicio = fecha_Y_m_d($request->input('filtro_fecha_inicio'));
        $fechaTermino = fecha_Y_m_d($request->input('filtro_fecha_termino'));
        // dd($fechaInicio,$fechaTermino);
        // dd($fechaInicio,$fechaTermino,$request->input('filtro_fecha_inicio'));

        $documentos = Documento::validados();
        
        if ( $request->input('filtro_numero_documento') == null ) {
            if ( $request->input('filtro_fecha_inicio') != null ) {
                // $documentos = $documentos->where('fecha_ingreso','>=',$fechaInicio.' 00:00:00');
                $documentos = $documentos->where('fecha_recepcion','>=',$fechaInicio);
            } else {
                $documentos = $documentos->where('fecha_ingreso','>=','2019-06-22 23:59:59');
            }
        } else {
            $documentos = $documentos->where('numero_documento', trim($request->input('filtro_numero_documento')) );
        }

        if ( $request->input('filtro_fecha_termino') != null ) {
            // $documentos = $documentos->where('fecha_ingreso','<=',$fechaTermino.' 23:59:59');
            $documentos = $documentos->where('fecha_recepcion','<=',$fechaTermino);
        }

        if ( $request->input('filtro_proveedor') != null ) {
            $documentos = $documentos->WhereIn('id_proveedor',$request->input('filtro_proveedor'));
        }

        if ( $request->input('filtro_tipo_documento') != null ) {
            $documentos = $documentos->WhereIn('id_tipo_documento',$request->input('filtro_tipo_documento'));
        }

        if ( $request->input('filtro_modalidad_compra') != null ) {
            $documentos = $documentos->WhereIn('id_modalidad_compra',$request->input('filtro_modalidad_compra'));
        }

        if ( $request->input('filtro_tipo_adjudicacion') != null ) {
            $documentos = $documentos->WhereIn('id_tipo_adjudicacion',$request->input('filtro_tipo_adjudicacion'));
        }

        if ( $request->input('filtro_tipo_informe') != null ) {
            $documentos = $documentos->WhereIn('id_tipo_informe',$request->input('filtro_tipo_informe'));
        }

        if ( $request->input('filtro_responsable') != null ) {
            $documentos = $documentos->WhereIn('id_responsable',$request->input('filtro_responsable'));
        }

        if ( $request->input('filtro_referente_tecnico') != null ) {
            $documentos = $documentos->WhereIn('id_referente_tecnico',$request->input('filtro_referente_tecnico'));
        }

        if ( $request->input('filtro_digitador') != null ) {
            $documentos = $documentos->WhereIn('id_digitador',$request->input('filtro_digitador'));
        }

        if ( $request->input('filtro_item_presupuestario') != null) {
            $documentos = $documentos->whereHas('getDevengos',function ($query) use ($request) {
                $query->WhereIn('id_item_presupuestario',$request->input('filtro_item_presupuestario'));
            });
        }

        if ( $request->input('filtro_cuadratura') != null ) {
            $documentos = $documentos->WhereIn('id_user_validado',$request->input('filtro_cuadratura'));
        }

        if ( $request->input('filtro_numero_documento_compra') != null ) {
            $documentos = $documentos->where('documento_compra', trim($request->input('filtro_numero_documento_compra')) );
        }

        $documentos = $documentos->get();

        //dd('filtros',$request->all(),$fechaInicio,$fechaTermino,$documentos);

        $tiposDocumento = TipoDocumento::whereNotIn('id',[2,8])->get();
        $modalidadesCompra = ModalidadCompra::all();
        $tiposAdjudicacion = TipoAdjudicacion::all();
        $tiposInforme = TipoInforme::all();
        $usuariosResponsables = User::where('responsable',1)->get();
        $referentesTecnicos = ReferenteTecnico::all();
        $usuariosDigitadores = User::has('getDocumentos')->get();
        $usuariosCuadratura = User::whereHas('getDocumentosCuadrados')->get();
        $proveedores = Proveedor::all();
        $itemsPresupuestarios = ItemPresupuestario::all();
        
        return view('documento.index_validados')->with('documentos',$documentos)
                                                ->with('tiposDocumento',$tiposDocumento)
                                                ->with('modalidadesCompra',$modalidadesCompra)
                                                ->with('tiposAdjudicacion',$tiposAdjudicacion)
                                                ->with('tiposInforme',$tiposInforme)
                                                ->with('usuariosResponsables',$usuariosResponsables)
                                                ->with('usuariosCuadratura',$usuariosCuadratura)
                                                ->with('referentesTecnicos',$referentesTecnicos)
                                                ->with('usuariosDigitadores',$usuariosDigitadores)
                                                ->with('proveedores',$proveedores)
                                                ->with('itemsPresupuestarios',$itemsPresupuestarios)
                                                //filtros
                                                ->with('filtroFechaInicio',$request->input('filtro_fecha_inicio'))
                                                ->with('filtroFechaTermino',$request->input('filtro_fecha_termino'))
                                                ->with('filtroProveedor',$request->input('filtro_proveedor'))
                                                ->with('filtroTipoDocumento',$request->input('filtro_tipo_documento'))
                                                ->with('filtroModalidadCompra',$request->input('filtro_modalidad_compra'))
                                                ->with('filtroTipoAdjudicacion',$request->input('filtro_tipo_adjudicacion'))
                                                ->with('filtroTipoInforme',$request->input('filtro_tipo_informe'))
                                                ->with('filtroResponsable',$request->input('filtro_responsable'))
                                                ->with('filtroCuadratura',$request->input('filtro_cuadratura'))
                                                ->with('filtroReferenteTecnico',$request->input('filtro_referente_tecnico'))
                                                ->with('filtroDigitador',$request->input('filtro_digitador'))
                                                ->with('filtroItems',$request->input('filtro_item_presupuestario'))
                                                ->with('filtroNumeroDocumento',$request->input('filtro_numero_documento'))
                                                ->with('filtroNumeroDocumentoCompra',$request->input('filtro_numero_documento_compra'));
    }

    public function postValidadosExcel(Request $request)
    {
        set_time_limit ( 0 );
        ini_set('max_execution_time', 0);

        \Excel::create('Documentos Validados '.date('d-m-Y').'', function($excel) use($request) {
            $fechaInicio = fecha_Y_m_d($request->input('filtro_fecha_inicio'));
            $fechaTermino = fecha_Y_m_d($request->input('filtro_fecha_termino'));
            // dd($fechaInicio,$fechaTermino);
            // dd($fechaInicio,$fechaTermino,$request->input('filtro_fecha_inicio'));

            $documentos = Documento::validados();

            if ( $request->input('filtro_fecha_inicio') != null ) {
                // $documentos = $documentos->where('fecha_ingreso','>=',$fechaInicio.' 00:00:00');
                $documentos = $documentos->where('fecha_recepcion','>=',$fechaInicio);
            } else {
                $documentos = $documentos->where('fecha_ingreso','>=','2019-06-22 23:59:59');
            }

            if ( $request->input('filtro_fecha_termino') != null ) {
                // $documentos = $documentos->where('fecha_ingreso','<=',$fechaTermino.' 23:59:59');
                $documentos = $documentos->where('fecha_recepcion','<=',$fechaTermino);
            }

            if ( $request->input('filtro_proveedor') != null ) {
                $documentos = $documentos->WhereIn('id_proveedor',$request->input('filtro_proveedor'));
            }

            if ( $request->input('filtro_tipo_documento') != null ) {
                $documentos = $documentos->WhereIn('id_tipo_documento',$request->input('filtro_tipo_documento'));
            }

            if ( $request->input('filtro_modalidad_compra') != null ) {
                $documentos = $documentos->WhereIn('id_modalidad_compra',$request->input('filtro_modalidad_compra'));
            }

            if ( $request->input('filtro_tipo_adjudicacion') != null ) {
                $documentos = $documentos->WhereIn('id_tipo_adjudicacion',$request->input('filtro_tipo_adjudicacion'));
            }

            if ( $request->input('filtro_tipo_informe') != null ) {
                $documentos = $documentos->WhereIn('id_tipo_informe',$request->input('filtro_tipo_informe'));
            }

            if ( $request->input('filtro_responsable') != null ) {
                $documentos = $documentos->WhereIn('id_responsable',$request->input('filtro_responsable'));
            }

            if ( $request->input('filtro_referente_tecnico') != null ) {
                $documentos = $documentos->WhereIn('id_referente_tecnico',$request->input('filtro_referente_tecnico'));
            }

            if ( $request->input('filtro_digitador') != null ) {
                $documentos = $documentos->WhereIn('id_digitador',$request->input('filtro_digitador'));
            }

            if ( $request->input('filtro_item_presupuestario') != null) {
                $documentos = $documentos->whereHas('getDevengos',function ($query) use ($request) {
                    $query->WhereIn('id_item_presupuestario',$request->input('filtro_item_presupuestario'));
                });
            }

            if ( $request->input('filtro_cuadratura') != null ) {
                $documentos = $documentos->WhereIn('id_user_validado',$request->input('filtro_cuadratura'));
            }

            if ( $request->input('filtro_numero_documento') != null ) {
                $documentos = $documentos->where('numero_documento', trim($request->input('filtro_numero_documento')) );
            }

            if ( $request->input('filtro_numero_documento_compra') != null ) {
                $documentos = $documentos->where('documento_compra', trim($request->input('filtro_numero_documento_compra')) );
            }

            $documentos = $documentos->get();

            $excel->sheet('Documentos', function($sheet) use($documentos) {
            
                $sheet->row(1, [
                    'Estado', 
                    // 'Responsable',
                    'Cuadratura',
                    'Factoring',
                    'Chile-Compra',
                    'M/Dev',
                    'Proveedor',
                    'RUT',
                    'Tipo Dcto.',
                    'N° Dcto.',
                    'F/Doc.',
                    'M/Doc.',
                    'Fecha RD',
                    'Mes RD',
                    'Item',
                    'ID SIGFE',
                    'Fecha ID',
                    'Devengado',
                    'Mecanismo de Adjudicación',
                    'Observación',
                    'Detalle',
                    'URL Factura',
                    'URL Recepción'
                ]);

                $totalDevengadoExcel = 0;
                $cuentaLineas = 1;
                foreach ( $documentos as $indexAux => $documento ) {
                    $items = '';
                    $totalDevengadoPorDocumento = 0;

                    foreach ( $documento->getDevengos as $index => $devengo) {
                        $cuentaLineas++;
                        $items = $devengo->getItemPresupuestario->codigo();
                        $totalDevengadoPorDocumento = $devengo->monto;
                        // para columna "devengado"
                        $devengado = '';
                        if ($documento->id_tipo_documento == 4 || $documento->id_tipo_documento == 10) {
                            $devengado .= '-'.$totalDevengadoPorDocumento;
                            $totalDevengadoExcel = $totalDevengadoExcel - $totalDevengadoPorDocumento;
                        } else {
                            $devengado .= $totalDevengadoPorDocumento;
                            $totalDevengadoExcel = $totalDevengadoExcel + $totalDevengadoPorDocumento;
                        }

                        // para columna factoring
                        $factoring = '';
                        if ($documento->getProveedorFactoring) {
                            $factoring .= $documento->getProveedorFactoring->nombre.' '.$documento->getProveedorFactoring->rut;
                        }

                        $estado = $documento->getEstadoDocumento('excel');

                        $mesSigfe = '';
                        $fechaSigfe = '';
                        if ($documento->fecha_sigfe != null && $documento->fecha_sigfe != '') {
                            $mesSigfe = explode("-",$documento->fecha_sigfe)[1];
                            $fechaSigfe = fecha_dmY($documento->fecha_sigfe);
                        }

                        $urlFactura = '';
                        if ( $documento->getArchivos ) {
                            foreach ($documento->getArchivos as $archivo) {
                                if ( $archivo->id_tipo_archivo == 1 ) {
                                    if ($archivo->cargado == 1) {
                                        $urlFactura = 'http://'.$archivo->ubicacion;
                                    } else {
                                        $urlFactura = 'http://10.4.237.28/finanzas/'.$archivo->ubicacion;
                                    }
                                    
                                }
                            }
                        }

                        $urlRecepcion = '';
                        if ( $documento->getUnaRelacionRecepcionValidada ) {
                            foreach ( $documento->getRelacionRecepcionesValidadas as $relacion ) {
                                if ( $relacion->getWsDocumento ) {
                                    if ( $relacion->getWsDocumento->getWsArchivos ) {
                                        foreach ($relacion->getWsDocumento->getWsArchivos as $archivo) {
                                            $urlRecepcion .= 'http://10.4.237.28/finanzas/'.$archivo->ubicacion.PHP_EOL;
                                        }
                                    }
                                }
                            }
                            
                        }

                        $cuadratura = '';
                        if ( $documento->id_user_validado != null ) {
                            $cuadratura = $documento->getUserValidado->name;
                        } elseif ( $documento->id_relacion != null ) {

                            if ( $documento->getUnaRelacionRecepcionValidada->getUsuarioResponsable ) {
                                $cuadratura = $documento->getUnaRelacionRecepcionValidada->getUsuarioResponsable->name;
                            } else {
                                $cuadratura = 'Automático';
                            }
                            
                        }

                        $sheet->row($cuentaLineas, [
                            $estado, 
                            // $documento->getResponsable->name,
                            $cuadratura,
                            $factoring,
                            $documento->documento_compra,
                            $mesSigfe, // mes de la fecha sigfe
                            $documento->getProveedor->nombre,
                            $documento->getProveedor->rut,
                            $documento->getTipoDocumento->nombre,
                            $documento->numero_documento,
                            fecha_dmY($documento->fecha_documento),
                            explode("-",$documento->fecha_documento)[1], // mes de la fecha del documento
                            fecha_dmY($documento->fecha_recepcion),
                            explode("-",$documento->fecha_recepcion)[1], // mes de la fecha de recepcion del documento
                            $items,
                            $documento->id_sigfe,
                            $fechaSigfe,
                            $devengado,
                            $documento->getTipoAdjudicacion->nombre,
                            $documento->observacion,
                            $documento->detalle,
                            $urlFactura,
                            $urlRecepcion
                        ]);
                    }
                }


                $sheet->row( $cuentaLineas+1 , [
                    '', 
                    '', 
                    // '',
                    '',
                    '', 
                    '',
                    '',
                    '',
                    '',
                    '',
                    '', 
                    '',
                    '',
                    '',
                    '',
                    'TOTAL',
                    $totalDevengadoExcel,
                    '',
                    '',
                    '',
                    '',
                    ''
                ]);

        });

        })->export('xlsx');
    }

    public function dataTableConProblemas(Request $request)
    {
        set_time_limit ( 0 );
        ini_set('max_execution_time', 0);

        $search = $request->get('search')['value'];
        $order_column = $request->get('order')[0]['column'];
        $order_dir = $request->get('order')[0]['dir'];

        $formJSON = json_decode($request->get('form'));
        $form = construyeFormulario($formJSON);
        
        $fechaInicio = fecha_Y_m_d($form->filtro_fecha_inicio);
        $fechaTermino = fecha_Y_m_d($form->filtro_fecha_termino);

        $documentos = Documento::conProblemas();

        $totalDocumentosConProblemas = Documento::where('fecha_ingreso','>=','2019-06-22 00:00:00')
                                                ->where('id_user_problema','<>', null);

        if ( $form->filtro_numero_documento == null ) {
            // Se filtra por las fechas

            if ( $form->filtro_fecha_inicio != null ) {

                if ( $form->filtro_fecha_a_utilizar == 'Ingreso' ) {
                    $documentos = $documentos->where('fecha_ingreso','>=',$fechaInicio.' 00:00:00');
                } elseif ( $form->filtro_fecha_a_utilizar == 'Recepción' ) {
                    $documentos = $documentos->where('fecha_recepcion','>=',$fechaInicio);
                }

            }

            if ( $form->filtro_fecha_termino != null ) {
                if ( $form->filtro_fecha_a_utilizar == 'Ingreso' ) {
                    $documentos = $documentos->where('fecha_ingreso','<=',$fechaTermino.' 23:59:59');
                } elseif ( $form->filtro_fecha_a_utilizar == 'Recepción' ) {
                    $documentos = $documentos->where('fecha_recepcion','<=',$fechaTermino);
                }

                $totalDocumentosConProblemas = $totalDocumentosConProblemas->where('fecha_ingreso','<=',$fechaTermino.' 23:59:59');
            }

        } else {
            // no se consideran las fechas, y se busca el numero del documento
            $documentos = $documentos->where('numero_documento', trim($form->filtro_numero_documento) );
        }

        if ( isset($form->filtro_proveedor) ) {
            $documentos = $documentos->WhereIn('id_proveedor' ,$form->filtro_proveedor);
            $totalDocumentosConProblemas = $totalDocumentosConProblemas->WhereIn('id_proveedor' ,$form->filtro_proveedor);
        }

        if ( isset($form->filtro_motivo_problema) ) {
            $documentos = $documentos->WhereHas('getDocumentoMotivoProblema', function($q) use($form){
                                            $q->WhereHas('getMotivoProblema', function($query) use($form){
                                                $query->WhereIn('id', $form->filtro_motivo_problema);
                                            });
                                        });

            $totalDocumentosConProblemas = $totalDocumentosConProblemas->WhereHas('getDocumentoMotivoProblema', function($q) use($form){
                                                                            $q->WhereHas('getMotivoProblema', function($query) use($form){
                                                                                $query->WhereIn('id', $form->filtro_motivo_problema);
                                                                            });
                                                                        });
        }

        if ( isset($form->filtro_tipo_documento) ) {
            $documentos = $documentos->WhereIn('id_tipo_documento', $form->filtro_tipo_documento);
            $totalDocumentosConProblemas = $totalDocumentosConProblemas->WhereIn('id_tipo_documento', $form->filtro_tipo_documento);
        } else {
            
            if ( ! \Entrust::hasRole(['propietario','administrador']) ) {
                $documentos = $documentos->whereIn('id_tipo_documento', [1,2,3,6,7,9]);
                $totalDocumentosConProblemas = $totalDocumentosConProblemas->whereIn('id_tipo_documento', [1,2,3,6,7,9]);
            }

        }

        if ( isset($form->filtro_modalidad_compra) ) {
            $documentos = $documentos->WhereIn('id_modalidad_compra', $form->filtro_modalidad_compra);
            $totalDocumentosConProblemas = $totalDocumentosConProblemas->WhereIn('id_modalidad_compra', $form->filtro_modalidad_compra);
        }

        if ( isset($form->filtro_tipo_adjudicacion) ) {
            $documentos = $documentos->WhereIn('id_tipo_adjudicacion', $form->filtro_tipo_adjudicacion);
            $totalDocumentosConProblemas = $totalDocumentosPorValidar->WhereIn('id_tipo_adjudicacion', $form->filtro_tipo_adjudicacion);
        }

        if ( isset($form->filtro_tipo_informe) ) {
            $documentos = $documentos->WhereIn('id_tipo_informe', $form->filtro_tipo_informe);
            $totalDocumentosConProblemas = $totalDocumentosConProblemas->WhereIn('id_tipo_informe', $form->filtro_tipo_informe);
        }

        if ( isset($form->filtro_responsable) ) {
            $documentos = $documentos->WhereIn('id_responsable', $form->filtro_responsable);
            $totalDocumentosConProblemas = $totalDocumentosConProblemas->WhereIn('id_responsable', $form->filtro_responsable);
        }

        if ( isset($form->filtro_referente_tecnico) ) {
            $documentos = $documentos->WhereIn('id_referente_tecnico', $form->filtro_referente_tecnico);
            $totalDocumentosConProblemas = $totalDocumentosConProblemas->WhereIn('id_referente_tecnico', $form->filtro_referente_tecnico);
        }

        if ( isset($form->filtro_digitador) ) {
            $documentos = $documentos->WhereIn('id_digitador', $form->filtro_digitador);
            $totalDocumentosConProblemas = $totalDocumentosConProblemas->WhereIn('id_digitador', $form->filtro_digitador);
        }

        if ( isset($form->filtro_item_presupuestario) ) {
            $documentos = $documentos->whereHas('getDevengos',function ($query) use ($form) {
                $query->WhereIn('id_item_presupuestario', $form->filtro_item_presupuestario);
            });
            $totalDocumentosConProblemas = $totalDocumentosConProblemas->whereHas('getDevengos',function ($query) use ($form) {
                $query->WhereIn('id_item_presupuestario', $form->filtro_item_presupuestario);
            });
        }

        if ( $form->filtro_numero_documento_compra != null ) {
            $documentos = $documentos->where('documento_compra', trim($form->filtro_numero_documento_compra) );
            $totalDocumentosConProblemas = $totalDocumentosConProblemas->where('documento_compra', trim($form->filtro_numero_documento_compra) );
        }

        if ( $form->filtro_proceso != '' ) {
            if ( $form->filtro_proceso == 'null' ) {
                $documentos = $documentos->where('proceso_solucion_problema', null);
                $totalDocumentosConProblemas = $totalDocumentosConProblemas->where('proceso_solucion_problema', null);
            } else {
                // es 1
                $documentos = $documentos->where('proceso_solucion_problema', $form->filtro_proceso);
                $totalDocumentosConProblemas = $totalDocumentosConProblemas->where('proceso_solucion_problema', $form->filtro_proceso);
            }
            
        }

        $documentos = $documentos->select(
            'fecha_recepcion', 'fecha_documento', 'total_documento','id',
            'numero_documento', 'documento_compra','id_proveedor','id_tipo_documento',
            'fecha_ingreso', 'id_relacion', 'validado', 'id_visto_bueno',
            'id_rechazo_visto_bueno', 'id_estado', 'id_devengador','total_documento_actualizado',
            'proceso_solucion_problema'
        );

        $documentos = $documentos->get();

        $totalDocumentosConProblemas = $totalDocumentosConProblemas->get()->count();

        $data = [];

        if ( $request->get('length') != -1 ) {
            $hasta = $request->get('start') + $request->get('length');
        } else {
            // Se quieren ver todos los datos
            $hasta = $request->get('start') + $documentos->count();
        }
        
        for( $i = $request->get('start') ; $i < $hasta ; $i++ ) {
            if ( isset($documentos[$i]) ) {
                $data[] = $documentos[$i]->getDatosConProblema();
            }
        }

        // Se realiza la busqueda y se ordenan los datos
        if ( $documentos->count() > 0 ) {

            foreach ($data as $clave => $fila) {

                if ( $search != '' ) {
                    if ( in_array_r($search,$fila) ) {
                        // dd("lo encuentra",$fila,$search);
                        $columna[$clave] = $fila[$order_column];
                    } else {
                        // dd("NO lo encuentra",$fila,$search);
                        unset( $data[$clave] ); // quita el elemento del data
                    }
                } else {
                    $columna[$clave] = $fila[$order_column];
                }
                
            }

            
            // Se ordenan los datos, segun la columna seleccionada
            if ( $order_dir == 'asc' ) {
                array_multisort($columna, SORT_ASC, $data);
            } elseif ( $order_dir == 'desc' ) {
                array_multisort($columna, SORT_DESC, $data);
            }

        }
            

        $respuesta = array(
            "draw" => $request->get('draw'),
            "recordsTotal" => count($documentos),
            "recordsFiltered"=> count($documentos),
            "data" => $data,
            "totalDocumentosConProblemas" => formatoMiles($totalDocumentosConProblemas),
        );

        return json_encode($respuesta);
    }

    /**
     * Documentos con problemas
     */
    public function getDocumentosConProblema()
    {
        if (!\Entrust::can(['problemas-documento','quitar-problemas-documento'])) {
            return \Redirect::to('home');
        }

        if ( \Entrust::hasRole(['propietario','administrador']) ) {
            $tiposDocumento = TipoDocumento::all();
        } else {
            $tiposDocumento = TipoDocumento::whereIn('id',[1,2,3,6,7,9])->get();
        }
        $modalidadesCompra = ModalidadCompra::all();
        $tiposAdjudicacion = TipoAdjudicacion::all();
        $tiposInforme = TipoInforme::all();
        $usuariosResponsables = User::where('responsable',1)->get();
        $referentesTecnicos = ReferenteTecnico::all();
        $usuariosDigitadores = User::has('getDocumentos')->get();
        $proveedores = Proveedor::all();
        $itemsPresupuestarios = ItemPresupuestario::all();

        // Select para Fechas a utilizar en el filtro
        $fechaUtilizar1 = new \stdClass;
        $fechaUtilizar1->opcion = 'Ingreso';
        $fechaUtilizar2 = new \stdClass;
        $fechaUtilizar2->opcion = 'Recepción';

        $fechasUtilizar[] = $fechaUtilizar1;
        $fechasUtilizar[] = $fechaUtilizar2;

        $motivosProblema = MotivoProblema::all();

        return view('documento.index_con_problema')->with('tiposDocumento',$tiposDocumento)
                                                   ->with('modalidadesCompra',$modalidadesCompra)
                                                   ->with('tiposAdjudicacion',$tiposAdjudicacion)
                                                   ->with('tiposInforme',$tiposInforme)
                                                   ->with('usuariosResponsables',$usuariosResponsables)
                                                   ->with('referentesTecnicos',$referentesTecnicos)
                                                   ->with('usuariosDigitadores',$usuariosDigitadores)
                                                   ->with('proveedores',$proveedores)
                                                   ->with('itemsPresupuestarios',$itemsPresupuestarios)
                                                   ->with('fechasUtilizar',$fechasUtilizar)
                                                   ->with('motivosProblema', $motivosProblema);
    }

    public function postDocumentosConProblemaExcel(Request $request)
    {
        set_time_limit ( 0 );
        ini_set('max_execution_time', 0);
        
        \Excel::create('Documentos Con Problemas '.date('d-m-Y').'', function($excel) use($request) {
            $fechaInicio = fecha_Y_m_d($request->input('filtro_fecha_inicio'));
            $fechaTermino = fecha_Y_m_d($request->input('filtro_fecha_termino'));

            $documentos = Documento::conProblemas();


            if ( $request->input('filtro_numero_documento') == null  ) {
                if ( $request->input('filtro_fecha_inicio') != null ) {

                    if ( $request->input('filtro_fecha_a_utilizar') == 'Ingreso' ) {
                        $documentos = $documentos->where('fecha_ingreso','>=',$fechaInicio.' 00:00:00');
                    } elseif ( $request->input('filtro_fecha_a_utilizar') == 'Recepción' ) {
                        $documentos = $documentos->where('fecha_recepcion','>=',$fechaInicio);
                    } 

                }

                if ( $request->input('filtro_fecha_termino') != null ) {

                    if ( $request->input('filtro_fecha_a_utilizar') == 'Ingreso' ) {
                        $documentos = $documentos->where('fecha_ingreso','<=',$fechaTermino.' 23:59:59');
                    } elseif ( $request->input('filtro_fecha_a_utilizar') == 'Recepción' ) {
                        $documentos = $documentos->where('fecha_recepcion','<=',$fechaTermino);
                    }
                }

            } else {
                $documentos = $documentos->where('numero_documento', trim($request->input('filtro_numero_documento')) );
            }
            

            if ( $request->input('filtro_proveedor') != null ) {
                $documentos = $documentos->WhereIn('id_proveedor', $request->input('filtro_proveedor'));
            }

            if ( $request->input('filtro_motivo_problema') != null ) {
                $documentos = $documentos->WhereHas('getDocumentoMotivoProblema', function($q) use($request){
                                                $q->WhereHas('getMotivoProblema', function($query) use($request){
                                                    $query->WhereIn('id', $request->input('filtro_motivo_problema'));
                                                });
                                            });
            }

            if ( $request->input('filtro_tipo_documento') != null ) {
                $documentos = $documentos->WhereIn('id_tipo_documento',$request->input('filtro_tipo_documento'));
            }

            if ( $request->input('filtro_modalidad_compra') != null ) {
                $documentos = $documentos->WhereIn('id_modalidad_compra',$request->input('filtro_modalidad_compra'));
            }

            if ( $request->input('filtro_tipo_adjudicacion') != null ) {
                $documentos = $documentos->WhereIn('id_tipo_adjudicacion',$request->input('filtro_tipo_adjudicacion'));
            }

            if ( $request->input('filtro_tipo_informe') != null ) {
                $documentos = $documentos->WhereIn('id_tipo_informe',$request->input('filtro_tipo_informe'));
            }

            if ( $request->input('filtro_responsable') != null ) {
                $documentos = $documentos->WhereIn('id_responsable',$request->input('filtro_responsable'));
            }

            if ( $request->input('filtro_referente_tecnico') != null ) {
                $documentos = $documentos->WhereIn('id_referente_tecnico',$request->input('filtro_referente_tecnico'));
            }

            if ( $request->input('filtro_digitador') != null ) {
                $documentos = $documentos->WhereIn('id_digitador',$request->input('filtro_digitador'));
            }

            if ( $request->input('filtro_item_presupuestario') != null) {
                $documentos = $documentos->whereHas('getDevengos',function ($query) use ($request) {
                    $query->WhereIn('id_item_presupuestario',$request->input('filtro_item_presupuestario'));
                });
            }

            if ( $request->input('filtro_numero_documento_compra') != null ) {
                $documentos = $documentos->where('documento_compra', trim($request->input('filtro_numero_documento_compra')) );
            }

            if ( $request->input('filtro_proceso') != '' ) {
                if ( $request->input('filtro_proceso') == 'null' ) {
                    $documentos = $documentos->where('proceso_solucion_problema', null);
                } else {
                    // es 1
                    $documentos = $documentos->where('proceso_solucion_problema', $request->input('filtro_proceso'));
                }
                
            }

            $documentos = $documentos->get();

            $excel->sheet('Documentos', function($sheet) use($documentos) {
            
                $sheet->row(1, [
                    'Estado',
                    'Motivos Problema',
                    'Observación Problema',
                    'Proceso',
                    // 'Responsable',
                    'Factoring',
                    'Chile-Compra',
                    'M/Dev',
                    'Proveedor',
                    'RUT',
                    'Tipo Informe',
                    'Tipo Dcto.',
                    'N° Dcto.',
                    'F/Doc.',
                    'M/Doc.',
                    'Fecha RD',
                    'Mes RD',
                    'Item',
                    'ID SIGFE',
                    'Fecha ID',
                    'Devengado',
                    'Mecanismo de Adjudicación',
                    'Observación',
                    'Detalle',
                    'URL Factura',
                    'URL Recepción'
                ]);

                $totalDevengadoExcel = 0;
                $cuentaLineas = 1;
                foreach ( $documentos as $indexAux => $documento ) {
                    $items = '';
                    $totalDevengadoPorDocumento = 0;

                    foreach ( $documento->getDevengos as $index => $devengo) {
                        $cuentaLineas++;
                        $items = $devengo->getItemPresupuestario->codigo();
                        $totalDevengadoPorDocumento = $devengo->monto;
                        // para columna "devengado"
                        $devengado = '';
                        if ($documento->id_tipo_documento == 4 || $documento->id_tipo_documento == 10) {
                            $devengado .= '-'.$totalDevengadoPorDocumento;
                            $totalDevengadoExcel = $totalDevengadoExcel - $totalDevengadoPorDocumento;
                        } else {
                            $devengado .= $totalDevengadoPorDocumento;
                            $totalDevengadoExcel = $totalDevengadoExcel + $totalDevengadoPorDocumento;
                        }

                        // para columna factoring
                        $factoring = '';
                        if ($documento->getProveedorFactoring) {
                            $factoring .= $documento->getProveedorFactoring->nombre.' '.$documento->getProveedorFactoring->rut;
                        }

                        $estado = $documento->getEstadoDocumento();
                        $motivosProblema = '';
                        foreach ($documento->getDocumentoMotivoProblema as $documentoMotivoProblema) {
                            $motivosProblema .= $documentoMotivoProblema->getMotivoProblema->nombre.' ';
                        }

                        $mesSigfe = '';
                        $fechaSigfe = '';
                        if ($documento->fecha_sigfe != null && $documento->fecha_sigfe != '') {
                            $mesSigfe = explode("-",$documento->fecha_sigfe)[1];
                            $fechaSigfe = fecha_dmY($documento->fecha_sigfe);
                        }

                        $urlFactura = '';
                        if ( $documento->getArchivos ) {
                            foreach ($documento->getArchivos as $archivo) {
                                if ( $archivo->id_tipo_archivo == 1 ) {
                                    if ($archivo->cargado == 1) {
                                        $urlFactura = 'http://'.$archivo->ubicacion;
                                    } else {
                                        $urlFactura = 'http://10.4.237.28/finanzas/'.$archivo->ubicacion;
                                    }
                                    
                                }
                            }
                        }

                        $urlRecepcion = '';
                        if ( $documento->getUnaRelacionRecepcionValidada ) {
                            foreach ( $documento->getRelacionRecepcionesValidadas as $relacion ) {
                                if ( $relacion->getWsDocumento ) {
                                    if ( $relacion->getWsDocumento->getWsArchivos ) {
                                        foreach ($relacion->getWsDocumento->getWsArchivos as $archivo) {
                                            $urlRecepcion .= 'http://10.4.237.28/finanzas/'.$archivo->ubicacion.PHP_EOL;
                                        }
                                    }
                                }
                            }
                            
                        }

                        $proceso = 'No';
                        if ( $documento->proceso_solucion_problema ) {
                            $proceso = 'Si';
                        }

                        $sheet->row($cuentaLineas, [
                            $estado,
                            $motivosProblema,
                            $documento->observacion_problema,
                            $proceso,
                            // $documento->getResponsable->name, 
                            $factoring,
                            $documento->documento_compra,
                            $mesSigfe, // mes de la fecha sigfe
                            $documento->getProveedor->nombre,
                            $documento->getProveedor->rut,
                            $documento->getTipoInforme->nombre,
                            $documento->getTipoDocumento->nombre,
                            $documento->numero_documento,
                            fecha_dmY($documento->fecha_documento),
                            explode("-",$documento->fecha_documento)[1], // mes de la fecha del documento
                            fecha_dmY($documento->fecha_recepcion),
                            explode("-",$documento->fecha_recepcion)[1], // mes de la fecha de recepcion del documento
                            $items,
                            $documento->id_sigfe,
                            $fechaSigfe,
                            $devengado,
                            $documento->getTipoAdjudicacion->nombre,
                            $documento->observacion,
                            $documento->detalle,
                            $urlFactura,
                            $urlRecepcion
                        ]);
                    }
                }


                $sheet->row( $cuentaLineas+1 , [
                    '',
                    '',
                    '',
                    '',
                    // '', 
                    '',
                    '',
                    '', 
                    '',
                    '',
                    '',
                    '',
                    '',
                    '', 
                    '',
                    '',
                    '',
                    '',
                    'TOTAL',
                    $totalDevengadoExcel,
                    '',
                    '',
                    '',
                    '',
                    ''
                ]);

        });

        })->export('xlsx');
    }

    public function getModalAgregarObservacionProblemaDocumento($id)
    {
        $documento = Documento::with(['getDocumentoMotivoProblema','getDocumentoMotivoProblema.getMotivoProblema'])
                              ->findOrFail($id);
                            
        return view('documento.modal_agregar_observacion_problema',compact('documento'));
    }

    public function postAgregarObservacionProblemaDocumento(Request $request)
    {

        // Mensajes del validador
        $messages = [
            'required' => 'Debe ingresar el :attribute',
            'motivos.required' => 'Debe seleccionar al menos un Motivo de Rechazo',
            'numeric' => 'El :attribute debe solo contener números',
            'max' => 'El :attribute no debe exeder los :max caracteres',
            'min' => 'El :attribute debe tener minimo :min caracteres',
            'unique' => 'El valor :attribute ya existe en la base de datos',
        ];

        // Reglas del validador
        $rules = [
            '_id' => 'required',
            'observacion_problema' => 'required'
        ];
        
        // Validador de los input del formulario
        $validator = \Validator::make($request->all(), $rules, $messages);

        //Si contiene errores se devuelven todos los errores, de lo contrario guarda en la base de datos
        if ( $validator->fails() ) {
            return response()->json($validator->errors(),400);
        } else {
            $documento = Documento::findOrFail( $request->get('_id') );
            $documento->observacion_problema = $documento->observacion_problema.PHP_EOL.Auth::user()->name.': '.trim( $request->get('observacion_problema') );

            if ( $request->get('proceso_solucion_problema') ) {
                $documento->proceso_solucion_problema = 1;
            } else {
                $documento->proceso_solucion_problema = null;
            }

            $documento->save();

            $datos = array(
                'mensaje' => 'Se ha agregado la observación de problema',
                'estado' => 'success',
                'id' => $documento->id,
            );
        }

        return response()->json($datos,200);
    }

    public function getModalQuitarProblemaDocumento($id)
    {
        $documento = Documento::with(['getProveedor','getTipoDocumento','getModalidadCompra',
                                      'getTipoAdjudicacion','getTipoInforme','getResponsable',
                                      'getReferenteTecnico','getDigitador','getDevengador',
                                      'getArchivos','getArchivos.getTipoArchivo','getDevengos',
                                      'getDevengos.getItemPresupuestario',
                                      'getRelacionRecepcionesValidadas.getUsuarioResponsable',
                                      'getVistoBueno','getRechazoVistoBueno'])
                              ->findOrFail($id);

        return view('documento.modal_quitar_problema',compact('documento'));
    }

    public function postQuitarProblema(Request $request)
    {
        // Mensajes del validador
        $messages = [
            'required' => 'Debe ingresar el :attribute',
            'motivos.required' => 'Debe seleccionar al menos un Motivo de Rechazo',
            'numeric' => 'El :attribute debe solo contener números',
            'max' => 'El :attribute no debe exeder los :max caracteres',
            'min' => 'El :attribute debe tener minimo :min caracteres',
            'unique' => 'El valor :attribute ya existe en la base de datos',
        ];

        // Reglas del validador
        $rules = [
            '_id' => 'required',
        ];
        
        // Validador de los input del formulario
        $validator = \Validator::make($request->all(), $rules, $messages);

        //Si contiene errores se devuelven todos los errores, de lo contrario guarda en la base de datos
        if ( $validator->fails() ) {
            return response()->json($validator->errors(),400);
        } else {

            $documento = Documento::findOrFail( $request->get('_id') );
            $documento->observacion_problema = $documento->observacion_problema.PHP_EOL.Auth::user()->name.': Quita el documento del listado "Con Problemas"';
            $documento->id_user_problema = null;
            $documento->id_user_quita_problema = Auth::user()->id;
            $documento->save();

            foreach ($documento->getDocumentoMotivoProblema as $documentoMotivoProblema) {
                $documentoMotivoProblema->delete();
            }

            $datos = array(
                'mensaje' => 'Se ha quitado el problema del Documento',
                'estado' => 'success',
                'id' => $documento->id,
            );

        }

        return response()->json($datos,200);
    }

    public function getGenerarPDFTrazabilidadDocumento($id)
    {
        $documento = Documento::with(['getProveedor','getTipoDocumento','getModalidadCompra',
                                      'getTipoAdjudicacion','getTipoInforme','getResponsable',
                                      'getReferenteTecnico','getDigitador','getDevengador',
                                      'getArchivos','getArchivos.getTipoArchivo','getDevengos',
                                      'getDevengos.getItemPresupuestario','getRelacionRecepcionesValidadas.getUsuarioResponsable',
                                      'getVistoBueno','getRechazoVistoBueno'])
                              ->findOrFail($id);

        $content = \View::make('pdf.trazabilidad_documento', compact('documento'))->render();
        $mpdf = new Mpdf([
            // 'mode' => 'c',
            'format'=>'Letter-P', // carta
            'mirrorMargins' => true,
            'tempDir' => public_path() . '/tempMpdf'
            ]);
        //$mpdf = new Mpdf(['mode'=>'c']);

        $mpdf->useSubstitutions = false;
        $mpdf->simpleTables = true;

        $mpdf->SetDisplayMode('fullpage');
        $mpdf->keep_table_proportions = true;
        $mpdf->showImageErrors = true;
        $content = mb_convert_encoding($content, 'UTF-8', 'UTF-8');
        $mpdf->WriteHTML($content);
        // $mpdf->setFooter('<div style="border-top: 1px solid #CCCCCC;" ><div style="text-align: left; font-family: Arial, Helvetica,sans-serif; font-weight: bold;font-size: 7pt; padding-top: 4px;display: inline-block;float: left;width: 70%;">MEMORANDUM N° '.$documento->numero_documento.' del </div><div style="text-align: right; font-family: Arial, Helvetica,sans-serif; font-weight: bold;font-size: 7pt; padding-top: 4px;display: inline-block;float: right;width: 30%;">Pag.  {PAGENO} / {nbpg}</div></div>');
        $mpdf->Output("INFORME TRAZABILIDAD DOCUMENTO N° $documento->numero_documento - DEPARTAMENTO FINANZAS - HSJD.pdf",'I');
    }

    /**
     * Funcion que muestra la modal con la traza del documento
     */
    public function getModalTrazaDocumento($idDocumento, $eliminado = false)
    {
        $documento = Documento::with(['getProveedor','getTipoDocumento','getModalidadCompra',
                                      'getTipoAdjudicacion','getTipoInforme','getResponsable',
                                      'getReferenteTecnico','getDigitador','getDevengador',
                                      'getArchivos','getArchivos.getTipoArchivo','getDevengos',
                                      'getDevengos.getItemPresupuestario','getRelacionRecepcionesValidadas.getUsuarioResponsable',
                                      'getVistoBueno','getRechazoVistoBueno','getFoliosSigfe',
                                      'getFoliosSigfe.getDevengos','getFoliosSigfe.getDevengos.getItemPresupuestario'
                                      ]);

        if ( $eliminado ) {
            $documento = $documento->onlyTrashed();
        }
                              
        $documento = $documento->findOrFail($idDocumento);
                              
        
        return view('documento.modal_traza_documento')->with('documento',$documento);
    }

    /**
     * Muestra todos los documentos que se rechazaron en el visto bueno, 
     * opcion de los referentes tecnicos
     */
    public function geDocumentosRechazoVistoBueno()
    {
        $documentos = Documento::with(['getProveedor','getTipoDocumento','getDevengos.getItemPresupuestario',
                                       'getReferenteTecnico'])
                               //->where('fecha_ingreso','>=',date('Y-m-d').' 00:00:00')
                               ->where('id_rechazo_visto_bueno','<>',null)
                               ->where('id_visto_bueno',null)
                               ->whereHas('getRechazoVistoBueno', function ($query) {
                                   $query->where('created_at','>=',date('Y-m').'-01 00:00:00');
                               })
                               ->get();
        
        $tiposDocumento = TipoDocumento::whereNotIn('id',[2,8])->get();
        $modalidadesCompra = ModalidadCompra::all();
        $tiposAdjudicacion = TipoAdjudicacion::all();
        $tiposInforme = TipoInforme::all();
        $usuariosResponsables = User::where('responsable',1)->get();
        $referentesTecnicos = ReferenteTecnico::all();
        $usuariosDigitadores = User::has('getDocumentos')->get();
        $proveedores = Proveedor::all();
        $itemsPresupuestarios = ItemPresupuestario::all();

        return view('documento.index_rechazo_visto_bueno')->with('documentos',$documentos)
                                                          ->with('tiposDocumento',$tiposDocumento)
                                                          ->with('modalidadesCompra',$modalidadesCompra)
                                                          ->with('tiposAdjudicacion',$tiposAdjudicacion)
                                                          ->with('tiposInforme',$tiposInforme)
                                                          ->with('usuariosResponsables',$usuariosResponsables)
                                                          ->with('referentesTecnicos',$referentesTecnicos)
                                                          ->with('usuariosDigitadores',$usuariosDigitadores)
                                                          ->with('proveedores',$proveedores)
                                                          ->with('itemsPresupuestarios',$itemsPresupuestarios);
    }

    public function postFiltrarDocumentosRechazoVistoBueno(Request $request)
    {
        $fechaInicio = fecha_Y_m_d($request->input('filtro_fecha_inicio'));
        $fechaTermino = fecha_Y_m_d($request->input('filtro_fecha_termino'));

        set_time_limit(0);
        $documentos = Documento::with(['getProveedor','getTipoDocumento','getDevengos.getItemPresupuestario',
                                       'getReferenteTecnico'])
                               ->where('id_rechazo_visto_bueno','<>',null)
                               ->where('id_visto_bueno',null);

        if ( $request->input('filtro_fecha_inicio') != null || $request->input('filtro_fecha_termino') != null ) {
            $documentos = $documentos->whereHas('getRechazoVistoBueno', function ($query) use ($request, $fechaInicio, $fechaTermino) {
                if ( $request->input('filtro_fecha_inicio') != null) {
                    $query->where('created_at','>=',$fechaInicio.' 00:00:00');
                }

                if ( $request->input('filtro_fecha_termino') != null) {
                    $query->where('created_at','<=',$fechaTermino.' 23:59:59');
                }
                
            });
        }

        if ( $request->input('filtro_proveedor') != null ) {
            $documentos = $documentos->WhereIn('id_proveedor',$request->input('filtro_proveedor'));
        }

        if ( $request->input('filtro_tipo_documento') != null ) {
            $documentos = $documentos->WhereIn('id_tipo_documento',$request->input('filtro_tipo_documento'));
        }

        if ( $request->input('filtro_modalidad_compra') != null ) {
            $documentos = $documentos->WhereIn('id_modalidad_compra',$request->input('filtro_modalidad_compra'));
        }

        if ( $request->input('filtro_tipo_adjudicacion') != null ) {
            $documentos = $documentos->WhereIn('id_tipo_adjudicacion',$request->input('filtro_tipo_adjudicacion'));
        }

        if ( $request->input('filtro_tipo_informe') != null ) {
            $documentos = $documentos->WhereIn('id_tipo_informe',$request->input('filtro_tipo_informe'));
        }

        if ( $request->input('filtro_responsable') != null ) {
            $documentos = $documentos->WhereIn('id_responsable',$request->input('filtro_responsable'));
        }

        if ( $request->input('filtro_referente_tecnico') != null ) {
            $documentos = $documentos->WhereIn('id_referente_tecnico',$request->input('filtro_referente_tecnico'));
        }

        if ( $request->input('filtro_digitador') != null ) {
            $documentos = $documentos->WhereIn('id_digitador',$request->input('filtro_digitador'));
        }

        if ( $request->input('filtro_item_presupuestario') != null) {
            $documentos = $documentos->whereHas('getDevengos',function ($query) use ($request) {
                $query->WhereIn('id_item_presupuestario',$request->input('filtro_item_presupuestario'));
            });
        }

        if ( $request->input('filtro_numero_documento') != null ) {
            $documentos = $documentos->where('numero_documento', trim($request->input('filtro_numero_documento')) );
        }

        if ( $request->input('filtro_numero_documento_compra') != null ) {
            $documentos = $documentos->where('documento_compra', trim($request->input('filtro_numero_documento_compra')) );
        }

        $documentos = $documentos->get();

        $tiposDocumento = TipoDocumento::whereNotIn('id',[2,8])->get();
        $modalidadesCompra = ModalidadCompra::all();
        $tiposAdjudicacion = TipoAdjudicacion::all();
        $tiposInforme = TipoInforme::all();
        $usuariosResponsables = User::where('responsable',1)->get();
        $referentesTecnicos = ReferenteTecnico::all();
        $usuariosDigitadores = User::has('getDocumentos')->get();
        $proveedores = Proveedor::all();
        $itemsPresupuestarios = ItemPresupuestario::all();

        return view('documento.index_rechazo_visto_bueno')->with('documentos',$documentos)
                                                        ->with('tiposDocumento',$tiposDocumento)
                                                        ->with('modalidadesCompra',$modalidadesCompra)
                                                        ->with('tiposAdjudicacion',$tiposAdjudicacion)
                                                        ->with('tiposInforme',$tiposInforme)
                                                        ->with('usuariosResponsables',$usuariosResponsables)
                                                        ->with('referentesTecnicos',$referentesTecnicos)
                                                        ->with('usuariosDigitadores',$usuariosDigitadores)
                                                        ->with('proveedores',$proveedores)
                                                        ->with('itemsPresupuestarios',$itemsPresupuestarios)
                                                        //filtros
                                                        ->with('filtroFechaInicio',$request->input('filtro_fecha_inicio'))
                                                        ->with('filtroFechaTermino',$request->input('filtro_fecha_termino'))
                                                        ->with('filtroProveedor',$request->input('filtro_proveedor'))
                                                        ->with('filtroTipoDocumento',$request->input('filtro_tipo_documento'))
                                                        ->with('filtroModalidadCompra',$request->input('filtro_modalidad_compra'))
                                                        ->with('filtroTipoAdjudicacion',$request->input('filtro_tipo_adjudicacion'))
                                                        ->with('filtroTipoInforme',$request->input('filtro_tipo_informe'))
                                                        ->with('filtroResponsable',$request->input('filtro_responsable'))
                                                        ->with('filtroReferenteTecnico',$request->input('filtro_referente_tecnico'))
                                                        ->with('filtroDigitador',$request->input('filtro_digitador'))
                                                        ->with('filtroItems',$request->input('filtro_item_presupuestario'))
                                                        ->with('filtroNumeroDocumento',$request->input('filtro_numero_documento'))
                                                        ->with('filtroNumeroDocumentoCompra',$request->input('filtro_numero_documento_compra'));
    }

    public function getItemsPresupuestariosDocRecepcion($arregloIdsItems, $idDocRecepcion)
    {

        $idsItem = json_decode($arregloIdsItems);
        $docRecepcion = WsDocumento::with(['getWsItemsPresupuestarios'])->findOrFail($idDocRecepcion);
        $respuesta = array(
            'mensaje' => 'No se encuentran Items Presupuestarios de la recepción seleccionada',
            'estado' => 'error'
        );
        $itemsRecepcion = [];

        if ( count( $docRecepcion->getWsItemsPresupuestarios ) > 0 ) {

            foreach ( $docRecepcion->getWsItemsPresupuestarios as $item ) {

                $itemPresupuestario = ItemPresupuestario::whereNotIn('id', $idsItem);
                $codigosItems = explode('.', $item->item_presupuestario);
                foreach ( $codigosItems as $index => $codigo ) {

                    switch ($index) {
                        case 0:
                            $itemPresupuestario = $itemPresupuestario->where('titulo', $codigo);
                            // $itemPresupuestario = ItemPresupuestario::where('titulo', $codigo);
                            break;
                        case 1:
                            $itemPresupuestario = $itemPresupuestario->where('subtitulo', $codigo);
                            break;
                        case 2:
                            $itemPresupuestario = $itemPresupuestario->where('item', $codigo);
                            break;
                        case 3:
                            $itemPresupuestario = $itemPresupuestario->where('asignacion', $codigo);
                            break;
                        case 4:
                            $itemPresupuestario = $itemPresupuestario->where('subasignacion', $codigo);
                            break;
                    }

                }

                $itemPresupuestario = $itemPresupuestario->get();
                
                if ( $itemPresupuestario->count() == 1 ) {
                    $datos = array(
                        'mensaje' => 'Se Encuentra el Item Presupuestario',
                        'estado' => 'success',
                        'id_item' => $itemPresupuestario->first()->id,
                        'codigo' => $itemPresupuestario->first()->codigo(),
                        'clasificacionPresupuestario' => $itemPresupuestario->first()->clasificador_presupuestario,
                    );

                    $itemsRecepcion[] = $datos;
                }
            }

            if ( count($itemsRecepcion) >= 1 ) {
                $respuesta['mensaje'] = 'Se han encontrado Items Presupuestarios de la recepción seleccionada';
                $respuesta['estado'] = 'success';
                $respuesta['items'] = $itemsRecepcion;
            } else {
                $respuesta['mensaje'] = 'No concuerdan los códigos de los Items Presupuestarios de la recepción con los del sistema';
                $respuesta['estado'] = 'error';
            }
            
        }

        return response()->json($respuesta,200);
    }

    public function getDocumentosEliminados()
    {

        if ( ! \Entrust::hasRole(['propietario','administrador']) ) {
            return \Redirect::to('home');
        }

        $tiposDocumento = TipoDocumento::whereNotIn('id',[2,8])->get();
        $modalidadesCompra = ModalidadCompra::all();
        $tiposAdjudicacion = TipoAdjudicacion::all();
        $tiposInforme = TipoInforme::all();
        $usuariosResponsables = User::where('responsable',1)->get();
        $referentesTecnicos = ReferenteTecnico::all();
        $usuariosDigitadores = User::has('getDocumentos')->get();
        $proveedores = Proveedor::all();
        $itemsPresupuestarios = ItemPresupuestario::all();

        // Select para Vistos Buenos
        // $vistoBueno1 = new \stdClass;
        // $vistoBueno1->opcion = "Solicitado";
        // $vistoBueno2 = new \stdClass;
        // $vistoBueno2->opcion = "Rechazado";
        // $vistoBueno3 = new \stdClass;
        // $vistoBueno3->opcion = "Confirmado";

        // $vistosBuenos[] = $vistoBueno1;
        // $vistosBuenos[] = $vistoBueno2;
        // $vistosBuenos[] = $vistoBueno3;

        // Select para Fechas a utilizar en el filtro
        $fechaUtilizar1 = new \stdClass;
        $fechaUtilizar1->opcion = 'Ingreso';
        $fechaUtilizar2 = new \stdClass;
        $fechaUtilizar2->opcion = 'Recepción';

        $fechasUtilizar[] = $fechaUtilizar1;
        $fechasUtilizar[] = $fechaUtilizar2;

        $filtroFechaInicio = date("d/m/Y");
        $filtroFechaTermino = date("d/m/Y");

        return view('documento.index_eliminados')->with('tiposDocumento',$tiposDocumento)
                                                 ->with('modalidadesCompra',$modalidadesCompra)
                                                 ->with('tiposAdjudicacion',$tiposAdjudicacion)
                                                 ->with('tiposInforme',$tiposInforme)
                                                 ->with('usuariosResponsables',$usuariosResponsables)
                                                 ->with('referentesTecnicos',$referentesTecnicos)
                                                 ->with('usuariosDigitadores',$usuariosDigitadores)
                                                 ->with('proveedores',$proveedores)
                                                 ->with('itemsPresupuestarios',$itemsPresupuestarios)
                                                //  ->with('vistosBuenos',$vistosBuenos)
                                                 ->with('fechasUtilizar',$fechasUtilizar);
    }

    /**
     * Datatable Eliminados
     */
    public function dataTableEliminados(Request $request)
    {
        // dd($request->all());

        set_time_limit ( 0 );
        ini_set('max_execution_time', 0);

        $search = $request->get('search')['value'];
        $order_column = $request->get('order')[0]['column'];
        $order_dir = $request->get('order')[0]['dir'];
        
        // Por si es necesario arreglar el search
        // if ( $search == '' ) {
        //     $documentos = Documento::with(['getProveedor','getTipoDocumento','getDevengos.getItemPresupuestario',
        //                                'audits.user'])
        //                            ->where('fecha_ingreso','>=',date('Y-m').'-9 00:00:00');
        // } else {

        // }

        // $documentos = $documentos->get();
        
        $formJSON = json_decode($request->get('form'));
        $form = construyeFormulario($formJSON);
        
        $fechaInicio = fecha_Y_m_d($form->filtro_fecha_inicio);
        $fechaTermino = fecha_Y_m_d($form->filtro_fecha_termino);

        $documentos = Documento::with([
                                        'getProveedor' => function ($query) {
                                            $query->select('id', 'nombre', 'rut');
                                        },
                                        'getTipoDocumento' => function ($query) {
                                            $query->select('id', 'nombre');
                                        }
                                        ,'getDevengos.getItemPresupuestario',
                                        'getVistoBueno','getUnaRelacionRecepcionValidada',
                                        'getRelacionRecepcionesNoValidadas'
                                    ])
                                    ->onlyTrashed();
                            
        if ( $form->filtro_numero_documento == null ) {
            // Se filtra por las fechas

            if ( $form->filtro_fecha_inicio != null ) {

                if ( $form->filtro_fecha_a_utilizar == 'Ingreso' ) {
                    $documentos = $documentos->where('fecha_ingreso','>=', $fechaInicio.' 00:00:00');
                } elseif ( $form->filtro_fecha_a_utilizar == 'Recepción' ) {
                    $documentos = $documentos->where('fecha_recepcion','>=', $fechaInicio);
                }

            }

            if ( $form->filtro_fecha_termino != null ) {
                if ( $form->filtro_fecha_a_utilizar == 'Ingreso' ) {
                    $documentos = $documentos->where('fecha_ingreso','<=', $fechaTermino.' 23:59:59');
                } elseif ( $form->filtro_fecha_a_utilizar == 'Recepción' ) {
                    $documentos = $documentos->where('fecha_recepcion','<=', $fechaTermino);
                }
            }

        } else {
            // no se consideran las fechas, y se busca el numero del documento
            $documentos = $documentos->where('numero_documento', trim($form->filtro_numero_documento) );
        }
        
        if ( isset($form->filtro_proveedor) ) {
            $documentos = $documentos->WhereIn('id_proveedor',$form->filtro_proveedor);
        }

        if ( isset($form->filtro_tipo_documento) ) {
            $documentos = $documentos->WhereIn('id_tipo_documento',$form->filtro_tipo_documento);
        }

        if ( isset($form->filtro_modalidad_compra) ) {
            $documentos = $documentos->WhereIn('id_modalidad_compra',$form->filtro_modalidad_compra);
        }

        if ( isset($form->filtro_tipo_adjudicacion) ) {
            $documentos = $documentos->WhereIn('id_tipo_adjudicacion',$form->filtro_tipo_adjudicacion);
        }

        if ( isset($form->filtro_tipo_informe) ) {
            $documentos = $documentos->WhereIn('id_tipo_informe',$form->filtro_tipo_informe);
        }

        if ( isset($form->filtro_responsable) ) {
            $documentos = $documentos->WhereIn('id_responsable',$form->filtro_responsable);
        }

        if ( isset($form->filtro_referente_tecnico) ) {
            $documentos = $documentos->WhereIn('id_referente_tecnico',$form->filtro_referente_tecnico);
        }

        if ( isset($form->filtro_digitador) ) {
            $documentos = $documentos->WhereIn('id_digitador',$form->filtro_digitador);
        }

        if ( isset($form->filtro_item_presupuestario) ) {
            $documentos = $documentos->whereHas('getDevengos',function ($query) use ($form) {
                $query->WhereIn('id_item_presupuestario',$form->filtro_item_presupuestario);
            });
        }

        if ( $form->filtro_numero_documento_compra != null ) {
            $documentos = $documentos->where('documento_compra', trim($form->filtro_numero_documento_compra) );
        }

        // if ( $form->filtro_visto_bueno != null ) {

        //     if ( $form->filtro_visto_bueno == 'Solicitado' ) {
        //         //Solicitado
        //         $documentos = $documentos->where( 'id_visto_bueno', '<>', null );
        //     }

        //     if ( $form->filtro_visto_bueno == 'Rechazado' ) {
        //         //Rechazado
        //         $documentos = $documentos->where( 'id_rechazo_visto_bueno', '<>', null );
        //     }

        //     if ( $form->filtro_visto_bueno == 'Confirmado' ) {
        //         //Confirmado
        //         $documentos = $documentos->whereHas( 'getVistoBueno' , function ( $queryTwo ) {
        //             $queryTwo->where( 'id_registrador_memo_respuesta', '<>', null );
        //         });
        //     }

        // }

        $documentos = $documentos->get();

        $data = [];

        if ( $request->get('length') != -1 ) {
            $hasta = $request->get('start') + $request->get('length');
        } else {
            // Se quieren ver todos los datos
            $hasta = $request->get('start') + $documentos->count();
        }
        
        for( $i = $request->get('start') ; $i < $hasta ; $i++ ) {
            if ( isset($documentos[$i]) ) {
                $data[] = $documentos[$i]->getDatosDocumentoEliminado();
            }
        }

        // Se realiza la busqueda y se ordenan los datos
        if ( $documentos->count() > 0 ) {

            foreach ($data as $clave => $fila) {

                if ( $search != '' ) {
                    if ( in_array_r($search,$fila) ) {
                        // dd("lo encuentra",$fila,$search);
                        $columna[$clave] = $fila[$order_column];
                    } else {
                        // dd("NO lo encuentra",$fila,$search);
                        unset( $data[$clave] ); // quita el elemento del data
                    }
                } else {
                    $columna[$clave] = $fila[$order_column];
                }
                
            }

            
            // Se ordenan los datos, segun la columna seleccionada
            if ( $order_dir == 'asc' ) {
                array_multisort($columna, SORT_ASC, $data);
            } elseif ( $order_dir == 'desc' ) {
                array_multisort($columna, SORT_DESC, $data);
            }

        }
            

        $respuesta = array(
            "draw" => $request->get('draw'),
            "recordsTotal" => count($documentos),
            "recordsFiltered"=> count($documentos),
            "data" => $data
        );

        return json_encode($respuesta);
    }

    /**
     * Excel de documentos eliminados
     */
    public function postEliminadosExcel(Request $request)
    {
        set_time_limit ( 0 );
        ini_set('max_execution_time', 0);

        \Excel::create('SIFCON | Documentos Eliminados '.date('d-m-Y').'', function($excel) use($request) {

            $documentos = Documento::with(['getProveedor','getTipoDocumento','getDevengos.getItemPresupuestario',
                                           'getDevengos.getFolioSigfe', 'getRelacionRecepcionesNoValidadas'])
                                   ->onlyTrashed();

            $fechaInicio = fecha_Y_m_d($request->input('filtro_fecha_inicio'));
            $fechaTermino = fecha_Y_m_d($request->input('filtro_fecha_termino'));

            if ( $request->input('filtro_numero_documento') == null  ) {

                if ( $request->input('filtro_fecha_inicio') != null ) {

                    if ( $request->input('filtro_fecha_a_utilizar') == 'Ingreso' ) {
                        $documentos = $documentos->where('fecha_ingreso','>=', $fechaInicio.' 00:00:00');
                    } elseif ( $request->input('filtro_fecha_a_utilizar') == 'Recepción' ) {
                        $documentos = $documentos->where('fecha_recepcion','>=', $fechaInicio);
                    }

                }

                if ( $request->input('filtro_fecha_termino') != null ) {
                    
                    if ( $request->input('filtro_fecha_a_utilizar') == 'Ingreso' ) {
                        $documentos = $documentos->where('fecha_ingreso','<=', $fechaTermino.' 23:59:59');
                    } elseif ( $request->input('filtro_fecha_a_utilizar') == 'Recepción' ) {
                        $documentos = $documentos->where('fecha_recepcion','<=', $fechaTermino);
                    }

                }

            } else {
                $documentos = $documentos->where('numero_documento', trim($request->input('filtro_numero_documento')) );
            }
            

            if ( $request->input('filtro_proveedor') != null ) {
                $documentos = $documentos->WhereIn('id_proveedor',$request->input('filtro_proveedor'));
            }

            if ( $request->input('filtro_tipo_documento') != null ) {
                $documentos = $documentos->WhereIn('id_tipo_documento',$request->input('filtro_tipo_documento'));
            }

            if ( $request->input('filtro_modalidad_compra') != null ) {
                $documentos = $documentos->WhereIn('id_modalidad_compra',$request->input('filtro_modalidad_compra'));
            }

            if ( $request->input('filtro_tipo_adjudicacion') != null ) {
                $documentos = $documentos->WhereIn('id_tipo_adjudicacion',$request->input('filtro_tipo_adjudicacion'));
            }

            if ( $request->input('filtro_tipo_informe') != null ) {
                $documentos = $documentos->WhereIn('id_tipo_informe',$request->input('filtro_tipo_informe'));
            }

            if ( $request->input('filtro_responsable') != null ) {
                $documentos = $documentos->WhereIn('id_responsable',$request->input('filtro_responsable'));
            }

            if ( $request->input('filtro_referente_tecnico') != null ) {
                $documentos = $documentos->WhereIn('id_referente_tecnico',$request->input('filtro_referente_tecnico'));
            }

            if ( $request->input('filtro_digitador') != null ) {
                $documentos = $documentos->WhereIn('id_digitador',$request->input('filtro_digitador'));
            }

            if ( $request->input('filtro_item_presupuestario') != null) {
                $documentos = $documentos->whereHas('getDevengos',function ($query) use ($request) {
                    $query->WhereIn('id_item_presupuestario',$request->input('filtro_item_presupuestario'));
                });
            }

            if ( $request->input('filtro_numero_documento_compra') != null ) {
                $documentos = $documentos->where('documento_compra', trim($request->input('filtro_numero_documento_compra')) );
            }

            $documentos = $documentos->get();

            $excel->sheet('Documentos', function($sheet) use($documentos) {
            
                $sheet->row(1, [
                    'Estado', 
                    // 'Responsable',
                    'Factoring',
                    'Chile-Compra',
                    'M/Dev',
                    'Proveedor',
                    'RUT',
                    'Tipo Dcto.',
                    'N° Dcto.',
                    'F/Doc.',
                    'M/Doc.',
                    'Fecha RD',
                    'Mes RD',
                    'Item',
                    'ID SIGFE',
                    'Fecha ID',
                    'Devengado',
                    'Mecanismo de Adjudicación',
                    'Observación',
                    'Detalle',
                    'URL Factura',
                    'URL Recepción'
                ]);

                $totalDevengadoExcel = 0;
                $cuentaLineas = 1;
                foreach ( $documentos as $indexAux => $documento ) {
                    $items = '';
                    $totalDevengadoPorDocumento = 0;

                    foreach ( $documento->getDevengos as $index => $devengo) {
                        $cuentaLineas++;
                        $items = $devengo->getItemPresupuestario->codigo();
                        $totalDevengadoPorDocumento = $devengo->monto;
                        // para columna "devengado"
                        $devengado = '';
                        if ($documento->id_tipo_documento == 4 || $documento->id_tipo_documento == 10) {
                            $devengado .= '-'.$totalDevengadoPorDocumento;
                            $totalDevengadoExcel = $totalDevengadoExcel - $totalDevengadoPorDocumento;
                        } else {
                            $devengado .= $totalDevengadoPorDocumento;
                            $totalDevengadoExcel = $totalDevengadoExcel + $totalDevengadoPorDocumento;
                        }

                        // para columna factoring
                        $factoring = '';
                        if ($documento->getProveedorFactoring) {
                            $factoring .= $documento->getProveedorFactoring->nombre.' '.$documento->getProveedorFactoring->rut;
                        }

                        $mesSigfe = '';
                        $fechaSigfe = '';
                        $idSigfe = '';
                        $detalleSigfe = '';
                        if ($devengo->id_folio_sigfe != null) {
                            $mesSigfe = (int) explode("-",$devengo->getFolioSigfe->fecha_sigfe)[1];
                            $fechaSigfe = fecha_dmY($devengo->getFolioSigfe->fecha_sigfe);
                            $idSigfe = $devengo->getFolioSigfe->id_sigfe;
                            $detalleSigfe = $devengo->getFolioSigfe->detalle_sigfe;
                        }

                        $urlRecepcion = '';
                        if ( $documento->getUnaRelacionRecepcionValidada ) {
                            foreach ( $documento->getRelacionRecepcionesValidadas as $relacion ) {
                                if ( $relacion->getWsDocumento ) {
                                    if ( $relacion->getWsDocumento->getWsArchivos ) {
                                        foreach ($relacion->getWsDocumento->getWsArchivos as $archivo) {
                                            $urlRecepcion .= 'http://10.4.237.28/finanzas/'.$archivo->ubicacion.PHP_EOL;
                                        }
                                    }
                                }
                            }
                            
                        }

                        $sheet->row($cuentaLineas, [
                            $documento->getEstadoDocumento('excel'),
                            // $documento->getResponsable->name,
                            $factoring,
                            $documento->documento_compra,
                            $mesSigfe, // mes de la fecha sigfe
                            $documento->getProveedor->nombre,
                            $documento->getProveedor->rut,
                            $documento->getTipoDocumento->nombre,
                            $documento->numero_documento,
                            fecha_dmY($documento->fecha_documento),
                            (int) explode("-",$documento->fecha_documento)[1], // mes de la fecha del documento
                            fecha_dmY($documento->fecha_recepcion),
                            (int) explode("-",$documento->fecha_recepcion)[1], // mes de la fecha de recepcion del documento
                            $items,
                            $idSigfe,
                            $fechaSigfe,
                            $devengado,
                            $documento->getTipoAdjudicacion->nombre,
                            $documento->observacion,
                            $detalleSigfe,
                            $documento->getUrlFactura(),
                            $urlRecepcion
                        ]);
                    }
                }


                $cuentaLineas++;
                $sheet->row( $cuentaLineas , [
                    '', 
                    // '', 
                    '',
                    '',
                    '', 
                    '',
                    '',
                    '',
                    '',
                    '',
                    '', 
                    '',
                    '',
                    '',
                    '',
                    'TOTAL',
                    $totalDevengadoExcel,
                    '',
                    '',
                    '',
                    '',
                    ''
                ]);

        });

        })->export('xlsx');
    }

    public function getModalRecuperarDocumento($id)
    {
        $documento = Documento::with(['getProveedor','getTipoDocumento','getModalidadCompra',
                                      'getTipoAdjudicacion','getTipoInforme','getResponsable',
                                      'getReferenteTecnico','getDigitador','getDevengador',
                                      'getArchivos','getArchivos.getTipoArchivo','getDevengos',
                                      'getDevengos.getItemPresupuestario','getRelacionRecepcionesValidadas.getUsuarioResponsable'])
                              ->onlyTrashed()
                              ->findOrFail($id);
        

        return view('documento.modal_recuperar_documento')->with('documento',$documento);
    }

    public function postRecuperarDocumento(Request $request)
    {
        // Mensajes de los validadores
        $messages = [
            '_id.required'    => 'Debe ingresar el id Documento',
            'numeric' => 'El :attribute debe solo contener números',
            'max' => 'El :attribute no debe exeder los :max caracteres',
            'min' => 'El :attribute debe tener minimo :min caracteres',
            'date_format' => 'La :attribute no tiene el formato correcto: día/mes/año',
        ];

        //validador de los input del formulario
        $validator = \Validator::make($request->all(), [
            '_id' => 'required|numeric',
        ], $messages);

        if ( $validator->fails() ) {
            //return redirect()->back()->withInput($request->all)->withErrors($validator);
            return response()->json($validator->errors(),400);
        } else {
            
            $documento = Documento::onlyTrashed()->findOrFail( $request->get('_id') );
            $documento->restore();
            $documento->id_user_deleted = null;
            $documento->save();

            $datos = array(
                'mensaje_documento' => 'Documento recuperado correctamente',
                'estado_documento'  => 'success',
                'id'                => $documento->id,
                'datatable'         => 'documentos_eliminados'
            );
            
            return response()->json($datos,200);
        }

    }    

    public function getUpload()
    {
        return view('documento.index_masivo');
    }

    public function postUpload(Request $request)
    {
        set_time_limit ( 0 );
        ini_set('max_execution_time', 0);

        $archivo = $request->file('archivo');
        
        $excel = Excel::selectSheetsByIndex(0)->load($archivo)->get();

        

        $log = '';
        //$archivoLog = fopen("log-documentos-upload.txt", "w");
        $contadorDocumentoEncontrado = 0;
        $contadorDocumentoNoEncontrado = 0;
        $contadorProveedorNoEncontrado = 0;
        foreach ($excel as $key => $fila) {

            // Se busca el proveedor.
            $proveedor = Proveedor::where('rut', $fila->rut )->first();
            // dd($fila,$proveedor);

            if ( ! is_object($proveedor) ) {
                $contadorProveedorNoEncontrado++;
                $log .= ($key + 1).'.- No se encuentra el proveedor : '.$fila->rut.' '.$fila->apellidos_y_nombres.' <br>';
            } else {

                if ( $fila->tipo_dcto == "Carta Certificada Debito" ) {
                    $tipoDoc = 11;
                } else {
                    $tipoDoc = 10; // de credito
                }

                //Buscar documento, para actualizar info. o para crear
                $documento = Documento::where('id_proveedor', $proveedor->id )
                                      ->where('numero_documento', $fila->num_dcto ) 
                                      ->where('id_tipo_documento', $tipoDoc ) 
                                      ->first();
                                       
                if ( ! is_object($documento) ) {
                    $contadorDocumentoNoEncontrado++;

                    // dd($fila);
                   

                } else {
                    // dd($documento);
                    $contadorDocumentoEncontrado++;
                    $documento->actualizarTotalDocumentoActualizadoDelDocumentoRelacionado();
                    $documento->delete();
                    

                }

            }

        }
        
        $datos = array(
            'mensaje' => 'Carga de documentos exitoso.',
            'estado' => 'success',
            'docEncontrato' => $contadorDocumentoEncontrado,
            'docNoEncontrato' => $contadorDocumentoNoEncontrado
        );

        return response()->json($datos,200);
    }

    public function getModalImprimirDocumento($id)
    {
        $documento = Documento::with([
                                    'getArchivos.getTipoArchivo','getRelacionRecepcionesValidadas'
                                ])
                              ->findOrFail($id);

        /**
         * Orden de impresion para los documentos
         * 1° DTE y sus DTE relacionados, no considerar cartas certificadas (son ajustes contables) // id_tipo_archivo = 1, 2, 11, 12
         * 2° Acta de recepcion, Visto Bueno, ART, memo // Obtener de relacion con bodega -  id_tipo_archivo = 5, 4, 16
         * 3° Orden de Compra // id_tipo_archivo = 3
         * 4° Resolución de contrato // id_tipo_archivo = 6
         * 5° Saldo de contrato // id_tipo_archivo = 17
         * 6° Trazabilidad de documento (PDF que genera el sistema)
         */

        $archivosImprimir = collect();

        // 1°
        $dtesDocumento = Archivo::where('id_documento', $documento->id)->whereIn('id_tipo_archivo', [1,2,11,12])->get();
        foreach($dtesDocumento as $dte) {
            $archivosImprimir->push($dte);
        }

        $dtesDocumentoRelacionado = Archivo::where('id_documento', $documento->id_relacionado)->whereIn('id_tipo_archivo', [1,2,11,12])->get();
        foreach($dtesDocumentoRelacionado as $dte) {
            $archivosImprimir->push($dte);
        }

        foreach ($documento->getDocumentosRelacionados as $documentoRelacionado) {
            $dtesDocumentoRelacionado = Archivo::where('id_documento', $documentoRelacionado->id)->whereIn('id_tipo_archivo', [1,2,11,12])->get();
            foreach($dtesDocumentoRelacionado as $dte) {
                $archivosImprimir->push($dte);
            }
        }

        
        // 2°
        foreach ( $documento->getRelacionRecepcionesValidadas as $relacion ) {
            
            $segundaPrioridad = WsArchivo::where('id_documento', $relacion->id_ws_documento)->whereIn('id_tipo_archivo', [5,4,16])->get();
            foreach($segundaPrioridad as $archivo) {
                $archivosImprimir->push($archivo);
            }

        }

        $segundaPrioridad = Archivo::where('id_documento', $documento->id)->whereIn('id_tipo_archivo', [5,4,16])->get();
        foreach($segundaPrioridad as $archivo) {
            $archivosImprimir->push($archivo);
        }
        

        // 3°
        $terceraPrioridad = Archivo::where('id_documento', $documento->id)->whereIn('id_tipo_archivo', [3])->get();
        foreach($terceraPrioridad as $archivo) {
            $archivosImprimir->push($archivo);
        }

        // 4°
        $cuartaPrioridad = Archivo::where('id_documento', $documento->id)->whereIn('id_tipo_archivo', [6])->get();
        foreach($cuartaPrioridad as $archivo) {
            $archivosImprimir->push($archivo);
        }

        // 5°
        $quintaPrioridad = Archivo::where('id_documento', $documento->id)->whereIn('id_tipo_archivo', [17])->get();
        foreach($quintaPrioridad as $archivo) {
            $archivosImprimir->push($archivo);
        }

        // dd($archivosImprimir);


        return view('documento.modal_imprimir_documento')->with('documento',$documento)->with('archivosImprimir', $archivosImprimir);
    }

    public function getImprmirTodosLosArchivos($id)
    {
        
        $combinador = new Merger(new TcpdiDriver);

        concatenarArchivosDocumento::concatenar($id, $combinador);
        
        $salida = $combinador->merge();
        
        concatenarArchivosDocumento::eliminarArchivosDinamicos($id);

        $nombreArchivo = "combinado.pdf";
        header("Content-type:application/pdf");
        header("Content-disposition: inline; filename=$nombreArchivo");
        header("content-Transfer-Encoding:binary");
        header("Accept-Ranges:bytes");
        # Imprimir salida luego de encabezados
        echo $salida;

        exit;
        
    }
    
}
