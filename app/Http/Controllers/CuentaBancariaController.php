<?php namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\CuentaBancaria;
use App\Banco;

class CuentaBancariaController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
    */
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index()
    {
        // if (!\Entrust::can(['crear-cuenta-bancaria','editar-cuenta-bancaria','eliminar-cuenta-bancaria','ver-cuenta-bancaria','ver-general-cuenta-bancaria'])) {
        //     return \Redirect::to('home');
        // }

        $cuentas = CuentaBancaria::all();
        return view('cuenta_bancaria.index',compact('cuentas'));
    }

    public function create()
    {
        $bancos = Banco::all();
        return view('cuenta_bancaria.modal_crear_cuenta_bancaria', compact('bancos'));
    }

    public function store(Request $request)
    {
        // Mensajes del validador
        $messages = [
            'required' => 'Debe ingresar el :attribute',
            'numeric' => 'El :attribute debe solo contener números',
            'max' => 'El :attribute no debe exeder los :max caracteres',
            'min' => 'El :attribute debe tener minimo :min caracteres',
            'unique' => 'El valor :attribute ya existe en la base de datos',
        ];

        // Reglas del validador
        $rules = [
            'codigo' => 'required',
            'banco'  => 'required',
            'saldo'  => 'required'
        ];

        // Validador de los input del formulario
        $validator = \Validator::make($request->all(), $rules, $messages);

        //Si contiene errores se devuelven todos los errores, de lo contrario guarda en la base de datos
        if ( $validator->fails() ) {
            return response()->json($validator->errors(),400);
        } else {

            try {
                $cuentaBancaria = new CuentaBancaria($request);
            } catch (QueryException $e) {
                $datos = array(
                    'estado' => 'error',
                    'mensaje' => 'Existen problemas al momento de guardar la Cuenta Bancaria.',
                );
                return response()->json($datos,200);
            }

            $datos = array(
                'mensaje' => 'Se ha creado la Cuenta Bancaria.',
                'estado'  => 'success',
                'codigo'  => $cuentaBancaria->codigo,
                'banco'   => $cuentaBancaria->getBanco->nombre,
                'saldo'   => formatoMiles($cuentaBancaria->saldo),
                'id'      => $cuentaBancaria->id,
            );
        }

        return response()->json($datos,200);
    }

    public function show($id)
    {
        $cuenta = CuentaBancaria::findOrFail($id);
        return view('cuenta_bancaria.modal_ver_cuenta_bancaria',compact('cuenta'));
    }

    public function getModalEditar($id)
    {
        $cuenta = CuentaBancaria::findOrFail($id);
        $bancos = Banco::all();
        return view('cuenta_bancaria.modal_editar_cuenta_bancaria',compact('cuenta','bancos'));
    }

    public function postEditar(Request $request)
    {
        // dd('Holo',$request->all());
        // Mensajes del validador
        $messages = [
            'required' => 'Debe ingresar el :attribute',
            'numeric' => 'El :attribute debe solo contener números',
            'max' => 'El :attribute no debe exeder los :max caracteres',
            'min' => 'El :attribute debe tener minimo :min caracteres',
            'unique' => 'El valor :attribute ya existe en la base de datos',
        ];

        // Reglas del validador
        $rules = [
            'codigo' => 'required',
            'banco'  => 'required',
            'saldo'  => 'required'
        ];
        
        // Validador de los input del formulario
        $validator = \Validator::make($request->all(), $rules, $messages);

        //Si contiene errores se devuelven todos los errores, de lo contrario guarda en la base de datos
        if ( $validator->fails() ) {
            return response()->json($validator->errors(),400);
        } else {
            $cuentaBancaria = CuentaBancaria::findOrFail($request->input('_id'));
            $cuentaBancaria->editCuentaBancaria($request);

            $datos = array(
                'mensaje' => 'Edición exitosa de la Cuenta Contable.',
                'estado'  => 'success',
                'codigo'  => $cuentaBancaria->codigo,
                'banco'   => $cuentaBancaria->getBanco->nombre,
                'saldo'   => formatoMiles($cuentaBancaria->saldo),
                'id'      => $cuentaBancaria->id,
            );
        }

        return response()->json($datos,200);
    }

    public function getModalEliminar($id)
    {
        $cuenta = CuentaBancaria::findOrFail($id);
        return view('cuenta_bancaria.modal_eliminar_cuenta_bancaria', compact('cuenta'));
    }

    public function postEliminar(Request $request)
    {
        $cuentaBancaria = CuentaBancaria::find($request->input('_id'));
        if ( !is_object($cuentaBancaria) ) {
            $datos = array(
                'mensaje' => 'No se encuentra la Cuenta Bancaria en la Base de Datos',
                'estado'  => 'error',
            );
        } else {
            $cuentaBancaria->delete();

            $datos = array(
                'mensaje' => 'Se ha eliminado correctamente la Cuenta Bancaria.',
                'estado'  => 'success',
                'id'      => $cuentaBancaria->id,
            );
        }

        return response()->json($datos,200);
    }
}
