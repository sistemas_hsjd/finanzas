<?php namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests;
use Auth;
use App\ModalidadCompra;

class ModalidadCompraController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
    */
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index()
    {
        if (!\Entrust::can(['crear-modalidad-compra','editar-modalidad-compra','eliminar-modalidad-compra','ver-modalidad-compra','ver-general-modalidad-compra'])) {
            return \Redirect::to('home');
        }
        $modalidadesCompra = ModalidadCompra::all();
        return view('modalidad_compra.index')->with('modalidadesCompra',$modalidadesCompra);
    }

    public function getModalVer($idModalidad)
    {
        $modalidadCompra = ModalidadCompra::findOrFail($idModalidad);
        return view('modalidad_compra.modal_ver_modalidad_compra')->with('modalidadCompra',$modalidadCompra);
    }

    public function getModalEditar($idModalidad)
    {
        $modalidadCompra = ModalidadCompra::findOrFail($idModalidad);
        return view('modalidad_compra.modal_editar_modalidad_compra')->with('modalidadCompra',$modalidadCompra);
    }

    public function postEditar(Request $request)
    {
        // dd('Holo',$request->all());
        // Mensajes del validador
        $messages = [
            'required' => 'Debe ingresar el :attribute',
            'nombre.required' => 'Debe ingresar el Nombre',
            'numeric' => 'El :attribute debe solo contener números',
            'max' => 'El :attribute no debe exeder los :max caracteres',
            'min' => 'El :attribute debe tener minimo :min caracteres',
            'unique' => 'El valor :attribute ya existe en la base de datos',
        ];

        // Reglas del validador
        $rules = [
            'nombre' => 'required',
        ];
        
        // Validador de los input del formulario
        $validator = \Validator::make($request->all(), $rules, $messages);

        //Si contiene errores se devuelven todos los errores, de lo contrario guarda en la base de datos
        if ( $validator->fails() ) {
            return response()->json($validator->errors(),400);
        } else {
            $editModalidadCompra = ModalidadCompra::findOrFail($request->input('id_modalidad'));
            $editModalidadCompra->editModalidadCompra($request);

            $datos = array(
                'mensaje' => 'Edición exitosa del ítem presupuestario',
                'estado' => 'success',
                'nombre' => $editModalidadCompra->nombre,
                'predeterminado' => $editModalidadCompra->predeterminado,
                'id' => $editModalidadCompra->id,
            );
        }

        return response()->json($datos,200);
    }

    public function create()
    {
        return view('modalidad_compra.modal_crear_modalidad_compra');
    }

    public function store(Request $request)
    {
        // Mensajes del validador
        $messages = [
            'required' => 'Debe ingresar el :attribute',
            'nombre.required' => 'Debe ingresar el Nombre',
            'numeric' => 'El :attribute debe solo contener números',
            'max' => 'El :attribute no debe exeder los :max caracteres',
            'min' => 'El :attribute debe tener minimo :min caracteres',
            'unique' => 'El valor :attribute ya existe en la base de datos',
        ];

        // Reglas del validador
        $rules = [
            'nombre' => 'required',
        ];

        // Validador de los input del formulario
        $validator = \Validator::make($request->all(), $rules, $messages);

        //Si contiene errores se devuelven todos los errores, de lo contrario guarda en la base de datos
        if ( $validator->fails() ) {
            return response()->json($validator->errors(),400);
        } else {

            try {
                $newModalidadCompra = new ModalidadCompra($request);
            } catch (QueryException $e) {
                $datos = array(
                    'estado' => 'error',
                    'mensaje' => 'Existen problemas al momento de guardar la Modalidad de Compra',
                );
                return response()->json($datos,200);
            }

            $datos = array(
                'mensaje' => 'Se ha creado la Modalidad de Compra',
                'estado' => 'success',
                'nombre' => $newModalidadCompra->nombre,
                'predeterminado' => $newModalidadCompra->predeterminado,
                'id' => $newModalidadCompra->id,
            );
        }

        return response()->json($datos,200);
    }

    public function getModalEliminar($idModalidad)
    {
        $modalidadCompra = ModalidadCompra::findOrFail($idModalidad);
        return view('modalidad_compra.modal_eliminar_modalidad_compra')->with('modalidadCompra',$modalidadCompra);
    }

    public function postEliminar(Request $request)
    {
        $deleteModalidadCompra = ModalidadCompra::find($request->input('id_modalidad'));
        if ( !is_object($deleteModalidadCompra) ) {
            $datos = array(
                'mensaje' => 'No se encuentra la Modalidad de Compra en la Base de Datos',
                'estado' => 'error',
            );
        } else {
            $deleteModalidadCompra->id_user_deleted = Auth::user()->id;
            $deleteModalidadCompra->timestamps = false;
            $deleteModalidadCompra->save();
            $deleteModalidadCompra->delete();

            $datos = array(
                'mensaje' => 'Se ha eliminado correctamente la Modalidad de Compra.',
                'estado' => 'success',
                'id' => $deleteModalidadCompra->id,
            );
        }

        return response()->json($datos,200);
    }
}
