<?php namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\MedioPago;

class MedioPagoController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
    */
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index()
    {
        // if (!\Entrust::can(['crear-medio-pagon','editar-medio-pagon','eliminar-medio-pagon','ver-medio-pagon','ver-general-medio-pagon'])) {
        //     return \Redirect::to('home');
        // }

        $mediosPago = MedioPago::all();
        return view('medio_pago.index')->with('mediosPago', $mediosPago);
    }

    public function create()
    {
        return view('medio_pago.modal_crear_medio_pago');
    }

    public function store(Request $request)
    {
        // Mensajes del validador
        $messages = [
            'required' => 'Debe ingresar el :attribute',
            'nombre.required' => 'Debe ingresar el Nombre',
            'numeric' => 'El :attribute debe solo contener números',
            'max' => 'El :attribute no debe exeder los :max caracteres',
            'min' => 'El :attribute debe tener minimo :min caracteres',
            'unique' => 'El valor :attribute ya existe en la base de datos',
        ];

        // Reglas del validador
        $rules = [
            'nombre' => 'required',
        ];

        // Validador de los input del formulario
        $validator = \Validator::make($request->all(), $rules, $messages);

        //Si contiene errores se devuelven todos los errores, de lo contrario guarda en la base de datos
        if ( $validator->fails() ) {
            return response()->json($validator->errors(),400);
        } else {

            try {
                $medioPago = new MedioPago($request);
            } catch (QueryException $e) {
                $datos = array(
                    'estado' => 'error',
                    'mensaje' => 'Existen problemas al momento de guardar el Medio de Pago.',
                );
                return response()->json($datos,200);
            }

            $datos = array(
                'mensaje' => 'Se ha creado el Medio de Pago.',
                'estado' => 'success',
                'nombre' => $medioPago->nombre,
                'id' => $medioPago->id,
            );
        }

        return response()->json($datos,200);
    }

    public function show($id)
    {
        $medioPago = MedioPago::findOrFail($id);
        return view('medio_pago.modal_ver_medio_pago')->with('medioPago',$medioPago);
    }

    public function getModalEditar($id)
    {
        $medioPago = MedioPago::findOrFail($id);
        return view('medio_pago.modal_editar_medio_pago')->with('medioPago',$medioPago);
    }

    public function postEditar(Request $request)
    {
        // dd('Holo',$request->all());
        // Mensajes del validador
        $messages = [
            'required' => 'Debe ingresar el :attribute',
            'nombre.required' => 'Debe ingresar el Nombre',
            'numeric' => 'El :attribute debe solo contener números',
            'max' => 'El :attribute no debe exeder los :max caracteres',
            'min' => 'El :attribute debe tener minimo :min caracteres',
            'unique' => 'El valor :attribute ya existe en la base de datos',
        ];

        // Reglas del validador
        $rules = [
            'nombre' => 'required',
        ];
        
        // Validador de los input del formulario
        $validator = \Validator::make($request->all(), $rules, $messages);

        //Si contiene errores se devuelven todos los errores, de lo contrario guarda en la base de datos
        if ( $validator->fails() ) {
            return response()->json($validator->errors(),400);
        } else {
            $medioPago = MedioPago::findOrFail($request->input('_id'));
            $medioPago->nombre = trim($request->get('nombre'));
            $medioPago->save();

            $datos = array(
                'mensaje' => 'Edición exitosa del Medio de Pago.',
                'estado'  => 'success',
                'nombre'  => $medioPago->nombre,
                'id'      => $medioPago->id,
            );
        }

        return response()->json($datos,200);
    }

    public function getModalEliminar($id)
    {
        $medioPago = MedioPago::findOrFail($id);
        return view('medio_pago.modal_eliminar_medio_pago')->with('medioPago',$medioPago);
    }

    public function postEliminar(Request $request)
    {
        $medioPago = MedioPago::find($request->input('_id'));
        if ( !is_object($medioPago) ) {
            $datos = array(
                'mensaje' => 'No se encuentra el Medio de Pago en la Base de Datos.',
                'estado' => 'error',
            );
        } else {

            $medioPago->delete();

            $datos = array(
                'mensaje' => 'Se ha eliminado correctamente el Medio de Pago.',
                'estado'  => 'success',
                'id'      => $medioPago->id,
            );
        }

        return response()->json($datos,200);
    }


}
