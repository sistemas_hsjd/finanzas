<?php namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Proveedor;

class BuscadorController extends Controller
{
    public function buscaProveedorNombre(Request $request)
    {

        $term = trim($request->term);
        $valid_data = [];
        if ($term == "") {
          $valid_data[] = ['id' => "",'text' => "Ingresar Datos"];
          return json_encode($valid_data);
        }
  
        $proveedores = Proveedor::where('nombre','LIKE',"%".$term."%")->take(10)->get();
  
        if (count($proveedores) != 0) {
          foreach ($proveedores as $key => $proveedor) {
            $valid_data[] = ['id' => $proveedor->id,'text' => mayusculas_acentos($proveedor->nombre)];
          }
        } else {
          $valid_data[] = ['id' => "",'text' => 'Sin Resultados'];
        }
  
        return json_encode($valid_data);
    }

    public function buscaProveedorRut(Request $request)
    {

        $term = trim($request->term);
        $valid_data = [];
        if ($term == "") {
          $valid_data[] = ['id' => "",'text' => "Ingresar Datos"];
          return json_encode($valid_data);
        } else {
          $term = str_replace('.','',$term);
        }
  
        $proveedores = Proveedor::where('rut','LIKE',"%".$term."%")->take(10)->get();
  
        if (count($proveedores) != 0) {
          foreach ($proveedores as $key => $proveedor) {
            $valid_data[] = ['id' => $proveedor->id,'text' => $proveedor->formatRut()];
          }
        } else {
          $valid_data[] = ['id' => "",'text' => 'Sin Resultados'];
        }
  
        return json_encode($valid_data);
    }

    public function buscaProveedorNombreRut(Request $request)
    {

        $term = trim($request->term);
        $valid_data = [];
        if ($term == "") {
          $valid_data[] = ['id' => "",'text' => "Ingresar Datos"];
          return json_encode($valid_data);
        } else {
          $term = str_replace('.','',$term);
        }
  
        $proveedores = Proveedor::where('rut','LIKE',"%".$term."%")
                                ->orWhere('nombre','LIKE',"%".$term."%")
                                ->take(10)->get();
  
        if (count($proveedores) != 0) {
          foreach ($proveedores as $key => $proveedor) {
            $valid_data[] = ['id' => $proveedor->id,'text' => $proveedor->formatRut()." ".mayusculas_acentos($proveedor->nombre)];
          }
        } else {
          $valid_data[] = ['id' => "",'text' => 'Sin Resultados'];
        }
  
        return json_encode($valid_data);
    }
}
