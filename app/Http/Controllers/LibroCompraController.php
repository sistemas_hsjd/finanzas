<?php namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests;
use Auth;
use App\Documento;
use App\TipoDocumento;
use App\Proveedor;

class LibroCompraController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
    */
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function getUpload()
    {
        return view('libro_compra.index_masivo');
    }

    public function postUpload(Request $request)
    {
        set_time_limit ( 0 );
        ini_set('max_execution_time', 0);

        $archivo = $request->file('archivo');
        $excel = \Excel::selectSheetsByIndex(0)->load($archivo)->get(); 
        // 4 Enero
        // 15 Diciembre

        $numDocEncontrados = 0;
        $numDocNoEncontrados = 0;
        $numNoEncontradoTipoDocProv = 0;
        $datosTablaEncontrados = array();
        $datosTablaNoEncontrados = array();
        $datosTablaNoEncontradoTipoDocProv = array();
        foreach ($excel as $key => $fila) {

            $estadoDoc = '';
            $totalDoc = '$ ';
            $tipoDoc = TipoDocumento::where('nombre','LIKE','%'.trim($fila->tipo_dcto).'%')->first();
            $proveedor = Proveedor::where('rut', trim($fila->rut) )->first();

            if ( is_object($tipoDoc) && is_object($proveedor) ) {

                $fechaDoc = fecha_Y_m_d( trim($fila->fdoc) );
                $documento = Documento::with([
                                        'getProveedor' => function($query) {
                                            $query->select('id', 'nombre', 'rut');
                                        },'getTipoDocumento' => function($query) {
                                            $query->select('id', 'nombre');
                                        }
                                        ])
                                      ->where('id_proveedor', $proveedor->id)
                                      ->where('id_tipo_documento', $tipoDoc->id)
                                      ->where('numero_documento', trim($fila->numero_documento) )
                                      ->where('fecha_documento', $fechaDoc)
                                      ->where('total_documento', formato_entero( trim($fila->total) ) )
                                      ->select(
                                                // Datos para actualizar
                                                'id_sigfe','fecha_sigfe','year_sigfe','id_estado',
                                                // Datos para mostrar en la tabla
                                                'id', 'id_proveedor', 'id_tipo_documento','numero_documento',
                                                'total_documento'
                                                )
                                      ->first();
                
                if ( is_object($documento) ) {

                    $documento->id_sigfe = trim($fila->id_sigfe);
                    $documento->fecha_sigfe = fecha_Y_m_d( trim($fila->fecha_id) );
                    $documento->year_sigfe = fecha_Y_m_d( trim($fila->fecha_id) );
                    $documento->id_estado = 2;
                    $documento->save();

                    $estadoDoc .= 'Actualizado';
                    $totalDoc .= formatoMiles($documento->total_documento);
                    $docJson = array(
                        'DT_RowID' => $documento->id,
                        $estadoDoc,
                        $documento->getProveedor->rut,
                        $documento->getProveedor->nombre,
                        $documento->getTipoDocumento->nombre,
                        $documento->numero_documento,
                        $totalDoc,
                        $documento->id_sigfe,
                        fecha_dmY($documento->fecha_sigfe)
                    );

                    $datosTablaEncontrados[] = $docJson;
                    $numDocEncontrados++;

                    // dd('Encuentra doc', $fila, $documento, $documento->getProveedor->nombre);
                    
                } else {
                    
                    $estadoDoc .= 'No Encontrado';
                    $totalDoc .= formato_entero( trim($fila->total) );
                    $nombreProveedor = trim($fila->proveedor);
                    
                    $docJson = array(
                        'DT_RowID' => 'X',
                        $estadoDoc,
                        trim($fila->rut),
                        $nombreProveedor,
                        trim($fila->tipo_dcto),
                        trim($fila->numero_documento),
                        $totalDoc,
                        formato_entero( trim($fila->id_sigfe) ),
                        fecha_dmY( trim($fila->fecha_id) )
                    );

                    $datosTablaNoEncontrados[] = $docJson;
                    $numDocNoEncontrados++;

                }                                
                
            } else {
                // dd('No encuentra tipo documento o proveedor',$fila,$fila->tipo_dcto,$tipoDoc,$proveedor);

                $estadoDoc .= 'No Encontrado Tipo Documento y/o Proveedor';
                $totalDoc .= formato_entero( trim($fila->total) );
                $nombreProveedor = trim($fila->proveedor);
                
                $docJson = array(
                    'DT_RowID' => 'Z',
                    $estadoDoc,
                    trim($fila->rut),
                    $nombreProveedor,
                    trim($fila->tipo_dcto),
                    trim($fila->numero_documento),
                    $totalDoc,
                    formato_entero( trim($fila->id_sigfe) ),
                    fecha_dmY( trim($fila->fecha_id) )
                );

                $datosTablaNoEncontradoTipoDocProv[] = $docJson;
                $numNoEncontradoTipoDocProv++;
            }

            
        }

        $resumen = '<strong>';
        $resumen .= "Archivo Utilizado : ".$archivo->getClientOriginalName()."<br>";
        $resumen .= 'Documentos Encontrados : '.formatoMiles($numDocEncontrados).'<br>';
        $resumen .= 'Documentos No Encontrados : '.formatoMiles($numDocNoEncontrados).'<br>';
        $resumen .= 'Documentos No Encontrados por Tipo Documento y/o Proveedor : '.formatoMiles($numNoEncontradoTipoDocProv).'<br>';
        $resumen .= '</strong>';

        $datos = array(
            'mensaje' => 'Carga Libro de Compra exitosa.',
            'estado' => 'success',
            'datosTablaEncontrados' => $datosTablaEncontrados,
            'datosTablaNoEncontrados' => $datosTablaNoEncontrados,
            'datosTablaNoEncontradoTipoDocProv' => $datosTablaNoEncontradoTipoDocProv,
            'resumen' => $resumen,
        );

        return response()->json($datos,200);
    }
}
