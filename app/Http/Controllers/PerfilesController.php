<?php namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests;

use Auth;
// use App\User;
use App\Role;
use App\Permission;

class PerfilesController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
    */
    public function __construct()
    {
        $this->middleware('auth');
    }
    
    public function index()
    {
        if (!\Entrust::hasRole(['propietario','administrador'])) {
            return \Redirect::to('home');
        }

        $perfiles = Role::with(['permissions','users'])->get();

        return view('perfil.index')->with('perfiles',$perfiles);
    }

    public function getModalEditar($id)
    {
        $perfil = Role::with(['permissions'])->findorFail($id);
        $arrayAux = array();
        foreach ($perfil->permissions as $permiso) {
            $arrayAux[] = $permiso->id;
        }
        $permisos = Permission::whereNotIn('id',$arrayAux)->get();
        return view('perfil.modal_editar_perfil')->with('perfil',$perfil)->with('permisos',$permisos);
    }

    public function postEditar(Request $request)
    {
        //  dd('editar perfil',$request->all());
        // Mensajes del validador
        $messages = [
            'required' => 'Debe ingresar el :attribute',
            'numeric' => 'El :attribute debe solo contener números',
            'max' => 'El :attribute no debe exeder los :max caracteres',
            'min' => 'El :attribute debe tener minimo :min caracteres',
            'unique' => 'El valor :attribute ya existe en la base de datos',
            'date_format' => 'La :attribute no tiene el formato correcto: día/mes/año',
        ];

        // Reglas del validador
        $rules = [
            'permisos' => 'required',
            'nombre_visual' => 'required',
            'nombre' => 'required',
        ];

        // Validador de los input del formulario
        $validator = \Validator::make($request->all(), $rules, $messages);

        //Si contiene errores se devuelven todos los errores, de lo contrario guarda en la base de datos
        if ( $validator->fails() ) {
            return response()->json($validator->errors(),400);
        } else {
            //dd('todito bien',$request->all());
            $perfil = Role::findOrFail($request->input('_id'));
            $perfil->display_name = trim($request->input('nombre_visual'));
            $perfil->save();
            //Se eliminan todos los permisos relacionados
            $perfil->permissions()->sync([]); // Delete relationship data

            // Se recorren los permisos y se agregan al perfil
            foreach ($request->input('permisos') as $key => $idPermiso) {
                $perfil->attachPermissions(array((int)$idPermiso));
            }

            $datos = array(
                'mensaje' => 'Edición exitosa del Perfil '.$perfil->display_name,
                'estado' => 'success',                
                'id' => $perfil->id,
            );
        }

        return response()->json($datos,200);
    }
    
    public function getModalVer($id)
    {
        $perfil = Role::with(['permissions'])->findorFail($id);
        return view('perfil.modal_ver_perfil')->with('perfil',$perfil);
    }

    public function getCreate()
    {
        $permisos = Permission::all();
        return view('perfil.modal_crear_perfil')->with('permisos',$permisos);
    }

    public function postCreate(Request $request)
    {
        //  dd('Crear perfil',$request->all());
        // Mensajes del validador
        $messages = [
            'required' => 'Debe ingresar el :attribute',
            'numeric' => 'El :attribute debe solo contener números',
            'max' => 'El :attribute no debe exeder los :max caracteres',
            'min' => 'El :attribute debe tener minimo :min caracteres',
            'unique' => 'El valor :attribute ya existe en la base de datos',
            'date_format' => 'La :attribute no tiene el formato correcto: día/mes/año',
        ];

        // Reglas del validador
        $rules = [
            'permisos' => 'required',
            'nombre_visual' => 'required',
            'nombre' => 'required',
        ];

        // Validador de los input del formulario
        $validator = \Validator::make($request->all(), $rules, $messages);

        //Si contiene errores se devuelven todos los errores, de lo contrario guarda en la base de datos
        if ( $validator->fails() ) {
            return response()->json($validator->errors(),400);
        } else {
            // dd('todito bien',$request->all());

            $perfilAux = Role::where( 'name',trim( $request->input('nombre') ) )->first();

            if ( is_object($perfilAux) ) {
                $datos = array(
                    'mensaje' => 'Ya existe un Perfil con el nombre "'.$perfilAux->name.'"',
                    'estado' => 'error',
                );  
            } else {
                $perfil = new Role();
                $perfil->name = trim( $request->input('nombre') );
                $perfil->display_name = trim($request->input('nombre_visual'));
                $perfil->save();

                // Se recorren los permisos y se agregan al perfil
                foreach ($request->input('permisos') as $key => $idPermiso) {
                    $perfil->attachPermissions(array( (int)$idPermiso) );
                }

                $datos = array(
                    'mensaje' => 'Registro exitoso del Nuevo Perfil ',
                    'estado' => 'success',
                );
            }
            
        }
        return response()->json($datos,200);
    }

    public function getEliminar($id)
    {
        $perfil = Role::with(['permissions'])->findorFail($id);
        return view('perfil.modal_eliminar_perfil')->with('perfil',$perfil);
    }

    public function postEliminar(Request $request)
    {
        //  dd('Crear perfil',$request->all());
        // Mensajes del validador
        $messages = [
            'required' => 'Debe ingresar el :attribute',
            'numeric' => 'El :attribute debe solo contener números',
            'max' => 'El :attribute no debe exeder los :max caracteres',
            'min' => 'El :attribute debe tener minimo :min caracteres',
            'unique' => 'El valor :attribute ya existe en la base de datos',
            'date_format' => 'La :attribute no tiene el formato correcto: día/mes/año',
        ];

        // Reglas del validador
        $rules = [
            'nombre_visual' => 'required',
            'nombre' => 'required',
        ];

        // Validador de los input del formulario
        $validator = \Validator::make($request->all(), $rules, $messages);

        //Si contiene errores se devuelven todos los errores, de lo contrario guarda en la base de datos
        if ( $validator->fails() ) {
            return response()->json($validator->errors(),400);
        } else {

            //dd('todito bien',$request->all());
            $perfil = Role::findOrFail( $request->input('_id') );
            
            //Se eliminan todos los permisos relacionados
            $perfil->permissions()->sync([]); // Delete relationship data
            $perfil->delete();
            $datos = array(
                'mensaje' => 'Se ha Eiminado exitosamente el Perfil',
                'estado' => 'success',
            );
            
        }

        return response()->json($datos,200);
    }
}
