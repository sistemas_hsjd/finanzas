<?php namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Auth;
use Mpdf\Mpdf;
use App\Traits\saldoContrato;

use App\Contrato;
use App\Unidad;
use App\Profesion;
use App\TipoArchivo;
use App\Archivo;
use App\TipoPrestacion;
use App\Categoria;
use App\TipoAdjudicacion;
use App\ReferenteTecnico;
use App\OrdenCompra;
use App\Documento;
use App\ArchivoAcepta;
use App\DocumentoContratoOrdenCompra;
use App\WsDocumento;

class ContratosController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
    */
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index()
    {
        // if (!\Entrust::can(['crear-contrato','editar-contrato','eliminar-contrato','ver-contrato','ver-general-contrato'])) {
        //     return \Redirect::to('home');
        // }

        
        return view('contrato.index');

    }

    public function dataTableIndex(Request $request)
    {

        set_time_limit ( 0 );
        ini_set('max_execution_time', 0);

        $search = $request->get('search')['value'];
        $order_column = $request->get('order')[0]['column'];
        $order_dir = $request->get('order')[0]['dir'];

        $form = construyeFormulario( json_decode($request->get('form')) );
        
        // $fechaInicio  = fecha_Y_m_d($form->filtro_fecha_inicio);
        // $fechaTermino = fecha_Y_m_d($form->filtro_fecha_termino);

        $contratos = Contrato::all();

        $data = [];

        foreach ( $contratos as $contrato ) {
            $data[] = $contrato->getDatosIndex();
        }

        $respuesta = array(
            "data" => $data
        );

        return json_encode($respuesta);

    }

    public function create()
    {
        $unidades = Unidad::all();
        $profesiones = Profesion::all();
        $tiposArchivo = TipoArchivo::all();
        $tiposPrestacion = TipoPrestacion::all();
        $categorias = Categoria::all();
        $tiposAdjudicacion = TipoAdjudicacion::all();
        $referentes = ReferenteTecnico::all();
        
        return view('contrato.modal_crear')
               ->with('unidades', $unidades)
               ->with('profesiones', $profesiones)
               ->with('tiposArchivo', $tiposArchivo)
               ->with('tiposPrestacion', $tiposPrestacion)
               ->with('categorias', $categorias)
               ->with('tiposAdjudicacion', $tiposAdjudicacion)
               ->with('referentes', $referentes);
    }

    public function store(Request $request)
    {
        // dd('creando contrato', $request->all());

        // Validar segun el tipo de contrato seleccionado

        //mensajes de los validadores
        $messages = [
            'required'    => 'Debe ingresar el :attribute',
            'numeric'     => 'El :attribute debe solo contener números',
            'max' => 'El :attribute no debe exeder los :max caracteres',
            'min' => 'El :attribute debe tener minimo :min caracteres',
            'date_format'        => 'La :attribute no tiene el formato correcto: día/mes/año',
            'reemplaza.required' => 'Debe seleccionar una factura'
        ];

        $rules = array(
            'tipo_contrato'    => 'required|numeric',
            'proveedor'        => 'required|numeric',
            'resolucion'       => 'required|numeric',
            'fecha_resolucion' => 'required|date_format:"d/m/Y"',
            'inicio_vigencia'  => 'required|date_format:"d/m/Y"',
            'fin_vigencia'     => 'required|date_format:"d/m/Y"',
            'monto_contrato'   => 'required',
            'tipo_adjudicacion' => 'required|numeric',
            'detalle_contrato'  => 'required'
        );

        if ( $request->input('tipo_contrato') == 1 ) {
            
            if ( ! $request->input('por_procedimiento') ) {

                $rules['valor_hora'] = 'required';

            }

            $rules['unidad']       = 'required|numeric';
            $rules['profesion']    = 'required|numeric';
            // $rules['categoria']    = 'numeric';
            $rules['prestacion']   = 'required|numeric';
            $rules['sueldo_horas'] = 'required';

        } elseif ( $request->input('tipo_contrato') == 2 ) {

            // $rules['licitacion'] = 'required';
            $rules['referente_tecnico'] = 'required|numeric';
            // Validar los datos de las boletas, si se ingresa alguno
            if ( $request->input('numero_boleta_garantia') || $request->input('monto_boleta_garantia') || $request->input('inicio_vigencia_boleta_garantia') || $request->input('fin_vigencia_boleta_garantia') ) {

                $rules['numero_boleta_garantia']          = 'required|numeric';
                $rules['monto_boleta_garantia']           = 'required';
                $rules['inicio_vigencia_boleta_garantia'] = 'required|date_format:"d/m/Y"';
                $rules['fin_vigencia_boleta_garantia']    = 'required|date_format:"d/m/Y"';

            }

        }

        //validador de los input del formulario
        $validator = \Validator::make($request->all(), $rules, $messages);

        //Si contiene errores se devuelven todos los errores, de lo contrario guarda en la base de datos
        if ( $validator->fails() ) {

            return response()->json($validator->errors(), 400);

        } else {

            $datos = array(
                'estado' => ''
            );

            // Validar que no se repita la resolucion por año.
            $contrato = Contrato::where('resolucion', formato_entero($request->input('resolucion')) )
                                ->where('fecha_resolucion', '>=', fecha_Y($request->input('fecha_resolucion')).'-01-01' )
                                ->where('fecha_resolucion', '<=', fecha_Y($request->input('fecha_resolucion')).'-12-31' )
                                ->first();

            if ( is_object($contrato) ) {

                $datos['mensaje'] = 'La resolución ya se encuentra en el sistema.';
                $datos['estado'] = 'error';

            } else {

                // validar la suma de monto_preventivo y monto_correctivo, debe ser igual a la suma del contrato (convenio)
                if ( $request->input('tipo_contrato') == 2 ) {

                    if ( $request->input('monto_preventivo') != 0  || $request->input('monto_correctivo') != 0 ) {

                        if ( formato_entero( $request->input('monto_contrato') ) != formato_entero( $request->input('monto_preventivo') ) + formato_entero( $request->input('monto_correctivo') ) ){

                            $datos['mensaje'] = 'El monto del contrato debe ser igual a la suma de los montos preventivo y correctivo';
                            $datos['estado'] = 'error';

                        }
                        
                    }

                }
                
                if ( $datos['estado'] != 'error' ) {

                
                    $contrato = new Contrato;
                    /**
                     * Obligatorios
                     */
                    $contrato->id_tipo_contrato = $request->input('tipo_contrato');
                    $contrato->id_proveedor = $request->input('proveedor');
                    $contrato->id_tipo_adjudicacion = $request->input('tipo_adjudicacion');
                    $contrato->id_referente_tecnico = $request->input('referente_tecnico');
                    $contrato->resolucion = formato_entero( $request->input('resolucion') );
                    $contrato->fecha_resolucion = fecha_Y_m_d( $request->input('fecha_resolucion') );
                    $contrato->inicio_vigencia = fecha_Y_m_d( $request->input('inicio_vigencia') );
                    $contrato->fin_vigencia = fecha_Y_m_d( $request->input('fin_vigencia') );
                    $contrato->monto_contrato = formato_entero( $request->input('monto_contrato') );
                    $contrato->saldo_contrato = $contrato->monto_contrato;
                    $contrato->detalle_contrato = trim( $request->input('detalle_contrato') );

                    /**
                     * Si es tipo contrato 1
                     */
                    if ( $request->input('tipo_contrato') == 1 ) {

                        $contrato->id_unidad = ( $request->input('unidad') ) ? $request->input('unidad') : null;
                        
                        $contrato->id_profesion = ( $request->input('profesion') ) ? $request->input('profesion') : null;

                        $contrato->id_tipo_prestacion = ( $request->input('prestacion') ) ? $request->input('prestacion') : null;

                        $contrato->id_categoria = ( $request->input('categoria') ) ? $request->input('categoria') : null;

                        $contrato->addemdum = ( $request->input('addemdum') ) ? formato_entero( $request->input('addemdum') ) : null;

                        $contrato->fecha_addemdum = ( $request->input('fecha_addemdum') ) ? fecha_Y_m_d( $request->input('fecha_addemdum') ) : null;

                        $contrato->sueldo_horas = ( $request->input('sueldo_horas') ) ? $request->input('sueldo_horas') : null;
                        
                        $contrato->valor_hora = ( $request->input('valor_hora') ) ? formato_entero( $request->input('valor_hora') ) : null;

                        $contrato->por_procedimiento = ( $request->input('por_procedimiento') ) ? $request->input('por_procedimiento') : 0;

                    } else {

                        $contrato->id_unidad = null;
                        
                        $contrato->id_profesion = null;

                        $contrato->id_tipo_prestacion = null;

                        $contrato->id_categoria = null;

                        $contrato->addemdum = null;

                        $contrato->fecha_addemdum = null;

                        $contrato->sueldo_horas = null;
                        
                        $contrato->valor_hora = null;

                        $contrato->por_procedimiento = 0;

                    }

                    /**
                     * Si es tipo contrato 2
                     */
                    if ( $request->input('tipo_contrato') == 2 ) {

                        $contrato->monto_preventivo = ( $request->input('monto_preventivo') ) ? formato_entero( $request->input('monto_preventivo') ) : null;

                        $contrato->monto_correctivo = ( $request->input('monto_correctivo') ) ? formato_entero( $request->input('monto_correctivo') ) : null;

                        $contrato->saldo_preventivo = $contrato->monto_preventivo;
                        $contrato->saldo_correctivo = $contrato->monto_correctivo;

                        $contrato->licitacion = ( $request->input('licitacion') ) ? trim( $request->input('licitacion') ) : null;
                        
                        $contrato->cuotas = ( $request->input('cuotas') ) ? $request->input('cuotas') : 0;

                        $contrato->numero_boleta_garantia = ( $request->input('numero_boleta_garantia') ) ? formato_entero( $request->input('numero_boleta_garantia') ) : null;

                        $contrato->monto_boleta_garantia = ( $request->input('monto_boleta_garantia') ) ? formato_entero( $request->input('monto_boleta_garantia') ) : null;

                        $contrato->inicio_vigencia_boleta_garantia = ( $request->input('inicio_vigencia_boleta_garantia') ) ? fecha_Y_m_d( $request->input('inicio_vigencia_boleta_garantia') ) : null;

                        $contrato->fin_vigencia_boleta_garantia = ( $request->input('fin_vigencia_boleta_garantia') ) ? fecha_Y_m_d( $request->input('fin_vigencia_boleta_garantia') ) : null;

                    }  else {

                        $contrato->monto_preventivo = null;

                        $contrato->monto_correctivo = null;

                        $contrato->saldo_preventivo = $contrato->monto_preventivo;
                        $contrato->saldo_correctivo = $contrato->monto_correctivo;

                        $contrato->licitacion = null;
                        
                        $contrato->cuotas = 0;

                        $contrato->numero_boleta_garantia = null;

                        $contrato->monto_boleta_garantia = null;

                        $contrato->inicio_vigencia_boleta_garantia = null;

                        $contrato->fin_vigencia_boleta_garantia = null;

                    }

                    $contrato->id_user_created = Auth::user()->id;
                    $contrato->save();

                    $archivo = $request->file('archivo');
                    if ( $archivo != null ) {

                        $nombreOriginalArchivo = str_replace('.pdf','',$archivo->getClientOriginalName() );
                        $arreglo_nombre = explode(".", $archivo->getClientOriginalName() );
                        $extension = strtolower( end( $arreglo_nombre ) );

                        if ($extension != 'pdf') {

                            $datos['mensaje_archivo'] = 'Extension incorrecta del archivo';
                            $datos['estado_archivo'] = 'error';

                        } else {

                            // Se trabajan las rutas del archivo, se guarda en el proyecto al final de la funcion
                            $nombre = $nombreOriginalArchivo.'_'.date('Y_m_d_G_i_s');
                            $rutaArchivo = '/contrato/'.$contrato->id.'/'.$nombre.'.pdf';
                            $pathArchivo = public_path().'/contrato/'.$contrato->id.'/';

                            $archivoContrato = new Archivo();
                            $archivoContrato->id_contrato = $contrato->id;
                            $archivoContrato->id_tipo_archivo = $request->input('tipo_archivo');

                            $archivoContrato->nombre = $nombre;
                            $archivoContrato->nombre_original = $nombreOriginalArchivo;
                            $archivoContrato->ubicacion = "contrato/".$contrato->id."/".$archivoContrato->nombre.".pdf";
                            $archivoContrato->extension = "pdf";
                            $archivoContrato->peso = $archivo->getSize();
                            $archivoContrato->save();

                            $datos['mensaje_archivo'] = 'Se ha guardado correctamente el archivo.';
                            $datos['estado_archivo'] = 'success';

                            $archivo->move( $pathArchivo , $nombre.'.pdf' ); // Se guarda el archivo

                        }
                        
                    } else {
                        
                        $datos['mensaje_archivo'] = '';
                        $datos['estado_archivo'] = '';

                    }

                    $datos['mensaje'] = 'Se ha guardado correctamente el Contrato.';
                    $datos['estado'] = 'success';

                }

            }

        }

        return response()->json($datos, 200);

    }

    public function getModalEditar($id)
    {
        $contrato = Contrato::with([
                        'getDocumentosContrato.getDocumento',
                        ])
                        ->findOrFail($id);
        
        $unidades = Unidad::all();
        $profesiones = Profesion::all();
        $tiposArchivo = TipoArchivo::all();
        $tiposPrestacion = TipoPrestacion::all();
        $categorias = Categoria::all();
        $tiposAdjudicacion = TipoAdjudicacion::all();
        $referentes = ReferenteTecnico::all();
        
        return view('contrato.modal_editar')
               ->with('unidades', $unidades)
               ->with('profesiones', $profesiones)
               ->with('tiposArchivo', $tiposArchivo)
               ->with('tiposPrestacion', $tiposPrestacion)
               ->with('categorias', $categorias)
               ->with('tiposAdjudicacion', $tiposAdjudicacion)
               ->with('referentes', $referentes)
               ->with('contrato', $contrato);
    }

    public function postEditar(Request $request)
    {
        // dd('editando el contrato', $request->all());

        //mensajes de los validadores
        $messages = [
            'required'    => 'Debe ingresar el :attribute',
            'numeric' => 'El :attribute debe solo contener números',
            'max' => 'El :attribute no debe exeder los :max caracteres',
            'min' => 'El :attribute debe tener minimo :min caracteres',
            'date_format' => 'La :attribute no tiene el formato correcto: día/mes/año',
            'reemplaza.required' => 'Debe seleccionar una factura'
        ];

        $rules = array(
            'tipo_contrato'    => 'required|numeric',
            'proveedor'        => 'required|numeric',
            'resolucion'       => 'required|numeric',
            'fecha_resolucion' => 'required|date_format:"d/m/Y"',
            'inicio_vigencia'  => 'required|date_format:"d/m/Y"',
            'fin_vigencia'     => 'required|date_format:"d/m/Y"',
            'monto_contrato'   => 'required',
            'tipo_adjudicacion' => 'required|numeric',
            'detalle_contrato' => 'required'
        );

        if ( $request->input('tipo_contrato') == 1 ) {

            if ( ! $request->input('por_procedimiento') ) {

                $rules['valor_hora'] = 'required';

            }
            
            $rules['unidad'] = 'required|numeric';
            $rules['profesion'] = 'required|numeric';
            // $rules['categoria'] = 'numeric';
            $rules['prestacion'] = 'required|numeric';
            $rules['sueldo_horas'] = 'required';

        } elseif ( $request->input('tipo_contrato') == 2 ) {

            // $rules['licitacion'] = 'required';
            $rules['referente_tecnico'] = 'required|numeric';
            // Validar los datos de las boletas, si se ingresa alguno
            if ( $request->input('numero_boleta_garantia') || $request->input('monto_boleta_garantia') || $request->input('inicio_vigencia_boleta_garantia') || $request->input('fin_vigencia_boleta_garantia') ) {

                $rules['numero_boleta_garantia']          = 'required|numeric';
                $rules['monto_boleta_garantia']           = 'required';
                $rules['inicio_vigencia_boleta_garantia'] = 'required|date_format:"d/m/Y"';
                $rules['fin_vigencia_boleta_garantia']    = 'required|date_format:"d/m/Y"';

            }

        }


        //validador de los input del formulario
        $validator = \Validator::make($request->all(), $rules, $messages);

        //Si contiene errores se devuelven todos los errores, de lo contrario guarda en la base de datos
        if ( $validator->fails() ) {

            return response()->json($validator->errors(), 400);

        } else {

            $datos = array();

            // Validar que no se repita la resolucion por año.
            $contrato = Contrato::where('resolucion', formato_entero($request->input('resolucion')) )
                                ->where('fecha_resolucion', '>=', fecha_Y($request->input('fecha_resolucion')).'-01-01' )
                                ->where('fecha_resolucion', '<=', fecha_Y($request->input('fecha_resolucion')).'-12-31' )
                                ->where('id', '!=', $request->input('_id') )
                                ->first();

            
            // Validar que las oc no quedan fuera de la vigencia del contrato
            $ocsDentro = OrdenCompra::where('id_contrato', $request->input('_id') )
                                    ->where('fecha_oc', '>=', fecha_Y_m_d( $request->input('inicio_vigencia') ))
                                    ->where('fecha_oc', '<=', fecha_Y_m_d( $request->input('fin_vigencia') ))
                                    ->get()->pluck('id')->toArray();

            $ocsFuera = OrdenCompra::where('id_contrato', $request->input('_id') )
                                   ->whereNotIn('id', $ocsDentro )
                                   ->get();
            
            // Validar que los documentos no quedan fuera de la vigencia del contrato
            $docsDentro = Documento::whereHas('getContratoOrdenCompra', function($query) use ($request) {
                                    $query->where('id_contrato', $request->input('_id'));
                                })
                                ->whereDate('periodo', '>=' , fecha_Y_m( $request->input('inicio_vigencia') ).'-01' )
                                ->whereDate('periodo', '<=' , fecha_Y_m_d( $request->input('fin_vigencia') ) )
                                ->get()->pluck('id')->toArray();

            $docsFuera = Documento::whereHas('getContratoOrdenCompra', function($query) use ($request) {
                                    $query->where('id_contrato', $request->input('_id'));
                                })
                                ->whereNotIn('id', $docsDentro )
                                ->get();

            if ( is_object($contrato) ) {

                $datos['mensaje'] = 'La resolución ya se encuentra en el sistema.';
                return response()->json($datos, 400);

            } elseif( $ocsFuera->count() > 0 ) {

                $datos['mensaje'] = 'Las ordenes de compra asociadas al contrato no pueden quedar fuera de la vigencia.';
                return response()->json($datos, 400);

            } elseif ( $docsFuera->count() > 0 ) {

                $datos['mensaje'] = 'Los documentos no pueden quedar fuera de la vigencia del Contrato.';
                return response()->json($datos, 400);

            } else {

                // validar la suma de monto_preventivo y monto_correctivo, debe ser igual a la suma del contrato (convenio)

                if ( $request->input('tipo_contrato') == 2 ) {

                    if ( $request->input('monto_preventivo') != 0  || $request->input('monto_correctivo') != 0 ) {

                        if ( formato_entero( $request->input('monto_contrato') ) != formato_entero( $request->input('monto_preventivo') ) + formato_entero( $request->input('monto_correctivo') ) ){

                            $datos['mensaje'] = 'El monto del contrato debe ser igual a la suma de los montos preventivo y correctivo';
                            return response()->json($datos, 400);

                        }
                        
                    }

                }

                $contrato = Contrato::findOrFail( $request->input('_id') );
                /**
                 * Obligatorios
                 */
                $contrato->id_tipo_contrato = $request->input('tipo_contrato');
                $contrato->id_proveedor = $request->input('proveedor');
                $contrato->id_tipo_adjudicacion = $request->input('tipo_adjudicacion');
                $contrato->id_referente_tecnico = $request->input('referente_tecnico');
                $contrato->resolucion = formato_entero( $request->input('resolucion') );
                $contrato->fecha_resolucion = fecha_Y_m_d( $request->input('fecha_resolucion') );
                $contrato->inicio_vigencia = fecha_Y_m_d( $request->input('inicio_vigencia') );
                $contrato->fin_vigencia = fecha_Y_m_d( $request->input('fin_vigencia') );

                /**
                 * Faltan reglas de negocio para la edicion:
                 * Al editar el monto del contrato, no se debe ver afectado el monto ocupado por los documentos asociados
                 */
                /************************************************************************************************************************************************************************************* */
                $contrato->monto_contrato = formato_entero( $request->input('monto_contrato') );
                $contrato->saldo_contrato = $contrato->monto_contrato - $contrato->getDocumentosContrato->sum('getDocumento.total_documento');
                
                $contrato->detalle_contrato = trim( $request->input('detalle_contrato') );

                /**
                 * Si es tipo contrato 1
                 */
                if ( $request->input('tipo_contrato') == 1 ) {

                    $contrato->id_unidad = ( $request->input('unidad') ) ? $request->input('unidad') : null;
                    
                    $contrato->id_profesion = ( $request->input('profesion') ) ? $request->input('profesion') : null;

                    $contrato->id_tipo_prestacion = ( $request->input('prestacion') ) ? $request->input('prestacion') : null;

                    $contrato->id_categoria = ( $request->input('categoria') ) ? $request->input('categoria') : null;

                    $contrato->addemdum = ( $request->input('addemdum') ) ? formato_entero( $request->input('addemdum') ) : null;

                    $contrato->fecha_addemdum = ( $request->input('fecha_addemdum') ) ? fecha_Y_m_d( $request->input('fecha_addemdum') ) : null;

                    $contrato->sueldo_horas = ( $request->input('sueldo_horas') ) ? $request->input('sueldo_horas') : null;
                    
                    $contrato->valor_hora = ( $request->input('valor_hora') ) ? formato_entero( $request->input('valor_hora') ) : null;

                    $contrato->por_procedimiento = ( $request->input('por_procedimiento') ) ? $request->input('por_procedimiento') : 0;

                } else {

                    $contrato->id_unidad = null;
                    
                    $contrato->id_profesion = null;

                    $contrato->id_tipo_prestacion = null;

                    $contrato->id_categoria = null;

                    $contrato->addemdum = null;

                    $contrato->fecha_addemdum = null;

                    $contrato->sueldo_horas = null;
                    
                    $contrato->valor_hora = null;

                    $contrato->por_procedimiento = 0;

                }
                
                /**
                 * Si es tipo contrato 2
                 * 
                 */
                if ( $request->input('tipo_contrato') == 2 ) {

                    $sumDocsPreventivoContrato = DocumentoContratoOrdenCompra::where('id_contrato', $contrato->id)
                                                                             ->where('ocupa_preventivo', 1)
                                                                             ->get()->sum('getDocumento.total_documento');
                    
                    $sumDocsCorrectivoContrato = DocumentoContratoOrdenCompra::where('id_contrato', $contrato->id)
                                                                             ->where('ocupa_correctivo', 1)
                                                                             ->get()->sum('getDocumento.total_documento');
                                                                          

                    /**
                     * Los montos preventivo y correctivo al ser editados no pueden ser menores
                     * a los montos ocupados por los documentos
                     */

                    $contrato->monto_preventivo = ( $request->input('monto_preventivo') ) ? formato_entero( $request->input('monto_preventivo') ) : null;

                    $contrato->monto_correctivo = ( $request->input('monto_correctivo') ) ? formato_entero( $request->input('monto_correctivo') ) : null;

                    $contrato->saldo_preventivo = $contrato->monto_preventivo - $sumDocsPreventivoContrato;
                    $contrato->saldo_correctivo = $contrato->monto_correctivo - $sumDocsCorrectivoContrato;

                    $contrato->licitacion = ( $request->input('licitacion') ) ? trim( $request->input('licitacion') ) : null;
                    
                    $contrato->cuotas = ( $request->input('cuotas') ) ? $request->input('cuotas') : 0;

                    $contrato->numero_boleta_garantia = ( $request->input('numero_boleta_garantia') ) ? formato_entero( $request->input('numero_boleta_garantia') ) : null;

                    $contrato->monto_boleta_garantia = ( $request->input('monto_boleta_garantia') ) ? formato_entero( $request->input('monto_boleta_garantia') ) : null;

                    $contrato->inicio_vigencia_boleta_garantia = ( $request->input('inicio_vigencia_boleta_garantia') ) ? fecha_Y_m_d( $request->input('inicio_vigencia_boleta_garantia') ) : null;

                    $contrato->fin_vigencia_boleta_garantia = ( $request->input('fin_vigencia_boleta_garantia') ) ? fecha_Y_m_d( $request->input('fin_vigencia_boleta_garantia') ) : null;

                } else {

                    $contrato->monto_preventivo = null;

                    $contrato->monto_correctivo = null;

                    $contrato->saldo_preventivo = $contrato->monto_preventivo;
                    $contrato->saldo_correctivo = $contrato->monto_correctivo;

                    $contrato->licitacion = null;
                    
                    $contrato->cuotas = 0;

                    $contrato->numero_boleta_garantia = null;

                    $contrato->monto_boleta_garantia = null;

                    $contrato->inicio_vigencia_boleta_garantia = null;

                    $contrato->fin_vigencia_boleta_garantia = null;

                }

                $contrato->id_user_updated = Auth::user()->id;
                $contrato->save();

                // Eliminar los archivos seleccionados para eliminar
                if ( $request->input('delete_list') ) {
                    foreach ($request->input('delete_list') as $idArchivo) {
                        $archivoDelete = Archivo::findOrfail($idArchivo);
                        $archivoDelete->delete();
                    }
                }

                $archivo = $request->file('archivo');
                if ( $archivo != null ) {

                    $nombreOriginalArchivo = str_replace('.pdf','',$archivo->getClientOriginalName() );
                    $arreglo_nombre = explode(".", $archivo->getClientOriginalName() );
                    $extension = strtolower( end( $arreglo_nombre ) );

                    if ($extension != 'pdf') {

                        $datos['mensaje_archivo'] = 'Extension incorrecta del archivo';
                        $datos['estado_archivo'] = 'error';

                    } else {

                        // Se trabajan las rutas del archivo, se guarda en el proyecto al final de la funcion
                        $nombre = $nombreOriginalArchivo.'_'.date('Y_m_d_G_i_s');
                        $rutaArchivo = '/contrato/'.$contrato->id.'/'.$nombre.'.pdf';
                        $pathArchivo = public_path().'/contrato/'.$contrato->id.'/';

                        $archivoContrato = new Archivo();
                        $archivoContrato->id_contrato = $contrato->id;
                        $archivoContrato->id_tipo_archivo = $request->input('tipo_archivo');

                        $archivoContrato->nombre = $nombre;
                        $archivoContrato->nombre_original = $nombreOriginalArchivo;
                        $archivoContrato->ubicacion = "contrato/".$contrato->id."/".$archivoContrato->nombre.".pdf";
                        $archivoContrato->extension = "pdf";
                        $archivoContrato->peso = $archivo->getSize();
                        $archivoContrato->save();

                        $datos['mensaje_archivo'] = 'Se ha guardado correctamente el archivo.';
                        $datos['estado_archivo'] = 'success';

                        $archivo->move( $pathArchivo , $nombre.'.pdf' ); // Se guarda el archivo

                    }
                    
                } else {
                    
                    $datos['mensaje_archivo'] = '';
                    $datos['estado_archivo'] = '';

                }

                $datos['mensaje'] = 'Se ha editado correctamente el Contrato.';
                $datos['estado'] = 'success';
                

            }

        }

        return response()->json($datos, 200);
    }

    public function getModalEliminar($id)
    {
        $contrato = Contrato::findOrFail($id);
        return view('contrato.modal_eliminar')
               ->with('contrato', $contrato);
    }

    public function postEliminar(Request $request)
    {
        // dd('eliminar contrato', $request->all());
        $contrato = Contrato::findOrFail( $request->input('_id') );

        if ( ! is_object($contrato) ) {

            $datos = array(
                'mensaje' => 'No se encuentra el Contrato en la Base de Datos.',
                'estado'  => 'error',
            );

        } else {

            $contrato->delete();

            $datos = array(
                'mensaje' => 'Se ha eliminado correctamente el Contrato.',
                'estado'  => 'success',
            );

        }

        return response()->json($datos,200);
    }

    public function getModalTerminar($id)
    {
        $contrato = Contrato::with([
            'getDocumentosContrato.getDocumento',
            'getUnidad',
            'getProfesion',
            'getCategoria',
            'getTipoPrestacion',
            'getTipoAdjudicacion',
            'getReferenteTecnico'
            ])
            ->findOrFail($id);

        $tiposArchivo = TipoArchivo::all();

        return view('contrato.modal_terminar')
               ->with('tiposArchivo', $tiposArchivo)
               ->with('contrato', $contrato);
    }

    public function postTerminar(Request $request)
    {
        // dd('terminar contrato', $request->all());

        //mensajes de los validadores
        $messages = [
            'required'    => 'Debe ingresar el :attribute',
            'numeric' => 'El :attribute debe solo contener números',
            'max' => 'El :attribute no debe exeder los :max caracteres',
            'min' => 'El :attribute debe tener minimo :min caracteres',
            'date_format' => 'La :attribute no tiene el formato correcto: día/mes/año',
            'reemplaza.required' => 'Debe seleccionar una factura'
        ];

        $rules = array(
            'fecha_termino'   => 'required|date_format:"d/m/Y"',
            'detalle_termino' => 'required',
            '_id'             => 'required'
        );

        //validador de los input del formulario
        $validator = \Validator::make($request->all(), $rules, $messages);

        //Si contiene errores se devuelven todos los errores, de lo contrario guarda en la base de datos
        if ( $validator->fails() ) {

            return response()->json($validator->errors(), 400);

        } else {

            $datos = array();

            $contrato = Contrato::findOrFail( $request->input('_id') );

            $contrato->fecha_termino = fecha_Y_m_d( $request->input('fecha_termino') );
            $contrato->detalle_termino = trim( $request->input('detalle_termino') );
            $contrato->termino = 1;

            $contrato->id_user_termino = Auth::user()->id;
            $contrato->save();

            // Eliminar los archivos seleccionados para eliminar
            if ( $request->input('delete_list') ) {
                foreach ($request->input('delete_list') as $idArchivo) {
                    $archivoDelete = Archivo::findOrfail($idArchivo);
                    $archivoDelete->delete();
                }
            }

            $archivo = $request->file('archivo');
            if ( $archivo != null ) {

                $nombreOriginalArchivo = str_replace('.pdf','',$archivo->getClientOriginalName() );
                $arreglo_nombre = explode(".", $archivo->getClientOriginalName() );
                $extension = strtolower( end( $arreglo_nombre ) );

                if ($extension != 'pdf') {

                    $datos['mensaje_archivo'] = 'Extension incorrecta del archivo';
                    $datos['estado_archivo'] = 'error';

                } else {

                    // Se trabajan las rutas del archivo, se guarda en el proyecto al final de la funcion
                    $nombre = $nombreOriginalArchivo.'_'.date('Y_m_d_G_i_s');
                    $rutaArchivo = '/contrato/'.$contrato->id.'/'.$nombre.'.pdf';
                    $pathArchivo = public_path().'/contrato/'.$contrato->id.'/';

                    $archivoContrato = new Archivo();
                    $archivoContrato->id_contrato = $contrato->id;
                    $archivoContrato->id_tipo_archivo = $request->input('tipo_archivo');

                    $archivoContrato->nombre = $nombre;
                    $archivoContrato->nombre_original = $nombreOriginalArchivo;
                    $archivoContrato->ubicacion = "contrato/".$contrato->id."/".$archivoContrato->nombre.".pdf";
                    $archivoContrato->extension = "pdf";
                    $archivoContrato->peso = $archivo->getSize();
                    $archivoContrato->save();

                    $datos['mensaje_archivo'] = 'Se ha guardado correctamente el archivo.';
                    $datos['estado_archivo'] = 'success';

                    $archivo->move( $pathArchivo , $nombre.'.pdf' ); // Se guarda el archivo

                }
                
            } else {
                
                $datos['mensaje_archivo'] = '';
                $datos['estado_archivo'] = '';

            }

            $datos['mensaje'] = 'Se ha terminado correctamente el Contrato.';
            $datos['estado'] = 'success';

        }

        return response()->json($datos, 200);

    }

    public function getModalVer($id)
    {
        $contrato = Contrato::with([
            'getDocumentosContrato.getDocumento.getContratoOrdenCompra.getOrdenCompra',
            'getUnidad',
            'getProfesion',
            'getCategoria',
            'getTipoPrestacion',
            'getTipoAdjudicacion',
            'getReferenteTecnico',
            'getOrdenesCompra',
            'getDocumentosContrato.getDocumento.getArchivos' => function ($q) {
                $q->whereIn('id_tipo_archivo', [1,2]); // Factura o boleta de honorarios
            }
            ])
            ->findOrFail($id);

        $archivosAcepta = ArchivoAcepta::where('id_proveedor', $contrato->id_proveedor)
                                       ->where('cargado', 0)
                                       ->where('id_user_rechazo', null)
                                       ->with(['getTipoDocumento'])
                                       ->get();

        return view('contrato.modal_ver')
             ->with('archivosAcepta', $archivosAcepta)
             ->with('contrato', $contrato);
    }

    public function getModalConsumir($id)
    {
        $contrato = Contrato::with([
            'getDocumentosContrato.getDocumento',
            'getUnidad',
            'getProfesion',
            'getCategoria',
            'getTipoPrestacion',
            'getTipoAdjudicacion',
            'getReferenteTecnico',
            'getOrdenesCompra' => function ($q) {
                $q->where('saldo_oc', '>', 0);
            }
            ])
            ->findOrFail($id);

        $documentos = Documento::where('id_proveedor', $contrato->id_proveedor)
                               ->doesnthave('getContratoOrdenCompra')
                               ->with(['getTipoDocumento'])
                               ->get();

        return view('contrato.modal_consumir')
             ->with('documentos', $documentos)
             ->with('contrato', $contrato);
    }

    public function postConsumir(Request $request)
    {
        // dd('consumir contrato', $request->all());

        //mensajes de los validadores
        $messages = [
            'required'    => 'Debe ingresar el :attribute',
            'numeric' => 'El :attribute debe solo contener números',
            'max' => 'El :attribute no debe exeder los :max caracteres',
            'min' => 'El :attribute debe tener minimo :min caracteres',
            'date_format' => 'La :attribute no tiene el formato correcto: mes/año',
            'reemplaza.required' => 'Debe seleccionar una factura'
        ];

        $rules = array(
            '_id'             => 'required',
            'documento'       => 'required',
            'periodo'         => 'required|date_format:"m/Y"',
        );

        //validador de los input del formulario
        $validator = \Validator::make($request->all(), $rules, $messages);

        //Si contiene errores se devuelven todos los errores, de lo contrario guarda en la base de datos
        if ( $validator->fails() ) {

            return response()->json($validator->errors(), 400);

        } else {

            $datos = array();

            $contrato = Contrato::findOrFail( $request->input('_id') );
            $documento = Documento::findOrFail( $request->input('documento') );
            $oc = null;

            if ( $request->input('orden_compra') != null ) {

                $oc = OrdenCompra::findOrFail( $request->input('orden_compra') );

            }

            if ( ! is_object($oc) ) {
                // Verificar si el contrato tiene OC disponibles con saldo.
                $ocContrato = OrdenCompra::where('id_contrato', $contrato->id)
                                         ->where('saldo_oc', '>', 0)
                                         ->first();

                if ( is_object($ocContrato) ) {

                    $datos['mensaje'] = 'Debe seleccionar una Orden de Compra del Contrato.';
                    return response()->json($datos, 400);

                }

            }

            // Validar con Contrato
            if ( $documento->id_tipo_documento == 4 || $documento->id_tipo_documento == 10 ) {
                // NC y CCC

                if ( $contrato->saldo_preventivo != null || $contrato->saldo_correctivo != null ) {
                    if ( $request->input('montoOcupar') == 'preventivo' && $documento->total_documento + $contrato->saldo_preventivo > $contrato->monto_preventivo ) {

                        $datos['mensaje'] = 'El total del documento más el saldo preventivo no puede superar el monto preventivo.';
                        return response()->json($datos, 400);
                        
                    }
                    if ( $request->input('montoOcupar') == 'correctivo' && $documento->total_documento + $contrato->saldo_correctivo > $contrato->monto_correctivo ) {

                        $datos['mensaje'] = 'El total del documento más el saldo correctivo no puede superar el monto correctivo.';
                        return response()->json($datos, 400);

                    }
                }

                if ( $documento->total_documento + $contrato->saldo_contrato > $contrato->monto_contrato ) {

                    $datos['mensaje'] = 'El total del documento más el saldo contrato no puede superar el monto del contrato.';
                    return response()->json($datos, 400);

                }

            } else {

                if ( $contrato->saldo_preventivo != null || $contrato->saldo_correctivo != null ) {
                    if ( $request->input('montoOcupar') == 'preventivo' && $documento->total_documento > $contrato->saldo_preventivo ) {

                        $datos['mensaje'] = 'El total del documento no puede superar el saldo preventivo.';
                        return response()->json($datos, 400);

                    }
                    if ( $request->input('montoOcupar') == 'correctivo' && $documento->total_documento > $contrato->saldo_correctivo ) {

                        $datos['mensaje'] = 'El total del documento no puede superar el saldo correctivo.';
                        return response()->json($datos, 400);

                    }
                }

                if ( $documento->total_documento > $contrato->saldo_contrato ) {

                    $datos['mensaje'] = 'El total del documento no puede superar el saldo contrato.';
                    return response()->json($datos, 400);

                }

            }
            
            // Validar OC
            if ( is_object($oc) ) {

                if ( $documento->id_tipo_documento == 4 || $documento->id_tipo_documento == 10 ) {
                    if ( $documento->total_documento + $oc->saldo_oc  > $oc->monto_oc ) {
                        
                        $datos['mensaje'] = 'El total del documento más el saldo de la orden de compra no puede superar el monto de la orden de compra.';
                        return response()->json($datos, 400);

                    }
                } else {
                    if ( $documento->total_documento > $oc->saldo_oc ) {
                        
                        $datos['mensaje'] = 'El total del documento no puede superar el saldo de la orden de compra.';
                        return response()->json($datos, 400);

                    }
                }

            }

            $documento->periodo = fecha_Y_m_d( '01/'.$request->input('periodo') );
            $documento->unirConContratoOrdenCompra($contrato, $oc, $request);
            $documento->save();

            $datos['mensaje'] = 'Se ha consumido correctamente el Contrato.';
            $datos['estado'] = 'success';

        }

        return response()->json($datos, 200);

    }

    public function getGenerarPDFSaldoConstrato($id)
    {
        saldoContrato::generarSaldo($id, true);
    }

    /**
     * Funciones Auxiliares
     */

    /**
     * Obtiene los contratos para ser seleccionados en el ingreso de documentos
     */
    public function getContratosParaDocumentos($id)
    {

        $contratos = Contrato::vigenteConSaldoParaProveedor($id)
                             ->get();

        if ( $contratos->count() > 0 ) {

            $options = '';

            foreach ( $contratos as $contrato ) {
                $options .= $contrato->getOpcionesParaSelect();
            }

            $datos = array(
                'options' => $options,
                'mensaje' => 'Todo bien.',
                'estado'  => 'success'
            );

        } else {

            $datos = array(
                'mensaje' => 'No se encuentran Contratos disponibles para el Proveedor.',
                'estado'  => 'error'
            );

        }

        return response()->json($datos, 200);

    }

    public function getUpload()
    {
        return view('contrato.upload');
    }

    public function postUpload(Request $request)
    {
        set_time_limit ( 0 );
        ini_set('max_execution_time', 0);

        $archivo = $request->file('archivo');
        $excel = \Excel::selectSheetsByIndex(0)->load($archivo)->get();

        foreach ($excel as $key => $fila) {
            // dd('holo', $fila);

            if ( trim(strtoupper( $fila->serviciounidad )) != '-') {
                $unidad = Unidad::where('nombre', trim(strtoupper( $fila->serviciounidad )))->first();
                if ( ! is_object($unidad) ) {
                    $unidad = New Unidad;
                    $unidad->nombre = trim(strtoupper( $fila->serviciounidad ));
                    $unidad->save();
                }
            }

            if ( trim(strtoupper( $fila->profesion )) != '-') {
                $profesion = Profesion::where('nombre', trim(strtoupper( $fila->profesion )))->first();
                if ( ! is_object($profesion) ) {
                    $profesion = New Profesion;
                    $profesion->nombre = trim(strtoupper( $fila->profesion ));
                    $profesion->save();
                }
            }

            if ( trim(strtoupper( $fila->tipo_de_prestacion )) != '-') {
                $tipoPrestacion = TipoPrestacion::where('nombre', trim(strtoupper( $fila->tipo_de_prestacion )))->first();
                if ( ! is_object($tipoPrestacion) ) {
                    $tipoPrestacion = New TipoPrestacion;
                    $tipoPrestacion->nombre = trim(strtoupper( $fila->tipo_de_prestacion ));
                    $tipoPrestacion->save();
                }
            }

            if ( trim(strtoupper( $fila->categoria )) != '-') {
                $categoria = Categoria::where('nombre', trim(strtoupper( $fila->categoria )))->first();
                if ( ! is_object($categoria) ) {
                    $categoria = New Categoria;
                    $categoria->nombre = trim(strtoupper( $fila->categoria ));
                    $categoria->save();
                }
            }
            
        }
        
        $datos = array(
            'mensaje' => 'Carga de auxiliar de contratos exitosa.',
            'estado' => 'success',
        );

        return response()->json($datos,200);
    }

}
