<?php namespace App\Http\Controllers;

use Illuminate\Http\Request;
use GuzzleHttp\Client;
use GuzzleHttp\Exception\RequestException;

use App\WsContrato;
use App\WsOrdenCompra;
use App\WsDocumento;
use App\WsArchivo;
use App\WsItemPresupuestario;


class ConsumeApiController extends Controller
{

    /**
     * Create a new controller instance.
     *
     * @return void
    */
    public function __construct()
    {
        $this->middleware('auth');
    }


    public function test()
    {
        $client = new Client();
        $response = $client->request('GET', 'http://18.234.47.233/api/recepciones/test');
        echo $response->getStatusCode();
        $statusCode = $response->getStatusCode();
        $body = $response->getBody()->getContents();
        echo '<br>';
        return $body;
    }

    
    /**
     * funcion que recorre todos los contratos y envia los datos, mediante post.
     */
    public function enviarRecepciones()
    {

        set_time_limit ( 0 );
        ini_set('max_execution_time', 0);

        $wsContratos = WsContrato::with(['getWsOrdenCompra.getWsDocumento.getWsArchivos',
                                        'getWsOrdenCompra.getWsDocumento.getWsItemsPresupuestarios'])
                                ->whereHas('getWsOrdenCompra.getWsDocumento.getWsArchivos')
                                ->whereHas('getWsOrdenCompra.getWsDocumento.getWsItemsPresupuestarios')
                                ->latest()
                                // ->where('created_at', '>', '2019-10-02 00:00:00')
                                ->get();

        foreach ($wsContratos as $key => $wsContrato) {
            
            $enviar = true;

            $contrato = [
                'rut_proveedor' => $wsContrato->rut_proveedor,
                'nombre_proveedor' => $wsContrato->nombre_proveedor,
                'id_licitacion' => $wsContrato->id_licitacion,
                'fecha_ingreso_contrato' => $wsContrato->fecha_ingreso_contrato,
                'resolucion' => $wsContrato->resolucion,
                'fecha_resolucion' => $wsContrato->fecha_resolucion,
                'fecha_inicio_contrato' => $wsContrato->fecha_inicio_contrato,
                'fecha_termino_contrato' => $wsContrato->fecha_termino_contrato,
                'monto_maximo' => $wsContrato->monto_maximo,
                'monto_licitacion' => $wsContrato->monto_licitacion,
                'saldo_licitacion' => $wsContrato->saldo_licitacion,
                'unidad_licitacion' => $wsContrato->unidad_licitacion,
                'referente_tecnico' => $wsContrato->referente_tecnico
            ];            

            $orden_compra = [
                'fecha_orden_compra' => $wsContrato->getWsOrdenCompra->fecha_orden_compra,
                'fecha_recepcion_orden_compra' => $wsContrato->getWsOrdenCompra->fecha_recepcion_orden_compra,
                'id_orden_compra' => $wsContrato->getWsOrdenCompra->id_orden_compra,
                'numero_orden_compra' => $wsContrato->getWsOrdenCompra->numero_orden_compra,
                'total_orden_compra' => $wsContrato->getWsOrdenCompra->total_orden_compra,
                'tipo_compra' => $wsContrato->getWsOrdenCompra->tipo_compra,
                'programa' => $wsContrato->getWsOrdenCompra->programa,
                'saldo_orden_compra' => $wsContrato->getWsOrdenCompra->saldo_orden_compra,
                'numero_orden_compra_mercado_publico' => $wsContrato->getWsOrdenCompra->numero_orden_compra_mercado_publico,
                'fecha_orden_compra_mercado_publico' => $wsContrato->getWsOrdenCompra->fecha_orden_compra_mercado_publico,
                'supera_mil_utm' => $wsContrato->getWsOrdenCompra->supera_mil_utm,
                'firma_abastecimiento' => $wsContrato->getWsOrdenCompra->firma_abastecimiento,
                'firma_sda' => $wsContrato->getWsOrdenCompra->firma_sda,
                'firma_direccion' => $wsContrato->getWsOrdenCompra->firma_direccion,
            ];

            $documento = [
                'documento' => $wsContrato->getWsOrdenCompra->getWsDocumento->documento,
                'tipo_documento' => $wsContrato->getWsOrdenCompra->getWsDocumento->tipo_documento,
                'nombre_usuario_responsable' => $wsContrato->getWsOrdenCompra->getWsDocumento->nombre_usuario_responsable,
                'documento_descuento_total' => $wsContrato->getWsOrdenCompra->getWsDocumento->documento_descuento_total,
                'documento_neto' => $wsContrato->getWsOrdenCompra->getWsDocumento->documento_neto,
                'documento_iva' => $wsContrato->getWsOrdenCompra->getWsDocumento->documento_iva,
                'documento_total' => $wsContrato->getWsOrdenCompra->getWsDocumento->documento_total,
            ];

            $archivos = [];
            foreach ($wsContrato->getWsOrdenCompra->getWsDocumento->getWsArchivos as $index => $archivo) {
                if ( file_exists(public_path().'/'.$archivo->ubicacion) ) {

                    $content = file_get_contents(public_path().'/'.$archivo->ubicacion);
                    $datosArchivo = [
                        'nombre_archivo' => strstr($archivo->nombre_archivo, '_', true),
                        'archivo_base64' => base64_encode($content),
                        'id_tipo_archivo' => $archivo->id_tipo_archivo,
                        'url_externa' => '',
                    ];
                    $archivos[] = $datosArchivo;

                } else {
                    // no enviar 
                    $enviar = false;
                }
            }

            $items_presupuestarios = [];
            foreach ($wsContrato->getWsOrdenCompra->getWsDocumento->getWsItemsPresupuestarios as $index => $item) {
                $datosItem = [
                    'id_producto' => $item->id_producto,
                    'cantidad_recepcionada' => $item->cantidad_recepcionada,
                    'valor_item_recepcionado' => $item->valor_item_recepcionado,
                    'item_presupuestario' => $item->item_presupuestario,
                    'descripcion_articulo' => $item->descripcion_articulo,
                    'codigo_articulo' => $item->codigo_articulo,
                ];

                $items_presupuestarios[] = $datosItem; 
            }

            $token = 'test_v1';
            $datos['token'] = $token;
            if ( $enviar == true && count($archivos) > 0 ) {

                try {
                    $client = new Client();
                    $response = $client->request('POST', 'http://10.4.237.28/finanzas/api/facturas/agregar',[
                    // $response = $client->request('POST', 'http://finanzas.test/api/facturas/agregar',[
                        'form_params' => [
                            'contrato' => json_encode($contrato),
                            'orden_compra' => json_encode($orden_compra),
                            'documento' => json_encode($documento),
                            'archivos' => json_encode($archivos),
                            'items_presupuestarios' => json_encode($items_presupuestarios),
                            'token' => json_encode($token),
                        ]
                    ]);

                    echo '<br>';
                    echo 'codigo respuesta para el contrato id '.$wsContrato->id.' '.$response->getStatusCode();
                    echo '<br>';
                    
                    echo 'respuesta para el contrato id '.$wsContrato->id.' '.$response->getBody()->getContents();
                    echo '<br>';
                    echo '<strong>numero documento '.$wsContrato->getWsOrdenCompra->getWsDocumento->documento.'</strong><br>';
                    
                } catch (RequestException $e) {

                    echo '<br>';
                    echo 'ERROR: codigo respuesta para el contrato id '.$wsContrato->id.' '.$e->getResponse()->getStatusCode();
                    echo '<br>';
                    
                    echo 'respuesta para el contrato id '.$wsContrato->id.' '.$e->getResponse()->getBody()->getContents();
                    echo '<br>';
                    echo '<strong>numero documento '.$wsContrato->getWsOrdenCompra->getWsDocumento->documento.'</strong><br>';
                    
                }
                
                sleep(3);
            }
        }

        echo '<br>';
        echo '<h3> <strong>Ha terminado el script</strong></h3>';
    }

    /**
     * Deja en limpio la bd, para enviar todas las recepciones
     */
    public function deleteRecepciones()
    {
        $archivos = WsArchivo::all();
        foreach ($archivos as $key => $archivo) {
            if ( file_exists(public_path().'/'.$archivo->ubicacion) ) {
                unlink($archivo->ubicacion); // elimina archivo
            }
    
            $archivo->delete();
        }

        WsArchivo::query()->truncate();
        WsContrato::query()->truncate();
        WsOrdenCompra::query()->truncate();
        WsDocumento::query()->truncate();
        WsItemPresupuestario::query()->truncate();
    }
}
