<?php namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests;
use App\Documento;
use App\FolioSigfe;
use App\TipoDocumento;
use App\ModalidadCompra;
use App\TipoAdjudicacion;
use App\TipoInforme;
use App\User;
use App\ReferenteTecnico;
use App\Proveedor;
use App\ItemPresupuestario;

class DeudaFlotanteController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
    */
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index()
    {
        // if ( ! \Entrust::can(['listado-devengado','editar-devengo']) ) {
        //     return \Redirect::to('home');
        // }
        
        $tiposDocumento = TipoDocumento::whereNotIn('id',[2,8])->get();
        $modalidadesCompra = ModalidadCompra::all();
        $tiposAdjudicacion = TipoAdjudicacion::all();
        $tiposInforme = TipoInforme::all();
        $usuariosResponsables = User::where('responsable',1)->get();
        $referentesTecnicos = ReferenteTecnico::all();
        $usuariosDigitadores = User::has('getDocumentos')->get();
        $proveedores = Proveedor::all();
        $itemsPresupuestarios = ItemPresupuestario::all();

        // Select para Fechas a utilizar en el filtro
        $fechaUtilizar1 = new \stdClass;
        $fechaUtilizar1->opcion = 'Ingreso';
        $fechaUtilizar2 = new \stdClass;
        $fechaUtilizar2->opcion = 'Recepción';
        // $fechaUtilizar3 = new \stdClass;
        // $fechaUtilizar3->opcion = 'SIGFE';

        // $fechasUtilizar[] = $fechaUtilizar3;
        $fechasUtilizar[] = $fechaUtilizar2;
        $fechasUtilizar[] = $fechaUtilizar1;

        // Permisos para no repetir la consulta al buscar datos en datatable
        $verDocumento = \Auth::user()->can('ver-documento');
        $problemasDocumento = \Auth::user()->can('problemas-documento');

        return view('deuda_flotante.index')->with('tiposDocumento', $tiposDocumento)
                                           ->with('modalidadesCompra', $modalidadesCompra)
                                           ->with('tiposAdjudicacion', $tiposAdjudicacion)
                                           ->with('tiposInforme', $tiposInforme)
                                           ->with('usuariosResponsables', $usuariosResponsables)
                                           ->with('referentesTecnicos', $referentesTecnicos)
                                           ->with('usuariosDigitadores', $usuariosDigitadores)
                                           ->with('proveedores', $proveedores)
                                           ->with('itemsPresupuestarios', $itemsPresupuestarios)
                                           ->with('fechasUtilizar', $fechasUtilizar)
                                           ->with('verDocumento', $verDocumento)
                                           ->with('problemasDocumento', $problemasDocumento);
                                                
    }

    public function dataTableDeudaFlotanteIndex(Request $request )
    {
        set_time_limit ( 0 );
        ini_set('max_execution_time', 0);

        $search = $request->get('search')['value'];
        $order_column = $request->get('order')[0]['column'];
        $order_dir = $request->get('order')[0]['dir'];

        $formJSON = json_decode($request->get('form'));
        $form = construyeFormulario($formJSON);
        
        $fechaInicio = '';
        if ( $form->filtro_fecha_inicio != '' ) {
            $fechaInicio = fecha_Y_m_d($form->filtro_fecha_inicio);
        }
        
        $fechaTermino = '';
        if ( $form->filtro_fecha_termino != '' ) {
            $fechaTermino = fecha_Y_m_d($form->filtro_fecha_termino);
        }

        $deudaTotal = isset($form->filtro_total_deuda) ? true : false;
        

        $documentos = Documento::deudas();
                            
        if ( $form->filtro_numero_documento == null ) {
            // Se filtra por las fechas
            if ( $deudaTotal == false ) {

                if ( $form->filtro_fecha_a_utilizar == 'Ingreso' ) {
                    if ( $fechaInicio != '' ) {
                        $documentos = $documentos->where('fecha_ingreso','>=', $fechaInicio.' 00:00:00');
                    }

                    if ( $fechaTermino != '' ) {
                        $documentos = $documentos->where('fecha_ingreso','<=', $fechaTermino.' 23:59:59');
                    }

                } elseif ( $form->filtro_fecha_a_utilizar == 'Recepción' ) {

                    if ( $fechaInicio != '' ) {
                        $documentos = $documentos->where('fecha_recepcion','>=', $fechaInicio);
                    }

                    if ( $fechaTermino != '' ) {
                        $documentos = $documentos->where('fecha_recepcion','<=', $fechaTermino);
                    }
                    
                // no se ocupa el filtro de fecha sigfe
                } elseif ( $form->filtro_fecha_a_utilizar == 'SIGFE' ) {

                    if ( $fechaInicio != '' ) {
                        $foliosSigfe = $foliosSigfe->where('folios_sigfe.fecha_sigfe','>=', $fechaInicio);
                    }

                    if ( $fechaTermino != '' ) {
                        $foliosSigfe = $foliosSigfe->where('folios_sigfe.fecha_sigfe','<=', $fechaTermino);
                    }                           

                }

            }

        } else {
            
            $documentos = $documentos->where('numero_documento', trim($form->filtro_numero_documento) );

        }

        if ( isset($form->filtro_proveedor) ) {

            $documentos = $documentos->WhereIn('id_proveedor', $form->filtro_proveedor);

        }

        if ( isset($form->filtro_tipo_documento) ) {

            $documentos = $documentos->WhereIn('id_tipo_documento', $form->filtro_tipo_documento);
            
        }

        if ( isset($form->filtro_modalidad_compra) ) {

            $documentos = $documentos->WhereIn('id_modalidad_compra', $form->filtro_modalidad_compra);
            
        }

        if ( isset($form->filtro_tipo_adjudicacion) ) {

            $documentos = $documentos->WhereIn('id_tipo_adjudicacion', $form->filtro_tipo_adjudicacion);
            
        }

        if ( isset($form->filtro_tipo_informe) ) {

            $documentos = $documentos->WhereIn('id_tipo_informe', $form->filtro_tipo_informe);
           
        }

        if ( isset($form->filtro_responsable) ) {

            $documentos = $documentos->WhereIn('id_responsable', $form->filtro_responsable);
            
        }

        if ( isset($form->filtro_referente_tecnico) ) {

            $documentos = $documentos->WhereIn('id_referente_tecnico', $form->filtro_referente_tecnico);
            
        }

        if ( isset($form->filtro_digitador) ) {

            $documentos = $documentos->WhereIn('id_digitador', $form->filtro_digitador);
           
        }

        if ( isset($form->filtro_item_presupuestario) ) {

            $documentos = $documentos->whereHas('getDevengos', function($query) use( $form ){
                                            $query->WhereIn('id_item_presupuestario', $form->filtro_item_presupuestario);
                                        });
            
        }

        $documentos = $documentos->get();

        // dd( $verDocumento, $problemasDocumento, $allValidations );
        $verDocumento = $form->permiso_ver_documento;
        $problemasDocumento = $form->permiso_problemas_documento;

        $data = [];
        $totalDeuda = 0;
        foreach ($documentos as $key => $documento) {
            
            if ( $form->filtro_documentos != 'Todos' ) {

                if ( $form->filtro_documentos == 'Cuadrados' && $documento->estadoDocumentoDeudaFlotante() != 'Cuadrado' ) {
                    continue;
                }

                if ( $form->filtro_documentos == 'Descuadrados' && $documento->estadoDocumentoDeudaFlotante() != 'Descuadrado' ) {
                    continue;
                }

            }
            
            if ( $documento->getDeuda() > 0 ) {
                if ( isset($form->filtro_item_presupuestario) ) {
                    $data[] = $documento->getDatosDeudaFlotante($form->filtro_item_presupuestario, $verDocumento, $problemasDocumento);
                } else {
                    $data[] = $documento->getDatosDeudaFlotante(null, $verDocumento, $problemasDocumento);
                }
            }

            $totalDeuda += $documento->getDeuda();

        }

        $respuesta = array(
            "data"       => $data,
            "totalDeuda" => '$ '.formatoMiles($totalDeuda)
        );

        return json_encode($respuesta);

    }

    public function postDeudaExcel(Request $request)
    {
        set_time_limit(0);
        ini_set('max_execution_time', 0);

        \Excel::create('SIFCON | Deuda Flotante '.date('d-m-Y').'', function($excel) use($request) {
            $fechaInicio = fecha_Y_m_d($request->input('filtro_fecha_inicio'));
            $fechaTermino = fecha_Y_m_d($request->input('filtro_fecha_termino'));
            $deudaTotal = $request->input('filtro_total_deuda');

            $documentos = Documento::deudas();

            if ( $request->input('filtro_numero_documento') == null ) {

                if ( $deudaTotal == null ) {
                    if ( $request->input('filtro_fecha_a_utilizar') == 'Ingreso' ) {
                        
                            if ( $fechaInicio != '' ) {
                                $documentos = $documentos->where('fecha_ingreso','>=', $fechaInicio.' 00:00:00');
                            }
                            
                            if ( $fechaTermino != '' ) {
                                $documentos = $documentos->where('fecha_ingreso','<=', $fechaTermino.' 23:59:59');
                            }                          

                    } elseif ( $request->input('filtro_fecha_a_utilizar') == 'Recepción' ) {

                            if ( $fechaInicio != '' ) {
                                $documentos = $documentos->where('fecha_recepcion','>=', $fechaInicio);
                            }

                            if ( $fechaTermino != '' ) {
                                $documentos = $documentos->where('fecha_recepcion','<=', $fechaTermino);
                            }
                                                            
                    // no se ocupa
                    } elseif ( $request->input('filtro_fecha_a_utilizar') == 'SIGFE' ) {

                        if ( $fechaInicio != '' ) {
                            $foliosSigfe = $foliosSigfe->where('fecha_sigfe','>=', $fechaInicio);
                        }
        
                        if ( $fechaTermino != '' ) {
                            $foliosSigfe = $foliosSigfe->where('fecha_sigfe','<=', $fechaTermino);
                        }

                    }
                }
                

            } else {

                $documentos = $documentos->where('numero_documento', trim( $request->input('filtro_numero_documento') )  );
            }
                        
            if ( $request->input('filtro_proveedor') != null ) {
                
                $documentos = $documentos->WhereIn('id_proveedor', $request->input('filtro_proveedor') );

            }

            if ( $request->input('filtro_tipo_documento') != null ) {

                $documentos = $documentos->WhereIn('id_tipo_documento', $request->input('filtro_tipo_documento') );

            }

            if ( $request->input('filtro_modalidad_compra') != null ) {

                $documentos = $documentos->WhereIn('id_modalidad_compra', $request->input('filtro_modalidad_compra'));

            }

            if ( $request->input('filtro_tipo_adjudicacion') != null ) {

                $documentos = $documentos->WhereIn('id_tipo_adjudicacion', $request->input('filtro_tipo_adjudicacion') );

            }

            if ( $request->input('filtro_tipo_informe') != null ) {

                $documentos = $documentos->WhereIn('id_tipo_informe', $request->input('filtro_tipo_informe') );

            }

            if ( $request->input('filtro_responsable') != null ) {

                $$documentos = $documentos->WhereIn('id_responsable', $request->input('filtro_responsable') );

            }

            if ( $request->input('filtro_referente_tecnico') != null ) {

                $documentos = $documentos->WhereIn('id_referente_tecnico', $request->input('filtro_referente_tecnico') );

            }

            if ( $request->input('filtro_digitador') != null ) {;

                $documentos = $documentos->WhereIn('id_digitador', $request->input('filtro_digitador') );

            }

            if ( $request->input('filtro_item_presupuestario') != null ) {

                $documentos = $documentos->whereHas('getDevengos', function($query) use( $request ){
                                                $query->WhereIn('id_item_presupuestario', $request->input('filtro_item_presupuestario') );
                                            });

            }

            $documentos = $documentos->get();

            $excel->sheet('Deuda Flotante', function($sheet) use ($documentos, $request) {

                $sheet->row(1, [
                    'Antigüedad',
                    'Estado',
                    'Nómina',
                    'Proveedor',
                    'RUT',
                    'Tipo Dcto.',
                    'N° Dcto.',
                    'N° Dcto Compra',
                    'F/Doc.',
                    'M/Doc.',
                    'Fecha RD',
                    'Mes RD',
                    'Íitem',
                    'Total Orig.',
                    'Total Act.',
                    'Deuda'
                ]);
                
                $cuentaLineas = 1;
                foreach ($documentos as $index => $documento) {

                    if ( $request->input('filtro_documentos') != 'Todos' ) {

                        if ( $request->input('filtro_documentos') == 'Cuadrados' && $documento->estadoDocumentoDeudaFlotante() != 'Cuadrado' ) {
                            continue;
                        }
        
                        if ( $request->input('filtro_documentos') == 'Descuadrados' && $documento->estadoDocumentoDeudaFlotante() != 'Descuadrado' ) {
                            continue;
                        }
        
                    }

                    if ( $documento->getDeuda() > 0 ) {

                        $estado = $documento->estadoDocumentoDeudaFlotante();
                
                        if ( $documento->id_user_problema != null ) {
                            $estado .= ' / Con problemas';
                        }

                        $nomina = $documento->getNominaPagoDocumento->count() > 0 ? 'Si':'No';
                        $items = '';
                        foreach ( $documento->getDevengos as $devengo ) {
                            $items .= $devengo->getItemPresupuestario->codigo().' | ';
                        }

                        $cuentaLineas++;
                        $sheet->row($cuentaLineas, [
                            $documento->dias_antiguedad,    // 'Antigüedad',
                            $estado,               // 'Estado',
                            $nomina,               // 'Nómina',
                            $documento->getProveedor ? $documento->getProveedor->nombre : '',   // 'Proveedor',
                            $documento->getProveedor ? $documento->getProveedor->rut : '',  // 'RUT',
                            $documento->getTipoDocumento ? $documento->getTipoDocumento->nombre : '',  // 'Tipo Dcto.',
                            $documento->numero_documento,  // 'N° Dcto.',
                            $documento->documento_compra,  // 'N° Dcto Compra',
                            fecha_dmY($documento->fecha_documento),    // 'F/Doc.',
                            explode("-",$documento->fecha_documento)[1],      // 'M/Doc.',
                            fecha_dmY($documento->fecha_recepcion),   // 'Fecha RD',
                            explode("-",$documento->fecha_recepcion)[1],  // 'Mes RD',
                            $items,    // 'Íitem',
                            $documento->total_documento,     // 'Total Orig.',
                            $documento->total_documento_actualizado,    // 'Total Act.',
                            $documento->getDeuda()       // 'Deuda'
                        ]);

                    }

                }

            });

        })->export('xlsx');
    }

}
