<?php namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests;
use Auth;
use App\ReferenteTecnico;
use App\Documento;
use App\VistoBueno;
use App\TipoArchivo;
use App\Archivo;
use Mpdf\Mpdf;
use App\User;
use App\RechazoVistoBueno;
use App\TipoDocumento;
use App\ModalidadCompra;
use App\TipoAdjudicacion;
use App\TipoInforme;
use App\Proveedor;
use App\ItemPresupuestario;

class ReferenteTecnicoController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
    */
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index()
    {
        if (!\Entrust::can(['crear-referente-tecnico','editar-referente-tecnico','eliminar-referente-tecnico','ver-referente-tecnico','ver-general-referente-tecnico'])) {
            return \Redirect::to('home');
        }
        $referentesTecnicos = ReferenteTecnico::all();
        return view('referente_tecnico.index')->with('referentesTecnicos',$referentesTecnicos);
    }

    public function getModalVer($id)
    {
        $referenteTecnico = ReferenteTecnico::with('getUser')->findOrFail($id);
        return view('referente_tecnico.modal_ver_referente_tecnico')->with('referenteTecnico',$referenteTecnico);
    }

    public function getModalEditar($id)
    {
        $referenteTecnico = ReferenteTecnico::with('getUser')->findOrFail($id);
        $usuarios = User::doesntHave('getReferenteTecnico')->get();
        return view('referente_tecnico.modal_editar_referente_tecnico')->with('referenteTecnico',$referenteTecnico)->with('usuarios',$usuarios);
    }

    public function postEditar(Request $request)
    {
        // dd('Holo',$request->all());
        // Mensajes del validador
        $messages = [
            'required' => 'Debe ingresar el :attribute',
            'numeric' => 'El :attribute debe solo contener números',
            'usuario.required' => 'Debe seleccionar un usuario.',
            'max' => 'El :attribute no debe exeder los :max caracteres',
            'min' => 'El :attribute debe tener minimo :min caracteres',
            'unique' => 'El valor :attribute ya existe en la base de datos',
        ];

        // Reglas del validador
        $rules = [
            'cargo' => 'required',
            'responsable' => 'required',
            'usuario' => 'required',
            // 'archivo' => 'image|mimes:jpeg,png,jpg'
        ];
        
        // Validador de los input del formulario
        $validator = \Validator::make($request->all(), $rules, $messages);

        //Si contiene errores se devuelven todos los errores, de lo contrario guarda en la base de datos
        if ( $validator->fails() ) {
            return response()->json($validator->errors(),400);
        } else {
            $editReferente = ReferenteTecnico::findOrFail($request->input('_id'));
            $editReferente->editReferenteTecnico($request);

            $datos = array(
                'mensaje' => 'Edición exitosa del Tipo de Informe',
                'estado' => 'success',
                'nombre' => $editReferente->nombre,
                'rut' => $editReferente->formatRut(),
                'responsable' => $editReferente->responsable,
                'id' => $editReferente->id,
            );

            $archivo = $request->file('archivo');
            if ( $archivo != null ) {
                $arreglo_nombre = explode(".", $archivo->getClientOriginalName() );
                $nombre = $arreglo_nombre[0].'_'.date('Y_m_d_G_i_s').'.'.$arreglo_nombre[1];
                $rutaArchivo = '/firmas/'.$nombre;
                $pathArchivo = public_path().'/firmas/';

                $archivo->move( $pathArchivo , $nombre ); // Se guarda el archivo
                $editReferente->url_firma = $rutaArchivo;
                $editReferente->save();
            }
        }

        return response()->json($datos,200);
    }

    public function create()
    {
        $usuarios = User::doesntHave('getReferenteTecnico')->get();
        return view('referente_tecnico.modal_crear_referente_tecnico')->with('usuarios',$usuarios);
    }

    public function store(Request $request)
    {
        // Mensajes del validador
        $messages = [
            'required' => 'Debe ingresar el :attribute',
            'numeric' => 'El :attribute debe solo contener números',
            'usuario.required' => 'Debe seleccionar un usuario.',
            'max' => 'El :attribute no debe exeder los :max caracteres',
            'min' => 'El :attribute debe tener minimo :min caracteres',
            'unique' => 'El valor :attribute ya existe en la base de datos',
        ];

        // Reglas del validador
        $rules = [
            'cargo' => 'required',
            'responsable' => 'required',
            'usuario' => 'required',
            // 'archivo' => 'image|mimes:jpeg,png,jpg'
            // 'archivo' => 'mimes:jpeg,png,jpg'
        ];

        // Validador de los input del formulario
        $validator = \Validator::make($request->all(), $rules, $messages);

        //Si contiene errores se devuelven todos los errores, de lo contrario guarda en la base de datos
        if ( $validator->fails() ) {
            return response()->json($validator->errors(),400);
        } else {

            try {
                $newReferenteTecnico = new ReferenteTecnico($request);
            } catch (QueryException $e) {
                $datos = array(
                    'estado' => 'error',
                    'mensaje' => 'Existen problemas al momento de guardar el Referente Técnico',
                );
                return response()->json($datos,200);
            }

            $datos = array(
                'mensaje' => 'Se ha creado el Referente Técnico',
                'estado' => 'success',
                'nombre' => $newReferenteTecnico->nombre,
                'rut' => $newReferenteTecnico->formatRut(),
                'responsable' => $newReferenteTecnico->responsable,
                'id' => $newReferenteTecnico->id,
            );

            $archivo = $request->file('archivo');
            if ( $archivo != null ) {
                $arreglo_nombre = explode(".", $archivo->getClientOriginalName() );
                $nombre = $arreglo_nombre[0].'_'.date('Y_m_d_G_i_s').'.'.$arreglo_nombre[1];
                $rutaArchivo = '/firmas/'.$nombre;
                $pathArchivo = public_path().'/firmas/';

                $archivo->move( $pathArchivo , $nombre ); // Se guarda el archivo
                $newReferenteTecnico->url_firma = $rutaArchivo;
                $newReferenteTecnico->save();
            }
        }

        return response()->json($datos,200);
    }

    public function getModalEliminar($id)
    {
        $referenteTecnico = ReferenteTecnico::findOrFail($id);
        return view('referente_tecnico.modal_eliminar_referente_tecnico')->with('referenteTecnico',$referenteTecnico);
    }

    public function postEliminar(Request $request)
    {
        $deleteReferenteTecnico = ReferenteTecnico::find($request->input('_id'));
        if ( !is_object($deleteReferenteTecnico) ) {
            $datos = array(
                'mensaje' => 'No se encuentra el Referente Técnico en la Base de Datos',
                'estado' => 'error',
            );
        } else {
            $deleteReferenteTecnico->id_user_deleted = Auth::user()->id;
            $deleteReferenteTecnico->timestamps = false;
            $deleteReferenteTecnico->save();
            $deleteReferenteTecnico->delete();

            $datos = array(
                'mensaje' => 'Se ha eliminado correctamente el Referente Técnico.',
                'estado' => 'success',
                'id' => $deleteReferenteTecnico->id,
            );
        }

        return response()->json($datos,200);
    }

    /*****************************************************************************************
     * ********************************************************************************************************************
     ********************************************************************************************************************
     * *************************************************************************** */
    /**
     * FUNCIONES DEL REFERENTE TECNICO
     */
    
    public function dataTableSolicitudVistoBueno(Request $request)
    {
        // dd($request->all());

        set_time_limit ( 0 );
        ini_set('max_execution_time', 0);

        $search = $request->get('search')['value'];
        $order_column = $request->get('order')[0]['column'];
        $order_dir = $request->get('order')[0]['dir'];
        
        if ( $order_column == null ) {
            $order_column = 1;
        }
        
        if ( $order_dir == null ) {
            $order_dir = 'asc';
        }


        $formJSON = json_decode($request->get('form'));
        $form = construyeFormulario($formJSON);

        $fechaInicio = fecha_Y_m_d($form->filtro_fecha_inicio);
        $fechaTermino = fecha_Y_m_d($form->filtro_fecha_termino);

        $documentos = Documento::where('id_visto_bueno',null)
                               ->where('id_referente_tecnico','<>',0)
                               ->whereNotIn('id_tipo_informe',[7,8])
                               ->whereNotIn('id_tipo_documento',[10,11]);
                            //    ->whereYear('fecha_recepcion',date('Y'))

        if ( $form->filtro_numero_documento == null ) {
            // Se filtra por las fechas

            if ( $form->filtro_fecha_inicio != null ) {

                if ( $form->filtro_fecha_a_utilizar == 'Ingreso' ) {
                    $documentos = $documentos->where('fecha_ingreso','>=',$fechaInicio.' 00:00:00');
                } elseif ( $form->filtro_fecha_a_utilizar == 'Recepción' ) {
                    $documentos = $documentos->where('fecha_recepcion','>=',$fechaInicio);
                }

            }

            if ( $form->filtro_fecha_termino != null ) {
                if ( $form->filtro_fecha_a_utilizar == 'Ingreso' ) {
                    $documentos = $documentos->where('fecha_ingreso','<=',$fechaTermino.' 23:59:59');
                } elseif ( $form->filtro_fecha_a_utilizar == 'Recepción' ) {
                    $documentos = $documentos->where('fecha_recepcion','<=',$fechaTermino);
                }
            }

        } else {
            // no se consideran las fechas, y se busca el numero del documento
            $documentos = $documentos->where('numero_documento', trim($form->filtro_numero_documento) );
        }
        
        if ( isset($form->filtro_proveedor) ) {
            $documentos = $documentos->WhereIn('id_proveedor',$form->filtro_proveedor);
        }

        if ( isset($form->filtro_tipo_documento) ) {
            $documentos = $documentos->WhereIn('id_tipo_documento',$form->filtro_tipo_documento);
        }

        if ( isset($form->filtro_modalidad_compra) ) {
            $documentos = $documentos->WhereIn('id_modalidad_compra',$form->filtro_modalidad_compra);
        }

        if ( isset($form->filtro_tipo_adjudicacion) ) {
            $documentos = $documentos->WhereIn('id_tipo_adjudicacion',$form->filtro_tipo_adjudicacion);
        }

        if ( isset($form->filtro_tipo_informe) ) {
            $documentos = $documentos->WhereIn('id_tipo_informe',$form->filtro_tipo_informe);
        }

        if ( isset($form->filtro_responsable) ) {
            $documentos = $documentos->WhereIn('id_responsable',$form->filtro_responsable);
        }

        if ( isset($form->filtro_referente_tecnico) ) {
            $documentos = $documentos->WhereIn('id_referente_tecnico',$form->filtro_referente_tecnico);
        }

        if ( isset($form->filtro_digitador) ) {
            $documentos = $documentos->WhereIn('id_digitador',$form->filtro_digitador);
        }

        if ( isset($form->filtro_item_presupuestario) ) {
            $documentos = $documentos->whereHas('getDevengos',function ($query) use ($form) {
                $query->WhereIn('id_item_presupuestario',$form->filtro_item_presupuestario);
            });
        }

        if ( $form->filtro_numero_documento_compra != null ) {
            $documentos = $documentos->where('documento_compra', trim($form->filtro_numero_documento_compra) );
        }
                               
                               
        $documentos = $documentos->orderby('id','asc')
                                 ->get();
                                 
        $data = [];

        if ( $request->get('length') != -1 ) {
            $hasta = $request->get('start') + $request->get('length');
        } else {
            // Se quieren ver todos los datos
            $hasta = $request->get('start') + $documentos->count();
        }
        
        for( $i = $request->get('start') ; $i < $hasta ; $i++ ) {
            if ( isset($documentos[$i]) ) {
                $data[] = $documentos[$i]->getDatosSolicitudVistoBueno();
            }
        }

        // Se realiza la busqueda y se ordenan los datos
        if ( $documentos->count() > 0 ) {

            foreach ($data as $clave => $fila) {

                if ( $search != '' ) {
                    if ( in_array_r($search,$fila) ) {
                        // dd("lo encuentra",$fila,$search);
                        $columna[$clave] = $fila[$order_column];
                    } else {
                        // dd("NO lo encuentra",$fila,$search);
                        unset( $data[$clave] ); // quita el elemento del data
                    }
                } else {
                    $columna[$clave] = $fila[$order_column];
                }
                
            }

            
            // Se ordenan los datos, segun la columna seleccionada
            if ( $order_dir == 'asc' ) {
                array_multisort($columna, SORT_ASC, $data);
            } elseif ( $order_dir == 'desc' ) {
                array_multisort($columna, SORT_DESC, $data);
            }

        }
            

        $respuesta = array(
            "draw" => $request->get('draw'),
            "recordsTotal" => $documentos->count(),
            "recordsFiltered"=> $documentos->count(),
            "data" => $data
        );

        return json_encode($respuesta);
    }

    /**
     * Index de documentos para generar Memo que solicita Visto Bueno
     */
    public function getIndexVistoBueno()
    {
        if (!\Entrust::can(['solicitar-visto-bueno'])) {
            return \Redirect::to('home');
        }

        // $documentos = Documento::where('id_visto_bueno',null)
        //                        ->where('id_referente_tecnico','<>',0)
        //                        ->whereYear('fecha_recepcion',date('Y'))
        //                        ->orderby('id','asc')
        //                        ->get();


        $tiposDocumento = TipoDocumento::whereNotIn('id',[10,11])->get();
        // $tiposDocumento = TipoDocumento::all();
        $modalidadesCompra = ModalidadCompra::all();
        $tiposAdjudicacion = TipoAdjudicacion::all();
        // $tiposInforme = TipoInforme::all();
        $tiposInforme = TipoInforme::whereNotIn('id',[7,8])->get();
        $usuariosResponsables = User::where('responsable',1)->get();
        $referentesTecnicos = ReferenteTecnico::all();
        $usuariosDigitadores = User::has('getDocumentos')->get();
        $proveedores = Proveedor::all();
        $itemsPresupuestarios = ItemPresupuestario::all();       

        // Select para Fechas a utilizar en el filtro
        $fechaUtilizar1 = new \stdClass;
        $fechaUtilizar1->opcion = 'Ingreso';
        $fechaUtilizar2 = new \stdClass;
        $fechaUtilizar2->opcion = 'Recepción';

        $fechasUtilizar[] = $fechaUtilizar1;
        $fechasUtilizar[] = $fechaUtilizar2;

        $filtroFechaInicio = date("d/m/Y");
        $filtroFechaTermino = date("d/m/Y");
        
        return view('referente_tecnico.index_visto_bueno')->with('tiposDocumento',$tiposDocumento)
                                                          ->with('modalidadesCompra',$modalidadesCompra)
                                                          ->with('tiposAdjudicacion',$tiposAdjudicacion)
                                                          ->with('tiposInforme',$tiposInforme)
                                                          ->with('usuariosResponsables',$usuariosResponsables)
                                                          ->with('referentesTecnicos',$referentesTecnicos)
                                                          ->with('usuariosDigitadores',$usuariosDigitadores)
                                                          ->with('proveedores',$proveedores)
                                                          ->with('itemsPresupuestarios',$itemsPresupuestarios)
                                                          ->with('fechasUtilizar',$fechasUtilizar);
    }

    /**
     * Modal para generar memo
     */
    public function getModalGenerarMemo($id)
    {
        $documento = Documento::findOrFail($id);
        // obtener ultimo visto bueno, para mostrar el numero de memo siguiente.
        $lastVistoBueno = VistoBueno::select('id')->get()->last()->id + 1;
        return view('referente_tecnico.modal_generar_memo')->with('documento',$documento)
                                                          ->with('lastVistoBueno',$lastVistoBueno);
    }

    /**
     * Funcion que recibe el post y genera el memo
     */
    public function postGenerarMemo(Request $request)
    {
        // Mensajes del validador
        $messages = [
            'required' => 'Debe ingresar el :attribute',
            'numeric' => 'El :attribute debe solo contener números',
            'max' => 'El :attribute no debe exeder los :max caracteres',
            'min' => 'El :attribute debe tener minimo :min caracteres',
            'unique' => 'El valor :attribute ya existe en la base de datos',
            'date_format' => 'La :attribute no tiene el formato correcto: día/mes/año',
        ];

        // Reglas del validador
        $rules = [
            'numero_memo' => 'required',
            'fecha_solicitud' => 'required|date_format:"d/m/Y"',
        ];

        // Validador de los input del formulario
        $validator = \Validator::make($request->all(), $rules, $messages);

        //Si contiene errores se devuelven todos los errores, de lo contrario guarda en la base de datos
        if ( $validator->fails() ) {
            return response()->json($validator->errors(),400);
        } else {
            $documento = Documento::findOrFail($request->input('_id'));
            $newVistoBueno = new VistoBueno($request,$documento->id_referente_tecnico);

            $documento->id_visto_bueno = $newVistoBueno->id;
            $documento->id_rechazo_visto_bueno = null;
            $documento->save();

            $datos = array(
                'mensaje' => 'Memo para Visto Bueno registrado correctamente.',
                'estado' => 'success',
                'id' => $documento->id,
            );
        }

        return response()->json($datos,200);
    }

    /**
     * Modal para generar memo masivo
     */
    public function getModalGenerarMemoMasivo($arregloIdsDocs)
    {
        $idsDocs = json_decode($arregloIdsDocs);
        $documentos = Documento::whereIn('id',$idsDocs)->get();
        foreach($documentos as $documento) {
            $idReferenteTecnico = $documento->id_referente_tecnico;
        }
        // obtener ultimo visto bueno, para mostrar el numero de memo siguiente.
        $lastVistoBueno = VistoBueno::select('id')->get()->last()->id + 1;

        return view('referente_tecnico.modal_generar_memo_masivo')->with('documentos',$documentos)
                                                                  ->with('idReferenteTecnico',$idReferenteTecnico)
                                                                  ->with('lastVistoBueno',$lastVistoBueno);
    }

    

    /**
     * Funcion que recibe el post y genera el memo en base a más de un documento,memo masivo
     */
    public function postGenerarMemoMasivo(Request $request)
    {
        // dd('memo masivo', $request->all());
        // Mensajes del validador
        $messages = [
            'required' => 'Debe ingresar el :attribute',
            'numeric' => 'El :attribute debe solo contener números',
            'max' => 'El :attribute no debe exeder los :max caracteres',
            'min' => 'El :attribute debe tener minimo :min caracteres',
            'unique' => 'El valor :attribute ya existe en la base de datos',
            'date_format' => 'La :attribute no tiene el formato correcto: día/mes/año',
        ];

        // Reglas del validador
        $rules = [
            'numero_memo' => 'required',
            'fecha_solicitud' => 'required|date_format:"d/m/Y"',
            'idReferenteTecnico' => 'required',
        ];

        // Validador de los input del formulario
        $validator = \Validator::make($request->all(), $rules, $messages);

        //Si contiene errores se devuelven todos los errores, de lo contrario guarda en la base de datos
        if ( $validator->fails() ) {
            return response()->json($validator->errors(),400);
        } else {
            $newVistoBueno = new VistoBueno($request,$request->input('idReferenteTecnico'));

            $arregloIdsDocs = [];
            foreach ($request->input('id_doc') as $idDocumento => $id) {
                $documento = Documento::findOrFail($idDocumento);
                $documento->id_visto_bueno = $newVistoBueno->id;
                $documento->id_rechazo_visto_bueno = null;
                $documento->save();
                $arregloIdsDocs[] = $documento->id;
            }
            

            $datos = array(
                'mensaje' => 'Memo para Visto Bueno registrado correctamente.',
                'estado' => 'success',
                'ids' => $arregloIdsDocs,
            );
        }

        return response()->json($datos,200);
    }

    /**
     * Index de los memos generados, para registrar el visto bueno del referente tecnico
     */
    public function getIndexMemo()
    {

        if (!\Entrust::can(['ver-memo-solicitud-visto-bueno','editar-solicitud-visto-bueno','registro-visto-bueno'])) {
            return \Redirect::to('home');
        }

        $tiposDocumento = TipoDocumento::all();
        $proveedores = Proveedor::all();

        return view('referente_tecnico.index_memo')->with('tiposDocumento', $tiposDocumento)->with('proveedores', $proveedores);

    }

    public function dataTableMemoVistoBueno(Request $request)
    {
        set_time_limit ( 0 );
        ini_set('max_execution_time', 0);

        $search = $request->get('search')['value'];
        $order_column = $request->get('order')[0]['column'];
        $order_dir = $request->get('order')[0]['dir'];

        $formJSON = json_decode($request->get('form'));
        $form = construyeFormulario($formJSON);

        $vistosBuenos = VistoBueno::where('id_registrador_memo_respuesta',null)
                                  ->orderby('memo','desc');

        if ( \Entrust::hasRole('referente-tecnico') ) {
            $vistosBuenos = $vistosBuenos->where('id_referente_tecnico', Auth::user()->getReferenteTecnico->id);
        }

        if ( $form->filtro_numero_documento != null ) {

            $vistosBuenos = $vistosBuenos->whereHas('getDocumentos', function ($query) use ($form) {
                                                $query->where('numero_documento', trim($form->filtro_numero_documento) );
                                           });

        }

        if ( isset($form->filtro_proveedor) ) {

            $vistosBuenos = $vistosBuenos->whereHas('getDocumentos', function ($query) use ($form) {
                                                $query->WhereIn('id_proveedor', $form->filtro_proveedor);
                                           });

        }

        if ( isset($form->filtro_tipo_documento) ) {

            $vistosBuenos = $vistosBuenos->whereHas('getDocumentos', function ($query) use ($form) {
                                                $query->WhereIn('id_tipo_documento',$form->filtro_tipo_documento);
                                           });

        }

        if ( $form->filtro_numero_memo!= null ) {
            $vistosBuenos = $vistosBuenos->where('memo', $form->filtro_numero_memo);
        }

        $vistosBuenos = $vistosBuenos->get();

        $data = [];

        if ( $request->get('length') != -1 ) {
            $hasta = $request->get('start') + $request->get('length');
        } else {
            // Se quieren ver todos los datos
            $hasta = $request->get('start') + $vistosBuenos->count();
        }
        
        for( $i = $request->get('start') ; $i < $hasta ; $i++ ) {
            if ( isset($vistosBuenos[$i]) ) {
                $data[] = $vistosBuenos[$i]->getDatosMemosVistoBueno();
            }
        }

        // Se realiza la busqueda y se ordenan los datos
        if ( $vistosBuenos->count() > 0 ) {

            foreach ($data as $clave => $fila) {

                if ( $search != '' ) {
                    if ( in_array_r($search,$fila) ) {
                        // dd("lo encuentra",$fila,$search);
                        $columna[$clave] = $fila[$order_column];
                    } else {
                        // dd("NO lo encuentra",$fila,$search);
                        unset( $data[$clave] ); // quita el elemento del data
                    }
                } else {
                    $columna[$clave] = $fila[$order_column];
                }
                
            }

            
            // Se ordenan los datos, segun la columna seleccionada
            if ( $order_dir == 'asc' ) {
                array_multisort($columna, SORT_ASC, $data);
            } elseif ( $order_dir == 'desc' ) {
                array_multisort($columna, SORT_DESC, $data);
            }

        }

        $respuesta = array(
            "draw" => $request->get('draw'),
            "recordsTotal" => $vistosBuenos->count(),
            "recordsFiltered"=> $vistosBuenos->count(),
            "data" => $data
        );

        return json_encode($respuesta);

    }

    public function getModalEliminarSolicitudVistoBueno($id)
    {
        $vistoBueno = VistoBueno::findOrFail($id);

        return view('referente_tecnico.modal_eliminar_solicitud_visto_bueno')->with('vistoBueno',$vistoBueno);
    }

    public function postEliminarSolicitudVistoBueno(Request $request)
    {
        // Mensajes del validador
        $messages = [
            'required' => 'Debe ingresar el :attribute',
            'numeric' => 'El :attribute debe solo contener números',
            'max' => 'El :attribute no debe exeder los :max caracteres',
            'min' => 'El :attribute debe tener minimo :min caracteres',
            'unique' => 'El valor :attribute ya existe en la base de datos',
            'date_format' => 'La :attribute no tiene el formato correcto: día/mes/año',
        ];

        // Reglas del validador
        $rules = [
            'numero_memo' => 'required',
            'fecha_solicitud' => 'required|date_format:"d/m/Y"',
        ];

        // Validador de los input del formulario
        $validator = \Validator::make($request->all(), $rules, $messages);

        //Si contiene errores se devuelven todos los errores, de lo contrario guarda en la base de datos
        if ( $validator->fails() ) {
            return response()->json($validator->errors(),400);
        } else {
            $vistoBueno = VistoBueno::with('getDocumentos')->findOrFail($request->input('_id'));

            foreach ($vistoBueno->getDocumentos as $key => $documento) {
                $documento->id_visto_bueno = null;
                $documento->save();
            }
            $vistoBueno->id_user_deleted = Auth::user()->id;
            $vistoBueno->save();
            $vistoBueno->delete();

            $datos = array(
                'mensajeMemo' => 'Se ha eliminado el Memo Solicitud Visto Bueno '.$vistoBueno->memo,
                'estadoMemo' => 'success',
                'id' => $vistoBueno->id,
            );
        }

        return response()->json($datos,200);
    }

    /**
     * Devuelve la cantidad de vistos buenos pendientes para el referente tecnico
     */
    public function getNumVistosBuenosPendientes()
    {
        $numDoc = 0;
        if ( \Entrust::hasRole('referente-tecnico') ) {
            $numDoc = VistoBueno::where( 'id_registrador_memo_respuesta',null )
                                ->where( 'id_referente_tecnico',Auth::user()->getReferenteTecnico->id )->count();
        }

        $liReemplaza = '<a href="'.asset('referente_tecnico/memo/index').'">';
        // $liReemplaza .= '<span class="label label-sm label-icon label-warning"><i class="fas fa-link fa-bounce"></i></span>';
        $liReemplaza .= '<i class="fas fa-tasks fa-lg fa-bounce" style="color: #008000;"></i>';
        $liReemplaza .= 'Hay '.$numDoc.' Vistos Buenos &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Pendientes';
        $liReemplaza .= '</a>';

        $datos = array (
            'estado' => 'success',
            'numDoc' => $numDoc,
            'liReemplaza' => $liReemplaza,
        );

        return response()->json($datos,200);
    }

    /**
     * Modal para editar el Memo que pide el visto bueno
     */
    public function getModalEditarMemo($id)
    {
        $vistoBueno = VistoBueno::findOrFail($id);
        $tiposArchivo = TipoArchivo::all();
        $documentosReferenteTecnico = Documento::where('id_referente_tecnico',$vistoBueno->id_referente_tecnico)
                                               ->where('id_visto_bueno', null)
                                               ->get();

        return view('referente_tecnico.modal_editar_memo')->with('vistoBueno',$vistoBueno)
                                                          ->with('tiposArchivo',$tiposArchivo)
                                                          ->with('documentosReferenteTecnico',$documentosReferenteTecnico);
    }

    /**
     * Guarda la edición realizada al Memo que pide el visto bueno
     */
    public function  postEditarMemo(Request $request)
    {
        // dd('editando memo',$request->all());
        // Mensajes del validador
        $messages = [
            'required' => 'Debe ingresar el :attribute',
            'numeric' => 'El :attribute debe solo contener números',
            'max' => 'El :attribute no debe exeder los :max caracteres',
            'min' => 'El :attribute debe tener minimo :min caracteres',
            'unique' => 'El valor :attribute ya existe en la base de datos',
            'date_format' => 'La :attribute no tiene el formato correcto: día/mes/año',
        ];

        // Reglas del validador
        $rules = [
            'numero_memo' => 'required',
            'fecha_solicitud' => 'required|date_format:"d/m/Y"',
        ];

        // Validador de los input del formulario
        $validator = \Validator::make($request->all(), $rules, $messages);

        //Si contiene errores se devuelven todos los errores, de lo contrario guarda en la base de datos
        if ( $validator->fails() ) {
            return response()->json($validator->errors(),400);
        } else {
            
            if ( $request->input('documentos') ) {
                $editVistoBueno = VistoBueno::findOrFail($request->input('_id'));
                $editVistoBueno->editMemoParaVistoBueno($request);

                $datos = array(
                    'mensajeMemo' => 'Edición exitosa del Memo para Visto Bueno',
                    'estadoMemo' => 'success',
                    'fechaSolicitud' => fecha_dmY($editVistoBueno->fecha_solicitud),
                    'memo' => $editVistoBueno->memo,
                    'responsable' => $editVistoBueno->getDigitador->name,
                    'referenteTecnico' => $editVistoBueno->getReferenteTecnico->nombre.' '.$editVistoBueno->getReferenteTecnico->responsable,
                    'id' => $editVistoBueno->id,
                );

                // Se trabajan los documentos que pertenecen al memo
                // Se recorren los documentos que pertenecen al memo y se les quita el id del visto bueno.
                foreach ($editVistoBueno->getDocumentos as $key => $documento) {
                    $documento->id_visto_bueno = null;
                    $documento->save();
                }
                // Se recorren los documentos y se les agrega el id del memo, visto bueno
                foreach ($request->input('documentos') as $key => $idDocumento) {
                    $documento = Documento::findOrFail($idDocumento);
                    $documento->id_visto_bueno = $editVistoBueno->id;
                    $documento->save();
                }

                // Eliminar los archivos seleccionados para eliminar
                if ( $request->input('delete_list') ) {
                    foreach ($request->input('delete_list') as $idArchivo) {
                        $archivoDelete = Archivo::findOrfail($idArchivo);
                        $archivoDelete->delete();
                    }
                }

                $archivo = $request->file('archivo');
                if ( $archivo != null ) {
                    $nombreOriginalArchivo = str_replace('.pdf','',$archivo->getClientOriginalName() );
                    $arreglo_nombre = explode(".", $archivo->getClientOriginalName() );
                    $extension = strtolower( end( $arreglo_nombre ) );

                    if ($extension != 'pdf') {
                        $datos['mensajeArchivo'] = 'Extension incorrecta del archivo';
                        $datos['estadoArchivo'] = 'error';
                    } else {
                        // Se trabajan las rutas del archivo, se guarda en el proyecto al final de la funcion
                        $nombre = $nombreOriginalArchivo.'_'.date('Y_m_d_G_i_s');
                        $rutaArchivo = '/documentos_memos/'.$editVistoBueno->id.'/'.$nombre.'.pdf';
                        $pathArchivo = public_path().'/documentos_memos/'.$editVistoBueno->id.'/';

                        $newArchivo = new Archivo();
                        $newArchivo->id_visto_bueno = $editVistoBueno->id;
                        $newArchivo->id_tipo_archivo = $request->input('tipo_archivo');

                        $newArchivo->nombre = $nombre;
                        $newArchivo->nombre_original = $nombreOriginalArchivo;
                        $newArchivo->ubicacion = "documentos_memos/".$editVistoBueno->id."/".$newArchivo->nombre.".pdf";
                        $newArchivo->extension = "pdf";
                        $newArchivo->peso = $archivo->getSize();
                        $newArchivo->save();

                        $datos['mensajeArchivo'] = 'Se ha guardado correctamente el archivo';
                        $datos['estadoArchivo'] = 'success';

                        $archivo->move( $pathArchivo , $nombre.'.pdf' ); // Se guarda el archivo
                    }
                    
                } else {
                    $datos['mensajeArchivo'] = '';
                    $datos['estadoArchivo'] = '';
                }

            } else {
                // No Viene Documento, hay problemas, mostrar error.
                $datos = array(
                    'mensajeMemo' => 'No se ha seleccionado ningún Documento para el Memo',
                    'estadoMemo' => 'error',
                );
            }

        }

        return response()->json($datos,200);
    }

    /**
     * Muestra modal para registrar el visto bueno del memo
     */
    public function getModalRegistrarMemoVistoBueno($id)
    {
        $vistoBueno = VistoBueno::with(['getDocumentos','getDocumentos.getProveedor','getDocumentos.getArchivos'])
                               ->findOrFail($id);
        $tiposArchivo = TipoArchivo::all();
        return view('referente_tecnico.modal_registrar_memo_visto_bueno')->with('vistoBueno',$vistoBueno)
                                                                         ->with('tiposArchivo',$tiposArchivo);
    }

    /**
     * Función para registrar el visto bueno del memo, lo debe realizar el referente tecnico
     */
    public function postRegistrarMemoVistoBueno(Request $request)
    {
        // dd('registro visto bueno',$request->all());
        // Mensajes del validador
        $messages = [
            'required' => 'Debe ingresar el :attribute',
            'numeric' => 'El :attribute debe solo contener números',
            'max' => 'El :attribute no debe exeder los :max caracteres',
            'min' => 'El :attribute debe tener minimo :min caracteres',
            'unique' => 'El valor :attribute ya existe en la base de datos',
            'date_format' => 'La :attribute no tiene el formato correcto: día/mes/año',
        ];

        // Reglas del validador
        $rules = [
            'fecha_respuesta' => 'required|date_format:"d/m/Y"',
            'numero_memo_respuesta' => 'required',
        ];

        // Validador de los input del formulario
        $validator = \Validator::make($request->all(), $rules, $messages);

        //Si contiene errores se devuelven todos los errores, de lo contrario guarda en la base de datos
        if ( $validator->fails() ) {
            return response()->json($validator->errors(),400);
        } else {

            $registroVistoBueno = VistoBueno::findOrFail($request->input('_id'));
            $registroVistoBueno->registrarVistoBueno($request);

            $datos = array(
                'mensajeMemo' => 'Registro exitoso del Visto Bueno',
                'estadoMemo' => 'success',
                'id' => $registroVistoBueno->id,
            );
            
            // Se trabajan las notas de los documentos.
            foreach ($request->input('nota_documento') as $idDocumento => $nota) {
                $documento = Documento::findOrFail($idDocumento);
                $documento->nota_visto_bueno = trim($nota);
                $documento->save();
            }
            
            // Se filtran los documentos, dependiendo si han sido seleccionados o no
            // Se buscan solo los documentos que no han sidos seleccionados para el visto bueno
            if ( $request->input('visto_bueno') != null ) {
                $documentosSinVistoBueno = Documento::whereNotIn( 'id',$request->input('visto_bueno') )
                                                    ->where('id_visto_bueno',$registroVistoBueno->id)
                                                    ->get();
            } else {
                $documentosSinVistoBueno = Documento::where('id_visto_bueno',$registroVistoBueno->id)
                                                    ->get();
            }
            
            if ( is_object($documentosSinVistoBueno) ) {
                // Se debe crear el rechazo visto bueno
                $rechazoVistoBueno = new RechazoVistoBueno();
                $rechazoVistoBueno->id_visto_bueno = $registroVistoBueno->id;
                $rechazoVistoBueno->id_user_created = Auth::user()->id;
                $rechazoVistoBueno->updated_at = null;
                $rechazoVistoBueno->save();

                foreach ( $documentosSinVistoBueno as $documento ) {
                    $documento->id_visto_bueno = null;
                    $documento->id_rechazo_visto_bueno = $rechazoVistoBueno->id;
                    $documento->invalidar();
                    $documento->save();
                }
            }

            // Para dejar los documentos como validados, gracias al visto bueno del referente tecnico.
            // Se validan los documentos relacionados del documento con visto bueno
            foreach ( $registroVistoBueno->getDocumentos as $documentoValidado ) {

                $documentoValidado->validar(); // ese es el unico validar que debe ir en los vistos buenos
                $documentoValidado->save();
                $documentoValidado->actualizaValidacionDocumentosRelacionados();

            }
            
            $archivo = $request->file('archivo');
            if ( $archivo != null ) {
                $nombreOriginalArchivo = str_replace('.pdf','',$archivo->getClientOriginalName() );
                $arreglo_nombre = explode(".", $archivo->getClientOriginalName() );
                $extension = strtolower( end( $arreglo_nombre ) );

                if ($extension != 'pdf') {
                    $datos['mensajeArchivo'] = 'Extension incorrecta del archivo';
                    $datos['estadoArchivo'] = 'error';
                } else {
                    // Se trabajan las rutas del archivo, se guarda en el proyecto al final de la funcion
                    $nombre = $nombreOriginalArchivo.'_'.date('Y_m_d_G_i_s');
                    $rutaArchivo = '/documentos_memos/'.$registroVistoBueno->id.'/'.$nombre.'.pdf';
                    $pathArchivo = public_path().'/documentos_memos/'.$registroVistoBueno->id.'/';

                    $newArchivo = new Archivo();
                    $newArchivo->id_visto_bueno = $registroVistoBueno->id;
                    $newArchivo->id_tipo_archivo = $request->input('tipo_archivo');

                    $newArchivo->nombre = $nombre;
                    $newArchivo->nombre_original = $nombreOriginalArchivo;
                    $newArchivo->ubicacion = "documentos_memos/".$registroVistoBueno->id."/".$newArchivo->nombre.".pdf";
                    $newArchivo->extension = "pdf";
                    $newArchivo->peso = $archivo->getSize();
                    $newArchivo->save();

                    $datos['mensajeArchivo'] = 'Se ha guardado correctamente el archivo';
                    $datos['estadoArchivo'] = 'success';

                    $archivo->move( $pathArchivo , $nombre.'.pdf' ); // Se guarda el archivo
                }
                
            } else {
                $datos['mensajeArchivo'] = '';
                $datos['estadoArchivo'] = '';
            }
        }

        return response()->json($datos,200);
    }

    /**
     * Muestra los memos listos
     */
    public function getIndexHistorial()
    {
        if (!\Entrust::can(['ver-memo-solicitud-visto-bueno','registro-visto-bueno','editar-registro-visto-bueno'])) {
            return \Redirect::to('home');
        }

        $vistosBuenos = VistoBueno::where('id_registrador_memo_respuesta','<>',null)
                                  ->where('id_digitador','<>',null);
                                  

        if ( \Entrust::hasRole('referente-tecnico') ) {
            $vistosBuenos = $vistosBuenos->where('id_referente_tecnico',Auth::user()->getReferenteTecnico->id)
                                         ->orderby('id','asc')->get();
        } else {
            $vistosBuenos = $vistosBuenos->orderby('id','asc')->get();
        }

        
        return view('referente_tecnico.index_historial')->with('vistosBuenos',$vistosBuenos);
    }

    /**
     * Funcion para ver el memo
     */
    public function getModalVerMemo($id)
    {
        $vistoBueno = VistoBueno::findOrFail($id);
        return view('referente_tecnico.modal_ver_memo')->with('vistoBueno',$vistoBueno);
    }

    /**
     * Modal para editar el visto bueno
     */
    public function getModalEditarVistoBueno($id)
    {
        $vistoBueno = VistoBueno::findOrFail($id);
        $tiposArchivo = TipoArchivo::all();

        return view('referente_tecnico.modal_editar_visto_bueno')->with('vistoBueno',$vistoBueno)
                                                          ->with('tiposArchivo',$tiposArchivo);
    }

    /**
     * Guarda la edición realizada al  visto bueno
     */
    public function  postEditarVistoBueno(Request $request)
    {
        // dd('editando visto bueno',$request->all());
        // Mensajes del validador
        $messages = [
            'required' => 'Debe ingresar el :attribute',
            'numeric' => 'El :attribute debe solo contener números',
            'max' => 'El :attribute no debe exeder los :max caracteres',
            'min' => 'El :attribute debe tener minimo :min caracteres',
            'unique' => 'El valor :attribute ya existe en la base de datos',
        ];

        // Reglas del validador
        $rules = [
            'numero_memo' => 'required',
            'numero_memo_respuesta' => 'required',
        ];

        // Validador de los input del formulario
        $validator = \Validator::make($request->all(), $rules, $messages);

        //Si contiene errores se devuelven todos los errores, de lo contrario guarda en la base de datos
        if ( $validator->fails() ) {
            return response()->json($validator->errors(),400);
        } else {
            
            $editVistoBueno = VistoBueno::findOrFail($request->input('_id'));
            $editVistoBueno->editVistoBueno($request);

            $datos = array(
                'mensajeMemo' => 'Edición exitosa del Visto Bueno',
                'estadoMemo' => 'success',
                'fechaSolicitud' => fecha_dmY($editVistoBueno->fecha_solicitud),
                'memo' => $editVistoBueno->memo,
                'responsable' => $editVistoBueno->getDigitador->name,
                'fechaRespuesta' => fecha_dmY($editVistoBueno->fecha_memo_respuesta),
                'memoRespuesta' => $editVistoBueno->memo_respuesta,
                'referenteTecnico' => $editVistoBueno->getReferenteTecnico->nombre.' '.$editVistoBueno->getReferenteTecnico->responsable,
                'id' => $editVistoBueno->id,
            );

            // Se trabajan las notas de los documentos.
            foreach ($request->input('nota_documento') as $idDocumento => $nota) {
                $documento = Documento::findOrFail($idDocumento);
                $documento->nota_visto_bueno = trim($nota);
                $documento->save();
            }

            // Eliminar los archivos seleccionados para eliminar
            if ( $request->input('delete_list') ) {
                foreach ($request->input('delete_list') as $idArchivo) {
                    $archivoDelete = Archivo::findOrfail($idArchivo);
                    $archivoDelete->delete();
                }
            }

            $archivo = $request->file('archivo');
            if ( $archivo != null ) {
                $nombreOriginalArchivo = str_replace('.pdf','',$archivo->getClientOriginalName() );
                $arreglo_nombre = explode(".", $archivo->getClientOriginalName() );
                $extension = strtolower( end( $arreglo_nombre ) );

                if ($extension != 'pdf') {
                    $datos['mensajeArchivo'] = 'Extension incorrecta del archivo';
                    $datos['estadoArchivo'] = 'error';
                } else {
                    // Se trabajan las rutas del archivo, se guarda en el proyecto al final de la funcion
                    $nombre = $nombreOriginalArchivo.'_'.date('Y_m_d_G_i_s');
                    $rutaArchivo = '/documentos_memos/'.$editVistoBueno->id.'/'.$nombre.'.pdf';
                    $pathArchivo = public_path().'/documentos_memos/'.$editVistoBueno->id.'/';

                    $newArchivo = new Archivo();
                    $newArchivo->id_visto_bueno = $editVistoBueno->id;
                    $newArchivo->id_tipo_archivo = $request->input('tipo_archivo');

                    $newArchivo->nombre = $nombre;
                    $newArchivo->nombre_original = $nombreOriginalArchivo;
                    $newArchivo->ubicacion = "documentos_memos/".$editVistoBueno->id."/".$newArchivo->nombre.".pdf";
                    $newArchivo->extension = "pdf";
                    $newArchivo->peso = $archivo->getSize();
                    $newArchivo->save();

                    $datos['mensajeArchivo'] = 'Se ha guardado correctamente el archivo';
                    $datos['estadoArchivo'] = 'success';

                    $archivo->move( $pathArchivo , $nombre.'.pdf' ); // Se guarda el archivo
                }
                
            } else {
                $datos['mensajeArchivo'] = '';
                $datos['estadoArchivo'] = '';
            }

        }

        return response()->json($datos,200);
    }

    /**
     * Genera el memo solicitando visto bueno
     */
    public function getGenerarPDFSolicitudVistoBueno($id)
    {
        $vistoBueno = VistoBueno::with(['getDigitador','getReferenteTecnico'])->findOrFail($id);
        $referenteTecnicoOrigen = ReferenteTecnico::where('origen',1)->first();
        // dd($vistoBueno,$vistoBueno->getDigitador);
        $vistoBueno->fechaMemo = fechaMemo($vistoBueno->fecha_solicitud);
        $content = \View::make('pdf.memo_solicitud_visto_bueno', compact('vistoBueno','referenteTecnicoOrigen'))->render();
        $mpdf = new Mpdf(['mode'=>'c','format'=>'Letter-P','mirrorMargins' => true,'tempDir' => public_path() . '/tempMpdf']);
        //$mpdf = new Mpdf(['mode'=>'c']);
        $mpdf->SetDisplayMode('fullpage');
        $mpdf->keep_table_proportions = true;
        $mpdf->showImageErrors = true;
        $content = mb_convert_encoding($content, 'UTF-8', 'UTF-8');
        $mpdf->WriteHTML($content);
        $mpdf->setFooter('<div style="border-top: 1px solid #CCCCCC;" ><div style="text-align: left; font-family: Arial, Helvetica,sans-serif; font-weight: bold;font-size: 7pt; padding-top: 4px;display: inline-block;float: left;width: 70%;">MEMORANDUM N° '.$vistoBueno->memo.' del '.$vistoBueno->fechaMemo.'.</div><div style="text-align: right; font-family: Arial, Helvetica,sans-serif; font-weight: bold;font-size: 7pt; padding-top: 4px;display: inline-block;float: right;width: 30%;">Pag.  {PAGENO} / {nbpg}</div></div>');
        $mpdf->Output('MEMORANDUM SOLICITUD N°'.$vistoBueno->memo.'.pdf','I');
    }

    /**
     * Genera el memo de visto bueno, realizado por el referente tecnico
     */
    public function getGenerarPDFVistoBueno($id)
    {
        $vistoBueno = VistoBueno::with(['getDigitador','getReferenteTecnico','getRegistrador'])->findOrFail($id);
        $referenteTecnicoOrigen = ReferenteTecnico::where('origen',1)->first();
        // dd($vistoBueno,$vistoBueno->getDigitador);
        $vistoBueno->fechaMemo = fechaMemo($vistoBueno->fecha_memo_respuesta);
        $content = \View::make('pdf.memo_visto_bueno', compact('vistoBueno','referenteTecnicoOrigen'))->render();
        $mpdf = new Mpdf(['mode'=>'c','format'=>'Letter-P','mirrorMargins' => true,'tempDir' => public_path() . '/tempMpdf']);
        //$mpdf = new Mpdf(['mode'=>'c']);
        $mpdf->SetDisplayMode('fullpage');
        $mpdf->keep_table_proportions = true;
        $mpdf->showImageErrors = true;
        $content = mb_convert_encoding($content, 'UTF-8', 'UTF-8');
        $mpdf->WriteHTML($content);
        $mpdf->setFooter('<div style="border-top: 1px solid #CCCCCC;" ><div style="text-align: left; font-family: Arial, Helvetica,sans-serif; font-weight: bold;font-size: 7pt; padding-top: 4px;display: inline-block;float: left;width: 70%;">MEMORANDUM N° '.$vistoBueno->memo_respuesta.' del '.$vistoBueno->fechaMemo.'.</div><div style="text-align: right; font-family: Arial, Helvetica,sans-serif; font-weight: bold;font-size: 7pt; padding-top: 4px;display: inline-block;float: right;width: 30%;">Pag.  {PAGENO} / {nbpg}</div></div>');
        $mpdf->Output('MEMORANDUM VISTO BUENO N°'.$vistoBueno->memo.'.pdf','I');
    }

    public function getUpload()
    {
        return view('referente_tecnico.index_masivo');
    }

    public function postUpload(Request $request)
    {
        set_time_limit ( 0 );
        ini_set('max_execution_time', 0);

        $archivo = $request->file('archivo');
        $excel = \Excel::selectSheetsByIndex(0)->load($archivo)->get();

        $log = '';
        // $archivoLog = fopen("log-visto-bueno-upload.txt", "w");

        foreach ($excel as $key => $fila) {
            // dd('holo',$fila);

            // se crea el visto bueno
            $newVistoBueno = new VistoBueno();
            $newVistoBueno->id = $fila->id;
            $newVistoBueno->memo = trim($fila->cd_memo);

            if ($fila->fc_solicitud == '0000-00-00' ) {
                $newVistoBueno->fecha_solicitud = null;
            } else {
                $newVistoBueno->fecha_solicitud = $fila->fc_solicitud;
            }
            
            if ($fila->cd_informe == 'NULL' ) {
                $newVistoBueno->informe = null;
            } else {
                $newVistoBueno->informe = $fila->cd_informe;
            }
            
            if ($fila->fc_informe == 'NULL') {
                $newVistoBueno->fecha_informe = null;
            } else {
                $newVistoBueno->fecha_informe = $fila->fc_informe;
            }
            
            if ($fila->fc_recepcion_fisica == 'NULL') {
                $newVistoBueno->fecha_recepcion_fisica = null;
            } else {
                $newVistoBueno->fecha_recepcion_fisica = $fila->fc_recepcion_fisica;
            }
            

            $newVistoBueno->id_referente_tecnico = $fila->id_servicio;
            $newVistoBueno->id_digitador = $fila->id_digitador;

            if ($fila->fc_recepcion_fisica == 'NULL') {
                $newVistoBueno->memo_respuesta = null;
            } else {
                $newVistoBueno->memo_respuesta = $fila->cd_memo_respuesta;
            }
            
            if ($fila->fc_recepcion == 'NULL') {
                $newVistoBueno->fecha_memo_respuesta = null;
            } else {
                $newVistoBueno->fecha_memo_respuesta = $fila->fc_recepcion;
            }
            
            if ($fila->id_registrador == 0) {
                $newVistoBueno->id_registrador_memo_respuesta = null;
            } else {
                $newVistoBueno->id_registrador_memo_respuesta = $fila->id_registrador;
            }
            $newVistoBueno->save();

            $log = ($key + 1).'.- Se registro correctamente El Visto Bueno : id '.$fila->id.' .'.PHP_EOL;
            // fwrite($archivoLog, $log);

        }
        // fclose($archivoLog);
        $datos = array(
            'mensaje' => 'Carga de vistos buenos exitosa.',
            'estado' => 'success',
        );

        return response()->json($datos,200);
    }


    /**
     * Funcion que muestra un texto, el que se copia en la seed.
     * Para que funcione es necesario cambiar a la base de datos de finanzas (Esta con el nombre de "factura" en la BD)
     * y cambiar la tabla de los modelos de servicio y usario
     */
    public function getSeeds()
    {
        
        $servicio = ReferenteTecnico::all();
        $string = '';
        foreach($servicio as $key => $servicio){

            $string .= PHP_EOL.'$newReferenteTecnico = new ReferenteTecnico();'.PHP_EOL;
            if ($servicio->gl_rut != null && $servicio->gl_rut != '') {
                $string .= '$newReferenteTecnico->rut = "'.trim($servicio->gl_rut).'";'.PHP_EOL;
            }
            
            $string .= '$newReferenteTecnico->nombre = "'.trim($servicio->gl_nombre).'";'.PHP_EOL;
            $string .= '$newReferenteTecnico->responsable = "'.trim($servicio->gl_responsable).'";'.PHP_EOL;
            $string .= '$newReferenteTecnico->origen = "'.trim($servicio->bo_origen).'";'.PHP_EOL;
            
            $string .= '$newReferenteTecnico->save();'.PHP_EOL;            
            
        }
        
        return "<textarea row='100000'>$string</textarea>";
    }
}
