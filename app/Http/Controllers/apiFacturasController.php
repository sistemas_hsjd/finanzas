<?php namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests;

use App\WsContrato;
use App\WsOrdenCompra;
use App\WsDocumento;
use App\WsArchivo;
use App\WsItemPresupuestario;
use App\Proveedor;
use App\Documento;
use App\Archivo;

class apiFacturasController extends Controller
{
    /**
     * Devuelve el link para poder visualizar la factura
     */
    public function ver(Request $request)
    {
        
        $datos = array(
            'messages' => [],
            'status'   => '',
            'code'     => '',
            'link'     => ''
        );

        $datosFactura = $request->input('datos_factura',null);

        $wsToken = $request->input('token',null);
        $token   = json_decode($wsToken);
        // dd($request->all(),$datosFactura,$wsToken,$token);

        if ($token != 'hsjd_ver_factura_v1') {

            $datos['code']                  = 400;
            $datos['status']                = 'error';
            $datos['messages']['resultado'] = 'Token incorrecto';

        } else {
            if ( $this->validaDatosFactura($datosFactura, $datos) ) {

                // Todo Bien
                $paramsDatosFactura = json_decode($datosFactura);
                $proveedor = Proveedor::where('rut', trim($paramsDatosFactura->rut_proveedor))
                                      ->select('rut','id')
                                      ->first();

                if ( is_object($proveedor) ) {
                    // Utilizar los datos para buscar el documento y enviar el link
                    $documento = Documento::where('id_proveedor',$proveedor->id)
                                          ->where('numero_documento',trim($paramsDatosFactura->numero_documento))
                                          ->where('total_documento',trim($paramsDatosFactura->monto_documento))
                                          ->where('id_tipo_documento',1) // Factura
                                          ->select('id_proveedor','numero_documento','total_documento','id_tipo_documento','id')
                                          ->first();

                    if ( is_object($documento) ) {
                        //Buscar el archivo
                        $archivo = Archivo::where('id_documento',$documento->id)
                                          ->where('id_tipo_archivo',1) // Factura
                                          ->first();

                        if ( is_object($archivo) ) {

                            $link = '';
                            if ( $archivo->cargado == 1 ) {
                                $link = $archivo->ubicacion;
                            } else {
                                $link = '10.4.237.28/finanzas/'.$archivo->ubicacion;
                            }

                            $datos['messages']['resultado'] = 'Todo Correcto';
                            $datos['status']                = 'success';
                            $datos['code']                  = 200;
                            $datos['link']                  = $link;

                            //dd('todo bien',$paramsDatosFactura,$paramsDatosFactura->rut_proveedor,$proveedor,$documento,$archivo);
                        } else {
                            $datos['status']                = 'error';
                            $datos['messages']['resultado'] = 'No hay registros de la factura para ser visualizada';
                            $datos['code']                  = 400;
                        }
                        
                    } else {
                        $datos['status']                = 'error';
                        $datos['messages']['resultado'] = 'No hay registros del documento';
                        $datos['code']                  = 400;
                    }
                    
                } else {
                    $datos['status']                = 'error';
                    $datos['messages']['resultado'] = 'No hay registros del proveedor';
                    $datos['code']                  = 400;
                }
                
            } else {
                $datos['code'] = 400;
            }
        }

        return response()->json($datos, $datos['code']);
    }

    /**
     * Valida los datos para ver la factura (enviar el link)
     */
    public function validaDatosFactura($datosFactura, &$datos)
    {
        $params_array = json_decode($datosFactura, true);

        //mensajes de los validadores
        $messages = [
            'required'    => 'Debe enviar el :attribute',
            'numeric'     => 'El :attribute debe solo contener números',
            'max'         => 'El :attribute no debe exeder los :max caracteres',
            'min'         => 'El :attribute debe tener minimo :min caracteres',
        ];

        //validador 
        $validator = \Validator::make($params_array, [
            'rut_proveedor'    => 'required',
            'numero_documento' => 'required',
            'monto_documento'  => 'required',
        ], $messages);

        if ($validator->fails()) {
            
            foreach ( json_decode( $validator->errors() ) as $key => $val ) {
                foreach ($val as $value) {
                    $datos['messages']['datos_factura'][] = "$value";
                }
            }
            $datos['status'] = 'error';
            return false;
            
        } else {
            return true;
        }
        
    }

    /**
     * Permite actualizar datos de OC, estos datos vienen desde Hermes
     * Actualiza el "estado" de la OC.
     * Tambien permite guardar una OC y su archivo sin recepcion.
     * Al momento que se reciba la recepcion, se actualizan los datos de la oc y archivo
     * para conectar con el documento enviado como recepcion (Factura o guia de despacho)
     */
    public function actualizarOc(Request $request)
    {

        // dd($request->all());
        $datos = array(
            'messages' => [],
            'status'   => '',
            'code'     => '',
        );

        $wsActualizaOrdenCompra = $request->input('orden_compra',null);
        $wsActualizaArchivos = $request->input('archivos',null);

        $wsToken = $request->input('token',null);
        $token = json_decode($wsToken);

        if ($token != 'test_oc_v1') {
            
            $datos['messages']['resultado'] = 'Token incorrecto';
            $datos['status']                = 'error';
            $datos['code']                  = 400;

        } else {

            if ( $this->validaOrdenCompra($wsActualizaOrdenCompra, $datos) && $this->validaArchivos($wsActualizaArchivos, $datos) ) {

                $paramsOrdenCompra = json_decode($wsActualizaOrdenCompra);
                $paramsArchivos = json_decode($wsActualizaArchivos);

                /**
                 * Obtener OC y actualizar Datos
                 * Una OC puede tener más de un registro en la Base de datos.
                 */
                $actualizaOcs = WsOrdenCompra::where('id_orden_compra', $paramsOrdenCompra->id_orden_compra)
                                             ->where('numero_orden_compra', $paramsOrdenCompra->numero_orden_compra)
                                             ->where('total_orden_compra', $paramsOrdenCompra->total_orden_compra)
                                             ->get();

                if ( $actualizaOcs->count() > 0 ) {
                    
                    foreach ($actualizaOcs as $actualizaOc) {
                        $actualizaOc->actualizarDatosOc($paramsOrdenCompra);
                    
                        $datos['messages']['resultado'] = 'Token Correcto, datos OC actualizados, No se actualiza el archivo';
                        $datos['status']                = 'success';
                        $datos['code']                  = 200;

                        foreach ($paramsArchivos as $archivo) {
                            // $newArchivo = new WsArchivo($archivo, $newDocumento->id, $newOrdenCompra->id);
                            // $actualizaArchivo = WsArchivo::where('id_tipo_archivo', $archivo->id_tipo_archivo)
                            $actualizaArchivo = WsArchivo::where('id_tipo_archivo', 3)
                                                         ->where('id_ws_orden_compra', $actualizaOc->id);

                            if ( $actualizaOc->getWsDocumento ) {
                                $actualizaArchivo = $actualizaArchivo->where('id_documento', $actualizaOc->getWsDocumento->id);
                            }

                            $actualizaArchivo = $actualizaArchivo->first();

                            // Actualizar el archivo, se debe borrar en caso que sea OC
                            if ( is_object($actualizaArchivo) ) {
                                $actualizaArchivo->actualizarArchivo($archivo);
                                $datos['messages']['resultado'] = 'Token Correcto, datos actualizados OC y su archivo';
                            } else {
                                if ( $actualizaOc->getWsDocumento ) {
                                    $newArchivo = new WsArchivo($archivo, $actualizaOc->getWsDocumento->id, $actualizaOc->id);
                                } else {
                                    $newArchivo = new WsArchivo($archivo, null, $actualizaOc->id);
                                }
                                
                                $datos['messages']['resultado'] = 'Token Correcto, datos actualizados OC y su archivo creado';
                            }
                            
                        }
                    }
                    
                } else {

                    // Crear OC y Archivos
                    $newOrdenCompra = new WsOrdenCompra($paramsOrdenCompra);
                
                    $datos['messages']['resultado'] = 'Token Correcto, Se ha guardado la OC, No se ha guardado el archivo';
                    $datos['status']                = 'success';
                    $datos['code']                  = 200;

                    foreach ($paramsArchivos as $archivo) {

                        $newArchivo = new WsArchivo($archivo, null, $newOrdenCompra->id);
                        $datos['messages']['resultado'] = 'Token Correcto, se ha guardado la OC y su archivo';

                    }

                }

            } else {
                $datos['code'] = 400;
            }

        }

        return response()->json($datos, $datos['code']);
    }

    /**
     * Permite agregar al sistema una recepcion realizada en sistema Hermes
     */
    public function agregarFacturasHermes(Request $request)
    {

        set_time_limit ( 0 );
        ini_set('max_execution_time', 0);
        
        // dd($request->all(),json_decode($request->input('token')));
        $datos = array(
            'messages' => [],
            'status'   => '',
            'code'     => '',
        );

        $wsContrato = $request->input('contrato',null); // Puede ser null, no todos los documentos tienen contrato asociado
        $wsOrdenCompra = $request->input('orden_compra',null);
        $wsDocumento = $request->input('documento',null);
        $wsArchivos = $request->input('archivos',null);
        $wsItems = $request->input('items_presupuestarios',null);

        $wsToken = $request->input('token',null);
        $token = json_decode($wsToken);
        
        if ($token != 'test_v1') {
            $datos['code']                  = 400;
            $datos['status']                = 'error';
            $datos['messages']['resultado'] = 'Token incorrecto';

        } else {
            /* para test
            $this->validaContrato($wsContrato, $datos);
            $this->validaOrdenCompra($wsOrdenCompra, $datos);
            $this->validaDocumento($wsDocumento, $datos);
            $this->validaArchivos($wsArchivos, $datos);
            $this->validaItems($wsItems, $datos);
            */
            
            /**
             * Cuidado con cambiar los modelos que empizan con ws, 
             * son utilizados en funcion kernel: CambiarRelacionHermes
             * 
             */
            if ( $this->validaContrato($wsContrato, $datos) && $this->validaOrdenCompra($wsOrdenCompra, $datos) && $this->validaDocumento($wsDocumento, $datos) && $this->validaArchivos($wsArchivos, $datos) && $this->validaItems($wsItems, $datos) ) {

                // todo bien
                $paramsOrdenCompra = json_decode($wsOrdenCompra);
                $paramsDocumento = json_decode($wsDocumento);
                $paramsArchivos = json_decode($wsArchivos);
                $paramsItems = json_decode($wsItems);
                $paramsContrato = json_decode($wsContrato); // para poder actualizar la recepcion

                // posibilidad de actualizar la recepción.
                $wsDocumento = WsDocumento::with(['getWsOrdenCompra.getWsContrato','getWsArchivos','getWsItemsPresupuestarios'])
                                          ->where('tipo_documento', $paramsDocumento->tipo_documento)
                                          ->where('documento', $paramsDocumento->documento)
                                          ->where('documento_total', $paramsDocumento->documento_total)
                                          ->whereHas('getWsOrdenCompra.getWsContrato',function ($query) use ($paramsContrato) {
                                                $query->where('rut_proveedor', $paramsContrato->rut_proveedor);
                                            })
                                          ->first();

                if ( ! is_object($wsDocumento) ) {
                        
                    $newContrato = new WsContrato($paramsContrato);

                    /**
                     * Se deberian optener todas las OC.
                     * y actualizar su archivo, con su dato.
                     * luego se debe crear la nueva OC, que es para la recepción entrante.
                     */

                    $newOrdenCompra = new WsOrdenCompra($paramsOrdenCompra, $newContrato->id);

                    $newDocumento = new WsDocumento($paramsDocumento, $newOrdenCompra->id);
                    
                    
                    foreach ($paramsItems as $item) {
                        $newItem = new WsItemPresupuestario($item ,$newDocumento->id);
                    }

                    foreach ($paramsArchivos as $archivo) {
                        $newArchivo = new WsArchivo($archivo, $newDocumento->id, $newOrdenCompra->id);
                    }

                    $datos['messages']['resultado'] = 'Todo Correcto ';
                    $datos['status']                = 'success';
                    $datos['code']                  = 200;

                } else {
                    
                    // Se encuentra el documento, por lo tanto se debe actualizar toda la recepcion

                    if ( $wsDocumento->getWsOrdenCompra->getWsContrato ) {
                        $wsDocumento->getWsOrdenCompra->getWsContrato->actualizaContrato($paramsContrato);
                    } else {
                        $newContrato = new WsContrato($paramsContrato);
                    }
                    
                    if ( $wsDocumento->getWsOrdenCompra->getWsContrato ) {
                        $wsDocumento->getWsOrdenCompra->actualizarDatosOc($paramsOrdenCompra, $wsDocumento->getWsOrdenCompra->getWsContrato->id);
                    } else {
                        $wsDocumento->getWsOrdenCompra->actualizarDatosOc($paramsOrdenCompra, $newContrato->id);
                    }
                    
                    $wsDocumento->actualizaDocumento($paramsDocumento,$wsDocumento->getWsOrdenCompra->id );

                    foreach ($wsDocumento->getWsItemsPresupuestarios as $item) {
                        $item->delete();
                    }

                    foreach ($paramsItems as $item) {
                        $newItem = new WsItemPresupuestario($item ,$wsDocumento->id);
                    }

                    foreach ($wsDocumento->getWsArchivos as $archivo) {
                        $archivo->eliminarArchivo();
                    }

                    foreach ($paramsArchivos as $archivo) {
                        $newArchivo = new WsArchivo($archivo, $wsDocumento->id, $wsDocumento->getWsOrdenCompra->id);
                    }

                    $datos['messages']['resultado'] = 'Todo Correcto, se actualizo la recepcion....';
                    $datos['status']                = 'success';
                    $datos['code']                  = 200;

                }

            } else {
                $datos['code'] = 400;
            }
        }

        return response()->json($datos, $datos['code']);
    }

    public function validaContrato($wsContrato, &$datos)
    {
        $params_array = json_decode($wsContrato, true);

        //mensajes de los validadores
        $messages = [
            'required'    => 'Debe enviar el :attribute',
            'numeric'     => 'El :attribute debe solo contener números',
            'max'         => 'El :attribute no debe exeder los :max caracteres',
            'min'         => 'El :attribute debe tener minimo :min caracteres',
        ];

        //validador de los input del formulario
        $validator = \Validator::make($params_array, [
            //'id_licitacion' => 'required',
            //'fecha_ingreso_contrato' => 'required',
            //'resolucion' => 'required',
            //'fecha_resolucion' => 'required',
            'nombre_proveedor' => 'required',
            'rut_proveedor'    => 'required',
            //'fecha_inicio_contrato' => 'required',
            //'fecha_termino_contrato' => 'required',
            //'monto_maximo' => 'required'
        ], $messages);

        if ($validator->fails()) {
            
            foreach ( json_decode( $validator->errors() ) as $key => $val ) {
                foreach ($val as $value) {
                    $datos['messages']['contrato'][] = "$value";
                }
            }
            $datos['status'] = 'error';
            return false;
            
        } else {
            return true;
        }
    }

    public function validaOrdenCompra($wsOrdenCompra, &$datos)
    {
        $params_array = json_decode($wsOrdenCompra, true);

        //mensajes de los validadores
        $messages = [
            'required' => 'Debe enviar el :attribute',
            'numeric'  => 'El :attribute debe solo contener números',
            'max'      => 'El :attribute no debe exeder los :max caracteres',
            'min'      => 'El :attribute debe tener minimo :min caracteres',
        ];

        //validador de los input del formulario
        $validator = \Validator::make($params_array, [
            'fecha_orden_compra'           => 'required',
            'fecha_recepcion_orden_compra' => 'required',
            'id_orden_compra'              => 'required',
            'numero_orden_compra'          => 'required',
            'total_orden_compra'           => 'required',
            'tipo_compra'                  => 'required',
        ], $messages);

        if ($validator->fails()) {
            
            foreach ( json_decode( $validator->errors() ) as $key => $val ) {
                foreach ($val as $value) {
                    $datos['messages']['orden_compra'][] = "$value";
                }
            }
            $datos['status'] = 'error';
            return false;
            
        } else {
            return true;
        }
    }

    public function validaDocumento($wsDocumento, &$datos)
    {
        $params_array = json_decode($wsDocumento, true);

        //mensajes de los validadores
        $messages = [
            'required' => 'Debe enviar el :attribute',
            'numeric'  => 'El :attribute debe solo contener números',
            'max'      => 'El :attribute no debe exeder los :max caracteres',
            'min'      => 'El :attribute debe tener minimo :min caracteres',
        ];

        //validador de los input del formulario
        $validator = \Validator::make($params_array, [
            'documento'                  => 'required',
            'tipo_documento'             => 'required',
            'nombre_usuario_responsable' => 'required',
            'documento_descuento_total'  => 'required',
            'documento_neto'             => 'required',
            'documento_iva'              => 'required',
            'documento_total'            => 'required'
        ], $messages);

        if ($validator->fails()) {
            
            foreach ( json_decode( $validator->errors() ) as $key => $val ) {
                foreach ($val as $value) {
                    $datos['messages']['documento'][] = "$value";
                }
            }
            $datos['status'] = 'error';
            return false;
            
        } else {
            return true;
        }
    }

    public function validaArchivos($wsArchivos, &$datos)
    {
        $auxReturn = true;
        $params_arrays = json_decode($wsArchivos, true);
        
        if ( $params_arrays == null) {

            $datos['status']               = 'error';
            $auxReturn                     = false;
            $datos['messages']['archivos'] = 'Los archivos deben venir contenidos en un Array';

        } elseif ( count($params_arrays) < 1 ) {

            $datos['status']               = 'error';
            $auxReturn                     = false;
            $datos['messages']['archivos'] = 'Debe ser al menos un archivo';

        } else {

            foreach ($params_arrays as $i => $params_array) {

                //mensajes de los validadores
                $messages = [
                    'required' => 'Debe enviar el :attribute',
                    'numeric'  => 'El :attribute debe solo contener números',
                    'max'      => 'El :attribute no debe exeder los :max caracteres',
                    'min'      => 'El :attribute debe tener minimo :min caracteres',
                ];

                if ( !is_array($params_array) ) {

                    $datos['status']               = 'error';
                    $auxReturn                     = false;
                    $datos['messages']['archivos'] = 'Los archivos deben venir contenidos en un Array';

                } else {

                    //validador de los input del formulario
                    $validator = \Validator::make($params_array, [
                        'nombre_archivo' => 'required',
                        // 'archivo_base64' => 'required',
                        // 'url_externa' => 'required'
                    ], $messages);
                    
                    if ($validator->fails()) {
                        
                        foreach ( json_decode( $validator->errors() ) as $key => $val ) {
                            foreach ($val as $value) {
                                $datos['messages']['archivos'][] = "$value para archivo : $i";
                            }
                        }
                        $datos['status'] = 'error';
                        $auxReturn       = false;
                        
                    }

                }
                
            }

        }
        

        return $auxReturn;
        
    }

    public function validaItems($wsItems, &$datos)
    {
        $auxReturn = true;
        $params_arrays = json_decode($wsItems, true);
        
        if ( $params_arrays == null) {

            $datos['status']            = 'error';
            $auxReturn                  = false;
            $datos['messages']['items'] = 'Los items presupuestarios deben venir contenidos en un Array';

        } else {
            foreach ($params_arrays as $i => $params_array) {

                //mensajes de los validadores
                $messages = [
                    'required' => 'Debe enviar el :attribute',
                    'numeric'  => 'El :attribute debe solo contener números',
                    'max'      => 'El :attribute no debe exeder los :max caracteres',
                    'min'      => 'El :attribute debe tener minimo :min caracteres',
                ];

                if ( !is_array($params_array) ) {

                    $datos['status']            = 'error';
                    $auxReturn                  = false;
                    $datos['messages']['items'] = 'Los items presupuestarios deben venir contenidos en un Array';

                } else {

                    //validador de los input del formulario
                    $validator = \Validator::make($params_array, [
                        'id_producto'             => 'required',
                        'cantidad_recepcionada'   => 'required',
                        'valor_item_recepcionado' => 'required',
                        'item_presupuestario'     => 'required'
                    ], $messages);

                    if ($validator->fails()) {
                        
                        foreach ( json_decode( $validator->errors() ) as $key => $val ) {
                            foreach ($val as $value) {
                                
                                $datos['messages']['items'][] = "$value para item : $i";
                            }
                        }
                        $datos['status'] = 'error';
                        $auxReturn       = false;
                        
                    }

                }
                
            }
        }
        

        return $auxReturn;
        
    }

}
