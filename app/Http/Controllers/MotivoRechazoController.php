<?php namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\MotivoRechazo;
use Auth;

class MotivoRechazoController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
    */
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index()
    {
        if (!\Entrust::can(['crear-motivo-rechazo','editar-motivo-rechazo','eliminar-motivo-rechazo','ver-motivo-rechazo','ver-general-motivo-rechazo'])) {
            return \Redirect::to('home');
        }
        $motivos = MotivoRechazo::all();
        return view('motivo_rechazo.index',compact('motivos'));
    }

    public function create()
    {
        return view('motivo_rechazo.modal_crear_motivo_rechazo');
    }

    public function store(Request $request)
    {
        // Mensajes del validador
        $messages = [
            'required' => 'Debe ingresar el :attribute',
            'nombre.required' => 'Debe ingresar el Nombre',
            'numeric' => 'El :attribute debe solo contener números',
            'max' => 'El :attribute no debe exeder los :max caracteres',
            'min' => 'El :attribute debe tener minimo :min caracteres',
            'unique' => 'El valor :attribute ya existe en la base de datos',
        ];

        // Reglas del validador
        $rules = [
            'nombre' => 'required',
        ];

        // Validador de los input del formulario
        $validator = \Validator::make($request->all(), $rules, $messages);

        //Si contiene errores se devuelven todos los errores, de lo contrario guarda en la base de datos
        if ( $validator->fails() ) {
            return response()->json($validator->errors(),400);
        } else {

            try {
                $newMotivo = new MotivoRechazo($request);
            } catch (QueryException $e) {
                $datos = array(
                    'estado' => 'error',
                    'mensaje' => 'Existen problemas al momento de guardar el Motivo de Rechazo',
                );
                return response()->json($datos,200);
            }

            $datos = array(
                'mensaje' => 'Se ha creado el Motivo de Rechazo',
                'estado' => 'success',
                'nombre' => $newMotivo->nombre,
                'id' => $newMotivo->id,
            );
        }

        return response()->json($datos,200);
    }

    public function show($id)
    {
        $motivo = MotivoRechazo::findOrFail($id);
        return view('motivo_rechazo.modal_ver_motivo_rechazo',compact('motivo'));
    }

    public function getModalEditar($id)
    {
        $motivo = MotivoRechazo::findOrFail($id);
        return view('motivo_rechazo.modal_editar_motivo_rechazo',compact('motivo'));
    }

    public function postEditar(Request $request)
    {
        // dd('Holo',$request->all());
        // Mensajes del validador
        $messages = [
            'required' => 'Debe ingresar el :attribute',
            'nombre.required' => 'Debe ingresar el Nombre',
            'numeric' => 'El :attribute debe solo contener números',
            'max' => 'El :attribute no debe exeder los :max caracteres',
            'min' => 'El :attribute debe tener minimo :min caracteres',
            'unique' => 'El valor :attribute ya existe en la base de datos',
        ];

        // Reglas del validador
        $rules = [
            'nombre' => 'required',
        ];
        
        // Validador de los input del formulario
        $validator = \Validator::make($request->all(), $rules, $messages);

        //Si contiene errores se devuelven todos los errores, de lo contrario guarda en la base de datos
        if ( $validator->fails() ) {
            return response()->json($validator->errors(),400);
        } else {
            $editMotivo = MotivoRechazo::findOrFail($request->input('_id'));
            $editMotivo->editMotivoRechazo($request);

            $datos = array(
                'mensaje' => 'Edición exitosa del Motivo de Rechazo',
                'estado' => 'success',
                'nombre' => $editMotivo->nombre,
                'id' => $editMotivo->id,
            );
        }

        return response()->json($datos,200);
    }

    public function getModalEliminar($id)
    {
        $motivo = MotivoRechazo::findOrFail($id);
        return view('motivo_rechazo.modal_eliminar_motivo_rechazo', compact('motivo'));
    }

    public function postEliminar(Request $request)
    {
        $deleteMotivo = MotivoRechazo::find($request->input('_id'));
        if ( !is_object($deleteMotivo) ) {
            $datos = array(
                'mensaje' => 'No se encuentra el Motivo de Rechazo en la Base de Datos',
                'estado' => 'error',
            );
        } else {
            $deleteMotivo->delete();

            $datos = array(
                'mensaje' => 'Se ha eliminado correctamente el Motivo de Rechazo.',
                'estado' => 'success',
                'id' => $deleteMotivo->id,
            );
        }

        return response()->json($datos,200);
    }
}
