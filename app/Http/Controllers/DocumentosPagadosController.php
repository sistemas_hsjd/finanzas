<?php namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Excel;

use App\Documento;
use App\TipoDocumento;
use App\ModalidadCompra;
use App\TipoAdjudicacion;
use App\TipoInforme;
use App\User;
use App\ReferenteTecnico;
use App\Proveedor;
use App\ItemPresupuestario;
use App\FolioSigfe;
use App\ReversaComprobante;
use App\ComprobanteContable;

class DocumentosPagadosController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
    */
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index()
    {

        $tiposDocumento = TipoDocumento::whereNotIn('id',[2,8])->get();
        $modalidadesCompra = ModalidadCompra::all();
        $tiposAdjudicacion = TipoAdjudicacion::all();
        $tiposInforme = TipoInforme::all();
        $usuariosResponsables = User::where('responsable',1)->get();
        $referentesTecnicos = ReferenteTecnico::all();
        $usuariosDigitadores = User::has('getDocumentos')->get();
        $proveedores = Proveedor::all();
        $itemsPresupuestarios = ItemPresupuestario::all();

        // Select para Fechas a utilizar en el filtro
        $fechaUtilizar1 = new \stdClass;
        $fechaUtilizar1->opcion = 'Ingreso';
        $fechaUtilizar2 = new \stdClass;
        $fechaUtilizar2->opcion = 'Recepción';
        $fechaUtilizar3 = new \stdClass;
        $fechaUtilizar3->opcion = 'SIGFE';

        $fechasUtilizar[] = $fechaUtilizar3;
        $fechasUtilizar[] = $fechaUtilizar2;
        $fechasUtilizar[] = $fechaUtilizar1;

        $verDocumento = \Auth::user()->can('ver-documento');

        return view('documentos_pagados.index')->with('tiposDocumento', $tiposDocumento)
                                               ->with('modalidadesCompra', $modalidadesCompra)
                                               ->with('tiposAdjudicacion', $tiposAdjudicacion)
                                               ->with('tiposInforme', $tiposInforme)
                                               ->with('usuariosResponsables', $usuariosResponsables)
                                               ->with('referentesTecnicos', $referentesTecnicos)
                                               ->with('usuariosDigitadores', $usuariosDigitadores)
                                               ->with('proveedores', $proveedores)
                                               ->with('itemsPresupuestarios', $itemsPresupuestarios)
                                               ->with('fechasUtilizar', $fechasUtilizar)
                                               ->with('verDocumento', $verDocumento);

    }

    public function dataTableDocumentosPagados(Request $request)
    {
        set_time_limit ( 0 );
        ini_set('max_execution_time', 0);

        $search = $request->get('search')['value'];
        $order_column = $request->get('order')[0]['column'];
        $order_dir = $request->get('order')[0]['dir'];

        $formJSON = json_decode($request->get('form'));
        $form = construyeFormulario($formJSON);

        $mesInicio = '';
        $añoInicio = '';
        if ( $form->filtro_fecha_inicio != '' ) {

            $mesInicio = (int) fecha_M('01/'.$form->filtro_fecha_inicio);
            $añoInicio = (int) fecha_Y('01/'.$form->filtro_fecha_inicio);

        }

        $mesTermino = '';
        $añoTermino = '';
        if ( $form->filtro_fecha_termino != '' ) {

            $mesTermino = (int) fecha_M('01/'.$form->filtro_fecha_termino);
            $añoTermino = (int) fecha_Y('01/'.$form->filtro_fecha_termino);

        }

        $pagadoTotal = isset($form->filtro_total_pagado) ? true : false;
                            
        if ( $form->filtro_numero_documento == null ) {

            /**
             * Malo: no ocupar whereHas, aumento considerable de tiempo en la consulta.
             */
            // $documentos = Documento::with([
            //     'getUnaRelacionRecepcionValidada', 'getVistoBueno',
            //     'getProveedor','getTipoDocumento',
            //     'getModalidadCompra',
            //     'getDevengos.getItemPresupuestario',
            //     'getComprobanteContableDocumento',
            //                             'getComprobanteContableDocumento.getComprobanteContable' => function ($q) 
            //                                 use ($mesInicio, $añoInicio, $mesTermino, $añoTermino) {

            //                                 $q->where('mes_proceso', '>=', $mesInicio)
            //                                   ->where('mes_proceso', '<=', $mesTermino)
            //                                   ->where('año_proceso', '>=', $añoInicio)
            //                                   ->where('año_proceso', '<=', $añoTermino);

            //                             },
            //                             'getReversaComprobante' => function ($q) use ($mesInicio, $añoInicio, $mesTermino, $añoTermino) {

            //                                 $q->where('mes_sigfe', '>=', $mesInicio) 
            //                                   ->where('mes_sigfe', '<=', $mesTermino)
            //                                   ->where('year_sigfe', '>=', $añoInicio)
            //                                   ->where('year_sigfe', '<=', $añoTermino);

            //                             },
            //                         ])
            //                         ->whereHas('getComprobanteContableDocumento',function ($query) 
            //                                         use ($mesInicio, $añoInicio, $mesTermino, $añoTermino) {

            //                                 $query->whereHas('getComprobanteContable', function ($q) 
            //                                                     use ($mesInicio, $añoInicio, $mesTermino, $añoTermino) {
            //                                               $q->where('mes_proceso', '>=', $mesInicio)
            //                                                 ->where('mes_proceso', '<=', $mesTermino)
            //                                                 ->where('año_proceso', '>=', $añoInicio)
            //                                                 ->where('año_proceso', '<=', $añoTermino);

            //                                         });
            //                         });

                                
            /**
             * Mejora query, utilizando whereIn
             */
            if ( $pagadoTotal == false ) {
                $documentos = Documento::pagados( $mesInicio, $añoInicio, $mesTermino, $añoTermino );
            } else {
                // no se consideran las fechas, la idea es obtener todo lo pagado
                $documentos = Documento::with([
                                            'getProveedor','getTipoDocumento',
                                            'getModalidadCompra',
                                            'getDevengos.getItemPresupuestario',
                                            'getComprobanteContableDocumento',
                                            'getComprobanteContableDocumento.getComprobanteContable',
                                            'getReversaComprobante',
                                            // para el estado del documento
                                            'getUnaRelacionRecepcionValidada', 'getVistoBueno',
                                            'getRelacionRecepcionesNoValidadas'
                                        ])
                                        ->has('getComprobanteContableDocumento.getComprobanteContable');

            }
            

        } else {

            // Modificar ?

            $documentos = Documento::with([
                                        'getProveedor','getTipoDocumento',
                                        'getModalidadCompra',
                                        'getDevengos.getItemPresupuestario',
                                        'getComprobanteContableDocumento',
                                        'getComprobanteContableDocumento.getComprobanteContable',
                                        'getReversaComprobante',
                                        // para el estado del documento
                                        'getUnaRelacionRecepcionValidada', 'getVistoBueno',
                                        'getRelacionRecepcionesNoValidadas'
                                    ])
                                    ->has('getComprobanteContableDocumento.getComprobanteContable');

            // no se consideran las fechas, y se busca el numero del documento
            $documentos = $documentos->where('numero_documento', trim($form->filtro_numero_documento) );

        }

        if ( isset($form->filtro_proveedor) ) {

            $documentos = $documentos->WhereIn('id_proveedor', $form->filtro_proveedor);
        }

        if ( isset($form->filtro_tipo_documento) ) {

            $documentos = $documentos->WhereIn('id_tipo_documento', $form->filtro_tipo_documento);
                                        
        }

        if ( isset($form->filtro_modalidad_compra) ) {

            $documentos = $documentos->WhereIn('id_modalidad_compra', $form->filtro_modalidad_compra);
        }

        if ( isset($form->filtro_tipo_adjudicacion) ) {

            $documentos = $documentos->WhereIn('id_tipo_adjudicacion', $form->filtro_tipo_adjudicacion);
                                        
        }

        if ( isset($form->filtro_tipo_informe) ) {

            $documentos = $documentos->WhereIn('id_tipo_informe', $form->filtro_tipo_informe);

        }

        if ( isset($form->filtro_responsable) ) {

            $documentos = $documentos->WhereIn('id_responsable', $form->filtro_responsable);
                                        
        }

        if ( isset($form->filtro_referente_tecnico) ) {

            $documentos = $documentos->WhereIn('id_referente_tecnico', $form->filtro_referente_tecnico);

        }

        if ( isset($form->filtro_digitador) ) {

            $documentos = $documentos->WhereIn('id_digitador', $form->filtro_digitador);
        }

        if ( isset($form->filtro_item_presupuestario) ) {

            $documentos = $documentos->whereHas('getDevengos', function($query) use( $form ){
                                            $query->WhereIn('id_item_presupuestario', $form->filtro_item_presupuestario);
                                        });
        }

        if ( $form->filtro_numero_documento_compra != null ) {
            $documentos = $documentos->where('documento_compra', trim($form->filtro_numero_documento_compra) );
        }

        $documentos = $documentos->get();

        $data = [];
        $verDocumento = $form->permiso_ver_documento;
        foreach ( $documentos as $documento ) {

            if ( isset($form->filtro_item_presupuestario) ) {
                $data[] = $documento->getDatosPagados($form->filtro_item_presupuestario, $form->filtro_numero_documento, $verDocumento );
            } else {
                $data[] = $documento->getDatosPagados(null, $form->filtro_numero_documento, $verDocumento);
            }

        }

        $totalPagado = 0;
        $totalReversado = 0;
        $flujoEfectivo = 0;
        foreach ( $documentos as $index => $documento ) {

            $reversadoDocumento = $documento->getReversaComprobante->sum('monto');

            $flujoDocumento = 0;
            foreach ( $documento->getComprobanteContableDocumento as $comprobante ) {
                
                if ( $comprobante->getComprobanteContable ) {
                    if ( $comprobante->getComprobanteContable->id_tipo_comprobante_contable == 1 ) {
                        $flujoDocumento += $comprobante->monto_comprobante;
                    } else {
                        $flujoDocumento -= $comprobante->monto_comprobante;
                    }
                }
                

            }

            $flujoDocumento -= $reversadoDocumento;

            $pagadoDocumento = 0;
            foreach ( $documento->getComprobanteContableDocumento as $comprobante ) {
                
                if ( $comprobante->getComprobanteContable && $comprobante->getComprobanteContable->id_tipo_comprobante_contable == 1 ) {
                    $pagadoDocumento += $comprobante->monto_comprobante;
                }

            }

            $totalPagado += $pagadoDocumento;

            $totalReversado += $reversadoDocumento;

            // Realmente este filtro no debe estar, no se pagan NC ni CCC
            if ($documento->id_tipo_documento == 4 || $documento->id_tipo_documento == 10) {
                $flujoEfectivo = $flujoEfectivo - $flujoDocumento;
            } else {
                $flujoEfectivo = $flujoEfectivo + $flujoDocumento;
            }
            
        }

         /**
         * Se obtienen las reversas que no fueron incluidas en la busqueda 
         * de los documentos pagados por periodo de tiempo
         */
        if ( $form->filtro_numero_documento == null ) {
            
            $reversas = ReversaComprobante::whereNotIn('id_documento', $documentos->pluck('id')->toArray() )
                                        ->where('mes_sigfe', '>=', $mesInicio)
                                        ->where('mes_sigfe', '<=', $mesTermino)
                                        ->where('year_sigfe', '>=', $añoInicio)
                                        ->where('year_sigfe', '<=', $añoTermino)
                                        ->get();
            
            $totalReversado += $reversas->sum('monto');
            $flujoEfectivo -= $reversas->sum('monto');

        }

        $respuesta = array(
            "data"           => $data,
            "totalPagado"    => ''.formatoMiles($totalPagado),
            "totalReversado" => '- '.formatoMiles($totalReversado),
            "flujoEfectivo"  => ''.formatoMiles($flujoEfectivo), 
        );

        return json_encode($respuesta);
    }

    public function documentosPagadosExcel(Request $request)
    {

        set_time_limit ( 0 );
        ini_set('max_execution_time', 0);

        \Excel::create('SIFCON | Documentos Pagados '.$request->input('filtro_fecha_inicio'). ' - '.$request->input('filtro_fecha_termino').' _ '.date('d-m-Y').'', function($excel) use($request) {

            $mesInicio = '';
            $añoInicio = '';
            if ( $request->input('filtro_fecha_inicio') != '' ) {

                $mesInicio = (int) fecha_M('01/'.$request->input('filtro_fecha_inicio') );
                $añoInicio = (int) fecha_Y('01/'.$request->input('filtro_fecha_inicio'));

            }

            $mesTermino = '';
            $añoTermino = '';
            if ( $request->input('filtro_fecha_termino') != '' ) {

                $mesTermino = (int) fecha_M('01/'.$request->input('filtro_fecha_termino'));
                $añoTermino = (int) fecha_Y('01/'.$request->input('filtro_fecha_termino'));

            }

            $pagadoTotal = $request->input('filtro_total_pagado');

            if ( $request->input('filtro_numero_documento') == null  ) {

                if ( $pagadoTotal == false ) {
                    $documentos = Documento::pagados( $mesInicio, $añoInicio, $mesTermino, $añoTermino );
                } else {
                    $documentos = Documento::with([
                                                'getProveedor','getTipoDocumento',
                                                'getModalidadCompra',
                                                'getDevengos.getItemPresupuestario',
                                                'getComprobanteContableDocumento',
                                                'getComprobanteContableDocumento.getComprobanteContable',
                                                'getReversaComprobante',
                                                // para el estado del documento
                                                'getUnaRelacionRecepcionValidada', 'getVistoBueno',
                                                'getRelacionRecepcionesNoValidadas'
                                            ])
                                            ->has('getComprobanteContableDocumento.getComprobanteContable');
                }

            } else {

                $documentos = Documento::with([
                                            'getProveedor','getTipoDocumento',
                                            'getModalidadCompra',
                                            'getDevengos.getItemPresupuestario',
                                            'getComprobanteContableDocumento',
                                            'getComprobanteContableDocumento.getComprobanteContable',
                                            'getReversaComprobante',
                                            // para el estado del documento
                                            'getUnaRelacionRecepcionValidada', 'getVistoBueno',
                                            'getRelacionRecepcionesNoValidadas'
                                        ])
                                        ->has('getComprobanteContableDocumento.getComprobanteContable');

                $documentos = $documentos->where('numero_documento', trim($request->input('filtro_numero_documento')) );

            }
            

            if ( $request->input('filtro_proveedor') != null ) {
                $documentos = $documentos->WhereIn('id_proveedor', $request->input('filtro_proveedor'));
            }

            if ( $request->input('filtro_tipo_documento') != null ) {
                $documentos = $documentos->WhereIn('id_tipo_documento', $request->input('filtro_tipo_documento'));
            }

            if ( $request->input('filtro_modalidad_compra') != null ) {
                $documentos = $documentos->WhereIn('id_modalidad_compra', $request->input('filtro_modalidad_compra'));
            }

            if ( $request->input('filtro_tipo_adjudicacion') != null ) {
                $documentos = $documentos->WhereIn('id_tipo_adjudicacion', $request->input('filtro_tipo_adjudicacion'));
            }

            if ( $request->input('filtro_tipo_informe') != null ) {
                $documentos = $documentos->WhereIn('id_tipo_informe', $request->input('filtro_tipo_informe'));
            }

            if ( $request->input('filtro_responsable') != null ) {
                $documentos = $documentos->WhereIn('id_responsable', $request->input('filtro_responsable'));
            }

            if ( $request->input('filtro_referente_tecnico') != null ) {
                $documentos = $documentos->WhereIn('id_referente_tecnico', $request->input('filtro_referente_tecnico'));
            }

            if ( $request->input('filtro_digitador') != null ) {
                $documentos = $documentos->WhereIn('id_digitador', $request->input('filtro_digitador'));
            }

            if ( $request->input('filtro_item_presupuestario') != null) {
                $documentos = $documentos->whereHas('getDevengos',function ($query) use ($request) {
                                                $query->WhereIn('id_item_presupuestario', $request->input('filtro_item_presupuestario'));
                                           });
            }


            if ( $request->input('filtro_numero_documento_compra') != null ) {
                $documentos = $documentos->where('documento_compra', trim($request->input('filtro_numero_documento_compra')) );
            }

            $documentos = $documentos->get();

            $reversas = ReversaComprobante::whereNotIn('id_documento', $documentos->pluck('id')->toArray())
                                          ->where('mes_sigfe', '>=', $mesInicio)
                                          ->where('mes_sigfe', '<=', $mesTermino)
                                          ->where('year_sigfe', '>=', $añoInicio)
                                          ->where('year_sigfe', '<=', $añoTermino)
                                          ->get();

            $excel->sheet('Documentos', function($sheet) use($documentos, $reversas, $request) {
            
                $sheet->row(1, [
                    'Estado', 
                    'Factoring',
                    'Chile-Compra',
                    'M/Dev',
                    'Proveedor',
                    'RUT',
                    'Tipo Dcto.',
                    'N° Dcto.',
                    'F/Doc.',
                    'M/Doc.',
                    'Fecha RD',
                    'Mes RD',
                    'Item',
                    'ID SIGFE',
                    'Fecha ID',
                    'Flujo Efectivo',
                    'Mecanismo de Adjudicación',
                    'Fecha Comprobante',
                    'N° Egreso',
                    'Folio Comprobante',
                    'Observación',
                    'Detalle',
                    'URL Factura',
                    'URL Recepción'
                ]);

                
                $totalFlujoEfectivoExcel = 0;
                $cuentaLineas = 1;
                foreach ( $documentos as $indexAux => $documento ) {
                    $cuentaLineas++;

                    $reversadoDocumento = $documento->getReversaComprobante->sum('monto');

                    $flujoEfectivoDocumento = 0;
                    foreach ( $documento->getComprobanteContableDocumento as $comprobante ) {

                        if ( $comprobante->getComprobanteContable ) {
                            if ( $comprobante->getComprobanteContable->id_tipo_comprobante_contable == 1 ) {
                                $flujoEfectivoDocumento += $comprobante->monto_comprobante;
                            } else {
                                $flujoEfectivoDocumento -= $comprobante->monto_comprobante;
                            }
                        }
                        

                    }

                    $flujoEfectivoDocumento -= $reversadoDocumento;

                    $flujoDocumento = '';
                    if ($documento->id_tipo_documento == 4 || $documento->id_tipo_documento == 10) {
                        $flujoDocumento .= '-'.$flujoEfectivoDocumento;
                        $totalFlujoEfectivoExcel = $totalFlujoEfectivoExcel - $flujoEfectivoDocumento;
                    } else {
                        $flujoDocumento .= $flujoEfectivoDocumento;
                        $totalFlujoEfectivoExcel = $totalFlujoEfectivoExcel + $flujoEfectivoDocumento;
                    }
                    
                    $items = '';
                    $mesSigfe = '';
                    $fechaSigfe = '';
                    $idSigfe = '';
                    $detalleSigfe = '';
                    foreach ( $documento->getDevengos as $index => $devengo) {
                        $items .= $devengo->getItemPresupuestario->codigo().' | ';
                        
                        if ($devengo->id_folio_sigfe != null) {
                            $mesSigfe = (int) explode("-",$devengo->getFolioSigfe->fecha_sigfe)[1];
                            $fechaSigfe = fecha_dmY($devengo->getFolioSigfe->fecha_sigfe);
                            $idSigfe = $devengo->getFolioSigfe->id_sigfe;
                            $detalleSigfe = $devengo->getFolioSigfe->detalle_sigfe;
                        }

                    }

                    // para columna factoring
                    $factoring = '';
                    if ($documento->getProveedorFactoring) {
                        $factoring .= $documento->getProveedorFactoring->nombre.' '.$documento->getProveedorFactoring->rut;
                    }

                    $urlRecepcion = '';
                    if ( $documento->getUnaRelacionRecepcionValidada ) {
                        foreach ( $documento->getRelacionRecepcionesValidadas as $relacion ) {
                            if ( $relacion->getWsDocumento ) {
                                if ( $relacion->getWsDocumento->getWsArchivos ) {
                                    foreach ($relacion->getWsDocumento->getWsArchivos as $archivo) {
                                        $urlRecepcion .= 'http://10.4.237.28/finanzas/'.$archivo->ubicacion.PHP_EOL;
                                    }
                                }
                            }
                        }
                        
                    }

                    $fechaComprobante = $documento->getFechasComprobantes('excel', $request->input('filtro_numero_documento'));
                    $numeroEgreso = $documento->getNumerosComprobantes('excel', $request->input('filtro_numero_documento'));
                    $folioEgreso = $documento->getFoliosComprobantes('excel', $request->input('filtro_numero_documento'));

                    $sheet->row($cuentaLineas, [
                        $documento->getEstadoDocumento('excel'),
                        $factoring,
                        $documento->documento_compra,
                        $mesSigfe, // mes de la fecha sigfe
                        $documento->getProveedor->nombre,
                        $documento->getProveedor->rut,
                        $documento->getTipoDocumento->nombre,
                        $documento->numero_documento,
                        fecha_dmY($documento->fecha_documento),
                        (int) explode("-",$documento->fecha_documento)[1], // mes de la fecha del documento
                        fecha_dmY($documento->fecha_recepcion),
                        (int) explode("-",$documento->fecha_recepcion)[1], // mes de la fecha de recepcion del documento
                        $items,
                        $idSigfe,
                        $fechaSigfe,
                        $flujoDocumento,
                        $documento->getTipoAdjudicacion->nombre,
                        $fechaComprobante,
                        $numeroEgreso,
                        $folioEgreso,
                        $documento->observacion,
                        $detalleSigfe,
                        $documento->getUrlFactura(),
                        $urlRecepcion
                    ]);

                }

                $cuentaLineas++;
                $sheet->row( $cuentaLineas , [
                    '',
                    '',
                    '',
                    '',
                    '',
                    '',
                    '',
                    '',
                    '',
                    '',
                    '',
                    '',
                    '',
                    '',
                    'TOTAL REVERSAS EXTTRAS',
                    $reversas->sum('monto'),
                    '',
                    '',
                    '',
                    '',
                    '',
                    '',
                    ''
                ]);
                
                $totalFlujoEfectivoExcel -= $reversas->sum('monto');
                $cuentaLineas++;
                $sheet->row( $cuentaLineas , [
                    '',
                    '',
                    '',
                    '',
                    '',
                    '',
                    '',
                    '',
                    '',
                    '',
                    '',
                    '',
                    '',
                    '',
                    'TOTAL',
                    $totalFlujoEfectivoExcel,
                    '',
                    '',
                    '',
                    '',
                    '',
                    '',
                    ''
                ]);

        });

        })->export('xlsx');

    }
}
