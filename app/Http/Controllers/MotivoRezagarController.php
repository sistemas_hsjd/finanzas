<?php namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\MotivoRezagar;
use Auth;

class MotivoRezagarController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
    */
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index()
    {
        // if (!\Entrust::can(['crear-motivo-rezagar','editar-motivo-rezagar','eliminar-motivo-rezagar','ver-motivo-rezagar','ver-general-motivo-rezagar'])) {
        //     return \Redirect::to('home');
        // }
        
        $motivos = MotivoRezagar::all();
        return view('motivo_rezagar.index',compact('motivos'));
    }

    public function create()
    {
        return view('motivo_rezagar.modal_crear_motivo_rezagar');
    }

    public function store(Request $request)
    {
        // Mensajes del validador
        $messages = [
            'required' => 'Debe ingresar el :attribute',
            'nombre.required' => 'Debe ingresar el Nombre',
            'numeric' => 'El :attribute debe solo contener números',
            'max' => 'El :attribute no debe exeder los :max caracteres',
            'min' => 'El :attribute debe tener minimo :min caracteres',
            'unique' => 'El valor :attribute ya existe en la base de datos',
        ];

        // Reglas del validador
        $rules = [
            'nombre' => 'required',
        ];

        // Validador de los input del formulario
        $validator = \Validator::make($request->all(), $rules, $messages);

        //Si contiene errores se devuelven todos los errores, de lo contrario guarda en la base de datos
        if ( $validator->fails() ) {
            return response()->json($validator->errors(),400);
        } else {

            try {
                $motivo = new MotivoRezagar($request);
            } catch (QueryException $e) {
                $datos = array(
                    'estado'  => 'error',
                    'mensaje' => 'Existen problemas al momento de guardar el Motivo para Rezagar.',
                );
                return response()->json($datos,200);
            }

            $datos = array(
                'mensaje' => 'Se ha creado el Motivo para Rezagar.',
                'estado'  => 'success',
                'nombre'  => $motivo->nombre,
                'id'      => $motivo->id,
            );
        }

        return response()->json($datos,200);
    }

    public function show($id)
    {
        $motivo = MotivoRezagar::findOrFail($id);
        return view('motivo_rezagar.modal_ver_motivo_rezagar',compact('motivo'));
    }

    public function getModalEditar($id)
    {
        $motivo = MotivoRezagar::findOrFail($id);
        return view('motivo_rezagar.modal_editar_motivo_rezagar',compact('motivo'));
    }

    public function postEditar(Request $request)
    {
        // dd('Holo',$request->all());
        // Mensajes del validador
        $messages = [
            'required' => 'Debe ingresar el :attribute',
            'nombre.required' => 'Debe ingresar el Nombre',
            'numeric' => 'El :attribute debe solo contener números',
            'max' => 'El :attribute no debe exeder los :max caracteres',
            'min' => 'El :attribute debe tener minimo :min caracteres',
            'unique' => 'El valor :attribute ya existe en la base de datos',
        ];

        // Reglas del validador
        $rules = [
            'nombre' => 'required',
        ];
        
        // Validador de los input del formulario
        $validator = \Validator::make($request->all(), $rules, $messages);

        //Si contiene errores se devuelven todos los errores, de lo contrario guarda en la base de datos
        if ( $validator->fails() ) {
            return response()->json($validator->errors(),400);
        } else {
            $motivo = MotivoRezagar::findOrFail($request->input('_id'));
            $motivo->editMotivoRezagar($request);

            $datos = array(
                'mensaje' => 'Edición exitosa del Motivo para Rezagar.',
                'estado'  => 'success',
                'nombre'  => $motivo->nombre,
                'id'      => $motivo->id,
            );
        }

        return response()->json($datos,200);
    }

    public function getModalEliminar($id)
    {
        $motivo = MotivoRezagar::findOrFail($id);
        return view('motivo_rezagar.modal_eliminar_motivo_rezagar', compact('motivo'));
    }

    public function postEliminar(Request $request)
    {
        $motivo = MotivoRezagar::find($request->input('_id'));
        if ( !is_object($motivo) ) {
            $datos = array(
                'mensaje' => 'No se encuentra el Motivo para Rezagar en la Base de Datos',
                'estado' => 'error',
            );
        } else {
            $motivo->delete();

            $datos = array(
                'mensaje' => 'Se ha eliminado correctamente el Motivo para Rezagar.',
                'estado'  => 'success',
                'id'      => $motivo->id,
            );
        }

        return response()->json($datos,200);
    }
}
