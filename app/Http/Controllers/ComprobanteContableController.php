<?php namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests;
use Auth;
use Mpdf\Mpdf;

use App\ComprobanteContable;
use App\TipoComprobanteContable;
use App\CuentaBancaria;
use App\Documento;
use App\ComprobanteContableDocumento;
use App\TipoDocumento;
use App\CuentaContable;
use App\Proveedor;
use App\ReversaComprobante;
use App\Banco;
use App\MedioPago;
use App\User;


class ComprobanteContableController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
    */
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function crearComprobantesTGR()
    {
        set_time_limit ( 0 );
        ini_set('max_execution_time', 0);
        // $comprobantesDocs = ComprobanteContableDocumento::with(['getComprobanteContable'])
        //                                                 ->where('id_cuenta_bancaria', 1)
        //                                                 ->whereHas('getComprobanteContable', function( $q ){
        //                                                     $q->where('id_tipo_comprobante_contable', 1) // Egreso
        //                                                     ->where('mes_proceso', 4)
        //                                                       ->where('año_proceso', 2020);
        //                                                 })
        //                                                 ->join('comprobantes_contables', 
        //                                                 'comprobantes_contables_documentos.id_comprobante_contable',
        //                                                 '=',
        //                                                 'comprobantes_contables.id')
        //                                                 ->orderBy('comprobantes_contables.numero_comprobante', 'asc')
        //                                                 ->select('comprobantes_contables_documentos.*')
        //                                                 ->get();

        // dd( "Cantidad de comprobantes :". $comprobantesDocs->count());
        // Esto no debe cambiar según la fecha

        // el más antiguo es del 11-05-2020

        $comprobanteDoc = ComprobanteContableDocumento::with(['getComprobanteContable'])
                                                        ->where('id_cuenta_bancaria', 1)
                                                        ->whereHas('getComprobanteContable', function( $q ){
                                                            $q->where('id_tipo_comprobante_contable', 1); // Egreso
                                                            //   ->where('mes_proceso', 4)
                                                            //   ->where('año_proceso', 2020);
                                                        })
                                                        ->join('comprobantes_contables', 
                                                        'comprobantes_contables_documentos.id_comprobante_contable',
                                                        '=',
                                                        'comprobantes_contables.id')
                                                        ->orderBy('comprobantes_contables.numero_comprobante', 'asc')
                                                        ->select('comprobantes_contables_documentos.*')
                                                        ->first();
                
        if ( is_object( $comprobanteDoc ) ) {
            // dd( $comprobanteDoc, $comprobanteDoc->getProveedor, $comprobanteDoc->getDocumento->numero_documento );
            $comprobantesDocs = ComprobanteContableDocumento::with(['getComprobanteContable', 'getDocumento'])
                                                        ->where('id_cuenta_bancaria', 1)
                                                        ->whereHas('getComprobanteContable', function( $q ){
                                                            $q->where('id_tipo_comprobante_contable', 1); // Egreso
                                                            //   ->where('mes_proceso', 4)
                                                            //   ->where('año_proceso', 2020);
                                                        })
                                                        ->where('comprobantes_contables_documentos.id_proveedor', $comprobanteDoc->id_proveedor)
                                                        ->join('documentos', 
                                                        'comprobantes_contables_documentos.id_documento',
                                                        '=',
                                                        'documentos.id')
                                                        ->select('comprobantes_contables_documentos.*')
                                                        ->orderBy('documentos.numero_documento', 'asc')
                                                        ->get();

            /**
             * Se debe crear un comprobante contable del tipo traspaso.
             * debe contener todos los documentos traidos de este proveedor,
             * deben estar en orden.
             * Se debe eliminar el comprobante contable asociado a cada 
             * comprobanteContableDocumento.
             */

            //Crear comprobante
            $comprobante = new ComprobanteContable();
            $comprobante->id_tipo_comprobante_contable = 3; // Traspaso

            $comprobante->fecha_proceso = $comprobanteDoc->getComprobanteContable->fecha_proceso;
            $comprobante->dia_proceso = $comprobanteDoc->getComprobanteContable->dia_proceso;
            $comprobante->mes_proceso = $comprobanteDoc->getComprobanteContable->mes_proceso;
            $comprobante->año_proceso = $comprobanteDoc->getComprobanteContable->año_proceso;
            $comprobante->folio = null; // se debe pasar el folio de cada comprobante a cada comprobanteContableDocumento
            $comprobante->id_user_created = $comprobanteDoc->getComprobanteContable->id_user_created;
            $comprobante->setNumeroComprobante();
            $comprobante->carga_sigfe = 1;
            $comprobante->save();

            // Se reccoren los comprobantesContablesDocumento para asociarlos al nuevo comprobante.
            foreach ( $comprobantesDocs as $compDoc ) {

                $compDoc->folio = $compDoc->getComprobanteContable->folio;
                $compDoc->id_comprobante_contable = $comprobante->id;
                $compDoc->save();
                $comprobante->monto_total += $compDoc->monto_comprobante;

                /**
                 * Borrar el comprobante que tenia el compDoc, no se evalua si tiene más doc asociados
                 * porque debe ser una relación 1 a 1 para este caso (TGR)
                 */
                $compDoc->getComprobanteContable->delete();
            }

            $comprobante->save(); // para guardar el monto total del comprobante

            /**
             * Para ajustar el numero del comprobante, de egreso
             * utilizar la fecha de proceso de las querys.
             * Obtener todos los egresos de ese fecha de proceso y comenzar los números,
             * como si fueran nuevos comprobantes.
             * Se supone que se sube el excel del mes de proceso anterior al actual, o el presente.
             * Pensar como y cuando generar el n° de comprobante, para que no séa un
             * catastrofe entregar nuevos n° a los comprobantes de egreso.
             * De estos n° debe quedar constancía, no pueden estar cambiando siempre.
             * 
             * 
             * Finalmente no es necesario ajustar el n° de los comprobantes de egreso, 
             * esto debe solucionarse cuando el modulo de tesoreria este funcionando
             * correctamente      
             */
            $this->crearComprobantesTGR();
        } else {
            return "No quedan comprobantes";
        }

    }

    public function test()
    {

        set_time_limit(0);
        ini_set('max_execution_time', 0);
        
        $comprobantes = ComprobanteContable::where('carga_sigfe',0)->get();


        // $comprobantesReversas = ComprobanteContable::with(['getComprobantesContablesDocumentos.getDocumento.getReversaComprobante'])
        //                             ->has('getComprobantesContablesDocumentos.getDocumento.getReversaComprobante')
        //                             // ->take(1)
        //                             ->get();

        
        $numReversas = 0;
        foreach($comprobantes as $comprobante) {

            foreach ($comprobante->getComprobantesContablesDocumentos as $compConDoc) {
                $compConDoc->carga_sigfe = 1;
                $compConDoc->save();
                // foreach($compConDoc->getDocumento->getReversaComprobante as $reversa) {

                //     if ( $reversa->folio > $comprobante->folio ) {

                //         $comprobante->reversado = 1;
                //         // $comprobante->save();
                //         $compConDoc->getDocumento->id_estado = 2;
                //         // $compConDoc->getDocumento->save();
                //         $numReversas++;

                //     }

                // }

            }

            $comprobante->carga_sigfe = 1;
            $comprobante->save();
            echo "Actualizado comprobante {$comprobante->id}";
        }

        // dd( $comprobantes->count() );
        return "listo";

    }

    public function test2()
    {

        set_time_limit(0);
        ini_set('max_execution_time', 0);

        $comprobantes = ComprobanteContable::with([
                                                'getComprobantesContablesDocumentos.getDocumento',
                                                'getUnComprobanteContableDocumento.getDocumento.getTipoDocumento',
                                                'getUnComprobanteContableDocumento.getDocumento.getProveedor'
                                            ])
                                            ->has('getComprobantesContablesDocumentos')
                                            // ->where('titulo', null)
                                            // ->where('descripcion', null)
                                            ->get();

        foreach ( $comprobantes as $key => $comprobante ) {

            if ( $comprobante->id_tipo_comprobante_contable == 1 ) {
                
                // Egreso
                $comprobante->titulo = 'Cancela ';
                $comprobante->titulo .= $comprobante->getUnComprobanteContableDocumento->getDocumento->getTipoDocumento->nombre;
                $comprobante->titulo .= ' '.$comprobante->getUnComprobanteContableDocumento->getProveedor->nombre;
                
            } elseif ( $comprobante->id_tipo_comprobante_contable == 2 ) {

                // Ingreso
                $comprobante->titulo = 'Percibe ';
                $comprobante->titulo .= $comprobante->getUnComprobanteContableDocumento->getDocumento->getTipoDocumento->nombre;
                $comprobante->titulo .= ' '.$comprobante->getUnComprobanteContableDocumento->getProveedor->nombre;

            } elseif ( $comprobante->id_tipo_comprobante_contable == 3 ) {

                // Traspaso
                $comprobante->titulo = 'Traspaso ';
                $comprobante->titulo .= $comprobante->getUnComprobanteContableDocumento->getDocumento->getTipoDocumento->nombre;
                $comprobante->titulo .= ' '.$comprobante->getUnComprobanteContableDocumento->getProveedor->nombre;                

            }

            $comprobante->descripcion = '';
            foreach ( $comprobante->getComprobantesContablesDocumentos as $key2 => $comprobanteContableDocumento ) {

                $comprobante->descripcion .= $comprobanteContableDocumento->getDocumento->numero_documento;

                if ( ($key2 + 1 ) < $comprobante->getComprobantesContablesDocumentos->count() ) {
                    $comprobante->descripcion .= ', ';
                }

            }

            $comprobante->save();

        }

        return 'Comprobantes modificados '.$comprobantes->count();   
    }

    public function test3()
    {
        set_time_limit(0);
        ini_set('max_execution_time', 0);

        $documentos = Documento::withTrashed()->get();

        foreach ( $documentos as $doc ) {
            $doc->dias_antiguedad = diferencia_dias( $doc->fecha_recepcion, date('Y-m-d') ) ;
            $doc->save();
        }

        return 'Documentos modificados '.$documentos->count();
    }

    public function index()
    {
        $comprobanteBoleta = ComprobanteContable::with(['getTipoComprobante','getUnComprobanteContableDocumento.getDocumento'])
                                          ->whereHas('getUnComprobanteContableDocumento.getDocumento', function ($query) {
                                              $query->where('id_tipo_documento', 3);
                                          })
                                          ->first();

        $comprobanteBoletaTesoreria = ComprobanteContable::with(['getTipoComprobante','getUnComprobanteContableDocumento.getProveedor'])
                                          ->whereHas('getUnComprobanteContableDocumento.getProveedor', function ($query) {
                                              $query->where('rut', '60805000-0');
                                          })
                                          ->first();

        $comprobanteReversado = ComprobanteContable::with(['getTipoComprobante','getUnComprobanteContableDocumento.getProveedor'])
                                                  ->where('reversado', 1)
                                                  ->first();
                                          
        return view('comprobante_contable.index')->with('comprobanteBoleta',$comprobanteBoleta)
                                                 ->with('comprobanteBoletaTesoreria', $comprobanteBoletaTesoreria)
                                                 ->with('comprobanteReversado', $comprobanteReversado);
    }

    public function dataTableIndex(Request $request)
    {
        set_time_limit ( 0 );
        ini_set('max_execution_time', 0);

        $search = $request->get('search')['value'];
        $order_column = $request->get('order')[0]['column'];
        $order_dir = $request->get('order')[0]['dir'];

        $dataTable = $request->get('dataTable');

        $form = construyeFormulario( json_decode($request->get('form')) );
        
        // $fechaInicio = fecha_Y_m_d($form->filtro_fecha_inicio);
        // $fechaTermino = fecha_Y_m_d($form->filtro_fecha_termino);

        $comprobantes = ComprobanteContable::all();

        $data = [];

        foreach ( $comprobantes as $comprobante ) {
            $data[] = $comprobante->getDatosIndex();
        }

        // if ( $request->get('length') != -1 ) {
        //     $hasta = $request->get('start') + $request->get('length');
        // } else {
        //     // Se quieren ver todos los datos
        //     $hasta = $request->get('start') + $comprobantes->count();
        // }

        // for( $i = $request->get('start') ; $i < $hasta ; $i++ ) {
        //     if ( isset($comprobantes[$i]) ) {
        //         $data[] = $comprobantes[$i]->getDatosIndex();
        //     }
        // }

        // if ( $comprobantes->count() > 0 ) {

        //     foreach ($data as $clave => $fila) {

        //         if ( $search != '' ) {
        //             if ( in_array_r($search,$fila) ) {
        //                 // dd("lo encuentra",$fila,$search);
        //                 $columna[$clave] = $fila[$order_column];
        //             } else {
        //                 // dd("NO lo encuentra",$fila,$search);
        //                 unset( $data[$clave] ); // quita el elemento del data
        //             }
        //         } else {
        //             $columna[$clave] = $fila[$order_column];
        //         }
                
        //     }

            
        //     // Se ordenan los datos, segun la columna seleccionada
        //     if ( $order_dir == 'asc' ) {
        //         array_multisort($columna, SORT_ASC, $data);
        //     } elseif ( $order_dir == 'desc' ) {
        //         array_multisort($columna, SORT_DESC, $data);
        //     }

        // }

        $respuesta = array(
            // "draw" => $request->get('draw'),
            // "recordsTotal" => $comprobantes->count(),
            // "recordsFiltered"=> $comprobantes->count(),
            "data" => $data
        );

        return json_encode($respuesta);

    }

    /**
     * Para generar un nuevo comprobante:
     * - seleccionar tipo comprobante contable
     * - ingresar folio (temporalmente)
     * - seleccionar fecha de proceso del comprobante contable
     */
    public function getModalNuevoComprobante()
    {
        $tiposComprobantes = TipoComprobanteContable::all();
        return view('comprobante_contable.modal_crear')->with('tiposComprobantes', $tiposComprobantes);
    }

    public function store(Request $request)
    {
        // dd($request->all());

        // Mensajes de los validadores
        $messages = [
            'required'    => 'Debe ingresar el  :attribute',
            'numeric' => 'El :attribute debe solo contener números',
            'date_format' => 'La :attribute no tiene el formato correcto: día/mes/año',
        ];

        //validador de los input del formulario
        $validator = \Validator::make($request->all(), [
            'tipo_comprobante' => 'required',
            'fecha_proceso' => 'required|date_format:"d/m/Y"'
        ], $messages);

        //Si contiene errores se devuelven todos los errores, de lo contrario guarda en la base de datos
        if ( $validator->fails() ) {
            return response()->json($validator->errors(),400);
        } else {

            $comprobante = new ComprobanteContable;        
            $comprobante->id_tipo_comprobante_contable = $request->tipo_comprobante;
            $comprobante->fecha_proceso = fecha_Y_m_d($request->fecha_proceso);
            $comprobante->dia_proceso = fecha_D($request->fecha_proceso);
            $comprobante->mes_proceso = fecha_M($request->fecha_proceso);
            $comprobante->año_proceso = fecha_Y($request->fecha_proceso);
            $comprobante->id_user_created = \Auth::user()->id;
            $comprobante->setNumeroComprobante();
            $comprobante->save();

            $datos = array(
                'idComprobante' => $comprobante->id,
                'estado'        => 'success',
                'mensaje'       => 'Comprobante creado con éxito.'
            );

            return response()->json($datos,200);

        }
        
    }

    public function getEdit($id , $nuevo = false)
    {
        $comprobante = ComprobanteContable::with(['getTipoComprobante','getUnComprobanteContableDocumento.getDocumento'])
                                          ->findOrFail($id);

        $nominas = Documento::has('getNominaPagoDocumento')->where('id_estado', 2)->get();
        
        // mostrar mensaje en la vista, en caso que se creo recientemente el comprobante
        return view('comprobante_contable.edit')->with('comprobante', $comprobante)
                                                ->with('nuevo', $nuevo)
                                                ->with('nominas', $nominas);
        
    }

    public function getSeleccionarDocumentos($idComprobante)
    {
        // llevar a modal para seleccionar proveedor y cuenta, luego se deben seleccionar los documentos a ingresar al comprobante
        $cuentas = CuentaBancaria::all();
        $cuentasContables = CuentaContable::all();
        $tiposDocumento = TipoDocumento::whereIn('id', [1,2,6,7])->get();
        return view('comprobante_contable.modal_seleccionar_documentos')->with('cuentas', $cuentas)
                                                                        ->with('idComprobante', $idComprobante)
                                                                        ->with('cuentasContables', $cuentasContables)
                                                                        ->with('tiposDocumento', $tiposDocumento);
    }

    public function dataTableDocumentosParaComprobante(Request $request)
    {
        
        set_time_limit ( 0 );
        ini_set('max_execution_time', 0);

        $formJSON = json_decode($request->get('form'));
        $form = construyeFormulario($formJSON);

        // dd($request->all(), $form);

        /**
         * Documentos que deben ser facturas, rendicion fondo fijo o boletas de servicio
         * Deben estar devengados
         * Deben estar en nómina de pago
         */
        $documentos = Documento::with(['getDevengos.getItemPresupuestario','getTipoDocumento','getDocumentosRelacionados'])
                                        // ->whereIn('id_tipo_documento', [1,2]) // Facturas
                                        // ->whereIn('id_tipo_documento', [1,2,6,7])
                                        ->where('id_estado', 2) // Devengado
                                        ->has('getNominaPagoDocumento'); // Que este en una nomina de pago

        if ( isset($form->filtro_tipo_documento_seleccion_documentos) ) {
            $documentos = $documentos->WhereIn('id_tipo_documento', $form->filtro_tipo_documento_seleccion_documentos);
        } else {
            $documentos = $documentos->whereIn('id_tipo_documento', [1,2,6,7]); // Facturas, rendicion fondo fijo, boletas de servicios
        }

        if ( $form->filtro_proveedor_seleccion_documentos != '' ) {
            $documentos = $documentos->where('id_proveedor', $form->filtro_proveedor_seleccion_documentos);
        }

        if ( $form->filtro_numero_documento_seleccion_documentos != '' ) {
            $documentos = $documentos->where('numero_documento', $form->filtro_numero_documento_seleccion_documentos);
        }

        // Falta realizar el filtro correctamente
        // if ( isset($form->filtro_cuenta_contable_seleccion_documentos) ) {
        //     $documentos = $documentos->WhereIn('id_tipo_documento', $form->filtro_cuenta_contable_seleccion_documentos);
        // }


        $documentos = $documentos->get();

        /**
         * Para documentos con 
         * - modalidad de compra: consumo basico y fondos fijos
         * - tipo de documento: boletas
         */
        $documentosDos = Documento::with(['getDevengos.getItemPresupuestario','getTipoDocumento','getDocumentosRelacionados'])
                                           ->whereIn('id_modalidad_compra', [4,5]) // consumo basico y fondos fijos
                                           ->where('id_estado', 2) // Devengado
                                           ->get();

        $documentosTres = Documento::with(['getDevengos.getItemPresupuestario','getTipoDocumento','getDocumentosRelacionados'])
                                        ->whereIn('id_tipo_documento', [3,9]) // Boletas
                                        ->where('id_estado', 2); // Devengado

        
        if ( $form->filtro_proveedor_seleccion_documentos != '' ) {
            $documentosTres = $documentosTres->where('id_proveedor', $form->filtro_proveedor_seleccion_documentos);
        }

        if ( $form->filtro_numero_documento_seleccion_documentos != '' ) {
            $documentosTres = $documentosTres->where('numero_documento', $form->filtro_numero_documento_seleccion_documentos);
        }
        
        $documentosTres = $documentosTres->get();
                                    
        $data = [];

        if ( ! isset($form->filtro_tipo_documento_seleccion_documentos) && !isset($form->filtro_cuenta_contable_seleccion_documentos) 
            && $form->filtro_proveedor_seleccion_documentos == '' && $form->filtro_numero_documento_seleccion_documentos == ''  ) {

            foreach ( $documentosDos as $doc ) {
                $data[] = $doc->getDatosDocumentoParaAgregarAlComprobanteContable();
            }

        }

        if ( ! isset($form->filtro_tipo_documento_seleccion_documentos) ) {

            foreach ( $documentosTres as $doc ) {
                $data[] = $doc->getDatosDocumentoParaAgregarAlComprobanteContable();
            }

        }

        foreach ( $documentos as $doc ) {
            $data[] = $doc->getDatosDocumentoParaAgregarAlComprobanteContable();
        }

        $respuesta = array(
            'data' => $data
        );

        return json_encode($respuesta);

    }

    public function postAgregarDocumentos(Request $request)
    {
        // dd($request->all());

         // Mensajes de los validadores
         $messages = [
            'required'    => 'Debe ingresar el  :attribute',
            'numeric'     => 'El :attribute debe solo contener números',
            'date_format' => 'La :attribute no tiene el formato correcto: día/mes/año',
            'cuenta_bancaria.required' => 'Debe seleccionar una cuenta bancaria.',
            'id.required' => 'Debe seleccionar al menos un Documento.',
        ];

        //validador de los input del formulario
        $validator = \Validator::make($request->all(), [
            'cuenta_bancaria' => 'required',
            'id'              => 'required',
            'idComprobante'   => 'required'
        ], $messages);

        //Si contiene errores se devuelven todos los errores, de lo contrario guarda en la base de datos
        if ( $validator->fails() ) {
            return response()->json($validator->errors(),400);
        } else {

            // Crear tabla intermedia por cada documento seleccionado

            $comprobanteContable = ComprobanteContable::findOrFail($request->get('idComprobante'));

            $sumaMontos = $comprobanteContable->monto_total;

            foreach ( $request->get('id') as $idDoc ) {
                
                $doc = Documento::findOrFail($idDoc);

                $comprobanteContableDocumento = new ComprobanteContableDocumento();
                $comprobanteContableDocumento->id_comprobante_contable = $comprobanteContable->id;
                $comprobanteContableDocumento->id_documento = $doc->id;
                $comprobanteContableDocumento->id_cuenta_bancaria = $request->get('cuenta_bancaria');
                $comprobanteContableDocumento->id_proveedor = $doc->id_proveedor;
                $comprobanteContableDocumento->monto_comprobante = $doc->total_documento_actualizado;
                $comprobanteContableDocumento->save();

                $doc->id_estado = 3;
                $doc->save();

                $sumaMontos = $sumaMontos + $doc->total_documento_actualizado;

            }

            $comprobanteContable->monto_total = $sumaMontos;
            $comprobanteContable->save();

            // para mostrar la cantidad de documentos y montos que quedan de las nominas
            $nominas = Documento::has('getNominaPagoDocumento')->where('id_estado', 2)->get();

            $datos = array(
                'estado'      => 'success',
                'mensaje'     => 'Todo correcto.',
                'sumaMontos'  => formatoMiles($sumaMontos),
                'docsNomina'  => formatoMiles($nominas->count()),
                'montoNomina' => formatoMiles($nominas->sum('total_documento_actualizado'))
            );

        }

        return response()->json($datos,200);
    }

    public function getDocumentosComprobante($idComprobante)
    {

        $comprobantesContablesDocumentos = ComprobanteContableDocumento::with([
                                                                               'getDocumento.getProveedorFactoring',
                                                                               'getDocumento.getTipoDocumento',
                                                                               'getCuentaContable'
                                                                            ])
                                                                       ->where('id_comprobante_contable', $idComprobante)
                                                                       ->get();
        
        $data = array();
        if ( $comprobantesContablesDocumentos->count() > 0 ) {

            foreach ( $comprobantesContablesDocumentos as $comprobanteContableDocumento ) {

                $data[] = $comprobanteContableDocumento->getDatosDocumentosDelComprobanteContable();
                
            }

        }

        $datos = array(
            "data" => $data
        );

        return json_encode($datos);
    }

    public function getModalQuitarDocumento($idDoc)
    {

        $doc = Documento::findOrFail($idDoc);
        return view('comprobante_contable.modal_quitar_documento')->with('doc', $doc);

    }

    public function postQuitarDocumento(Request $request)
    {
        // dd($request->all());

        // Mensajes de los validadores
        $messages = [
            'required'    => 'Debe ingresar el  :attribute',
            'numeric'     => 'El :attribute debe solo contener números',
        ];

        //validador de los input del formulario
        $validator = \Validator::make($request->all(), [
            '_id'       => 'required'
        ], $messages);

        //Si contiene errores se devuelven todos los errores, de lo contrario guarda en la base de datos
        if ( $validator->fails() ) {
            return response()->json($validator->errors(),400);
        } else {

            $doc = Documento::findOrFail($request->get('_id'));

            $comprobanteContableDocumento = ComprobanteContableDocumento::where('id_documento', $doc->id)->first();
            $comprobanteContable = ComprobanteContable::findOrFail($comprobanteContableDocumento->id_comprobante_contable);

            $comprobanteContableDocumento->delete();
            
            $comprobanteContable->monto_total = $comprobanteContable->monto_total - $doc->total_documento_actualizado;
            $comprobanteContable->save();

            $doc->id_estado = 2;
            $doc->save();

            // para mostrar la cantidad de documentos y montos que quedan de las nominas
            $nominas = Documento::has('getNominaPagoDocumento')->where('id_estado', 2)->get();

            $datos = array(
                'estado'      => 'success',
                'mensaje'     => 'Documento quitado del comprobante con éxito.',
                'sumaMontos'  => formatoMiles($comprobanteContable->monto_total),
                'docsNomina'  => formatoMiles($nominas->count()),
                'montoNomina' => formatoMiles($nominas->sum('total_documento_actualizado'))
            );

            return response()->json($datos,200);

        }

    }

    public function getModalEliminarComprobante($idComprobante)
    {

        $comprobante = ComprobanteContable::findOrFail($idComprobante);
        return view('comprobante_contable.modal_eliminar_comprobante')->with('comprobante', $comprobante);

    }

    public function postEliminarComprobante(Request $request)
    {
        // dd($request->all());

        $messages = [
            'required'    => 'Debe ingresar el  :attribute',
            'numeric'     => 'El :attribute debe solo contener números',
        ];

        //validador de los input del formulario
        $validator = \Validator::make($request->all(), [
            '_id'       => 'required'
        ], $messages);

        //Si contiene errores se devuelven todos los errores, de lo contrario guarda en la base de datos
        if ( $validator->fails() ) {
            return response()->json($validator->errors(),400);
        } else {

            $comprobanteContable = ComprobanteContable::with(['getComprobantesContablesDocumentos'])->findOrFail($request->get('_id'));
            // dd($comprobanteContable, $comprobanteContable->getComprobantesContablesDocumentos->count());

            if ( $comprobanteContable->getComprobantesContablesDocumentos->count() > 0 ) {
                foreach ( $comprobanteContable->getComprobantesContablesDocumentos as $comprobanteContableDocumento ) {

                    $comprobanteContableDocumento->getDocumento->id_estado = 2;
                    $comprobanteContableDocumento->getDocumento->save();
                    $comprobanteContableDocumento->delete();

                }
            }

            $comprobanteContable->delete();

            $datos = array(
                'estado'   => 'success',
                'mensaje'  => 'Comprobante Contable eliminado correctamente.'
            );

            return response()->json($datos,200);

        }
    }

    public function getCargaSigfe()
    {
        return view('comprobante_contable.carga_sigfe');
    }

    public function postCargaSigfe(Request $request)
    {
        // dd('Cargando info para crear comprobantes', $request->all());
        set_time_limit ( 0 );
        ini_set('max_execution_time', 0);
        
        $archivo = $request->file('archivo');
        // se selecciona la hoja que se quire utilziar del excel
        $excel = \Excel::selectSheetsByIndex(1)->load($archivo)->get();

        $numDocEncontrados = 0;
        $datosTablaComprobantesCreados = array();

        $numComprobanteEncontrado = 0;
        $numRelacionesCreadas = 0;
        $numComprobanteCreado = 0;
        $numDocNoEncontrados = 0;
        $datosTablaNoEncontrados = array();
        $numReversasCreadas = 0;
        $numComprobantesReversados = 0;
        $montoTotalNoIngresado = 0;

        foreach ($excel as $key => $fila) {
            $montoSigfe = 0;

            if ( $fila->debe != null ) {
                try{
                    $montoSigfe = (int) formato_entero( str_replace(['(',')'],'', $fila->debe) );
                } catch ( \ErrorException $e) {
                    dd($e,$montoSigfe,$fila,'errors');
                }
                
            }

            // Variables utilizadas para mostrar en la tabla
            $estadoDoc = '';
            $totalDoc = '$ ';
            $totalDocAct = '$ ';
            $id_tipo_documento = 0;

            switch ( trim($fila->tipo_documento) ) {
                case 'Rendición de cuentas':
                    $id_tipo_documento = 6; // Rendicion de fondo fijo
                    break;

                case 'Boleta de Honorarios':
                    $id_tipo_documento = 3; // Boleta de Honorarios Afecta
                    break;

                case 'Factura Exenta':
                    $id_tipo_documento = 1; // Factura Electronica
                    break;

                case 'Factura Exenta Electrónica':
                    $id_tipo_documento = 1; // Factura Electronica
                    break;

                case 'Factura Afecta Electrónica':
                    $id_tipo_documento = 1; // Factura Electronica
                    break;

                case 'Factura Afecta':
                        $id_tipo_documento = 1; // Factura Electronica
                        break;

                case 'Nota de Credito':
                    $id_tipo_documento = 4; // Nota de Credito
                    break;

                case 'Nota de Debito':
                    $id_tipo_documento = 5; // Nota de Debito
                    break;

                case 'Documento de Negocio':
                    $id_tipo_documento = 10000;
                    break;

                default:
                    $id_tipo_documento = 0;
                    break;
            }
             
            // NC y ND no tienen comprobante (en este caso)

            // Buscar Documento para pasar los datos del sigfe
            $doc = Documento::with(['getRelacionRecepcionesValidadas.getWsDocumento.getWsItemsPresupuestarios',
                                    'getDevengos'
                                ])
                            ->where('numero_documento', formato_entero( trim($fila->numero_documento) ))
                            ->whereHas('getProveedor', function ($query) use ($fila) {
                                $query->where('rut', explode(' ', trim($fila->principal))[0]);
                            });

            if ( $id_tipo_documento != 10000 ) {
                $doc = $doc->where('id_tipo_documento', $id_tipo_documento);
            } else {
                $doc = $doc->whereNotIn('id_tipo_documento', [4,5]);
            }

            $doc = $doc->first();

            /**
             * Primero se comprueba si el comprobante es un ajuste
             * De no serlo, se realizan los filtros necesarios para establecer si 
             * es un egreso o una reversa.
             */

            
            if ( strpos( $fila->titulo, 'AJUSTE' ) !== false || strpos( $fila->titulo, 'Ajuste' ) !== false || strpos( $fila->titulo, 'ajuste' ) !== false ) {
                // en este caso el monto sigfe puede ser 0, porque el debe puede ser 0, o venir un monto.

                if( is_object($doc) ) {
                    
                    if ( $fila->debe != null && $fila->debe != 0 ) {
                        try{
                            $montoSigfe = (int) formato_entero( str_replace(['(',')'],'', $fila->debe) );
                        } catch ( \ErrorException $e) {
                            dd($e,$montoSigfe,$fila,'errors');
                        }
                        
                    }

                    $comprobante = ComprobanteContable::where('folio', trim($fila->folio) )
                                                      ->where('año_proceso', fecha_Y( trim($fila->fecha) ) )
                                                      ->first();

                    $this->evaluarComprobante(
                        'Ingreso', $comprobante, $numComprobanteEncontrado, $doc, 
                        $fila, $numComprobanteCreado, $numRelacionesCreadas, $montoSigfe
                    );

                } else {

                    $this->documentoNoEncontrado($numDocNoEncontrados, $estadoDoc, $totalDoc, $fila, $id_tipo_documento, $key, $datosTablaNoEncontrados);
                    $montoTotalNoIngresado -= $montoSigfe;

                } // else is_object($doc)

            } else {

                if ( $fila->debe != null && $montoSigfe != 0 ) {
                
                    // if ( $id_tipo_documento == 10000 ) {
                    //     dd($id_tipo_documento, $fila, $doc);
                    // }

                    if( is_object($doc) ) {

                        $numDocEncontrados++;

                        // verificar si es un comprobante o una reversa
                        
                        // if ( strpos( $fila->debe, '(' ) !== false && $doc->id_tipo_documento != 3 ) {
                        if ( strpos( $fila->debe, '(' ) !== false ) {

                            // Esta bien

                            $reversa = ReversaComprobante::where('folio_sigfe', $fila->folio)
                                                         ->where('id_documento', $doc->id)
                                                         ->where('year_sigfe', fecha_Y( trim($fila->fecha) ))
                                                         ->first();

                            if ( ! is_object($reversa) ) {

                                // Crear la reversa y cambiar estado del comprobante, si existe
                                $reversa = new ReversaComprobante;
                                /**
                                 * no se guarda el id cuenta contable, se debe guardar en la segunda 
                                 * carga de excel, para completar la informacion
                                 */
                                $reversa->id_documento = $doc->id;
                                $reversa->folio_sigfe = $fila->folio;
                                $reversa->fecha_sigfe = fecha_Y_m_d($fila->fecha);
                                $reversa->dia_sigfe = fecha_D($fila->fecha);
                                $reversa->mes_sigfe = fecha_M($fila->fecha);
                                $reversa->year_sigfe = fecha_Y($fila->fecha);
                                $reversa->detalle_sigfe = $fila->titulo;
                                $reversa->monto = $montoSigfe;
                                $reversa->save();

                                $numReversasCreadas++;

                                foreach ( $doc->getComprobanteContableDocumento->sortByDesc('id') as $comprobanteContableDocumento ) {

                                    if (    $comprobanteContableDocumento->getComprobanteContable->id_tipo_comprobante_contable == 1 && 
                                            $comprobanteContableDocumento->getComprobanteContable->folio < $fila->folio && 
                                            $comprobanteContableDocumento->getComprobanteContable->reversado == 0 &&
                                            $reversa->id_comprobante_contable == null &&
                                            $reversa->id_comprobante_contable_documento == null
                                        ) {

                                        $reversa->id_comprobante_contable = $comprobanteContableDocumento->id_comprobante_contable;
                                        $reversa->id_comprobante_contable_documento = $comprobanteContableDocumento->id;
                                        $reversa->save();

                                        $comprobanteContableDocumento->getComprobanteContable->monto_total -= $reversa->monto;
                                        $comprobanteContableDocumento->getComprobanteContable->reversado = ( $comprobanteContableDocumento->getComprobanteContable->monto_total <= 0 ) ? 1 : 0;
                                        $comprobanteContableDocumento->getComprobanteContable->save();

                                        $comprobanteContableDocumento->reversado = 1;
                                        $comprobanteContableDocumento->save();

                                        break;

                                    }

                                }

                                /**
                                 * El documento deja de estar pagado cuando la ultima reversa tiene mayor folio
                                 * que el ultimo comprobante contable
                                 */
                                if ( 
                                        $doc->getComprobanteContableDocumento->count() > 0 && $doc->getReversaComprobante->count() > 0 && 
                                        $doc->getComprobanteContableDocumento->last()->getComprobanteContable->id_tipo_comprobante_contable == 1 &&
                                        $doc->getComprobanteContableDocumento->last()->getComprobanteContable->folio < $doc->getReversaComprobante->last()->folio_sigfe 
                                    ) {

                                    $doc->id_estado = 2; // Devengado y pendiente de pago
                                    $doc->save();

                                }
                                
                            } else {

                                if ( $reversa->id_comprobante_contable == null && $reversa->id_comprobante_contable_documento == null ) {

                                    foreach ( $doc->getComprobanteContableDocumento->sortByDesc('id') as $comprobanteContableDocumento ) {

                                        if (    $comprobanteContableDocumento->getComprobanteContable->id_tipo_comprobante_contable == 1 && 
                                                $comprobanteContableDocumento->getComprobanteContable->folio < $fila->folio && 
                                                $comprobanteContableDocumento->getComprobanteContable->reversado == 0  
                                            ) {

                                            $reversa->id_comprobante_contable = $comprobanteContableDocumento->id_comprobante_contable;
                                            $reversa->id_comprobante_contable_documento = $comprobanteContableDocumento->id;
                                            $reversa->save();

                                            $comprobanteContableDocumento->getComprobanteContable->monto_total -= $reversa->monto;
                                            $comprobanteContableDocumento->getComprobanteContable->reversado = ( $comprobanteContableDocumento->getComprobanteContable->monto_total <= 0 ) ? 1 : 0;
                                            $comprobanteContableDocumento->getComprobanteContable->save();

                                            $comprobanteContableDocumento->reversado = 1;
                                            $comprobanteContableDocumento->save();

                                            break;

                                        }

                                    }

                                    /**
                                     * El documento deja de estar pagado cuando la ultima reversa tiene mayor folio
                                     * que el ultimo comprobante contable
                                     */
                                    if ( 
                                            $doc->getComprobanteContableDocumento->count() > 0 && $doc->getReversaComprobante->count() > 0 && 
                                            $doc->getComprobanteContableDocumento->last()->getComprobanteContable->id_tipo_comprobante_contable == 1 &&
                                            $doc->getComprobanteContableDocumento->last()->getComprobanteContable->folio < $doc->getReversaComprobante->last()->folio_sigfe 
                                        ) {

                                        $doc->id_estado = 2; // Devengado y pendiente de pago
                                        $doc->save();

                                    }

                                }

                            }

                        } else {

                            $comprobante = ComprobanteContable::where('folio', trim($fila->folio) )
                                                              ->where('año_proceso', fecha_Y( trim($fila->fecha) ) )
                                                              ->first();
                                                            
                            // Se verifica si es boleta, para crear el comprobante de la tesoreria, que es del impuesto
                            if ( $doc->id_estado != 3 || $doc->id_tipo_documento == 3 ) {

                                $this->evaluarComprobante(
                                    'Egreso', $comprobante, $numComprobanteEncontrado, $doc, 
                                    $fila, $numComprobanteCreado, $numRelacionesCreadas, $montoSigfe
                                );

                            }

                        } // else ( strpos( $fila->debe, '(' ) !== false && $id_tipo_documento != 3 ) 

                    } else {

                        $this->documentoNoEncontrado($numDocNoEncontrados, $estadoDoc, $totalDoc, $fila, $id_tipo_documento, $key, $datosTablaNoEncontrados);
                        if ( strpos( $fila->debe, '(' ) !== false ) {
                            $montoTotalNoIngresado -= $montoSigfe;
                        } else {
                            $montoTotalNoIngresado += $montoSigfe;
                        }
                    } // else is_object($doc)

                } // If ( $fila->debe != null && $montoSigfe != 0 )

            } // else ¿Es ajuste?

        } // foreach ($excel as $key => $fila)

        $resumen = '<strong>';
        $resumen .= 'Documentos encontrados : '.formatoMiles($numDocEncontrados).'<br>';
        $resumen .= 'Comprobantes encontrados (1 por cada documento) : '.formatoMiles($numComprobanteEncontrado).'<br><br>';

        $resumen .= 'Documentos relacionados a comprobantes : '.formatoMiles($numRelacionesCreadas).'<br>';
        $resumen .= 'Comprobantes creados : '.formatoMiles($numComprobanteCreado).'<br><br>';

        $resumen .= 'Documentos no encontrados : '.formatoMiles($numDocNoEncontrados).'<br><br>';

        $resumen .= 'Reversar creadas : '.formatoMiles($numReversasCreadas).'<br>';
        $resumen .= 'Comprobantes reversados : '.formatoMiles($numComprobantesReversados).'<br><br>';

        $resumen .= 'Monto total efectivo no procesado : $'.formatoMiles($montoTotalNoIngresado).'<br>';

        $resumen .= '</strong>';

        $datos = array(
          'mensaje' => 'Carga Exitosa.',
          'estado'  => 'success',
          'resumen' => $resumen,
          'datosTablaNoEncontrados' => $datosTablaNoEncontrados
        );

        return response()->json($datos,200);
    }

    /**
     * Evalua como crear la conexion entre el comprobante contable
     * y el documento.
     * depende el tipo de documento y los montos que vienen del excel
     */
    public function conectaDocumentoConComprobante(Documento $doc,ComprobanteContable $comprobante,CuentaContable $cuentaContable, &$numRelacionesCreadas, $montoSigfe)
    {

        if ( $doc->id_tipo_documento != 3 ) {

            $comprobanteContableDocumento = new ComprobanteContableDocumento();
            $comprobanteContableDocumento->id_comprobante_contable = $comprobante->id;
            $comprobanteContableDocumento->id_documento = $doc->id;
            $comprobanteContableDocumento->id_proveedor = $doc->id_proveedor;
            $comprobanteContableDocumento->id_cuenta_contable = $cuentaContable->id;        
            $comprobanteContableDocumento->monto_comprobante = $montoSigfe;
            $comprobanteContableDocumento->carga_sigfe = 1;
            $comprobanteContableDocumento->save();

            $comprobante->monto_total = $comprobante->monto_total + $comprobanteContableDocumento->monto_comprobante;
            $comprobante->save();

            if ( $comprobante->id_tipo_comprobante_contable == 1 && $doc->id_estado != 3 ) {
                $doc->id_estado = 3;
                $doc->save();
            }

            $numRelacionesCreadas++;

        } else {

            if ( $montoSigfe == $doc->impuesto ) {

                // El comprobante es para la tesoreria de la republica
                $provTesoreria = Proveedor::where('rut', '60805000-0')->first();

                if ( ! is_object($provTesoreria) ) {
                    $provTesoreria = new Proveedor;
                    $provTesoreria->rut = '60805000-0';
                    $provTesoreria->nombre = 'Tesoreria General de la Republica';
                    $provTesoreria->activo = 1;
                    $provTesoreria->save();
                }

                $comprobanteContableDocumento = new ComprobanteContableDocumento();
                $comprobanteContableDocumento->id_comprobante_contable = $comprobante->id;
                $comprobanteContableDocumento->id_documento = $doc->id;
                $comprobanteContableDocumento->id_proveedor = $provTesoreria->id;
                $comprobanteContableDocumento->id_cuenta_contable = $cuentaContable->id;                
                $comprobanteContableDocumento->monto_comprobante = $montoSigfe;
                $comprobanteContableDocumento->carga_sigfe = 1;
                $comprobanteContableDocumento->save();

                $comprobante->monto_total = $comprobante->monto_total + $comprobanteContableDocumento->monto_comprobante;
                $comprobante->save();

                if ( $comprobante->id_tipo_comprobante_contable == 1 && $doc->id_estado != 3 ) {
                    $doc->id_estado = 3;
                    $doc->save();
                }

                $numRelacionesCreadas++;

            } elseif ( $montoSigfe == $doc->liquido ) {

                // El comprobante es del proveedor
                $comprobanteContableDocumento = new ComprobanteContableDocumento();
                $comprobanteContableDocumento->id_comprobante_contable = $comprobante->id;
                $comprobanteContableDocumento->id_documento = $doc->id;
                $comprobanteContableDocumento->id_proveedor = $doc->id_proveedor;
                $comprobanteContableDocumento->id_cuenta_contable = $cuentaContable->id;
                $comprobanteContableDocumento->monto_comprobante = $montoSigfe;
                $comprobanteContableDocumento->carga_sigfe = 1;
                $comprobanteContableDocumento->save();

                $comprobante->monto_total = $comprobante->monto_total + $comprobanteContableDocumento->monto_comprobante;
                $comprobante->save();

                if ( $comprobante->id_tipo_comprobante_contable == 1 && $doc->id_estado != 3 ) {
                    $doc->id_estado = 3;
                    $doc->save();
                }

                $numRelacionesCreadas++;

            } elseif ( $montoSigfe == $doc->total_documento ) {
                // La boleta no es afecta, se paga el total de la boleta

                // El comprobante es del proveedor
                $comprobanteContableDocumento = new ComprobanteContableDocumento();
                $comprobanteContableDocumento->id_comprobante_contable = $comprobante->id;
                $comprobanteContableDocumento->id_documento = $doc->id;
                $comprobanteContableDocumento->id_proveedor = $doc->id_proveedor;
                $comprobanteContableDocumento->id_cuenta_contable = $cuentaContable->id;
                $comprobanteContableDocumento->monto_comprobante = $montoSigfe;
                $comprobanteContableDocumento->carga_sigfe = 1;
                $comprobanteContableDocumento->save();

                $comprobante->monto_total = $comprobante->monto_total + $comprobanteContableDocumento->monto_comprobante;
                $comprobante->save();

                if ( $comprobante->id_tipo_comprobante_contable == 1 && $doc->id_estado != 3 ) {
                    $doc->id_estado = 3;
                    $doc->save();
                }

                $numRelacionesCreadas++;

            }

        } // else ( $id_tipo_documento != 3 )
        
        // Fin Funcion
    }

    /**
     * Funcion para llegar la visualización del documento no encontrado en la carga de sigfe
     */
    public function documentoNoEncontrado(&$numDocNoEncontrados, &$estadoDoc, &$totalDoc, $fila, $id_tipo_documento, $key, &$datosTablaNoEncontrados) {
        // No se encuentra el documento, mostrar en tabla
        $numDocNoEncontrados++;

        $estadoDoc .= 'No Encontrado';

        try{
            // $montoSigfe = (int) str_replace(['(',')'],'', $fila->haber);
            $totalDoc .= trim( str_replace(['(',')'],'', $fila->debe) );
        } catch ( \ErrorException $e) {
            dd($e,$montoSigfe,$fila,'errors');
        }

        // $totalDoc .= formatoMiles( formato_entero( trim($fila->haber) ) );
        
        $nombreProveedor = '';
        $auxProv = explode(' ', trim($fila->principal) );
        for ( $i = 1; $i < count($auxProv); $i++ ) {
            $nombreProveedor .= $auxProv[$i].' ';
        }

        // Para realizar la carga del documento al sistema
        $proveedor = Proveedor::where('rut',explode(' ',trim($fila->principal))[0])->first();

        $idProveedor = 0;
        if ( is_object($proveedor) ) {
            $idProveedor = $proveedor->id;
        }

        $numeroDocumento = formato_entero( trim($fila->numero_documento) );
        $totalDocumento = formato_entero( trim($fila->debe));
        $idSigfe = trim($fila->folio);
        $fechaSigfe = fecha_Y_m_d(trim($fila->fecha));
        // $fechaSigfe = trim($fila->fecha);

        $botonCarga = '<div class="btn-group">';

        if ( $id_tipo_documento != 0 && $id_tipo_documento != 10000 && $idProveedor != 0 ) {

            $botonCarga .= '<button type="button" class="btn btn-primary btn-xs" title="Ingresar Documento" ';
            $botonCarga .= 'onclick="carga('.$idProveedor.','.$id_tipo_documento.','.$numeroDocumento.','.$totalDocumento.','.$idSigfe.',\''.$fechaSigfe.'\','.$key.')" >';
            $botonCarga .= '<i class="fas fa-file-export fa-lg"></i>&nbsp;<i class="fas fa-file-invoice fa-lg"></i></button>';

        }

        $botonCarga .= '</div>';
        
        $docJson = array(
            'DT_RowID' => $key,
            $botonCarga,
            $estadoDoc,
            trim(explode(' ',$fila->principal)[0]),
            $nombreProveedor,
            trim($fila->tipo_documento),
            trim($fila->numero_documento),
            $fila->debe,
            trim($fila->folio),
            trim($fila->fecha)
        );

        $datosTablaNoEncontrados[] = $docJson;

    }

    /**
     * funcion que evalua si el comprobante existe o no.
     * Adjunta el documento al comprobante.
     */
    public function evaluarComprobante($tipoComprobante, $comprobante, &$numComprobanteEncontrado, Documento $doc, $fila, &$numComprobanteCreado, &$numRelacionesCreadas, $montoSigfe)
    {

        if ( is_object($comprobante) ) {

            $numComprobanteEncontrado++;

            // ver si el documento ya esta asociado al comprobante, si no lo esta, asociarlo
            $comprobanteContableDocumento = ComprobanteContableDocumento::where('id_comprobante_contable', $comprobante->id)
                                                                        ->where('id_documento', $doc->id)
                                                                        ->first();
                                                            
            if ( ! is_object($comprobanteContableDocumento) ) {

                $cuentaContable = CuentaContable::where('codigo', preg_replace('/[^0-9]/', '', $fila->cuenta_contable) )->first();

                if ( ! is_object($cuentaContable) ) {

                    $cuentaContable = new CuentaContable;
                    $cuentaContable->codigo = preg_replace('/[^0-9]/', '', $fila->cuenta_contable);
                    $cuentaContable->glosa = preg_replace('/[0-9]+/', '', $fila->cuenta_contable);
                    $cuentaContable->save();

                }

                $this->conectaDocumentoConComprobante($doc, $comprobante, $cuentaContable, $numRelacionesCreadas, $montoSigfe);
            
            } // if ( ! is_object($comprobanteContableDocumento) )

        } else {

            // Crear comprobante y asociar el documento

            $cuentaContable = CuentaContable::where('codigo', preg_replace('/[^0-9]/', '', $fila->cuenta_contable) )->first();

            if ( ! is_object($cuentaContable) ) {

                $cuentaContable = new CuentaContable;
                $cuentaContable->codigo = preg_replace('/[^0-9]/', '', $fila->cuenta_contable);
                $cuentaContable->glosa = preg_replace('/[0-9]+/', '', $fila->cuenta_contable);
                $cuentaContable->save();

            }

            $comprobante = new ComprobanteContable();
            if ( $tipoComprobante == 'Egreso' ) {
                $comprobante->id_tipo_comprobante_contable = 1; // Egreso
            }

            if ( $tipoComprobante == 'Ingreso' ) {
                $comprobante->id_tipo_comprobante_contable = 2; // Ingreso
            }
            
            $comprobante->fecha_proceso = fecha_Y_m_d( trim($fila->fecha) );
            $comprobante->dia_proceso = fecha_D( trim($fila->fecha) );
            $comprobante->mes_proceso = fecha_M( trim($fila->fecha) );
            $comprobante->año_proceso = fecha_Y( trim($fila->fecha) );
            $comprobante->folio = trim($fila->folio);
            $comprobante->id_user_created = \Auth::user()->id;
            $comprobante->setNumeroComprobante();
            $comprobante->carga_sigfe = 1;
            $comprobante->save();

            $numComprobanteCreado++;

            $this->conectaDocumentoConComprobante($doc, $comprobante, $cuentaContable, $numRelacionesCreadas, $montoSigfe);

        } // else is_object($comprobanteContable)

    }

    public function getCargaPagosSigfe()
    {
        return view('comprobante_contable.carga_pagos_sigfe');
    }

    public function postCargaPagosSigfe(Request $request)
    {
        // dd('carga de pagos');

        set_time_limit ( 0 );
        ini_set('max_execution_time', 0);
        
        $archivo = $request->file('archivo');
        // se selecciona la hoja que se quire utilziar del excel
        $excel = \Excel::selectSheetsByIndex(0)->load($archivo)->get();

        $numDocEncontrados = 0;
        $datosTablaComprobantesCreados = array();

        $numComprobanteEncontrado = 0;
        $numDocNoEncontrados = 0;
        $datosTablaNoEncontrados = array();
        $numComprobanteNoEncontrado = 0;
        $numDocumentoNoAsociadoAlComprobante = 0;
        $actualizacionInformacionPago = 0;
        $reversaEncontrada = 0;
        $reversaNoEncontrada = 0;
        $numDocumentoNoAsociadoReversa = 0;

        foreach ( $excel as $key => $fila ) {
            
            // $fila->fecha_generacion = str_replace('-','.', trim($fila->fecha_generacion));
            // dd($fila);

            // Buscar el documento.
            $tipoDocumento = trim( preg_replace('/[0-9]+/', '', $fila->tipo_documento) );
            $estadoDoc = '';
            $totalDoc = '$ ';
            $id_tipo_documento = 0;

            switch ( trim($tipoDocumento) ) {
                case 'Rendición de cuentas':
                    $id_tipo_documento = 6; // Rendicion de fondo fijo
                    break;

                case 'Boleta de Honorarios':
                    $id_tipo_documento = 3; // Boleta de Honorarios Afecta
                    break;

                case 'Factura Exenta':
                    $id_tipo_documento = 1; // Factura Electronica
                    break;

                case 'Factura Exenta Electrónica':
                    $id_tipo_documento = 1; // Factura Electronica
                    break;

                case 'Factura Afecta Electrónica':
                    $id_tipo_documento = 1; // Factura Electronica
                    break;

                case 'Factura Afecta':
                    $id_tipo_documento = 1; // Factura Electronica
                    break;

                case 'Nota de Credito':
                    $id_tipo_documento = 4; // Nota de Credito
                    break;

                case 'Nota de Debito':
                    $id_tipo_documento = 5; // Nota de Debito
                    break;

                case 'Documento de Negocio':
                    $id_tipo_documento = 10000;
                    break;

                default:
                    $id_tipo_documento = 0;
                    break;
            }

            $doc = Documento::with(['getRelacionRecepcionesValidadas.getWsDocumento.getWsItemsPresupuestarios',
                                    'getDevengos'
                                ])
                            ->where('numero_documento', formato_entero( trim($fila->nro_documento) ))
                            ->whereHas('getProveedor', function ($query) use ($fila) {
                                $query->where('rut', explode(' ', trim($fila->principal))[0]);
                            });

            if ( $id_tipo_documento != 10000 ) {
                $doc = $doc->where('id_tipo_documento', $id_tipo_documento);
            } else {
                $doc = $doc->whereNotIn('id_tipo_documento', [4,5]);
            }

            $doc = $doc->first();

            if ( ! is_object($doc) ) {
                
                $this->documentoNoEncontrado($numDocNoEncontrados, $estadoDoc, $totalDoc, $fila, $id_tipo_documento, $key, $datosTablaNoEncontrados);

            } else { // if ( ! is_object($doc) )
                
                $numDocEncontrados++;

                // Buscar el comprobante por el folio

                $comprobante = ComprobanteContable::where('folio', trim($fila->folio) )
                                                  ->where('año_proceso', fecha_Y( trim($fila->fecha_generacion) ) )
                                                  ->first();

                if ( ! is_object($comprobante) ) {

                    $numComprobanteNoEncontrado++;

                    // Buscar reversa
                    $reversa = ReversaComprobante::where('folio_sigfe', trim($fila->folio))
                                                 ->where('year_sigfe', fecha_Y( trim($fila->fecha_generacion) ))
                                                 ->first();

                    if ( ! is_object($reversa) ) {
                        
                        $reversaNoEncontrada++;

                    } else {

                        $reversaEncontrada++;

                        if ( $reversa->id_documento != $doc->id ) {

                            $numDocumentoNoAsociadoReversa++;

                        } else {

                            $cuentaContable = CuentaContable::where('codigo', preg_replace('/[^0-9]/', '', $fila->cuenta_contable) )->first();

                            if ( ! is_object($cuentaContable) ) {

                                $cuentaContable = new CuentaContable;
                                $cuentaContable->codigo = preg_replace('/[^0-9]/', '', $fila->cuenta_contable);
                                $cuentaContable->save();

                            }

                            $reversa->id_cuenta_contable = $cuentaContable->id;
                            $reversa->save();

                        }

                    }

                } else { // if ! is_object($comprobante)

                    $numComprobanteEncontrado++;

                    // verificar si el documento esta relacionado al folio del comprobante o de reversa
                    $comprobanteContableDocumento = ComprobanteContableDocumento::where('id_comprobante_contable', $comprobante->id)
                                                                                ->where('id_documento', $doc->id)
                                                                                ->first();

                    if ( ! is_object($comprobanteContableDocumento) ) {

                        $numDocumentoNoAsociadoAlComprobante++;

                    } else { // if ( ! is_object($comprobanteContableDocumento) )

                        // cambia la cuenta por la que corresponde y agregar los datos del pago

                        $cuentaContable = CuentaContable::where('codigo', preg_replace('/[^0-9]/', '', $fila->cuenta_contable) )->first();

                        if ( ! is_object($cuentaContable) ) {

                            $cuentaContable = new CuentaContable;
                            $cuentaContable->codigo = preg_replace('/[^0-9]/', '', $fila->cuenta_contable);
                            $cuentaContable->save();

                        }

                        $comprobanteContableDocumento->id_cuenta_contable = $cuentaContable->id;

                        if ( $fila->banco_cta_corriente != null && $fila->banco_cta_corriente != '' ) {

                            // Añadir la cuenta bancaria.
                            $banco = Banco::where('nombre', trim( preg_replace('/[0-9]+/', '', $fila->banco_cta_corriente) ) )->first();
                            if ( ! is_object($banco) ) {

                                $banco = new Banco;
                                $banco->nombre = trim( preg_replace('/[0-9]+/', '', $fila->banco_cta_corriente) );
                                $banco->save();

                            }

                            $cuentaBancaria = CuentaBancaria::where('id_banco', $banco->id)
                                                            ->where('codigo', trim( preg_replace('/[^0-9]/', '', $fila->banco_cta_corriente) ) )
                                                            ->first();

                            if ( ! is_object($cuentaBancaria) ) {

                                $cuentaBancaria = new CuentaBancaria;
                                $cuentaBancaria->id_banco = $banco->id;
                                $cuentaBancaria->codigo = preg_replace('/[^0-9]/', '', $fila->banco_cta_corriente);
                                $cuentaBancaria->save();

                            }

                             $comprobanteContableDocumento->id_cuenta_bancaria = $cuentaBancaria->id;

                         }

                        /**
                        * falta agregar:
                        * medio de pago : integer (mantenedor)
                        * tipo de medio de pago: se utiliza para identificar cheques
                        * numero documento de pago: integer
                        * fecha de emision de pago: date time, viene fecha y hora
                        * todo nullable
                        */

                        if ( $fila->medio_pago != null && $fila->medio_pago != '' ) {

                            if ( strpos( trim($fila->tipo_medio_pago), '13' ) !== false ) {
                                // Cheque

                                $medioPago = MedioPago::where('nombre', 'Cheque' )->first();
                                if ( ! is_object($medioPago) ) {
    
                                    $medioPago = new MedioPago;
                                    $medioPago->nombre = 'Cheque';
                                    $medioPago->save();
    
                                }

                            } else {

                                $medioPago = MedioPago::where('nombre', trim($fila->medio_pago) )->first();
                                if ( ! is_object($medioPago) ) {
    
                                    $medioPago = new MedioPago;
                                    $medioPago->nombre = trim($fila->medio_pago);
                                    $medioPago->save();
    
                                }

                            }

                            $comprobanteContableDocumento->id_medio_pago = $medioPago->id;

                        }

                        if ( $fila->nro_documento_pago != null && $fila->nro_documento_pago != '' && is_numeric($fila->nro_documento_pago) ) {

                            $comprobanteContableDocumento->numero_documento_pago = $fila->nro_documento_pago;

                        }

                        // Validar si es fecha. Finalmente no se valida, ya que si viene dato, siempre es fecha
                        if ( $fila->fecha_emision != null && $fila->fecha_emision != '' ) {

                            $comprobanteContableDocumento->fecha_emision_pago = $fila->fecha_emision;

                        }

                        $comprobanteContableDocumento->save();
                        $actualizacionInformacionPago++;

                    }

                } // else ! is_object($comprobante)

            } // else ( ! is_object($doc) )

        } // foreach ( $excel as $key => $fila )

        $resumen = '<strong>';
        $resumen .= 'Documentos encontrados : '.formatoMiles($numDocEncontrados).'<br>';
        $resumen .= 'Comprobantes encontrados : '.formatoMiles($numComprobanteEncontrado).'<br>';
        $resumen .= 'Reversas encontradas : '.formatoMiles($reversaEncontrada).'<br><br>';

        $resumen .= 'Actualización de información de pago : '.formatoMiles($actualizacionInformacionPago).'<br><br>';
        // $resumen .= 'Comprobantes creados : '.formatoMiles($numComprobanteCreado).'<br><br>';

        $resumen .= 'Documentos no encontrados : '.formatoMiles($numDocNoEncontrados).'<br>';
        $resumen .= 'Comprobantes no encontrados : '.formatoMiles($numComprobanteNoEncontrado).'<br>';
        $resumen .= 'Reversas no encontradas : '.formatoMiles($reversaNoEncontrada).'<br>';
        // $resumen .= 'Documentos no asociados al comprobante que corresponde : '.formatoMiles($numDocumentoNoAsociadoAlComprobante).'<br>';

        $resumen .= '</strong>';

        $datos = array(
          'mensaje' => 'Carga Exitosa.',
          'estado'  => 'success',
          'resumen' => $resumen,
          'datosTablaNoEncontrados' => $datosTablaNoEncontrados
        );

        return response()->json($datos,200);

    }

    public function getModalGenerarPDF($id) {

        $comprobante = ComprobanteContable::findOrFail($id);

        $usuarios = User::where('responsable', 1)->get();

        /**
         * Obtener usuarios responsables de revisión
         */
        $usuariosRevision = User::whereIn('id',[34,8,14,7,3])->get();

        return view('comprobante_contable.modal_generar_pdf')
                ->with('comprobante', $comprobante)
                ->with('usuarios', $usuarios)
                ->with('usuariosRevision', $usuariosRevision);
    }

    public function postGenerarPDFComprobante(Request $request)
    {
        $comprobante = ComprobanteContable::findOrFail($request->get('_id'));
        $primerUsuario = User::findOrFail($request->get('primer_usuario_autoriza'));
        $segundoUsuario = User::findOrFail($request->get('segundo_usuario_autoriza'));
        if ( $request->input('usuario_revision') ) {
            $usuarioRevision = User::findOrFail($request->get('usuario_revision'));
        } else {
            $usuarioRevision = null;
        }

        $content = \View::make('pdf.comprobante_contable', compact('comprobante','primerUsuario','segundoUsuario', 'usuarioRevision'))->render();
        $mpdf = new Mpdf([
            // 'mode' => 'c',
            // 'format'=>'Letter-P',
            'format'=>'Legal',
            'mirrorMargins' => true,
            'tempDir' => public_path() . '/tempMpdf'
            ]);

        //$mpdf = new Mpdf(['mode'=>'c']);
        $mpdf->SetDisplayMode('fullpage');
        $mpdf->keep_table_proportions = true;
        $mpdf->showImageErrors = true;
        $content = mb_convert_encoding($content, 'UTF-8', 'UTF-8');
        $mpdf->WriteHTML($content);
        // $mpdf->setFooter('<div style="border-top: 1px solid #CCCCCC;" ><div style="text-align: left; font-family: Arial, Helvetica,sans-serif; font-weight: bold;font-size: 7pt; padding-top: 4px;display: inline-block;float: left;width: 70%;">MEMORANDUM N° '.$documento->numero_documento.' del </div><div style="text-align: right; font-family: Arial, Helvetica,sans-serif; font-weight: bold;font-size: 7pt; padding-top: 4px;display: inline-block;float: right;width: 30%;">Pag.  {PAGENO} / {nbpg}</div></div>');
        $mpdf->Output("Comprobante de Liquidación de Fondos - DEPARTAMENTO FINANZAS - HSJD.pdf",'I');
    }

}
