<?php namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests;
use App\Helpers\JwtAuth;

use Auth;
use App\User;
use App\Role;
use App\Permission;

class UserController extends Controller
{

	/**
     * Create a new controller instance.
     *
     * @return void
    */
    public function __construct()
    {
        $this->middleware('auth');
	}
	
	public function index()
    {
        if (!\Entrust::can(['crear-user','editar-user','eliminar-user','ver-user','ver-general-user'])) {
            return \Redirect::to('home');
        }

        $usuarios = User::with('roles')->get();
        return view('usuario.index')->with('usuarios',$usuarios);
	}
	
	public function getModalVer($id)
	{
		$usuario = User::with('roles')->findOrFail($id);
		return view('usuario.modal_ver_usuario')->with('usuario',$usuario);
	}

	public function getModalEditar($id)
	{
        $usuario = User::with('roles')->findOrFail($id);
        $perfiles = Role::all();
		return view('usuario.modal_editar_usuario')->with('usuario',$usuario)->with('perfiles',$perfiles);
	}

	public function postEditar(Request $request)
	{
		// dd('wololo',$request->all());
		// Mensajes del validador
        $messages = [
            'required' => 'Debe ingresar el :attribute',
            'rut.required' => 'Debe ingresar el Rut',
            'nombre.required' => 'Debe ingresar el Nombre',
            'numeric' => 'El :attribute debe solo contener números',
            'max' => 'El :attribute no debe exeder los :max caracteres',
            'min' => 'El :attribute debe tener minimo :min caracteres',
            'unique' => 'El valor :attribute ya existe en la base de datos',
            'email' => 'Debe ingresar un E-mail valido',
            'cl_rut' => 'El Rut es incorrecto',
        ];

        // Reglas del validador
        $rules = [
            'rut' => 'required|unique:users,rut,'.$request->input("_id").',id,deleted_at,NULL|cl_rut',
            'nombre' => 'required',
            'perfil' => 'required',
            //'email'  => 'email',
        ];

        if ( $request->input('email') != null ) {
            $rules['email'] = 'email';
        }

        // Validador de los input del formulario
        $validator = \Validator::make($request->all(), $rules, $messages);

        //Si contiene errores se devuelven todos los errores, de lo contrario guarda en la base de datos
        if ( $validator->fails() ) {
            return response()->json($validator->errors(),400);
        } else {
			$editUser = User::findOrFail($request->input('_id'));
			$editUser->editUser($request);
            $perfil = Role::findOrFail($request->input('perfil'));

			$datos = array(
                'mensaje' => 'Edición exitosa del usuario',
				'estado' => 'success',
				'nombre' => $editUser->name,
				'rut' => $editUser->formatRut(),
				'iniciales' => $editUser->iniciales,
                'email' => $editUser->email,
                'suspendido' => $editUser->suspendido(),
                'responsable' => $editUser->responsable(),
                'id' => $editUser->id,
                'perfil' => $perfil->display_name,
            );
		}

		return response()->json($datos,200);
	}

	public function create()
	{
        $perfiles = Role::all();
		return view('usuario.modal_crear_usuario')->with('perfiles',$perfiles);
	}

	public function store(Request $request)
	{

		// Mensajes del validador
        $messages = [
            'required' => 'Debe ingresar el :attribute',
            'rut.required' => 'Debe ingresar el Rut',
            'nombre.required' => 'Debe ingresar el Nombre',
            'numeric' => 'El :attribute debe solo contener números',
            'max' => 'El :attribute no debe exeder los :max caracteres',
            'min' => 'El :attribute debe tener minimo :min caracteres',
            'unique' => 'El valor :attribute ya existe en la base de datos',
            'email' => 'Debe ingresar un E-mail valido',
            'cl_rut' => 'El Rut es incorrecto',
        ];

        // Reglas del validador
        $rules = [
            'rut' => 'required|unique:users,rut,NULL,NULL,deleted_at,NULL|cl_rut',
            'nombre' => 'required',
            'perfil' => 'required',
            //'email'  => 'email',
        ];

        if ( $request->input('email') != null ) {
            $rules['email'] = 'email';
        }

        // Validador de los input del formulario
        $validator = \Validator::make($request->all(), $rules, $messages);

        //Si contiene errores se devuelven todos los errores, de lo contrario guarda en la base de datos
        if ( $validator->fails() ) {
            return response()->json($validator->errors(),400);
        } else {
			// Crear el nuevo usuario
            try {
                $newUsuario = new User($request);
            } catch (QueryException $e) {
                $datos = array(
                    'estado' => 'error',
                    'mensaje' => 'Existen problemas al momento de guardar el Usuario',
                );
                return response()->json($datos,200);
            }
            
			$perfil = Role::findOrFail($request->input('perfil'));
			$datos = array(
                'mensaje' => 'Se ha creado el usuario',
				'estado' => 'success',
				'nombre' => $newUsuario->name,
				'rut' => $newUsuario->formatRut(),
				'iniciales' => $newUsuario->iniciales,
                'email' => $newUsuario->email,
                'suspendido' => $newUsuario->suspendido(),
                'responsable' => $newUsuario->responsable(),
                'id' => $newUsuario->id,
                'perfil' => $perfil->display_name,
            );
		}
		return response()->json($datos,200);
	}

	public function getModalEliminar($id)
	{
		$usuario = User::with('roles')->findOrFail($id);
		return view('usuario.modal_eliminar_usuario')->with('usuario',$usuario);
	}

	public function postEliminar(Request $request)
	{
		$deleteUser = User::find($request->input('_id'));
        if ( !is_object($deleteUser) ) {
            $datos = array(
                'mensaje' => 'No se encuentra el Usuario en la Base de Datos',
                'estado' => 'error',
            );
        } else {

            //$deleteUser->id_user_deleted = Auth::user()->id;
            //$deleteUser->timestamps = false;
            //$deleteUser->save();

            //Se elimina el perfil(role_user) que tiene el usuario
            if ( $deleteUser->roles ) {
                $deleteUser->roles()->sync([]);
            }
            $deleteUser->delete();

            $datos = array(
                'mensaje' => 'Se ha eliminado correctamente el Usuario.',
                'estado' => 'success',
                'id' => $deleteUser->id,
            );
        }

        return response()->json($datos,200);
	}



	public function getModalEditarPerfil()
	{
		$usuario = User::with('roles')->findOrFail(\Auth::user()->id);
		return view('usuario.modal_editar_perfil')->with('usuario',$usuario);
	}

	public function postEditarPerfil(Request $request)
	{
	
		// Mensajes del validador
        $messages = [
            'required' => 'Debe ingresar el :attribute',
            'rut.required' => 'Debe ingresar el Rut',
            'nombre.required' => 'Debe ingresar el Nombre',
            'numeric' => 'El :attribute debe solo contener números',
            'max' => 'El :attribute no debe exeder los :max caracteres',
            'min' => 'El :attribute debe tener minimo :min caracteres',
            'unique' => 'El valor :attribute ya existe en la base de datos',
            'email' => 'Debe ingresar un E-mail valido',
            'cl_rut' => 'El Rut es incorrecto',
        ];

        // Reglas del validador
        $rules = [
            'rut' => 'required|unique:users,rut,'.$request->input("_id").',id,deleted_at,NULL|cl_rut',
			'nombre' => 'required',
            //'email'  => 'email',
		];
		
		if ( $request->input('nueva_clave') != null ) {
			$rules['nueva_clave'] = 'string|min:6';
		}

        if ( $request->input('email') != null ) {
            $rules['email'] = 'email';
        }

        // Validador de los input del formulario
        $validator = \Validator::make($request->all(), $rules, $messages);

        //Si contiene errores se devuelven todos los errores, de lo contrario guarda en la base de datos
        if ( $validator->fails() ) {
            return response()->json($validator->errors(),400);
        } else {
			$editUser = User::findOrFail($request->input('_id'));
			$editUser->editPerfil($request);

			$datos = array(
				'estado' => 'success',
				'nombre' => $editUser->name,
				'rut' => $editUser->formatRut(),
				'iniciales' => $editUser->iniciales,
                'email' => $editUser->email,
                'suspendido' => $editUser->suspendido(),
                'responsable' => $editUser->responsable(),
                'id' => $editUser->id,
			);
			
			if ( $request->input('nueva_clave') != null ) {
				$datos['mensaje'] = 'Edición exitosa del Perfil <br><strong style="color:black;">Recuerda que has cambiado tu clave</strong>';
			} else {
				$datos['mensaje'] = 'Edición exitosa del Perfil ';
			}
		}

		return response()->json($datos,200);
	}

    public function getTestEntrust()
    {
        
        
        //dd($usuarioAdmin);
        $usarioPropietario = User::findorfail(1);
        $propietario = Role::findorfail(1);
        
        // $usuarioAdmin->attachRole($propietario);
        /*Permisos de mantenedores */
        // $administrador->attachPermissions(array(1, 2, 3, 4, 5)); //Permisos Usuario
        // $propietario->attachPermissions(array(6, 7, 8, 9, 10)); // Permisos Proveedor
        // $propietario->attachPermissions(array(11, 12, 13, 14, 15)); // Permisos Tipo Documento
        // $propietario->attachPermissions(array(16, 17, 18, 19, 20)); // Permisos Item Presupuestario
        // $propietario->attachPermissions(array(21, 22, 23, 24, 25)); // Permisos Modalidad de compra
        // $propietario->attachPermissions(array(26, 27, 28, 29, 30)); // Permisos Tipo Adjudicacion
        // $propietario->attachPermissions(array(31, 32, 33, 34, 35)); // Permisos Tipo Informe
        // $propietario->attachPermissions(array(36, 37, 38, 39, 40)); // Permisos Tipo Archivo
        // $propietario->attachPermissions(array(41, 42, 43, 44, 45)); // Permisos Referente Tecnico
        /* Documentos y funcionalidades del sistema */
        // $propietario->attachPermissions(array(46, 47, 48, 49, 50)); // Permisos Documentos
        // $propietario->attachPermissions(array(51, 52)); // Permisos Documentos con Recepcion por confirmar
        // $propietario->attachPermissions(array(53, 54)); // Permisos Documentos con Recepcion confirmada
        // $propietario->attachPermissions(array(55, 56, 57, 58, 59)); // Permisos Documentos sin recepcion,pendiente cuadratura,cuadrar,cuadrados e invalidar
        // $propietario->attachPermissions(array(60, 61, 62, 63, 64)); // Permisos Documentos Bodega
        // $propietario->attachPermissions(array(65, 66, 67, 68, 69, 70, 71)); // Permisos Documentos Acepta
        // $propietario->attachPermissions(array(72, 73, 74, 75, 76, 77, 78, 79)); // Permisos Factoring
        // $propietario->attachPermissions(array(80, 81, 82, 83, 84, 85)); // Permisos Devengo
        $propietario->attachPermissions(array(86, 87, 88, 89, 90)); // Permisos Referente tecnico (opciones)
        // dd('listo1');
        // dd($usuarioAdmin,$propietario);

        // $test = new Role();
        // $test->name         = 'test';
        // $test->display_name = 'Usuario Test'; // optional
        //$test->description  = 'User is the owner of a given project'; // optional
        // $test->save();


        $test = Role::findorfail(4);
        //$usuarioTest = User::where('rut','11975531-K')->first();
        //$usuarioTest->attachRole($test);
        $test->attachPermissions(array(86,87,88));
        dd('listo2');
        
        // dd('holo');

        // $propietario = new Role();
        // $propietario->name         = 'propietario';
        // $propietario->display_name = 'Propietario Sistema'; // optional
        // $propietario->description  = 'User is the owner of a given project'; // optional
        // $propietario->save();

        // $aministrador = new Role();
        // $aministrador->name         = 'administrador';
        // $aministrador->display_name = 'Administrador'; // optional
        // $admin->description  = 'User is allowed to manage and edit other users'; // optional
        // $aministrador->save();
        // $propietario = Role::findorfail(1);
        // $usarioPropietario = User::findorfail(1);

        
        //dd($usarioPropietario,$propietario,$crearUser);
        // $usarioPropietario->attachRole($propietario);
        
        // $crearUser = new Permission();
        // $crearUser->name         = 'crear-user';
        // $crearUser->display_name = 'Crear Usuario'; // optional
        // Permite que un usuario...
        // $crearUser->description  = 'registre usuarios'; // optional
        //$crearUser->save();
        
        // $editarUser = new Permission();
        // $editarUser->name         = 'editar-user';
        // $editarUser->display_name = 'Editar Usuario'; // optional
        // Permite que un usuario...
        // $editarUser->description  = 'editar usuarios'; // optional
        //$editarUser->save();

        // $eliminarUser = new Permission();
        // $eliminarUser->name         = 'eliminar-user';
        // $eliminarUser->display_name = 'Eliminar Usuario'; // optional
        // Permite que un usuario...
        // $eliminarUser->description  = 'eliminar usuarios'; // optional
        //$eliminarUser->save();

        // $verUser = new Permission();
        // $verUser->name         = 'ver-user';
        // $verUser->display_name = 'Ver Usuario'; // optional
        // Permite que un usuario...
        // $verUser->description  = 'ver usuarios'; // optional
        //$verUser->save();


        // $verGeneralUser = new Permission();
        // $verGeneralUser->name         = 'ver-general-user';
        // $verGeneralUser->display_name = 'Ver Listado Usuarios'; // optional
        // Permite que un usuario...
        // $verGeneralUser->description  = 'ver listado de usuarios'; // optional
        //$verGeneralUser->save();


        //$propietario->attachPermissions(array($crearUser, $editarUser, $eliminarUser, $verUser, $verGeneralUser));
        //dd($usarioPropietario,$propietario,$crearUser);
    }


	// Funciones para la AP
	/*
    public function register(Request $request) {
    	//Recoger post
    	$json = $request->input('json',null);
    	$params = json_decode($json);

        $name = (!is_null($json) && isset($params->name)) ? $params->name : null;
        $rut = (!is_null($json) && isset($params->rut)) ? $params->rut : null;
        $password = (!is_null($json) && isset($params->password)) ? $params->password : null;
    	$email = (!is_null($json) && isset($params->email)) ? $params->email : null;
        $iniciales = (!is_null($json) && isset($params->iniciales)) ? $params->iniciales : null;

        $responsable = (!is_null($json) && isset($params->responsable)) ? $params->responsable : 0;
        $suspendido = (!is_null($json) && isset($params->suspendido)) ? $params->suspendido : 0;

    	if ( !is_null($password) && $password != "" && !is_null($name) && $name != "" && !is_null($rut) && $rut != "" ) {
    		// Crear el usuario
    		$user = new User();
    		
    		$user->name = $name;
            $user->rut = $rut;

    		$pwd = hash('sha256', $password);
            $user->password = $pwd;
            $user->email = $email;
            $user->iniciales = $iniciales;
            $user->responsable = $responsable;
            $user->suspendido = $suspendido;
    		
    		//Comprobar usuario duplicado
    		$isset_user = User::where('rut','=',$rut)->first();
            
    		if ( $isset_user == null ) {
    			//guardar el usuario
    			$user->save();
    			$data = array(
	    			'status' => 'success',
	    			'code' => 400,
	    			'message' => 'Usuario registrado correctamente'
	    		);
    		} else {
    			//no guardarlo, ya existe
    			$data = array(
	    			'status' => 'error',
	    			'code' => 400,
	    			'message' => 'Usuario duplicado, no puede registrarse'
	    		);
    		}

    	} else {
    		$data = array(
    			'status' => 'error',
    			'code' => 400,
    			'message' => 'Usuario no creado'
    		);
    	}

    	return response()->json($data,200);
    }

    public function login(Request $request) {
		$jwtAuth = new JwtAuth();

		// Recibir POST
		$json = $request->input('json',null);
		$params = json_decode($json);
		$rut = (!is_null($json) && isset($params->rut)) ? $params->rut : null;
		$password = (!is_null($json) && isset($params->password)) ? $params->password : null;
		$getToken =  (!is_null($json) && isset($params->getToken)) ? $params->getToken : null;

		// Cifrar la password
		$pwd = hash('sha256',$password);

		if (!is_null($rut) && !is_null($password) && ($getToken == null || $getToken == 'false')) {

			$signup = $jwtAuth->signup($rut,$pwd);

		} elseif ($getToken != null) {
			
			$signup = $jwtAuth->signup($rut,$pwd, $getToken);

		} else {
			$signup = array(
				'status' => 'error',
				'message' => 'Envia correctamente tus datos por post'
			);
		}

		return response()->json($signup, 200);
	}
	*/
}
