<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Carbon\Carbon;

use App\Documento;
use App\User;
use App\ItemPresupuestario;
use App\FolioSigfe;
use App\ArchivoAcepta;

use App\Role;

class GraficosController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function getDocumentosIngresados()
    {

        if ( !\Entrust::hasRole(['propietario','administrador']) ) {
            return \Redirect::to('home');
        }

        $indicadores = new \stdClass;

        $indicadores->docsMesPrograma = Documento::where('fecha_ingreso','>=',date('Y-m').'-01 00:00:00')
                                                 ->where('id_tipo_informe', 1)
                                                 ->count();

        $indicadores->docsMesCenabast = Documento::where('fecha_ingreso','>=',date('Y-m').'-01 00:00:00')
                                                 ->where('id_tipo_informe', 2)
                                                 ->count();

        $indicadores->docsMesClinicas = Documento::where('fecha_ingreso','>=',date('Y-m').'-01 00:00:00')
                                                 ->where('id_tipo_informe', 3)
                                                 ->count();

        $indicadores->docsMesConvenio = Documento::where('fecha_ingreso','>=',date('Y-m').'-01 00:00:00')
                                                 ->where('id_tipo_informe', 4)
                                                 ->count();

        $indicadores->docsMesConsumosBasicos = Documento::where('fecha_ingreso','>=',date('Y-m').'-01 00:00:00')
                                                 ->where('id_tipo_informe', 5)
                                                 ->count();

        $indicadores->docsMesHonorarios = Documento::where('fecha_ingreso','>=',date('Y-m').'-01 00:00:00')
                                                 ->where('id_tipo_informe', 6)
                                                 ->count();

        $indicadores->docsMesFondoFijo = Documento::where('fecha_ingreso','>=',date('Y-m').'-01 00:00:00')
                                                 ->where('id_tipo_informe', 7)
                                                 ->count();

        $indicadores->docsMesLiquidaAnticipo = Documento::where('fecha_ingreso','>=',date('Y-m').'-01 00:00:00')
                                                 ->where('id_tipo_informe', 8)
                                                 ->count();

        $indicadores->docsMesGobTransparente = Documento::where('fecha_ingreso','>=',date('Y-m').'-01 00:00:00')
                                                 ->where('id_tipo_informe', 9)
                                                 ->count();

        $indicadores->documentosMes = formatoMiles( Documento::where('fecha_ingreso','>=',date('Y-m').'-01 00:00:00')->count() );


        $usuariosIngresoDocs = Documento::where('fecha_ingreso','>=',date('Y-m').'-01 00:00:00')->get()->pluck('id_digitador')->toArray();

        $users = User::whereIn('id', $usuariosIngresoDocs)
                      ->with(['getDocumentos' => function ($q){
                        $q->where('fecha_ingreso','>=',date('Y-m').'-01 00:00:00');
                      }])
                      ->get();

        return view('graficos.documentos_ingresados')
             ->with('indicadores', $indicadores)
             ->with('users', $users);
    }

    public function postDocumentosIngresados(Request $request)
    {

        if ( !\Entrust::hasRole(['propietario','administrador']) ) {
            return \Redirect::to('home');
        }

        $fechaInicio = fecha_Y_m_d('01/'.$request->filtro_fecha_inicio);
        $fechaTermino = fecha_Y_m_d('31/'.$request->filtro_fecha_termino);

        // dd($request->all(), $fechaInicio, $fechaTermino);

        $indicadores = new \stdClass;

        $indicadores->docsMesPrograma = Documento::where('fecha_ingreso','>=',$fechaInicio.' 00:00:00')
                                                 ->where('fecha_ingreso', '<=', $fechaTermino.' 23:59:59')
                                                 ->where('id_tipo_informe', 1)
                                                 ->count();

        $indicadores->docsMesCenabast = Documento::where('fecha_ingreso','>=',$fechaInicio.' 00:00:00')
                                                 ->where('fecha_ingreso', '<=', $fechaTermino.' 23:59:59')
                                                 ->where('id_tipo_informe', 2)
                                                 ->count();

        $indicadores->docsMesClinicas = Documento::where('fecha_ingreso','>=',$fechaInicio.' 00:00:00')
                                                 ->where('fecha_ingreso', '<=', $fechaTermino.' 23:59:59')
                                                 ->where('id_tipo_informe', 3)
                                                 ->count();

        $indicadores->docsMesConvenio = Documento::where('fecha_ingreso','>=',$fechaInicio.' 00:00:00')
                                                 ->where('fecha_ingreso', '<=', $fechaTermino.' 23:59:59')
                                                 ->where('id_tipo_informe', 4)
                                                 ->count();

        $indicadores->docsMesConsumosBasicos = Documento::where('fecha_ingreso','>=',$fechaInicio.' 00:00:00')
                                                        ->where('fecha_ingreso', '<=', $fechaTermino.' 23:59:59')
                                                        ->where('id_tipo_informe', 5)
                                                        ->count();

        $indicadores->docsMesHonorarios = Documento::where('fecha_ingreso','>=',$fechaInicio.' 00:00:00')
                                                   ->where('fecha_ingreso', '<=', $fechaTermino.' 23:59:59')
                                                   ->where('id_tipo_informe', 6)
                                                   ->count();

        $indicadores->docsMesFondoFijo = Documento::where('fecha_ingreso','>=',$fechaInicio.' 00:00:00')
                                                  ->where('fecha_ingreso', '<=', $fechaTermino.' 23:59:59')
                                                  ->where('id_tipo_informe', 7)
                                                  ->count();

        $indicadores->docsMesLiquidaAnticipo = Documento::where('fecha_ingreso','>=',$fechaInicio.' 00:00:00')
                                                        ->where('fecha_ingreso', '<=', $fechaTermino.' 23:59:59')
                                                        ->where('id_tipo_informe', 8)
                                                        ->count();

        $indicadores->docsMesGobTransparente = Documento::where('fecha_ingreso','>=',$fechaInicio.' 00:00:00')
                                                        ->where('fecha_ingreso', '<=', $fechaTermino.' 23:59:59')
                                                        ->where('id_tipo_informe', 9)
                                                        ->count();

        $indicadores->documentosMes = formatoMiles( Documento::where('fecha_ingreso','>=',$fechaInicio.' 00:00:00')
                                                             ->where('fecha_ingreso', '<=', $fechaTermino.' 23:59:59')
                                                             ->count() );


        $usuariosIngresoDocs = Documento::where('fecha_ingreso','>=',$fechaInicio.' 00:00:00')
                                        ->where('fecha_ingreso', '<=', $fechaTermino.' 23:59:59')
                                        ->get()->pluck('id_digitador')->toArray();

        $users = User::whereIn('id', $usuariosIngresoDocs)
                    ->with(['getDocumentos' => function ($q) use ( $fechaInicio, $fechaTermino ){
                        $q->where('fecha_ingreso','>=',$fechaInicio.' 00:00:00')
                          ->where('fecha_ingreso', '<=', $fechaTermino.' 23:59:59');
                    }])
                    ->get();

        return view('graficos.documentos_ingresados')
             ->with('indicadores', $indicadores)
             ->with('fechaInicio', $request->filtro_fecha_inicio)
             ->with('fechaTermino', $request->filtro_fecha_termino)
             ->with('users', $users);
    }


    public function getItemsDevengados()
    {

        if ( !\Entrust::hasRole(['propietario','administrador']) ) {
            return \Redirect::to('home');
        }

        $itemsPresupuestarios = $this->getOpcionesItems();
        
        return view('graficos.items_devengados')->with('itemsPresupuestarios', $itemsPresupuestarios);
        
    }

    public function postItemsDevengados(Request $request)
    {

        if ( !\Entrust::hasRole(['propietario','administrador']) ) {
            return \Redirect::to('home');
        }

        $itemsPresupuestarios = $this->getOpcionesItems($request);

        /**
         * 1° obtener el id de los items presupuestarios
         * 2° buscar los folios sigfe segun la fecha y el id de los items presupuestarios.
         * 3° las fechas son por años, 2018, 2019, 2020
         */

         // 2019
        $año2019 = array();
        $añoQuery = 2019;
        $año2019 = $this->getDatosPorAño($request, $añoQuery);

        $año2018 = array();
        $añoQuery = 2018;
        $año2018 = $this->getDatosPorAño($request, $añoQuery);
        
        // if ( $request != null && in_array(4, $request->get('filtro_item_presupuestario'))  ) {
            
        //     $item2204 = new \stdClass;
        //     $item2204->item = '2204';
        //     $titulo = '22';
        //     $subtitulo = '04';

        //     for ($mes = 1; $mes < 13 ; $mes++) {
        
        //         $item2204->$mes = formatoMiles($this->getMontoMensualItem($mes, $añoQuery, $titulo, $subtitulo) );

        //     }

        //     $año2019[] = $item2204;
        // }

        

        // dd($año2019);
        // foreach($año2019 as $item) {
        //     dd($item);
        // }

        return view('graficos.items_devengados')->with('itemsPresupuestarios', $itemsPresupuestarios)->with('año2019', $año2019)->with('año2018', $año2018);
    }

    /**
     * Opciones para el select multiple
     * son segun el titulo y subtitulo del item
     * retorna un array
     */
    public function getOpcionesItems($request = null)
    {
        
        $item2201 = new \stdClass;
        $item2201->value = 1;
        $item2201->option = '2201';
        $item2201->selected = '';
        if ( $request != null && in_array(1, $request->get('filtro_item_presupuestario'))  ) {
            $item2201->selected = 'selected';
        }
        $itemsPresupuestarios[] = $item2201;

        $item2202 = new \stdClass;
        $item2202->value = 2;
        $item2202->option = '2202';
        $item2202->selected = '';
        if ( $request != null && in_array(2, $request->get('filtro_item_presupuestario'))  ) {
            $item2202->selected = 'selected';
        }
        $itemsPresupuestarios[] = $item2202;

        $item2203 = new \stdClass;
        $item2203->value = 3;
        $item2203->option = '2203';
        $item2203->selected = '';
        if ( $request != null && in_array(3, $request->get('filtro_item_presupuestario'))  ) {
            $item2203->selected = 'selected';
        }
        $itemsPresupuestarios[] = $item2203;

        $item2204 = new \stdClass;
        $item2204->value = 4;
        $item2204->option = '2204';
        $item2204->selected = '';
        if ( $request != null && in_array(4, $request->get('filtro_item_presupuestario'))  ) {
            $item2204->selected = 'selected';
        }
        $itemsPresupuestarios[] = $item2204;

        $item2205 = new \stdClass;
        $item2205->value = 5;
        $item2205->option = '2205';
        $item2205->selected = '';
        if ( $request != null && in_array(5, $request->get('filtro_item_presupuestario'))  ) {
            $item2205->selected = 'selected';
        }
        $itemsPresupuestarios[] = $item2205;

        $item2206 = new \stdClass;
        $item2206->value = 6;
        $item2206->option = '2206';
        $item2206->selected = '';
        if ( $request != null && in_array(6, $request->get('filtro_item_presupuestario'))  ) {
            $item2206->selected = 'selected';
        }
        $itemsPresupuestarios[] = $item2206;

        $item2207 = new \stdClass;
        $item2207->value = 7;
        $item2207->option = '2207';
        $item2207->selected = '';
        if ( $request != null && in_array(7, $request->get('filtro_item_presupuestario'))  ) {
            $item2207->selected = 'selected';
        }
        $itemsPresupuestarios[] = $item2207;

        $item2208 = new \stdClass;
        $item2208->value = 8;
        $item2208->option = '2208';
        $item2208->selected = '';
        if ( $request != null && in_array(8, $request->get('filtro_item_presupuestario'))  ) {
            $item2208->selected = 'selected';
        }
        $itemsPresupuestarios[] = $item2208;

        $item2209 = new \stdClass;
        $item2209->value = 9;
        $item2209->option = '2209';
        $item2209->selected = '';
        if ( $request != null && in_array(9, $request->get('filtro_item_presupuestario'))  ) {
            $item2209->selected = 'selected';
        }
        $itemsPresupuestarios[] = $item2209;

        $item2210 = new \stdClass;
        $item2210->value = 10;
        $item2210->option = '2210';
        $item2210->selected = '';
        if ( $request != null && in_array(10, $request->get('filtro_item_presupuestario'))  ) {
            $item2210->selected = 'selected';
        }
        $itemsPresupuestarios[] = $item2210;

        $item2211 = new \stdClass;
        $item2211->value = 11;
        $item2211->option = '2211';
        $item2211->selected = '';
        if ( $request != null && in_array(11, $request->get('filtro_item_presupuestario'))  ) {
            $item2211->selected = 'selected';
        }
        $itemsPresupuestarios[] = $item2211;

        $item2212 = new \stdClass;
        $item2212->value = 12;
        $item2212->option = '2212';
        $item2212->selected = '';
        if ( $request != null && in_array(12, $request->get('filtro_item_presupuestario'))  ) {
            $item2212->selected = 'selected';
        }
        $itemsPresupuestarios[] = $item2212;

        $item3407 = new \stdClass;
        $item3407->value = 13;
        $item3407->option = '3407';
        $item3407->selected = '';
        if ( $request != null && in_array(13, $request->get('filtro_item_presupuestario'))  ) {
            $item3407->selected = 'selected';
        }
        $itemsPresupuestarios[] = $item3407;


        return $itemsPresupuestarios;
    }

    /**
     * Query para obtener la cantidad devengada en los 12 meses de cada año.
     * retorna el monto de cada mes.
     */
    public function getMontoMensualItem($mes, $añoQuery, $titulo, $subtitulo, &$numeroTotalDevengoItem)
    {
        $foliosSigfe = FolioSigfe::where('year_sigfe', $añoQuery)
                                ->where('mes_sigfe', $mes)

                                ->with(['getDevengos'=> function ($quey) use ($titulo, $subtitulo){
                                            $quey->whereHas('getItemPresupuestario', function ($q) use ($titulo, $subtitulo){
                                                $q->where('titulo', $titulo)->where('subtitulo', $subtitulo);
                                            });
                                        },'getDocumento'])

                                ->whereHas('getDevengos', function ($query) use ($titulo, $subtitulo){
                                    $query->whereHas('getItemPresupuestario', function ($q) use ($titulo, $subtitulo){
                                        $q->where('titulo', $titulo)->where('subtitulo', $subtitulo);
                                    });
                                })
                                ->get();

        $montoMes = 0;
        foreach ( $foliosSigfe as $folio ) {
            
            foreach ( $folio->getDevengos as $devengo ) {

                if ( $folio->getDocumento->id_tipo_documento == 4 || $folio->getDocumento->id_tipo_documento == 10 ) {
                    $montoMes -= $devengo->monto;
                } else {
                    $montoMes += $devengo->monto;
                }

                $numeroTotalDevengoItem++;

            }

        }

        return $montoMes;
    }

    /**
     * Se obtienen los datos para cada año, respecto al grafico por items.
     * Devuelve el arreglo con todos los montos de cada para el año
     */
    public function getDatosPorAño($request, $añoQuery) {

        $añoArray = array();

        if ( $request != null && in_array(1, $request->get('filtro_item_presupuestario'))  ) {
            
            // $item = new \stdClass;
            $item['item'] = '2201';
            $titulo = '22';
            $subtitulo = '01';
            $montoTotalItem = 0;
            $numeroTotalDevengoItem = 0;

            for ($mes = 1; $mes < 13 ; $mes++) {
        
                $item[$mes] = $this->getMontoMensualItem($mes, $añoQuery, $titulo, $subtitulo, $numeroTotalDevengoItem);
                $montoTotalItem += $item[$mes];

            }

            $item[13] = $montoTotalItem / 12;

            $añoArray[] = $item;
        }

        if ( $request != null && in_array(2, $request->get('filtro_item_presupuestario'))  ) {
            
            // $item = new \stdClass;
            $item['item'] = '2202';
            $titulo = '22';
            $subtitulo = '02';
            $montoTotalItem = 0;
            $numeroTotalDevengoItem = 0;

            for ($mes = 1; $mes < 13 ; $mes++) {
        
                $item[$mes] = $this->getMontoMensualItem($mes, $añoQuery, $titulo, $subtitulo, $numeroTotalDevengoItem);
                $montoTotalItem += $item[$mes];

            }

            $item[13] = $montoTotalItem / 12;

            $añoArray[] = $item;
        }

        if ( $request != null && in_array(3, $request->get('filtro_item_presupuestario'))  ) {
            
            // $item = new \stdClass;
            $item['item'] = '2203';
            $titulo = '22';
            $subtitulo = '03';
            $montoTotalItem = 0;
            $numeroTotalDevengoItem = 0;

            for ($mes = 1; $mes < 13 ; $mes++) {
        
                $item[$mes] = $this->getMontoMensualItem($mes, $añoQuery, $titulo, $subtitulo, $numeroTotalDevengoItem);
                $montoTotalItem += $item[$mes];

            }

            $item[13] = $montoTotalItem / 12;

            $añoArray[] = $item;
        }

        if ( $request != null && in_array(4, $request->get('filtro_item_presupuestario'))  ) {
            
            // $item = new \stdClass;
            $item['item'] = '2204';
            $titulo = '22';
            $subtitulo = '04';
            $montoTotalItem = 0;
            $numeroTotalDevengoItem = 0;

            for ($mes = 1; $mes < 13 ; $mes++) {
        
                $item[$mes] = $this->getMontoMensualItem($mes, $añoQuery, $titulo, $subtitulo, $numeroTotalDevengoItem);
                $montoTotalItem += $item[$mes];

            }

            $item[13] = $montoTotalItem / 12;

            $añoArray[] = $item;
        }

        if ( $request != null && in_array(5, $request->get('filtro_item_presupuestario'))  ) {
            
            // $item = new \stdClass;
            $item['item'] = '2205';
            $titulo = '22';
            $subtitulo = '05';
            $montoTotalItem = 0;
            $numeroTotalDevengoItem = 0;

            for ($mes = 1; $mes < 13 ; $mes++) {
        
                $item[$mes] = $this->getMontoMensualItem($mes, $añoQuery, $titulo, $subtitulo, $numeroTotalDevengoItem);
                $montoTotalItem += $item[$mes];

            }

            $item[13] = $montoTotalItem / 12;

            $añoArray[] = $item;
        }

        if ( $request != null && in_array(6, $request->get('filtro_item_presupuestario'))  ) {
            
            // $item = new \stdClass;
            $item['item'] = '2206';
            $titulo = '22';
            $subtitulo = '06';
            $montoTotalItem = 0;
            $numeroTotalDevengoItem = 0;

            for ($mes = 1; $mes < 13 ; $mes++) {
        
                $item[$mes] = $this->getMontoMensualItem($mes, $añoQuery, $titulo, $subtitulo, $numeroTotalDevengoItem);
                $montoTotalItem += $item[$mes];

            }

            $item[13] = $montoTotalItem / 12;

            $añoArray[] = $item;
        }

        if ( $request != null && in_array(7, $request->get('filtro_item_presupuestario'))  ) {
            
            // $item = new \stdClass;
            $item['item'] = '2207';
            $titulo = '22';
            $subtitulo = '07';
            $montoTotalItem = 0;
            $numeroTotalDevengoItem = 0;

            for ($mes = 1; $mes < 13 ; $mes++) {
        
                $item[$mes] = $this->getMontoMensualItem($mes, $añoQuery, $titulo, $subtitulo, $numeroTotalDevengoItem);
                $montoTotalItem += $item[$mes];

            }

            $item[13] = $montoTotalItem / 12;

            $añoArray[] = $item;
        }

        if ( $request != null && in_array(8, $request->get('filtro_item_presupuestario'))  ) {
            
            // $item = new \stdClass;
            $item['item'] = '2208';
            $titulo = '22';
            $subtitulo = '08';
            $montoTotalItem = 0;
            $numeroTotalDevengoItem = 0;

            for ($mes = 1; $mes < 13 ; $mes++) {
        
                $item[$mes] = $this->getMontoMensualItem($mes, $añoQuery, $titulo, $subtitulo, $numeroTotalDevengoItem);
                $montoTotalItem += $item[$mes];

            }

            $item[13] = $montoTotalItem / 12;

            $añoArray[] = $item;
        }

        if ( $request != null && in_array(9, $request->get('filtro_item_presupuestario'))  ) {
            
            // $item = new \stdClass;
            $item['item'] = '2209';
            $titulo = '22';
            $subtitulo = '09';
            $montoTotalItem = 0;
            $numeroTotalDevengoItem = 0;

            for ($mes = 1; $mes < 13 ; $mes++) {
        
                $item[$mes] = $this->getMontoMensualItem($mes, $añoQuery, $titulo, $subtitulo, $numeroTotalDevengoItem);
                $montoTotalItem += $item[$mes];

            }

            $item[13] = $montoTotalItem / 12;

            $añoArray[] = $item;
        }

        if ( $request != null && in_array(10, $request->get('filtro_item_presupuestario'))  ) {
            
            // $item = new \stdClass;
            $item['item'] = '2210';
            $titulo = '22';
            $subtitulo = '10';
            $montoTotalItem = 0;
            $numeroTotalDevengoItem = 0;

            for ($mes = 1; $mes < 13 ; $mes++) {
        
                $item[$mes] = $this->getMontoMensualItem($mes, $añoQuery, $titulo, $subtitulo, $numeroTotalDevengoItem);
                $montoTotalItem += $item[$mes];

            }

            $item[13] = $montoTotalItem / 12;

            $añoArray[] = $item;
        }

        if ( $request != null && in_array(11, $request->get('filtro_item_presupuestario'))  ) {
            
            // $item = new \stdClass;
            $item['item'] = '2211';
            $titulo = '22';
            $subtitulo = '11';
            $montoTotalItem = 0;
            $numeroTotalDevengoItem = 0;

            for ($mes = 1; $mes < 13 ; $mes++) {
        
                $item[$mes] = $this->getMontoMensualItem($mes, $añoQuery, $titulo, $subtitulo, $numeroTotalDevengoItem);
                $montoTotalItem += $item[$mes];

            }

            $item[13] = $montoTotalItem / 12;
            // $item['item'] .= 'monto total : '.$montoTotalItem.' | n°Total : '.$numeroTotalDevengoItem;

            $añoArray[] = $item;
        }

        if ( $request != null && in_array(12, $request->get('filtro_item_presupuestario'))  ) {
            
            // $item = new \stdClass;
            $item['item'] = '2212';
            $titulo = '22';
            $subtitulo = '12';
            $montoTotalItem = 0;
            $numeroTotalDevengoItem = 0;

            for ($mes = 1; $mes < 13 ; $mes++) {
        
                $item[$mes] = $this->getMontoMensualItem($mes, $añoQuery, $titulo, $subtitulo, $numeroTotalDevengoItem);
                $montoTotalItem += $item[$mes];

            }

            $item[13] = $montoTotalItem / 12;

            $añoArray[] = $item;
        }

        if ( $request != null && in_array(13, $request->get('filtro_item_presupuestario'))  ) {
            
            // $item = new \stdClass;
            $item['item'] = '3407';
            $titulo = '34';
            $subtitulo = '07';
            $montoTotalItem = 0;
            $numeroTotalDevengoItem = 0;

            for ($mes = 1; $mes < 13 ; $mes++) {
        
                $item[$mes] = $this->getMontoMensualItem($mes, $añoQuery, $titulo, $subtitulo, $numeroTotalDevengoItem);
                $montoTotalItem += $item[$mes];

            }

            $item[13] = $montoTotalItem / 12;
            // $item['item'] .= ' monto total : '.$montoTotalItem.' | n°Total : '.$numeroTotalDevengoItem;
            $añoArray[] = $item;
        }

        return $añoArray;

    }

    public function getUsuariosAcepta()
    {

        if ( !\Entrust::hasRole(['propietario','administrador']) ) {
            return \Redirect::to('home');
        }

        $users = User::with(['getArchivosAcepta' => function ($q) {
                          $q->where('created_at','>=',date('Y-m').'-01 00:00:00');
                      }])
                      ->whereHas('getArchivosAcepta', function($query) {
                          $query->where('created_at','>=',date('Y-m').'-01 00:00:00');
                      })
                      ->get();

        foreach ( $users as $user ) {

            $archivosCargados = ArchivoAcepta::where('id_user_responsable', $user->id)
                                             ->where('created_at','>=',date('Y-m').'-01 00:00:00')
                                             ->where('cargado', 1)
                                             ->get();

            $archivosRezagados = ArchivoAcepta::where('id_user_responsable', $user->id)
                                             ->where('created_at','>=',date('Y-m').'-01 00:00:00')
                                             ->where('cargado', 0)
                                            //  ->where('id_user_rechazo', null)
                                             ->where('id_user_rezagar', '<>', null)
                                             ->get();

            $archivosRechazados = ArchivoAcepta::where('id_user_responsable', $user->id)
                                             ->where('created_at','>=',date('Y-m').'-01 00:00:00')
                                             ->where('cargado', 0)
                                             ->where('id_user_rechazo', '<>', null)
                                             ->where('id_user_rezagar', null)
                                             ->get();

            $archivosToBodega = ArchivoAcepta::where('id_user_responsable', $user->id)
                                             ->where('created_at','>=',date('Y-m').'-01 00:00:00')
                                             ->where('cargado', 0)
                                             ->has('getArchivoAceptaRevisionBodega')
                                             ->where('id_user_rechazo', null)
                                             ->where('id_user_rezagar', null)
                                             ->get();


            $archivosSinProcesar = ArchivoAcepta::where('id_user_responsable', $user->id)
                                                ->where('created_at','>=',date('Y-m').'-01 00:00:00')
                                                ->where('cargado', 0)
                                                ->doesnthave('getArchivoAceptaRevisionBodega')
                                                ->where('id_user_rechazo', null)
                                                ->where('id_user_rezagar', null)
                                                ->get();

            $user->archivosTotales = $user->getArchivosAcepta->count();
            $user->archivosCargados = $archivosCargados->count();
            $user->archivosRezagados = $archivosRezagados->count();
            $user->archivosRechazados = $archivosRechazados->count();
            $user->archivosToBodega = $archivosToBodega->count();
            $user->archivosSinProcesar = $archivosSinProcesar->count();

        }

        return view('graficos.usuarios_acepta')
             ->with('users', $users);

    }

    public function postUsuariosAcepta(Request $request)
    {
        $fechaInicio = fecha_Y_m_d('01/'.$request->filtro_fecha_inicio);
        $fechaTermino = fecha_Y_m_d('31/'.$request->filtro_fecha_termino);

        $users = User::with(['getArchivosAcepta' => function ($q) use ($fechaInicio, $fechaTermino) {
                                $q->where('created_at', '>=', $fechaInicio.' 00:00:00')
                                  ->where('created_at', '<=', $fechaTermino.' 23:59:59');
                            }])
                            ->whereHas('getArchivosAcepta', function($query) use ($fechaInicio, $fechaTermino) {
                                $query->where('created_at', '>=', $fechaInicio.' 00:00:00')
                                      ->where('created_at', '<=', $fechaTermino.' 23:59:59');
                            })
                            ->get();

        foreach ( $users as $user ) {

            $archivosCargados = ArchivoAcepta::where('id_user_responsable', $user->id)
                                             ->where('created_at', '>=', $fechaInicio.' 00:00:00')
                                             ->where('created_at', '<=', $fechaTermino.' 23:59:59')
                                             ->where('cargado', 1)
                                             ->get();

            $archivosRezagados = ArchivoAcepta::where('id_user_responsable', $user->id)
                                              ->where('created_at', '>=', $fechaInicio.' 00:00:00')
                                              ->where('created_at', '<=', $fechaTermino.' 23:59:59')
                                              ->where('cargado', 0)
                                              ->where('id_user_rezagar', '<>', null)
                                              ->get();

            $archivosRechazados = ArchivoAcepta::where('id_user_responsable', $user->id)
                                               ->where('created_at', '>=', $fechaInicio.' 00:00:00')
                                               ->where('created_at', '<=', $fechaTermino.' 23:59:59')
                                               ->where('cargado', 0)
                                               ->where('id_user_rechazo', '<>', null)
                                               ->where('id_user_rezagar', null)
                                               ->get();


            $archivosSinProcesar = ArchivoAcepta::where('id_user_responsable', $user->id)
                                                ->where('created_at', '>=', $fechaInicio.' 00:00:00')
                                                ->where('created_at', '<=', $fechaTermino.' 23:59:59')
                                                ->where('cargado', 0)
                                                ->where('id_user_rechazo', null)
                                                ->where('id_user_rezagar', null)
                                                ->get();

            $user->archivosTotales = $user->getArchivosAcepta->count();
            $user->archivosCargados = $archivosCargados->count();
            $user->archivosRezagados = $archivosRezagados->count();
            $user->archivosRechazados = $archivosRechazados->count();
            $user->archivosSinProcesar = $archivosSinProcesar->count();

        }

        return view('graficos.usuarios_acepta')
                ->with('users', $users)
                ->with('fechaInicio', $request->filtro_fecha_inicio)
                ->with('fechaTermino', $request->filtro_fecha_termino);
    }

    public function getUsuariosDocumentos()
    {
        if ( !\Entrust::hasRole(['propietario','administrador']) ) {
            return \Redirect::to('home');
        }

        // Se obtienen los ususarios que ingresan documentos
        // $users = Role::where('name','usuario-ingreso')->first()->users()->get();
        $usuariosResponsables = User::where('responsable',1)->get();
        $users = User::where('responsable', 1)->get();

        foreach ( $users as $key => $user) {
            // obtener los documentos ingresados
            $user->docsIngresados = Documento::where('id_responsable', $user->id)
                                             ->where('created_at','>=',date('Y-m-d').' 00:00:00')
                                             ->get()->count();

            if ( $user->docsIngresados !== 0 ) {
                $user->docsPorCuadrar = Documento::where('id_responsable', $user->id)
                                                 ->where('created_at','>=',date('Y-m-d').' 00:00:00')
                                                 ->where('validado', null)
                                                 ->where('id_user_problema', null)
                                                 ->get()->count();

                $user->docsCuadrados = Documento::where('id_responsable', $user->id)
                                                ->where('created_at','>=',date('Y-m-d').' 00:00:00')
                                                ->where('validado', 1)
                                                ->where('id_user_problema', null)
                                                ->get()->count();

                $user->docsPorDevengar = Documento::where('id_responsable', $user->id)
                                                  ->where('created_at','>=',date('Y-m-d').' 00:00:00')
                                                  ->where('id_estado', 1)
                                                  ->where('total_documento', '>', 0)
                                                  ->where('reclamado', null)
                                                  ->get()->count();

                $user->docsDevengados = FolioSigfe::whereHas('getDocumento', function($query) use ($user){
                                                        $query->where('id_responsable', $user->id)
                                                            ->where('created_at','>=',date('Y-m-d').' 00:00:00');
                                                    })
                                                    ->get()->count();

                $user->docsConProblemas = Documento::where('id_responsable', $user->id)
                                                   ->where('created_at','>=',date('Y-m-d').' 00:00:00')
                                                   ->where('id_user_problema', '<>',null)
                                                   ->get()->count();
            } else {
                $users->forget($key);
            }

        }

        return view('graficos.usuarios_documentos')->with('users', $users)->with('usuariosResponsables', $usuariosResponsables);
    }
    
    public function postUsuariosDocumentos(Request $request)
    {
        

        $fechaInicio = fecha_Y_m_d( $request->input('filtro_fecha_inicio' ) );
        $fechaTermino = fecha_Y_m_d( $request->input('filtro_fecha_termino' ) );

        $usuariosResponsables = User::where('responsable',1)->get();

        if ( $request->input('filtro_responsable') == null ) {
            $users = User::where('responsable', 1)->get();
        } else {
            $users = User::where('responsable', 1)->WhereIn('id', $request->input('filtro_responsable') )->get();
        }
        

        foreach ( $users as $key => $user) {
            // obtener los documentos ingresados
            $user->docsIngresados = Documento::where('id_responsable', $user->id)
                                             ->where('created_at','>=', $fechaInicio.' 00:00:00')
                                             ->where('created_at','<=', $fechaTermino.' 23:59:59')
                                             ->get()->count();
                                       
            if ( $user->docsIngresados !== 0 ) {
                $user->docsPorCuadrar = Documento::where('id_responsable', $user->id)
                                                 ->where('created_at','>=', $fechaInicio.' 00:00:00')
                                                 ->where('created_at','<=', $fechaTermino.' 23:59:59')
                                                 ->where('validado', null)
                                                 ->where('id_user_problema', null)
                                                 ->get()->count();

                $user->docsCuadrados = Documento::where('id_responsable', $user->id)
                                                ->where('created_at','>=', $fechaInicio.' 00:00:00')
                                                ->where('created_at','<=', $fechaTermino.' 23:59:59')
                                                ->where('validado', 1)
                                                ->where('id_user_problema', null)
                                                ->get()->count();

                $user->docsPorDevengar = Documento::where('id_responsable', $user->id)
                                                  ->where('created_at','>=', $fechaInicio.' 00:00:00')
                                                  ->where('created_at','<=', $fechaTermino.' 23:59:59')
                                                  ->where('id_estado', 1)
                                                  ->where('total_documento', '>', 0)
                                                  ->where('reclamado', null)
                                                  ->get()->count();

                $user->docsDevengados = FolioSigfe::whereHas('getDocumento', function($query) use ($user, $fechaInicio, $fechaTermino){
                                                        $query->where('id_responsable', $user->id)
                                                            ->where('created_at','>=', $fechaInicio.' 00:00:00')
                                                            ->where('created_at','<=', $fechaTermino.' 23:59:59');
                                                    })->get()->count();

                $user->docsConProblemas = Documento::where('id_responsable', $user->id)
                                                   ->where('created_at','>=', $fechaInicio.' 00:00:00')
                                                   ->where('created_at','<=', $fechaTermino.' 23:59:59')
                                                   ->where('id_user_problema', '<>',null)
                                                   ->get()->count();
            } else {
                $users->forget($key);
            }

        }

        return view('graficos.usuarios_documentos')
                ->with('users', $users)
                ->with('usuariosResponsables', $usuariosResponsables)
                ->with('filtroFechaInicio',$request->input('filtro_fecha_inicio'))
                ->with('filtroFechaTermino',$request->input('filtro_fecha_termino'))
                ->with('filtroResponsable', $request->input('filtro_responsable') );

    }

}
