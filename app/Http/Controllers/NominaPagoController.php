<?php namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests;
use Carbon\Carbon;

use iio\libmergepdf\Merger;
use iio\libmergepdf\Driver\TcpdiDriver;

use App\Traits\concatenarArchivosDocumento;

use App\Documento;
use App\TipoInforme;
use App\NominaPago;
use App\NominaPagoDocumento;


class NominaPagoController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
    */
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index()
    {
        return view('nomina_pago.index');
    }

    public function dataTableIndex(Request $request)
    {
        set_time_limit ( 0 );
        ini_set('max_execution_time', 0);

        $search = $request->get('search')['value'];
        $order_column = $request->get('order')[0]['column'];
        $order_dir = $request->get('order')[0]['dir'];

        $dataTable = $request->get('dataTable');

        $form = construyeFormulario( json_decode($request->get('form')) );
        
        // $fechaInicio = fecha_Y_m_d($form->filtro_fecha_inicio);
        // $fechaTermino = fecha_Y_m_d($form->filtro_fecha_termino);

        $nominas = NominaPago::all();

        $data = [];

        if ( $request->get('length') != -1 ) {
            $hasta = $request->get('start') + $request->get('length');
        } else {
            // Se quieren ver todos los datos
            $hasta = $request->get('start') + $nominas->count();
        }

        for( $i = $request->get('start') ; $i < $hasta ; $i++ ) {
            if ( isset($nominas[$i]) ) {
                $data[] = $nominas[$i]->getDatosIndex();
            }
        }

        if ( $nominas->count() > 0 ) {

            foreach ($data as $clave => $fila) {

                if ( $search != '' ) {
                    if ( in_array_r($search,$fila) ) {
                        // dd("lo encuentra",$fila,$search);
                        $columna[$clave] = $fila[$order_column];
                    } else {
                        // dd("NO lo encuentra",$fila,$search);
                        unset( $data[$clave] ); // quita el elemento del data
                    }
                } else {
                    $columna[$clave] = $fila[$order_column];
                }
                
            }

            
            // Se ordenan los datos, segun la columna seleccionada
            if ( $order_dir == 'asc' ) {
                array_multisort($columna, SORT_ASC, $data);
            } elseif ( $order_dir == 'desc' ) {
                array_multisort($columna, SORT_DESC, $data);
            }

        }

        $respuesta = array(
            "draw" => $request->get('draw'),
            "recordsTotal" => $nominas->count(),
            "recordsFiltered"=> $nominas->count(),
            "data" => $data
        );

        return json_encode($respuesta);
    }


    public function getModalNuevaNomina()
    {
        return view('nomina_pago.modal_crear');
    }

    public function store(Request $request)
    {
        // dd($request->all());

        // Mensajes de los validadores
        $messages = [
            'required'    => 'Debe ingresar el  :attribute',
            'numeric'     => 'El :attribute debe solo contener números',
            'date_format' => 'La :attribute no tiene el formato correcto: día/mes/año',
        ];

        //validador de los input del formulario
        $validator = \Validator::make($request->all(), [
            'saldo_disponible' => 'required',
            'fecha_pago'       => 'required|date_format:"m/Y"',
            'tipo_documentos'   => 'required'
        ], $messages);

        //Si contiene errores se devuelven todos los errores, de lo contrario guarda en la base de datos
        if ( $validator->fails() ) {
            return response()->json($validator->errors(), 400);
        } else {

            $fechaUltimoDia = Carbon::parse( strtotime( '01.'.str_replace('/','.', $request->get('fecha_pago'))) )
                                    ->endOfMonth()->toDateString();

            $nomina = new NominaPago;
            $nomina->saldo_disponible = formato_entero($request->get('saldo_disponible'));
            $nomina->sumatoria_nomina_pago = 0;
            $nomina->saldo_pago = formato_entero($request->get('saldo_disponible'));

            $nomina->fecha_nomina = $fechaUltimoDia;
            $nomina->mes_nomina = fecha_M($fechaUltimoDia);
            $nomina->año_nomina = fecha_Y($fechaUltimoDia);
            $nomina->save();

            // dd($nomina, $request->all(), 'listoco');

            $nomina->agregarDocumentos($request->input('tipo_documentos'), $nomina->saldo_disponible, $nomina->sumatoria_nomina_pago);

            $datos = array(
                'idNomina' => $nomina->id,
                'estado'   => 'success',
                'mensaje'  => 'Nómina creada con éxito.'
            );

            return response()->json($datos, 200);

        }
    }

    public function postGuardarSaldoDisponible(Request $request)
    {
        // dd($request->all());

        // Mensajes de los validadores
        $messages = [
            'required'    => 'Debe ingresar el  :attribute',
            'numeric'     => 'El :attribute debe solo contener números',
            'date_format' => 'La :attribute no tiene el formato correcto: día/mes/año',
        ];

        //validador de los input del formulario
        $validator = \Validator::make($request->all(), [
            'saldo_disponible' => 'required',
            '_id'       => 'required'
        ], $messages);

        //Si contiene errores se devuelven todos los errores, de lo contrario guarda en la base de datos
        if ( $validator->fails() ) {
            return response()->json($validator->errors(),400);
        } else {

            $fechaUltimoDia = Carbon::parse( strtotime( '01.'.str_replace('/','.', $request->get('fecha_pago'))) )
                                    ->endOfMonth()->toDateString();

            $nomina = NominaPago::findOrFail($request->get('_id'));
            $nomina->saldo_disponible = formato_entero($request->get('saldo_disponible'));
            $nomina->sumatoria_nomina_pago = formato_entero($request->get('nomina_pago'));
            $nomina->saldo_pago = formato_entero($request->get('saldo_pago'));
            $nomina->save();

            // dd($nomina, $request->all(), 'listoco');

            $datos = array(
                'idNomina' => $nomina->id,
                'estado'   => 'success',
                'mensaje'  => 'Guardado con éxito.'
            );

            return response()->json($datos,200);

        }
    }

    public function getEdit($id , $nuevo = false)
    {
        
        $nomina = NominaPago::findOrFail($id);
        return view('nomina_pago.edit')->with('nomina', $nomina)->with('nuevo', $nuevo);
        
    }

    public function getSeleccionarDocumentos($id)
    {
        $tiposInforme = TipoInforme::all();
        // Select para Fechas a utilizar en el filtro
        $fechaUtilizar1 = new \stdClass;
        $fechaUtilizar1->opcion = 'A 30 días';
        $fechaUtilizar2 = new \stdClass;
        $fechaUtilizar2->opcion = 'De 31 a 45 Días';
        $fechaUtilizar3 = new \stdClass;
        $fechaUtilizar3->opcion = 'De 46 a 60 Días';
        $fechaUtilizar4 = new \stdClass;
        $fechaUtilizar4->opcion = 'De 61 a 90 Días';
        $fechaUtilizar5 = new \stdClass;
        $fechaUtilizar5->opcion = 'De 91 a 120 Días';
        $fechaUtilizar6 = new \stdClass;
        $fechaUtilizar6->opcion = 'De 121 a 150 Días';
        $fechaUtilizar7 = new \stdClass;
        $fechaUtilizar7->opcion = 'De 151 a 180 Días';
        $fechaUtilizar8 = new \stdClass;
        $fechaUtilizar8->opcion = 'De 181 a más Días';
        
        $fechasUtilizar[] = $fechaUtilizar1;
        $fechasUtilizar[] = $fechaUtilizar2;
        $fechasUtilizar[] = $fechaUtilizar3;
        $fechasUtilizar[] = $fechaUtilizar4;
        $fechasUtilizar[] = $fechaUtilizar5;
        $fechasUtilizar[] = $fechaUtilizar6;
        $fechasUtilizar[] = $fechaUtilizar7;
        $fechasUtilizar[] = $fechaUtilizar8;

        return view('nomina_pago.modal_seleccionar_documentos')->with('idNomina', $id)
                                                               ->with('tiposInforme', $tiposInforme)
                                                               ->with('fechasUtilizar', $fechasUtilizar);
    }

    public function dataTableDocumentosParaNomina(Request $request)
    {
    
        set_time_limit ( 0 );
        ini_set('max_execution_time', 0);

        $formJSON = json_decode($request->get('form'));
        $form = construyeFormulario($formJSON);

        $verDocumento = \Auth::user()->can('ver-documento');
        $data = [];

        $honorarios = null;
        $fondosFijos = null;
        $liquidanAnticipos = null;
        $consumosBasicos = null;

        if ( ! isset($form->filtro_tipo_informe_seleccion_documentos) ) {

            $honorarios = Documento::DocumentosParaNominas()
                                   ->where('id_tipo_informe', 6);

            //  ¿quitar ?
            $fondosFijos = Documento::DocumentosParaNominas()
                                    ->where('id_tipo_informe', 7);

            // quitar 
            // $liquidanAnticipos = Documento::DocumentosParaNominas()
            //                             ->where('id_tipo_informe', 8);

            $consumosBasicos = Documento::DocumentosParaNominas()
                                        ->where('id_tipo_informe', 5);

        }
        

        $documentos = Documento::DocumentosParaNominas();

        switch ($form->filtro_días_antiguedad_seleccion_documentos) {
            case 'A 30 días':
                if ( ! isset($form->filtro_tipo_informe_seleccion_documentos) ) {
                    $honorarios = $honorarios->where('dias_antiguedad', '<=', 30);
                    $fondosFijos = $fondosFijos->where('dias_antiguedad', '<=', 30);
                    // $liquidanAnticipos = $liquidanAnticipos->where('dias_antiguedad', '<=', 30);
                    $consumosBasicos = $consumosBasicos->where('dias_antiguedad', '<=', 30);
                }
                $documentos = $documentos->where('dias_antiguedad', '<=', 30);
                break;

            case 'De 31 a 45 Días':
                if ( ! isset($form->filtro_tipo_informe_seleccion_documentos) ) {
                    $honorarios = $honorarios->where('dias_antiguedad', '>=', 31)
                                             ->where('dias_antiguedad', '<=', 45);

                    $fondosFijos = $fondosFijos->where('dias_antiguedad', '>=', 31)
                                               ->where('dias_antiguedad', '<=', 45);

                    // $liquidanAnticipos = $liquidanAnticipos->where('dias_antiguedad', '>=', 31)
                    //                                     ->where('dias_antiguedad', '<=', 45);

                    $consumosBasicos = $consumosBasicos->where('dias_antiguedad', '>=', 31)
                                                       ->where('dias_antiguedad', '<=', 45);
                }
                $documentos = $documentos->where('dias_antiguedad', '>=', 31)
                                         ->where('dias_antiguedad', '<=', 45);
                break;

            case 'De 46 a 60 Días':
                if ( ! isset($form->filtro_tipo_informe_seleccion_documentos) ) {
                    $honorarios = $honorarios->where('dias_antiguedad', '>=', 46)
                                             ->where('dias_antiguedad', '<=', 60);

                    $fondosFijos = $fondosFijos->where('dias_antiguedad', '>=', 46)
                                               ->where('dias_antiguedad', '<=', 60);

                    // $liquidanAnticipos = $liquidanAnticipos->where('dias_antiguedad', '>=', 46)
                    //                                     ->where('dias_antiguedad', '<=', 60);

                    $consumosBasicos = $consumosBasicos->where('dias_antiguedad', '>=', 46)
                                                       ->where('dias_antiguedad', '<=', 60);
                }

                $documentos = $documentos->where('dias_antiguedad', '>=', 46)
                                         ->where('dias_antiguedad', '<=', 60);
                break;

            case 'De 61 a 90 Días':
                if ( ! isset($form->filtro_tipo_informe_seleccion_documentos) ) {
                    $honorarios = $honorarios->where('dias_antiguedad', '>=', 61)
                                             ->where('dias_antiguedad', '<=', 90);

                    $fondosFijos = $fondosFijos->where('dias_antiguedad', '>=', 61)
                                               ->where('dias_antiguedad', '<=', 90);

                    // $liquidanAnticipos = $liquidanAnticipos->where('dias_antiguedad', '>=', 61)
                    //                                     ->where('dias_antiguedad', '<=', 90);

                    $consumosBasicos = $consumosBasicos->where('dias_antiguedad', '>=', 61)
                                                       ->where('dias_antiguedad', '<=', 90);
                }
                $documentos = $documentos->where('dias_antiguedad', '>=', 61)
                                         ->where('dias_antiguedad', '<=', 90);
                break;

            case 'De 91 a 120 Días':
                if ( ! isset($form->filtro_tipo_informe_seleccion_documentos) ) {
                    $honorarios = $honorarios->where('dias_antiguedad', '>=', 91)
                                             ->where('dias_antiguedad', '<=', 120);

                    $fondosFijos = $fondosFijos->where('dias_antiguedad', '>=', 91)
                                               ->where('dias_antiguedad', '<=', 120);

                    // $liquidanAnticipos = $liquidanAnticipos->where('dias_antiguedad', '>=', 91)
                    //                                     ->where('dias_antiguedad', '<=', 120);

                    $consumosBasicos = $consumosBasicos->where('dias_antiguedad', '>=', 91)
                                                       ->where('dias_antiguedad', '<=', 120);
                }

                $documentos = $documentos->where('dias_antiguedad', '>=', 91)
                                         ->where('dias_antiguedad', '<=', 120);
                break;

            case 'De 121 a 150 Días':
                if ( ! isset($form->filtro_tipo_informe_seleccion_documentos) ) {
                    $honorarios = $honorarios->where('dias_antiguedad', '>=', 121)
                                             ->where('dias_antiguedad', '<=', 150);

                    $fondosFijos = $fondosFijos->where('dias_antiguedad', '>=', 121)
                                               ->where('dias_antiguedad', '<=', 150);

                    // $liquidanAnticipos = $liquidanAnticipos->where('dias_antiguedad', '>=', 121)
                    //                                     ->where('dias_antiguedad', '<=', 150);

                    $consumosBasicos = $consumosBasicos->where('dias_antiguedad', '>=', 121)
                                                       ->where('dias_antiguedad', '<=', 150);
                }

                $documentos = $documentos->where('dias_antiguedad', '>=', 121)
                                         ->where('dias_antiguedad', '<=', 150);
                break;

            case 'De 151 a 180 Días':
                if ( ! isset($form->filtro_tipo_informe_seleccion_documentos) ) {
                    $honorarios = $honorarios->where('dias_antiguedad', '>=', 151)
                                             ->where('dias_antiguedad', '<=', 180);

                    $fondosFijos = $fondosFijos->where('dias_antiguedad', '>=', 151)
                                               ->where('dias_antiguedad', '<=', 180);

                    // $liquidanAnticipos = $liquidanAnticipos->where('dias_antiguedad', '>=', 151)
                    //                                     ->where('dias_antiguedad', '<=', 180);

                    $consumosBasicos = $consumosBasicos->where('dias_antiguedad', '>=', 151)
                                                       ->where('dias_antiguedad', '<=', 180);
                }

                $documentos = $documentos->where('dias_antiguedad', '>=', 151)
                                         ->where('dias_antiguedad', '<=', 180);
                break;

            case 'De 181 a más Días':
                if ( ! isset($form->filtro_tipo_informe_seleccion_documentos) ) {
                    $honorarios = $honorarios->where('dias_antiguedad', '>=', 181);
                    $fondosFijos = $fondosFijos->where('dias_antiguedad', '>=', 181);
                    // $liquidanAnticipos = $liquidanAnticipos->where('dias_antiguedad', '>=', 181);
                    $consumosBasicos = $consumosBasicos->where('dias_antiguedad', '>=', 181);
                }
                $documentos = $documentos->where('dias_antiguedad', '>=', 181);
                break;
            
            default:
                # code...
                break;
        }

        if ( isset($form->filtro_tipo_informe_seleccion_documentos) ) {
            $documentos = $documentos->WhereIn('id_tipo_informe', $form->filtro_tipo_informe_seleccion_documentos);
            $documentos = $documentos->get();

        } else {

            $honorarios = $honorarios->get();
            $fondosFijos = $fondosFijos->get();
            // $liquidanAnticipos = $liquidanAnticipos->get();
            $consumosBasicos = $consumosBasicos->get();
            
            $documentos = $documentos->whereNotIn('id',
                            array_merge( 
                                $honorarios->pluck('id')->toArray(), 
                                $fondosFijos->pluck('id')->toArray(), 
                                // $liquidanAnticipos->pluck('id')->toArray(), 
                                $consumosBasicos->pluck('id')->toArray() 
                            ) 
                        )->get();

            foreach ( $consumosBasicos as $doc ) {
                if ( $doc->getDeuda() > 0 ) {
                    $data[] = $doc->getDatosDocumentoParaNomina($verDocumento);
                }
            }

            foreach ( $fondosFijos as $doc ) {
                if ( $doc->getDeuda() > 0 ) {
                    $data[] = $doc->getDatosDocumentoParaNomina($verDocumento);
                }
            }

            // foreach ( $liquidanAnticipos as $doc ) {
            //     if ( $doc->getDeuda() > 0 ) {
            //         $data[] = $doc->getDatosDocumentoParaNomina($verDocumento);
            //     }
            // }

            foreach ( $honorarios as $doc ) {
                if ( $doc->getDeuda() > 0 ) {
                    $data[] = $doc->getDatosDocumentoParaNomina($verDocumento);
                }
            }
        }
 
        foreach ( $documentos as $doc ) {
            if ( $doc->getDeuda() > 0 && $doc->estadoDocumentoDeudaFlotante() == 'Cuadrado' ) {
                $data[] = $doc->getDatosDocumentoParaNomina($verDocumento);
            }
        }

        $respuesta = array(
            'data' => $data
        );

        return json_encode($respuesta);

    }

    public function postAgregarDocumentos(Request $request)
    {
        // dd($request->all());
        // Mensajes de los validadores
        $messages = [
            'required'    => 'Debe ingresar el  :attribute',
            'numeric'     => 'El :attribute debe solo contener números',
            'date_format' => 'La :attribute no tiene el formato correcto: día/mes/año',
            'id.required' => 'Debe seleccionar al menos un Documento.'
        ];

        //validador de los input del formulario
        $validator = \Validator::make($request->all(), [
            'id'       => 'required',
            'idNomina' => 'required'
        ], $messages);

        //Si contiene errores se devuelven todos los errores, de lo contrario guarda en la base de datos
        if ( $validator->fails() ) {
            return response()->json($validator->errors(),400);
        } else {

            $nominaPago = NominaPago::findOrFail($request->get('idNomina'));

            $mensajeDocs = '';
            foreach ( $request->get('id') as $idDoc ) {

                $saldoDisponible = $nominaPago->saldo_disponible;
                $sumatoriaNominaPago = $nominaPago->sumatoria_nomina_pago;
                $saldoPago = $nominaPago->saldo_disponible - $nominaPago->sumatoria_nomina_pago;
                    
                $doc = Documento::findOrFail($idDoc);

                if ( ($doc->total_documento_actualizado + $sumatoriaNominaPago) <= $saldoPago ) {

                    $nominaPagoDocumento = new NominaPagoDocumento();
                    $nominaPagoDocumento->id_nomina_pago = $nominaPago->id;
                    $nominaPagoDocumento->id_documento = $doc->id;
                    $nominaPagoDocumento->save();

                    $nominaPago->sumatoria_nomina_pago = $sumatoriaNominaPago + $doc->total_documento_actualizado;
                    $nominaPago->save();
                    $mensajeDocs .= '<br>Doc. '.$doc->numero_documento.' Añadido a la nómina.';

                } else {
                    $mensajeDocs .= '<br>Doc. '.$doc->numero_documento.' No añadido a la nómina.';
                }

            }

            $nominaPago->saldo_pago = $nominaPago->saldo_disponible - $nominaPago->sumatoria_nomina_pago;
            $nominaPago->save();

            $datos = array(
                'estado'               => 'success',
                'mensaje'              => 'Todo correcto.'.$mensajeDocs,
                'sumatoriaNominaPago'  => formatoMiles($nominaPago->sumatoria_nomina_pago),
                'saldoPago'            => formatoMiles($nominaPago->saldo_pago)
            );

        }

        return response()->json($datos,200);

    }

    public function getDocumentosNomina($idNomina)
    {

        $nominasPagosDocumentos = NominaPagoDocumento::where('id_nomina_pago', $idNomina)->get();
                                                     
        $documentos = Documento::with([
                                    'getProveedorFactoring',
                                    'getProveedor',
                                    'getTipoDocumento',
                                    'getComprobanteContableDocumento.getComprobanteContable',
                                    'getReversaComprobante',
                                    'getFoliosSigfe',
                                    'getRelacionRecepcionesNoValidadas'
                                ])
                                ->whereIn('id', $nominasPagosDocumentos->pluck('id_documento')->toArray() )
                                ->get();
                                
        foreach ( $documentos as $documento ) {
            
            $nominaPagoDocumento = NominaPagoDocumento::where('id_nomina_pago', $idNomina)
                                                      ->where('id_documento', $documento->id)
                                                      ->first();

            $documento->id_nomina_pago = $nominaPagoDocumento->id;
            $documento->fechaDevengo = $documento->getFoliosSigfe->first()->fecha_sigfe;
            $documento->añoDevengo = $documento->getFoliosSigfe->first()->year_sigfe;
            $documento->mesDevengo = $documento->getFoliosSigfe->first()->mes_sigfe;
            $documento->diaDevengo = $documento->getFoliosSigfe->first()->dia_sigfe;

        }

        $documentos = $documentos->sortBy('diaDevengo')
                                 ->sortBy('mesDevengo')
                                 ->sortBy('añoDevengo')
                                 ->sortBy('id_proveedor');


        $verDocumento = \Auth::user()->can('ver-documento');
        $data = [];
        if ( $documentos->count() > 0 ) {

            foreach( $documentos as $documento) {
                // $documento->id_nomina_pago = $nominaPagoDocumento->id;
                $data[] = $documento->getDatosDocumentosDeNominaPago($verDocumento);
            }

        }

        $datos = array(
            'data' => $data
        );

        return json_encode($datos);

    }

    public function getModalQuitarDocumento($idDoc)
    {

        $doc = Documento::findOrFail($idDoc);
        return view('nomina_pago.modal_quitar_documento')->with('doc', $doc);

    }

    public function postQuitarDocumento(Request $request)
    {
        // dd($request->all());

        // Mensajes de los validadores
        $messages = [
            'required'    => 'Debe ingresar el  :attribute',
            'numeric'     => 'El :attribute debe solo contener números',
        ];

        //validador de los input del formulario
        $validator = \Validator::make($request->all(), [
            '_id'       => 'required'
        ], $messages);

        //Si contiene errores se devuelven todos los errores, de lo contrario guarda en la base de datos
        if ( $validator->fails() ) {
            return response()->json($validator->errors(),400);
        } else {

            $doc = Documento::findOrFail($request->get('_id'));

            $nominaPagoDocumento = NominaPagoDocumento::where('id_documento', $doc->id)->first();
            $nominaPago = NominaPago::findOrFail($nominaPagoDocumento->id_nomina_pago);

            $nominaPagoDocumento->delete();
            
            $nominaPago->sumatoria_nomina_pago = $nominaPago->sumatoria_nomina_pago - $doc->total_documento_actualizado;
            $nominaPago->saldo_pago = $nominaPago->saldo_disponible - $nominaPago->sumatoria_nomina_pago;
            $nominaPago->save();

            // dd($nomina, $request->all(), 'listoco');

            $datos = array(
                'idDoc'    => $doc->id,
                'estado'   => 'success',
                'mensaje'  => 'Documento quitado de la nómina con éxito.',
                'sumatoriaNominaPago'  => formatoMiles($nominaPago->sumatoria_nomina_pago),
                'saldoPago'            => formatoMiles($nominaPago->saldo_pago)
            );

            return response()->json($datos,200);

        }

    }

    public function getModalEliminarNomina($idNomina)
    {
        
        $nomina = NominaPago::findOrFail($idNomina);
        return view('nomina_pago.modal_eliminar_nomina')->with('nomina', $nomina);

    }

    public function postEliminarNomina(Request $request)
    {
        // dd($request->all());

        $messages = [
            'required'    => 'Debe ingresar el  :attribute',
            'numeric'     => 'El :attribute debe solo contener números',
        ];

        //validador de los input del formulario
        $validator = \Validator::make($request->all(), [
            '_id'       => 'required'
        ], $messages);

        //Si contiene errores se devuelven todos los errores, de lo contrario guarda en la base de datos
        if ( $validator->fails() ) {
            return response()->json($validator->errors(),400);
        } else {

            $nominaPago = NominaPago::with(['getNominasPagosDocumentos'])->findOrFail($request->get('_id'));
            // dd($nominaPago, $nominaPago->getNominasPagosDocumentos->count());

            if ( $nominaPago->getNominasPagosDocumentos->count() > 0 ) {
                foreach ( $nominaPago->getNominasPagosDocumentos as $nominaPagoDocumento ) {
                    $nominaPagoDocumento->delete();
                }
            }

            $nominaPago->delete();

            $datos = array(
                'estado'   => 'success',
                'mensaje'  => 'Nómina de Pago eliminada correctamente.'
            );

            return response()->json($datos,200);

        }
    }

    public function getImprimirNomina($id)
    {
        
        $nominasPagosDocumentos = NominaPagoDocumento::where('id_nomina_pago', $id)->get();
                                                     
        $documentos = Documento::with([
                                    'getProveedorFactoring',
                                    'getProveedor',
                                    'getTipoDocumento',
                                    'getComprobanteContableDocumento.getComprobanteContable',
                                    'getReversaComprobante',
                                    'getFoliosSigfe'
                                ])
                                ->whereIn('id', $nominasPagosDocumentos->pluck('id_documento')->toArray() )
                                ->get();
                                
        foreach ( $documentos as $documento ) {

            $documento->añoDevengo = $documento->getFoliosSigfe->first()->year_sigfe;
            $documento->mesDevengo = $documento->getFoliosSigfe->first()->mes_sigfe;
            $documento->diaDevengo = $documento->getFoliosSigfe->first()->dia_sigfe;

        }

        $documentos = $documentos->sortBy('diaDevengo')
                                 ->sortBy('mesDevengo')
                                 ->sortBy('añoDevengo')
                                 ->sortBy('id_proveedor')
                                 ->values();

        $combinador = new Merger(new TcpdiDriver);
        
        foreach ( $documentos as $key => $documento ) {
            if ( ($key + 1) < $documentos->count() ) {
                concatenarArchivosDocumento::concatenar($documento->id, $combinador, true);
            } else {
                concatenarArchivosDocumento::concatenar($documento->id, $combinador);
            }
        }

        $salida = $combinador->merge();

        foreach ( $documentos as $key => $documento ) {
            concatenarArchivosDocumento::eliminarArchivosDinamicos($documento->id);
        }
    
        header("Content-type:application/pdf");
        header("Content-disposition: inline; filename=nomina_pago.pdf");
        header("content-Transfer-Encoding:binary");
        header("Accept-Ranges:bytes");
        # Imprimir salida luego de encabezados
        echo $salida;

        exit;

    }

}
