<?php namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests;
use Auth;
use App\TipoDocumento;

use Illuminate\Database\QueryException;

class TipoDocumentoController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
    */
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index()
    {
        if (!\Entrust::can(['crear-tipo-documento','editar-tipo-documento','eliminar-tipo-documento','ver-tipo-documento','ver-general-tipo-documento'])) {
            return \Redirect::to('home');
        }
        $tiposDocumento = TipoDocumento::all();
        return view('tipo_documento.index')->with('tiposDocumento',$tiposDocumento);
    }

    public function getModalVerTipoDocumento($idTipoDocumento)
    {
        $tipoDocumento = TipoDocumento::findOrfail($idTipoDocumento);
        return view('tipo_documento.modal_ver_tipo_documento')->with('tipoDocumento',$tipoDocumento);
    }

    public function getModalEditarTipoDocumento($idTipoDocumento)
    {
        $tipoDocumento = TipoDocumento::findOrfail($idTipoDocumento);
        return view('tipo_documento.modal_editar_tipo_documento')->with('tipoDocumento',$tipoDocumento);
    }

    public function postEditarTipoDocumento(Request $request)
    {
        //dd('HOLA',$request->all());
        // Mensajes del validador
        $messages = [
            'required' => 'Debe ingresar el :attribute',
            'sigla.required' => 'Debe ingresar la Sigla',
            'nombre.required' => 'Debe ingresar el Nombre',
            'numeric' => 'El :attribute debe solo contener números',
            'max' => 'El :attribute no debe exeder los :max caracteres',
            'min' => 'El :attribute debe tener minimo :min caracteres',
            'unique' => 'El valor :attribute ya existe en la base de datos',
        ];

        // Reglas del validador
        $rules = [
            'sigla' => 'required|unique:tipos_documento,sigla,'.$request->input('id_tipo_documento').',id,deleted_at,NULL',
            'nombre' => 'required',
        ];

        // Validador de los input del formulario
        $validator = \Validator::make($request->all(), $rules, $messages);

        //Si contiene errores se devuelven todos los errores, de lo contrario guarda en la base de datos
        if ( $validator->fails() ) {
            return response()->json($validator->errors(),400);
        } else {
            $editTipoDocumento = TipoDocumento::findOrFail($request->input('id_tipo_documento'));
            $editTipoDocumento->editTipoDocumento($request);

            $datos = array(
                'mensaje' => 'Edición exitosa del tipo de documento',
                'estado' => 'success',
                'sigla' => $editTipoDocumento->sigla,
                'nombre' => $editTipoDocumento->nombre,
                'id' => $editTipoDocumento->id,
            );

        }

        return response()->json($datos,200);
    }

    public function create()
    {
        return view('tipo_documento.modal_crear_tipo_documento');
    }

    public function store(Request $request)
    {
        //dd('HOLA',$request->all());
        // Mensajes del validador
        $messages = [
            'required' => 'Debe ingresar el :attribute',
            'sigla.required' => 'Debe ingresar la Sigla',
            'nombre.required' => 'Debe ingresar el Nombre',
            'numeric' => 'El :attribute debe solo contener números',
            'max' => 'El :attribute no debe exeder los :max caracteres',
            'min' => 'El :attribute debe tener minimo :min caracteres',
            'unique' => 'El valor :attribute ya existe en la base de datos',
        ];

        // Reglas del validador
        $rules = [
            'sigla' => 'required|unique:tipos_documento,sigla,NULL,NULL,deleted_at,NULL',
            'nombre' => 'required',
        ];

        // Validador de los input del formulario
        $validator = \Validator::make($request->all(), $rules, $messages);

        //Si contiene errores se devuelven todos los errores, de lo contrario guarda en la base de datos
        if ( $validator->fails() ) {
            return response()->json($validator->errors(),400);
        } else {
            
            try {
                $newTipoDocumento = new TipoDocumento($request);
            } catch (QueryException $e) {
                $datos = array(
                    'estado' => 'error',
                    'mensaje' => 'Existen problemas al momento de guardar el Tipo Documento',
                );
                return response()->json($datos,200);
            }

            $datos = array(
                'mensaje' => 'Se ha creado el tipo de documento',
                'estado' => 'success',
                'sigla' => $newTipoDocumento->sigla,
                'nombre' => $newTipoDocumento->nombre,
                'id' => $newTipoDocumento->id,
            );
        }

        return response()->json($datos,200);
    }

    public function getModalEliminarTipoDocumento($idTipoDocumento)
    {
        $tipoDocumento = TipoDocumento::findOrfail($idTipoDocumento);
        return view('tipo_documento.modal_eliminar_tipo_documento')->with('tipoDocumento',$tipoDocumento);
    }

    public function postEliminarTipoDocumento(Request $request)
    {
        $deleteTipoDocumento = TipoDocumento::find($request->input('id_tipo_documento'));
        if ( !is_object($deleteTipoDocumento) ) {
            $datos = array(
                'mensaje' => 'No se encuentra el Tipo Documento en la Base de Datos',
                'estado' => 'error',
            );
        } else {
            $deleteTipoDocumento->id_user_deleted = Auth::user()->id;
            $deleteTipoDocumento->timestamps = false;
            $deleteTipoDocumento->save();
            $deleteTipoDocumento->delete();

            $datos = array(
                'mensaje' => 'Se ha eliminado correctamente el Tipo Documento.',
                'estado' => 'success',
                'id' => $deleteTipoDocumento->id,
            );
        }

        return response()->json($datos,200);
    }
}
