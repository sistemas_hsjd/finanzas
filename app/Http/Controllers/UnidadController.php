<?php namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Unidad;

class UnidadController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
    */
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index()
    {
        // if (!\Entrust::can(['crear-unidad','editar-unidad','eliminar-unidad','ver-unidad','ver-general-unidad'])) {
        //     return \Redirect::to('home');
        // }

        $unidades = Unidad::all();
        return view('unidad.index')->with('unidades',$unidades);
    }

    public function create()
    {
        return view('unidad.modal_crear_unidad');
    }

    public function store(Request $request)
    {
        // Mensajes del validador
        $messages = [
            'required' => 'Debe ingresar el :attribute',
            'nombre.required' => 'Debe ingresar el Nombre',
            'numeric' => 'El :attribute debe solo contener números',
            'max' => 'El :attribute no debe exeder los :max caracteres',
            'min' => 'El :attribute debe tener minimo :min caracteres',
            'unique' => 'El valor :attribute ya existe en la base de datos',
        ];

        // Reglas del validador
        $rules = [
            'nombre' => 'required',
        ];

        // Validador de los input del formulario
        $validator = \Validator::make($request->all(), $rules, $messages);

        //Si contiene errores se devuelven todos los errores, de lo contrario guarda en la base de datos
        if ( $validator->fails() ) {
            return response()->json($validator->errors(),400);
        } else {

            try {

                $unidad = new Unidad;
                $unidad->nombre = trim( $request->input('nombre') );
                $unidad->save();

            } catch (QueryException $e) {

                $datos = array(
                    'estado' => 'error',
                    'mensaje' => 'Existen problemas al momento de guardar la Unidad',
                );
                return response()->json($datos,200);

            }

            $datos = array(
                'mensaje' => 'Se ha creado la Unidad',
                'estado' => 'success',
                'nombre' => $unidad->nombre,
                'id' => $unidad->id,
            );
        }

        return response()->json($datos, 200);
    }

    public function show($id)
    {
        $unidad = Unidad::findOrFail($id);
        return view('unidad.modal_ver_unidad')->with('unidad',$unidad);
    }

    public function getModalEditar($id)
    {
        $unidad = Unidad::findOrFail($id);
        return view('unidad.modal_editar_unidad')->with('unidad',$unidad);
    }

    public function postEditar(Request $request)
    {
        // dd('Holo',$request->all());
        // Mensajes del validador
        $messages = [
            'required' => 'Debe ingresar el :attribute',
            'nombre.required' => 'Debe ingresar el Nombre',
            'numeric' => 'El :attribute debe solo contener números',
            'max' => 'El :attribute no debe exeder los :max caracteres',
            'min' => 'El :attribute debe tener minimo :min caracteres',
            'unique' => 'El valor :attribute ya existe en la base de datos',
        ];

        // Reglas del validador
        $rules = [
            'nombre' => 'required',
        ];
        
        // Validador de los input del formulario
        $validator = \Validator::make($request->all(), $rules, $messages);

        //Si contiene errores se devuelven todos los errores, de lo contrario guarda en la base de datos
        if ( $validator->fails() ) {
            return response()->json($validator->errors(),400);
        } else {
            $unidad = Unidad::findOrFail($request->input('_id'));
            $unidad->nombre = trim( $request->input('nombre') );
            $unidad->save();

            $datos = array(
                'mensaje' => 'Edición exitosa de la Unidad',
                'estado' => 'success',
                'nombre' => $unidad->nombre,
                'id' => $unidad->id,
            );
        }

        return response()->json($datos,200);
    }

    public function getModalEliminar($id)
    {
        $unidad = Unidad::findOrFail($id);
        return view('unidad.modal_eliminar_unidad')->with('unidad',$unidad);
    }

    public function postEliminar(Request $request)
    {
        $unidad = Unidad::find($request->input('_id'));

        if ( !is_object($unidad) ) {

            $datos = array(
                'mensaje' => 'No se encuentra la Unidad en la Base de Datos',
                'estado' => 'error',
            );

        } else {

            $unidad->delete();

            $datos = array(
                'mensaje' => 'Se ha eliminado correctamente la Unidad.',
                'estado' => 'success',
                'id' => $unidad->id,
            );

        }

        return response()->json($datos,200);
    }


}
