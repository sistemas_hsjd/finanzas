<?php namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\CuentaContable;

class CuentaContableController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
    */
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index()
    {
        // if (!\Entrust::can(['crear-cuenta-contable','editar-cuenta-contable','eliminar-cuenta-contable','ver-cuenta-contable','ver-general-cuenta-contable'])) {
        //     return \Redirect::to('home');
        // }

        $cuentas = CuentaContable::all();
        return view('cuenta_contable.index',compact('cuentas'));
    }

    public function create()
    {
        return view('cuenta_contable.modal_crear_cuenta_contable');
    }

    public function store(Request $request)
    {
        // Mensajes del validador
        $messages = [
            'required' => 'Debe ingresar el :attribute',
            'numeric' => 'El :attribute debe solo contener números',
            'max' => 'El :attribute no debe exeder los :max caracteres',
            'min' => 'El :attribute debe tener minimo :min caracteres',
            'unique' => 'El valor :attribute ya existe en la base de datos',
        ];

        // Reglas del validador
        $rules = [
            'codigo' => 'required',
            'glosa'  => 'required',
            'saldo'  => 'required'
        ];

        // Validador de los input del formulario
        $validator = \Validator::make($request->all(), $rules, $messages);

        //Si contiene errores se devuelven todos los errores, de lo contrario guarda en la base de datos
        if ( $validator->fails() ) {
            return response()->json($validator->errors(),400);
        } else {

            try {
                $cuentaContable = new CuentaContable($request);
            } catch (QueryException $e) {
                $datos = array(
                    'estado' => 'error',
                    'mensaje' => 'Existen problemas al momento de guardar la Cuenta Contable.',
                );
                return response()->json($datos,200);
            }

            $datos = array(
                'mensaje' => 'Se ha creado la Cuenta Contable.',
                'estado'  => 'success',
                'codigo'  => $cuentaContable->codigo,
                'glosa'   => $cuentaContable->glosa,
                'saldo'   => formatoMiles($cuentaContable->saldo),
                'id'      => $cuentaContable->id,
            );
        }

        return response()->json($datos,200);
    }

    public function show($id)
    {
        $cuenta = CuentaContable::findOrFail($id);
        return view('cuenta_contable.modal_ver_cuenta_contable',compact('cuenta'));
    }

    public function getModalEditar($id)
    {
        $cuenta = CuentaContable::findOrFail($id);
        return view('cuenta_contable.modal_editar_cuenta_contable',compact('cuenta'));
    }

    public function postEditar(Request $request)
    {
        // dd('Holo',$request->all());
        // Mensajes del validador
        $messages = [
            'required' => 'Debe ingresar el :attribute',
            'numeric' => 'El :attribute debe solo contener números',
            'max' => 'El :attribute no debe exeder los :max caracteres',
            'min' => 'El :attribute debe tener minimo :min caracteres',
            'unique' => 'El valor :attribute ya existe en la base de datos',
        ];

        // Reglas del validador
        $rules = [
            'codigo' => 'required',
            'glosa'  => 'required',
            'saldo'  => 'required'
        ];
        
        // Validador de los input del formulario
        $validator = \Validator::make($request->all(), $rules, $messages);

        //Si contiene errores se devuelven todos los errores, de lo contrario guarda en la base de datos
        if ( $validator->fails() ) {
            return response()->json($validator->errors(),400);
        } else {
            $cuentaContable = CuentaContable::findOrFail($request->input('_id'));
            $cuentaContable->editCuentaContable($request);

            $datos = array(
                'mensaje' => 'Edición exitosa de la Cuenta Contable.',
                'estado'  => 'success',
                'codigo'  => $cuentaContable->codigo,
                'glosa'   => $cuentaContable->glosa,
                'saldo'   => formatoMiles($cuentaContable->saldo),
                'id'      => $cuentaContable->id,
            );
        }

        return response()->json($datos,200);
    }

    public function getModalEliminar($id)
    {
        $cuenta = CuentaContable::findOrFail($id);
        return view('cuenta_contable.modal_eliminar_cuenta_contable', compact('cuenta'));
    }

    public function postEliminar(Request $request)
    {
        $cuentaContable = CuentaContable::find($request->input('_id'));
        if ( !is_object($cuentaContable) ) {
            $datos = array(
                'mensaje' => 'No se encuentra la Cuenta Contable en la Base de Datos',
                'estado'  => 'error',
            );
        } else {
            $cuentaContable->delete();

            $datos = array(
                'mensaje' => 'Se ha eliminado correctamente la Cuenta Contable.',
                'estado'  => 'success',
                'id'      => $cuentaContable->id,
            );
        }

        return response()->json($datos,200);
    }


}
