<?php namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests;
use App\ItemPresupuestario;
use App\Clasificacion;
use Auth;
use Illuminate\Database\QueryException;

class ItemPresupuestarioController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
    */
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index()
    {
        if (!\Entrust::can(['crear-item-presupuestario','editar-item-presupuestario','eliminar-item-presupuestario','ver-item-presupuestario','ver-general-item-presupuestario'])) {
            return \Redirect::to('home');
        }
        $itemsPresupuestarios = ItemPresupuestario::all();
        return view('item_presupuestario.index')->with('itemsPresupuestarios',$itemsPresupuestarios);
    }

    public function getModalVer($idItem)
    {
        $item = ItemPresupuestario::findOrFail($idItem);
        return view('item_presupuestario.modal_ver_item')->with('item',$item);
    }

    public function getModalEditar($idItem)
    {
        $item = ItemPresupuestario::findOrFail($idItem);
        $clasificaciones = Clasificacion::all();
        return view('item_presupuestario.modal_editar_item')->with('item',$item)
                                                            ->with('clasificaciones',$clasificaciones);
    }

    public function postEditar(Request $request)
    {
        //dd('Hola',$request->all());
        // Mensajes del validador
        $messages = [
            'required' => 'Debe ingresar el :attribute',
            'titulo.required' => 'Debe ingresar Título',
            'clasificador_presupuestario.required' => 'Debe ingresar el Clasificador Presupuestario',
            'clasificacion.required' => 'Debe seleccionar una Clasificación',
            'numeric' => 'El :attribute debe solo contener números',
            'max' => 'El :attribute no debe exeder los :max caracteres',
            'min' => 'El :attribute debe tener minimo :min caracteres',
            'unique' => 'El valor :attribute ya existe en la base de datos',
        ];

        // Reglas del validador
        $rules = [
            'titulo' => 'required',
            'clasificador_presupuestario' => 'required',
            'clasificacion' => 'required',
        ];
        
        // Validador de los input del formulario
        $validator = \Validator::make($request->all(), $rules, $messages);

        //Si contiene errores se devuelven todos los errores, de lo contrario guarda en la base de datos
        if ( $validator->fails() ) {
            return response()->json($validator->errors(),400);
        } else {
            $editItem = ItemPresupuestario::findOrFail($request->input('id_item'));
            $editItem->editItemPresupuestario($request);

            $datos = array(
                'mensaje' => 'Edición exitosa del ítem presupuestario',
                'estado' => 'success',
                'titulo' => $editItem->titulo,
                'subtitulo' => $editItem->subtitulo,
                'item' => $editItem->item,
                'asignacion' => $editItem->asignacion,
                'subasignacion' => $editItem->subasignacion,
                'clasificadorPresupuestario' => $editItem->clasificador_presupuestario,
                'nombreClasificacion' => $editItem->getClasificacion->nombre,
                'id' => $editItem->id,
            );
        }

        return response()->json($datos,200);
    }

    public function create()
    {
        $clasificaciones = Clasificacion::all();
        return view('item_presupuestario.modal_crear_item')->with('clasificaciones',$clasificaciones);
    }

    public function store(Request $request)
    {
        // Mensajes del validador
        $messages = [
            'required' => 'Debe ingresar el :attribute',
            'titulo.required' => 'Debe ingresar Título',
            'clasificador_presupuestario.required' => 'Debe ingresar el Clasificador Presupuestario',
            'clasificacion.required' => 'Debe seleccionar una Clasificación',
            'numeric' => 'El :attribute debe solo contener números',
            'max' => 'El :attribute no debe exeder los :max caracteres',
            'min' => 'El :attribute debe tener minimo :min caracteres',
            'unique' => 'El valor :attribute ya existe en la base de datos',
        ];

        // Reglas del validador
        $rules = [
            'titulo' => 'required',
            'clasificador_presupuestario' => 'required',
            'clasificacion' => 'required',
        ];

        // Validador de los input del formulario
        $validator = \Validator::make($request->all(), $rules, $messages);

        //Si contiene errores se devuelven todos los errores, de lo contrario guarda en la base de datos
        if ( $validator->fails() ) {
            return response()->json($validator->errors(),400);
        } else {

            try {
                $newItem = new ItemPresupuestario($request);
            } catch (QueryException $e) {
                $datos = array(
                    'estado' => 'error',
                    'mensaje' => 'Existen problemas al momento de guardar el Ítem Presupuestario',
                );
                return response()->json($datos,200);
            }

            $datos = array(
                'mensaje' => 'Se ha creado el ítem presupuestario',
                'estado' => 'success',
                'titulo' => $newItem->titulo,
                'subtitulo' => $newItem->subtitulo,
                'item' => $newItem->item,
                'asignacion' => $newItem->asignacion,
                'subasignacion' => $newItem->subasignacion,
                'clasificadorPresupuestario' => $newItem->clasificador_presupuestario,
                'nombreClasificacion' => $newItem->getClasificacion->nombre,
                'id' => $newItem->id,
            );
        }

        return response()->json($datos,200);
    }

    public function getModalEliminar($idItem)
    {
        $item = ItemPresupuestario::findOrFail($idItem);
        return view('item_presupuestario.modal_eliminar_item')->with('item',$item);
    }

    public function postEliminar(Request $request)
    {
        $deleteItem = ItemPresupuestario::find($request->input('id_item'));
        if ( !is_object($deleteItem) ) {
            $datos = array(
                'mensaje' => 'No se encuentra el Ítem Presupuestario en la Base de Datos',
                'estado' => 'error',
            );
        } else {
            $deleteItem->id_user_deleted = Auth::user()->id;
            $deleteItem->timestamps = false;
            $deleteItem->save();
            $deleteItem->delete();

            $datos = array(
                'mensaje' => 'Se ha eliminado correctamente el Ítem Presupuestario.',
                'estado' => 'success',
                'id' => $deleteItem->id,
            );
        }

        return response()->json($datos,200);
    }

    public function getUpload()
    {
        return view('item_presupuestario.index_masivo');
    }

    public function postUpload(Request $request)
    {
        set_time_limit ( 0 );
        ini_set('max_execution_time', 0);

        $archivo = $request->file('archivo');
        $excel = \Excel::selectSheetsByIndex(0)->load($archivo)->get();

        $log = '';
        // $archivoLog = fopen("log-item-presupuestario-upload.txt", "w");

        foreach ($excel as $key => $fila) {
            // dd('holo',$fila);

            // se crea el item
            $newItem = new ItemPresupuestario();
            $newItem->id = $fila->id;
            $newItem->titulo = trim($fila->cd_titulo);
            $newItem->subtitulo = trim($fila->cd_subtitulo);
            $newItem->item = trim($fila->cd_item);

            if ($fila->cd_asignacion == 'NULL' || $fila->cd_asignacion == '') {
                $newItem->asignacion = null;
            } else {
                $newItem->asignacion = trim($fila->cd_asignacion);
            }

            if ($fila->cd_subasignacion == 'NULL' || $fila->cd_subasignacion == '') {
                $newItem->subasignacion = null;
            } else {
                $newItem->subasignacion = trim($fila->cd_subasignacion);
            }

            $newItem->clasificador_presupuestario = trim($fila->gl_clasificador_presupuestario);

            if ($fila->id_clasificacion == 'NULL' || $fila->id_clasificacion == 0) {
                $newItem->id_clasificacion = null;
            } else {
                $newItem->id_clasificacion = trim($fila->id_clasificacion);
            }

            $newItem->save();

            $log = ($key + 1).'.- Se registro correctamente El Item : id '.$fila->id.' .'.PHP_EOL;
            // fwrite($archivoLog, $log);

        }

        // fclose($archivoLog);
        $datos = array(
            'mensaje' => 'Carga de items exitoso.',
            'estado' => 'success',
        );

        return response()->json($datos,200);
    }

    /**
     * Funcion que muestra un texto, el que se copia en la seed.
     * Para que funcione es necesario cambiar a la base de datos de finanzas (Esta con el nombre de "factura" en la BD)
     * y cambiar la tabla de los modelos de itemPresupuestario y usario
     */
    public function getSeeds()
    {
        
        $items = ItemPresupuestario::all();
        $string = '';
        foreach($items as $key => $item){
            if ($item->id_clasificacion != null && $item->id_clasificacion != 0) {
                $string .= PHP_EOL.'$newItem = new ItemPresupuestario();'.PHP_EOL;
                if ($item->cd_titulo != null && $item->cd_titulo != '') {
                    $string .= '$newItem->titulo = "'.trim($item->cd_titulo).'";'.PHP_EOL;
                }

                if ($item->cd_subtitulo != null && $item->cd_subtitulo != '') {
                    $string .= '$newItem->subtitulo = "'.trim($item->cd_subtitulo).'";'.PHP_EOL;
                }

                if ($item->cd_item != null && $item->cd_item != '') {
                    $string .= '$newItem->item = "'.trim($item->cd_item).'";'.PHP_EOL;
                }

                if ($item->cd_asignacion != null && $item->cd_asignacion != '') {
                    $string .= '$newItem->asignacion = "'.trim($item->cd_asignacion).'";'.PHP_EOL;
                }

                if ($item->cd_subasignacion != null && $item->cd_subasignacion != '') {
                    $string .= '$newItem->subasignacion = "'.trim($item->cd_subasignacion).'";'.PHP_EOL;
                }

                if ($item->gl_clasificador_presupuestario != null && $item->gl_clasificador_presupuestario != '') {
                    $string .= '$newItem->clasificador_presupuestario = "'.trim($item->gl_clasificador_presupuestario).'";'.PHP_EOL;
                }

                $string .= '$newItem->id_clasificacion = "'.trim($item->id_clasificacion).'";'.PHP_EOL;

                $string .= '$newItem->save();'.PHP_EOL;      
            }
                 
        }
        
        return "<textarea row='100000'>$string</textarea>";
    }

    /**
     * Funcion que muestra un texto, el que se copia en la seed.
     * Para que funcione es necesario cambiar a la base de datos de finanzas (Esta con el nombre de "factura" en la BD)
     * y cambiar la tabla de los modelos de clasificacion y usario
     */
    public function getSeedsClasificacion()
    {
        
        $clasificaciones = Clasificacion::all();
        $string = '';
        foreach($clasificaciones as $key => $clasificacion){
            
            $string .= PHP_EOL.'$newClasificacion = new Clasificacion();'.PHP_EOL;
            $string .= '$newClasificacion->nombre = "'.trim($clasificacion->gl_nombre).'";'.PHP_EOL;
            $string .= '$newClasificacion->save();'.PHP_EOL;      
            
                 
        }
        
        return "<textarea row='100000'>$string</textarea>";
    }
}
