<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Profesion;

class ProfesionController extends Controller
{
     /**
     * Create a new controller instance.
     *
     * @return void
    */
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index()
    {
        // if (!\Entrust::can(['crear-profesion','editar-profesion','eliminar-profesion','ver-profesion','ver-general-profesion'])) {
        //     return \Redirect::to('home');
        // }

        $profesiones = Profesion::all();
        return view('profesion.index')->with('profesiones',$profesiones);
    }

    public function create()
    {
        return view('profesion.modal_crear_profesion');
    }

    public function store(Request $request)
    {
        // Mensajes del validador
        $messages = [
            'required' => 'Debe ingresar el :attribute',
            'nombre.required' => 'Debe ingresar el Nombre',
            'numeric' => 'El :attribute debe solo contener números',
            'max' => 'El :attribute no debe exeder los :max caracteres',
            'min' => 'El :attribute debe tener minimo :min caracteres',
            'unique' => 'El valor :attribute ya existe en la base de datos',
        ];

        // Reglas del validador
        $rules = [
            'nombre' => 'required',
        ];

        // Validador de los input del formulario
        $validator = \Validator::make($request->all(), $rules, $messages);

        //Si contiene errores se devuelven todos los errores, de lo contrario guarda en la base de datos
        if ( $validator->fails() ) {
            return response()->json($validator->errors(),400);
        } else {

            try {

                $profesion = new Profesion;
                $profesion->nombre = trim( $request->input('nombre') );
                $profesion->save();

            } catch (QueryException $e) {

                $datos = array(
                    'estado' => 'error',
                    'mensaje' => 'Existen problemas al momento de guardar la Profesión.',
                );
                return response()->json($datos,200);

            }

            $datos = array(
                'mensaje' => 'Se ha creado la Unidad',
                'estado' => 'success',
                'nombre' => $profesion->nombre,
                'id' => $profesion->id,
            );
        }

        return response()->json($datos, 200);
    }

    public function show($id)
    {
        $profesion = Profesion::findOrFail($id);
        return view('profesion.modal_ver_profesion')->with('profesion',$profesion);
    }

    public function getModalEditar($id)
    {
        $profesion = Profesion::findOrFail($id);
        return view('profesion.modal_editar_profesion')->with('profesion',$profesion);
    }

    public function postEditar(Request $request)
    {
        // dd('Holo',$request->all());
        // Mensajes del validador
        $messages = [
            'required' => 'Debe ingresar el :attribute',
            'nombre.required' => 'Debe ingresar el Nombre',
            'numeric' => 'El :attribute debe solo contener números',
            'max' => 'El :attribute no debe exeder los :max caracteres',
            'min' => 'El :attribute debe tener minimo :min caracteres',
            'unique' => 'El valor :attribute ya existe en la base de datos',
        ];

        // Reglas del validador
        $rules = [
            'nombre' => 'required',
        ];
        
        // Validador de los input del formulario
        $validator = \Validator::make($request->all(), $rules, $messages);

        //Si contiene errores se devuelven todos los errores, de lo contrario guarda en la base de datos
        if ( $validator->fails() ) {
            return response()->json($validator->errors(),400);
        } else {
            $profesion = Profesion::findOrFail($request->input('_id'));
            $profesion->nombre = trim( $request->input('nombre') );
            $profesion->save();

            $datos = array(
                'mensaje' => 'Edición exitosa de la Profesión',
                'estado'  => 'success',
                'nombre'  => $profesion->nombre,
                'id'      => $profesion->id,
            );
        }

        return response()->json($datos,200);
    }

    public function getModalEliminar($id)
    {
        $profesion = Profesion::findOrFail($id);
        return view('profesion.modal_eliminar_profesion')->with('profesion',$profesion);
    }

    public function postEliminar(Request $request)
    {
        $profesion = Profesion::find($request->input('_id'));

        if ( !is_object($profesion) ) {

            $datos = array(
                'mensaje' => 'No se encuentra la Profesión en la Base de Datos',
                'estado' => 'error',
            );

        } else {

            $profesion->delete();

            $datos = array(
                'mensaje' => 'Se ha eliminado correctamente la Profesión.',
                'estado' => 'success',
                'id' => $profesion->id,
            );

        }

        return response()->json($datos,200);
    }
}
