<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Documento;
use Carbon\Carbon;
use App\WsDocumento;
use App\ArchivoAcepta;
use App\ComprobanteContableDocumento;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $inicioSemana = Carbon::now()->startOfWeek()->toDateString();

        $indicadores = new \stdClass;
        return view('home');
        
    }

    public function getIndicadores()
    {
        // \Debugbar::disable();
        set_time_limit(0);
        ini_set('max_execution_time', 0);

        $inicioSemana = Carbon::now()->startOfWeek()->toDateString();

        $indicadores = new \stdClass;

        /**
         * Documentos ingresados
         */
        $indicadores->documentosHoy = formatoMiles( Documento::where('fecha_ingreso','>=',date('Y-m-d').' 00:00:00')->count() );

        $indicadores->documentosSemana = formatoMiles( 
                                            Documento::where('fecha_ingreso','>=', $inicioSemana.' 00:00:00')
                                                    ->where('fecha_ingreso','<=', date('Y-m-d').' 23:59:59')
                                                    ->count()
                                                    );

        $indicadores->documentosMes = formatoMiles( Documento::where('fecha_ingreso','>=',date('Y-m').'-01 00:00:00')->count() );

        $indicadores->documentosYear = formatoMiles( Documento::where('fecha_ingreso','>=',date('Y').'-01-01 00:00:00')->count() );

        /**
         * Documentos cuadrados
         */
        $indicadores->cuadradosHoy = formatoMiles( Documento::whereNotIn('id_tipo_documento',[3,9])
                                                ->where('validado', '=', 1)
                                                ->where('fecha_validado','>=',date('Y-m-d').' 00:00:00')
                                                ->count() );

        $indicadores->cuadradosSemana = formatoMiles( Documento::whereNotIn('id_tipo_documento',[3,9])
                                            ->where('validado', '=', 1)
                                            ->where('fecha_validado','>=',date('Y-m-d').' 00:00:00')
                                            ->count() );
                                        
        $indicadores->cuadradosMes = formatoMiles( Documento::whereNotIn('id_tipo_documento',[3,9])
                                            ->where('validado', '=', 1)
                                            ->where('fecha_validado','>=',date('Y-m-d').' 00:00:00')
                                            ->count() );

        $indicadores->cuadradosYear = formatoMiles( Documento::whereNotIn('id_tipo_documento',[3,9])
                                            ->where('validado', '=', 1)
                                            ->where('fecha_validado','>=',date('Y-m-d').' 00:00:00')
                                            ->count() );

        /**
         * Documentos devengados
         */

        $indicadores->devengadosHoy = formatoMiles( Documento::where('id_devengador','<>',null)
                                                                    ->whereHas('getFoliosSigfe', function ($query) {
                                                                    $query->where('fecha_sigfe', '=', date('Y-m-d'));
                                                                    })
                                                                    ->count() );
        
        $indicadores->devengadosSemana = formatoMiles( Documento::where('id_devengador','<>',null)
                                                                ->whereHas('getFoliosSigfe', function ($query) use ($inicioSemana) {
                                                                    $query->where('fecha_sigfe', '>=', $inicioSemana);
                                                                    })
                                                                ->count() );

        $indicadores->devengadosMes = formatoMiles( Documento::where('id_devengador','<>',null)
                                                                ->whereHas('getFoliosSigfe', function ($query) {
                                                                    $query->where('fecha_sigfe','>=',date('Y-m').'-01');
                                                                })
                                                                ->count() );

        $indicadores->devengadosYear = formatoMiles( Documento::where('id_devengador','<>',null)
                                                                ->whereHas('getFoliosSigfe', function ($query) {
                                                                    $query->where('fecha_sigfe','>=',date('Y').'-01-01');
                                                                })
                                                                ->count() );

        /**
         * Archivo acepta por ingresar 2019
         */
        $indicadores->documentosAceptaInputarAnhio2019 = formatoMiles( ArchivoAcepta::where('cargado', 0)
                                                                                ->where('emision','>=','2019-06-22 00:00:00')
                                                                                ->where('emision','<=','2019-12-31 23:59:59')
                                                                                ->where('id_user_rechazo', null)
                                                                                ->where('reclamado', null)
                                                                                ->count() );

        $indicadores->montoSuma2019 = ArchivoAcepta::where('cargado', 0)
                                                ->where('id_user_rechazo', null)
                                                ->where('emision','>=','2019-06-22 00:00:00')
                                                ->where('emision','<=','2019-12-31 23:59:59')
                                                ->whereIn('id_tipo_documento', [1,2,3,5,7,9])
                                                ->select('monto_total', 'id_tipo_documento')
                                                ->where('reclamado', null)
                                                ->sum('monto_total');

        $indicadores->montoResta2019 = ArchivoAcepta::where('cargado', 0)
                                                ->where('id_user_rechazo', null)
                                                ->where('emision','>=','2019-06-22 00:00:00')
                                                ->where('emision','<=','2019-12-31 23:59:59')
                                                ->where('id_tipo_documento', 4)
                                                ->select('monto_total', 'id_tipo_documento')
                                                ->where('reclamado', null)
                                                ->sum('monto_total');

        $indicadores->documentosAceptaInputarAnhioMonto2019 = formatoMiles ( $indicadores->montoSuma2019 - $indicadores->montoResta2019 );

        /**
         * Devengo 2019
         */
        $documentosPorDevengar2019 = Documento::with(['getDevengos'])
                                                ->doesntHave('getFoliosSigfe')
                                                ->where('fecha_recepcion' , '>=', '2019-06-22')
                                                ->where('fecha_recepcion' , '<=', '2019-12-31')
                                                ->where('total_documento', '>', 0)
                                                ->where('reclamado', null)
                                                ->get();

        $totalPorDevengar2019 = 0;
        foreach ( $documentosPorDevengar2019 as $index => $docPorDevengar ) {
            $totalPorDevengarDocumento = 0;
            if ( $docPorDevengar->getDevengos->count() == 0 ) {
                $totalPorDevengarDocumento += $docPorDevengar->total_documento_actualizado;
            } else {
                foreach ( $docPorDevengar->getDevengos as $devengo) {
                    $totalPorDevengarDocumento += $devengo->monto;
                }
            }

            if ($docPorDevengar->id_tipo_documento == 4 || $docPorDevengar->id_tipo_documento == 10) {
                $totalPorDevengar2019 = $totalPorDevengar2019 - $totalPorDevengarDocumento;
            } else {
                $totalPorDevengar2019 = $totalPorDevengar2019 + $totalPorDevengarDocumento;
            }
        }
        $indicadores->totalPorDevengar2019 = formatoMiles( $totalPorDevengar2019 );
        $indicadores->documentosPorDevengar2019 = formatoMiles( $documentosPorDevengar2019->count() );

        /**
         * Archivo acepta por ingresar 2020
         */
        $indicadores->documentosAceptaInputarAnhio2020 = formatoMiles( ArchivoAcepta::where('cargado', 0)
                                                                                    ->where('emision','>=','2020-01-01 00:00:00')
                                                                                    ->where('emision','<=','2020-12-31 23:59:59')
                                                                                    ->where('id_user_rechazo', null)
                                                                                    ->where('reclamado', null)
                                                                                    ->count() );

        $indicadores->montoSuma2020 = ArchivoAcepta::where('cargado', 0)
                                                ->where('id_user_rechazo', null)
                                                ->where('emision','>=','2020-01-01 00:00:00')
                                                ->where('emision','<=','2020-12-31 23:59:59')
                                                ->whereIn('id_tipo_documento', [1,2,3,5,7,9])
                                                ->select('monto_total', 'id_tipo_documento')
                                                ->where('reclamado', null)
                                                ->sum('monto_total');

        $indicadores->montoResta2020 = ArchivoAcepta::where('cargado', 0)
                                                ->where('id_user_rechazo', null)
                                                ->where('emision','>=','2020-01-01 00:00:00')
                                                ->where('emision','<=','2020-12-31 23:59:59')
                                                ->where('id_tipo_documento', 4)
                                                ->select('monto_total', 'id_tipo_documento')
                                                ->where('reclamado', null)
                                                ->sum('monto_total');

        $indicadores->documentosAceptaInputarAnhioMonto2020 = formatoMiles ( $indicadores->montoSuma2020 - $indicadores->montoResta2020 );

        /**
         * Devengo 2020
         */

        $documentosPorDevengar2020 = Documento::with(['getDevengos'])
                                                ->doesntHave('getFoliosSigfe')
                                                ->where('fecha_recepcion' , '>=', '2020-01-01')
                                                ->where('fecha_recepcion' , '<=', '2020-12-31')
                                                ->where('total_documento', '>', 0)
                                                ->where('reclamado', null)
                                                ->get();

        $totalPorDevengar2020 = 0;
        foreach ( $documentosPorDevengar2020 as $index => $docPorDevengar ) {
            $totalPorDevengarDocumento = 0;
            if ( $docPorDevengar->getDevengos->count() == 0 ) {
                $totalPorDevengarDocumento += $docPorDevengar->total_documento_actualizado;
            } else {
                foreach ( $docPorDevengar->getDevengos as $devengo) {
                    $totalPorDevengarDocumento += $devengo->monto;
                }
            }
            

            if ($docPorDevengar->id_tipo_documento == 4 || $docPorDevengar->id_tipo_documento == 10) {
                $totalPorDevengar2020 = $totalPorDevengar2020 - $totalPorDevengarDocumento;
            } else {
                $totalPorDevengar2020 = $totalPorDevengar2020 + $totalPorDevengarDocumento;
            }
        }

        $indicadores->totalPorDevengar2020 = formatoMiles( $totalPorDevengar2020 );
        $indicadores->documentosPorDevengar2020 = formatoMiles( $documentosPorDevengar2020->count() );

        /**
         * Doc pendientes validar
         */
        $documentosPendientesValidacion = Documento::porValidar()->get();

        $totalPendienteValidacion = 0;
        foreach ( $documentosPendientesValidacion as $index => $docPendienteValidacion ) {
            $totalPorDevengarDocumento = 0;
            foreach ( $docPendienteValidacion->getDevengos as $devengo) {
                $totalPorDevengarDocumento += $devengo->monto;
            }

            if ($docPendienteValidacion->id_tipo_documento == 4 || $docPendienteValidacion->id_tipo_documento == 10) {
                $totalPendienteValidacion = $totalPendienteValidacion - $totalPorDevengarDocumento;
            } else {
                $totalPendienteValidacion = $totalPendienteValidacion + $totalPorDevengarDocumento;
            }
        }

        $indicadores->documentosPendientesValidacion = formatoMiles( $documentosPendientesValidacion->count() );
        $indicadores->totalPendienteValidacion = formatoMiles( $totalPendienteValidacion );

        /**
         * Convenios pendientes de validación
         */
        $conveniosPendientesValidacion = Documento::porValidar()
                                                  ->where('id_tipo_informe', 4)
                                                  ->get();

            
        $totalConvenio = 0;
        foreach ( $conveniosPendientesValidacion as $index => $convenio ) {
            $totalPorDevengarDocumento = 0;
            foreach ( $convenio->getDevengos as $devengo) {
                $totalPorDevengarDocumento += $devengo->monto;
            }

            if ($convenio->id_tipo_documento == 4 || $convenio->id_tipo_documento == 10) {
                $totalConvenio = $totalConvenio - $totalPorDevengarDocumento;
            } else {
                $totalConvenio = $totalConvenio + $totalPorDevengarDocumento;
            }
        }

        $indicadores->conveniosPendientesValidacion = formatoMiles( $conveniosPendientesValidacion->count() );
        $indicadores->totalConveniosPendientesValidacion = formatoMiles( $totalConvenio );

        /**
         * Programas pendientes de validación
         */

        $programasPendientesValidacion = Documento::porValidar()
                                                  ->where('id_tipo_informe', 1)
                                                  ->get();

            
        $totalPrograma = 0;
        foreach ( $programasPendientesValidacion as $index => $programa ) {
            $totalPorDevengarDocumento = 0;
            foreach ( $programa->getDevengos as $devengo) {
                $totalPorDevengarDocumento += $devengo->monto;
            }

            if ($programa->id_tipo_documento == 4 || $programa->id_tipo_documento == 10) {
                $totalPrograma = $totalPrograma - $totalPorDevengarDocumento;
            } else {
                $totalPrograma = $totalPrograma + $totalPorDevengarDocumento;
            }
        }

        $indicadores->programasPendientesValidacion = formatoMiles( $programasPendientesValidacion->count() );
        $indicadores->totalProgramasPendientesValidacion = formatoMiles( $totalPrograma );

        /**
         * Deuda real
         */
        $documentosPorPagar = Documento::with(['getDevengos'])
                                       ->whereIn('id_tipo_documento', [1,2,6,7])
                                       ->where('fecha_ingreso','>=','2019-06-22 00:00:00')
                                       ->where('id_estado', 2)
                                       ->get();

        $indicadores->deudaReal = formatoMiles( $documentosPorPagar->count() );
        $totalPorPagar = 0;
        foreach ( $documentosPorPagar as $index => $docPorDevengar ) {
            $totalPorDevengarDocumento = 0;
            foreach ( $docPorDevengar->getDevengos as $devengo) {
                $totalPorDevengarDocumento += $devengo->monto;
            }

            if ($docPorDevengar->id_tipo_documento == 4 || $docPorDevengar->id_tipo_documento == 10) {
                $totalPorPagar = $totalPorPagar - $totalPorDevengarDocumento;
            } else {
                $totalPorPagar = $totalPorPagar + $totalPorDevengarDocumento;
            }
        }
        
        $indicadores->montoDeudaReal = formatoMiles( $documentosPorPagar->sum('total_documento_actualizado') );

        /**
         * Estimación Deuda
         */

        $documentosPorDevengar = Documento::with(['getDevengos'])
                                          ->doesntHave('getFoliosSigfe')
                                          ->where('fecha_ingreso', '>=' ,'2019-06-22 00:00:00')
                                          ->whereIn('id_tipo_documento', [1,2,6,7])
                                          ->where('total_documento', '>', 0)
                                          ->get();

        $totalPorDevengar = 0;
        foreach ( $documentosPorPagar as $index => $docPorDevengar ) {
            $totalPorDevengarDocumento = 0;
            foreach ( $docPorDevengar->getDevengos as $devengo) {
                $totalPorDevengarDocumento += $devengo->monto;
            }

            if ($docPorDevengar->id_tipo_documento == 4 || $docPorDevengar->id_tipo_documento == 10) {
                $totalPorDevengar = $totalPorDevengar - $totalPorDevengarDocumento;
            } else {
                $totalPorDevengar = $totalPorDevengar + $totalPorDevengarDocumento;
            }
        }

        $indicadores->estimacionDeuda = formatoMiles( $documentosPorPagar->count() + $documentosPorDevengar->count() );
        $indicadores->montoEstimacionDeuda = formatoMiles( $documentosPorPagar->sum('total_documento_actualizado') + $documentosPorDevengar->sum('total_documento_actualizado') );

        /**
         * Pagados
         */

        $documentosPagados = Documento::with(['getDevengos', 'getComprobanteContableDocumento'])
                                      ->where('fecha_ingreso','>=','2019-06-22 00:00:00')
                                      ->where('id_estado', 3)
                                      ->get();

        $totalPagados = 0;

        foreach ( $documentosPorPagar as $index => $docPorDevengar ) {
            $totalPorDevengarDocumento = 0;
            foreach ( $docPorDevengar->getDevengos as $devengo) {
                $totalPorDevengarDocumento += $devengo->monto;
            }

            if ($docPorDevengar->id_tipo_documento == 4 || $docPorDevengar->id_tipo_documento == 10) {
                $totalPorPagar = $totalPorPagar - $totalPorDevengarDocumento;
            } else {
                $totalPorPagar = $totalPorPagar + $totalPorDevengarDocumento;
            }
        }

        $indicadores->pagados = formatoMiles ( $documentosPagados->count() );
        $indicadores->montoPagados = formatoMiles( $documentosPagados->sum('total_documento_actualizado') );

        // return 'Obteniendo indicadores';
        return response()->json($indicadores, 200);
    }

    public function getDeudaReal()
    {

        // $docsPorPagar = Documento::with(['getDevengos'])
        //                                ->whereIn('id_tipo_documento', [1,2,6,7])
        //                                ->where('fecha_ingreso','>=','2018-10-01 00:00:00')
        //                                ->where('id_estado', 2)
        //                                ->get();

        // $totalPorPagar = 0;
        // foreach ( $docsPorPagar as $index => $docPorPagar ) {
        //     $totalPorPagar += $docPorPagar->getDeuda() > 0 ? $docPorPagar->getDeuda() : 0;
        // }

        ini_set('memory_limit','-1');
        set_time_limit ( 0 );
        ini_set('max_execution_time', 0);
        $docs = Documento::deudas()
                        ->where('fecha_ingreso','>=','2018-01-01 00:00:00')
                        ->get();

        $totalDeuda = 0;
        $quitaDeuda = 0;
        foreach ($docs as $key => $doc) {
            
            if ( $doc->getDeuda() > 0 ) {
                // if ( isset($form->filtro_item_presupuestario) ) {
                //     $data[] = $documento->getDatosDeudaFlotante($form->filtro_item_presupuestario, $verDocumento, $problemasDocumento);
                // } else {
                //     $data[] = $documento->getDatosDeudaFlotante(null, $verDocumento, $problemasDocumento);
                // }
                $totalDeuda += $doc->getDeuda();
            } else {
                $quitaDeuda -= $doc->getDeuda();
            }

        }

        return "Deuda desde el 01-01-2018: $ ".formatoMiles( $totalDeuda ). " Le quita a la deuda : $ ".formatoMiles( $quitaDeuda );
        

    }

    public function getRestanDeuda()
    {
        ini_set('memory_limit','-1');
        set_time_limit ( 0 );
        ini_set('max_execution_time', 0);
        $docs = Documento::deudas()
                        ->where('fecha_ingreso','>=','2018-01-01 00:00:00')
                        ->latest()
                        ->take(1500)
                        ->get();

        $totalDeuda = 0;
        $quitaDeuda = 0;
        $conta = 0;
        foreach ($docs as $key => $doc) {
            if ( $conta > 6 ) {
                break;
            }
            if ( $doc->getDeuda() > 0 ) {
                $totalDeuda += $doc->getDeuda();
            } else {
                $quitaDeuda -= $doc->getDeuda();
                if ( $doc->getDeuda() < 0 ) {
                    echo " Documento quita deuda:  ".$doc->numero_documento." Proveedor ".$doc->getProveedor->rut." monto quita : ". $doc->getDeuda()."<br>";
                    $conta++;
                }
            }

        }

        return "Deuda desde el 01-01-2018: $ ".formatoMiles( $totalDeuda ). " Le quita a la deuda : $ ".formatoMiles( $quitaDeuda );

    }
}
