<?php namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Auth;

use App\OrdenCompra;
use App\TipoArchivo;
use App\Contrato;
use App\Archivo;
use App\DocumentoContratoOrdenCompra;
use App\Documento;

class OrdenCompraController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
    */
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index()
    {
        // if (!\Entrust::can(['crear-orden-compra','editar-orden-compra','eliminar-orden-compra','ver-orden-compra','ver-general-orden-compra'])) {
        //     return \Redirect::to('home');
        // }

        
        return view('orden_compra.index');
    }

    public function dataTableIndex(Request $request)
    {
        set_time_limit ( 0 );
        ini_set('max_execution_time', 0);

        $search = $request->get('search')['value'];
        $order_column = $request->get('order')[0]['column'];
        $order_dir = $request->get('order')[0]['dir'];

        $form = construyeFormulario( json_decode($request->get('form')) );
        
        // $fechaInicio = fecha_Y_m_d($form->filtro_fecha_inicio);
        // $fechaTermino = fecha_Y_m_d($form->filtro_fecha_termino);

        $ordenes = OrdenCompra::with(['getArchivos'])->get();

        $data = [];

        foreach ( $ordenes as $orden ) {
            $data[] = $orden->getDatosIndex();
        }

        $respuesta = array(
            "data" => $data
        );

        return json_encode($respuesta);

    }

    public function create()
    {
        $tiposArchivo = TipoArchivo::all();
        
        return view('orden_compra.modal_crear')
               ->with('tiposArchivo', $tiposArchivo);
    }

    public function store(Request $request)
    {
        // dd('creando OC', $request->all());

        //mensajes de los validadores
        $messages = [
            'required'    => 'Debe ingresar el :attribute',
            'numeric'     => 'El :attribute debe solo contener números',
            'max' => 'El :attribute no debe exeder los :max caracteres',
            'min' => 'El :attribute debe tener minimo :min caracteres',
            'date_format'        => 'La :attribute no tiene el formato correcto: día/mes/año',
            'reemplaza.required' => 'Debe seleccionar una factura'
        ];


        //validador de los input del formulario
        $validator = \Validator::make($request->all(), [
            'proveedor' => 'required|numeric',
            'numero_oc' => 'required',
            'fecha_oc'  => 'required|date_format:"d/m/Y"',
            'inicio_vigencia' => 'required|date_format:"d/m/Y"',
            'fin_vigencia'    => 'required|date_format:"d/m/Y"',
            'monto_oc'   => 'required',
            'detalle_oc' => 'required'
        ], $messages);

        //Si contiene errores se devuelven todos los errores, de lo contrario guarda en la base de datos
        if ( $validator->fails() ) {

            return response()->json($validator->errors(), 400);

        } else {

            $datos = array();

            $oc = OrdenCompra::where('numero_oc', $request->input('numero_oc'))
                             ->where('fecha_oc', '>=', fecha_Y($request->input('fecha_oc')).'-01-01' )
                             ->where('fecha_oc', '<=', fecha_Y($request->input('fecha_oc')).'-12-31' )
                             ->first();

            if ( is_object($oc) ) {

                $datos['mensaje'] = 'El Número OC ya se encuentra en el sistema.';
                $datos['estado'] = 'error';

            } else {

                $oc = new OrdenCompra;

                $oc->id_proveedor = $request->input('proveedor');
                $oc->id_contrato = ( $request->input('contrato') ) ? $request->input('contrato') : null;

                $oc->numero_oc = $request->input('numero_oc');
                $oc->fecha_oc = fecha_Y_m_d( $request->input('fecha_oc') );
                $oc->inicio_vigencia = fecha_Y_m_d( $request->input('inicio_vigencia') );
                $oc->fin_vigencia = fecha_Y_m_d( $request->input('fin_vigencia') );

                $oc->monto_oc = formato_entero( $request->input('monto_oc') );
                $oc->saldo_oc = $oc->monto_oc;
                $oc->detalle_oc = trim( $request->input('detalle_oc') );

                $oc->id_user_created = Auth::user()->id;
                $oc->save();

                $archivo = $request->file('archivo');
                if ( $archivo != null ) {

                    $nombreOriginalArchivo = str_replace('.pdf','',$archivo->getClientOriginalName() );
                    $arreglo_nombre = explode(".", $archivo->getClientOriginalName() );
                    $extension = strtolower( end( $arreglo_nombre ) );

                    if ($extension != 'pdf') {

                        $datos['mensaje_archivo'] = 'Extension incorrecta del archivo';
                        $datos['estado_archivo'] = 'error';

                    } else {

                        // Se trabajan las rutas del archivo, se guarda en el proyecto al final de la funcion
                        $nombre = $nombreOriginalArchivo.'_'.date('Y_m_d_G_i_s');
                        $rutaArchivo = '/orden_compra/'.$oc->id.'/'.$nombre.'.pdf';
                        $pathArchivo = public_path().'/orden_compra/'.$oc->id.'/';

                        $archivoOC = new Archivo();
                        $archivoOC->id_orden_compra = $oc->id;
                        $archivoOC->id_tipo_archivo = $request->input('tipo_archivo');

                        $archivoOC->nombre = $nombre;
                        $archivoOC->nombre_original = $nombreOriginalArchivo;
                        $archivoOC->ubicacion = "orden_compra/".$oc->id."/".$archivoOC->nombre.".pdf";
                        $archivoOC->extension = "pdf";
                        $archivoOC->peso = $archivo->getSize();
                        $archivoOC->save();

                        $datos['mensaje_archivo'] = 'Se ha guardado correctamente el archivo.';
                        $datos['estado_archivo'] = 'success';

                        $archivo->move( $pathArchivo , $nombre.'.pdf' ); // Se guarda el archivo

                    }
                    
                } else {
                    
                    $datos['mensaje_archivo'] = '';
                    $datos['estado_archivo'] = '';

                }

                $mensajeOc = '';
                if ( $request->input('tipo_archivo') != 3 ) {
                    $this->obtenerPdfOcMercadoPublico($oc, $mensajeOc);
                }

                $datos['mensaje'] = 'Se ha guardado correctamente la Orden de Compra.';
                $datos['estado'] = 'success';

                $datos['mensaje'] .= $mensajeOc != '' ? '<br>'.$mensajeOc : $mensajeOc;

            }

        }

        return response()->json($datos, 200);

    }

    public function getModalEditar($id)
    {
        $oc = OrdenCompra::with(['getDocumentosOrdenCompra.getDocumento'])->findOrFail($id);
        $tiposArchivo = TipoArchivo::all();

        if ( $oc->getContrato ) {

            $oc->montoMaximoOC = $oc->getContrato->monto_contrato - $oc->getContrato->getOrdenesCompra->sum('monto_oc');
            if ( $oc->montoMaximoOC == 0 ) {

                $oc->montoMaximoOC = $oc->monto_oc;

            }

        } else {

            $oc->montoMaximoOC = 'Sin Contrato';

        }
        
        return view('orden_compra.modal_editar')
               ->with('tiposArchivo', $tiposArchivo)
               ->with('oc', $oc);
    }

    public function postEditar(Request $request)
    {
        // dd('editando la oc', $request->all());

        //mensajes de los validadores
        $messages = [
            'required'    => 'Debe ingresar el :attribute',
            'numeric'     => 'El :attribute debe solo contener números',
            'max' => 'El :attribute no debe exeder los :max caracteres',
            'min' => 'El :attribute debe tener minimo :min caracteres',
            'date_format'        => 'La :attribute no tiene el formato correcto: día/mes/año',
            'reemplaza.required' => 'Debe seleccionar una factura'
        ];


        //validador de los input del formulario
        $validator = \Validator::make($request->all(), [
            'proveedor' => 'required|numeric',
            'numero_oc' => 'required',
            'fecha_oc'  => 'required|date_format:"d/m/Y"',
            'inicio_vigencia' => 'required|date_format:"d/m/Y"',
            'fin_vigencia'    => 'required|date_format:"d/m/Y"',
            'monto_oc'   => 'required',
            'detalle_oc' => 'required'
        ], $messages);

        //Si contiene errores se devuelven todos los errores, de lo contrario guarda en la base de datos
        if ( $validator->fails() ) {

            return response()->json($validator->errors(), 400);

        } else {

            $datos = array();

            $oc = OrdenCompra::where('numero_oc', $request->input('numero_oc'))
                             ->where('fecha_oc', '>=', fecha_Y($request->input('fecha_oc')).'-01-01' )
                             ->where('fecha_oc', '<=', fecha_Y($request->input('fecha_oc')).'-12-31' )
                             ->where('id', '!=', $request->input('_id') )
                             ->first();

            // Validar que los documentos no quedan fuera de la vigencia de la oc
            $docsDentro = Documento::whereHas('getContratoOrdenCompra', function($query) use ($request) {
                                        $query->where('id_orden_compra', $request->input('_id'));
                                    })
                                    ->whereDate('periodo', '>=' , fecha_Y_m( $request->input('inicio_vigencia') ).'-01' )
                                    ->whereDate('periodo', '<=' , fecha_Y_m_d( $request->input('fin_vigencia') ) )
                                    ->get()->pluck('id')->toArray();

            $docsFuera = Documento::whereHas('getContratoOrdenCompra', function($query) use ($request) {
                                        $query->where('id_orden_compra', $request->input('_id'));
                                    })
                                    ->whereNotIn('id', $docsDentro )
                                    ->get();

            if ( is_object($oc) ) {

                $datos['mensaje'] = 'El Número OC ya se encuentra en el sistema.';
                return response()->json($datos, 400);

            } elseif ( $docsFuera->count() > 0 ) {

                $datos['mensaje'] = 'Los documentos no pueden quedar fuera de la vigencia de la OC.';
                return response()->json($datos, 400);

            } else {

                $oc = OrdenCompra::findOrFail( $request->input('_id') );
                $oc->id_contrato = ( $request->input('contrato') ) ? $request->input('contrato') : null;

                $oc->numero_oc = $request->input('numero_oc');
                $oc->fecha_oc = fecha_Y_m_d( $request->input('fecha_oc') );
                $oc->inicio_vigencia = fecha_Y_m_d( $request->input('inicio_vigencia') );
                $oc->fin_vigencia = fecha_Y_m_d( $request->input('fin_vigencia') );

                /**
                 * Faltan reglas de negocio para validar correctamente los montos al editar.
                 */
                $oc->monto_oc = formato_entero( $request->input('monto_oc') );
                $oc->saldo_oc = $oc->monto_oc - $oc->getDocumentosOrdenCompra->sum('getDocumento.total_documento');
                $oc->detalle_oc = trim( $request->input('detalle_oc') );

                $oc->id_user_updated = Auth::user()->id;
                $oc->save();

                // Eliminar los archivos seleccionados para eliminar
                if ( $request->input('delete_list') ) {
                    foreach ($request->input('delete_list') as $idArchivo) {
                        $archivoDelete = Archivo::findOrfail($idArchivo);
                        $archivoDelete->delete();
                    }
                }

                $archivo = $request->file('archivo');
                if ( $archivo != null ) {

                    $nombreOriginalArchivo = str_replace('.pdf','',$archivo->getClientOriginalName() );
                    $arreglo_nombre = explode(".", $archivo->getClientOriginalName() );
                    $extension = strtolower( end( $arreglo_nombre ) );

                    if ($extension != 'pdf') {

                        $datos['mensaje_archivo'] = 'Extension incorrecta del archivo';
                        $datos['estado_archivo'] = 'error';

                    } else {

                        // Se trabajan las rutas del archivo, se guarda en el proyecto al final de la funcion
                        $nombre = $nombreOriginalArchivo.'_'.date('Y_m_d_G_i_s');
                        $rutaArchivo = '/orden_compra/'.$oc->id.'/'.$nombre.'.pdf';
                        $pathArchivo = public_path().'/orden_compra/'.$oc->id.'/';

                        $archivoOC = new Archivo();
                        $archivoOC->id_orden_compra = $oc->id;
                        $archivoOC->id_tipo_archivo = $request->input('tipo_archivo');

                        $archivoOC->nombre = $nombre;
                        $archivoOC->nombre_original = $nombreOriginalArchivo;
                        $archivoOC->ubicacion = "orden_compra/".$oc->id."/".$archivoOC->nombre.".pdf";
                        $archivoOC->extension = "pdf";
                        $archivoOC->peso = $archivo->getSize();
                        $archivoOC->save();

                        $datos['mensaje_archivo'] = 'Se ha guardado correctamente el archivo.';
                        $datos['estado_archivo'] = 'success';

                        $archivo->move( $pathArchivo , $nombre.'.pdf' ); // Se guarda el archivo

                    }
                    
                } else {
                    
                    $datos['mensaje_archivo'] = '';
                    $datos['estado_archivo'] = '';

                }

                $datos['mensaje'] = 'Se ha editado correctamente la Orden de Compra.';
                $datos['estado'] = 'success';

            }            

        }

        return response()->json($datos, 200);
        
    }

    public function getModalEliminar($id)
    {
        $oc = OrdenCompra::findOrFail($id);
        return view('orden_compra.modal_eliminar')
               ->with('oc', $oc);
    }

    public function postEliminar(Request $request)
    {
        // dd('eliminar oc', $request->all());
        $oc = OrdenCompra::findOrFail( $request->input('_id') );

        if ( ! is_object($oc) ) {

            $datos = array(
                'mensaje' => 'No se encuentra la Orden de Compra en la Base de Datos.',
                'estado'  => 'error',
            );

        } else {

            $oc->delete();

            $datos = array(
                'mensaje' => 'Se ha eliminado correctamente la Orden de Compra.',
                'estado'  => 'success',
            );

        }

        return response()->json($datos,200);
    }

    public function getModalConsumir($id)
    {
        $oc = OrdenCompra::findOrFail($id);
        $contrato = ( $oc->id_contrato != null ) ? Contrato::with([
                                                        'getDocumentosContrato.getDocumento',
                                                        'getUnidad',
                                                        'getProfesion',
                                                        'getCategoria',
                                                        'getTipoPrestacion',
                                                        'getTipoAdjudicacion',
                                                        'getReferenteTecnico'
                                                        ])
                                                        ->findOrFail($oc->id_contrato) : null;

        $documentos = Documento::where('id_proveedor', $oc->id_proveedor)
                               ->doesnthave('getContratoOrdenCompra')
                               ->with(['getTipoDocumento'])
                               ->get();
        
        return view('orden_compra.modal_consumir')
               ->with('oc', $oc)
               ->with('contrato', $contrato)
               ->with('documentos', $documentos);
    }

    public function postConsumir(Request $request)
    {
        // dd('consumiendo oc', $request->all());

        //mensajes de los validadores
        $messages = [
            'required'    => 'Debe ingresar el :attribute',
            'numeric'     => 'El :attribute debe solo contener números',
            'max' => 'El :attribute no debe exeder los :max caracteres',
            'min' => 'El :attribute debe tener minimo :min caracteres',
            'date_format'  => 'La :attribute no tiene el formato correcto: mes/año',
            '_id.required' => 'Debe seleccionar la Orden de Compra'
        ];

        $rules = array(
            '_id'             => 'required',
            'documento'       => 'required',
            'periodo'         => 'required|date_format:"m/Y"',
        );

        //validador de los input del formulario
        $validator = \Validator::make($request->all(), $rules, $messages);

        //Si contiene errores se devuelven todos los errores, de lo contrario guarda en la base de datos
        if ( $validator->fails() ) {

            return response()->json($validator->errors(), 400);

        } else {

            $datos = array();

            $oc = OrdenCompra::findOrFail( $request->input('_id') );
            $documento = Documento::findOrFail( $request->input('documento') );
            $contrato = ( $oc->id_contrato != null ) ? Contrato::with([
                                                                'getDocumentosContrato.getDocumento',
                                                                'getUnidad',
                                                                'getProfesion',
                                                                'getCategoria',
                                                                'getTipoPrestacion',
                                                                'getTipoAdjudicacion',
                                                                'getReferenteTecnico'
                                                                ])
                                                                ->findOrFail($oc->id_contrato) : null;

            
             // Validar con Contrato
            if ( is_object($contrato) ) {

                if ( $documento->id_tipo_documento == 4 || $documento->id_tipo_documento == 10 ) {
                    // NC y CCC

                    if ( $contrato->saldo_preventivo != null || $contrato->saldo_correctivo != null ) {
                        if ( $request->input('montoOcupar') == 'preventivo' && $documento->total_documento + $contrato->saldo_preventivo > $contrato->monto_preventivo ) {

                            $datos['mensaje'] = 'El total del documento más el saldo preventivo no puede superar el monto preventivo.';
                            return response()->json($datos, 400);
                            
                        }
                        if ( $request->input('montoOcupar') == 'correctivo' && $documento->total_documento + $contrato->saldo_correctivo > $contrato->monto_correctivo ) {

                            $datos['mensaje'] = 'El total del documento más el saldo correctivo no puede superar el monto correctivo.';
                            return response()->json($datos, 400);

                        }
                    }

                    if ( $documento->total_documento + $contrato->saldo_contrato > $contrato->monto_contrato ) {

                        $datos['mensaje'] = 'El total del documento más el saldo contrato no puede superar el monto del contrato.';
                        return response()->json($datos, 400);

                    }

                } else {

                    if ( $contrato->saldo_preventivo != null || $contrato->saldo_correctivo != null ) {
                        if ( $request->input('montoOcupar') == 'preventivo' && $documento->total_documento > $contrato->saldo_preventivo ) {

                            $datos['mensaje'] = 'El total del documento no puede superar el saldo preventivo.';
                            return response()->json($datos, 400);

                        }
                        if ( $request->input('montoOcupar') == 'correctivo' && $documento->total_documento > $contrato->saldo_correctivo ) {

                            $datos['mensaje'] = 'El total del documento no puede superar el saldo correctivo.';
                            return response()->json($datos, 400);

                        }
                    }

                    if ( $documento->total_documento > $contrato->saldo_contrato ) {

                        $datos['mensaje'] = 'El total del documento no puede superar el saldo contrato.';
                        return response()->json($datos, 400);

                    }

                }

            }

            // Validar OC
            if ( $documento->id_tipo_documento == 4 || $documento->id_tipo_documento == 10 ) {
                if ( $documento->total_documento + $oc->saldo_oc  > $oc->monto_oc ) {
                    
                    $datos['mensaje'] = 'El total del documento más el saldo de la orden de compra no puede superar el monto de la orden de compra.';
                    return response()->json($datos, 400);

                }
            } else {
                if ( $documento->total_documento > $oc->saldo_oc ) {
                    
                    $datos['mensaje'] = 'El total del documento no puede superar el saldo de la orden de compra.';
                    return response()->json($datos, 400);

                }
            }


            $documento->periodo = fecha_Y_m_d( '01/'.$request->input('periodo') );
            $documento->unirConContratoOrdenCompra($contrato, $oc, $request);
            $documento->save();

            $datos['mensaje'] = 'Se ha consumido correctamente la Orden de Compra.';
            $datos['estado'] = 'success';

        }

        return response()->json($datos, 200);

    }

    /**
     * Funciones Auxiliares
     */

    /**
     * Obtiene todos los contratos del proveedor, para seleccionar en la OC
     */
    public function getContratosProveedor($id)
    {

        $contratos = Contrato::with(['getOrdenesCompra'])
                             ->vigenteConSaldoParaProveedor($id)
                             ->get();

        if ( $contratos->count() > 0 ) {

            $options = '';
            foreach ( $contratos as $contrato ) {

                $montoMaximoOC = $contrato->monto_contrato - $contrato->getOrdenesCompra->sum('monto_oc');
                
                if ( $montoMaximoOC > 0 ) {

                    $options .= PHP_EOL.'<option value="'.$contrato->id.'" ';
                    $options .= ' id="contrato_seleccionado_'.$contrato->id.'" ';
                    $options .= ' data-saldocontrato="'.$contrato->saldo_contrato.'" ';
                    $options .= ' data-montomaximooc="'.$montoMaximoOC.'" ';
                    $options .= ' data-iniciovigenciacontrato="'.fecha_dmY($contrato->inicio_vigencia).'" ';
                    $options .= ' data-finvigenciacontrato="'.fecha_dmY($contrato->fin_vigencia).'" ';
                
                    $options .= ' >'.$contrato->resolucion.'</option>'.PHP_EOL;
                    
                }
                
            }

            $datos = array(
                'options'           => $options,
                'mensaje'           => 'Se encuentran Contratos.',
                'cantidadContratos' => $contratos->count(),
                'estado'            => 'success'
            );

        } else {
            
            $datos = array(
                'mensaje'           => 'No se encuentran Contratos.',
                'cantidadContratos' => 0,
                'estado'            => 'success'
            );

        }
        
        return response()->json($datos, 200);

    }

    /**
     * Obtiene las oc y la informacion del contrato para el ingreso de documentos
     */
    public function getOcParaBoleta($idProveedor, $idContrato)
    {
        $ordenes = OrdenCompra::where('id_proveedor', $idProveedor)
                              ->where('saldo_oc', '>', 0);

        if ( $idContrato != null && $idContrato != 'null' ) {

            $ordenes = $ordenes->where('id_contrato', $idContrato);

        }
        
        $ordenes = $ordenes->get();

        if ( $ordenes->count() > 0 ) {

            $options = '';

            foreach ( $ordenes as $oc ) {
                $options .= $oc->getOpcionesParaSelect();
            }

            $datos = array(
                'options' => $options,
                'mensaje' => 'Todo bien.',
                'estado'  => 'success'
            );

        } else {

            if ( $idContrato != null && $idContrato != 'null'  ) {

                $mensaje = 'No se encuentran Ordenes de Compra disponibles para el Contrato.';

            } else {

                $mensaje = 'No se encuentran Ordenes de Compra disponibles para el Proveedor.';

            }

            $datos = array(
                'mensaje' => $mensaje,
                'estado'  => 'error'
            );

        }

        return response()->json($datos, 200);

    }

    public function getObtenerPdfOc($id)
    {
        $oc = OrdenCompra::findOrFail($id);
        $mensaje = '';
        $datos['estado'] = $this->obtenerPdfOcMercadoPublico($oc, $mensaje) ? 'success' : 'error';
        $datos['mensaje'] = $mensaje;
        
        return response()->json($datos, 200);
    }

    /**
     * Para obtener PDF de OC desde mercado publico
     */
    public function obtenerPdfOcMercadoPublico(OrdenCompra $oc, &$mensaje = '')
    {
        $content = file_get_contents('http://www.mercadopublico.cl/PurchaseOrder/Modules/PO/DetailsPurchaseOrder.aspx?codigooc='.$oc->numero_oc);
        
        if ( strpos( $content,'Número de orden de compra no es válido') === false ) {

            $posicionInicial = strpos($content, 'PDFReport.aspx?qs');
            $content2 = substr($content, $posicionInicial);
            $posicionFinal = strpos($content2, "==");
            $content3 = substr($content2, 0, $posicionFinal);

            $pdfOC = file_get_contents('http://www.mercadopublico.cl/PurchaseOrder/Modules/PO/'.$content3.'==');

            $nombre = $oc->numero_oc.'_'.date('Y_m_d_G_i_s');

            $archivoOC = new Archivo();
            $archivoOC->id_orden_compra = $oc->id;
            $archivoOC->id_tipo_archivo = 3;

            $archivoOC->nombre = $nombre;
            $archivoOC->nombre_original = $oc->numero_oc;
            $archivoOC->ubicacion = "orden_compra/".$oc->id."/".$archivoOC->nombre.".pdf";
            $archivoOC->extension = "pdf";
            $archivoOC->peso = 0;
            $archivoOC->save();

            if ( ! file_exists( public_path().'/orden_compra') ) {
                mkdir(public_path().'/orden_compra', 0775, true);
                chmod(public_path().'/orden_compra', 0775);
            }
            if ( ! file_exists( public_path().'/orden_compra/'.$oc->id) ) {
                mkdir(public_path().'/orden_compra/'.$oc->id, 0775, true);
                chmod(public_path().'/orden_compra/'.$oc->id, 0775);
            }

            file_put_contents( public_path().'/orden_compra/'.$oc->id.'/'.$archivoOC->nombre.'.pdf' , $pdfOC);

            $mensaje .= 'Se ha obtenido el PDF de la OC desde Mercado Público.';
            return true;
        } else {
            $mensaje .= 'Ha ingresado un número de OC no válido, no se ha obtenido el PDF de la OC.';
            return false;
        }
    }

}
