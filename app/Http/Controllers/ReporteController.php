<?php namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Carbon\Carbon;
use App\FolioSigfe;
use App\Documento;

class ReporteController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
    */
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function getLibroDeCompras()
    {
        return view('reportes.libro_de_compras');
    }

    public function postLibroDeCompras(Request $request)
    {
        // dd($request->all());
        set_time_limit ( 0 );
        ini_set('max_execution_time', 0);
        // ini_set('memory_limit','4000M');

        \Excel::create('SIFCON - Libro de Compras del '.$request->filtro_fecha.' | Creado '.date('d_m_Y-G:i:s').'', function($excel) use($request) {

            $mes = fecha_M('01/'.$request->filtro_fecha);
            $year = fecha_Y('01/'.$request->filtro_fecha);

            $foliosSigfe = FolioSigfe::with(['getDevengos','getDevengos.getItemPresupuestario','getDocumento',
                                             'getDocumento.getTipoInforme','getDocumento.getProveedor',
                                             'getDocumento.getTipoDocumento','getDocumento.getModalidadCompra',
                                             'getDocumento.getDevengos','getDocumento.getDevengos.getItemPresupuestario',
                                             'getDocumento.getResponsable','getDocumento.getRelacionRecepcionesNoValidadas'
                                            ])
                                     ->where('mes_sigfe', $mes)
                                     ->where('year_sigfe', $year)
                                     ->get();

            // dd($mes,$year, $foliosSigfe->count());

            // $documentos = $documentos->get();
            
            $excel->sheet('SIGFE´s', function($sheet) use($foliosSigfe, $request, $year, $mes) {
            
            $sheet->row(1, [
                'Estado', 
                'Informe', 
                'Nombre Factoring',
                'Mercado Público',
                'M/Dev',
                'Proveedor',
                'RUT',
                'Tipo Dcto',
                'Numero Documento',
                'Doc. Relacionado',
                'F/Doc',
                'M/Doc',
                'Fecha RD',
                'Mes RD',
                'Item',
                'ID SIGFE',
                'Fecha ID',
                'Devengado',
                'Pagado',
                'Dcto.',
                'Egreso',
                'F/Egreso',
                'Impago',
                'F/Actual',
                'Dias',
                'A 30 Dias',
                'De 31 a 45 Dias',
                'De 46 a 60 Dias',
                'De 61 a 90 Dias',
                'De 91 a 120 Dias',
                'De 121 a 150 Dias',
                'De 151 a 180 Dias',
                'A 181 y Mas',
                'Total',
                'Mecanismo de Adjudicación',
                'Observaciones',
                'Detalle'
            ]);

            $totalDevengadoExcel = 0;
            $cuentaLineas = 1;

            foreach ( $foliosSigfe as $indexAux => $folioSigfe ) {
                $items = '';
                $totalDevengadoPorDocumento = 0;

                foreach ( $folioSigfe->getDevengos as $index => $devengo ) {
                    
                    $cuentaLineas++;
                    $items = $devengo->getItemPresupuestario->codigo();
                    $totalDevengadoPorDocumento = $devengo->monto;

                    // para columna factoring
                    $factoring = '';
                    if ($folioSigfe->getDocumento->getProveedorFactoring) {
                        $factoring .= $folioSigfe->getDocumento->getProveedorFactoring->nombre.' '.$folioSigfe->getDocumento->getProveedorFactoring->rut;
                    }

                    // para columna "devengado"
                    $devengado = '';
                    if ($folioSigfe->getDocumento->id_tipo_documento == 4 || $folioSigfe->getDocumento->id_tipo_documento == 10) {
                        $devengado .= '-'.$totalDevengadoPorDocumento;
                        $totalDevengadoExcel = $totalDevengadoExcel - $totalDevengadoPorDocumento;
                    } else {
                        $devengado .= $totalDevengadoPorDocumento;
                        $totalDevengadoExcel = $totalDevengadoExcel + $totalDevengadoPorDocumento;
                    }

                    // Ocupar el último día de la fecha ingresada.
                    $lastDay = Carbon::createFromDate($year, $mes, 1)->endOfMonth()->format("Y-m-d");
                    $dias = diferencia_dias($folioSigfe->getDocumento->fecha_documento,$lastDay);

                    // Segun la cantidad de dias, el devengado va.

                    $a30dias = '-';
                    $de31a45dias = '-';
                    $de46a60dias = '-';
                    $de61a90dias = '-';
                    $de91a120dias = '-';
                    $de121a150dias = '-';
                    $de151a180dias = '-';
                    $a181yMas = '-';
                    $total = $devengado; // ¿Deberia ir sumando todos los montos segun diferencia de dias...?

                    if ( $dias <= 30 ) {
                        $a30dias = $devengado;
                    } elseif ( $dias >= 31 && $dias <= 45 ) {
                        $de31a45dias = $devengado;
                    } elseif ( $dias >= 46 && $dias <= 60 ) {
                        $de46a60dias = $devengado;
                    } elseif ( $dias >= 61 && $dias <= 90 ) {
                        $de61a90dias = $devengado;
                    } elseif ( $dias >= 91 && $dias <= 120 ) {
                        $de91a120dias = $devengado;
                    } elseif ( $dias >= 121 && $dias <= 150 ) {
                        $de121a150dias = $devengado;
                    } elseif ( $dias >= 151 && $dias <= 180 ) {
                        $de151a180dias = $devengado;
                    } elseif ( $dias <= 181 ) {
                        $a181yMas = $devengado;
                    }



                    // dd($lastDay, $request->filtro_fecha, $folioSigfe->getDocumento->fecha_documento , diferencia_dias($folioSigfe->getDocumento->fecha_documento,$lastDay));
                    // $fechaActual = 

                    $documentoRelacionado = '';
                    if ( $folioSigfe->getDocumento->getDocumentoRelacionado ) {
                        $documentoRelacionado = $folioSigfe->getDocumento->getDocumentoRelacionado->numero_documento;
                    }

                    // Variables que aun no se ocupan
                    $pagado = '';
                    $dcto = '';
                    $egreso = '';
                    $fechaEgreso = '';
                    $impago = '';


                    $sheet->row($cuentaLineas, [
                        str_replace('<br>',' ', $folioSigfe->getDocumento->getEstadoDocumento()),            // Estado
                        $folioSigfe->getDocumento->getTipoInforme->nombre,            // Informe
                        $factoring,                                                 // Nombre Factoring
                        $folioSigfe->getDocumento->documento_compra,                // Mercado Publico
                        (int)explode("-",$folioSigfe->fecha_sigfe)[1],              // Mes Dev
                        $folioSigfe->getDocumento->getProveedor->nombre,            // Proveedor
                        $folioSigfe->getDocumento->getProveedor->rut,               // RUT
                        $folioSigfe->getDocumento->getTipoDocumento->nombre,        // Tipo Dcto
                        $folioSigfe->getDocumento->numero_documento,                // Numero Documento
                        $documentoRelacionado,                                      // Documento Relacionado
                        fecha_dmY($folioSigfe->getDocumento->fecha_documento),      // F/Doc
                        (int)explode("-",$folioSigfe->getDocumento->fecha_documento)[1], // M/Doc
                        fecha_dmY($folioSigfe->getDocumento->fecha_recepcion),      // Fecha RD
                        (int)explode("-",$folioSigfe->getDocumento->fecha_recepcion)[1], // Mes RD
                        $items,                                                     // Item
                        $folioSigfe->id_sigfe,                                      // ID SIGFE
                        fecha_dmY($folioSigfe->fecha_sigfe),                        // Fecha ID
                        $devengado,                                                 // Devengado
                        $pagado,                                                    // Pagado
                        $dcto,                                                      // Dcto
                        $egreso,                                                    // Egreso
                        $fechaEgreso,                                               // F/Egreso
                        $impago,                                                    // Impago
                        $lastDay,                                                   // F/Actual
                        $dias,                                                      // Dias
                        $a30dias,                                                   // 30 dias
                        $de31a45dias,                                               // 31 a 45 dias
                        $de46a60dias,                                               // 46 a 60 dias
                        $de61a90dias,                                               // 61 a 90 dias
                        $de91a120dias,                                              // 91 a 120 dias
                        $de121a150dias,                                             // 121 a 150 dias
                        $de151a180dias,                                             // 151 a 180 dias
                        $a181yMas,                                                  // 181 dias y +
                        $total,                                                     // total
                        $folioSigfe->getDocumento->getTipoAdjudicacion->nombre,     // Mecanismo de Adjudicacion
                        $folioSigfe->getDocumento->observacion,                     // Observaciones
                        $folioSigfe->detalle_sigfe                                  // Detalle
                    ]);

                }

            }
        
        });
        
        })->export('xlsx');

    }

    public function getLibroDeHonorarios(Request $request)
    {
        return view('reportes.libro_de_honorarios');
    }

    public function postLibroDeHonorarios(Request $request)
    {
        // dd('honorarios',$request->all());
        set_time_limit ( 0 );
        ini_set('max_execution_time', 0);

        \Excel::create('SIFCON - Libro de Honorarios del '.$request->filtro_fecha.' | Creado '.date('d_m_Y-G:i:s').'', function($excel) use($request) {

            $mes = fecha_M('01/'.$request->filtro_fecha);
            $year = fecha_Y('01/'.$request->filtro_fecha);

            $foliosSigfe = FolioSigfe::with(['getDevengos','getDevengos.getItemPresupuestario','getDocumento',
                                             'getDocumento.getTipoInforme','getDocumento.getProveedor',
                                             'getDocumento.getTipoDocumento','getDocumento.getModalidadCompra',
                                             'getDocumento.getDevengos','getDocumento.getDevengos.getItemPresupuestario',
                                             'getDocumento.getResponsable',
                                             'getDocumento.getContratoOrdenCompra',
                                             'getDocumento.getContratoOrdenCompra.getContrato',
                                             'getDocumento.getContratoOrdenCompra.getOrdenCompra',
                                            ])
                                     ->where('mes_sigfe', $mes)
                                     ->where('year_sigfe', $year)
                                     ->whereHas('getDocumento', function($query){
                                         $query->whereIn('id_tipo_documento', [3, 7, 9]);
                                     })
                                     ->get();

            // dd($mes,$year, $foliosSigfe->count());

            // $documentos = $documentos->get();
            
            $excel->sheet('SIGFE´s', function($sheet) use($foliosSigfe, $request, $year, $mes) {
            
            $sheet->row(1, [
                'O/C', 
                'RUT', 
                'Servicio',
                'Apellidos y Nombres',
                'N° Boleta',
                'Emision',
                'Recepcion',
                'Boleta E/M',
                'Total Bruto',
                '10% Impto.',
                'Líquido a Pagar',
                '',
                '',
                '',
                'Folio',
                'Fecha Folio',
                'Mes Devengo',
                'Documento',
                'Egreso',
                'Fecha Egreso',
                'Resolucion N°',
                'Fecha Resol.',
                'Addemdum',
                'Fecha Addemdum',
                'Profesion',
                'Item',
                'Periodo',
                'Tipo de Prestación',
                'Valor Hora',
                'Horas',
                // 'Sociedad Si/No',
                'Mecanismo de Adjudicación',
                'Observacion',
                'Categoria',
                'Sueldo / Horas Medicas'
            ]);

            $totalDevengadoExcel = 0;
            $cuentaLineas = 1;

            foreach ( $foliosSigfe as $indexAux => $folioSigfe ) {
                $items = '';
                $totalDevengadoPorDocumento = 0;

                foreach ( $folioSigfe->getDevengos as $index => $devengo ) {
                    
                    $cuentaLineas++;
                    $items = $devengo->getItemPresupuestario->codigo();
                    $totalDevengadoPorDocumento = $devengo->monto;

                    // para columna "devengado"
                    $devengado = '';
                    if ($folioSigfe->getDocumento->id_tipo_documento == 4 || $folioSigfe->getDocumento->id_tipo_documento == 10) {
                        $devengado .= '-'.$totalDevengadoPorDocumento;
                        $totalDevengadoExcel = $totalDevengadoExcel - $totalDevengadoPorDocumento;
                    } else {
                        $devengado .= $totalDevengadoPorDocumento;
                        $totalDevengadoExcel = $totalDevengadoExcel + $totalDevengadoPorDocumento;
                    }

                    // Ocupar el último día de la fecha ingresada.
                    $lastDay = Carbon::createFromDate($year, $mes, 1)->endOfMonth()->format("Y-m-d");
                    $dias = diferencia_dias($folioSigfe->getDocumento->fecha_documento,$lastDay);
                    
                    // Variables que aun no se ocupan
                    $egreso = ''; // arreglar con tesoreria
                    $fechaEgreso = ''; // arreglar con tesoreria



                    $mecanismoAdjudicacion = '';
                    $servicio = '';
                    $resolucion = '';
                    $fechaResolucion = '';
                    $profesion = '';
                    $tipoPrestacion = '';
                    $valorHora = '';
                    $categoria = '';
                    $sueldoHorasMedicas = '';
                    $addemdum = '';
                    $fechaAddendum = '';
                    if ( $folioSigfe->getDocumento->getContratoOrdenCompra && $folioSigfe->getDocumento->getContratoOrdenCompra->getContrato ) {

                        $mecanismoAdjudicacion = $folioSigfe->getDocumento->getContratoOrdenCompra->getContrato->getTipoAdjudicacion->nombre;
                        $resolucion = $folioSigfe->getDocumento->getContratoOrdenCompra->getContrato->resolucion;
                        $fechaResolucion = fecha_dmY($folioSigfe->getDocumento->getContratoOrdenCompra->getContrato->resolucion->fecha_resolucion);

                        if ( $folioSigfe->getDocumento->getContratoOrdenCompra->getContrato->getUnidad ) {
                            $servicio = $folioSigfe->getDocumento->getContratoOrdenCompra->getContrato->getUnidad->nombre;
                        }

                        if ( $folioSigfe->getDocumento->getContratoOrdenCompra->getContrato->getProfesion ) {
                            $profesion = $folioSigfe->getDocumento->getContratoOrdenCompra->getContrato->getProfesion->nombre;
                        }
                        
                        if ( $folioSigfe->getDocumento->getContratoOrdenCompra->getContrato->getTipoPrestacion ) {
                            $tipoPrestacion = $folioSigfe->getDocumento->getContratoOrdenCompra->getContrato->getTipoPrestacion->nombre;
                        }

                        if ( $folioSigfe->getDocumento->getContratoOrdenCompra->getContrato->valor_hora != null ) {
                            $valorHora = formatoMiles($folioSigfe->getDocumento->getContratoOrdenCompra->getContrato->valor_hora);
                        }

                        
                        if ( $folioSigfe->getDocumento->getContratoOrdenCompra->getContrato->getCategoria ) {
                            $categoria = $folioSigfe->getDocumento->getContratoOrdenCompra->getContrato->getCategoria->nombre;
                        }

                        if ( $folioSigfe->getDocumento->getContratoOrdenCompra->getContrato->sueldo_horas != null ) {
                            $sueldoHorasMedicas = $folioSigfe->getDocumento->getContratoOrdenCompra->getContrato->sueldo_horas;
                        }

                        if ( $folioSigfe->getDocumento->getContratoOrdenCompra->getContrato->addemdum != null ) {
                            $addemdum = $folioSigfe->getDocumento->getContratoOrdenCompra->getContrato->addemdum;
                            $fechaAddendum = fecha_dmY($folioSigfe->getDocumento->getContratoOrdenCompra->getContrato->addemdum_fecha);
                        }

                    } else {

                        $mecanismoAdjudicacion = $folioSigfe->getDocumento->getTipoAdjudicacion->nombre;

                    }

                    $horas = '';
                    if ( $folioSigfe->getDocumento->getContratoOrdenCompra && $folioSigfe->getDocumento->getContratoOrdenCompra->horas_boleta != null ) {
                        $horas = formatoMilesDecimal($folioSigfe->getDocumento->getContratoOrdenCompra->horas_boleta);
                    }

                    $periodo = $folioSigfe->getDocumento->periodo != null ? fechaPeriodoDocSaldoContrato($folioSigfe->getDocumento->periodo) :'';
                    
                    // $sociedad = '';
                    $oc = '';
                    if ( $folioSigfe->getDocumento->getContratoOrdenCompra && $folioSigfe->getDocumento->getContratoOrdenCompra->getOrdenCompra ) {
                        $oc = $folioSigfe->getDocumento->getContratoOrdenCompra->getOrdenCompra->numero_oc;
                    } else {
                        $oc = $folioSigfe->getDocumento->documento_compra;
                    }

                    $sheet->row($cuentaLineas, [
                        $oc,                                                     // OC
                        $folioSigfe->getDocumento->getProveedor->rut,            // Rut
                        $servicio,                                               // Servicio
                        $folioSigfe->getDocumento->getProveedor->nombre,         // Proveedor
                        $folioSigfe->getDocumento->numero_documento,             // N° Boleta
                        fecha_dmY($folioSigfe->getDocumento->fecha_documento),   // Emision
                        fecha_dmY($folioSigfe->getDocumento->fecha_recepcion),   // Recepcion
                        $folioSigfe->getDocumento->getTipoDocumento->nombre,     // Boleta E/M
                        $folioSigfe->getDocumento->total_documento,              // Total Bruto
                        $folioSigfe->getDocumento->impuesto,                     // 10% impuesto
                        $folioSigfe->getDocumento->liquido,                      // Liquido a pagar
                        '',
                        '',
                        '',
                        $folioSigfe->id_sigfe,                                      // Folio
                        fecha_dmY($folioSigfe->fecha_sigfe),                        // Fecha Folio
                        (int)explode("-",$folioSigfe->fecha_sigfe)[1],              // Mes Devengo
                        '',                                                         // Documento
                        $egreso,
                        $fechaEgreso,
                        $resolucion,
                        $fechaResolucion,
                        $addemdum,
                        $fechaAddendum,
                        $profesion,
                        $items,                                                     // Item
                        $periodo,
                        $tipoPrestacion,
                        $valorHora,
                        $horas,
                        // $sociedad,
                        $mecanismoAdjudicacion,
                        $folioSigfe->getDocumento->observacion,
                        $categoria,
                        $sueldoHorasMedicas,
                        
                    ]);

                }

            }
        
        });
        
        })->export('xlsx');

    }

    public function getLibroDeDeudas(Request $request)
    {
        return view('reportes.libro_de_deudas');
    }

    public function postLibroDeDeudas(Request $request)
    {
        // dd('deudas',$request->all());
        set_time_limit ( 0 );
        ini_set('max_execution_time', 0);

        \Excel::create('SIFCON - Libro de Deudas del '.$request->filtro_fecha.' | Creado '.date('d_m_Y-G:i:s').'', function($excel) use($request) {

            $inicio = fecha_Y_m_d('01/'.$request->filtro_fecha_inicio);
            $final = fecha_Y_m_d('31/'.$request->filtro_fecha_final);
            // $mes = fecha_M('01/'.$request->filtro_fecha);
            // $year = fecha_Y('01/'.$request->filtro_fecha);
            // dd('deudas',$request->all(), $inicio, $final);

            $documentos = Documento::deudas()
                                   ->with([
                                       'getDocumentoMotivoProblema',
                                       'getDocumentoMotivoProblema.getMotivoProblema',
                                       'getResponsable',
                                       'getProveedorFactoring',
                                       'getContratoOrdenCompra.getOrdenCompra',
                                       'getDevengos.getFolioSigfe.getDevengos',
                                       'getTipoAdjudicacion',
                                       'getRelacionRecepcionesNoValidadas'
                                    ])
                                   ->where('fecha_recepcion', '>=', $inicio)
                                   ->where('fecha_recepcion', '<=', $final)
                                   ->get();
                                           

            // dd('deudas',$request->all(), $inicio, $final, $documentos);
            
            $excel->sheet('SIGFE´s', function($sheet) use($documentos, $request, $inicio, $final) {
            
            $sheet->row(1, [
                'ESTADO', 
                'PROBLEMA', 
                'RESPONSABLE',
                'FACTORING',
                'MERCADO PUBLICO',
                'M/DEV',
                'PROVEEDOR',
                'RUT',
                'TIPO DOC',
                'NUM DCTO',
                'F/DOC',
                'M/DOC',
                'F/RECEPCION',
                'M/RECEPCION',
                'ITEM',
                'ID SIGFE',
                'F/ID',
                'DEVENGADO',
                'SALDO PENDIENTE DE DEUDA',
                'PAGADO',
                'PAGADOS POR ANTICIPO',
                'DOCUMENTO',
                'EGRESO',
                'F/EGRESO',
                'IMPAGO',
                'F/ACTUAL',
                'DIAS',
                'A 30 DIAS',
                'DE 31 A 45 DIAS',
                'DE 46 A 60 DIAS',
                'DE 61 A 90 DIAS',
                'DE 91 A 120 DIAS',
                'DE 121 A 150 DIAS',
                'DE 151 A 180 DIAS',
                'A 181 Y MAS',
                'TOTAL',
                'MECANISMO DE ADJUDICACION',
                'INFORME',
                'OBSERVACION',
                'DETALLE',
                'SUBTITULO',
                'TESORERIA',
                'AUTORIZA PAGO',
                'ESTADO',
                'NOMINA'
            ]);

            $totalDevengadoExcel = 0;
            $totalFlujoEfectivoExcel = 0;
            $cuentaLineas = 1;

            foreach ( $documentos as $indexAux => $documento ) {
                
                if ( $documento->getDeuda() > 0 ) {

                    $cuentaLineas++;
                    $items = $documento->getDevengos->first()->getItemPresupuestario->codigo();
                    $totalDevengadoPorDocumento = 0;

                    if ( $documento->getDevengos->first()->getFolioSigfe ) {
                        if ($documento->getDevengos->first()->getFolioSigfe->año_sigfe < date('Y') ) {
                            $subtitulo = $documento->getDevengos->last()->getItemPresupuestario->titulo;
                        } else {
                            $subtitulo = $documento->getDevengos->first()->getItemPresupuestario->titulo;
                        }
                        $devengado = $documento->getDevengos->first()->getFolioSigfe->getDevengos->sum('monto');
                    } else {

                        $subtitulo = $documento->getDevengos->first()->getItemPresupuestario->titulo;
                        $devengado = $documento->getDevengos->sum('monto');
                        
                    }
                    
                    $oc = '';
                    if ( $documento->getContratoOrdenCompra && $documento->getContratoOrdenCompra->getOrdenCompra ) {
                        $oc = $documento->getContratoOrdenCompra->getOrdenCompra->numero_oc;
                    } else {
                        $oc = $documento->documento_compra;
                    }

                    $problema = '';
                    foreach ($documento->getDocumentoMotivoProblema as $documentoMotivoProblema) {
                        $problema .= $documentoMotivoProblema->getMotivoProblema->nombre.'|';
                    }

                    $fechaComprobante = $documento->getFechasComprobantes('excel', null);
                    $numeroEgreso = $documento->getNumerosComprobantes('excel', null);

                    $nomina = '';
                    foreach ( $documento->getNominaPagoDocumento as $nomina ) {
                        $nomina .= $nomina->getNominaPago->id.'|';
                    }

                    $sheet->row($cuentaLineas, [
                        $documento->getEstadoDocumento(true), // 'ESTADO', 
                        $problema,                            // 'PROBLEMA',
                        $documento->getResponsable ? $documento->getResponsable->name : '', // 'RESPONSABLE',
                        $documento->getProveedorFactoring ? $documento->getProveedorFactoring->nombre.' '.$documento->getProveedorFactoring->rut : '', // 'FACTORING',
                        $oc, // 'MERCADO PUBLICO',
                        $documento->getDevengos->first()->getFolioSigfe ? $documento->getDevengos->first()->getFolioSigfe->mes_sigfe : '', // 'M/DEV',
                        $documento->getProveedor ? $documento->getProveedor->nombre : '' , // 'PROVEEDOR',
                        $documento->getProveedor ? $documento->getProveedor->rut : '', // 'RUT',
                        $documento->getTipoDocumento ? $documento->getTipoDocumento->nombre : '', // 'TIPO DOC',
                        $documento->numero_documento, // 'NUM DCTO',
                        fecha_dmY($documento->fecha_documento), // 'F/DOC',
                        explode("-",$documento->fecha_documento)[1], // 'M/DOC',
                        fecha_dmY($documento->fecha_recepcion), // 'F/RECEPCION',
                        explode("-",$documento->fecha_recepcion)[1], // 'M/RECEPCION',
                        $items, // 'ITEM',
                        $documento->getDevengos->first()->getFolioSigfe ? $documento->getDevengos->first()->getFolioSigfe->id_sigfe : '',// 'ID SIGFE',
                        $documento->getDevengos->first()->getFolioSigfe ? fecha_dmY($documento->getDevengos->first()->getFolioSigfe->fecha_sigfe)  : '',// 'F/ID',
                        $devengado, // 'DEVENGADO',
                        $documento->getDeuda(), // 'SALDO PENDIENTE DE DEUDA',
                        $documento->getPagado(), // 'PAGADO',
                        '', // 'PAGADOS POR ANTICIPO',
                        '', // 'DOCUMENTO',
                        $numeroEgreso, // 'EGRESO',
                        $fechaComprobante, // 'F/EGRESO',
                        $documento->getDeuda(), // 'IMPAGO',
                        date("d/m/Y"), // 'F/ACTUAL',
                        $documento->dias_antiguedad, // 'DIAS',
                        $documento->dias_antiguedad <= 30 ? $documento->getDeuda() : '-', // 'A 30 DIAS',
                        $documento->dias_antiguedad > 30 && $documento->dias_antiguedad <= 45 ? $documento->getDeuda() : '-', // 'DE 31 A 45 DIAS',
                        $documento->dias_antiguedad > 45 && $documento->dias_antiguedad <= 60 ? $documento->getDeuda() : '-', // 'DE 46 A 60 DIAS',
                        $documento->dias_antiguedad > 60 && $documento->dias_antiguedad <= 90 ? $documento->getDeuda() : '-', // 'DE 61 A 90 DIAS',
                        $documento->dias_antiguedad > 90 && $documento->dias_antiguedad <= 120 ? $documento->getDeuda() : '-', // 'DE 91 A 120 DIAS',
                        $documento->dias_antiguedad > 120 && $documento->dias_antiguedad <= 150 ? $documento->getDeuda() : '-', // 'DE 121 A 150 DIAS',
                        $documento->dias_antiguedad > 150 && $documento->dias_antiguedad <= 180 ? $documento->getDeuda() : '-',// 'DE 151 A 180 DIAS',
                        $documento->dias_antiguedad > 180 ? $documento->getDeuda() : '-',// 'A 181 Y MAS',
                        $documento->getDeuda(),// 'TOTAL',
                        $documento->getTipoAdjudicacion ? $documento->getTipoAdjudicacion->nombre : '', // 'MECANISMO DE ADJUDICACION',
                        $documento->getTipoInforme ? $documento->getTipoInforme->nombre : '', // 'INFORME',
                        $documento->observacion, // 'OBSERVACION',
                        $documento->getDevengos->first()->getFolioSigfe ? $documento->getDevengos->first()->getFolioSigfe->detalle_sigfe : '', // 'DETALLE',
                        $subtitulo,// 'SUBTITULO',
                        '', // 'TESORERIA',
                        '', // 'AUTORIZA PAGO',
                        '', // 'ESTADO',
                        $nomina,// 'NOMINA'
                        
                    ]);

                }

            }
        
        });
        
        })->export('xlsx');

    }

}
