<?php namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests;

use Auth;
// use App\User;
use App\Role;
use App\Permission;

class PermisosController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
    */
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index()
    {
        if (!\Entrust::hasRole(['propietario','administrador'])) {
            return \Redirect::to('home');
        }

        $permisos = Permission::all();

        return view('permiso.index')->with('permisos',$permisos);
    }

    public function getModalEditar($id)
    {
        $permiso = Permission::findOrFail($id);
        return view('permiso.modal_editar_permiso')->with('permiso',$permiso);
    }

    public function postEditar(Request $request)
    {
        //  dd('editar perfil',$request->all());
        // Mensajes del validador
        $messages = [
            'required' => 'Debe ingresar el :attribute',
            'numeric' => 'El :attribute debe solo contener números',
            'max' => 'El :attribute no debe exeder los :max caracteres',
            'min' => 'El :attribute debe tener minimo :min caracteres',
            'unique' => 'El valor :attribute ya existe en la base de datos',
            'date_format' => 'La :attribute no tiene el formato correcto: día/mes/año',
        ];

        // Reglas del validador
        $rules = [
            'descripcion' => 'required',
            'nombre_visual' => 'required',
            'nombre' => 'required',
        ];

        // Validador de los input del formulario
        $validator = \Validator::make($request->all(), $rules, $messages);

        //Si contiene errores se devuelven todos los errores, de lo contrario guarda en la base de datos
        if ( $validator->fails() ) {
            return response()->json($validator->errors(),400);
        } else {

            // dd('todito bien',$request->all());

            $permiso = Permission::findOrFail($request->input('_id'));
            $permiso->display_name = trim($request->input('nombre_visual'));
            $permiso->description = trim($request->input('descripcion'));
            $permiso->save();

            $datos = array(
                'mensaje' => 'Edición exitosa del Permiso '.$permiso->display_name,
                'estado' => 'success',                
                'id' => $permiso->id,
                'displayName' => $permiso->display_name,
                'description' => $permiso->description,
                'name' => $permiso->name,
            );
        }

        return response()->json($datos,200);
    }

    public function getEliminar($id)
    {
        $permiso = Permission::findOrFail($id);
        return view('permiso.modal_eliminar_permiso')->with('permiso',$permiso);
    }

    public function postEliminar(Request $request)
    {
        //  dd('Crear perfil',$request->all());
        // Mensajes del validador
        $messages = [
            'required' => 'Debe ingresar el :attribute',
            'numeric' => 'El :attribute debe solo contener números',
            'max' => 'El :attribute no debe exeder los :max caracteres',
            'min' => 'El :attribute debe tener minimo :min caracteres',
            'unique' => 'El valor :attribute ya existe en la base de datos',
            'date_format' => 'La :attribute no tiene el formato correcto: día/mes/año',
        ];

        // Reglas del validador
        $rules = [
            'nombre_visual' => 'required',
            'nombre' => 'required',
        ];

        // Validador de los input del formulario
        $validator = \Validator::make($request->all(), $rules, $messages);

        //Si contiene errores se devuelven todos los errores, de lo contrario guarda en la base de datos
        if ( $validator->fails() ) {
            return response()->json($validator->errors(),400);
        } else {

            //dd('todito bien',$request->all());
            $permiso = Permission::findOrFail( $request->input('_id') );
            
            //Se eliminan todos los permisos relacionados
            // $permiso->permissions()->sync([]); // Delete relationship data

            $permiso->delete();
            $datos = array(
                'mensaje' => 'Se ha Eiminado exitosamente el Permiso',
                'estado' => 'success',
                'id' => $permiso->id,
            );
            
        }

        return response()->json($datos,200);
    }

    public function getCreate()
    {
        return view('permiso.modal_crear_permiso');
    }

    public function postCreate(Request $request)
    {
         //  dd('editar perfil',$request->all());
        // Mensajes del validador
        $messages = [
            'required' => 'Debe ingresar el :attribute',
            'numeric' => 'El :attribute debe solo contener números',
            'max' => 'El :attribute no debe exeder los :max caracteres',
            'min' => 'El :attribute debe tener minimo :min caracteres',
            'unique' => 'El valor :attribute ya existe en la base de datos',
            'date_format' => 'La :attribute no tiene el formato correcto: día/mes/año',
        ];

        // Reglas del validador
        $rules = [
            'descripcion' => 'required',
            'nombre_visual' => 'required',
            'nombre' => 'required',
        ];

        // Validador de los input del formulario
        $validator = \Validator::make($request->all(), $rules, $messages);

        //Si contiene errores se devuelven todos los errores, de lo contrario guarda en la base de datos
        if ( $validator->fails() ) {
            return response()->json($validator->errors(),400);
        } else {

            // dd('todito bien',$request->all());

            $permisoAux = Permission::where( 'name',trim( $request->input('nombre') ) )->first();
            
            if ( is_object( $permisoAux ) ) {
                $datos = array(
                    'mensaje' => 'Ya existe un Permiso con el nombre "'.$permisoAux->name.'"',
                    'estado' => 'error',
                ); 
            } else {
                $permiso = new Permission();
                $permiso->display_name = trim($request->input('nombre_visual'));
                $permiso->description = trim($request->input('descripcion'));
                $permiso->name = trim($request->input('nombre'));

                $permiso->save();

                $datos = array(
                    'mensaje' => 'Se ha Creado el Permiso '.$permiso->display_name,
                    'estado' => 'success',                
                    'id' => $permiso->id,
                    'displayName' => $permiso->display_name,
                    'description' => $permiso->description,
                    'name' => $permiso->name,
                );
            }

            
        }

        return response()->json($datos,200);
    }
}
