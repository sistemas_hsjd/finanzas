<?php namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests;
use Auth;
use App\Proveedor;
use App\Banco;
use App\TipoCuenta;
use App\WsContrato;

use Illuminate\Database\QueryException;

use Freshwork\ChileanBundle\Rut;

class ProveedorController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
    */
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function test()
    {

        $wsContratos = WsContrato::where('rut_proveedor','NOT LIKE','%-%')->get();
        $countWsContratosArreglados = 0;
        foreach ( $wsContratos as $contrato ) {

            if ( Rut::parse( $contrato->rut_proveedor )->validate() ) {

                $contrato->rut_proveedor = Rut::parse( $contrato->rut_proveedor )->format(Rut::FORMAT_WITH_DASH);
                $contrato->save();
                $countWsContratosArreglados++;

            }

        }

        // dd($wsContratos);
        // dd(Rut::parse('179194058')->validate() , Rut::parse('179194058')->format(Rut::FORMAT_WITH_DASH));

        $proveedores = Proveedor::where('rut', 'LIKE', '0%')
                                ->get();
        
        foreach ( $proveedores as $prov ) {
            $prov->rut = substr($prov->rut, 1);
            $prov->save();
        }
        
        return $proveedores->count().' ws: '.$countWsContratosArreglados;
    }

    public function dataTableIndex(Request $request)
    {
        set_time_limit ( 0 );
        ini_set('max_execution_time', 0);

        $search = $request->get('search')['value'];
        $order_column = $request->get('order')[0]['column'];
        $order_dir = $request->get('order')[0]['dir'];

        if ( $search == '' ) {
            $proveedores = Proveedor::with(['getProveedorMaestro','getProveedoresFusion'])
                                    ->get();
        } else {
            $proveedores = Proveedor::with(['getProveedorMaestro','getProveedoresFusion'])
                                    ->where('rut','LIKE','%'.$search.'%')
                                    ->orWhere('nombre','LIKE','%'.$search.'%')
                                    ->orWhere('email','LIKE','%'.$search.'%')
                                    ->orWhere('contacto','LIKE','%'.$search.'%')
                                    ->get();
        }

        $data = [];

        if ( $request->get('length') != -1 ) {
            $hasta = $request->get('start') + $request->get('length');
        } else {
            // Se quieren ver todos los datos
            $hasta = $request->get('start') + $proveedores->count();
        }

        for( $i = $request->get('start') ; $i < $hasta ; $i++ ) {
            if ( isset($proveedores[$i]) ) {
                $data[] = $proveedores[$i]->getDataForIndex();
            }
        }

        if ( $proveedores->count() > 0 ) {
            
            foreach ($data as $clave => $fila) {
                $columna[$clave] = $fila[$order_column];
            }

            // Se ordenan los datos, segun la columna seleccionada
            if ( $order_dir == 'asc' ) {
                array_multisort($columna, SORT_ASC, $data);
            } elseif ( $order_dir == 'desc' ) {
                array_multisort($columna, SORT_DESC, $data);
            }

        }

        $respuesta = array(
            "draw" => $request->get('draw'),
            "recordsTotal" => $proveedores->count(),
            "recordsFiltered"=> $proveedores->count(),
            "data" => $data
        );

        return json_encode($respuesta);


    }

    public function index()
    {
        if (!\Entrust::can(['crear-proveedor','editar-proveedor','eliminar-proveedor','ver-proveedor','ver-general-proveedor'])) {
            return \Redirect::to('home');
        }
        
        return view('proveedor.index');
    }

    public function getModalVerProveedor($idProveedor)
    {
        $proveedor = Proveedor::with(['getProveedorMaestro','getProveedoresFusion'])
                              ->findOrfail($idProveedor);
        return view('proveedor.modal_ver_proveedor')->with('proveedor',$proveedor);
    }

    public function getModalEditarProveedor($idProveedor)
    {
        $proveedor = Proveedor::with(['getProveedorMaestro','getProveedoresFusion'])
                              ->findOrfail($idProveedor);
        // dd($proveedor->getProveedorMaestro, $proveedor->getProveedoresFusion, $proveedor->getProveedoresFusion->pluck('id')->toArray());
        $bancos = Banco::all();
        $tiposCuenta = TipoCuenta::all();

        $proveedoresParaFusion = Proveedor::whereNotIn('id', [$proveedor->id])
                                          ->where('id_proveedor_maestro', null);

        if ( $proveedor->getProveedoresFusion ) {
            $proveedoresParaFusion = $proveedoresParaFusion->whereNotIn('id', $proveedor->getProveedoresFusion->pluck('id')->toArray());
        }

        $proveedoresParaFusion = $proveedoresParaFusion->get();

        return view('proveedor.modal_editar_proveedor')->with('proveedor', $proveedor)
                                                       ->with('bancos', $bancos)
                                                       ->with('tiposCuenta', $tiposCuenta)
                                                       ->with('proveedoresParaFusion', $proveedoresParaFusion);
    }

    /**
     * Función para guardar la edicion del proveedor
     */
    public function postEditarProveedor(Request $request)
    {
        // dd('editando proveedor', $request->all());
        // Mensajes del validador
        $messages = [
            'required'                     => 'Debe ingresar el :attribute',
            'rut.required'                 => 'Debe ingresar el Rut',
            'nombre_razon_social.required' => 'Debe ingresar el Nombre / Razón Social',
            'numeric'                      => 'El :attribute debe solo contener números',
            'max'                          => 'El :attribute no debe exeder los :max caracteres',
            'min'                          => 'El :attribute debe tener minimo :min caracteres',
            'unique'                       => 'El valor :attribute ya existe en la base de datos',
            'email'                        => 'Debe ingresar un E-mail valido',
            'cl_rut'                       => 'El Rut es incorrecto',
        ];

        // Reglas del validador
        $rules = [
            'rut'                 => 'required|unique:proveedores,rut,'.$request->input("id_proveedor").',id,deleted_at,NULL|cl_rut',
            'nombre_razon_social' => 'required',
            //'email'  => 'email',
        ];

        if ( $request->input('email') != null ) {
            $rules['email'] = 'email';
        }

        // Validador de los input del formulario
        $validator = \Validator::make($request->all(), $rules, $messages);

        //Si contiene errores se devuelven todos los errores, de lo contrario guarda en la base de datos
        if ( $validator->fails() ) {
            return response()->json($validator->errors(),400);
        } else {
            $editProveedor = Proveedor::findOrFail($request->input('id_proveedor'));
            $editProveedor->editProveedor($request);

            
            $transferencia = '';
            if ($editProveedor->transferencia == 0) {
                $transferencia = 'No';
            } else {
                $transferencia = 'Si';
            }

            $activo = '';
            if ($editProveedor->activo == 0) {
                $activo = 'No';
            } else {
                $activo = 'Si';
            }

            $datos = array(
                'mensaje'                => 'Edición exitosa del proveedor',
                'estado'                 => 'success',
                'rutProveedor'           => $editProveedor->rut,
                'nombreProveedor'        => $editProveedor->nombre,
                'emailProveedor'         => $editProveedor->email,
                'contactoProveedor'      => $editProveedor->contacto,
                'transferenciaProveedor' => $transferencia,
                'activoProveedor'        => $activo,
                'idProveedor'            => $editProveedor->id,
            );
        }

        return response()->json($datos,200);
    }

    public function create()
    {
        $bancos = Banco::all();
        $tiposCuenta = TipoCuenta::all();

        $proveedoresParaFusion = Proveedor::where('id_proveedor_maestro', null)->get();

        return view('proveedor.modal_crear_proveedor')->with('bancos', $bancos)
                                                      ->with('tiposCuenta', $tiposCuenta)
                                                      ->with('proveedoresParaFusion', $proveedoresParaFusion);
    }

    public function store(Request $request)
    {

        //dd('store',$request->all());
        $messages = [
            'required' => 'Debe ingresar el :attribute',
            'rut.required' => 'Debe ingresar el Rut',
            'nombre_razon_social.required' => 'Debe ingresar el Nombre / Razón Social',
            'numeric' => 'El :attribute debe solo contener números',
            'max' => 'El :attribute no debe exeder los :max caracteres',
            'min' => 'El :attribute debe tener minimo :min caracteres',
            'unique' => 'El valor :attribute ya existe en la base de datos',
            'email' => 'Debe ingresar un E-mail valido',
            'cl_rut' => 'El Rut es incorrecto',
        ];

        // Reglas del validador
        $rules = [
            'rut' => 'required|unique:proveedores,rut,NULL,NULL,deleted_at,NULL|cl_rut',
            'nombre_razon_social' => 'required',
        ];

        if ( $request->input('email') != null ) {
            $rules['email'] = 'email';
        }

        // Validador de los input del formulario
        $validator = \Validator::make($request->all(), $rules, $messages);

        //Si contiene errores se devuelven todos los errores, de lo contrario guarda en la base de datos
        if ( $validator->fails() ) {
            return response()->json($validator->errors(),400);
        } else {
            // Crear el nuevo proveedor
            try {
                $newProveedor = new Proveedor($request);
            } catch (QueryException $e) {
                $datos = array(
                    'estado' => 'error',
                    'mensaje' => 'Existen problemas al momento de guardar el Proveedor',
                );
                return response()->json($datos,200);
            }
            
            $transferencia = '';
            if ($newProveedor->transferencia == 0) {
                $transferencia = 'No';
            } else {
                $transferencia = 'Si';
            }

            $activo = '';
            if ($newProveedor->activo == 0) {
                $activo = 'No';
            } else {
                $activo = 'Si';
            }

            $datos = array(
                'mensaje' => 'Se ha creado el proveedor',
                'estado' => 'success',
                'rutProveedor' => $newProveedor->rut,
                'nombreProveedor' => $newProveedor->nombre,
                'emailProveedor' => $newProveedor->email,
                'contactoProveedor' => $newProveedor->contacto,
                'transferenciaProveedor' => $transferencia,
                'activoProveedor' => $activo,
                'idProveedor' => $newProveedor->id,
            );
        }

        return response()->json($datos,200);
    }

    public function getModalEliminarProveedor($idProveedor)
    {
        $proveedor = Proveedor::with(['getProveedorMaestro','getProveedoresFusion'])
                              ->findOrFail($idProveedor);
        return view('proveedor.modal_eliminar_proveedor')->with('proveedor',$proveedor);
    }

    public function postEliminarProveedor(Request $request)
    {
        $deleteProveedor = Proveedor::with(['getProveedorMaestro','getProveedoresFusion'])
                                    ->find($request->input('id_proveedor'));
        if ( !is_object($deleteProveedor) ) {
            $datos = array(
                'mensaje' => 'No se encuentra el Proveedor en la Base de Datos',
                'estado' => 'error',
            );
        } else {

            $deleteProveedor->id_user_deleted = Auth::user()->id;
            $deleteProveedor->timestamps = false;
            $deleteProveedor->save();

            $proveedoresFueraFusion = Proveedor::where('id_proveedor_maestro', $deleteProveedor->id)
                                               ->get();

            if ( $proveedoresFueraFusion->count() > 0 ) {
                foreach ( $proveedoresFueraFusion as $provFuera ) {
                    $provFuera->id_proveedor_maestro = null;
                    $provFuera->id_user_updated = Auth::user()->id;
                    $provFuera->save();
                }
            }

            $deleteProveedor->delete();

            $datos = array(
                'mensaje' => 'Se ha eliminado correctamente el Proveedor.',
                'estado' => 'success',
                'idProveedor' => $deleteProveedor->id,
            );
        }

        return response()->json($datos,200);
    }

    public function getUpload()
    {
        return view('proveedor.index_masivo');
    }

    public function postUpload(Request $request)
    {
        set_time_limit ( 0 );
        ini_set('max_execution_time', 0);

        $archivo = $request->file('archivo');
        $excel = \Excel::selectSheetsByIndex(0)->load($archivo)->get();

        $log = '';
        //$archivoLog = fopen("log-proveedores-upload.txt", "w");

        foreach ($excel as $key => $fila) {
            // dd('holo',$fila);

            // se valida que el proveedor no se encuentra en la base de datos
            $proveedor = Proveedor::where('rut',$fila->rut)->first();
            if ( is_object($proveedor) ) {
                //$log = ($key + 1).'.- YA EXISTE El Proveedor : '.$fila->gl_rut.'  .'.PHP_EOL;
                // fwrite($archivoLog, $log);
            } else {
                // se crea el proveedor
                $newProveedor = new Proveedor();
                // $newProveedor->id = $fila->id;
                $newProveedor->nombre = trim($fila->nombre);
                $newProveedor->rut = str_replace('.','',trim($fila->rut));
                // if ($fila->gl_direccion == 'NULL' || $fila->gl_direccion == '') {
                //     $newProveedor->direccion = null;
                // } else {
                //     $newProveedor->direccion = trim($fila->gl_direccion);
                // }
                
                // if ($fila->gl_telefono == 'NULL' || $fila->gl_telefono == '') {
                //     $newProveedor->telefono = null;
                // } else {
                //     $newProveedor->telefono = trim($fila->gl_telefono);
                // }

                // if ($fila->gl_mail == 'NULL' || $fila->gl_mail == '') {
                //     $newProveedor->email = null;
                // } else {
                //     $newProveedor->email = trim($fila->gl_mail);
                // }

                // if ($fila->gl_contacto == 'NULL' || $fila->gl_contacto == '') {
                //     $newProveedor->contacto = null;
                // } else {
                //     $newProveedor->contacto = trim($fila->gl_contacto);
                // }

                // if ($fila->id_banco == 'NULL' || $fila->id_banco == '' || $fila->id_banco == 0) {
                //     $newProveedor->id_banco = null;
                // } else {
                //     $newProveedor->id_banco = trim($fila->id_banco);
                // }

                // if ($fila->id_tipo_cuenta == 'NULL' || $fila->id_tipo_cuenta == '' || $fila->id_tipo_cuenta == 0) {
                //     $newProveedor->id_tipo_cuenta = null;
                // } else {
                //     $newProveedor->id_tipo_cuenta = trim($fila->id_tipo_cuenta);
                // }

                // if ($fila->gl_numero_cuenta == 'NULL' || $fila->gl_numero_cuenta == '') {
                //     $newProveedor->numero_cuenta = null;
                // } else {
                //     $newProveedor->numero_cuenta = trim($fila->gl_numero_cuenta);
                // }

                // $newProveedor->transferencia = $fila->bo_transferencia;
                // $newProveedor->activo = $fila->bo_activo;
                $newProveedor->activo = 1;
                $newProveedor->save();

                // $log = ($key + 1).'.- Se registro correctamente El Proveedor : '.$fila->gl_rut.' '.$fila->gl_nombre.' .'.PHP_EOL;
                //fwrite($archivoLog, $log);

                // if ($fila->bo_eliminado == 1) {;
                //     $newProveedor->delete();
                // }                
                
            }

        }
        // fclose($archivoLog);
        $datos = array(
            'mensaje' => 'Carga de proveedores exitoso.',
            'estado' => 'success',
        );

        return response()->json($datos,200);
    }

    /**
     * Funcion que muestra un texto, el que se copia en la seed.
     * Para que funcione es necesario cambiar a la base de datos de finanzas (Esta con el nombre de "factura" en la BD)
     * y cambiar la tabla de los modelos de proveedor y usario
     */
    // public function getSeeds()
    // {
        
    //     $proveedores = \DB::table('ft_proveedor')->get();
    //     $string = '';
    //     foreach($proveedores as $key => $proveedor){
    //         if ($key > 0 ){
    //             $string .= PHP_EOL.'$newProveedor = new Proveedor();'.PHP_EOL;
    //             $string .= '$newProveedor->nombre = "'.trim($proveedor->gl_nombre).'";'.PHP_EOL;
    //             $string .= '$newProveedor->rut = "'.trim($proveedor->gl_rut).'";'.PHP_EOL;
    //             if ($proveedor->gl_direccion != null && $proveedor->gl_direccion != '') {
    //                 $string .= '$newProveedor->direccion = "'.trim($proveedor->gl_direccion).'";'.PHP_EOL;
    //             }
                
    //             if ($proveedor->gl_telefono != null && $proveedor->gl_direccion != '') {
    //                 $string .= '$newProveedor->telefono = "'.trim($proveedor->gl_telefono).'";'.PHP_EOL;
    //             }

    //             if ($proveedor->gl_mail != null && $proveedor->gl_mail != '') {
    //                 $string .= '$newProveedor->email = "'.trim($proveedor->gl_mail).'";'.PHP_EOL;
    //             }

    //             if ($proveedor->gl_contacto != null && $proveedor->gl_contacto != '') {
    //                 $string .= '$newProveedor->contacto = "'.trim($proveedor->gl_contacto).'";'.PHP_EOL;
    //             }

    //             if ($proveedor->id_banco != null && $proveedor->id_banco != 0) {
    //                 $string .= '$newProveedor->id_banco = '.$proveedor->id_banco.';'.PHP_EOL;
    //             }
                
    //             if ($proveedor->id_tipo_cuenta != null && $proveedor->id_tipo_cuenta != 0) {
    //                 $string .= '$newProveedor->id_tipo_cuenta = '.$proveedor->id_tipo_cuenta.';'.PHP_EOL;
    //             }

    //             if ($proveedor->gl_numero_cuenta != null && $proveedor->gl_numero_cuenta != '') {
    //                 $string .= '$newProveedor->numero_cuenta = "'.trim($proveedor->gl_numero_cuenta).'";'.PHP_EOL;
    //             }

    //             $string .= '$newProveedor->transferencia = '.$proveedor->bo_transferencia.';'.PHP_EOL;
    //             $string .= '$newProveedor->activo = '.$proveedor->bo_activo.';'.PHP_EOL;
    //             // $string .= '$newProveedor->eliminado = '.$proveedor->bo_eliminado.';'.PHP_EOL;
    //             $string .= '$newProveedor->save();'.PHP_EOL;
    //             if ($proveedor->bo_eliminado == 1) {
    //                 $string .= '$newProveedor->delete();'.PHP_EOL;
    //             }
    //         }
            
    //     }
        
    //     return "<textarea row='100000'>$string</textarea>";
    // }
}
