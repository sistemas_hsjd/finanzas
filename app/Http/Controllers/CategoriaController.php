<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Categoria;

class CategoriaController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
    */
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index()
    {
        // if (!\Entrust::can(['crear-categoria','editar-categoria','eliminar-categoria','ver-categoria','ver-general-categoria'])) {
        //     return \Redirect::to('home');
        // }

        $categorias = Categoria::all();
        return view('categoria.index')->with('categorias', $categorias);
    }

    public function create()
    {
        return view('categoria.modal_crear_categoria');
    }

    public function store(Request $request)
    {
        // Mensajes del validador
        $messages = [
            'required' => 'Debe ingresar el :attribute',
            'nombre.required' => 'Debe ingresar el Nombre',
            'numeric' => 'El :attribute debe solo contener números',
            'max' => 'El :attribute no debe exeder los :max caracteres',
            'min' => 'El :attribute debe tener minimo :min caracteres',
            'unique' => 'El valor :attribute ya existe en la base de datos',
        ];

        // Reglas del validador
        $rules = [
            'nombre' => 'required',
        ];

        // Validador de los input del formulario
        $validator = \Validator::make($request->all(), $rules, $messages);

        //Si contiene errores se devuelven todos los errores, de lo contrario guarda en la base de datos
        if ( $validator->fails() ) {
            return response()->json($validator->errors(),400);
        } else {

            try {

                $categoria = new Categoria;
                $categoria->nombre = trim( $request->input('nombre') );
                $categoria->save();

            } catch (QueryException $e) {

                $datos = array(
                    'estado' => 'error',
                    'mensaje' => 'Existen problemas al momento de guardar la Categoría.',
                );
                return response()->json($datos,200);

            }

            $datos = array(
                'mensaje' => 'Se ha creado la Categoría.',
                'estado'  => 'success',
                'nombre'  => $categoria->nombre,
                'id'      => $categoria->id,
            );
        }

        return response()->json($datos, 200);
    }

    public function show($id)
    {
        $categoria = Categoria::findOrFail($id);
        return view('categoria.modal_ver_categoria')->with('categoria', $categoria);
    }

    public function getModalEditar($id)
    {
        $categoria = Categoria::findOrFail($id);
        return view('categoria.modal_editar_categoria')->with('categoria', $categoria);
    }

    public function postEditar(Request $request)
    {
        // dd('Holo',$request->all());
        // Mensajes del validador
        $messages = [
            'required' => 'Debe ingresar el :attribute',
            'nombre.required' => 'Debe ingresar el Nombre',
            'numeric' => 'El :attribute debe solo contener números',
            'max' => 'El :attribute no debe exeder los :max caracteres',
            'min' => 'El :attribute debe tener minimo :min caracteres',
            'unique' => 'El valor :attribute ya existe en la base de datos',
        ];

        // Reglas del validador
        $rules = [
            'nombre' => 'required',
        ];
        
        // Validador de los input del formulario
        $validator = \Validator::make($request->all(), $rules, $messages);

        //Si contiene errores se devuelven todos los errores, de lo contrario guarda en la base de datos
        if ( $validator->fails() ) {
            return response()->json($validator->errors(),400);
        } else {
            $categoria = Categoria::findOrFail($request->input('_id'));
            $categoria->nombre = trim( $request->input('nombre') );
            $categoria->save();

            $datos = array(
                'mensaje' => 'Edición exitosa de Categoría.',
                'estado'  => 'success',
                'nombre'  => $categoria->nombre,
                'id'      => $categoria->id,
            );
        }

        return response()->json($datos,200);
    }

    public function getModalEliminar($id)
    {
        $categoria = Categoria::findOrFail($id);
        return view('categoria.modal_eliminar_categoria')->with('categoria', $categoria);
    }

    public function postEliminar(Request $request)
    {
        $categoria = Categoria::find($request->input('_id'));

        if ( !is_object($categoria) ) {

            $datos = array(
                'mensaje' => 'No se encuentra la Categoría en la Base de Datos',
                'estado' => 'error',
            );

        } else {

            $categoria->delete();

            $datos = array(
                'mensaje' => 'Se ha eliminado correctamente la Categoría.',
                'estado'  => 'success',
                'id'      => $categoria->id,
            );

        }

        return response()->json($datos,200);
    }
}
