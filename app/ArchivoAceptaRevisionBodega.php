<?php namespace App;

use Illuminate\Database\Eloquent\Model;

class ArchivoAceptaRevisionBodega extends Model
{
    protected $table = 'archivo_acepta_revision_bodega';

    public function getUsuarioEnviaToRevision()
    {
        return $this->hasOne('App\User','id', 'id_user_created');
    }

    public function getUsuarioRespuesta()
    {
        return $this->hasOne('App\User','id', 'id_user_respuesta');
    }
}
