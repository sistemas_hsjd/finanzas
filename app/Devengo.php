<?php namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use OwenIt\Auditing\Contracts\Auditable;

class Devengo extends Model implements Auditable
{
    use \OwenIt\Auditing\Auditable;
    use SoftDeletes;
    protected $table = 'devengos';

    /**
     * Attributes to include in the Audit.
     *
     * @var array
     */
    protected $auditInclude = [];
    
    /**
     * Should the timestamps be audited?
     *
     * @var bool
     */
    protected $auditTimestamps = true;

    /**
     * The attributes that should be mutated to dates.
     *
     * @var array
     */
    protected $dates = ['deleted_at'];

    public function getItemPresupuestario()
    {
        
        return $this->belongsTo('App\ItemPresupuestario','id_item_presupuestario')->withTrashed();
    }

    public function getDocumento()
    {
        
        return $this->belongsTo('App\Documento','id_documento')->withTrashed();
    }

    public function getFolioSigfe()
    {
        
        return $this->belongsTo('App\FolioSigfe','id_folio_sigfe')->withTrashed();
    }


    
}
