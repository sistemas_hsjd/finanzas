<?php namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use App\Documento;
use App\NominaPagoDocumento;

class NominaPago extends Model
{
    use SoftDeletes;
    protected $table = 'nominas_pago';

    public function getNominasPagosDocumentos()
    {
        return $this->hasMany('App\NominaPagoDocumento','id_nomina_pago','id');
    }

    public function getDatosIndex()
    {
        $fechaNomina = fecha_M($this->fecha_nomina).'/'.fecha_Y($this->fecha_nomina);

        $botones = '<div class="btn-group">';

        // if ( \Entrust::can('editar-nomina-pago') ) {
            $botones .= '<a href="'. url("nomina_pago/edit/{$this->id}") .'" class="btn btn-warning btn-xs" title="Editar nómina">';
            $botones .= '<i class="fas fa-edit"></i></a>';
        // }

        // if ( \Entrust::can('eliminar-nomina-pago') ) {
            $botones .= '<button type="button" class="btn btn-danger btn-xs" title="Eliminar nómina" onclick="eliminarNomina('. $this->id .');">';
            $botones .= '<i class="fas fa-trash"></i></button>';
        // }

        $botones .= '</div>';

        
        $saldoDisponible = '$ '.formatoMiles($this->saldo_disponible);
        $nominaPago = '$ '.formatoMiles($this->sumatoria_nomina_pago);
        $saldoPago = '$ '.formatoMiles($this->saldo_pago);

        $docJson = array(
            'DT_RowID' => $this->id,
            $this->id,
            $saldoDisponible,
            $nominaPago,
            $saldoPago,
            $fechaNomina,
            $botones
        );

        return $docJson;
    }

    /**
     * Agrega documentos automaticamente a la nomina.
     * los documentos deben ser del tipo de documento seleccionado.
     * deben estar con deuda disponible para pago y (en caso que no sean consumo basico) cuadrados
     * Se obtiene el documento más antiguo en deuda, funcion recursiva.
     */
    public function agregarDocumentos($tipoDocumentos, $saldoDisponible, $sumatoriaNominaPago, $idDocsDeudaCeroOrDescuadrados = [])
    {

        set_time_limit ( 0 );
        ini_set('max_execution_time', 0);
        
        $saldoPago = $saldoDisponible - $sumatoriaNominaPago;

        $doc = Documento::with([
                            'getComprobanteContableDocumento.getComprobanteContable',
                            'getReversaComprobante',
                            'getVistoBueno',
                            'getRelacionRecepcionesValidadas',
                            'getRelacionRecepcionesNoValidadas',
                        ])
                        // ->whereIn('id_tipo_documento', [1, 2, 3, 6, 7, 8, 9])
                        ->whereNotIn('id_tipo_documento', [4, 5, 10, 11])
                        ->has('getFoliosSigfe')
                        ->doesnthave('getNominaPagoDocumento')
                        ->where('total_documento_actualizado', '>', 0)
                        ->where('total_documento_actualizado', '<=', $saldoPago)
                        ->whereNotIn('id', $idDocsDeudaCeroOrDescuadrados);

        if ( $tipoDocumentos == 'Honorarios' ) {
            $doc =   $doc->where('id_tipo_informe', 6);
        }

        if ( $tipoDocumentos == 'Consumos Basicos' ) {
            $doc = $doc->where('id_tipo_informe', 5);
        }

        if ( $tipoDocumentos == 'Facturas') {
            $doc = $doc->whereNotIn('id_tipo_informe',[5, 6])
                       ->whereIn('id_tipo_documento', [1, 2, 6, 8]);
        }

        $doc = $doc->orderBy('dias_antiguedad', 'DESC')->take(1)->first();

        if ( is_object($doc) ) {

            if ( $doc->getDeuda() > 0 && ( $tipoDocumentos == 'Consumos Basicos' || $doc->estadoDocumentoDeudaFlotante() == 'Cuadrado') ) {

                // dd($saldoDisponible , $sumatoriaNominaPago, $saldoPago, $tipoDocumentos, $doc, $this);
                // Agregar documento a la nomina, y volver a llamar a la funcion, para que busque con sumatoriaNominaPago actualizada
                $nominaPagoDocumento = new NominaPagoDocumento();
                $nominaPagoDocumento->id_nomina_pago = $this->id;
                $nominaPagoDocumento->id_documento = $doc->id;
                $nominaPagoDocumento->save();

                $sumatoriaNominaPago += $doc->total_documento_actualizado;
                $this->sumatoria_nomina_pago = $sumatoriaNominaPago;
                $this->save();
                $this->agregarDocumentos($tipoDocumentos, $saldoDisponible , $sumatoriaNominaPago, $idDocsDeudaCeroOrDescuadrados);

            } else {

                $idDocsDeudaCeroOrDescuadrados[] = $doc->id;
                $this->agregarDocumentos($tipoDocumentos, $saldoDisponible , $sumatoriaNominaPago, $idDocsDeudaCeroOrDescuadrados);

            }

        } else {
            // significa que ningun documento cabe en la nomina, o que no queda saldo
            // dd($saldoDisponible , $sumatoriaNominaPago, $saldoPago, $tipoDocumentos, $doc, $this);
            $this->saldo_disponible = $sumatoriaNominaPago != 0 ? $sumatoriaNominaPago : $saldoDisponible;
            $this->sumatoria_nomina_pago = $sumatoriaNominaPago;
            $this->saldo_pago = $this->saldo_disponible - $this->sumatoria_nomina_pago;
            $this->save();
            return 'ok';

        }

    }

}
