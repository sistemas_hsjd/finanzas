<?php namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class ItemPresupuestario extends Model
{
    use SoftDeletes;
    protected $table = 'items_presupuestarios';

    public function __construct( $request = null, array $attributes = array())
    {
        parent::__construct($attributes);
        if ($request != null ) {
            $this->titulo = $request->input('titulo');
            $this->subtitulo = $request->input('subtitulo');
            $this->item = $request->input('item');
            $this->asignacion = $request->input('asignacion');
            $this->subasignacion = $request->input('subasignacion');
            $this->clasificador_presupuestario = trim($request->input('clasificador_presupuestario'));
            $this->id_clasificacion = $request->input('clasificacion');
            
            $this->id_user_created = \Auth::user()->id;
            $this->updated_at = NULL;
            $this->save();
        }
    }

    public function codigo()
    {
        $codigo = '';
        if ( $this->titulo != null ) {
            $codigo .= $this->titulo;
        }

        if ( $this->subtitulo != null ) {
            $codigo .= ".$this->subtitulo";
        }

        if ( $this->item != null ) {
            $codigo .= ".$this->item";
        }

        if ( $this->asignacion != null ) {
            $codigo .= ".$this->asignacion";
        }

        if ( $this->subasignacion != null ) {
            $codigo .= ".$this->subasignacion";
        }

        return $codigo;
    }

    public function codigoSinPuntos()
    {
        return $this->titulo.$this->subtitulo.$this->item.$this->asignacion.$this->subasignacion;
    }

    public function getClasificacion()
    {
        return $this->belongsTo('App\Clasificacion','id_clasificacion');
    }

    public function editItemPresupuestario($request)
    {
        $this->titulo = $request->input('titulo');
        $this->subtitulo = $request->input('subtitulo');
        $this->item = $request->input('item');
        $this->asignacion = $request->input('asignacion');
        $this->subasignacion = $request->input('subasignacion');
        $this->clasificador_presupuestario = trim($request->input('clasificador_presupuestario'));
        $this->id_clasificacion = $request->input('clasificacion');

        $this->id_user_updated = \Auth::user()->id;
        $this->save();
    }
}
