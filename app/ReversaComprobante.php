<?php namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class ReversaComprobante extends Model
{
    use SoftDeletes;
    protected $table = 'reversas_comprobantes';

    public function getDocumento()
    {
        return $this->belongsTo('App\Documento', 'id_documento')->withTrashed();
    }

    public function getCuentaContable()
    {
        return $this->hasOne('App\CuentaContable','id','id_cuenta_contable')->withTrashed();
    }

    public function getComprobanteContable()
    {
        return $this->hasOne('App\ComprobanteContable','id','id_comprobante_contable');
    }

    public function getComprobanteContableDocumento()
    {
        return $this->hasOne('App\ComprobanteContableDocumento','id','id_comprobante_contable_documento');
    }

}
