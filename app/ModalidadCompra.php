<?php namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class ModalidadCompra extends Model
{
    use SoftDeletes;
    protected $table = 'modalidades_compra';

    public function __construct( $request = null, array $attributes = array())
    {
        parent::__construct($attributes);
        if ($request != null ) {
            $this->nombre = trim($request->input('nombre'));
            $this->predeterminado = trim($request->input('valor_predeterminado'));
            
            $this->id_user_created = \Auth::user()->id;
            $this->updated_at = NULL;
            $this->save();
        }
    }

    public function editModalidadCompra($request)
    {
        $this->nombre = trim($request->input('nombre'));
        $this->predeterminado = trim($request->input('valor_predeterminado'));

        $this->id_user_updated = \Auth::user()->id;
        $this->save();
    }
}
