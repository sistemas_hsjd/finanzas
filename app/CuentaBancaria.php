<?php namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class CuentaBancaria extends Model
{
    use SoftDeletes;
    protected $table = 'cuentas_bancarias';

    public function __construct( $request = null, array $attributes = array())
    {
        parent::__construct($attributes);
        if ($request != null ) {
            $this->id_banco = $request->get('banco');
            $this->codigo = trim($request->get('codigo'));
            $this->saldo = formato_entero($request->get('saldo'));
            $this->save();
        }
    }

    public function editCuentaBancaria($request)
    {
        $this->id_banco = $request->get('banco');
        $this->codigo = trim($request->get('codigo'));
        $this->saldo = formato_entero($request->get('saldo'));
        $this->save();
    }

    public function getBanco()
    {
        return $this->belongsTo('App\Banco','id_banco');
    }
}
