<?php namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Contrato extends Model
{

    use SoftDeletes;
    protected $table = 'contratos';

    public function getProveedor()
    {
        return $this->belongsTo('App\Proveedor','id_proveedor')->withTrashed();
    }

    /**
     * Servicio
     */
    public function getUnidad()
    {
        return $this->hasOne('App\Unidad','id', 'id_unidad')->withTrashed();
    }

    public function getProfesion()
    {
        return $this->hasOne('App\Profesion','id', 'id_profesion')->withTrashed();
    }

    public function getCategoria()
    {
        return $this->hasOne('App\Categoria','id', 'id_categoria')->withTrashed();
    }

    public function getTipoPrestacion()
    {
        return $this->hasOne('App\TipoPrestacion','id', 'id_tipo_prestacion')->withTrashed();
    }    

    public function getTipoAdjudicacion()
    {
        return $this->belongsTo('App\TipoAdjudicacion','id_tipo_adjudicacion')->withTrashed();
    }

    public function getReferenteTecnico()
    {
        return $this->belongsTo('App\ReferenteTecnico','id_referente_tecnico')->withTrashed();
    }

    public function getArchivos()
    {
        return $this->hasMany('App\Archivo','id_contrato','id');
    }

    public function getOrdenesCompra()
    {
        return $this->hasMany('App\OrdenCompra','id_contrato','id');
    }

    public function getDocumentosContrato()
    {
        return $this->hasMany('App\DocumentoContratoOrdenCompra','id_contrato','id');
    }

    public function getUserTermino()
    {
        return $this->hasOne('App\User','id', 'id_user_termino')->withTrashed();
    }

    public function getMontoMinimo()
    {
        return $this->monto_contrato - $this->saldo_contrato;
    }

    public function getMontoMinimoPreventivo()
    {
        return $this->monto_preventivo - $this->saldo_preventivo;
    }

    public function getMontoMinimoCorrectivo()
    {
        return $this->monto_correctivo - $this->saldo_correctivo;
    }

    /**
     * Trae los contratos del proveedor, que cuentas con saldo y estan vigentes
     */
    public function scopeVigenteConSaldoParaProveedor($query, $idProveedor)
    {
        return $query->where('id_proveedor', $idProveedor)
                     ->where('saldo_contrato', '>', 0)
                     ->where('termino', 0);
    }

    public function getDatosIndex()
    {

        $fechaProceso = fecha_dmY($this->fecha_proceso);

        $botones = '<div class="btn-group">';

        $botones .= '<button type="button" class="btn btn-success btn-xs" title="Ver" onclick="ver('. $this->id .');">';
        $botones .= '<i class="fas fa-eye fa-lg"></i></button>';

        // if ( \Entrust::can('editar-comprobante-contable') ) {

        if ( $this->termino == 0 ) {
            $botones .= '<button type="button" class="btn btn-warning btn-xs" title="Editar" onclick="editar('. $this->id .');">';
            $botones .= '<i class="fas fa-edit fa-lg"></i></button>';
        // }
        }

        if ( $this->saldo_contrato > 0 && $this->termino == 0 ) {

            $botones .= '<button type="button" class="btn btn-info btn-xs" title="Consumir" onclick="consumir('. $this->id .');">';
            $botones .= '<i class="fas fa-donate fa-lg"></i></button>';
        
        }

        // if ( \Entrust::can('eliminar-comprobante-contable') ) {
        if ( $this->monto_contrato == $this->saldo_contrato ) {

            $botones .= '<button type="button" class="btn btn-danger btn-xs" title="Eliminar" onclick="eliminar('. $this->id .');">';
            $botones .= '<i class="fas fa-trash fa-lg"></i></button>';

        }
        // }

        if ( $this->saldo_contrato > 0 && $this->termino == 0 ) {

            $botones .= '<button type="button" class="btn btn-primary btn-xs" title="Dar Término" onclick="terminar('. $this->id .');">';
            $botones .= '<i class="fas fa-user-slash fa-lg"></i></button>';
        
        }

        $rutaPDF = asset('contratos/pdf_saldo_contrato/').'/'.$this->id;
        $botones .= '<a class="btn btn-danger btn-xs" title="PDF Saldo" target="_blank" href="'.$rutaPDF.'">';
        $botones .= '<i class="fas fa-file-pdf fa-lg"></i></a>';

        $botones .= '</div>';

        // $monto = '$ '.formatoMiles($this->monto_total);

        $docJson = array(
            'DT_RowID' => $this->id,
            ( $this->termino == 0 ) ? 'Vigente' : 'Terminado',
            $this->getProveedor->rut.' '.$this->getProveedor->nombre,
            $this->resolucion,
            fecha_dmY($this->inicio_vigencia),
            fecha_dmY($this->fin_vigencia),
            formatoMiles($this->monto_contrato),
            formatoMiles($this->saldo_contrato),
            $botones
        );

        return $docJson;

    }

    public function obtenerValorHoraEdicionDocumento()
    {

        $valorHora = '';
        if ( $this->valor_hora != null ) {

            $valorHora .= formatoMiles($this->valor_hora);

        } else {

            if ( $this->por_procedimiento == 1 ) {

                $valorHora .= 'Por procedimiento';

            } else {

                $valorHora .= '0';

            }

        }

        return $valorHora;

    }

    public function tieneSaldosPreventivoCorrectivo()
    {
        return ( $this->saldo_preventivo != null && $this->saldo_correctivo != null ) ? true : false;
    }

    public function getOpcionesParaSelect($documento = null)
    {

        $option =  PHP_EOL.'<option value="'.$this->id.'" ';
        $option .= ' id="contrato_seleccionado_'.$this->id.'" ';

        $option .= ' data-resolucion-contrato="'.$this->resolucion.'" ';
        $option .= ' data-inicio-vigencia-contrato="'.fecha_dmY($this->inicio_vigencia).'" ';
        $option .= ' data-fin-vigencia-contrato="'.fecha_dmY($this->fin_vigencia).'" ';
        $option .= ' data-monto-contrato="'.formatoMiles($this->monto_contrato).'" ';
        $option .= ' data-saldo-contrato="'.formatoMiles($this->saldo_contrato).'" ';

        if ( $this->valor_hora != null ) {

            $option .= ' data-valor-hora="'.formatoMiles($this->valor_hora).'" ';

        } else {

            if ( $this->por_procedimiento == 1 ) {

                $option .= ' data-valor-hora="Por procedimiento" ';

            } else {

                $option .= ' data-valor-hora="0" ';

            }

        }

        $option .= ' data-inicio-vigencia-contrato-periodo="'.fecha_mY($this->inicio_vigencia).'" ';
        $option .= ' data-fin-vigencia-contrato-periodo="'.fecha_mY($this->fin_vigencia).'" ';

        if ( $this->saldo_preventivo != null && $this->saldo_correctivo != null ) {

            $option .= ' data-monto-preventivo="'.formatoMiles($this->monto_preventivo).'" ';
            $option .= ' data-monto-correctivo="'.formatoMiles($this->monto_correctivo).'" ';

            $option .= ' data-saldo-preventivo="'.formatoMiles($this->saldo_preventivo).'" ';
            $option .= ' data-saldo-correctivo="'.formatoMiles($this->saldo_correctivo).'" ';

        } else {

            $option .= ' data-monto-preventivo="" ';
            $option .= ' data-monto-correctivo="" ';

            $option .= ' data-saldo-preventivo="" ';
            $option .= ' data-saldo-correctivo="" ';

        }
        
        $option .= ' data-tipo-adjudicacion="'.$this->id_tipo_adjudicacion.'" ';
        $option .= ' data-referente-tecnico="'.$this->id_referente_tecnico.'" ';

        if ( is_object($documento) && $documento->getContratoOrdenCompra && $documento->getContratoOrdenCompra->id_contrato == $this->id ) {
            $option .= ' selected ';
        }

        $option .= ' >'.$this->resolucion.'</option>'.PHP_EOL;

        return $option;

    }

}
