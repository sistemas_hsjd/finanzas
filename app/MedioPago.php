<?php namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class MedioPago extends Model
{
    
    use SoftDeletes;
    protected $table = 'medios_pago';

    public function __construct( $request = null, array $attributes = array())
    {
        parent::__construct($attributes);
        if ($request != null ) {
            $this->nombre = trim($request->input('nombre'));
            $this->save();
        }
    }

}
