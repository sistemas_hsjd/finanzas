<?php namespace App;

use Illuminate\Database\Eloquent\Model;

class WsOrdenCompra extends Model
{
    protected $table = 'ws_orden_compra';

    public function __construct( $paramsOrdenCompra = null, $idContrato = null ,array $attributes = array())
    {
        parent::__construct($attributes);
        if ( $paramsOrdenCompra != null ) {
            $this->id_contrato = $idContrato; //puede ser null

            // Datos Requeridos
            $this->fecha_orden_compra = $paramsOrdenCompra->fecha_orden_compra;
            $this->fecha_recepcion_orden_compra = $paramsOrdenCompra->fecha_recepcion_orden_compra;
            $this->id_orden_compra = $paramsOrdenCompra->id_orden_compra;
            $this->numero_orden_compra = $paramsOrdenCompra->numero_orden_compra;
            $this->total_orden_compra = $paramsOrdenCompra->total_orden_compra;
            $this->tipo_compra = $paramsOrdenCompra->tipo_compra;

            // Datos Opcionales
            if ( @$paramsOrdenCompra->programa != '' && @$paramsOrdenCompra->programa != null) {
                $this->programa = $paramsOrdenCompra->programa;
            }

            if ( @$paramsOrdenCompra->saldo_orden_compra != 0 || @$paramsOrdenCompra->saldo_orden_compra != null ) {
                $this->saldo_orden_compra = $paramsOrdenCompra->saldo_orden_compra;
            }

            if ( @$paramsOrdenCompra->numero_orden_compra_mercado_publico != null ) {
                $this->numero_orden_compra_mercado_publico = $paramsOrdenCompra->numero_orden_compra_mercado_publico;
            }

            if ( @$paramsOrdenCompra->fecha_orden_compra_mercado_publico != null ) {
                $this->fecha_orden_compra_mercado_publico = $paramsOrdenCompra->fecha_orden_compra_mercado_publico;
            }

            // Nuevos Datos para estado de OC,se actualizan desde Hermes
            if ( @$paramsOrdenCompra->supera_mil_utm != '' && @$paramsOrdenCompra->supera_mil_utm != null) {
                $this->supera_mil_utm = $paramsOrdenCompra->supera_mil_utm;
            } else {
                $this->supera_mil_utm = 0;
            }

            if ( @$paramsOrdenCompra->firma_abastecimiento != '' && @$paramsOrdenCompra->firma_abastecimiento != null) {
                $this->firma_abastecimiento = $paramsOrdenCompra->firma_abastecimiento;
            } else {
                $this->firma_abastecimiento = 0;
            }

            if ( @$paramsOrdenCompra->firma_sda != '' && @$paramsOrdenCompra->firma_sda != null) {
                $this->firma_sda = $paramsOrdenCompra->firma_sda;
            } else {
                $this->firma_sda = 0;
            }

            if ( @$paramsOrdenCompra->firma_direccion != '' && @$paramsOrdenCompra->firma_direccion != null) {
                $this->firma_direccion = $paramsOrdenCompra->firma_direccion;
            } else {
                $this->firma_direccion = 0;
            }
            
            $this->save();
        }
        
    }

    public function actualizarDatosOc($paramsOrdenCompra, $idContrato = null)
    {
        $this->id_contrato = $idContrato; //puede ser null

        // Datos Requeridos
        $this->fecha_orden_compra = $paramsOrdenCompra->fecha_orden_compra;
        $this->fecha_recepcion_orden_compra = $paramsOrdenCompra->fecha_recepcion_orden_compra;
        $this->id_orden_compra = $paramsOrdenCompra->id_orden_compra;
        $this->numero_orden_compra = $paramsOrdenCompra->numero_orden_compra;
        $this->total_orden_compra = $paramsOrdenCompra->total_orden_compra;
        $this->tipo_compra = $paramsOrdenCompra->tipo_compra;

        // Datos Opcionales
        if ( @$paramsOrdenCompra->programa != '' && @$paramsOrdenCompra->programa != null) {
            $this->programa = $paramsOrdenCompra->programa;
        }

        if ( @$paramsOrdenCompra->saldo_orden_compra != 0 || @$paramsOrdenCompra->saldo_orden_compra != null ) {
            $this->saldo_orden_compra = $paramsOrdenCompra->saldo_orden_compra;
        }

        if ( @$paramsOrdenCompra->numero_orden_compra_mercado_publico != null ) {
            $this->numero_orden_compra_mercado_publico = $paramsOrdenCompra->numero_orden_compra_mercado_publico;
        }

        if ( @$paramsOrdenCompra->fecha_orden_compra_mercado_publico != null ) {
            $this->fecha_orden_compra_mercado_publico = $paramsOrdenCompra->fecha_orden_compra_mercado_publico;
        }

        // Nuevos Datos para estado de OC,se actualizan desde Hermes
        if ( @$paramsOrdenCompra->supera_mil_utm != '' && $paramsOrdenCompra->supera_mil_utm != null) {
            $this->supera_mil_utm = $paramsOrdenCompra->supera_mil_utm;
        } else {
            $this->supera_mil_utm = 0;
        }

        if ( @$paramsOrdenCompra->firma_abastecimiento != '' && $paramsOrdenCompra->firma_abastecimiento != null) {
            $this->firma_abastecimiento = $paramsOrdenCompra->firma_abastecimiento;
        } else {
            $this->firma_abastecimiento = 0;
        }

        if ( @$paramsOrdenCompra->firma_sda != '' && $paramsOrdenCompra->firma_sda != null) {
            $this->firma_sda = $paramsOrdenCompra->firma_sda;
        } else {
            $this->firma_sda = 0;
        }

        if ( @$paramsOrdenCompra->firma_direccion != '' && $paramsOrdenCompra->firma_direccion != null) {
            $this->firma_direccion = $paramsOrdenCompra->firma_direccion;
        } else {
            $this->firma_direccion = 0;
        }
        
        $this->save();
    }

    public function getWsContrato()
    {        
        return $this->belongsTo('App\WsContrato','id_contrato');
    }

    public function getWsDocumento()
    {
        return $this->hasOne('App\WsDocumento','id_orden_compra');
    }

    public function getWsArchivos()
    {
        return $this->hasMany('App\WsArchivo','id_ws_orden_compra');
    }

    public function colorPorFirmas()
    {
        $color = '';
        $cuentaFirmas = 0;
        if ( $this->firma_abastecimiento == 1 ) {
            $cuentaFirmas++;
        }

        if ( $this->firma_sda == 1 ) {
            $cuentaFirmas++;
        }

        if ( $this->firma_direccion == 1 ) {
            $cuentaFirmas++;
        }


        if ( $this->supera_mil_utm == 1 ) {
            //necesita 3 firmas
            if ( $cuentaFirmas  == 3 ) {
                $color = 'green';
            } elseif ( $cuentaFirmas == 2 ) {
                $color = '#e2e200f5';
            } elseif ( $cuentaFirmas == 1 ) {
                $color = '#ff8300';
            } elseif ( $cuentaFirmas == 0 ) {
                $color = 'red';
            }
        } else {
            //necesita 2 firmas
            if ( $cuentaFirmas == 2 ) {
                $color = 'green';
            } elseif ( $cuentaFirmas == 1 ) {
                $color = '#e2e200f5';
            } elseif ( $cuentaFirmas == 0 ) {
                $color = 'red';
            }
        }

        return $color;

    }
}
