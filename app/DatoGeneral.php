<?php namespace App;

use Illuminate\Database\Eloquent\Model;

class DatoGeneral extends Model
{
    protected $table = 'datos_generales';
}
