<?php namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;


class MotivoProblema extends Model
{
    use SoftDeletes;
    protected $table = 'motivos_problema';

    public function __construct( $request = null, array $attributes = array())
    {
        parent::__construct($attributes);
        if ($request != null ) {
            $this->nombre = trim($request->get('nombre'));
            $this->updated_at = NULL;
            $this->save();
        }
    }

    public function editMotivoProblema($request)
    {
        $this->nombre = trim($request->get('nombre'));
        $this->save();
    }
}
