<?php namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class CuentaContable extends Model
{
    use SoftDeletes;
    protected $table = 'cuentas_contables';

    public function __construct( $request = null, array $attributes = array())
    {
        parent::__construct($attributes);
        if ($request != null ) {
            $this->codigo = trim($request->get('codigo'));
            $this->glosa = trim($request->get('glosa'));
            $this->saldo = formato_entero($request->get('saldo'));
            $this->save();
        }
    }

    public function editCuentaContable($request)
    {
        $this->codigo = trim($request->get('codigo'));
        $this->glosa = trim($request->get('glosa'));
        $this->saldo = formato_entero($request->get('saldo'));
        $this->save();
    }

}
