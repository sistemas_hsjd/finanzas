<?php namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class OrdenCompra extends Model
{
    use SoftDeletes;
    protected $table = 'ordenes_compra';

    public function getProveedor()
    {
        return $this->belongsTo('App\Proveedor','id_proveedor')->withTrashed();
    }

    public function getContrato()
    {
        return $this->belongsTo('App\Contrato','id_contrato')->withTrashed();
    }

    public function getArchivos()
    {
        return $this->hasMany('App\Archivo','id_orden_compra','id');
    }

    public function getDocumentosOrdenCompra()
    {
        return $this->hasMany('App\DocumentoContratoOrdenCompra','id_orden_compra','id');
    }

    public function getMontoMinimo()
    {
        return $this->monto_oc - $this->saldo_oc;
    }

    public function getDatosIndex()
    {

        $fechaProceso = fecha_dmY($this->fecha_proceso);

        $botones = '<div class="btn-group">';

        // if ( \Entrust::can('editar-comprobante-contable') ) {
            $botones .= '<button type="button" class="btn btn-warning btn-xs" title="Editar" onclick="editar('. $this->id .');">';
            $botones .= '<i class="fas fa-edit fa-lg"></i></button>';
        // }

        // if ( \Entrust::can('eliminar-comprobante-contable') ) {
        if ( $this->monto_oc == $this->saldo_oc ) {

            $botones .= '<button type="button" class="btn btn-danger btn-xs" title="Eliminar" onclick="eliminar('. $this->id .');">';
            $botones .= '<i class="fas fa-trash fa-lg"></i></button>';
        
        }
        // }

        if ( $this->saldo_oc > 0 ) {

            if ( $this->getContrato && $this->getContrato->saldo_contrato > 0 && $this->getContrato->termino == 0 ) {

                $botones .= '<button type="button" class="btn btn-info btn-xs" title="Consumir" onclick="consumir('. $this->id .');">';
                $botones .= '<i class="fas fa-donate fa-lg"></i></button>';

            } else {

                $botones .= '<button type="button" class="btn btn-info btn-xs" title="Consumir" onclick="consumir('. $this->id .');">';
                $botones .= '<i class="fas fa-donate fa-lg"></i></button>';

            }

        }

        if ( $this->getArchivos->count() > 0 ) {
            $oc = false;
            foreach ( $this->getArchivos as $archivo ) {
                $oc = $archivo->id_tipo_archivo == 3 ? true : false;
            }
            if ( ! $oc ) {
                //boton oc
                $botones .= '<button type="button" class="btn btn-success btn-xs" title="Obtener PDF OC" onclick="obtenerPDF('. $this->id .');">';
                $botones .= '<i class="fas fa-folder-plus fa-lg"></i></button>';
            }
        } else {
            //boton oc
            $botones .= '<button type="button" class="btn btn-success btn-xs" title="Obtener PDF OC" onclick="obtenerPDF('. $this->id .');">';
            $botones .= '<i class="fas fa-folder-plus fa-lg"></i></button>';
        }

        $botones .= '</div>';

        // $monto = '$ '.formatoMiles($this->monto_total);

        $docJson = array(
            'DT_RowID' => $this->id,
            $this->getProveedor->rut.' '.$this->getProveedor->nombre,
            $this->numero_oc,
            formatoMiles($this->monto_oc),
            formatoMiles($this->saldo_oc),
            $botones
        );

        return $docJson;

    }

    public function getOpcionesParaSelect($documento = null)
    {

        $option = '';
        $option .= PHP_EOL.'<option value="'.$this->id.'" ';
        $option .= ' id="oc_seleccionada_'.$this->id.'" ';
        $option .= ' data-id-contrato="'.$this->id_contrato.'" ';

        $option .= ' data-fecha-oc="'.fecha_dmY($this->fecha_oc).'" ';
        $option .= ' data-monto-oc="'.formatoMiles($this->monto_oc).'" ';
        $option .= ' data-saldo-oc="'.formatoMiles($this->saldo_oc).'" ';

        $option .= ' data-inicio-vigencia-oc="'.fecha_dmY($this->inicio_vigencia).'" ';
        $option .= ' data-fin-vigencia-oc="'.fecha_dmY($this->fin_vigencia).'" ';
        $option .= ' data-inicio-vigencia-oc-periodo="'.fecha_mY($this->inicio_vigencia).'" ';
        $option .= ' data-fin-vigencia-oc-periodo="'.fecha_mY($this->fin_vigencia).'" ';

        if ( is_object($documento) && $documento->getContratoOrdenCompra && $documento->getContratoOrdenCompra->id_orden_compra == $this->id ) {
            $option .= ' selected ';
        }

        $option .= ' >'.$this->numero_oc.'</option>'.PHP_EOL;

        return $option;
        
    }

}
