<?php namespace App;

use Illuminate\Database\Eloquent\Model;

class WsItemPresupuestario extends Model
{
    protected $table = 'ws_item_presupuestario';

    public function __construct( $paramsItem = null, $idDocumento = null ,array $attributes = array())
    {
        parent::__construct($attributes);
        if ( $paramsItem != null ) {
            $this->id_documento = $idDocumento;
            // Datos Requeridos 
            $this->id_producto = $paramsItem->id_producto;
            $this->cantidad_recepcionada = $paramsItem->cantidad_recepcionada;
            $this->valor_item_recepcionado = $paramsItem->valor_item_recepcionado;
            $this->item_presupuestario = $paramsItem->item_presupuestario != null && $paramsItem->item_presupuestario != '' ? $paramsItem->item_presupuestario : '0';

            // Datos Opcionales
            if ( @$paramsItem->descripcion_articulo != null ) {
                $this->descripcion_articulo = $paramsItem->descripcion_articulo;
            }
            if ( @$paramsItem->codigo_articulo != null ) {
                $this->codigo_articulo = $paramsItem->codigo_articulo;
            }
            
            $this->save();
        }
        
    }
}
