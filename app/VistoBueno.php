<?php namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class VistoBueno extends Model
{
    use SoftDeletes;
    protected $table = 'vistos_buenos';

    public function __construct( $request = null,$idReferenteTecnico = null, array $attributes = array())
    {
        parent::__construct($attributes);
        if ( $request != null ) {
            $this->memo = trim($request->input('numero_memo'));
            $this->fecha_solicitud = fecha_Y_m_d($request->input('fecha_solicitud'));
            // $this->informe = trim($request->input('numero_informe'));
            // $this->fecha_informe = fecha_Y_m_d($request->input('fecha_informe'));
            $this->id_digitador = \Auth::user()->id;
            $this->id_referente_tecnico = $idReferenteTecnico;
            
            $this->id_user_created = \Auth::user()->id;
            $this->updated_at = null;
            $this->save();
        }
    }

    public function editMemoParaVistoBueno($request = null)
    {
        if ($request != null) {
            $this->memo = trim($request->input('numero_memo'));
            $this->fecha_solicitud = fecha_Y_m_d($request->input('fecha_solicitud'));
            // $this->informe = trim($request->input('numero_informe'));
            // $this->fecha_informe = fecha_Y_m_d($request->input('fecha_informe'));
            
            $this->id_user_updated = \Auth::user()->id;
            $this->save();
        }
    }

    public function registrarVistoBueno($request = null)
    {
        if ($request != null) {
            $this->memo_respuesta = trim($request->input('numero_memo_respuesta'));
            $this->fecha_memo_respuesta = fecha_Y_m_d($request->input('fecha_respuesta'));
            $this->observacion_memo_respuesta = trim($request->input('observacion'));
            $this->id_registrador_memo_respuesta = \Auth::user()->id;
            $this->id_user_updated = \Auth::user()->id;
            $this->save();
        }
    }

    public function editVistoBueno($request = null)
    {
        if ($request != null) {
            $this->memo = trim($request->input('numero_memo'));
            $this->memo_respuesta = trim($request->input('numero_memo_respuesta'));
            
            $this->observacion_memo_respuesta = trim($request->input('observacion'));
            $this->id_user_updated = \Auth::user()->id;
            $this->save();
        }
    }

    public function getReferenteTecnico()
    {
        return $this->belongsTo('App\ReferenteTecnico','id_referente_tecnico')->withTrashed();
    }

    public function getDigitador()
    {
        
        return $this->belongsTo('App\User','id_digitador')->withTrashed();
    }
    
    public function getRegistrador()
    {
        
        return $this->belongsTo('App\User','id_registrador_memo_respuesta')->withTrashed();
    }

    public function getArchivos()
    {
        
        return $this->hasMany('App\Archivo','id_visto_bueno','id');
    }

    public function getDocumentos()
    {
        return $this->hasMany('App\Documento','id_visto_bueno','id')->withTrashed();
    }

    public function getDatosMemosVistoBueno()
    {
        $fechaSolicitud = fecha_dmY($this->fecha_solicitud);
        $refrenteTecnico = '';
        if ( $this->getReferenteTecnico ) {
            $refrenteTecnico = $this->getReferenteTecnico->nombre.' '.$this->getReferenteTecnico->responsable;
        }

        $botones = '<div class="btn-group">';

        if ( \Entrust::can('registro-visto-bueno') ) {
            $botones .= '<button class="btn btn-info btn-xs " title="Registrar" onclick="registrarVistoBueno('. $this->id .');">';
            $botones .= '<i class="fas fa-file-signature fa-lg"></i></button>';
        }

        if ( \Entrust::can('ver-memo-solicitud-visto-bueno') ) {
            $botones .= '<a class="btn btn-danger btn-xs " title="Ver Memo Solicitud Visto Bueno" target="_blank" href="'. asset('referente_tecnico/generar/pdf_solicitud_visto_bueno/'.$this->id) .'">';
            $botones .= '<i class="fas fa-file-pdf fa-lg"></i></a>';
        }

        if ( \Entrust::can('editar-solicitud-visto-bueno') ) {
            $botones .= '<button class="btn btn-warning btn-xs " title="Editar Memo" onclick="editarMemo('. $this->id .');">';
            $botones .= '<i class="fas fa-edit fa-lg"></i></button>';
        }

        if ( \Entrust::can('eliminar-solicitud-visto-bueno') ) {
            $botones .= '<button class="btn btn-danger btn-xs" title="Eliminar Memo" onclick="eliminarMemo('. $this->id .')" > ';
            $botones .= ' <i class="fa fa-trash fa-lg" ></i></button>';
        }

        $botones .= '</div>';

        $docJson = array(
            'DT_RowID' => $this->id,
            $fechaSolicitud,
            $this->memo,
            $this->getDigitador->name,
            $refrenteTecnico,
            $botones
        );

        return $docJson;
    }
}
