<?php namespace App\Traits;

use App\Documento;
use App\HermesDocumento;
use App\RelacionDocumentoHermesDocumento;


trait relacionRecepcionesNoValidadas
{
    /**
     * Valida y guarda la relacion entre un documentos y hermes documento.
     * hermes documento no debe tener relaciones (match).
     * (NO) Se valida el documento despues de guardar la relacion.
     */
    public static function validarGuardarRelacionesHermes($folioDocRecepcion, Documento $documento, &$mensajeDocumento = null, &$observacionMatchHermes = null)
    {

        $hermesDocumento = HermesDocumento::where('id_proveedor', $documento->id_proveedor)
                                          ->where('folio_documento', $folioDocRecepcion )
                                          ->doesnthave('getRelacion')
                                          ->first();

        if ( is_object($hermesDocumento) ) {

            $relacionHermesDocumento = new RelacionDocumentoHermesDocumento();
            $relacionHermesDocumento->id_documento = $documento->id;
            $relacionHermesDocumento->id_hermes_documento = $hermesDocumento->id;
            $relacionHermesDocumento->id_user_relacion = \Auth::user()->id;
            $relacionHermesDocumento->save();
            
            // $documento->validar();

            $mensajeDocumento .= '<br><strong style="colo:black !important;">';

            if ( strpos($hermesDocumento->tipo_documento, 'Factura') !== false ) {

                $mensajeDocumento .= 'Se ha conectado exitosamente con la Factura Hermes N° '.$hermesDocumento->folio_documento.'.';

            } elseif ( strpos($hermesDocumento->tipo_documento, 'Guia') !== false ) {

                $mensajeDocumento .= 'Se ha conectado exitosamente con la Guia Hermes N° '.$hermesDocumento->folio_documento.'.';

            }

            $mensajeDocumento .= '</strong>';
            $observacionMatchHermes .= $hermesDocumento->folio_documento.' | ';

        } else {

            $hermesDocumentoAux = HermesDocumento::where('id_proveedor', $documento->id_proveedor)
                                                 ->where('folio_documento', $folioDocRecepcion )
                                                 ->has('getRelacion')
                                                 ->first();

            
            $mensajeDocumento .= '<br><strong style="colo:black !important;">';

            if ( is_object($hermesDocumentoAux) ) {

                // Mensaje de que el documento hermes ya tiene una relacion.
                if ( strpos($hermesDocumentoAux->tipo_documento, 'Factura') !== false ) {

                    $mensajeDocumento .= 'No se ha conectado con la Factura Hermes N° '.$hermesDocumentoAux->folio_documento.', ya esta relacionada a otro documento.';

                } elseif ( strpos($hermesDocumentoAux->tipo_documento, 'Guia') !== false ) {

                    $mensajeDocumento .= 'No se ha conectado con la Guia Hermes N° '.$hermesDocumentoAux->folio_documento.', ya esta relacionada a otro documento.';

                }

            } else {
                // Mensaje de que el documento hermes no se encontro
                $mensajeDocumento .= 'No se ha encontrado la recepción de hermes, N° documento : '.$hermesDocumentoAux->folio_documento;
            }
            
            $mensajeDocumento .= '</strong>';

        }
    }

    
}
