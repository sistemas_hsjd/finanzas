<?php namespace App\Traits;

use Mpdf\Mpdf;
use iio\libmergepdf\Merger;
use iio\libmergepdf\Driver\TcpdiDriver;

use App\Documento;
use App\WsDocumento;
use App\Contrato;
use App\HermesDocumento;


trait saldoContrato
{
    /**
     * Funcion que genera el pdf del saldo del contrato
     */
    public static function generarSaldo($id, $mostrar = true)
    {

        $contrato = Contrato::with([
                                'getDocumentosContrato.getDocumento',
                                'getUnidad',
                                'getProfesion',
                                'getCategoria',
                                'getTipoPrestacion',
                                'getTipoAdjudicacion',
                                'getReferenteTecnico',
                                'getOrdenesCompra'
                              ])
                            ->findOrFail($id);

        /**
         * Construir Saldo
         */
        $cantidadMeses = cantidadMesesEntreFechas($contrato->inicio_vigencia, $contrato->fin_vigencia);
        $mes = fecha_M($contrato->inicio_vigencia);
        $año = fecha_Y($contrato->inicio_vigencia);
        $cuotasContrato = array();
        $totalMontoRecepciones = 0;

        for ( $i = 0; $i < $cantidadMeses ; $i++) {

            $mesConsulta = $mes + $i;
            
            if ( $mesConsulta > 12 ) {

                $añosPasados = (int) ( ($año - 2000) / $mesConsulta );

                // dd($año, $mesConsulta, $añosPasados);
                $mesConsulta -=  $añosPasados * 12;
                // trabajar fecha de consulta
                $periodoConsulta = $mesConsulta < 10 ? $año + $añosPasados. '-0' .$mesConsulta. '-01' : $año + $añosPasados. '-0' .$mesConsulta. '-01' ;

                 // buscar docs
                 $docs = Documento::with(['getContratoOrdenCompra','getRelacionRecepcionesValidadas.getWsDocumento'])
                                  ->documentosDelContratoSegunPeriodo($periodoConsulta, $contrato->id)
                                  ->get();

                if ( $docs->count() > 0 ) {
                    foreach ( $docs as $doc ) {

                        $numRecepciones = '';
                        $totalRecepciones = 0;
                        foreach ( $doc->getRelacionRecepcionesValidadas as $relacion ) {

                            $numRecepciones .= $relacion->getWsDocumento->documento.' ';
                            $totalRecepciones += $relacion->getWsDocumento->documento_total;
                            $totalMontoRecepciones += $relacion->getWsDocumento->documento_total;

                        }

                        $cuota = new \stdClass;

                        $cuota->recepciones = $numRecepciones;
                        $cuota->totalRecepciones = $totalRecepciones != 0 ? '$ '.formatoMiles($totalRecepciones) : '';

                        $cuota->orden_compra = $doc->getContratoOrdenCompra->getOrdenCompra ? $doc->getContratoOrdenCompra->getOrdenCompra->numero_oc : $doc->documento_compra;
                        $cuota->numero_documento = $doc->numero_documento;
                        $cuota->fecha_documento = fecha_dmY($doc->fecha_documento);
                        $cuota->num_cuota = "CUOTA ". ($i+1) ."/". $cantidadMeses;
                        $cuota->periodo_doc = fechaPeriodoDocSaldoContrato($doc->periodo);
                        $cuota->monto_doc = '$ '.formatoMiles($doc->total_documento);
                        $cuotasContrato[] = $cuota;

                    }
                }

            } else {

                // trabajar fecha de consulta
                $periodoConsulta = $mesConsulta < 10 ? $año.'-0'.$mesConsulta.'-01' : $año.'-0'.$mesConsulta.'-01' ;
                
                // buscar docs
                $docs = Documento::with(['getContratoOrdenCompra','getRelacionRecepcionesValidadas.getWsDocumento'])
                                ->documentosDelContratoSegunPeriodo($periodoConsulta, $contrato->id)
                                ->get();
                
                if ( $docs->count() > 0 ) {                    
                    foreach ( $docs as $doc ) {

                        $numRecepciones = '';
                        $totalRecepciones = 0;
                        foreach ( $doc->getRelacionRecepcionesValidadas as $relacion ) {

                            $numRecepciones .= $relacion->getWsDocumento->documento.' ';
                            $totalRecepciones += $relacion->getWsDocumento->documento_total;
                            $totalMontoRecepciones += $relacion->getWsDocumento->documento_total;

                        }

                        $cuota = new \stdClass;

                        $cuota->recepciones = $numRecepciones;
                        $cuota->totalRecepciones = $totalRecepciones != 0 ? '$ '.formatoMiles($totalRecepciones) : '';

                        $cuota->orden_compra = $doc->getContratoOrdenCompra->getOrdenCompra ? $doc->getContratoOrdenCompra->getOrdenCompra->numero_oc : $doc->documento_compra;
                        $cuota->numero_documento = $doc->numero_documento;
                        $cuota->fecha_documento = fecha_dmY($doc->fecha_documento);
                        $cuota->num_cuota = "CUOTA ". ($i+1) ."/". $cantidadMeses;
                        $cuota->periodo_doc = fechaPeriodoDocSaldoContrato($doc->periodo);
                        $cuota->monto_doc = '$ '.formatoMiles($doc->total_documento);
                        $cuotasContrato[] = $cuota;

                    }
                }
                
            }

        }

        /**
         * Se traen los documentos que tienen relacion para 
         * no considerarlos en el monto del contrato menos el monto de las recepciones
         */
        $documentosBodega = WsDocumento::with(['getWsOrdenCompra.getWsContrato'])
                                       ->whereHas('getWsOrdenCompra.getWsContrato',function ($query) use ($contrato) {
                                            // $query->whereIn('rut_proveedor', $contrato->getProveedor->getTodosLosRut());
                                            $query->where('rut_proveedor', $contrato->getProveedor->rut);
                                         })
                                       ->where('id_relacion', '<>', null)
                                       ->get()->pluck('documento')->toArray();

        // Buscar las recepciones de hermes, según el id del contrato, resolucion
        $recepcionesHermes = HermesDocumento::where('rut_proveedor', $contrato->getProveedor->rut )
                                            ->where('resolucion_contrato', $contrato->resolucion)
                                            ->get();

        foreach ( $recepcionesHermes as $recepcion ) {

            $cuota = new \stdClass;
            $cuota->recepciones = $recepcion->folio_documento;
            $cuota->totalRecepciones = '$ '.formatoMiles($recepcion->documento_total);
            $totalMontoRecepciones += $recepcion->documento_total;

            $cuota->orden_compra = '';
            $cuota->numero_documento = '';
            $cuota->fecha_documento = '';
            $cuota->num_cuota = '';
            $cuota->periodo_doc = '';
            $cuota->monto_doc = '';
            $cuotasContrato[] = $cuota;

        }

        $contrato->total_monto_recepciones = $totalMontoRecepciones;
        
        $content = \View::make('pdf.saldo_contrato', compact('contrato','cuotasContrato'))->render();

        $mpdf = new Mpdf([
            'format' => 'LETTER-L', // Carta horizontal
            'mirrorMargins' => true,
            'tempDir' => public_path() . '/tempMpdf'
            ]);
        //$mpdf = new Mpdf(['mode'=>'c']);
        $mpdf->SetDisplayMode('fullpage');
        $mpdf->keep_table_proportions = true;
        $mpdf->showImageErrors = true;
        $content = mb_convert_encoding($content, 'UTF-8', 'UTF-8');

        $mpdf->SetWatermarkImage(
            'logo-finanzas-navA.png',
            0.1,
            'F'
        );

        $mpdf->showWatermarkImage = true;

        $mpdf->WriteHTML($content);
        $path = public_path() . "/tempMpdf/";

        if ( $mostrar == true ) {
            $mpdf->Output( $path."SIFCON - Saldo Contrato N° $contrato->resolucion - DEPARTAMENTO FINANZAS - HSJD.pdf",'I');
        } else {
            $mpdf->Output( $path."SIFCON - Saldo Contrato $contrato->id - DEPARTAMENTO FINANZAS - HSJD.pdf",'F');
        }
        
        
    }
}