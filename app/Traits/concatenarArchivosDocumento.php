<?php namespace App\Traits;

use Mpdf\Mpdf;
use iio\libmergepdf\Merger;
use iio\libmergepdf\Driver\TcpdiDriver;
use App\Traits\saldoContrato;

use App\Documento;
use App\Archivo;
use App\WsArchivo;


trait concatenarArchivosDocumento
{
    /**
     * funcion para concatenar todos los archivos de un documento
     * en un orden establecido por la jefatura de finanzas
     */
    public static function concatenar($id, &$combinador, $paginaEnBlanco = false)
    {
        
        $documento = Documento::with([
                                'getArchivos.getTipoArchivo',
                                'getDocumentosRelacionados',
                                'getRelacionRecepcionesValidadas',
                                'getContratoOrdenCompra.getOrdenCompra.getArchivos',
                                'getContratoOrdenCompra.getContrato.getArchivos',
                            ])
                            ->findOrFail($id);

        /**
        * Orden de impresion para los documentos
        * 1° DTE y sus DTE relacionados, no considerar cartas certificadas (son ajustes contables) // id_tipo_archivo = 1, 2, 11, 12
        * 2° Acta de recepcion, Visto Bueno, ART, memo // Obtener de relacion con bodega -  id_tipo_archivo = 5, 4, 16
        * 3° Orden de Compra // id_tipo_archivo = 3
        * 4° Resolución de contrato // id_tipo_archivo = 6
        * 5° Saldo de contrato // id_tipo_archivo = 17
        * 6° Trazabilidad de documento (PDF que genera el sistema)
        */

        $archivosImprimir = collect();

        // 1°
        $dtesDocumento = Archivo::where('id_documento', $documento->id)->whereIn('id_tipo_archivo', [1,2,11,12])->get();
        foreach($dtesDocumento as $dte) {
            $archivosImprimir->push($dte);
        }

        $dtesDocumentoRelacionado = Archivo::where('id_documento', $documento->id_relacionado)->whereIn('id_tipo_archivo', [1,2,11,12])->get();
        foreach($dtesDocumentoRelacionado as $dte) {
            $archivosImprimir->push($dte);
        }

        foreach ($documento->getDocumentosRelacionados as $documentoRelacionado) {
            $dtesDocumentoRelacionado = Archivo::where('id_documento', $documentoRelacionado->id)->whereIn('id_tipo_archivo', [1,2,11,12])->get();
            foreach($dtesDocumentoRelacionado as $dte) {
                $archivosImprimir->push($dte);
            }
        }


        // 2°
        foreach ( $documento->getRelacionRecepcionesValidadas as $relacion ) {
            $segundaPrioridad = WsArchivo::where('id_documento', $relacion->id_ws_documento)->whereIn('id_tipo_archivo', [5,4,16])->get();
            foreach($segundaPrioridad as $archivo) {
                $archivosImprimir->push($archivo);
            }
        }

        $segundaPrioridad = Archivo::where('id_documento', $documento->id)->whereIn('id_tipo_archivo', [5,4,16])->get();
        foreach($segundaPrioridad as $archivo) {
            $archivosImprimir->push($archivo);
        }


        // 3°
        if ( $documento->getContratoOrdenCompra && $documento->getContratoOrdenCompra->getOrdenesCompra ){
            $terceraPrioridad = Archivo::where('id_orden_compra', $documento->getContratoOrdenCompra->id_orden_compra)
                                     ->whereIn('id_tipo_archivo', [3])->get();
        } else {
            $terceraPrioridad = Archivo::where('id_documento', $documento->id)->whereIn('id_tipo_archivo', [3])->get();
        }

        foreach($terceraPrioridad as $archivo) {
            $archivosImprimir->push($archivo);
        }
        

        foreach ( $documento->getRelacionRecepcionesValidadas as $relacion ) {
            $terceraPrioridad = WsArchivo::where('id_documento', $relacion->id_ws_documento)->whereIn('id_tipo_archivo', [3])->get();
            foreach($terceraPrioridad as $archivo) {
                $archivosImprimir->push($archivo);
            }
        }

        // 4°
        if ( $documento->getContratoOrdenCompra && $documento->getContratoOrdenCompra->getContrato ){
            $cuartaPrioridad = Archivo::where('id_contrato', $documento->getContratoOrdenCompra->id_contrato)
                                     ->whereIn('id_tipo_archivo', [6])->get();
        } else {
            $cuartaPrioridad = Archivo::where('id_documento', $documento->id)->whereIn('id_tipo_archivo', [6])->get();
        }
        
        foreach($cuartaPrioridad as $archivo) {
            $archivosImprimir->push($archivo);
        }

        // 5°

        if ( $documento->getContratoOrdenCompra && $documento->getContratoOrdenCompra->getContrato ){
            
            /**
             * PDF saldo del contrato (del documento)
             */
            saldoContrato::generarSaldo($documento->getContratoOrdenCompra->id_contrato, false);
            /**
             * Fin PDF saldo del contrato
             */

        } else {
            $quintaPrioridad = Archivo::where('id_documento', $documento->id)->whereIn('id_tipo_archivo', [17])->get();
            foreach($quintaPrioridad as $archivo) {
                $archivosImprimir->push($archivo);
            }
        }

      
        // Se concatenan todos los archivos encontrados
        foreach ($archivosImprimir as $archivo ) {

            if ( $archivo->cargado == 0 ) {

                if ( file_exists(  public_path().'/'.$archivo->ubicacion ) ) {

                    $combinador->addFile( public_path().'/'.$archivo->ubicacion );

                }

            } else {

                $combinador->addFile( "http://".$archivo->ubicacion);

            } 

        }

        $path = public_path() . "/tempMpdf/";
        // Se concatena saldo contrato
        if ( $documento->getContratoOrdenCompra && $documento->getContratoOrdenCompra->getContrato ) {

            if ( file_exists( $path."SIFCON - Saldo Contrato ".$documento->getContratoOrdenCompra->id_contrato." - DEPARTAMENTO FINANZAS - HSJD.pdf" ) ) {
                $combinador->addFile(  $path."SIFCON - Saldo Contrato ".$documento->getContratoOrdenCompra->id_contrato." - DEPARTAMENTO FINANZAS - HSJD.pdf" );
            }
                
        }

        // 6°
        /**
         * PDF traza documento.
         */
        $content = \View::make('pdf.trazabilidad_documento', compact('documento'))->render();
        $mpdf = new Mpdf([
            'format'=>'Letter-P',
            'mirrorMargins' => true,
            'tempDir' => public_path() . '/tempMpdf'
        ]);

        $mpdf->SetDisplayMode('fullpage');
        $mpdf->keep_table_proportions = true;
        $mpdf->showImageErrors = true;
        $content = mb_convert_encoding($content, 'UTF-8', 'UTF-8');
        $mpdf->WriteHTML($content);
        

        $mpdf->Output( $path."INFORME_TRAZABILIDAD_".$documento->id.".pdf" ,'F');
        /**
         * Fin PDF traza documento
         */

        $combinador->addFile( $path."INFORME_TRAZABILIDAD_".$documento->id.".pdf" );

        if ( $paginaEnBlanco == true ) {
            $content = \View::make('pdf.en_blanco')->render();
            $mpdf = new Mpdf([
                'format'=>'Letter-P',
                'mirrorMargins' => true,
                'tempDir' => public_path() . '/tempMpdf'
            ]);

            $mpdf->SetDisplayMode('fullpage');
            $mpdf->keep_table_proportions = true;
            $mpdf->showImageErrors = true;
            $content = mb_convert_encoding($content, 'UTF-8', 'UTF-8');
            $mpdf->WriteHTML($content);
            $path = public_path() . "/tempMpdf/";

            $mpdf->Output( $path."en_blanco_".$documento->id.".pdf" ,'F');
            /**
             * Fin PDF traza documento
             */
            $combinador->addFile( $path."en_blanco_".$documento->id.".pdf" );
        }

    }

    /**
     * Elimina los archivos dinamicos del documento, utiliza el id del doc
     */
    public static function eliminarArchivosDinamicos($id)
    {

        $path = public_path() . "/tempMpdf/";
        
        if ( file_exists( $path."INFORME_TRAZABILIDAD_".$id.".pdf" ) ) {
            unlink( $path."INFORME_TRAZABILIDAD_".$id.".pdf" ); // elimina archivo
        }

        if ( file_exists( $path."en_blanco_".$id.".pdf" ) ) {
            unlink( $path."en_blanco_".$id.".pdf" ); // elimina archivo
        }

        $documento = Documento::with([
                                    'getContratoOrdenCompra.getContrato',
                                ])
                                ->findOrFail($id);

        if ( $documento->getContratoOrdenCompra && $documento->getContratoOrdenCompra->getContrato ) {

            if ( file_exists( $path."SIFCON - Saldo Contrato ".$documento->getContratoOrdenCompra->id_contrato." - DEPARTAMENTO FINANZAS - HSJD.pdf" ) ) {
                unlink(  $path."SIFCON - Saldo Contrato ".$documento->getContratoOrdenCompra->id_contrato." - DEPARTAMENTO FINANZAS - HSJD.pdf" );
            }
                
        }
        
    }
}
