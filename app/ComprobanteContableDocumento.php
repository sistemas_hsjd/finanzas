<?php namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class ComprobanteContableDocumento extends Model
{
    use SoftDeletes;
    protected $table = 'comprobantes_contables_documentos';

    public function getDocumento()
    {
        return $this->hasOne('App\Documento','id','id_documento');
    }

    public function getComprobanteContable()
    {
        return $this->hasOne('App\ComprobanteContable','id','id_comprobante_contable');
    }
    

    public function getProveedor()
    {
        return $this->hasOne('App\Proveedor','id','id_proveedor');
    }

    public function getCuentaContable()
    {
        return $this->hasOne('App\CuentaContable','id','id_cuenta_contable');
    }

    public function getCuentaBancaria()
    {
        return $this->hasOne('App\CuentaBancaria','id','id_cuenta_bancaria');
    }

    public function getMedioPago()
    {
        return $this->hasOne('App\MedioPago','id','id_medio_pago');
    }
    
    /**
     * Se obtiene la información para dataTable.
     */
    public function getDatosDocumentosDelComprobanteContable()
    {

        $fechaDocumento = fecha_dmY($this->getDocumento->fecha_documento);
        $montoParaComprobante = '$ '.formatoMiles($this->monto_comprobante);
        $totalDocumento = '$ '.formatoMiles($this->monto_comprobante);
        $totalDocumentoActualizado = '$ '.formatoMiles($this->getDocumento->total_documento_actualizado);

        $botones = '<div class="btn-group">';

        // Trazabilidad Documento
        if ( \Entrust::can('ver-documento') ) {
            $botones .= '<button type="button" class="btn btn-primary btn-xs" title="Trazabilidad documento" onclick="traza('. $this->id_documento .');">';
            $botones .= '<i class="fas fa-map-signs fa-lg"></i></button>';
        }

        if ( $this->carga_sigfe != 1 ) {
            $botones .= '<button type="button" class="btn btn-danger btn-xs" title="Quitar documento del Comprobante" onclick="eliminarDoc('. $this->id_documento .');">';
            $botones .= '<i class="fas fa-trash"></i></button>';
        }

        $botones .= '</div>';

        $docJson = array(
            'DT_RowID' => $this->id_documento.'_'.$this->id,
            $this->getProveedor->rut.' '.$this->getProveedor->nombre,
            ( $this->getDocumento->getProveedorFactoring ) ? $this->getDocumento->getProveedorFactoring->rut.' '.
                                                            $this->getDocumento->getProveedorFactoring->nombre : '',
            $this->getDocumento->getTipoDocumento->nombre,
            $this->getDocumento->numero_documento,
            $this->getDocumento->getTipoInforme ? $this->getDocumento->getTipoInforme->nombre : '',
            $fechaDocumento,
            ( $this->getCuentaContable ) ? $this->getCuentaContable->codigo : '',
            ( $this->getCuentaBancaria ) ? $this->getCuentaBancaria->getBanco->nombre.'<br>'.$this->getCuentaBancaria->codigo : '',
            $montoParaComprobante,
            $totalDocumento,
            $totalDocumentoActualizado,
            $botones
        );

        return $docJson;
    }

}
