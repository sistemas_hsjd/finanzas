<?php namespace App;

use Illuminate\Database\Eloquent\Model;

class AceptaMotivoRechazo extends Model
{
    protected $table = 'acepta_motivo_rechazo';

    public function getMotivoRechazo()
    {
        return $this->belongsTo('App\MotivoRechazo','id_motivo_rechazo');
    }

    public function getArchivoAcepta()
    {
        return $this->belongsTo('App\ArchivoAcepta','id_archivo_acepta');
    }

}
