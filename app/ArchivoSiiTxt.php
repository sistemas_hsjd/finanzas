<?php namespace App;

use Illuminate\Database\Eloquent\Model;

class ArchivoSiiTxt extends Model
{
    protected $table = 'archivo_sii_txt';

    public function getUser()
    {
        return $this->belongsTo('App\User','id_usuario');
    }
}
