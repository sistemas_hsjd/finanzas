<?php namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class TipoComprobanteContable extends Model
{
    use SoftDeletes;
    protected $table = 'tipos_comprobantes_contables';

    
}
