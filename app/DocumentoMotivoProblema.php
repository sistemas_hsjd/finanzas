<?php namespace App;

use Illuminate\Database\Eloquent\Model;

class DocumentoMotivoProblema extends Model
{
    protected $table = 'documento_motivo_problema';

    public function getMotivoProblema()
    {
        return $this->belongsTo('App\MotivoProblema','id_motivo_problema');
    }

    public function getDocumento()
    {
        return $this->belongsTo('App\Documento','id_documento');
    }

}
