<?php namespace App\Helpers;

use Firebase\JWT\JWT;
use Illuminate\Support\Facades\DB;
use App\User;

/**
 * 
 */
class JwtAuth
{
	public $key;

	//function __construct(argument)
	function __construct()
	{
		$this->key = 'esta-es-la-clave-secreta-2019-qwertyñandù';
	}

	public function signup($rut, $password, $getToken = null) {
		$user = User::where(
			array(
				'rut' => $rut,
				'password' => $password
			))->first();
		
		$signup = false;
		if (is_object($user)) {
			$signup = true;
		}

		if ($signup) {

			//Generar el token y devolverlo
			$token = array(
				'sub' => $user->id,
                'rut' => $user->rut,
                'name' => $user->name,
				'email' => $user->email,
				'iat' => time(), //empieza el token,created_at
				'exp' => time() + (7 * 24 * 60 * 60) //se da una semana de duración para el token, expiración del token
			);

			$jwt = JWT::encode($token, $this->key, 'HS256');
			$decoded = JWT::decode($jwt, $this->key, array('HS256'));

			if (is_null($getToken)) {
				return $jwt;
			} else {
				return $decoded;
			}

		} else {

			//devolver un error
			return array('status' => 'error', 'message' => 'Login ha fallado !!');
		}
	}

	public function checkToken($jwt, $getIdentity = false) {

		$auth = false;
		try{
			$decoded = JWT::decode($jwt, $this->key, array('HS256'));
		} catch(\UnexpectedValueException $e) {
			$auth = false;
		} catch(\DomainException $e) {
			$auth = false;
		}

		if (isset($decoded) && is_object($decoded) && isset($decoded->sub)) {
			$auth = true;
		} else {
			$auth = false;
		}

		if ($getIdentity) {
			return $decoded;
		}

		return $auth;
	}
}
