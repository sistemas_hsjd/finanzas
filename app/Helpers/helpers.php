<?php

use Carbon\Carbon;

// Resive un JSON
function construyeFormulario($formJSON)
{
	// Para construir el formulario
	$form = new \stdClass;
	foreach ( $formJSON as $key => $value ) {
		// $formJSON[$key]->name = 'Nombre del inpunt';
		// $formJSON[$key]->value = 'Valor del inpunt';

		if ( strpos($formJSON[$key]->name, '[') !== false ) {
			// soy un arreglo
			$formJSON[$key]->name = str_replace( ['[',']'] ,'', $formJSON[$key]->name);
			$form->{$formJSON[$key]->name}[] = $formJSON[$key]->value;
		} else {
			$form->{$formJSON[$key]->name} = $formJSON[$key]->value;
		}

	}

	return $form;
}

function in_array_r($needle, $haystack, $strict = false) {
    foreach ($haystack as $item) {
        if (($strict ? $item === $needle : $item == $needle) || (is_array($item) && in_array_r($needle, $item, $strict))) {
            return true;
        }
    }

    return false;
}

function diaSemana($fecha_ingreso)
{
	$fechaComoEntero = strtotime($fecha_ingreso);
	setlocale(LC_ALL,"es_ES");
	return strftime('%A',$fechaComoEntero);
}

function getColorPorDiasCuadratura($fecha_ingreso)
{
	$dias = diferencia_dias($fecha_ingreso,date('Y-m-d'));
	$diasParaCuadrar = 8 - $dias;
	
	$color = '';
	if ($diasParaCuadrar == 2 || $diasParaCuadrar == 1) {
		$color = '#ff8300'; //naranjo
	} elseif ( $diasParaCuadrar == 3 || $diasParaCuadrar == 4 || $diasParaCuadrar == 5 ) {
		$color = '#e2e200f5'; //amarillo
	} elseif ( $diasParaCuadrar == 6 || $diasParaCuadrar == 7 || $diasParaCuadrar == 8 ) {
		$color = 'green';
	} else {
		$color = 'red';
	}
	
	return $color;
}

function diferencia_dias($fecha_i ,$fecha_f ){
	$dias	= (strtotime($fecha_i)-strtotime($fecha_f))/86400;
	$dias 	= abs($dias);
	$dias = floor($dias);
	return $dias;
}

function diferencia_dias_cuadratura($fecha_i ,$fecha_f ){
	$dias	= (strtotime($fecha_i)-strtotime($fecha_f))/86400;
	$dias 	= abs($dias);
	$dias = floor($dias);

	$diasCuadratura = 8 - $dias;
	return $diasCuadratura;
}

function getColorPorDiasParaPoderRechazar($emision)
{
	$dias = diferencia_dias($emision, date('Y-m-d'));
	$diasParaRechazar = 8 - $dias;
	
	$color = '';
	if ($diasParaRechazar == 2 || $diasParaRechazar == 1) {
		$color = '#ff8300'; //naranjo
	} elseif ( $diasParaRechazar == 3 || $diasParaRechazar == 4 || $diasParaRechazar == 5 ) {
		$color = '#e2e200f5'; //amarillo
	} elseif ( $diasParaRechazar == 6 || $diasParaRechazar == 7 || $diasParaRechazar == 8 ) {
		$color = 'green';
	} else {
		$color = 'red';
	}
	
	return $color;
}

function getDiasRestantestParaRechazar($emision)
{
	$dias = diferencia_dias($emision, date('Y-m-d'));

	$diasParaRechazar = 8 - $dias;
	return $diasParaRechazar;
}

function fecha_Y_m_d_h_m_s($fecha)
{   
    $fecha = str_replace('/','.',$fecha);
    /*
    $test = Carbon::createFromTimeStamp(strtotime($fecha))->format("Y-m-d");
    $test2 = "21.05.2019";
    dd($test,$fecha,Carbon::createFromTimeStamp(strtotime($test2))->format("Y-m-d"));
	*/
	return Carbon::createFromTimeStamp(strtotime($fecha))->format("Y-m-d H:i:s");
}

function fecha_Y_m_d($fecha)
{   
	$fecha = str_replace('/','.',$fecha);
    /*
    $test = Carbon::createFromTimeStamp(strtotime($fecha))->format("Y-m-d");
    $test2 = "21.05.2019";
    dd($test,$fecha,Carbon::createFromTimeStamp(strtotime($test2))->format("Y-m-d"));
    */
	return Carbon::createFromTimeStamp(strtotime($fecha))->format("Y-m-d");
}

function fecha_Y_m($fecha)
{   
    $fecha = str_replace('/','.',$fecha);
    
	return Carbon::createFromTimeStamp(strtotime($fecha))->format("Y-m");
}

function fecha_Y($fecha)
{   
	$fecha = str_replace('/','.',$fecha);
	return Carbon::createFromTimeStamp(strtotime($fecha))->format("Y");
}

function fecha_M($fecha)
{   
	$fecha = str_replace('/','.',$fecha);
	return Carbon::createFromTimeStamp(strtotime($fecha))->format("m");
}

function fecha_D($fecha)
{   
	$fecha = str_replace('/','.',$fecha);
	return Carbon::createFromTimeStamp(strtotime($fecha))->format("d");
}

function fecha_dmY_hms($fecha)
{
    return Carbon::createFromTimeStamp(strtotime($fecha))->format("d/m/Y H:i:s");
}

function fecha_dmY_hm($fecha)
{
    return Carbon::createFromTimeStamp(strtotime($fecha))->format("d/m/Y H:i");
}

function fecha_dmY($fecha)
{
	if ($fecha != null) {
		return Carbon::createFromTimeStamp(strtotime($fecha))->format("d/m/Y");
	} else {
		return '';
	}
}

function fecha_mY($fecha)
{
	if ($fecha != null) {
		return Carbon::createFromTimeStamp(strtotime($fecha))->format("m/Y");
	} else {
		return '';
	}
}

function fecha_dmY_sinSeparador($fecha)
{
	return Carbon::createFromTimeStamp(strtotime($fecha))->format("dmY");
}

function fechaHoraMinutoSegundo($fecha)
{
	return Carbon::createFromTimeStamp(strtotime($fecha))->format("H:i:s");
}

function fechaMemo($fecha) 
{
	$dia = Carbon::createFromTimeStamp(strtotime($fecha))->format("d");
	$mes = getMes(Carbon::createFromTimeStamp(strtotime($fecha))->format("m"));
	$year = Carbon::createFromTimeStamp(strtotime($fecha))->format("Y");
	return "$dia de $mes $year";
}

function fechaSaldoContrato($fecha)
{
	$mes = getMes(Carbon::createFromTimeStamp(strtotime($fecha))->format("m"));
	$year = Carbon::createFromTimeStamp(strtotime($fecha))->format("Y");
	return "$mes $year";
}

function fechaPeriodoDocSaldoContrato($fecha)
{
	$mes = getMesAbreviado(Carbon::createFromTimeStamp(strtotime($fecha))->format("m"));
	$year = Carbon::createFromTimeStamp(strtotime($fecha))->format("Y");
	return "$mes - $year";
}

function getMes($num=0) 
{
	switch ($num) {
		case '1':
			return 'Enero';
			break;
		case '2':
			return 'Febrero';
			break;
		case '3':
			return 'Marzo';
			break;
		case '4':
			return 'Abril';
			break;
		case '5':
			return 'Mayo';
			break;
		case '6':
			return 'Junio';
			break;
		case '7':
			return 'Julio';
			break;
		case '8':
			return 'Agosto';
			break;
		case '9':
			return 'Septiembre';
			break;
		case '10':
			return 'Octubre';
			break;
		case '11':
			return 'Noviembre';
			break;
		case '12':
			return 'Diciembre';
			break;
		default:
			return '';
			break;
	}
}

function getMesAbreviado($num=0) 
{
	switch ($num) {
		case '1':
			return 'Ene';
			break;
		case '2':
			return 'Feb';
			break;
		case '3':
			return 'Mar';
			break;
		case '4':
			return 'Abr';
			break;
		case '5':
			return 'May';
			break;
		case '6':
			return 'Jun';
			break;
		case '7':
			return 'Jul';
			break;
		case '8':
			return 'Ago';
			break;
		case '9':
			return 'Sep';
			break;
		case '10':
			return 'Oct';
			break;
		case '11':
			return 'Nov';
			break;
		case '12':
			return 'Dic';
			break;
		default:
			return '';
			break;
	}
}

function redondeadoDecimales($numero, $decimales)
{
    $factor = pow( 10 , $decimales ); 
    return ( round( $numero * $factor ) / $factor ); 
}

function pesoArchivoEnMB($peso)
{
    //math equation="nr_peso/(1024*1024)" nr_peso=$item_acepta.nr_peso format="%.2f"} MB
    $resultado = redondeadoDecimales( $peso/(1024*1024) , 2 );
    return "$resultado MB";
}

function mayusculas_acentos($string)
{
    $string = trim($string);

    $string = str_replace(
        array('á','é','í','ó','ú','ñ'),
        array('Á','É','Í','Ó','Ú','Ñ'),
        $string
    );

    return $string;
}

function formato_entero($number)
{
	return str_replace(',', '', str_replace('.', '', $number));
}

function formatoMiles($number)
{
	if ($number != null) {
		$number = number_format($number, 0, '', '.');
	} else {
		// $number = '';
		$number = 0;
	}
	
	return $number;
}

function formato_decimales($number)
{
	return str_replace(',', '.', str_replace('.', '', $number));
}

function formatoMilesDecimal($number)
{
	if ($number != null) {
		$number = number_format($number, 1, ',', '.');
	} else {
		// $number = '';
		$number = 0;
	}
	
	return $number;
}

function cantidadMesesEntreFechas($fechaUno, $fechaDos)
{
	$f1 = new DateTime( $fechaUno );
	$f2 = new DateTime( $fechaDos );
	  
	$intervalo = $f1->diff($f2);
	// $diferenciaMeses = $intervalo->format('%m');
	// $diferenciaAños = $intervalo->format('%y') * 12;

	// if ( $diferenciaAños > 0 ) {

	// 	$f2 = new DateTime( fecha_Y($fechaUno).'-12-31' ); // y-m-d
	// 	$intervalo = $f1->diff($f2);
	// 	$diferenciaMeses = $intervalo->format('%m');
		
	// }

	// $diferenciaTotal = $diferenciaMeses + $diferenciaAños;
	$diferenciaTotal = ( $intervalo->y * 12) + $intervalo->m;
	
	return $diferenciaTotal;
}