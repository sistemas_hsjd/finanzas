<?php namespace App;

use Illuminate\Database\Eloquent\Model;
use Freshwork\ChileanBundle\Rut;

class WsContrato extends Model
{
    protected $table = 'ws_contrato';

    public function __construct( $paramsContrato = null, array $attributes = array())
    {
        parent::__construct($attributes);
        if ( $paramsContrato != null ) {
            // Datos Requeridos
            $this->nombre_proveedor = $paramsContrato->nombre_proveedor;
            if ( Rut::parse( $paramsContrato->rut_proveedor )->validate() ) {

                $this->rut_proveedor = Rut::parse( $paramsContrato->rut_proveedor )->format(Rut::FORMAT_WITH_DASH);

            } else {
                $this->rut_proveedor = $paramsContrato->rut_proveedor;
            }
            

            // Datos Opcionales
            //verifica si existe el dato
            if ( @$paramsContrato->id_licitacion != null ) {
                $this->id_licitacion = $paramsContrato->id_licitacion;
            }

            if ( @$paramsContrato->fecha_ingreso_contrato != null ) {
                $this->fecha_ingreso_contrato = $paramsContrato->fecha_ingreso_contrato;
            }
            
            if ( @$paramsContrato->resolucion != 0 || @$paramsContrato->resolucion != null ) {
                $this->resolucion = $paramsContrato->resolucion;
            }

            if ( @$paramsContrato->fecha_resolucion != null ) {
                $this->fecha_resolucion = $paramsContrato->fecha_resolucion;
            }

            if ( @$paramsContrato->fecha_inicio_contrato != null ) {
                $this->fecha_inicio_contrato = $paramsContrato->fecha_inicio_contrato;
            }

            if ( @$paramsContrato->fecha_termino_contrato != null ) {
                $this->fecha_termino_contrato = $paramsContrato->fecha_termino_contrato;
            }
            
            if ( @$paramsContrato->monto_maximo != 0 || @$paramsContrato->monto_maximo != null ) {
                $this->monto_maximo = $paramsContrato->monto_maximo;
            }

            if ( @$paramsContrato->monto_licitacion != 0 || @$paramsContrato->monto_licitacion != null ) {
                $this->monto_licitacion = $paramsContrato->monto_licitacion;
            }

            if ( @$paramsContrato->saldo_licitacion != 0 || @$paramsContrato->saldo_licitacion != null ) {
               $this->saldo_licitacion = $paramsContrato->saldo_licitacion;
            }

            if ( @$paramsContrato->unidad_licitacion != null ) {
                $this->unidad_licitacion = $paramsContrato->unidad_licitacion;
            }

            if ( @$paramsContrato->referente_tecnico != null ) {
                $this->referente_tecnico = $paramsContrato->referente_tecnico;
            }

            
            $this->save();
        }
        
    }

    public function actualizaContrato($paramsContrato)
    {
        if ( $paramsContrato != null ) {
            // Datos Requeridos
            $this->nombre_proveedor = $paramsContrato->nombre_proveedor;
            
            if ( Rut::parse( $paramsContrato->rut_proveedor )->validate() ) {
                $this->rut_proveedor = Rut::parse( $paramsContrato->rut_proveedor )->format(Rut::FORMAT_WITH_DASH);
            } else {
                $this->rut_proveedor = $paramsContrato->rut_proveedor;
            }

            // Datos Opcionales
            //verifica si existe el dato
            if ( @$paramsContrato->id_licitacion != null ) {
                $this->id_licitacion = $paramsContrato->id_licitacion;
            }

            if ( @$paramsContrato->fecha_ingreso_contrato != null ) {
                $this->fecha_ingreso_contrato = $paramsContrato->fecha_ingreso_contrato;
            }
            
            if ( @$paramsContrato->resolucion != 0 || @$paramsContrato->resolucion != null ) {
                $this->resolucion = $paramsContrato->resolucion;
            }

            if ( @$paramsContrato->fecha_resolucion != null ) {
                $this->fecha_resolucion = $paramsContrato->fecha_resolucion;
            }

            if ( @$paramsContrato->fecha_inicio_contrato != null ) {
                $this->fecha_inicio_contrato = $paramsContrato->fecha_inicio_contrato;
            }

            if ( @$paramsContrato->fecha_termino_contrato != null ) {
                $this->fecha_termino_contrato = $paramsContrato->fecha_termino_contrato;
            }
            
            if ( @$paramsContrato->monto_maximo != 0 || @$paramsContrato->monto_maximo != null ) {
                $this->monto_maximo = $paramsContrato->monto_maximo;
            }

            if ( @$paramsContrato->monto_licitacion != 0 || @$paramsContrato->monto_licitacion != null ) {
                $this->monto_licitacion = $paramsContrato->monto_licitacion;
            }

            if ( @$paramsContrato->saldo_licitacion != 0 || @$paramsContrato->saldo_licitacion != null ) {
               $this->saldo_licitacion = $paramsContrato->saldo_licitacion;
            }

            if ( @$paramsContrato->unidad_licitacion != null ) {
                $this->unidad_licitacion = $paramsContrato->unidad_licitacion;
            }

            if ( @$paramsContrato->referente_tecnico != null ) {
                $this->referente_tecnico = $paramsContrato->referente_tecnico;
            }
            
            $this->save();
        }
    }

    /**
     * Funcion para guardar los datos del contrato.
     * Trae parametros desde la API
     */
    public function saveContrato($paramsContrato)
    {
        $this->id_licitacion = $paramsContrato->id_licitacion;
        $this->fecha_ingreso_contrato = $paramsContrato->fecha_ingreso_contrato;
        $this->resolucion = $paramsContrato->resolucion;
        $this->fecha_resolucion = $paramsContrato->fecha_resolucion;
        $this->nombre_proveedor = $paramsContrato->nombre_proveedor;
        $this->rut_proveedor = $paramsContrato->rut_proveedor;
        $this->fecha_inicio_contrato = $paramsContrato->fecha_inicio_contrato;
        $this->fecha_termino_contrato = $paramsContrato->fecha_termino_contrato;
        $this->monto_maximo = $paramsContrato->monto_maximo;
        $this->save();
    }

    public function getWsOrdenCompra()
    {        
        return $this->hasOne('App\WsOrdenCompra','id_contrato','id');
    }
}
