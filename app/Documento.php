<?php namespace App;

use Illuminate\Database\Eloquent\Model;
use Awobaz\Compoships\Compoships;
use Illuminate\Database\Eloquent\SoftDeletes;
use OwenIt\Auditing\Contracts\Auditable;
use App\WsDocumento;
use App\ArchivoAcepta;
use App\FolioSigfe;
use App\Devengo;
use App\DocumentoMotivoProblema;
use App\DocumentoContratoOrdenCompra;
use App\ComprobanteContable;
use App\HermesDocumento;
use App\User;

class Documento extends Model implements Auditable
{
    use \OwenIt\Auditing\Auditable;
    use SoftDeletes;
    use Compoships;
    protected $table = 'documentos';

    /**
     * Attributes to include in the Audit.
     *
     * @var array
     */
    protected $auditInclude = [];
    
    /**
     * Should the timestamps be audited?
     *
     * @var bool
     */
    protected $auditTimestamps = true;

    public function __construct( $request = null, array $attributes = array())
    {
        parent::__construct($attributes);
        if ( $request != null ) {
            if ( $request->input('periodo') ) {
                $this->periodo = fecha_Y_m_d( '01/'.$request->input('periodo') );
            }
            $this->fecha_recepcion = fecha_Y_m_d($request->input('fecha_recepcion'));
            $this->id_proveedor = $request->input('nombre_proveedor'); // Trae el id del proveedor

            // Para dar numero de documento automatico a las cartas certificadas.
            if ( $request->input('tipo_documento') == 10 || $request->input('tipo_documento') == 11 ) {
                $lastCarta = Documento::whereIn('id_tipo_documento', [10,11])->orderBy('created_at','desc')->first();
                $this->numero_documento = $lastCarta->numero_documento + 10000;
            } else {
                $this->numero_documento = $request->input('numero_documento'); // Equivalente al folio
            }
            

            $this->id_tipo_documento = $request->input('tipo_documento');
            $this->fecha_documento = fecha_Y_m_d($request->input('fecha_documento'));
            $this->total_documento = formato_entero($request->input('valor_total_documento'));
            $this->total_documento_actualizado = $this->total_documento;

            $this->id_modalidad_compra = $request->input('modalidad_compra');
            $this->id_tipo_adjudicacion = $request->input('tipo_adjudicacion');
            $this->documento_compra = $request->input('numero_orden_compra');
            $this->id_tipo_informe = $request->input('tipo_informe');

            $this->id_responsable =  \Auth::user()->id; // si se carga mediante acepta, se cambia esto en el controlador de acepta, al momento de ingresar el documento

            $this->id_referente_tecnico = $request->input('referente_tecnico');
            $this->licitacion = trim($request->input('numero_licitacion'));
            $this->observacion = trim($request->input('observacion_documento'));
            $this->id_digitador = \Auth::user()->id;

            /**
             * Refactura Documentos ingresados en el sistema.
             * La refactura es con documento del tipo factura (id_tipo_documento == 1)
             * los otros documentos rebajan el monto de la factura (NC = 4 y  CC = 10)
             * los otros documentos aumentan el monto de la factura (ND = 5 y CD = 11)
             */
            if ( $request->input('reemplaza') && $request->input('reemplaza') != '' ) {

                $this->id_relacionado = $request->input('reemplaza');
                $documentoReemplaza = Documento::with(['getDocumentosRelacionados'])
                                               ->findOrFail($request->input('reemplaza'));

                if ( $this->id_tipo_documento == 1 && $documentoReemplaza->id_tipo_documento == 1 && $documentoReemplaza->total_documento_actualizado == 0 ) {
                    // Factura que tiene monto 0, por NCs, por lo tanto la factura esta anulada

                    $this->observacion = $this->observacion.PHP_EOL.' Re-facturación de Factura '.$documentoReemplaza->numero_documento.', anulada con la(s) Nota(s) de Crédito ';

                    if ( $documentoReemplaza->getDocumentosRelacionados->count() > 0 ) {
                        foreach ($documentoReemplaza->getDocumentosRelacionados as $docNc) {
                            if ( $docNc->id_tipo_documento == 4 ) {
                                $this->observacion = $this->observacion.' '.$docNc->numero_documento;
                            }
                        }
                    }
                    
                }

                // Para actualizar el total_documento_actualizado
                if ( $this->id_tipo_documento == 4 || $this->id_tipo_documento == 10 ) {
                    // NC y CC, resta al total
                    $documentoReemplaza->total_documento_actualizado -= $this->total_documento;
                    $documentoReemplaza->save();
                }

                if ( $this->id_tipo_documento == 5 || $this->id_tipo_documento == 11 ) {
                    // ND y CD, suma al total
                    $documentoReemplaza->total_documento_actualizado += $this->total_documento;
                    $documentoReemplaza->save();
                }

                // Para pasar directo a poder devengar las NC, ND y factura
                if ( $this->id_tipo_documento == 4 || $this->id_tipo_documento == 5 ) {
                    $this->directo_por_devengar = 1;
                    $documentoReemplaza->directo_por_devengar = 1;
                    $documentoReemplaza->save();
                }
                
            }

            // Refactura documentos rechazados de acepta
            // la factura rechazada debe tener motivo de rechazo "anula" (id == 5)
            if ( $request->input('reemplaza_archivo_acepta') && $request->input('reemplaza_archivo_acepta') != '' ) {

                $this->id_archivo_acepta_refacturado = $request->input('reemplaza_archivo_acepta');
                $archivoAcepta = ArchivoAcepta::select('id','folio')->findOrFail( $request->input('reemplaza_archivo_acepta') );
                $this->observacion = $this->observacion.PHP_EOL.' Re-facturación de Factura '.$archivoAcepta->folio.' (archivo acepta rechazado)';

            }

            // Para pasar el documento directamente a por devengar
            if ( $request->get('directo_por_devengar') ) {
                $this->directo_por_devengar = 1;
            }

            // Se agregar los campos para la boleta, si el tipo de documento es boleta afecta.
            if ( $request->get('tipo_documento') == 3 ) {
                
                $this->liquido = formato_entero($request->get('liquido'));
                $this->impuesto = formato_entero($request->get('impuesto'));

            }
            
            $this->id_user_created = \Auth::user()->id;
            $this->updated_at = NULL;
            $this->setObservacionDocumentoIngreso();
            $this->dias_antiguedad = diferencia_dias( $this->fecha_recepcion, date('Y-m-d') );
            if ( $this->id_tipo_informe == 6 || $this->id_tipo_informe == 7 || $this->id_tipo_informe == 8 ) {
                // Honorarios, Fondo fijo y Liquido anticipo
                $this->validar(); // deben ingresar cuadrados al sistema.
            }
            $this->save();
            $this->quitaValidacionFactura();
        }
    }

    public function editDocumento($request)
    {
        $this->actualizarTotalDocumentoActualizadoDelDocumentoRelacionado();

        if ( $request->input('periodo') ) {
            $this->periodo = fecha_Y_m_d( '01/'.$request->input('periodo') );
        }

        $this->fecha_recepcion = fecha_Y_m_d($request->input('fecha_recepcion'));
        $this->id_proveedor = $request->input('nombre_proveedor'); // Trae el id del proveedor
        $this->numero_documento = $request->input('numero_documento'); // Equivalente al folio

        if ( $this->id_tipo_documento != $request->input('tipo_documento') ) {
            
            $this->id_tipo_documento = $request->input('tipo_documento');
            // En caso que un documento pase a ser NC o ND, se aplica la logica...
            $this->quitaValidacionFactura();

        }
        
        $this->fecha_documento = fecha_Y_m_d($request->input('fecha_documento'));

        // if ( $this->total_documento != formato_entero($request->input('valor_total_documento')) ) {

            $this->total_documento = formato_entero($request->input('valor_total_documento'));
             // Los documentos a los que se le puede aplicar NC,ND, CCC, CCD tiene documentos relacionados
            if ( $this->getDocumentosRelacionados->count() > 0 ) {
                $this->total_documento_actualizado = $this->total_documento;
                // Se actualiza el monto segun los documentos que tiene relacionado el documento editado
                foreach ( $this->getDocumentosRelacionados as $docRelacionado ){

                    if ( $docRelacionado->id_tipo_documento == 4 || $docRelacionado->id_tipo_documento == 10 ) {
                        // NC o CC, resta al total
                        $this->total_documento_actualizado -= $docRelacionado->total_documento;
                    }
                    if ( $docRelacionado->id_tipo_documento == 5 || $docRelacionado->id_tipo_documento == 11 ) {
                        // ND o CD, suma al total
                        $this->total_documento_actualizado += $docRelacionado->total_documento;
                    }

                }
            } else {
                $this->total_documento_actualizado = $this->total_documento;
            }

        // }

        $this->id_modalidad_compra = $request->input('modalidad_compra');
        $this->id_tipo_adjudicacion = $request->input('tipo_adjudicacion');
        $this->documento_compra = $request->input('numero_orden_compra');
        $this->id_tipo_informe = $request->input('tipo_informe');
        
        $this->id_referente_tecnico = $request->input('referente_tecnico');
        $this->licitacion = trim($request->input('numero_licitacion'));
        $this->observacion = trim($request->input('observacion_documento'));

        // Refactura Documentos ingresados en el sistema.
        // La refactura es con documento del tipo factura (id_tipo_documento == 1)
        // los otros documentos rebajan el monto de la factura (NC y ND, CCC y CCD)
        if ( $request->input('reemplaza') && $request->input('reemplaza') != '' ) {

            if ( $this->id_relacionado != $request->input('reemplaza') ) {

                $this->id_relacionado = $request->input('reemplaza');
                $documentoReemplaza = Documento::with(['getDocumentosRelacionados'])->findOrFail($request->input('reemplaza'));

                if ( $this->id_tipo_documento == 1 && $documentoReemplaza->id_tipo_documento == 1 && $documentoReemplaza->total_documento_actualizado == 0 ) {
                    // Factura que tiene monto 0, por NCs, por lo tanto la factura esta anulada

                    $this->observacion = $this->observacion.PHP_EOL.' Re-facturación de Factura '.$documentoReemplaza->numero_documento.', anulada con la(s) Nota(s) de Crédito ';
                    
                    if ( $documentoReemplaza->getDocumentosRelacionados->count() > 0 ) {
                        foreach ($documentoReemplaza->getDocumentosRelacionados as $docNc) {
                            if ( $docNc->id_tipo_documento == 4 ) {
                                $this->observacion = $this->observacion.' '.$docNc->numero_documento;
                            }
                        }
                    }
                    
                }

                // Para actualizar el total_documento_actualizado
                if ( $this->id_tipo_documento == 4 || $this->id_tipo_documento == 10 ) {
                    // NC o CC, resta al total
                    $documentoReemplaza->total_documento_actualizado -= $this->total_documento;
                    $documentoReemplaza->save();
                }

                if ( $this->id_tipo_documento == 5 || $this->id_tipo_documento == 11 ) {
                    // ND o CD, suma al total
                    $documentoReemplaza->total_documento_actualizado += $this->total_documento;
                    $documentoReemplaza->save();
                }

            } else {

                // Es el mismo documento relacionado, puede ser la misma factura a la que se le aplica NC o ND
                if ( $this->getDocumentoRelacionado != null ) {

                    // Para actualizar el total_documento_actualizado del documento relacionado
                    if ( $this->id_tipo_documento == 4 || $this->id_tipo_documento == 10 ) {
                        // NC o CC, resta al total
                        $this->getDocumentoRelacionado->total_documento_actualizado -= $this->total_documento;
                        $this->getDocumentoRelacionado->save();
                    }

                    if ( $this->id_tipo_documento == 5 || $this->id_tipo_documento == 11 ) {
                        // ND o CD, suma al total
                        $this->getDocumentoRelacionado->total_documento_actualizado += $this->total_documento;
                        $this->getDocumentoRelacionado->save();
                    }

                }

            }

            

        } else {

            if ( $this->id_relacionado != null ) {

                $documentoReemplaza = Documento::with(['getDocumentosRelacionados'])->findOrFail($this->id_relacionado);

                if ( $this->id_tipo_documento == 1 && $documentoReemplaza->id_tipo_documento == 1 && $documentoReemplaza->total_documento_actualizado == 0 ) {
                    // Factura que tiene monto 0, por NCs, por lo tanto la factura esta anulada
    
                    $this->observacion = $this->observacion.PHP_EOL.'Se Anula la Re-facturación de Factura '.$documentoReemplaza->numero_documento.', anulada con la(s) Nota(s) de Crédito ';
                    
                    if ( $documentoReemplaza->getDocumentosRelacionados->count() > 0 ) {
                        foreach ($documentoReemplaza->getDocumentosRelacionados as $docNc) {
                            if ( $docNc->id_tipo_documento == 4 ) {
                                $this->observacion = $this->observacion.' '.$docNc->numero_documento;
                            }
                        }
                    }
                     
                }

                $this->id_relacionado = null;

            }

        }


        // Refactura documentos rechazados de acepta
        // la factura rechazada debe tener motivo de rechazo "anula" (id == 5)
        if ( $request->input('reemplaza_archivo_acepta') && $request->input('reemplaza_archivo_acepta') != '' ) {

            $this->id_archivo_acepta_refacturado = $request->input('reemplaza_archivo_acepta');
            $archivoAcepta = ArchivoAcepta::select('id','folio')->findOrFail( $request->input('reemplaza_archivo_acepta') );
            $this->observacion = $this->observacion.PHP_EOL.' Re-facturación de Factura '.$archivoAcepta->folio.' (archivo acepta rechazado)';

        }

        $this->id_user_updated = \Auth::user()->id;

        if ( $request->input('datatable') == 'pendientes_validacion' ) {

            $this->validar();
            if ( $this->id_user_problema != null ) {
                // Se quita el problema del documento, y pasa a estar validado
                $this->observacion_problema = $this->observacion_problema.PHP_EOL.\Auth::user()->name.': Quita el documento del listado "Con Problemas"';
                $this->id_user_problema = null;
                $this->id_user_quita_problema = \Auth::user()->id;
            }

        }

        if ( $request->input('datatable') == 'validados' ) {
            $this->invalidar();
        }

        // Se agregar los campos para la boleta, si el tipo de documento es boleta afecta.
        if ( $request->get('tipo_documento') == 3 ) {
                
            $this->liquido = formato_entero($request->get('liquido'));
            $this->impuesto = formato_entero($request->get('impuesto'));
            
        } else {

            $this->liquido = null;
            $this->impuesto = null;

        }

        // $this->pasarDirectoPorDevengar();

        $this->actualizaValidacionDocumentosRelacionados();
        $this->save();
    }

    /**
     * Para pasar ciertos documentos a directo por devengar
     */
    public function pasarDirectoPorDevengar()
    {
        if ( $this->directo_por_devengar != null) {
            /**
             * Tipo documento : Boletas.
             * Tipo informe :
             * cenabast, clinicas, convenio, consumo basico, 
             * honorarios, fondo fijo, liquida anticipo, gob transparente.
             */
            if ( $this->id_tipo_documento == 3 || $this->id_tipo_documento == 9 ||
                 $this->id_tipo_informe == 2 || $this->id_tipo_informe == 3 || 
                 $this->id_tipo_informe == 4 || $this->id_tipo_informe == 5 ||
                 $this->id_tipo_informe == 6 || $this->id_tipo_informe == 7 ||
                 $this->id_tipo_informe == 8 || $this->id_tipo_informe == 9
               ) {
                   $this->directo_por_devengar = 1;
               }
        }
    }

    /**
     * Actualiza la validacion de los documentos relacionados, como 
     * NC,ND, CCC, CCD
     * Si el documento principal es validado == null,
     *    se quita el validado a los docs relacionados.
     * Si el documento es validado != null,
     *    se validan los docs relacionados.
     */
    public function actualizaValidacionDocumentosRelacionados()
    {
        if ( $this->validado == null ) {

            // Quita el validado de los documentos relacionados
            if ( $this->getDocumentosRelacionados->count() > 0 ) {
                foreach ( $this->getDocumentosRelacionados as $docRelacionado ) {
                    $docRelacionado->invalidar();
                }
            }

        } else {

             // Validar los documentos relacionados
            if ( $this->getDocumentosRelacionados->count() > 0 ) {
                foreach ( $this->getDocumentosRelacionados as $docRelacionado ) {
                    $docRelacionado->validar();
                }
            }

        }
    }

    /**
     * Si se ingresa una nota de credito, nota debito,carta certificada de credito o
     * carta certificada de debito, se quita la validacion de la factura.
     * Tambien se quita la relacion con recepciones que puede tener la factura.
     * Para dar aviso que se ha ingresado un documento que afecta al valor de la factura.
     * El usuario puede ver este cambio en el listado de documentos pendientes de validacion.
     */
    public function quitaValidacionFactura()
    {   

        if (
             ( $this->id_tipo_documento == 4 || $this->id_tipo_documento == 5 || 
               $this->id_tipo_documento == 10 || $this->id_tipo_documento == 11 
             ) && 
             $this->getDocumentoRelacionado != null 
            ) {

            foreach ( $this->getDocumentoRelacionado->getRelacionRecepcionesValidadas as $relacion ) {
                // Se elimiman las relaciones
                $relacion->getWsDocumento->id_relacion = null;
                $relacion->getWsDocumento->save();

                $relacion->delete();
            }

            $this->getDocumentoRelacionado->id_relacion = null;
            
            foreach ($this->getDocumentoRelacionado->getRelacionRecepcionesNoValidadas as $relacionHermes ) {
                $relacionHermes->delete(); // se eliminan las relaciones
            }


            $this->getDocumentoRelacionado->invalidar();
            $this->getDocumentoRelacionado->save();
            $this->getDocumentoRelacionado->actualizaValidacionDocumentosRelacionados();

        }

    }

    public function editDocumentoDevengar($request)
    {
        // Borrar las columnas de los documentos
        // $this->id_sigfe = trim($request->input('id_sigfe'));
        // $this->fecha_sigfe = fecha_Y_m_d($request->input('fecha_id'));
        // $this->year_sigfe = fecha_Y_m_d($request->input('fecha_id'));
        // $this->detalle = trim($request->input('detalle_documento'));

        /**
         * Se debe buscar si el documento ya tiene un folio sigfe para el mes y año ingresados.
         * Si tiene uno, se edita. 
         * Si no tiene uno, se crea.
         */
        $folioSigfe = FolioSigfe::where('id_documento', $this->id)
                                ->where('year_sigfe', fecha_Y($request->input('fecha_id')) )
                                ->where('mes_sigfe', fecha_M($request->input('fecha_id')) )
                                ->first();

        if ( is_object($folioSigfe) ) {

            $folioSigfe->id_sigfe = trim($request->get('id_sigfe'));
            $folioSigfe->fecha_sigfe = fecha_Y_m_d( $request->input('fecha_id') );
            $folioSigfe->dia_sigfe = fecha_D( $request->input('fecha_id') );
            $folioSigfe->mes_sigfe = fecha_M( $request->input('fecha_id') );
            $folioSigfe->year_sigfe = fecha_Y( $request->input('fecha_id') );
            $folioSigfe->detalle_sigfe = trim($request->input('detalle_documento'));
            $folioSigfe->save();
            $folioSigfe->estadoFolio = 'editado';

        } else {

            $folioSigfe = new FolioSigfe;
            $folioSigfe->id_documento = $this->id;
            $folioSigfe->id_sigfe = trim($request->input('id_sigfe'));
            $folioSigfe->fecha_sigfe = fecha_Y_m_d( $request->input('fecha_id') );
            $folioSigfe->dia_sigfe = fecha_D( $request->input('fecha_id') );
            $folioSigfe->mes_sigfe = fecha_M( $request->input('fecha_id') );
            $folioSigfe->year_sigfe = fecha_Y( $request->input('fecha_id') );
            $folioSigfe->detalle_sigfe = trim($request->input('detalle_documento'));
            $folioSigfe->save();
            $folioSigfe->estadoFolio = 'nuevo';

        }


        $this->observacion = trim($request->input('observacion_documento'));
        $this->id_devengador = \Auth::user()->id;
        $this->id_estado = 2;
        $this->fecha_devengado = date('Y-m-d H:i:s'); // Esto puede que no sea necesario

        $this->id_user_updated = \Auth::user()->id;
        $this->save();

        return $folioSigfe;
    }

    public function devengarItemsPresupuestarios($request, $folioSigfe)
    {
        $arregloIdItems = []; // Se guardaran los id_item que vienen, para eliminar los quitados en la vista
        if ( $this->getFoliosSigfe->count() == 1 ) {

            foreach ($request->input('valor_item') as $id_item => $valor) {

                $arregloIdItems[] = $id_item;

                $devengo = Devengo::where('id_documento', $this->id)
                                  ->where('id_item_presupuestario', $id_item);

                if ( $folioSigfe->estadoFolio == 'editado' ) {
                    $devengo = $devengo->where('id_folio_sigfe', $folioSigfe->id);
                } else {
                    $devengo = $devengo->where('id_folio_sigfe', null);
                }

                $devengo = $devengo->first();
                
                if ( is_object($devengo) ) {

                    $devengo->monto = formato_entero($valor);
                    if ( $folioSigfe->estadoFolio == 'nuevo' && $devengo->id_folio_sigfe == null ) {
                        $devengo->id_folio_sigfe = $folioSigfe->id;
                    }
                    $devengo->save();

                } else {

                    if ( formato_entero($valor) > 0 ) {
                        $devengo = new Devengo();
                        $devengo->id_documento = $this->id;
                        $devengo->id_folio_sigfe = $folioSigfe->id;
                        $devengo->id_item_presupuestario = $id_item;
                        $devengo->monto = formato_entero($valor);
                        $devengo->save();
                    }

                }
                
            }

            /**
             * Se buscan los devengo que pertenecen al documento (y al folio sigfe en caso que se este editando),
             * pero que no estan en el arregloIdItems. Son los que se tienen que eliminar
             */
            
            $devengosDelete = Devengo::where('id_documento','=', $this->id)
                                     ->whereNotIn('id_item_presupuestario', $arregloIdItems);

            if ( $folioSigfe->estadoFolio == 'editado' ) {
                $devengosDelete = $devengosDelete->where('id_folio_sigfe', $folioSigfe->id);
            } else {
                $devengosDelete = $devengosDelete->where('id_folio_sigfe', null);
            }
            
            $devengosDelete = $devengosDelete->get();
                                    
            foreach ($devengosDelete as $devengoDelete){
                $devengoDelete->delete();
            }

        } else if ( $this->getFoliosSigfe->count() > 1 ) {

            foreach ($request->input('valor_item') as $id_item => $valor) {

                $arregloIdItems[] = $id_item;

                if ( $folioSigfe->estadoFolio == 'editado' ) {

                    $devengo = Devengo::where('id_folio_sigfe', $folioSigfe->id)
                                      ->where('id_item_presupuestario', $id_item)
                                      ->first();

                    if ( is_object($devengo) ) {

                        $devengo->monto = formato_entero($valor);
                        $devengo->save();

                    } else {

                        if ( formato_entero($valor) > 0 ) {
                            $devengo = new Devengo();
                            // $devengo->id_documento = $this->id; // No es necesario el id_documento, porque ya esta devengado
                            $devengo->id_folio_sigfe = $folioSigfe->id;
                            $devengo->id_item_presupuestario = $id_item;
                            $devengo->monto = formato_entero($valor);
                            $devengo->save();
                        }

                    }
                    
                } else {
                    // Nuevo folio sigfe
                    if ( formato_entero($valor) > 0 ) {
                        $devengo = new Devengo();
                        // $devengo->id_documento = $this->id; // No es necesario el id_documento
                        $devengo->id_folio_sigfe = $folioSigfe->id;
                        $devengo->id_item_presupuestario = $id_item;
                        $devengo->monto = formato_entero($valor);
                        $devengo->save();
                    }

                }
                
            }

            /**
             * Se buscan los devengo que pertenecen al documento (y al folio sigfe en caso que se este editando),
             * pero que no estan en el arregloIdItems. Son los que se tienen que eliminar
             */
            
            $devengosDelete = Devengo::whereNotIn('id_item_presupuestario', $arregloIdItems);

            if ( $folioSigfe->estadoFolio == 'editado' ) {

                // Si es esta editando, todos los devengo deben tener el id_folio correspondiente
                $devengosDelete = $devengosDelete->where('id_folio_sigfe', $folioSigfe->id);

            } else {

                $devengosDelete = $devengosDelete->where('id_documento', $devengarDocumento->id);

            }
            
            $devengosDelete = $devengosDelete->get();
                                    
            foreach ($devengosDelete as $devengoDelete) {
                if ( $devengoDelete->id_folio_sigfe == null ) {

                    $devengoDelete->delete();

                } else {
                    // no se borra, debe quedar registrado para el id folio
                    $devengoDelete->id_documento = null;

                }
            }

        }
    }

    /**
     * Modifica el total_documento_actualizado del documento relacionado en caso de las NC y ND
     * Devuelve los montos.
     */
    public function actualizarTotalDocumentoActualizadoDelDocumentoRelacionado()
    {
        if ( $this->getDocumentoRelacionado != null ) {

            if ( $this->id_tipo_documento == 4 || $this->id_tipo_documento == 10 ) {
                // NC o CC, resta al total
                // En esta ocasion suma, ya que se devuelve el monto antes de editar todo
                $this->getDocumentoRelacionado->total_documento_actualizado += $this->total_documento;
                $this->getDocumentoRelacionado->save();
                
            }

            if ( $this->id_tipo_documento == 5 || $this->id_tipo_documento == 11 ) {
                // ND o CD, suma al total
                // en esta ocasion resta, ya que se devuelve el monto antes de editar todo
                $this->getDocumentoRelacionado->total_documento_actualizado -= $this->total_documento;
                $this->getDocumentoRelacionado->save();
            }

        }
    }

    public function getTipoDocumento()
    {        
        return $this->belongsTo('App\TipoDocumento','id_tipo_documento')->withTrashed()->withDefault();
    }

    public function getProveedor()
    {
        return $this->belongsTo('App\Proveedor','id_proveedor')->withTrashed()->withDefault();
    }

    public function getProveedorFactoring()
    {
        return $this->belongsTo('App\Proveedor','id_factoring')->withTrashed()->withDefault();
    }

    public function getModalidadCompra()
    {
        
        return $this->belongsTo('App\ModalidadCompra','id_modalidad_compra')->withTrashed()->withDefault();
    }

    public function getTipoAdjudicacion()
    {
        
        return $this->belongsTo('App\TipoAdjudicacion','id_tipo_adjudicacion')->withTrashed()->withDefault();
    }

    public function getTipoInforme()
    {
        
        return $this->belongsTo('App\TipoInforme','id_tipo_informe')->withTrashed()->withDefault();
    }

    public function getResponsable()
    {
        
        return $this->belongsTo('App\User','id_responsable')->withTrashed()->withDefault();
    }

    public function getUserQueElimino()
    {
        
        return $this->belongsTo('App\User','id_user_deleted')->withTrashed()->withDefault();
    }

    public function getReferenteTecnico()
    {
        
        return $this->belongsTo('App\ReferenteTecnico','id_referente_tecnico')->withTrashed()->withDefault();
    }

    public function getDigitador()
    {
        
        return $this->belongsTo('App\User','id_digitador')->withTrashed()->withDefault();
    }

    public function getDigitadorProblema()
    {
        
        return $this->belongsTo('App\User','id_user_problema')->withTrashed();
    }

    public function getDevengador()
    {
        
        return $this->belongsTo('App\User','id_devengador')->withTrashed()->withDefault();
    }

    public function getUserParaDevengar()
    {
        
        return $this->belongsTo('App\User','id_user_para_devengar')->withTrashed()->withDefault();
    }

    public function getArchivos()
    {
        
        return $this->hasMany('App\Archivo','id_documento','id');
    }

    public function getDevengos()
    {

        return $this->hasMany('App\Devengo','id_documento','id');
    }

    public function getNominaPagoDocumento()
    {

        return $this->hasMany('App\NominaPagoDocumento','id_documento','id');
    }

    public function getComprobanteContableDocumento()
    {
        return $this->hasMany('App\ComprobanteContableDocumento','id_documento','id');
    }

    public function getReversaComprobante()
    {
        return $this->hasMany('App\ReversaComprobante','id_documento','id');
    }

    public function getFoliosSigfe()
    {

        return $this->hasMany('App\FolioSigfe','id_documento','id');
    }

    public function getDocumentoRelacionado()
    {    
        return $this->hasOne('App\Documento','id','id_relacionado');
    }

    public function getDocumentosRelacionados()
    {
        return $this->hasMany('App\Documento','id_relacionado','id');
    }

    public function getUnaRelacionRecepcionValidada()
    {    
        return $this->hasOne('App\RelacionDocumentoWsDocumento','id_documento','id')->oldest();
    }

    public function getRelacionRecepcionesValidadas()
    {    
        return $this->hasMany('App\RelacionDocumentoWsDocumento','id_documento','id');
    }

    public function getRelacionGuiasDespacho()
    {

        $conGuiasDespacho = false;
        foreach ($this->getRelacionRecepcionesValidadas as $key => $relacion) {
            if ( $relacion->getWsDocumento->tipo_documento == 'Guia de Despacho Electronica') {
                $conGuiasDespacho = true;
                break;
            }
        }
        
        return $conGuiasDespacho;

    }

    public function getRelacionRecepcionesNoValidadas()
    {    
        return $this->hasMany('App\RelacionDocumentoHermesDocumento','id_documento','id');
    }

    public function getUnaRelacionRecepcionNoValidada()
    {    
        return $this->hasOne('App\RelacionDocumentoHermesDocumento','id_documento','id')->oldest();
    }

    public function getRelacionGuiasDespachoHermes()
    {

        $conGuiasDespacho = false;
        foreach ($this->getRelacionRecepcionesNoValidadas as $key => $relacion) {
            if ( $relacion->getHermesDocumento->tipo_documento == 'Guia de Despacho Electronica') {
                $conGuiasDespacho = true;
                break;
            }
        }
        
        return $conGuiasDespacho;

    }

    public function getUserValidado()
    {
        
        return $this->belongsTo('App\User','id_user_validado')->withTrashed()->withDefault();
    }

    public function getRechazoVistoBueno()
    {    
        return $this->belongsTo('App\RechazoVistoBueno','id_rechazo_visto_bueno');
    }

    public function getVistoBueno()
    {    
        return $this->belongsTo('App\VistoBueno','id_visto_bueno');
    }

    public function getDocumentoMotivoProblema()
    {
        return $this->hasMany('App\DocumentoMotivoProblema','id_documento','id');
    }

    public function getArchivoAcepta()
    {
        return $this->hasOne('App\ArchivoAcepta', 
                ['folio', 'id_proveedor', 'id_tipo_documento','monto_total'], // campos de archivos_acepta
                ['numero_documento', 'id_proveedor', 'id_tipo_documento','total_documento']); // campos de documentos
    }

    public function getArchivoAceptaRefacturado()
    {
        return $this->hasOne('App\ArchivoAcepta', 'id', 'id_archivo_acepta_refacturado');
    }

    public function montoAplicadoAlValorTotal()
    {
        $totalMonto = 0;

        if ( $this->getDocumentosRelacionados->count() > 0 ) {
            foreach ( $this->getDocumentosRelacionados as $docRelacionado ) {
                if ( $docRelacionado->id_tipo_documento == 4 || $docRelacionado->id_tipo_documento == 10 ) {
                    // NC o CC, resta al total
                    $totalMonto -= $docRelacionado->total_documento;
                }
        
                if ( $docRelacionado->id_tipo_documento == 5 || $docRelacionado->id_tipo_documento == 11 ) {
                    // ND o CD, suma al total
                    $totalMonto += $docRelacionado->total_documento;
                }
            }
        }

        return $totalMonto;
    }

    /**
     * Retorna un color segín la diferencia de dia entre
     * fecha de ingreso y fecha actual:
     * verde <= 4
     * amarillo == 5 y 6
     * anaranjado == 7 y 8
     * rojo >= 9
     */
    public function getColorPorDias()
    {
        $dias = diferencia_dias($this->fecha_ingreso ,date('Y-m-d'));
        
        $color = '';
        if ($dias <= 4) {
            $color = '#008000'; // verde
        } elseif ( $dias == 5 || $dias == 6 ) {
            $color = '#e2e200f5'; // amarillo
        } elseif ( $dias == 7 || $dias == 8 ) {
            $color = '#ff8300'; // anaranjado
        } else {
            $color = '#ff0000'; // rojo
        }
        
        return $color;
    }

    public function getColorPorDiasCuadratura()
    {
        $dias = diferencia_dias($this->fecha_ingreso,date('Y-m-d'));
        $diasParaCuadrar = 8 - $dias;
        
        $color = '';
        if ($diasParaCuadrar == 2 || $diasParaCuadrar == 1) {
            $color = '#ff8300'; //naranjo
        } elseif ( $diasParaCuadrar == 3 || $diasParaCuadrar == 4 || $diasParaCuadrar == 5 ) {
            $color = '#e2e200f5'; //amarillo
        } elseif ( $diasParaCuadrar == 6 || $diasParaCuadrar == 7 || $diasParaCuadrar == 8 ) {
            $color = 'green';
        } else {
            $color = 'red';
        }
        
        return $color;
    }

    public function getColorPorDiasDiferenciaIngresoCuadratura()
    {
        // $dias = 0;
        // if ( $this->getUnaRelacionRecepcionValidada ) {

        //     $dias = diferencia_dias( $this->fecha_ingreso, $this->getUnaRelacionRecepcionValidada->created_at );

        // } elseif ( $this->validado != null ) {

        //     $dias = diferencia_dias( $this->fecha_ingreso, $this->fecha_validado );

        // } elseif ( $this->id_visto_bueno && $this->getVistoBueno->fecha_memo_respuesta != null ) {

        //     $dias = diferencia_dias ( $this->fecha_ingreso, $this->getVistoBueno->fecha_memo_respuesta );
            
        // }

        $dias = $this->validado != null ? diferencia_dias( $this->fecha_ingreso, $this->fecha_validado ) : 0;

        $diasParaCuadrar = 8 - $dias;
        
        $color = '';
        if ($diasParaCuadrar == 2 || $diasParaCuadrar == 1) {
            $color = '#ff8300'; //naranjo
        } elseif ( $diasParaCuadrar == 3 || $diasParaCuadrar == 4 || $diasParaCuadrar == 5 ) {
            $color = '#e2e200f5'; //amarillo
        } elseif ( $diasParaCuadrar == 6 || $diasParaCuadrar == 7 || $diasParaCuadrar == 8 ) {
            $color = 'green';
        } else {
            $color = 'red';
        }
        
        return $color;
    }

    public function getColorPorDiasDiferenciaDocumentoIngreso()
    {
        $dias = diferencia_dias($this->fecha_documento,$this->fecha_ingreso);

        $color = '';
        if ($dias <= 4) {
            $color = 'green';
        } elseif ( $dias == 5 || $dias == 6 ) {
            $color = '#e2e200f5';
        } elseif ( $dias == 7 || $dias == 8 ) {
            $color = '#ff8300';
        } else {
            $color = 'red';
        }
        
        return $color;
    }

    public function getColorPorDiasDiferenciaCuadraturaDevengo()
    {
        // $dias = 0;

        // if ( $this->getUnaRelacionRecepcionValidada ) {

        //     $dias = diferencia_dias( $this->getUnaRelacionRecepcionValidada->created_at, $this->fecha_devengado );

        // } elseif ( $this->validado == null ) {

        //     $dias = diferencia_dias( $this->fecha_validado, $this->fecha_devengado );

        // } elseif ( $this->id_visto_bueno != null && $this->getVistoBueno->id_registrador_memo_respuesta != null ) {

        //     $dias = diferencia_dias( $this->getVistoBueno->fecha_memo_respuesta, $this->fecha_devengado );

        // } else {

        //     $dias = diferencia_dias( $this->fecha_recepcion, $this->fecha_devengado );

        // }

        $dias = $this->validado == null ? diferencia_dias( $this->fecha_validado, $this->fecha_devengado ) : 0;
        
        $color = '';
        if ($dias <= 4) {
            $color = 'green';
        } elseif ( $dias == 5 || $dias == 6 ) {
            $color = '#e2e200f5';
        } elseif ( $dias == 7 || $dias == 8 ) {
            $color = '#ff8300';
        } else {
            $color = 'red';
        }
        
        return $color;
    }

    /**
     * Revisa si el documento tiene el archivo del tipo factura
     */
    public function tieneLosArchivosNecesarios()
    {
        $tieneLosArchivos = false;

        if ($this->getArchivos) {
            foreach ($this->getArchivos as $archivo) {
                // Factura
                if ( $archivo->id_tipo_archivo == 1 ) {
                    $tieneLosArchivos = true;
                    break;
                }
            }
        }

        return $tieneLosArchivos;
    }

    /**
     * retorna string con la url de la factura
     * 
     */
    public function getUrlFactura()
    {
        $urlFactura = '';

        if ( $this->getArchivos ) {
            foreach ($this->getArchivos as $archivo) {
                if ( $archivo->id_tipo_archivo == 1 ) {
                    if ($archivo->cargado == 1) {
                        $urlFactura = 'http://'.$archivo->ubicacion;
                    } else {
                        $urlFactura = 'http://10.4.237.28/finanzas/'.$archivo->ubicacion;
                    }
                    
                }
            }
        }
        
        return $urlFactura;
    }
    

    /**
     * Función que retorna un array para la tabla general de documentos
     */
    public function getDatosGeneral($dataTable = null)
    {
        $fechaRecepcion = fecha_dmY($this->fecha_recepcion);
        $fechaDocumento = fecha_dmY($this->fecha_documento);
        $itemsDevengo = '';
        foreach ( $this->getDevengos as $devengo ) {
            $itemsDevengo .= $devengo->getItemPresupuestario->codigo().'</br>';
        }
        $totalDocumento = '$ '.formatoMiles($this->total_documento);
        $totalDocumentoActualizado = '$ '.formatoMiles($this->total_documento_actualizado);

        $botones = '';
        $botones .= '<div class="btn-group">';
        // Ver
        if ( \Entrust::can('ver-documento') ) {
            $botones .= '<button type="button" class="btn btn-success btn-xs" title="Ver Documento" onclick="ver('. $this->id .');">';
            $botones .= '<i class="fa fa-eye"></i></button>';
        }
        
        // Editar
        if ( \Entrust::can('editar-documento') && $dataTable == null ) {
            $botones .= '<button type="button" class="btn btn-warning btn-xs" title="Editar Documento" onclick="editar('. $this->id .');">';
            $botones .= '<i class="fas fa-edit"></i></button>';
        }

        // // Eliminar Documento
        // if ( \Entrust::can('eliminar-documento') ) {
        //     $botones .= '<button type="button" class="btn btn-danger btn-xs" title="Eliminar Documento" onclick="eliminar('. $this->id .');">';
        //     $botones .= '<i class="fa fa-trash"></i></button>';
        // }

        // Trazabilidad Documento
        if ( \Entrust::can('ver-documento') ) {
            $botones .= '<button type="button" class="btn btn-primary btn-xs" title="Trazabilidad Documento" onclick="traza('. $this->id .');">';
            $botones .= '<i class="fas fa-map-signs fa-lg"></i></button>';
        }

        $rutaPDF = asset('documentos/generar/pdf_trazabilidad_documento/').'/'.$this->id;
        $botones .= '<a class="btn btn-danger btn-xs" title="PDF Trazabilidad Dcto. Finanzas" target="_blank" href="'.$rutaPDF.'">';
        $botones .= '<i class="fas fa-file-pdf fa-lg"></i></a>';

        $botones .= '</div>';

        $docJson = array(
            'DT_RowID' => $this->id,
            $this->getEstadoDocumento(),
            $this->getProveedor->rut,
            $this->getProveedor->nombre,
            $this->getTipoDocumento->nombre,
            $this->numero_documento,
            $this->documento_compra,
            $fechaRecepcion,
            $fechaDocumento,
            $itemsDevengo,
            $totalDocumento,
            $totalDocumentoActualizado,
            $botones
        );

        return $docJson;
    }

    /**
     * Función que retorna un array para la tabla pendientes de validación
     */
    public function getDatosPendientesValidacion()
    {
        $tieneMatch = false;
        if ( $this->tieneLosArchivosNecesarios() ) {

            // Buscar si tiene una Recepción con la cual hacer match
            $match = WsDocumento::where('id_relacion', null)
                                ->where('tipo_documento', 'LIKE', $this->getTipoDocumento->nombre)
                                ->where('documento', $this->numero_documento)
                                ->where('documento_total', $this->total_documento_actualizado)
                                ->whereHas('getWsOrdenCompra.getWsContrato', function ($query) {
                                    $query->where('rut_proveedor', $this->getProveedor->rut);
                                })
                                ->first();
                                
            if ( is_object($match) ) {
                $tieneMatch = true;
            }

        }
        

        $fechaIngreso = fecha_dmY($this->fecha_ingreso);
        $fechaRecepcion = fecha_dmY($this->fecha_recepcion);
        $fechaDocumento = fecha_dmY($this->fecha_documento);
        $itemsDevengo = '';
        foreach ( $this->getDevengos as $devengo ) {
            $itemsDevengo .= $devengo->getItemPresupuestario->codigo().'</br>';
        }
        $totalDocumento = '$ '.formatoMiles($this->total_documento);
        $totalDocumentoActualizado = '$ '.formatoMiles($this->total_documento_actualizado);

        $botones = '';
        $botones .= '<div class="btn-group">';

        // Editar
        if ( \Entrust::can('editar-documento') ) {
            $botones .= '<button type="button" class="btn btn-warning btn-xs" title="Editar Documento" onclick="editar('. $this->id .');">';
            $botones .= '<i class="fas fa-edit"></i></button>';
        }        

        // Validar Documento
        if ( \Entrust::can('cuadrar-documento') ) {
            $botones .= '<button class="btn btn-success btn-xs" title="Validar Documento" onclick="validar('. $this->id .');">';
            $botones .= '<i class="fas fa-thumbs-up fa-lg"></i></button>';
        }

        // Eliminar Documento
        if ( $this->id_devengador == null && \Entrust::can('eliminar-documento') ) {
            $botones .= '<button type="button" class="btn btn-danger btn-xs" title="Eliminar Documento" onclick="eliminar('. $this->id .');">';
            $botones .= '<i class="fa fa-trash"></i></button>';
        }

        // Boton para conectar los documentos con match directo.
        if ( $tieneMatch ) {

            if ( \Entrust::can('match-documentos-con-recepcion') ) {
                $botones .= '<button class="btn btn-info btn-xs" title="Conectar Documentos" onclick="conectarDocumentos('. $this->id .','. $match->id .');">';
                $botones .= '<i class="fas fa-code-branch fa-lg fa-rotate-270" style="color:black;"></i></button>';
            }
            $tieneMatch = 'Si';

        } else {
            $tieneMatch = 'No';
        }

        // Boton para pasar a documento con problema
        if ( \Entrust::can('problemas-documento') ) {
            $botones .= '<button type="button" class="btn btn-default btn-xs" title="Documento con problema" onclick="problema('. $this->id .');">';
            $botones .= '<i class="fas fa-exclamation-triangle fa-lg" style="color: #d9534f;border-color: #d43f3a;"></i></button>';
        }

        $botones .= '</div>';

        $diferenciaDias = diferencia_dias_cuadratura($this->fecha_ingreso,date('Y-m-d'));
        $diferenciaDias .= '&nbsp;&nbsp;<i class="fas fa-clipboard-check fa-lg fa-bounce" style="color:'. $this->getColorPorDiasCuadratura() .' !important"></i>';

        $docJson = array(
            'DT_RowID' => $this->id,
            $diferenciaDias,
            $fechaIngreso,
            $this->getProveedor->rut,
            $this->getProveedor->nombre,
            $tieneMatch,
            $this->getTipoDocumento->nombre,
            $this->numero_documento,
            $this->documento_compra,
            $fechaRecepcion,
            $fechaDocumento,
            $itemsDevengo,
            $totalDocumento,
            $totalDocumentoActualizado,
            $botones
        );

        return $docJson;
    }

    /**
     * Función que retorna un array para la tabla documentos con problema
     */
    public function getDatosConProblema()
    {
        $tieneMatch = false;
        if ( $this->tieneLosArchivosNecesarios() ) {

            // Buscar si tiene una Recepción con la cual hacer match
            $match = WsDocumento::where('id_relacion', null)
                                ->where('tipo_documento', 'LIKE', $this->getTipoDocumento->nombre)
                                ->where('documento', $this->numero_documento)
                                ->where('documento_total', $this->total_documento_actualizado)
                                ->whereHas('getWsOrdenCompra.getWsContrato', function ($query) {
                                    $query->where('rut_proveedor', $this->getProveedor->rut);
                                })
                                ->first();
                                
            if ( is_object($match) ) {
                $tieneMatch = true;
            }

        }
        

        $fechaIngreso = fecha_dmY($this->fecha_ingreso);
        $fechaRecepcion = fecha_dmY($this->fecha_recepcion);
        $fechaDocumento = fecha_dmY($this->fecha_documento);
        // Se quita columna de item, para agregar columna de proceso (proceso de solucion de problema)
        // $itemsDevengo = '';
        // foreach ( $this->getDevengos as $devengo ) {
        //     $itemsDevengo .= $devengo->getItemPresupuestario->codigo().'</br>';
        // }
        $procesoSolucion = 'No';
        if ( $this->proceso_solucion_problema ) {
            $procesoSolucion = 'Si';
        }
        $totalDocumento = '$ '.formatoMiles($this->total_documento);
        $totalDocumentoActualizado = '$ '.formatoMiles($this->total_documento_actualizado);

        $botones = '';
        $botones .= '<div class="btn-group">';

        // Editar
        if ( \Entrust::can('editar-documento') ) {
            $botones .= '<button type="button" class="btn btn-warning btn-xs" title="Editar Documento" onclick="editar('. $this->id .');">';
            $botones .= '<i class="fas fa-edit"></i></button>';
        }        

        // Validar Documento
        if ( \Entrust::can('cuadrar-documento') ) {
            $botones .= '<button class="btn btn-success btn-xs" title="Validar Documento" onclick="validar('. $this->id .');">';
            $botones .= '<i class="fas fa-thumbs-up fa-lg"></i></button>';
        }

        // Eliminar Documento
        // if ( $this->id_devengador == null && \Entrust::can('eliminar-documento') ) {
        //     $botones .= '<button type="button" class="btn btn-danger btn-xs" title="Eliminar Documento" onclick="eliminar('. $this->id .');">';
        //     $botones .= '<i class="fa fa-trash"></i></button>';
        // }

        // Boton para conectar los documentos con match directo.
        if ( $tieneMatch ) {

            // if ( \Entrust::can('match-documentos-con-recepcion') ) {
            //     $botones .= '<button class="btn btn-info btn-xs" title="Conectar Documentos" onclick="conectarDocumentos('. $this->id .','. $match->id .');">';
            //     $botones .= '<i class="fas fa-code-branch fa-lg fa-rotate-270" style="color:black;"></i></button>';
            // }
            $tieneMatch = 'Si';

        } else {
            $tieneMatch = 'No';
        }

        // Boton para pasar a documento con problema
        if ( \Entrust::can('problemas-documento') ) {
            $botones .= '<button type="button" class="btn btn-info btn-xs" title="Agregar observación de problema" onclick="agregarObservacion('. $this->id .');">';
            $botones .= '<i class="fas fa-comments fa-lg" ></i></button>';
        }

        // Boton para pasar a documento con problema
        if ( \Entrust::can('quitar-problemas-documento') ) {
            $botones .= '<button type="button" class="btn btn-default btn-xs" title="Quitar Problema" onclick="quitarProblema('. $this->id .');">';
            $botones .= '<i class="fas fa-exclamation-triangle fa-lg" style="color: #5cb85c;border-color: #4cae4c;"></i></button>';
        }

        $botones .= '</div>';

        $diferenciaDias = diferencia_dias_cuadratura($this->fecha_ingreso,date('Y-m-d'));
        $diferenciaDias .= '&nbsp;&nbsp;<i class="fas fa-clipboard-check fa-lg fa-bounce" style="color:'. $this->getColorPorDiasCuadratura() .' !important"></i>';

        $docJson = array(
            'DT_RowID' => $this->id,
            $diferenciaDias,
            $fechaIngreso,
            $this->getProveedor->rut,
            $this->getProveedor->nombre,
            $tieneMatch,
            $this->getTipoDocumento->nombre,
            $this->numero_documento,
            $this->documento_compra,
            $fechaRecepcion,
            $fechaDocumento,
            // $itemsDevengo,
            $procesoSolucion,
            $totalDocumento,
            $totalDocumentoActualizado,
            $botones
        );

        return $docJson;
    }

    /**
     * Función que retorna un array para la tabla por devengar
     */
    public function getDatosPorDevengar($filtroItemPresupuestario = null, &$totalPorDevengar = null)
    {
        if ( $this->id_tipo_documento == 4 && $this->total_documento == 0 ) {
            return '';
        }
        $diferenciaDias = '<i class="fas fa-exclamation-circle fa-lg" style="color:'. $this->getColorPorDias() .' !important"></i>';
        $diferenciaDias .= ' '.(int)diferencia_dias($this->fecha_recepcion,date('Y-m-d'));
        $fechaRD = fecha_dmY($this->fecha_recepcion);

        $itemsDevengo = '';
        $totalDocumento = '';
        foreach ( $this->getDevengos as $devengo ) {

            if ( $filtroItemPresupuestario != null ) {

                //comprobar si el item se encuentra en el filtro
                if ( in_array($devengo->id_item_presupuestario, $filtroItemPresupuestario) ) {
                    
                    $itemsDevengo .= $devengo->getItemPresupuestario->codigo().'</br>';
                    $totalDocumento .= '$ '.formatoMiles($devengo->monto).'</br>';
                    
                    if ($this->id_tipo_documento == 4 || $this->id_tipo_documento == 10) {
                        $totalPorDevengar = $totalPorDevengar - $devengo->monto;
                    } else {
                        $totalPorDevengar = $totalPorDevengar + $devengo->monto;
                    }

                }
                
            } else {
                $itemsDevengo .= $devengo->getItemPresupuestario->codigo().'</br>';
                $totalDocumento .= '$ '.formatoMiles($devengo->monto).'</br>';
                if ( is_numeric($totalPorDevengar) ) {
                    
                    if ($this->id_tipo_documento == 4 || $this->id_tipo_documento == 10) {
                        $totalPorDevengar = $totalPorDevengar - $devengo->monto;
                    } else {
                        $totalPorDevengar = $totalPorDevengar + $devengo->monto;
                    }
                }
            }
            
        }

        $totalDocumentoActualizado = '$ '.formatoMiles($this->total_documento_actualizado);

        $botones = '';
        $botones .= '<div class="btn-group">';

        //Devengar
        if ( \Entrust::can('devengar-documento') ) {
            $botones .= '<button class="btn btn-info btn-xs" title="Devengar Documento" onclick="devengarDocumento('. $this->id .');">';
            $botones .= '<i class="fas fa-gavel"></i></button>';
        }

        $botones .= '</div>';

        $usuarioParaDevengar = '';
        if ( \Auth::user()->id == 1 || \Auth::user()->id == 18 || \Auth::user()->id == 33 || \Auth::user()->id == 13 || \Auth::user()->id == 16 ) {
            $usuarioParaDevengar .= $this->getUserParaDevengar->name;
            $docJson = array(
                'DT_RowID' => $this->id,
                $diferenciaDias,
                $fechaRD,
                $usuarioParaDevengar,
                $this->getTipoInforme->nombre,
                $this->getProveedor->rut,
                $this->getProveedor->nombre,
                $this->getTipoDocumento->nombre,
                $this->numero_documento,
                $this->getModalidadCompra->nombre,
                $this->documento_compra,
                $itemsDevengo,
                $totalDocumento,
                $totalDocumentoActualizado,
                $botones
            );
        } else {
            $docJson = array(
                'DT_RowID' => $this->id,
                $diferenciaDias,
                $fechaRD,
                $this->getTipoInforme->nombre,
                $this->getProveedor->rut,
                $this->getProveedor->nombre,
                $this->getTipoDocumento->nombre,
                $this->numero_documento,
                $this->getModalidadCompra->nombre,
                $this->documento_compra,
                $itemsDevengo,
                $totalDocumento,
                $totalDocumentoActualizado,
                $botones
            );
        }

        return $docJson;
    }

    /**
     * Función que retorna un array para la tabla por devengar 2.0
     */
    public function getDatosPorDevengar2($filtroItemPresupuestario = null, &$totalPorDevengar = null)
    {
        if ( $this->id_tipo_documento == 4 && $this->total_documento == 0 ) {
            return '';
        }
        
        $diferenciaDias = '';
        $fechaConformidadDocumento = ''; // muestra la fecha que se utiliza para el conteo de los 8 dias para devengar

        $diferenciaDias = '<i class="fas fa-exclamation-circle fa-lg" style="color: '. $this->getColorPorDias() .' !important"></i>';
        $diferenciaDias .= ' '.diferencia_dias( $this->fecha_ingreso, date('Y-m-d') );
        $fechaConformidadDocumento .= fecha_dmY( $this->fecha_ingreso );
        // if ( $this->getUnaRelacionRecepcionValidada ) {

        //     $diferenciaDias = '<i class="fas fa-exclamation-circle fa-lg" style="color: '. $this->getUnaRelacionRecepcionValidada->getColorPorDias() .' !important"></i>';
        //     $diferenciaDias .= ' '.diferencia_dias($this->getUnaRelacionRecepcionValidada->created_at,date('Y-m-d'));
        //     $fechaConformidadDocumento .= fecha_dmY($this->getUnaRelacionRecepcionValidada->created_at);

        // } elseif ( $this->validado != null ) {

        //     $diferenciaDias = '<i class="fas fa-exclamation-circle fa-lg" style="color: '. $this->getColorPorDias() .' !important"></i>';
        //     $diferenciaDias .= ' '.diferencia_dias($this->fecha_validado,date('Y-m-d'));
        //     $fechaConformidadDocumento .= fecha_dmY($this->fecha_validado);

        // } elseif ( $this->id_visto_bueno != null && $this->getVistoBueno->id_registrador_memo_respuesta != null ) {

        //     $diferenciaDias = '<i class="fas fa-exclamation-circle fa-lg" style="color: '. $this->getColorPorDias() .' !important"></i>';
        //     $diferenciaDias .= ' '.diferencia_dias( $this->getVistoBueno->fecha_memo_respuesta, date('Y-m-d') );
        //     $fechaConformidadDocumento .= fecha_dmY( $this->getVistoBueno->fecha_memo_respuesta );

        // } elseif ( $this->directo_por_devengar == 1 ) {
        //     $diferenciaDias = '<i class="fas fa-exclamation-circle fa-lg" style="color: '. $this->getColorPorDias() .' !important"></i>';
        //     $diferenciaDias .= ' '.diferencia_dias( $this->fecha_ingreso, date('Y-m-d') );
        //     $fechaConformidadDocumento .= fecha_dmY( $this->fecha_ingreso );
        // }


        $itemsDevengo = '';
        $totalDocumento = '';
        foreach ( $this->getDevengos as $devengo ) {

            if ( $filtroItemPresupuestario != null ) {

                //comprobar si el item se encuentra en el filtro
                if ( in_array($devengo->id_item_presupuestario, $filtroItemPresupuestario) ) {
                    
                    $itemsDevengo .= $devengo->getItemPresupuestario->codigo().'</br>';
                    $totalDocumento .= '$ '.formatoMiles($devengo->monto).'</br>';
                    
                    if ($this->id_tipo_documento == 4 || $this->id_tipo_documento == 10) {
                        $totalPorDevengar = $totalPorDevengar - $devengo->monto;
                    } else {
                        $totalPorDevengar = $totalPorDevengar + $devengo->monto;
                    }

                }

            } else {

                $itemsDevengo .= $devengo->getItemPresupuestario->codigo().'</br>';
                $totalDocumento .= '$ '.formatoMiles($devengo->monto).'</br>';
                if ( is_numeric($totalPorDevengar) ) {
                    
                    if ($this->id_tipo_documento == 4 || $this->id_tipo_documento == 10) {
                        $totalPorDevengar = $totalPorDevengar - $devengo->monto;
                    } else {
                        $totalPorDevengar = $totalPorDevengar + $devengo->monto;
                    }
                }
                
            }
            
        }

        $totalDocumentoActualizado = '$ '.formatoMiles($this->total_documento_actualizado);

        $botones = '';
        $botones .= '<div class="btn-group">';

        //Devengar
        if ( \Entrust::can('devengar-documento') ) {
            $botones .= '<button class="btn btn-info btn-xs" title="Devengar Documento" onclick="devengarDocumento('. $this->id .');">';
            $botones .= '<i class="fas fa-gavel"></i></button>';
        }

        $botones .= '</div>';

        $usuarioParaDevengar = $this->getResponsable ? $this->getResponsable->name : '';
        $docJson = array(
            'DT_RowID' => $this->id,
            $diferenciaDias,
            $fechaConformidadDocumento,
            $usuarioParaDevengar,
            $this->getTipoInforme->nombre,
            $this->getProveedor->rut,
            $this->getProveedor->nombre,
            $this->getTipoDocumento->nombre,
            $this->numero_documento,
            $this->getModalidadCompra->nombre,
            $this->documento_compra,
            $itemsDevengo,
            $totalDocumento,
            $totalDocumentoActualizado,
            $botones
        );

        return $docJson;
    }

    /**
     * Función que retorna un array para la tabla devengados
     */
    public function getDatosDevengados($filtroItemPresupuestario = null, &$totalDevengado = NULL)
    {
        // var_dump($totalDevengado);
        // dd($totalDevengado,is_numeric($totalDevengado));
        $fechaDocumento = fecha_dmY($this->fecha_documento);
        $fechaSigfe = fecha_dmY($this->fechaSigfe);
        $itemsDevengo = '';
        $totalDocumento = '';
        foreach ( $this->devengosFolioSigfe as $devengo ) {

            if ( $filtroItemPresupuestario != null ) {

                //comprobar si el item se encuentra en el filtro
                if ( in_array($devengo->id_item_presupuestario, $filtroItemPresupuestario) ) {
                    
                    $itemsDevengo .= $devengo->getItemPresupuestario->codigo().'</br>';
                    $totalDocumento .= '$ '.formatoMiles($devengo->monto).'</br>';
                    
                    if ($this->id_tipo_documento == 4 || $this->id_tipo_documento == 10) {
                        $totalDevengado = $totalDevengado - $devengo->monto;
                    } else {
                        $totalDevengado = $totalDevengado + $devengo->monto;
                    }

                }
                
            } else {
                
                $itemsDevengo .= $devengo->getItemPresupuestario->codigo().'</br>';
                $totalDocumento .= '$ '.formatoMiles($devengo->monto).'</br>';
                if ( is_numeric($totalDevengado) ) {
                    
                    if ($this->id_tipo_documento == 4 || $this->id_tipo_documento == 10) {
                        $totalDevengado = $totalDevengado - $devengo->monto;
                    } else {
                        $totalDevengado = $totalDevengado + $devengo->monto;
                    }
                }

            }
            
        }
        
        $totalDocumentoActualizado = '$ '.formatoMiles($this->total_documento_actualizado);
        
        $botones = '';
        $botones .= '<div class="btn-group">';
        // Ver
        if ( \Entrust::can('ver-documento') ) {
            $botones .= '<button class="btn btn-success btn-xs" title="Ver Documento" onclick="verDocumento('. $this->id .');">';
            $botones .= '<i class="fa fa-eye"></i></button>';
        }
        // dd($this->getFoliosSigfe);
        // Editar
        if ( \Entrust::can('editar-devengo') ) {
            $botones .= '<button class="btn btn-info btn-xs" title="Editar Devengo" onclick="devengarDocumento('. $this->id .','.$this->idFolioSigfe.');">';
            $botones .= '<i class="fas fa-edit"></i></button>';
        }

        // Eliminar
        if ( \Entrust::can('eliminar-devengo') ) {
            $botones .= '<button class="btn btn-danger btn-xs" title="Eliminar Devengo" onclick="eliminarDevengo('.$this->idFolioSigfe.');">';
            $botones .= '<i class="fas fa-trash"></i></button>';
        }

        $botones .= '</div>';

        $docJson = array(
            'DT_RowID' => $this->id,
            // $this->getTipoInforme->nombre,
            $this->getTipoDocumento->nombre,
            $this->numero_documento,
            $this->getProveedor->rut,
            $this->getProveedor->nombre,
            $this->getModalidadCompra->nombre,
            $this->documento_compra,
            $fechaDocumento,
            $fechaSigfe,
            $this->idSigfe,
            $itemsDevengo,
            $this->detalleSigfe,
            $totalDocumento,
            $totalDocumentoActualizado,
            $botones
        );

        return $docJson;
    }

    /**
     * Función que retorna un array para opciones de reemplazo de documento.
     */
    public function getDatosDocumentosProveedores()
    {
        // obtener items
        $datosItems = array();
        foreach ( $this->getDevengos as $devengo ) {
            $datoItem = array(
                'id_item'                     => $devengo->getItemPresupuestario->id,
                'codigo'                      => $devengo->getItemPresupuestario->codigo(),
                'clasificacionPresupuestario' => $devengo->getItemPresupuestario->clasificador_presupuestario,
            );
            $datosItems[] = $datoItem;
        }
        
        $datosJson = array(
            'ordenCompra'               => $this->documento_compra,
            'idModalidadCompra'         => $this->id_modalidad_compra,
            'idResponsable'             => $this->id_responsable,
            'idTipoInforme'             => $this->id_tipo_informe,
            'idTipoAdjudicacion'        => $this->id_tipo_adjudicacion,
            'idReferenteTecnico'        => $this->id_referente_tecnico,
            'numeroLicitacon'           => $this->licitacion,
            'items'                     => $datosItems,
            'numeroDocumento'           => $this->numero_documento,
            'totalDocumento'            => formatoMiles($this->total_documento),
            'totalActualizadoDocumento' => $this->total_documento_actualizado
        );

        return $datosJson;
    }

    public function getEstadoDocumento($excel = null)
    {
        $estado = '';
        // dd($excel);
        if ( $this->fecha_ingreso >= '2019-06-22 00:00:00' && $this->validado == null ) {
            $estado .= 'Pendiente Cuadratura';
        } elseif ( $this->validado != null ) {
            $estado .= 'Cuadrado';
        }

        // Validar el monto actualizado con el monto de las relaciones
        // if ( $this->getRelacionRecepcionesValidadas->count() > 0 || $this->getRelacionRecepcionesNoValidadas->count() > 0 ) {
        //     if ( $this->diferenciaConRecepciones() > 100 || $this->diferenciaConRecepciones() < -100 ) {
        //         $estado = 'Diferencia($) con recepción';
        //     }
        // }
        if ( $this->getRelacionRecepcionesValidadas->count() > 0 ) {
            if ( $this->diferenciaConRecepciones() > 100 || $this->diferenciaConRecepciones() < -100 ) {
                $estado = 'Diferencia($) con recepción';
            }
        }

        /**
         * id estado :
         * 1 = ingresado
         * 2 = devengado
         * 3 = pagado
         */

        if ( $this->id_estado < 2 ) {

            $estado .= ($excel) ? ' / ': '<br>';
            $estado .= 'Pendiente Devengo';

        } else {

            $estado .= ($excel) ? ' / ': '<br>';
            $estado .= 'Devengado';
            
        }

        if ( $this->id_estado < 3 ) {

            $estado .= ($excel) ? ' / ': '<br>';
            $estado .= 'Pendiente Pago';
            
        } else {

            $estado .= ($excel) ? ' / ': '<br>';
            $estado .= 'Pagado';

        }

        if ( $this->id_user_problema != null ) {

            $estado .= ($excel) ? ' / ': '<br>';
            $estado .= 'Con problemas';

        }

        if ( $this->id_rechazo_visto_bueno != null ) {

            $estado .= ($excel) ? ' / ': '<br>';
            $estado .= 'Rechazo de visto bueno';

        }

        if ( $this->reclamado == 1 ) {

            $estado .= ($excel) ? ' / ': '<br>';
            $estado .= 'Reclamado';

        }

        if ( $this->deleted_at != null ) {

            $estado .= ($excel) ? ' / ': '<br>';
            $estado .= 'Eliminado';
            
        }

        // if ( $this->getContratoOrdenCompra ) {
        //     $estado .= ($excel) ? ' / ': '<br>';
        //     $estado .= 'ContratoOC.';
        // }
        

        return $estado;

    }


    public function getDatosSolicitudVistoBueno()
    {

        $checkBox = "<input class='select_item' name='id[]' id='id[]' value='{$this->id}' type='checkbox' ";
        $checkBox .= " data-referente='{$this->id_referente_tecnico}' />";
        $fechaRecepcion = fecha_dmY($this->fecha_recepcion);
        $totalDocumento = '$ '.formatoMiles($this->total_documento);
        $totalDocumentoActualizado = '$ '.formatoMiles($this->total_documento_actualizado);

        $botones = '<div class="btn-group">';

        $botones .= '<button class="btn btn-success btn-xs " title="Ver Documento" onclick="verDocumento('. $this->id. ');">';
        $botones .= '<i class="fa fa-eye fa-lg"></i></button>';

        if ( \Entrust::can('solicitar-visto-bueno') ) {
            $botones .= '<button class="btn btn-info btn-xs " title="Generar Memo" onclick="generarMemo('. $this->id. ');">';
            $botones .= '<i class="fas fa-file-signature fa-lg"></i></button>';
        }

        $botones .= '</div>';

        $docJson = array(
            'DT_RowID' => $this->id,
            $checkBox,
            $this->getProveedor->rut.' '.$this->getProveedor->nombre,
            $this->getResponsable->name,
            $this->getDigitador->name,
            $this->getTipoDocumento->nombre,
            $this->numero_documento,
            $this->documento_compra,
            $fechaRecepcion,
            $this->getTipoInforme->nombre,
            $totalDocumento,
            $totalDocumentoActualizado,
            $this->getReferenteTecnico->nombre.' '.$this->getReferenteTecnico->responsable,
            $botones
        );

        return $docJson;
        
    }

    /**
     * Función que retorna un array para la tabla general de documentos
     */
    public function getDatosDocumentoEliminado()
    {
        $fechaRecepcion = fecha_dmY($this->fecha_recepcion);
        $fechaDocumento = fecha_dmY($this->fecha_documento);
        $itemsDevengo = '';
        foreach ( $this->getDevengos as $devengo ) {
            $itemsDevengo .= $devengo->getItemPresupuestario->codigo().'</br>';
        }
        $totalDocumento = '$ '.formatoMiles($this->total_documento);
        $totalDocumentoActualizado = '$ '.formatoMiles($this->total_documento_actualizado);

        $botones = '';
        $botones .= '<div class="btn-group">';
        
        // Ver
        // if ( \Entrust::can('ver-documento') ) {
        //     $botones .= '<button type="button" class="btn btn-success btn-xs" title="Ver Documento" onclick="ver('. $this->id .');">';
        //     $botones .= '<i class="fa fa-eye"></i></button>';
        // }
        
        // Editar
        // if ( \Entrust::can('editar-documento') ) {
        //     $botones .= '<button type="button" class="btn btn-warning btn-xs" title="Editar Documento" onclick="editar('. $this->id .');">';
        //     $botones .= '<i class="fas fa-edit"></i></button>';
        // }

        // // Eliminar Documento
        // if ( \Entrust::can('eliminar-documento') ) {
        //     $botones .= '<button type="button" class="btn btn-danger btn-xs" title="Eliminar Documento" onclick="eliminar('. $this->id .');">';
        //     $botones .= '<i class="fa fa-trash"></i></button>';
        // }

        // Trazabilidad Documento
        if ( \Entrust::can('ver-documento') ) {
            $botones .= '<button type="button" class="btn btn-primary btn-xs" title="Trazabilidad Documento" onclick="traza('. $this->id .');">';
            $botones .= '<i class="fas fa-map-signs fa-lg"></i></button>';
        }

        if ( \Entrust::hasRole(['propietario','administrador']) ) {
            $botones .= '<button type="button" class="btn btn-default btn-xs" style="background-color: black;border-color: #0e1918;" ';
            $botones .= ' title="Recuperar Documento" onclick="recuperar('. $this->id .');">';
            $botones .= '<i class="fas fa-trash-restore fa-lg" style="color:white;"></i></button>';
        }

        // $rutaPDF = asset('documentos/generar/pdf_trazabilidad_documento/').'/'.$this->id;
        // $botones .= '<a class="btn btn-danger btn-xs" title="PDF Trazabilidad Dcto. Finanzas" target="_blank" href="'.$rutaPDF.'">';
        // $botones .= '<i class="fas fa-file-pdf fa-lg"></i></a>';

        $botones .= '</div>';

        $docJson = array(
            'DT_RowID' => $this->id,
            $this->getEstadoDocumento(),
            $this->getProveedor->rut,
            $this->getProveedor->nombre,
            $this->getTipoDocumento->nombre,
            $this->numero_documento,
            $this->documento_compra,
            $fechaRecepcion,
            $fechaDocumento,
            $itemsDevengo,
            $totalDocumento,
            $totalDocumentoActualizado,
            $botones
        );

        return $docJson;
    }


    public function getDatosDocumentoParaAgregarAlComprobanteContable()
    {

        $checkBox = "<input class='select_item' name='id[]' id='id[]' value='{$this->id}' type='checkbox' ";
        $checkBox .= " data-monto='{$this->total_documento_actualizado}' />";
        $fechaDocumento = fecha_dmY($this->fecha_documento);
        $totalDocumento = '$ '.formatoMiles($this->total_documento);
        $totalDocumentoActualizado = '$ '.formatoMiles($this->total_documento_actualizado);

        $botones = '<div class="btn-group">';

         // Trazabilidad Documento
         if ( \Entrust::can('ver-documento') ) {
            $botones .= '<button type="button" class="btn btn-primary btn-xs" title="Trazabilidad Documento" onclick="traza('. $this->id .');">';
            $botones .= '<i class="fas fa-map-signs fa-lg"></i></button>';
        }
        $botones .= '</div>';

        $factoring = '';
        if ( $this->getProveedorFactoring->id != null ) {
            $factoring = $this->getProveedorFactoring->rut.' '.$this->getProveedorFactoring->nombre;
        }

        $docJson = array(
            'DT_RowID' => $this->id,
            $checkBox,
            $this->getProveedor->rut.' '.$this->getProveedor->nombre,
            $factoring,
            $this->getTipoDocumento->nombre,
            $this->numero_documento,
            $this->documento_compra,
            $fechaDocumento,
            $totalDocumento,
            $totalDocumentoActualizado,
            $botones
        );

        return $docJson;
    }
    /**
     * Función que retorna un array para la tabla del buscador
     */
    public function getDatosBuscador($verDocumento)
    {
        $fechaRecepcion = $this->fecha_recepcion;
        $fechaDocumento = $this->fecha_documento;
        $itemsDevengo = '';
        foreach ( $this->getDevengos as $devengo ) {
            $itemsDevengo .= $devengo->getItemPresupuestario->codigo().'</br>';
        }
        $totalDocumento = $this->total_documento;
        $totalDocumentoActualizado = $this->total_documento_actualizado;

        $botones = '<div class="btn-group">';

        // Ver
        if ( $verDocumento && $this->deleted_at == null ) {
            $botones .= '<button type="button" class="btn btn-success btn-xs" title="Ver Documento" onclick="ver('. $this->id .');">';
            $botones .= '<i class="fa fa-eye"></i></button>';
        }
        
        // Trazabilidad Documento
        if ( $verDocumento) {

            $botones .= '<button type="button" class="btn btn-primary btn-xs" title="Trazabilidad Documento" onclick="traza('. $this->id .'';
            if ( $this->deleted_at != null ) {
                $botones .= ',true);"> ';
            } else {
                $botones .= ');"> ';
            }
            
            $botones .= '<i class="fas fa-map-signs fa-lg"></i></button>';

        }

        if ( $this->deleted_at == null ) {

            // $rutaPDF = asset('documentos/generar/pdf_trazabilidad_documento/').'/'.$this->id;
            // $botones .= '<a class="btn btn-danger btn-xs" title="PDF Trazabilidad Dcto. Finanzas" target="_blank" href="'.$rutaPDF.'">';
            // $botones .= '<i class="fas fa-file-pdf fa-lg"></i></a>';

            // modal que muestra todos los archivos para imprimir del documento

            // $botones .= '<button type="button" class="btn btn-danger btn-xs" title="Imprimir Archivos" onclick="imprimirArchivos('. $this->id .');" >';
            // $botones .= '<i class="fas fa-print fa-lg"></i></button>';

            $botones .= '<a class="btn btn-danger btn-xs" href="'.asset('documentos/imprimir/todos_los_archivos/').'/'.$this->id.'" ';
            $botones .= 'title="Imprimir Todos" target="_blank" ><i class="fa fa-print fa-lg"></i></a>';
        }
       

        $botones .= '</div>';

        $docJson = array(
            'DT_RowID' => $this->id,
            $botones,
            $this->getEstadoDocumento(),
            $this->getProveedor->rut.' '.$this->getProveedor->nombre,
            $this->getTipoDocumento->nombre,
            $this->numero_documento,
            $this->documento_compra,
            $fechaRecepcion,
            $fechaDocumento,
            $itemsDevengo,
            $totalDocumento,
            $totalDocumentoActualizado
        );

        return $docJson;
    }

    public function getDatosDocumentosDelComprobanteContable()
    {

        $fechaDocumento = fecha_dmY($this->fecha_documento);
        $totalDocumento = '$ '.formatoMiles($this->total_documento);
        $totalDocumentoActualizado = '$ '.formatoMiles($this->total_documento_actualizado);

        $botones = '<div class="btn-group">';

        // Trazabilidad Documento
        if ( \Entrust::can('ver-documento') ) {
            $botones .= '<button type="button" class="btn btn-primary btn-xs" title="Trazabilidad documento" onclick="traza('. $this->id .');">';
            $botones .= '<i class="fas fa-map-signs fa-lg"></i></button>';
        }

        $botones .= '<button type="button" class="btn btn-danger btn-xs" title="Quitar documento del Comprobante" onclick="eliminarDoc('. $this->id .');">';
        $botones .= '<i class="fas fa-trash"></i></button>';

        $botones .= '</div>';

        $factoring = '';
        if ( $this->getProveedorFactoring->id != null ) {
            $factoring = $this->getProveedorFactoring->rut.' '.$this->getProveedorFactoring->nombre;
        }

        $docJson = array(
            'DT_RowID' => $this->id.'_'.$this->id_comprobante_contable_documento,
            $this->proveedorComprobante,
            $factoring,
            $this->getTipoDocumento->nombre,
            $this->numero_documento,
            $this->documento_compra,
            $fechaDocumento,
            $totalDocumento,
            $totalDocumentoActualizado,
            $botones
        );

        return $docJson;
    }

    /**
     * Función que retorna un array para la tabla deuda flotante
     */
    public function getDatosDeudaFlotante( $filtroItemPresupuestario = null, $verDocumento, $problemasDocumento )
    {
        
        $fechaDocumento = $this->fecha_documento;
        $fechaRecepcion = $this->fecha_recepcion;
        $itemsDevengo = '';
        // $totalDocumento = '';
        foreach ( $this->getDevengos as $devengo ) {

            if ( $filtroItemPresupuestario != null ) {

                //comprobar si el item se encuentra en el filtro
                if ( in_array($devengo->id_item_presupuestario, $filtroItemPresupuestario) ) {
                    
                    $itemsDevengo .= $devengo->getItemPresupuestario->codigo().'</br>';
                    // $totalDocumento .= '$ '.formatoMiles($devengo->monto).'</br>';

                }
                
            } else {
                
                $itemsDevengo .= $devengo->getItemPresupuestario->codigo().'</br>';
                // $totalDocumento .= '$ '.formatoMiles($devengo->monto).'</br>';

            }
            
        }
        
        $totalDocumentoActualizado = $this->total_documento_actualizado;
        
        $botones = '<div class="btn-group">';
        
        $botones .= '<button type="button" class="btn btn-success btn-xs" title="Ver Archivos" onclick="imprimir('. $this->id .');" >';
        $botones .= '<i class="fas fa-eye fa-lg"></i></button>';

        // Trazabilidad Documento
        // if ( \Entrust::can('ver-documento') ) {
        if ( $verDocumento ) {
            $botones .= '<button type="button" class="btn btn-primary btn-xs" title="Trazabilidad documento" onclick="traza('. $this->id .');">';
            $botones .= '<i class="fas fa-map-signs fa-lg"></i></button>';
        }
        
        // Boton para pasar a documento con problema
        // if ( \Entrust::can('problemas-documento') && $this->id_user_problema == null ) {
        if ( $problemasDocumento && $this->id_user_problema == null ) {
            $botones .= '<button type="button" class="btn btn-default btn-xs" title="Documento con problema" onclick="problema('. $this->id .');">';
            $botones .= '<i class="fas fa-exclamation-triangle fa-lg" style="color: #d9534f;border-color: #d43f3a;"></i></button>';
        }

        $botones .= '</div>';

        $estado = '<strong>';
        $estado .= $this->estadoDocumentoDeudaFlotante();        

        // Validar el monto actualizado con el monto de las relaciones
        // if ( $this->getRelacionRecepcionesValidadas->count() > 0 && $this->total_documento_actualizado != $this->getRelacionRecepcionesValidadas->sum('getWsDocumento.documento_total') ) {
        //     $estado = 'Diferencia($) con recepción.';
        // }

        if ( $this->id_user_problema != null ) {
            $estado .= '<br>Con problemas';
        }
        $estado .= '</strong>';

        $antiguedad = $this->dias_antiguedad;
        $nomina = $this->getNominaPagoDocumento->count() > 0 ? 'Si':'No';

        $docJson = array(
            'DT_RowID' => $this->id,
            $antiguedad,
            $estado,
            $nomina,
            $this->getTipoDocumento ? $this->getTipoDocumento->nombre : '',
            $this->numero_documento,
            $this->getProveedor ? $this->getProveedor->rut.' '.$this->getProveedor->nombre : '',
            $this->getTipoInforme ? $this->getTipoInforme->nombre : '',
            $this->documento_compra,
            $fechaDocumento,
            $fechaRecepcion,
            $itemsDevengo,
            $totalDocumentoActualizado,
            $this->getDeuda(),
            $botones
        );

        return $docJson;
    }

    public function getDatosDocumentoParaNomina($verDocumento)
    {
        
        $checkBox = "<input class='select_item' name='id[]' value='{$this->id}' type='checkbox' ";
        $checkBox .= " data-monto='{$this->total_documento_actualizado}' />";
        $fechaDocumento = $this->fecha_documento;
        $totalDocumentoActualizado = $this->total_documento_actualizado;
        $deuda = $this->getDeuda();

        $botones = '<div class="btn-group">';
        // Trazabilidad Documento
        if ( $verDocumento ) {
            $botones .= '<button type="button" class="btn btn-primary btn-xs" title="Trazabilidad Documento" onclick="traza('. $this->id .');">';
            $botones .= '<i class="fas fa-map-signs fa-lg"></i></button>';
        }
        $botones .= '</div>';

        $factoring = '';
        if ( $this->getProveedorFactoring->id != null ) {
            $factoring = $this->getProveedorFactoring->rut.' '.$this->getProveedorFactoring->nombre;
        }

        $docJson = array(
            'DT_RowID' => $this->id,
            $checkBox,
            $this->dias_antiguedad,
            $this->getProveedor->rut.' '.$this->getProveedor->nombre,
            $factoring,
            $this->getTipoDocumento->nombre,
            $this->numero_documento,
            $this->getTipoInforme ? $this->getTipoInforme->nombre : '',
            $fechaDocumento,
            $totalDocumentoActualizado,
            $deuda,
            $botones
        );

        return $docJson;
    }

    public function getDatosDocumentosDeNominaPago($verDocumento)
    {

        $fechaDocumento = fecha_dmY($this->fecha_documento);
        $deuda = '$ '.formatoMiles($this->getDeuda());
        $totalDocumentoActualizado = '$ '.formatoMiles($this->total_documento_actualizado);

        $botones = '<div class="btn-group">';

        // Trazabilidad Documento
        if ( $verDocumento ) {
            $botones .= '<button type="button" class="btn btn-primary btn-xs" title="Trazabilidad documento" onclick="traza('. $this->id .');">';
            $botones .= '<i class="fas fa-map-signs fa-lg"></i></button>';
        }

        $botones .= '<button type="button" class="btn btn-danger btn-xs" title="Quitar documento de la nomina" onclick="eliminarDoc('. $this->id .');">';
        $botones .= '<i class="fas fa-trash"></i></button>';

        $botones .= '</div>';

        $factoring = '';
        if ( $this->getProveedorFactoring->id != null ) {
            $factoring = $this->getProveedorFactoring->rut.' '.$this->getProveedorFactoring->nombre;
        }

        $docJson = array(
            'DT_RowID' => $this->id.'_'.$this->id_comprobante_contable_documento,
            $this->dias_antiguedad,
            $this->getEstadoDocumento(),
            $this->getProveedor->rut.' '.$this->getProveedor->nombre,
            $factoring,
            $this->getTipoDocumento->nombre,
            $this->numero_documento,
            $this->getTipoInforme ? $this->getTipoInforme->nombre : '',
            $fechaDocumento,
            $totalDocumentoActualizado,
            $deuda,
            $botones
        );

        return $docJson;
    }

    /**
     * Función que retorna un array para la tabla de documentos pagados
     */
    public function getDatosPagados( $filtroItemPresupuestario = null, $filtroPorNumeroDocumento, $verDocumento )
    {
        
        $fechaDocumento = $this->fecha_documento;
        $fechaComprobante = $this->getFechasComprobantes(null, $filtroPorNumeroDocumento);
        $numeroEgreso = $this->getNumerosComprobantes(null, $filtroPorNumeroDocumento);

        $itemsDevengo = '';
        foreach ( $this->getDevengos as $devengo ) {

            if ( $filtroItemPresupuestario != null ) {

                //comprobar si el item se encuentra en el filtro
                if ( in_array($devengo->id_item_presupuestario, $filtroItemPresupuestario) ) {
                    
                    $itemsDevengo .= $devengo->getItemPresupuestario->codigo().'</br>';
                    // $totalDocumento .= '$ '.formatoMiles($devengo->monto).'</br>';

                }
                
            } else {
                
                $itemsDevengo .= $devengo->getItemPresupuestario->codigo().'</br>';
                // $totalDocumento .= '$ '.formatoMiles($devengo->monto).'</br>';

            }
            
        }
        
        $reversadoDocumento = $this->getReversaComprobante->sum('monto');

        $flujoDocumento = 0;
        foreach ( $this->getComprobanteContableDocumento as $comprobante ) {

            try {

                if ( $comprobante->getComprobanteContable ) {
                    if ( $comprobante->getComprobanteContable->id_tipo_comprobante_contable == 1 ) {
                        $flujoDocumento += $comprobante->monto_comprobante;
                    } else {
                        $flujoDocumento -= $comprobante->monto_comprobante;
                    }
                }

            } catch (\Exception $e) {
                dd('Error', $e->getMessage(), $e->getFile(), $e->getLine(), $comprobante, $this);
            }

        }

        $flujoDocumento -= $reversadoDocumento;
        // $flujoEfectivo = $flujoDocumento;
        
        $botones = '<div class="btn-group">';
        
        // Trazabilidad Documento
        if ( $verDocumento ) {
            $botones .= '<button type="button" class="btn btn-primary btn-xs" title="Trazabilidad documento" onclick="traza('. $this->id .');">';
            $botones .= '<i class="fas fa-map-signs fa-lg"></i></button>';
        }
        
        // Boton para pasar a documento con problema
        // if ( \Entrust::can('problemas-documento') && $this->id_user_problema == null ) {
        //     $botones .= '<button type="button" class="btn btn-default btn-xs" title="Documento con problema" onclick="problema('. $this->id .');">';
        //     $botones .= '<i class="fas fa-exclamation-triangle fa-lg" style="color: #d9534f;border-color: #d43f3a;"></i></button>';
        // }

        $botones .= '</div>';

        $docJson = array(
            'DT_RowID' => $this->id,
            $this->getEstadoDocumento(),
            $this->getTipoDocumento->nombre,
            $this->numero_documento,
            $this->getProveedor->rut.' '.$this->getProveedor->nombre,
            $this->getModalidadCompra->nombre,
            $this->documento_compra,
            $fechaDocumento,
            $fechaComprobante,
            $numeroEgreso,
            $itemsDevengo,
            $flujoDocumento,
            $botones
        );

        return $docJson;
    }

    /**
     * Comprobar si el documento tiene problema.
     * En base a la relacion establecida, o a futura relacion con recepcion
     * del sistema hermes.
     * Solo si la diferencia entre la recepcion y el documento es mayor a
     * 100, el documento es pasado a con problema.
     */
    public function setProblemaDocumento()
    {
        /**
         * 1° evaluar relacion en el sistema
         * 2° evaluar relacion con hermes (recepcion)
         */
        $observacion = '';
        $diferencia = 0;
        $problema = false;

        if ( $this->getRelacionRecepcionesValidadas->count() > 0 ) {

            $sumaMontoRelaciones = 0;

            foreach ( $this->getRelacionRecepcionesValidadas as $key => $relacion ) {
                $sumaMontoRelaciones += $relacion->getWsDocumento->documento_total;
            }

            if ( $this->total_documento_actualizado > $sumaMontoRelaciones ) {

                $diferencia = $this->total_documento_actualizado - $sumaMontoRelaciones;

                if ( $diferencia > 100 ) {
                    $observacion .= 'Es necesaria una NC. Por $ '.formatoMiles($diferencia);
                }

            } else {

                $diferencia = $sumaMontoRelaciones - $this->total_documento_actualizado;

                if ( $diferencia > 100 ) {
                    $observacion .= 'Es necesaria una ND. Por $ '.formatoMiles($diferencia);
                }

            }

        } elseif ( $this->getRelacionRecepcionesNoValidadas->count() > 0 ) {

            $sumaMontoRelaciones = 0;

            foreach ( $this->getRelacionRecepcionesNoValidadas as $key => $relacion ) {
                $sumaMontoRelaciones += $relacion->getHermesDocumento->documento_total;
            }

            if ( $this->total_documento_actualizado > $sumaMontoRelaciones ) {

                $diferencia = $this->total_documento_actualizado - $sumaMontoRelaciones;

                if ( $diferencia > 100 ) {
                    $observacion .= 'Es necesaria una NC. Por $ '.formatoMiles($diferencia);
                }

            } else {

                $diferencia = $sumaMontoRelaciones - $this->total_documento_actualizado;

                if ( $diferencia > 100 ) {
                    $observacion .= 'Es necesaria una ND. Por $ '.formatoMiles($diferencia);
                }

            }

        } else {

            $hermesDocumento = HermesDocumento::where('rut_proveedor', $this->getProveedor->rut )
                                              ->where('folio_documento', $this->numero_documento )
                                              ->where('documento_total', '!=', $this->total_documento_actualizado )
                                              ->doesnthave('getRelacion')
                                              ->first();

            if ( is_object($hermesDocumento) ) {

                if ( $this->total_documento_actualizado > $hermesDocumento->documento_total ) {

                    $diferencia = $this->total_documento_actualizado - $hermesDocumento->documento_total;
                    if ( $diferencia > 100 ) {
                        $observacion .= 'Es necesaria una NC. Por $ '.formatoMiles($diferencia);
                    }

                } else {

                    $diferencia = $hermesDocumento->documento_total - $this->total_documento_actualizado;
                    if ( $diferencia > 100 ) {
                        $observacion .= 'Es necesaria una ND. Por $ '.formatoMiles($diferencia);
                    }
                    
                }
            }

            // Se evalua la diferencia, y se pasa a documento con problema, en caso que supere los 100
            if ( $diferencia > 100 ) {

                $this->observacion_problema = \Auth::user()->name.': '.$observacion;
                $this->id_user_problema = \Auth::user()->id;
                $this->save();

                $documentoMotivoProblema = new DocumentoMotivoProblema();
                $documentoMotivoProblema->id_documento = $this->id;
                $documentoMotivoProblema->id_motivo_problema = 9; // Otros, detallar.
                $documentoMotivoProblema->save();

                $problema = true;

            }

            
        }

        return $problema;
    }

    /**
     * Entrega el porcentaje del impuesto para el documento
     */
    public function getLabelImpuesto()
    {
        $label = '';
        if (fecha_Y($this->fecha_documento) == 2019 ) {
            $label .= '10%';
        }

        if (fecha_Y($this->fecha_documento) == 2020 ) {
            $label .= '10,75%';
        }

        return $label;
    }

        
    public function setObservacionDocumento()
    {

        if ( $this->id_tipo_documento == 4 || $this->id_tipo_documento == 10 ) {
            // NC y CC, resta al total
        if ( $this->getDocumentoRelacionado != null ) {
            $this->observacion .= PHP_EOL.'Disminuye '.$this->getDocumentoRelacionado->getTipoDocumento->nombre.' N° '.$this->getDocumentoRelacionado->numero_documento;
            $this->save();

            //    $this->getDocumentoRelacionado->observacion .= PHP_EOL.$this->getTipoDocumento->nombre.' N° '.$this->numero_documento.' Disminuye $ '.formatoMiles($this->total_documento_actualizado);
            //    $this->getDocumentoRelacionado->save();
        }
        }

        if ( $this->id_tipo_documento == 5 || $this->id_tipo_documento == 11 ) {
            // ND y CD, suma al total
            if ( $this->getDocumentoRelacionado != null ) {
                $this->observacion .= PHP_EOL.'Aumenta '.$this->getDocumentoRelacionado->getTipoDocumento->nombre.' N° '.$this->getDocumentoRelacionado->numero_documento;
                $this->save();

                // $this->getDocumentoRelacionado->observacion .= PHP_EOL.$this->getTipoDocumento->nombre.' N° '.$this->numero_documento.' Aumenta $ '.formatoMiles($this->total_documento_actualizado);
                // $this->getDocumentoRelacionado->save();
            }
        }

        if ( $this->getDocumentosRelacionados->count() > 0 ) {

            foreach ( $this->getDocumentosRelacionados as $docRelacionado ){

                if ( $docRelacionado->id_tipo_documento == 4 || $docRelacionado->id_tipo_documento == 10 ) {
                    // NC y CC, resta al total
                
                    $this->observacion .= PHP_EOL.$docRelacionado->getTipoDocumento->nombre.' N° '.$docRelacionado->numero_documento.' Disminuye $ '.formatoMiles($docRelacionado->total_documento_actualizado);
                    
                }

                if ( $docRelacionado->id_tipo_documento == 5 || $docRelacionado->id_tipo_documento == 11 ) {
                    // ND y CD, suma al total
                
                    $this->observacion .= PHP_EOL.$docRelacionado->getTipoDocumento->nombre.' N° '.$docRelacionado->numero_documento.' Aumenta $ '.formatoMiles($docRelacionado->total_documento_actualizado);
                    
                }

            }

            $this->save();

        }

    }

    /**
     * Arregla observaciones al documento dependiente del tipo y de sus relaciones
     */
    public function setObservacionDocumentoIngreso()
    {

        if ( $this->id_tipo_documento == 4 || $this->id_tipo_documento == 10 ) {
            // NC y CC, resta al total
        if ( $this->getDocumentoRelacionado != null ) {
            $this->observacion .= PHP_EOL.'Disminuye '.$this->getDocumentoRelacionado->getTipoDocumento->nombre.' N° '.$this->getDocumentoRelacionado->numero_documento;
            $this->save();

            $this->getDocumentoRelacionado->observacion .= PHP_EOL.$this->getTipoDocumento->nombre.' N° '.$this->numero_documento.' Disminuye $ '.formatoMiles($this->total_documento_actualizado);
            $this->getDocumentoRelacionado->save();
        }
        }

        if ( $this->id_tipo_documento == 5 || $this->id_tipo_documento == 11 ) {
            // ND y CD, suma al total
            if ( $this->getDocumentoRelacionado != null ) {
                $this->observacion .= PHP_EOL.'Aumenta '.$this->getDocumentoRelacionado->getTipoDocumento->nombre.' N° '.$this->getDocumentoRelacionado->numero_documento;
                $this->save();

                $this->getDocumentoRelacionado->observacion .= PHP_EOL.$this->getTipoDocumento->nombre.' N° '.$this->numero_documento.' Aumenta $ '.formatoMiles($this->total_documento_actualizado);
                $this->getDocumentoRelacionado->save();
            }
        }

        if ( $this->getDocumentosRelacionados->count() > 0 ) {

            foreach ( $this->getDocumentosRelacionados as $docRelacionado ){

                if ( $docRelacionado->id_tipo_documento == 4 || $docRelacionado->id_tipo_documento == 10 ) {
                    // NC y CC, resta al total
                
                    $this->observacion .= PHP_EOL.$docRelacionado->getTipoDocumento->nombre.' N° '.$docRelacionado->numero_documento.' Disminuye $ '.formatoMiles($docRelacionado->total_documento_actualizado);
                    
                }

                if ( $docRelacionado->id_tipo_documento == 5 || $docRelacionado->id_tipo_documento == 11 ) {
                    // ND y CD, suma al total
                
                    $this->observacion .= PHP_EOL.$docRelacionado->getTipoDocumento->nombre.' N° '.$docRelacionado->numero_documento.' Aumenta $ '.formatoMiles($docRelacionado->total_documento_actualizado);
                    
                }

            }

            $this->save();

        }

    }

    /**
     * Devuelve un string con las fechas de los comprobantes
     */
    public function getFechasComprobantes($excel = null, $filtroPorNumeroDocumento)
    {
        $fechaComprobante = '';
        if ( $filtroPorNumeroDocumento != null && $this->getComprobanteContableDocumento->last()->getComprobanteContable &&
             $this->getComprobanteContableDocumento->last()->getComprobanteContable->id_tipo_comprobante == 1
            ) {
                $fechaComprobante .= fecha_dmY($this->getComprobanteContableDocumento->last()->getComprobanteContable->fecha_proceso);
        } else {
        
            foreach( $this->getComprobanteContableDocumento as $key => $comprobanteDocumento ) {
                
                if ( $comprobanteDocumento->getComprobanteContable && 
                     $comprobanteDocumento->getComprobanteContable->id_tipo_comprobante_contable == 1 
                    ) {

                    $fechaComprobante .= fecha_dmY($comprobanteDocumento->getComprobanteContable->fecha_proceso);
                    
                    if ( $key+1 < $this->getComprobanteContableDocumento->count() ) {

                        $fechaComprobante .= $excel == null ? '<br>' : ' | ';
                        
                    }

                }
                
            }

        }

        return $fechaComprobante;
    }

    /**
     * Devuelve un string con los folios de los comprobantes
     */
    public function getNumerosComprobantes($excel = null, $filtroPorNumeroDocumento = null)
    {
        
        $numeroEgreso = '';
        if ( $filtroPorNumeroDocumento != null && $this->getComprobanteContableDocumento->last()->getComprobanteContable &&
             $this->getComprobanteContableDocumento->last()->getComprobanteContable->id_tipo_comprobante == 1 
            ) {

            $numeroEgreso .= $this->getComprobanteContableDocumento->last()->getComprobanteContable->getNumeroComprobante(6);

        } else {

            foreach( $this->getComprobanteContableDocumento as $key => $comprobanteDocumento ) {
                
                if ( $comprobanteDocumento->getComprobanteContable && 
                     $comprobanteDocumento->getComprobanteContable->id_tipo_comprobante_contable == 1   
                    ) {
                    
                    $numeroEgreso .= $comprobanteDocumento->getComprobanteContable->getNumeroComprobante(6);
                    if ( $key+1 < $this->getComprobanteContableDocumento->count() ) {

                        $numeroEgreso .= $excel == null ? '<br>' : ' | ';
                        
                    }

                }
                
            }

        }

        return $numeroEgreso;
    }

    /**
     * Devuelve un string con los folios de los comprobantes
     */
    public function getFoliosComprobantes($excel = null, $filtroPorNumeroDocumento = null)
    {

        $folioEgreso = '';
        if ( $filtroPorNumeroDocumento != null && $this->getComprobanteContableDocumento->last()->getComprobanteContable &&
             $this->getComprobanteContableDocumento->last()->getComprobanteContable->id_tipo_comprobante == 1 
            ) {
            
            $folioEgreso .= $this->getComprobanteContableDocumento->last()->getComprobanteContable->folio;

        } else {

            foreach( $this->getComprobanteContableDocumento as $key => $comprobanteDocumento ) {
                
                if ( $comprobanteDocumento->getComprobanteContable && 
                     $comprobanteDocumento->getComprobanteContable->id_tipo_comprobante_contable == 1 
                    ) {
                    
                    $folioEgreso .= $comprobanteDocumento->getComprobanteContable->folio;
                    if ( $key+1 < $this->getComprobanteContableDocumento->count() ) {

                        $folioEgreso .= $excel == null ? '<br>' : ' | ';

                    }

                }
                
            }

        }

        return $folioEgreso;
    }

    /**
     * Retorna:
     * + suma monto comprobante de egreso
     * - resta monto comprobante de traspaso
     * - resta monto comprobante de ingreso
     * + suma sumatoria monto de reversas asociadas
     */
    public function getPagado()
    {

        $flujoDocumento = 0;
        foreach ( $this->getComprobanteContableDocumento as $comprobante ) {
                
            if ( $comprobante->getComprobanteContable ) {
                if ( $comprobante->getComprobanteContable->id_tipo_comprobante_contable == 1 ) {
                    $flujoDocumento += $comprobante->monto_comprobante;
                } else {
                    $flujoDocumento -= $comprobante->monto_comprobante;
                }
            }
            
        }

        return $flujoDocumento + $this->getReversaComprobante->sum('monto');

    }

    /**
     * Devuelve string: Cuadrado o Descuadrado.
     * Considera que el total actualizado del documento sea igual
     * a la suma de las recepciones relacionadas.
     */
    public function estadoDocumentoDeudaFlotante()
    {

        $estado = '';
        if ( $this->validado != null ) {
            $estado = 'Cuadrado';
            
            // Validar el monto actualizado con el monto de las relaciones
            // if ( $this->getRelacionRecepcionesValidadas->count() > 0 || $this->getRelacionRecepcionesNoValidadas->count() > 0 ) {
            //     if ( $this->diferenciaConRecepciones() > 100 || $this->diferenciaConRecepciones() < -100 ) {
            //         $estado = 'Descuadrado';
            //     }
            // }

            if ( $this->getRelacionRecepcionesValidadas->count() > 0 ) {
                if ( $this->diferenciaConRecepciones() > 100 || $this->diferenciaConRecepciones() < -100 ) {
                    $estado = 'Descuadrado Por diferencia de montos';
                    // $estado = 'Descuadrado';
                }
            }
            
        } else {
            $estado = 'Descuadrado';
        }
        
        return $estado;

    }

    public function unirConContratoOrdenCompra($contrato, $oc, $request, Documento $documentoValidacionContratoOC = null)
    {

        /**
         * $documentoValidacionContratoOC viene de la validacion de la edicion del documento,
         * por lo tanto tiene los montos antiguos del documento, los que deben cuadrar con la
         * conexion previa con el contrato y la oc
         */

        if ( $this->getContratoOrdenCompra ) {
            /**
             * De esta forma se devuelven los montos a los daldos,
             *  antes de realizar todo el descuento y actualizar lo saldos correspondientes
             */
            $documentoValidacionContratoOC->devolverMontos('contrato');
            $documentoValidacionContratoOC->devolverMontos('oc');
        }

        if ( is_object($contrato) || is_object($oc) ) {

            if ( $this->getContratoOrdenCompra ) {
                $unionContratoOc = $this->getContratoOrdenCompra;
            } else {
                $unionContratoOc = new DocumentoContratoOrdenCompra;
            }

            if ( $request->input('horas_boleta') ) {
                $unionContratoOc->horas_boleta = formato_decimales( $request->input('horas_boleta') );
            } else {
                $unionContratoOc->horas_boleta = null;
            }

            if ( is_object($contrato) ) {

                $contrato->refresh(); // obtiene los datos de la bd nuevamente.
                $unionContratoOc->id_contrato = $contrato->id;

                if ( $contrato->saldo_preventivo != null || $contrato->saldo_correctivo != null ) {

                    if ( $request->input('montoOcupar') == 'preventivo' ) {

                        $unionContratoOc->ocupa_preventivo = 1;

                        if ( $this->id_tipo_documento == 4 || $this->id_tipo_documento == 10 ) {
                            $contrato->saldo_preventivo = $contrato->saldo_preventivo + $this->total_documento;
                        } else {
                            $contrato->saldo_preventivo = $contrato->saldo_preventivo - $this->total_documento;
                        }

                    } else {
                        $unionContratoOc->ocupa_preventivo = 0;
                    }

                    if ( $request->input('montoOcupar') == 'correctivo' ) {

                        $unionContratoOc->ocupa_correctivo = 1;

                        if ( $this->id_tipo_documento == 4 || $this->id_tipo_documento == 10 ) {
                            $contrato->saldo_correctivo = $contrato->saldo_correctivo + $this->total_documento;
                        } else {
                            $contrato->saldo_correctivo = $contrato->saldo_correctivo - $this->total_documento;
                        }
                        
                    } else {
                        $unionContratoOc->ocupa_correctivo = 0;
                    }

                } else {

                    $unionContratoOc->ocupa_preventivo = 0;
                    $unionContratoOc->ocupa_correctivo = 0;

                }

                if ( $this->id_tipo_documento == 4 || $this->id_tipo_documento == 10 ) {
                    $contrato->saldo_contrato = $contrato->saldo_contrato + $this->total_documento;
                } else {
                    $contrato->saldo_contrato = $contrato->saldo_contrato - $this->total_documento;
                }

                $contrato->id_user_updated = \Auth::user()->id;
                $contrato->save();
                

            } else {

                $unionContratoOc->id_contrato = null;
                $unionContratoOc->ocupa_preventivo = 0;
                $unionContratoOc->ocupa_correctivo = 0;

            }

            if ( is_object($oc) ) {

                $oc->refresh(); // obtiene los datos de la bd nuevamente.
                $unionContratoOc->id_orden_compra = $oc->id;

                if ( $this->id_tipo_documento == 4 || $this->id_tipo_documento == 10 ) {
                    $oc->saldo_oc = $oc->saldo_oc + $this->total_documento;
                } else {
                    $oc->saldo_oc = $oc->saldo_oc - $this->total_documento;
                }

                $oc->id_user_updated = \Auth::user()->id;
                $oc->save();

            } else {

                $unionContratoOc->id_orden_compra = null;

            }

            if ( ! $this->getContratoOrdenCompra ) {
                $unionContratoOc->id_user_created = \Auth::user()->id;
                $unionContratoOc->updated_at = NULL;
            }

            $unionContratoOc->id_documento = $this->id;
            $unionContratoOc->save();

        } else {

            if ($this->getContratoOrdenCompra) {
                $this->getContratoOrdenCompra->delete();
            }

        }
        
    }

    /**
     * Devuelve los montos a los saldos correspondientes del contrato y OC
     */
    public function devolverMontos($opcion = null)
    {
        if ( $opcion == null ) {

            return 'Error en devolver montos,documento';

        } else {

            if ( $opcion == 'contrato' && $this->getContratoOrdenCompra && $this->getContratoOrdenCompra->getContrato ) {

                if ( $this->getContratoOrdenCompra->ocupa_preventivo == 1 ) {

                    if ( $this->id_tipo_documento == 4 || $this->id_tipo_documento == 10 ) {

                        $this->getContratoOrdenCompra->getContrato->saldo_preventivo = $this->getContratoOrdenCompra->getContrato->saldo_preventivo - $this->total_documento;

                    } else {

                        $this->getContratoOrdenCompra->getContrato->saldo_preventivo  = $this->getContratoOrdenCompra->getContrato->saldo_preventivo  + $this->total_documento;

                    }

                }

                if ( $this->getContratoOrdenCompra->ocupa_correctivo == 1 ) {

                    if ( $this->id_tipo_documento == 4 || $this->id_tipo_documento == 10 ) {

                        $this->getContratoOrdenCompra->getContrato->saldo_correctivo = $this->getContratoOrdenCompra->getContrato->saldo_correctivo - $this->total_documento;

                    } else {

                        $this->getContratoOrdenCompra->getContrato->saldo_correctivo = $this->getContratoOrdenCompra->getContrato->saldo_correctivo + $this->total_documento;

                    }

                }

                if ( $this->id_tipo_documento == 4 || $this->id_tipo_documento == 10 ) {

                    $this->getContratoOrdenCompra->getContrato->saldo_contrato = $this->getContratoOrdenCompra->getContrato->saldo_contrato - $this->total_documento;

                } else {

                    $this->getContratoOrdenCompra->getContrato->saldo_contrato = $this->getContratoOrdenCompra->getContrato->saldo_contrato + $this->total_documento;

                }

                $this->getContratoOrdenCompra->getContrato->id_user_updated = \Auth::user()->id;
                $this->getContratoOrdenCompra->getContrato->save();

            }

            if ( $opcion == 'oc' && $this->getContratoOrdenCompra && $this->getContratoOrdenCompra->getOrdenCompra ) {

                if ( $this->id_tipo_documento == 4 || $this->id_tipo_documento == 10 ) {

                    $this->getContratoOrdenCompra->getOrdenCompra->saldo_oc = $this->getContratoOrdenCompra->getOrdenCompra->saldo_oc - $this->total_documento;

                } else {

                    $this->getContratoOrdenCompra->getOrdenCompra->saldo_oc = $this->getContratoOrdenCompra->getOrdenCompra->saldo_oc + $this->total_documento;

                }

                $this->getContratoOrdenCompra->getOrdenCompra->id_user_updated = \Auth::user()->id;
                $this->getContratoOrdenCompra->getOrdenCompra->save();

            }

        }
    }

    public function getContratoOrdenCompra()
    {    
        return $this->hasOne('App\DocumentoContratoOrdenCompra','id_documento');
    }

    /**
     * Retorna:
     * total_documento actualizado - getPagado()
     * getPagado() retorna:
     * + monto comprobante de egreso (id=1)
     * - monto comprobante de traspaso
     * - monto comprobante de ingreso
     * + sumatoria monto de reversas asociadas
     */
    public function getDeuda()
    {
        return $this->total_documento_actualizado - $this->getPagado();
    }

    /**
     * Trae documento asociados al contrato, para el saldo contrato
     */
    public function scopeDocumentosDelContratoSegunPeriodo($query, $periodoConsulta, $idContrato)
    {
        return $query->whereIn('id_tipo_documento',[1,2,3,7,9]) // Factura electronica, factura, boleta de honorarios, boleta de venta y servicio
                     ->where('periodo', $periodoConsulta)
                     ->whereHas('getContratoOrdenCompra', function($q) use ($idContrato) {
                            $q->where('id_Contrato', $idContrato);
                     });
    }

    public function scopeDocumentosParaNominas($query)
    {

        return $query->with([
                            'getProveedor' => function ($query) {
                                $query->select('id', 'nombre', 'rut');
                            },
                            'getTipoDocumento' => function ($query) {
                                $query->select('id', 'nombre');
                            },
                            'getProveedorFactoring',
                            'getComprobanteContableDocumento.getComprobanteContable',
                            'getReversaComprobante',
                            'getVistoBueno',
                            'getTipoInforme',
                            'getRelacionRecepcionesValidadas',
                            'getRelacionRecepcionesNoValidadas'
                        ])
                        // ->whereIn('id_tipo_documento', [1, 2, 3, 6, 7, 8, 9])
                        ->whereNotIn('id_tipo_documento', [4, 5, 10, 11])
                        ->has('getFoliosSigfe')
                        ->doesnthave('getNominaPagoDocumento');

    }

    public function scopePagados( $query, $mesInicio, $añoInicio, $mesTermino, $añoTermino )
    {

        return $query->with([
                            // para el estado del documento
                            'getUnaRelacionRecepcionValidada', 'getVistoBueno',
                            'getProveedor','getTipoDocumento',
                            'getModalidadCompra',
                            'getDevengos.getItemPresupuestario',
                            'getComprobanteContableDocumento',
                            'getComprobanteContableDocumento.getComprobanteContable' => function ($q) 
                                use ($mesInicio, $añoInicio, $mesTermino, $añoTermino) {

                                $q->where('mes_proceso', '>=', $mesInicio)
                                ->where('mes_proceso', '<=', $mesTermino)
                                ->where('año_proceso', '>=', $añoInicio)
                                ->where('año_proceso', '<=', $añoTermino);

                            },
                            'getReversaComprobante' => function ($q) use ($mesInicio, $añoInicio, $mesTermino, $añoTermino) {

                                $q->where('mes_sigfe', '>=', $mesInicio) 
                                ->where('mes_sigfe', '<=', $mesTermino)
                                ->where('year_sigfe', '>=', $añoInicio)
                                ->where('year_sigfe', '<=', $añoTermino);

                            },
                        ])
                        ->whereIn('id', function ($query) use ($mesInicio, $añoInicio, $mesTermino, $añoTermino) {
                            $query->select('id_documento')->from('comprobantes_contables_documentos')
                                ->whereIn('id_comprobante_contable', function ($query) use ($mesInicio, $añoInicio, $mesTermino, $añoTermino) {
                                    $query->select('id')->from('comprobantes_contables')
                                            ->where('mes_proceso', '>=', $mesInicio)
                                            ->where('mes_proceso', '<=', $mesTermino)
                                            ->where('año_proceso', '>=', $añoInicio)
                                            ->where('año_proceso', '<=', $añoTermino);
                                });
                        });


    }

    public function scopeDeudas( $query )
    {

        return $query->with([
                            'getRelacionRecepcionesValidadas',
                            'getRelacionRecepcionesNoValidadas',
                            'getDevengos.getItemPresupuestario',
                            'getTipoInforme',
                            'getProveedor',
                            'getTipoDocumento',
                            'getComprobanteContableDocumento.getComprobanteContable',
                            'getReversaComprobante',
                            'getNominaPagoDocumento',
                            'getVistoBueno'
                       ])
                       /**
                        * Factura electronica, factura, boleta de honorarios,
                        * Rendicion fondo fijo, boleta de venta y servicios, 
                        * Boleta exenta elenctrónica
                        */
                     ->whereIn('id_tipo_documento', [1,2,3,6,7,9])
                     ->where('id_estado', '!=', 1)
                     ->where('documentos.total_documento_actualizado', '>', 0);

    }

    /**
     * Retorna la diferencia entre el total_documento_actualizado
     * y el total de las recepciones (WsDocumento o HermesDocumento).
     * Es necesario validar que el documento tiene relaciones antes de utilizar esta función.
     */
    public function diferenciaConRecepciones()
    {
        $this->fresh();
        $totalRecepciones = 0;

        // if ( $this->getRelacionRecepcionesValidadas->count() > 0 || $this->getRelacionRecepcionesNoValidadas->count() > 0 ) {
            
        //     if ( $this->getRelacionRecepcionesValidadas->count() > 0 ) {
        //         $totalRecepciones = $this->getRelacionRecepcionesValidadas->sum('getWsDocumento.documento_total');
        //     }
        //     if ( $this->getRelacionRecepcionesNoValidadas->count() > 0 ) {
        //         $totalRecepciones = $this->getRelacionRecepcionesNoValidadas->sum('getHermesDocumento.documento_total');
        //     }

        //     return $totalRecepciones - $this->total_documento_actualizado;
        //     // if ( $dif > 100 || $dif < -100 ) {
        //     //     // no cuadrar
        //     // } 
        // }
        if ( $this->getRelacionRecepcionesValidadas->count() > 0 ) {
            
            if ( $this->getRelacionRecepcionesValidadas->count() > 0 ) {
                $totalRecepciones = $this->getRelacionRecepcionesValidadas->sum('getWsDocumento.documento_total');
            }

            return $totalRecepciones - $this->total_documento_actualizado;
            // if ( $dif > 100 || $dif < -100 ) {
            //     // no cuadrar
            // } 
        }
    }

    /**
     * Busca si tiene recepciones asociadas.
     * Si tiene recepciones asociadas:
     * - valida el documento si el total_documento_actualizado es igual al monto total de las recepcion.
     * - valida el documento si el monto total de las recepciones y el total_documento_actualizado no tienen una diferencia
     *   mayor a 100 pesos, positivos o negativos.
     * - no valida el documento si no se cumplen las dos opciones anteriores y se invalida el documento.
     * No tiene recepciones asociadas:
     * - valida el documento
     * 
     * Tiene un save() al final de la funcion
     */
    public function validar()
    {
        $cuadrar = true;
        $this->fresh();
        // if ( $this->getRelacionRecepcionesValidadas->count() > 0 || $this->getRelacionRecepcionesNoValidadas->count() > 0 ) {

        //     if ( $this->diferenciaConRecepciones() > 100 || $this->diferenciaConRecepciones() < -100 ) {
        //         // no cuadrar
        //         $cuadrar = false;
        //     }
            
        // }

        if ( $this->getRelacionRecepcionesValidadas->count() > 0 ) {

            if ( $this->diferenciaConRecepciones() > 100 || $this->diferenciaConRecepciones() < -100 ) {
                // no cuadrar
                $cuadrar = false;
            }
            
        }

        if ( $cuadrar ) {

            $this->validado = 1;
            $this->fecha_validado = date('Y-m-d H:i:s');
            if ( \Auth::check() ) {
                $this->id_user_validado = \Auth::user()->id;
            } else {

                $userAutomatico = User::where('rut','66666666-6')->first();
                if ( ! is_object($userAutomatico) ) {
                    $userAutomatico = new User();
                    $userAutomatico->rut = '66666666-6';
                    $userAutomatico->password = \Hash::make( substr($userAutomatico->rut,0,-2) );
                    $userAutomatico->name = 'Automático';
                    $userAutomatico->save();
                }

                $this->id_user_validado = $userAutomatico->id;

            }

            $this->save();

        } else {
            $this->invalidar();
        }
            
    }

    /**
     * Invalida el documento.
     * Tiene un save() al final de la funcion
     */
    public function invalidar()
    {
        $this->validado = null;
        $this->fecha_validado = null;
        if ( \Auth::check() ) {
            $this->id_user_validado = \Auth::user()->id;
        } else {

            $userAutomatico = User::where('rut','66666666-6')->first();
            if ( ! is_object($userAutomatico) ) {
                $userAutomatico = new User();
                $userAutomatico->rut = '66666666-6';
                $userAutomatico->password = \Hash::make( substr($userAutomatico->rut,0,-2) );
                $userAutomatico->name = 'Automático';
                $userAutomatico->save();
            }

            $this->id_user_validado = $userAutomatico->id;

        }

        $this->save();
    }

    public function scopeValidados( $query )
    {
        return $query->with(['getProveedor',
                             'getTipoDocumento',
                             'getDevengos.getItemPresupuestario',
                             'getRelacionRecepcionesNoValidadas'
                            ])
                     ->where('validado', 1)
                     ->where('id_user_problema', null);
    }

    public function scopePendientesValidacion( $query )
    {
        return $query->with(['getProveedor','getTipoDocumento','getDevengos.getItemPresupuestario'])
                     ->where('validado', null)
                     ->where('id_user_problema', null);
    }

    public function scopePorDevengar( $query )
    {
        return $query->with([
                                'getTipoInforme','getProveedor',
                                'getTipoDocumento','getModalidadCompra',
                                'getDevengos.getItemPresupuestario',
                                'getRelacionRecepcionesValidadas.getWsDocumento',
                                'getResponsable'
                            ])
                      ->where('id_estado', 1)
                      ->where('total_documento', '>', 0)
                      ->where('reclamado', null);
    }

    public function scopeConProblemas( $query )
    {
        return $query->with([
                            'getProveedor' => function ($query) {
                                $query->select('id', 'nombre', 'rut');
                            },
                            'getTipoDocumento' => function ($query) {
                                $query->select('id', 'nombre');
                            },
                            'getDevengos.getItemPresupuestario',
                            'getDocumentoMotivoProblema.getMotivoProblema',
                            'getTipoInforme',
                            'getRelacionRecepcionesNoValidadas'
                        ])
                    //  ->where('validado', null)
                     ->where('id_user_problema', '<>',null);
    }

    /**
     * Tipos Documentos:
     * Factura electronica
     * Factura
     * Rendición de fondo fijo
     * Boleta de venta y servicio
     * Boleta Exenta Electronica
     */
    public function scopePorValidar( $query )
    {
        return $query->with(['getDevengos'])
                    ->where('fecha_ingreso','>=','2019-06-22 00:00:00')
                    ->where('validado', null)
                    ->where('id_user_problema', null)
                    ->whereIn('id_tipo_documento', [1,2,6,7,9]);
    }

}
