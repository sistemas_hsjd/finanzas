<?php namespace App;

use Illuminate\Database\Eloquent\Model;

class AceptaMotivoRezagar extends Model
{
    protected $table = 'acepta_motivo_rezagar';

    public function getMotivoRezagar()
    {
        return $this->belongsTo('App\MotivoRezagar','id_motivo_rezagar');
    }

    public function getArchivoAcepta()
    {
        return $this->belongsTo('App\ArchivoAcepta','id_archivo_acepta');
    }
}
