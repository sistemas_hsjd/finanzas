<?php namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use OwenIt\Auditing\Contracts\Auditable;

class FolioSigfe extends Model implements Auditable
{
    use \OwenIt\Auditing\Auditable;
    use SoftDeletes;
    protected $table = 'folios_sigfe';

    /**
     * Attributes to include in the Audit.
     *
     * @var array
     */
    protected $auditInclude = [];
    
    /**
     * Should the timestamps be audited?
     *
     * @var bool
     */
    protected $auditTimestamps = true;

    public function getDocumento()
    {
        return $this->belongsTo('App\Documento', 'id_documento')->withTrashed();
    }

    public function getDevengos()
    {
        return $this->hasMany('App\Devengo','id_folio_sigfe','id');
    }

    /**
     * Scope
     */

    /**
      * Para traer los documentos que tienen deuda
      */
    public function scopeDeudaFlotante($query)
    {
        return $query->with([
                            'getDevengos',
                            'getDevengos.getItemPresupuestario',
                            'getDocumento',
                            'getDocumento.getTipoInforme',
                            'getDocumento.getProveedor',
                            'getDocumento.getTipoDocumento',
                            'getDocumento.getComprobanteContableDocumento.getComprobanteContable',
                            'getDocumento.getReversaComprobante',
                            'getDocumento.getNominaPagoDocumento',
                            'getDocumento.getVistoBueno',
                        ])
                    ->join('documentos','folios_sigfe.id_documento','=', 'documentos.id')
                    ->whereIn('documentos.id_tipo_documento', [1,2,3,6,7,9])
                    ->where('documentos.total_documento_actualizado', '>', 0);

                    // ->whereHas('getDocumento', function($query){
                    //     $query->whereIn('id_tipo_documento', [1,2,3,6,7,9])
                    //             ->where('total_documento_actualizado', '>', 0);
                    // });
    }
}
