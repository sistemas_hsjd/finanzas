<?php namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class NubeLibroCompra extends Model
{
    use SoftDeletes;
    protected $table = 'nube_libros_compras';

    public function getUser()
    {
        return $this->belongsTo('App\User','id_usuario_crea');
    }


}
