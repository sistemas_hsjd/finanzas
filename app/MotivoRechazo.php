<?php namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class MotivoRechazo extends Model
{
    use SoftDeletes;
    protected $table = 'motivos_rechazo';

    public function __construct( $request = null, array $attributes = array())
    {
        parent::__construct($attributes);
        if ($request != null ) {
            $this->nombre = trim($request->get('nombre'));
            $this->updated_at = NULL;
            $this->save();
        }
    }

    public function editMotivoRechazo($request)
    {
        $this->nombre = trim($request->get('nombre'));
        $this->save();
    }

    public function getAceptaMotivosRechazo()
    {
        return $this->hasMany('App\AceptaMotivoRechazo','id_motivo_rechazo','id');
    }
}
