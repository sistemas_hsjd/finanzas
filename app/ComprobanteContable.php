<?php namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class ComprobanteContable extends Model
{
    use SoftDeletes;
    protected $table = 'comprobantes_contables';

    public function getTipoComprobante()
    {
        return $this->belongsTo('App\TipoComprobanteContable','id_tipo_comprobante_contable');
    }

    public function getUsuario()
    {
        return $this->hasOne('App\User','id','id_user_created');
    }

    public function getComprobantesContablesDocumentos()
    {

        return $this->hasMany('App\ComprobanteContableDocumento','id_comprobante_contable','id')->latest();
    }

    public function getUnComprobanteContableDocumento()
    {

        return $this->hasOne('App\ComprobanteContableDocumento','id_comprobante_contable','id')->oldest();
    }

    public function getReversas()
    {
        return $this->hasMany('App\ReversaComprobante','id_comprobante_contable','id');
    }

    public function getDatosIndex()
    {

        $fechaProceso = fecha_dmY($this->fecha_proceso);

        $botones = '<div class="btn-group">';

        // if ( \Entrust::can('editar-comprobante-contable') ) {
            $botones .= '<a href="'. url("comprobantes/edit/{$this->id}") .'" class="btn btn-warning btn-xs" title="Editar comprobante">';
            $botones .= '<i class="fas fa-edit"></i></a>';
        // }

        // if ( \Entrust::can('eliminar-comprobante-contable') ) {
        if ( $this->carga_sigfe != 1 ) {
            $botones .= '<button type="button" class="btn btn-danger btn-xs" title="Eliminar comprobante" onclick="eliminarComprobante('. $this->id .');">';
            $botones .= '<i class="fas fa-trash"></i></button>';
        }
        // }

        $botones .= '</div>';

        $monto = '$ '.formatoMiles($this->monto_total);

        $docJson = array(
            'DT_RowID' => $this->id,
            $this->getTipoComprobante->nombre,
            $this->folio,
            $this->getNumeroComprobante(6),
            $this->titulo,
            $monto,
            $fechaProceso,
            ($this->reversado) ? 'Si':'No',
            $botones
        );

        return $docJson;

    }

    public function getNumeroComprobante($largo = 6)
    {
        return str_pad($this->numero_comprobante, $largo, '0', STR_PAD_LEFT);
    }

    public function setNumeroComprobante()
    {

        // Buscar el último comprobante que no tenga el numero_comprobante null, segun el año y tipo del comprobante
        $ultimoComprobante = ComprobanteContable::where('numero_comprobante', '!=' ,null)
                                                ->where('id_tipo_comprobante_contable', $this->id_tipo_comprobante_contable)
                                                ->where('año_proceso', $this->año_proceso)
                                                ->orderBy('numero_comprobante', 'DESC')
                                                ->first();

        if ( ! is_object($ultimoComprobante) ) {
            $this->numero_comprobante = 1;
        } else {
            $this->numero_comprobante = $ultimoComprobante->numero_comprobante + 1;
        }

    }
}
