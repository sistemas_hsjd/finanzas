<?php namespace App;

use Illuminate\Database\Eloquent\Model;

class DocumentoContratoOrdenCompra extends Model
{
    protected $table = 'documentos_contratos_ordenes_compra';

    public function getOrdenCompra()
    {
        return $this->belongsTo('App\OrdenCompra','id_orden_compra');
    }

    public function getContrato()
    {
        return $this->belongsTo('App\Contrato','id_contrato');
    }

    public function getDocumento()
    {
        return $this->belongsTo('App\Documento','id_documento');
    }

}
