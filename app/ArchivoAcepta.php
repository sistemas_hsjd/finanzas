<?php namespace App;

use Illuminate\Database\Eloquent\Model;
use Awobaz\Compoships\Compoships;
use App\HermesDocumento;

class ArchivoAcepta extends Model
{
    protected $table = 'archivos_acepta';
    use Compoships;

    public function getTipoDocumento()
    {
        return $this->belongsTo('App\TipoDocumento','id_tipo_documento');
    }

    public function getProveedor()
    {
        return $this->belongsTo('App\Proveedor','id_proveedor');
    }

    public function getAceptaMotivoRechazo()
    {
        return $this->hasMany('App\AceptaMotivoRechazo','id_archivo_acepta','id');
    }

    public function getAceptaMotivoRezagar()
    {
        return $this->hasMany('App\AceptaMotivoRezagar','id_archivo_acepta','id');
    }

    public function getNotaCreditoMenorAlCienPorciento()
    {
        return $this->hasOne('App\ArchivoAcepta','id','id_nota_credito_menor_100_porciento');
    }

    public function getUsuarioResponsable()
    {
        return $this->hasOne('App\User','id', 'id_user_responsable');
    }

    public function getUsuarioRechazo()
    {
        return $this->hasOne('App\User','id', 'id_user_rechazo');
    }

    public function getUsuarioQuitaRechazo()
    {
        return $this->hasOne('App\User','id', 'id_user_quita_rechazo');
    }

    public function getUsuarioRezagado()
    {
        return $this->hasOne('App\User','id', 'id_user_rezagar');
    }

    public function getUsuarioQuitaRezagado()
    {
        return $this->hasOne('App\User','id', 'id_user_quita_rezagar');
    }

    public function getDocumento()
    {
        return $this->hasOne('App\Documento',
                ['numero_documento', 'id_proveedor', 'id_tipo_documento', 'total_documento'], //campos de documentos
                ['folio', 'id_proveedor', 'id_tipo_documento','monto_total']); // campos de archivos_acepta
    }

    /**
     * Trae la factura con la que se realizo la refactura del archivo acepta que se encuentra rechazado
     */
    public function getDocumentoFactura()
    {
        return $this->hasOne('App\Documento','id_archivo_acepta_refacturado', 'id');
    }

    /**
     * Función que retorna un array para opciones de reemplazo de documento.
     */
    public function getDatosDocumentosProveedores()
    {   
        $datosJson = array(
            'numeroDocumento' => $this->folio,
            'totalDocumento'  => formatoMiles($this->monto_total),
        );

        return $datosJson;
    }

    public function getArchivoAceptaRevisionBodega()
    {
        return $this->belongsTo('App\ArchivoAceptaRevisionBodega','id', 'id_archivo_acepta');
    }

    /**
     * Entrega el color para el calendario, según los dias que quedan para rechazar
     */
    public function getColorPorDiasParaPoderRechazar()
    {
        $dias = diferencia_dias($this->publicacion, date('Y-m-d'));
        $diasParaRechazar = 8 - $dias;
        
        $color = '';
        if ($diasParaRechazar == 2 || $diasParaRechazar == 1 ) {
            $color = '#ff8300'; //naranjo
        } elseif ( $diasParaRechazar == 3 || $diasParaRechazar == 4 || $diasParaRechazar == 5 ) {
            $color = '#e2e200f5'; //amarillo
        } elseif ( $diasParaRechazar == 6 || $diasParaRechazar == 7 || $diasParaRechazar == 8 ) {
            $color = 'green';
        } else {
            $color = 'red';
        }
        
        return $color;
    }

    /**
     * Entrega los días que faltan para poder rechazar, utiliza la fecha de publicacion
     */
    public function getDiasRestantestParaRechazar()
    {
        $dias = diferencia_dias($this->publicacion, date('Y-m-d'));

        $diasParaRechazar = 8 - $dias;
        return $diasParaRechazar;
    }

    /**
     * Función que retoruna un array para la grilla
     */
    public function getDatosSinRecepcion( $verFacturaDocumentoAcepta, $cargaDocumentoAceptaAlSistema, $rechazarDocumentoAcepta, $isAdmin = false )
    {

        $tipoDocumento = $this->getNombreTipoDocumento();
                
        $fechaDocumento = $this->emision;
        $totalDocumento = $this->monto_total;

        $botones = '<div class="btn-group">';
        $botones .= $this->getBotones($this->getRutProveedor(), true, $verFacturaDocumentoAcepta, $cargaDocumentoAceptaAlSistema, $rechazarDocumentoAcepta, $isAdmin);

    
        // Rezagar Archivo
        // if ( \Entrust::can('rezagar-documento-acepta') ) {
            $botones .= '<button class="btn btn-default btn-xs" title="Rezagar" onclick="rezagar('. $this->id .')">';
            $botones .= '<i class="fas fa-hourglass-half" style="color: #d9534f;border-color: #d43f3a;"></i></button>';
        // }

        // Boton para pasar a revisión de bodega
        $botones .= '<button class="btn btn-default btn-xs" title="Pasar a revisión de bodega" ';
        $botones .= ' onclick="revisionBodega('. $this->id .');" >';
        $botones .= '<i class="fas fa-question-circle fa-lg"></i></button>';

        $botones .= '</div>';

        $diasParaRechazo = $this->getColumnaDiasRechazo();

        $guias = '';
        if ( $this->getProveedor ) {
            
            $docBodegaDisponibles = \DB::table('ws_documento')
                                        ->join('ws_orden_compra','ws_documento.id_orden_compra','=','ws_orden_compra.id')
                                        ->join('ws_contrato','ws_orden_compra.id_contrato','=','ws_contrato.id')
                                        ->where('ws_contrato.rut_proveedor',$this->getProveedor->rut)
                                        ->where('ws_documento.id_relacion',null)
                                        ->where('ws_documento.tipo_documento','LIKE','Guia de Despacho Electronica')
                                        ->select('ws_documento.id')
                                        ->get();
            
            $guias = 'No';
            if (count($docBodegaDisponibles) > 0) {
                $guias = 'Si';
            }

        }
        
        $ncMenor100 = 'No';
        if ( $this->nota_credito_menor_100_porciento == 1 ) {
            $ncMenor100 = 'Si';
        }

        $user = '';
        if ( $this->getUsuarioResponsable ) {
            $user = $this->getUsuarioResponsable->name;
        }
        

        $docJson = array(
            'DT_RowID' => $this->id,
            // $user,
            $diasParaRechazo,
            $tipoDocumento,
            $this->getRutProveedor().' '.$this->getNombreProveedor(),
            $guias,
            $ncMenor100,
            $this->folio,
            $fechaDocumento,
            $totalDocumento,
            $botones
        );

        return $docJson;
    }

    public function getDatosBuscador($verFacturaDocumentoAcepta)
    {
        $fechaDocumento = $this->emision;
        $totalDocumento = $this->monto_total;

        $botones = '<div class="btn-group">';

        // Ver Archivo
        if ($verFacturaDocumentoAcepta) {
            $botones .= '<a class="btn btn-success btn-xs" href="http://windte1806.acepta.com/ca4webv3/PdfViewMedia?url='. $this->uri .'" target="_blank" title="Ver Archivo">';
            $botones .= '<i class="fas fa-eye fa-lg"></i></a>';

            $botones .= '<button type="button" class="btn btn-primary btn-xs" title="Trazabilidad Acepta" onclick="trazaAcepta('. $this->id .');">';
            $botones .= '<i class="fas fa-map-signs fa-lg"></i></button>';
        }

        $botones .= '</div>';

        $docJson = array(
            'DT_RowID' => $this->id,
            $botones,
            $this->getEstadoAcepta(),
            $this->getProveedor->rut.' '.$this->getProveedor->nombre,
            $this->getTipoDocumento->nombre,
            $this->folio,
            '',
            '',
            $fechaDocumento,
            '',
            $totalDocumento,
            ''
        );

        return $docJson;
    }

    /**
     * Devuelve el estado del archivo acepta
     */
    public function getEstadoAcepta($excel = null)
    {
        $estado = '';

        // if ( $this->conRecepcion() ) {
        //     $estado .= 'Con Recepción.';
        // } else {
        //     $estado .= 'Sin Recepción.';
        // }

        $estado .= ( $this->conRecepcion() ) ? 'Con Recepción.' : 'Sin Recepción.';

        if ( $this->id_user_rechazo ) {
            $estado .= ($excel) ? ' / ': '<br>';
            $estado .= 'Rechazado.';
        }

        if ( $this->id_user_rezagar ) {
            $estado .= ($excel) ? ' / ': '<br>';
            $estado .= 'Rezagado.';
        }

        if ( $this->reclamado ) {
            $estado .= ($excel) ? ' / ': '<br>';
            $estado .= 'Reclamado.';
        }

        if ( $this->getArchivoAceptaRevisionBodega ) {
            $estado .= ($excel) ? ' / ': '<br>';
            $estado .= 'En Revisión.';
        }

        return $estado;
    }

    /**
     * devuelve true si el archivo tiene recepcion
     */
    public function conRecepcion()
    {
        $documentoBodega = WsDocumento::with(['getWsOrdenCompra.getWsContrato'])
                                        ->whereHas('getWsOrdenCompra.getWsContrato',function ($query) {
                                            $query->whereIn('rut_proveedor', $this->getProveedor->getTodosLosRut());
                                        })
                                        ->where('id_relacion', null)
                                        ->where('tipo_documento', 'LIKE', $this->getTipoDocumento->nombre)
                                        ->where('documento', $this->folio)
                                        ->where('documento_total', $this->monto_total)
                                        ->first();

        return ( is_object($documentoBodega) ) ? true : false ;
    }

    /**
     * Trae los datos para la tabla de rezagados
     */
    public function getDatosRezagados()
    {
        
        $proveedor = $this->getRutProveedor().' '.$this->getNombreProveedor();

        $tipoDocumento = $this->getNombreTipoDocumento();

        $fechaDocumento = $this->emision;
        $totalDocumento = '$ '.formatoMiles($this->monto_total);

        $botones = '<div class="btn-group">';
        $botones .= $this->getBotones($this->getRutProveedor());

        // Boton para pasar a revisión de bodega
        $botones .= '<button class="btn btn-default btn-xs" title="Pasar a revisión de bodega" onclick="revisionBodega('. $this->id .');" ';
        $botones .= ' >';
        $botones .= '<i class="fas fa-question-circle fa-lg"></i></button>';

        $botones .= '</div>';

        $diasParaRechazo = $this->getColumnaDiasRechazo();

        $motivosRezagar = '';

        foreach ($this->getAceptaMotivoRezagar as $aceptaMotivoRezagar) {
            $motivosRezagar .= $aceptaMotivoRezagar->getMotivoRezagar->nombre.''.PHP_EOL;
        }

        $docJson = array(
            'DT_RowID' => $this->id,
            $diasParaRechazo,
            $tipoDocumento,
            $proveedor,
            $this->folio,
            $fechaDocumento,
            $totalDocumento,
            $motivosRezagar,
            $this->observacion_rezagar,
            $botones
        );

        return $docJson;
    }

    /**
     * Retorna el rut del proveedor
     */
    public function getRutProveedor()
    {
        return ( $this->getProveedor ) ? $this->getProveedor->rut : $this->emisor ;
    }

    /**
     * Retorna el nombre del proveedor
     */
    public function getNombreProveedor()
    {
        return ( $this->getProveedor ) ? $this->getProveedor->nombre : $this->razon_social_emisor ;
    }

    /**
     * Retorna el nombre del tipo de documento
     */
    public function getNombreTipoDocumento()
    {
        return ( $this->getTipoDocumento ) ? $this->getTipoDocumento->nombre : $this->tipo_documento ;
    }

    /**
     * Retorna botones para los listados.
     * Retorna un string con todos los botones listos.
     */
    public function getBotones($rutProveedor, $auxPermisos = false, $verFacturaDocumentoAcepta = null, $cargaDocumentoAceptaAlSistema = null, $rechazarDocumentoAcepta = null, $isAdmin = false)
    {
        
        $botones = '';
        $style = '';
        
        if ( $this->estado_reclamo_contenido != '' ) {

            $botones .= '<button class="btn btn-danger btn-xs" title="Documento con Reclamo de Contenido: '.$this->estado_reclamo_contenido.' " >';
            $botones .= '<i class="fas fa-exclamation-circle fa-lg"></i></button>';
            $style   .= ' style="background-color: black;border-color: black;" ';

        }

        if ( $this->estado_reclamo_contenido == null && $this->reclamado == 1 ) {

            $botones .= '<button class="btn btn-danger btn-xs" title="Documento Reclamado" >';
            $botones .= '<i class="fas fa-exclamation-circle fa-lg"></i></button>';
            $style   .= ' style="background-color: black;border-color: black;" ';

            if ( $isAdmin ) {
                $botones .= '<button class="btn btn-info btn-xs" title="Quitar Reclamo" onclick="quitarReclamo('.$this->id.')"  >';
                $botones .= '<i class="fas fa-eraser fa-lg"></i></button>';
            }
            
        }

        // Para buscar si el documento tiene recepcion en el sistema pero por un monto distinto.
        $recepcionDisponible = \DB::table('archivos_acepta')
                                  ->join('proveedores','archivos_acepta.id_proveedor','=','proveedores.id')
                                  ->join('tipos_documento','archivos_acepta.id_tipo_documento','tipos_documento.id')
                                  ->join('ws_contrato','proveedores.rut','=','ws_contrato.rut_proveedor')
                                  ->join('ws_orden_compra','ws_contrato.id','=','ws_orden_compra.id_contrato')
                                  ->join('ws_documento','ws_orden_compra.id','=','ws_documento.id_orden_compra')
                                  ->whereColumn('tipos_documento.nombre','LIKE','ws_documento.tipo_documento')
                                  ->whereColumn('archivos_acepta.folio','=','ws_documento.documento')
                                //   ->whereColumn('archivos_acepta.monto_total','!=','ws_documento.documento_total')
                                  ->where('ws_documento.id_relacion',null)
                                  ->where('archivos_acepta.cargado', 0)
                                  ->where('archivos_acepta.id_user_rechazo', null)
                                  ->where('archivos_acepta.folio', $this->folio)
                                  ->first();

        if ( is_object($recepcionDisponible) ) {
            
            $extra = '';

            if ( $this->monto_total > $recepcionDisponible->documento_total ) {
                $dif = $this->monto_total - $recepcionDisponible->documento_total;
                if ( $dif > 100 ) {
                    $extra .= 'Es necesaria una NC.';
                }
            } else {
                $dif = $recepcionDisponible->documento_total - $this->monto_total;
                if ( $dif > 100 ) {
                    $extra .= 'Es necesaria una ND.';
                }
            }           
            
            $botones .= '<button class="btn btn-info btn-xs" title="Documento con recepción disponible. ';

            if ( $dif != 0 ) {
                $botones .= $extra.' Diferencia $ '.formatoMiles($dif).' ';
            }

            $botones .= '" style="background-color: #ff8000;border-color: #ff8000;" >';
            $botones .= '<i class="fas fa-exclamation-circle fa-lg"></i></button>';

        } else {

            // Recepcion en Hermes
        
            // $query = \DB::connection('hermes')
            //             ->table('ACTA_RECEPCION')
            //             ->leftJoin('RESOLUCION', 'ACTA_RECEPCION.ID_RESOLUCION', '=', 'RESOLUCION.ID')
            //             ->leftJoin('PJURIDICA', 'RESOLUCION.ID_PROVEEDOR', '=', 'PJURIDICA.ID')
            //             ->leftJoin('PERSONA', 'PJURIDICA.ID_PERSONA', '=', 'PERSONA.ID')
            //             ->where('ACTA_RECEPCION.CODIGO_DOCUMENTO', '=', $this->folio)
            //             ->where('PERSONA.RUT_PERSONA', '=', explode('-',$rutProveedor)[0])
            //             ->where('PERSONA.DIGITO_VERIFICADOR', '=', explode('-',$rutProveedor)[1])
            //             ->where('ACTA_RECEPCION.TOTAL', '=', $this->monto_total)
            //             ->where('ACTA_RECEPCION.ES_VALIDADO', 1)
            //             ->select(
            //                 'ACTA_RECEPCION.CODIGO_DOCUMENTO',
            //                 'ACTA_RECEPCION.FECHA_INGRESO',
            //                 'ACTA_RECEPCION.NETO',
            //                 'ACTA_RECEPCION.DESCUENTO',
            //                 'ACTA_RECEPCION.TOTAL',
            //                 'ACTA_RECEPCION.CODIGO_PEDIDO',
            //                 'PERSONA.RUT_PERSONA',
            //                 'PERSONA.DIGITO_VERIFICADOR',
            //                 'PJURIDICA.RAZON_SOCIAL'
            //             )
            //             ->first();

            /**
             * Se busca en primer lugar la recepcion con el mismo monto de documento
             */
            $hermesDocumento = HermesDocumento::where('rut_proveedor', $rutProveedor )
                                              ->where('folio_documento', $this->folio )
                                              ->where('documento_total', $this->monto_total )
                                              ->doesnthave('getRelacion')
                                              ->first();


            // $query = null;
            // if ( $query != null ) {
            if ( is_object($hermesDocumento) ) {

                $botones .= '<button class="btn btn-info btn-xs" title="Documento con recepción disponible en Hermes" >';
                $botones .= '<i class="fas fa-exclamation-circle fa-lg"></i></button>';

            } else {

                // $query = \DB::connection('hermes')
                //         ->table('ACTA_RECEPCION')
                //         ->leftJoin('RESOLUCION', 'ACTA_RECEPCION.ID_RESOLUCION', '=', 'RESOLUCION.ID')
                //         ->leftJoin('PJURIDICA', 'RESOLUCION.ID_PROVEEDOR', '=', 'PJURIDICA.ID')
                //         ->leftJoin('PERSONA', 'PJURIDICA.ID_PERSONA', '=', 'PERSONA.ID')
                //         ->where('ACTA_RECEPCION.CODIGO_DOCUMENTO', '=', $this->folio)
                //         ->where('PERSONA.RUT_PERSONA', '=', explode('-',$rutProveedor)[0])
                //         ->where('PERSONA.DIGITO_VERIFICADOR', '=', explode('-',$rutProveedor)[1])
                //         ->where('ACTA_RECEPCION.TOTAL', '!=', $this->monto_total)
                //         ->where('ACTA_RECEPCION.ES_VALIDADO', 1)
                //         ->select(
                //             'ACTA_RECEPCION.CODIGO_DOCUMENTO',
                //             'ACTA_RECEPCION.FECHA_INGRESO',
                //             'ACTA_RECEPCION.NETO',
                //             'ACTA_RECEPCION.DESCUENTO',
                //             'ACTA_RECEPCION.TOTAL',
                //             'PERSONA.RUT_PERSONA',
                //             'PERSONA.DIGITO_VERIFICADOR',
                //             'PJURIDICA.RAZON_SOCIAL'
                //         )
                //         ->first();

                /**
                 * Se busca en segundo lugar la recepcion, con el monto distinto, para indicar si necesita NC o ND
                 */
                $hermesDocumento = HermesDocumento::where('rut_proveedor', $rutProveedor )
                                                  ->where('folio_documento', $this->folio )
                                                  ->where('documento_total', '!=', $this->monto_total )
                                                  ->doesnthave('getRelacion')
                                                  ->first();

                // $query = null;

                // if ( $query != null ) {
                if ( is_object($hermesDocumento) ) {

                    $extra = '';
                    if ( $this->monto_total > $hermesDocumento->documento_total ) {

                        $dif = $this->monto_total - $hermesDocumento->documento_total;
                        if ( $dif > 100 ) {
                            $extra .= 'Es necesaria una NC.';
                        }

                    } else {

                        $dif = $hermesDocumento->documento_total - $this->monto_total;
                        if ( $dif > 100 ) {
                            $extra .= 'Es necesaria una ND.';
                        }
                        
                    }

                    $botones .= '<button class="btn btn-info btn-xs" title="Documento con recepción disponible en Hermes. '.$extra.' Diferencia $ '.formatoMiles($dif).'" >';
                    $botones .= '<i class="fas fa-exclamation-circle fa-lg"></i></button>';

                }
            }

        }

        // Para saber si nota de credito tiene el documento en el sistema.
        if ( $this->id_tipo_documento == 4 ) {
            $numero_documento = null; // guarda el numero del documento para buscarlo en el sistema, en el caso de NC 
            if ($this->referencias != "" && $this->referencias != null) {
                
                $arrayJson = json_decode($this->referencias, true);
                
                if ($arrayJson != null) {
                    
                    foreach ($arrayJson as $itemJson) {
                        
                        if ( ( $itemJson['Tipo'] == 30 || $itemJson['Tipo'] == 32 || $itemJson['Tipo'] == 33 || $itemJson['Tipo'] == 34 ) && $itemJson['Folio'] != 0 ) { // Factura
                            $numero_documento = $itemJson['Folio'];
                        }

                    }

                }

            }

            if ( $numero_documento != null ) {
                $factura = Documento::where('id_proveedor', $this->id_proveedor)
                                    ->whereIn('id_tipo_documento',[1,2,8])
                                    ->where('numero_documento', '=', $numero_documento)
                                    ->select('id')
                                    ->first();

                if ( is_object($factura) ) {
                    $botones .= '<button class="btn btn-success btn-xs" title="NC con factura en sistema" style="background-color: #3a6df1;border-color: #3a6df1;" >';
                    $botones .= '<i class="fas fa-exclamation-circle fa-lg"></i></button>';
                }
            }
        }

        if ( $auxPermisos == false ) {
            // Ver Archivo
            if ( \Entrust::can('ver-factura-documento-acepta') ) {
                $botones .= '<a class="btn btn-success btn-xs" href="http://windte1806.acepta.com/ca4webv3/PdfViewMedia?url='. $this->uri .'" target="_blank" title="Ver Archivo">';
                $botones .= '<i class="fas fa-eye fa-lg"></i></a>';
            }

            // Cargar Archivo
            if ( \Entrust::can('carga-documento-acepta-al-sistema') ) {
                $botones .= '<button class="btn btn-warning btn-xs" title="Cargar Archivo" onclick="cargarArchivo('. $this->id .')" '.$this->style .' >';
                $botones .= '<i class="fas fa-file-export fa-lg"></i>&nbsp;<i class="fas fa-file-invoice fa-lg"></i></button>';
            }

            // Rechazar Archivo
            // if ( $this->getDiasRestantestParaRechazar() > 0 && \Entrust::can('rechazar-documento-acepta') ) {
                if ( \Entrust::can('rechazar-documento-acepta') ) {
                $botones .= '<button class="btn btn-danger btn-xs" title="Rechazar" onclick="rechazar('. $this->id .')" >';
                $botones .= '<i class="fas fa-ban fa-lg"></i></button>';
                }
            // }
        } else {

            // Ver Archivo
            if ( $verFacturaDocumentoAcepta ) {
                $botones .= '<a class="btn btn-success btn-xs" href="http://windte1806.acepta.com/ca4webv3/PdfViewMedia?url='. $this->uri .'" target="_blank" title="Ver Archivo">';
                $botones .= '<i class="fas fa-eye fa-lg"></i></a>';
            }

            // Cargar Archivo
            if ( $cargaDocumentoAceptaAlSistema ) {
                $botones .= '<button class="btn btn-warning btn-xs" title="Cargar Archivo" onclick="cargarArchivo('. $this->id .')" '.$this->style .' >';
                $botones .= '<i class="fas fa-file-export fa-lg"></i>&nbsp;<i class="fas fa-file-invoice fa-lg"></i></button>';
            }

            // Rechazar Archivo
            // if ( $this->getDiasRestantestParaRechazar() > 0 && \Entrust::can('rechazar-documento-acepta') ) {
                if ( $rechazarDocumentoAcepta ) {
                $botones .= '<button class="btn btn-danger btn-xs" title="Rechazar" onclick="rechazar('. $this->id .')" >';
                $botones .= '<i class="fas fa-ban fa-lg"></i></button>';
                }
            // }

        }

        return $botones;

    }

    /**
     * Retorna la columna para los dias de rechazo.
     * retorna un string
     */
    public function getColumnaDiasRechazo()
    {
        $columnaRechazo = $this->getDiasRestantestParaRechazar();
        $columnaRechazo .= '&nbsp;&nbsp;<i class="fas fa-clipboard-check fa-lg" style="color:'. $this->getColorPorDiasParaPoderRechazar() .' !important"></i>';
        return $columnaRechazo;
    }

    /**
     * Trae los datos para la tabla de revision de bodega
     */
    public function getDatosRevisionBodega($esReferente, $esReferenteBodega, $verFacturaDocumentoAcepta, $cargaDocumentoAceptaAlSistema, $rechazarDocumentoAcepta)
    {

        $proveedor = $this->getRutProveedor().' '.$this->getNombreProveedor();

        $tipoDocumento = $this->getNombreTipoDocumento();

        $fechaDocumento = $this->emision;
        $totalDocumento = $this->monto_total;

        $botones = '<div class="btn-group">';
        $botones .= $this->getBotones( $this->getRutProveedor(), true , $verFacturaDocumentoAcepta, $cargaDocumentoAceptaAlSistema, $rechazarDocumentoAcepta);

        // Boton para ver factura y dar respuesta de bodega
        // if ( $this->getArchivoAceptaRevisionBodega->revisado == 0 && \Auth::user()->getReferenteTecnico && \Auth::user()->getReferenteTecnico->id == 13 ) {
            if ( $this->getArchivoAceptaRevisionBodega->revisado == 0 && $esReferente && $esReferenteBodega ) {

            $botones .= '<a class="btn btn-success btn-xs" href="http://windte1806.acepta.com/ca4webv3/PdfViewMedia?url='. $this->uri .'" target="_blank" title="Ver Archivo">';
            $botones .= '<i class="fas fa-eye fa-lg"></i></a>';
            
            $botones .= '<button class="btn btn-success btn-xs" title="Respuesta de bodega" onclick="respuestaBodega('. $this->id .');" ';
            $botones .= ' style="    background-color: blue;border-color: blue;" >';
            $botones .= '<i class="fas fa-question-circle fa-lg"></i></button>';
        }
        

        $botones .= '</div>';

        $diasParaRechazo = $this->getColumnaDiasRechazo();

        $motivosRezagar = '';

        foreach ($this->getAceptaMotivoRezagar as $aceptaMotivoRezagar) {
            $motivosRezagar .= $aceptaMotivoRezagar->getMotivoRezagar->nombre.''.PHP_EOL;
        }

        $respuesta = '';
        if ( $this->getArchivoAceptaRevisionBodega->recepcion_total == 1 ) {
            $respuesta = 'Recepción Total';
        }

        if ( $this->getArchivoAceptaRevisionBodega->recepcion_parcial == 1 ) {
            $respuesta = 'Recepción Parcial';
        }

        if ( $this->getArchivoAceptaRevisionBodega->sin_recepcion == 1 ) {
            $respuesta = 'Sin Recepción';
        }

        $docJson = array(
            'DT_RowID' => $this->id,
            $diasParaRechazo,
            $tipoDocumento,
            $proveedor,
            $this->folio,
            $fechaDocumento,
            $totalDocumento,
            $motivosRezagar,
            $respuesta,
            $this->getArchivoAceptaRevisionBodega->comentario,
            $botones
        );

        return $docJson;
    }

    public function getDatosRechazados($verFacturaDocumentoAcepta, $quitarRechazoDocumentoAcepta)
    {
        
        $proveedor = $this->getRutProveedor().' '.$this->getNombreProveedor();

        $tipoDocumento = $this->getNombreTipoDocumento();

        $fechaDocumento = $this->emision;
        $totalDocumento = $this->monto_total;

        $botones = '<div class="btn-group">';
        // $botones .= $this->getBotones( $this->getRutProveedor() );

        // Ver Archivo
        if ( $verFacturaDocumentoAcepta ) {
            $botones .= '<a class="btn btn-success btn-xs" href="http://windte1806.acepta.com/ca4webv3/PdfViewMedia?url='. $this->uri .'" target="_blank" title="Ver Archivo">';
            $botones .= '<i class="fas fa-eye fa-lg"></i></a>';
        }

        // quitar rechazo
        if ( $quitarRechazoDocumentoAcepta ) {
            $botones .= '<button class="btn btn-warning btn-xs" title="Quitar Rechazo" onclick="quitarRechazo('. $this->id .')">';
            $botones .= '<i class="fas fa-ban"></i></button>';
        }
        $botones .= '</div>';

        $motivosRechazar = '';

        foreach ( $this->getAceptaMotivoRechazo as $aceptaMotivoRechazar ) {
            $motivosRechazar .= $aceptaMotivoRechazar->getMotivoRechazo->nombre.''.PHP_EOL;
        }

        $docJson = array(
            'DT_RowID' => $this->id,
            $tipoDocumento,
            $proveedor,
            $this->folio,
            $fechaDocumento,
            $totalDocumento,
            $motivosRechazar,
            $this->observacion_rechazo,
            $botones
        );

        return $docJson;

    }

    public function getDatosConRecepcion($verPosibleConexionAceptaRecepcion, $verFacturaDocumentoAcepta, $cargaDocumentoAceptaAlSistema, $rechazarDocumentoAcepta, $isAdmin = false)
    {

        $diasParaRechazo = $this->getColumnaDiasRechazo();

        $botones = '<div class="btn-group">';

        $style = '';
        if ( $this->estado_reclamo_contenido != '' ) {

            $botones .= '<button class="btn btn-danger btn-xs" title="Documento con Reclamo de Contenido: '.$this->estado_reclamo_contenido.' " >';
            $botones .= '<i class="fas fa-exclamation-circle fa-lg"></i></button>';
            $style   .= ' style="background-color: black;border-color: black;" ';

        }
        
        if ( $this->estado_reclamo_contenido == null && $this->reclamado == 1 ) {
            $botones .= '<button class="btn btn-danger btn-xs" title="Documento Reclamado" >';
            $botones .= '<i class="fas fa-exclamation-circle fa-lg"></i></button>';
            $style   .= ' style="background-color: black;border-color: black;" ';

            if ( $isAdmin ) {
                $botones .= '<button class="btn btn-info btn-xs" title="Quitar Reclamo" onclick="quitarReclamo('.$this->id_archivo_acepta.')"  >';
                $botones .= '<i class="fas fa-eraser fa-lg"></i></button>';
            }
            
        }

        if ( $verPosibleConexionAceptaRecepcion ) {
            $botones .= '<button class="btn btn-warning btn-xs" title="Posible Conexión Documentos" ';
            $botones .= 'onclick="verPosibleConexionDocumentos('. $this->id_archivo_acepta .','. $this->id_ws_documento .');">';
            $botones .= '<i class="fas fa-code-branch fa-lg fa-rotate-270" style="color:black;"></i></button>';
        }

        if ( $verFacturaDocumentoAcepta ) {
            $botones .= '<a class="btn btn-success btn-xs" href="http://windte1806.acepta.com/ca4webv3/PdfViewMedia?url='. $this->uri .'" target="_blank" title="Ver Archivo">';
            $botones .= '<i class="fas fa-eye fa-lg"></i></a>';
        }

        if ( $cargaDocumentoAceptaAlSistema ) {
            $botones .= '<button class="btn btn-warning btn-xs" title="Cargar Archivo" onclick="cargarArchivo('. $this->id_archivo_acepta .')" '.$style .'>';
            $botones .= '<i class="fas fa-file-export fa-lg"></i>&nbsp;<i class="fas fa-file-invoice fa-lg"></i></button>';
        }

        if ( getDiasRestantestParaRechazar($this->publicacion) > -1 && $rechazarDocumentoAcepta ) {
            $botones .= '';
        }

        $botones .= '</div>';

        $docJson = array(
            'DT_RowID' => $this->id_archivo_acepta,
            $diasParaRechazo,
            $this->rut_proveedor,
            $this->nombre_proveedor,
            $this->nombre_tipo_documento,
            $this->folio,
            $this->ws_tipo_documento,
            $this->ws_numero_documento,
            $botones
        );

        return $docJson;
    }

    public function getReferencias()
    {
        $arrayJson = json_decode($this->referencias,true );
        $referencias = '';
        if ( $arrayJson != null ) {
            foreach ($arrayJson as $itemJson) {
                foreach ($itemJson as $key => $item) {
                    // dd($arrayJson, $itemJson, $key, $item);
                    $referencias .= $key.': '.$item.'<br>';
                }
            }
        }

        return $referencias;
    }

    /**
     * Función para las notas de credito.
     */
    public function rechazarFactura()
    {
        if ($this->referencias != "" && $this->referencias != null) {

            $folioFactura = null;
            $arrayJson = json_decode($this->referencias, true);
            if ($arrayJson != null) {

                foreach ($arrayJson as $itemJson) {

                    if ( $itemJson['Tipo'] == 30 || $itemJson['Tipo'] == 32 || $itemJson['Tipo'] == 33 || $itemJson['Tipo'] == 34 ) { // Factura

                        $folioFactura = $itemJson['Folio'];

                        // Buscar la Factura
                        $facturaAcepta = ArchivoAcepta::where('cargado', 0)
                                                      ->where('id_proveedor', $this->id_proveedor)
                                                      ->whereIn('id_tipo_documento', [1,2,8]) // Factura y Re-factura
                                                      ->where('folio', $folioFactura)
                                                      ->where('id_user_rechazo', null)
                                                      ->where('monto_total', $this->monto_total)
                                                      ->select('id','id_proveedor','id_tipo_documento','folio','monto_total','emision')
                                                      ->first();

                        if ( is_object($facturaAcepta) ) {

                            // Rechazar Factura
                            $facturaAcepta->observacion_rechazo = 'Documento rechazado automáticamente por el sistema.';
                            $facturaAcepta->observacion_rechazo .= PHP_EOL.'Tiene una Nota de Crédito por el total del monto.';
                            $facturaAcepta->observacion_rechazo .= PHP_EOL.'Folio de la Nota de Crédito : '.$this->folio;
                            $facturaAcepta->id_user_rechazo = 85;
                            $facturaAcepta->save();

                            $auxMovitoRechazo = AceptaMotivoRechazo::where('id_archivo_acepta', $facturaAcepta->id)
                                                                   ->where('id_motivo_rechazo', 5)
                                                                   ->first();
                            if ( ! is_object($auxMovitoRechazo) ) {

                                $aceptaMotivo = new AceptaMotivoRechazo();
                                $aceptaMotivo->id_archivo_acepta = $facturaAcepta->id;
                                $aceptaMotivo->id_motivo_rechazo = 5;
                                $aceptaMotivo->save();

                            }
                            

                            // Rechazar Nota de Credito
                            $this->observacion_rechazo = 'Documento rechazado automáticamente por el sistema.';
                            $this->observacion_rechazo .= PHP_EOL.'Anula una Factura, la Nota de Crédito es por el total del monto.';
                            $this->observacion_rechazo .= PHP_EOL.'Folio de la Factura : '.$facturaAcepta->folio;
                            $this->id_user_rechazo = 85;
                            $this->save();

                            $auxMovitoRechazo = AceptaMotivoRechazo::where('id_archivo_acepta', $this->id)
                                                                   ->where('id_motivo_rechazo', 5)
                                                                   ->first();

                            if ( ! is_object($auxMovitoRechazo) ) {
                                $aceptaMotivo = new AceptaMotivoRechazo();
                                $aceptaMotivo->id_archivo_acepta = $this->id;
                                $aceptaMotivo->id_motivo_rechazo = 5;
                                $aceptaMotivo->save();
                            }

                            break;

                        } else {

                            /**
                             * Para rechazar la nota de credito en caso de que la factura esté rechazada
                             */

                            $facturaAcepta = ArchivoAcepta::where('cargado', 0)
                                                          ->where('id_proveedor', $this->id_proveedor)
                                                          ->whereIn('id_tipo_documento', [1,2,8]) // Factura y Re-factura
                                                          ->where('folio', $folioFactura)
                                                          ->where('id_user_rechazo','<>' ,null)
                                                          ->select('id','id_proveedor','id_tipo_documento','folio','monto_total','emision')
                                                          ->first();

                            if ( is_object($facturaAcepta) ) {

                                // Rechazar Factura
                                $facturaAcepta->observacion_rechazo = 'Documento rechazado automáticamente por el sistema.';
                                $facturaAcepta->observacion_rechazo .= PHP_EOL.'Tiene una Nota de Crédito por el total del monto.';
                                $facturaAcepta->observacion_rechazo .= PHP_EOL.'Folio de la Nota de Crédito : '.$this->folio;
                                // $facturaAcepta->id_user_rechazo = 1;
                                $facturaAcepta->save();

                                $auxMovitoRechazo = AceptaMotivoRechazo::where('id_archivo_acepta', $facturaAcepta->id)
                                                                       ->where('id_motivo_rechazo', 5)
                                                                       ->first();
                                if ( ! is_object($auxMovitoRechazo) ) {

                                    $aceptaMotivo = new AceptaMotivoRechazo();
                                    $aceptaMotivo->id_archivo_acepta = $facturaAcepta->id;
                                    $aceptaMotivo->id_motivo_rechazo = 5;
                                    $aceptaMotivo->save();

                                }

                                // Rechazar Nota de Credito
                                $this->observacion_rechazo = 'Documento rechazado automáticamente por el sistema.';
                                $this->observacion_rechazo .= PHP_EOL.'Aplica a una Factura que ya esta rechazada.';
                                $this->observacion_rechazo .= PHP_EOL.'Folio de la Factura : '.$facturaAcepta->folio;
                                $this->id_user_rechazo = 85;
                                $this->save();

                                $auxMovitoRechazo = AceptaMotivoRechazo::where('id_archivo_acepta', $this->id)
                                                                       ->where('id_motivo_rechazo', 5)
                                                                       ->first();

                                if ( ! is_object($auxMovitoRechazo) ) {
                                    
                                    $aceptaMotivo = new AceptaMotivoRechazo();
                                    $aceptaMotivo->id_archivo_acepta = $this->id;
                                    $aceptaMotivo->id_motivo_rechazo = 5;
                                    $aceptaMotivo->save();

                                }

                                break;

                            }
                            
                        }
                          
                    }

                }
            }

        }
    }
}
