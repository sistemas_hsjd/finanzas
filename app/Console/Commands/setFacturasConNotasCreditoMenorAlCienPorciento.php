<?php namespace App\Console\Commands;

use Illuminate\Console\Command;
use App\ArchivoAcepta;

class setFacturasConNotasCreditoMenorAlCienPorciento extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'set-facturas-con-notas-credito-menor-al-cien-porciento';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Busca las facturas que tienen notas de credito
                              pero por un monto total menor al 100% de la factura.
                              se cambia el valor de dos columnas, para mostrar dicha informacion
                              en el listado de documentos sin recepcion';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {

        set_time_limit(0);
        ini_set('max_execution_time', 0);
        
        // Para dar aviso de las facturas que tienen nota de credito no por el 100%
        $notasCreditoAcepta = ArchivoAcepta::where('cargado', 0)
                                           ->where('id_tipo_documento', 4)
                                           ->where('referencias','<>', null)
                                           ->where('id_user_rechazo', null)
                                           ->select('id','referencias','monto_total','id_proveedor','folio')
                                           ->orderBy('created_at','desc')
                                           ->get();

        $contador = 0;

        foreach ( $notasCreditoAcepta as $ncA ) {
            $numero_documento = null;

            if ($ncA->referencias != "" && $ncA->referencias != null) {

                $arrayJson = json_decode($ncA->referencias, true);
                if ($arrayJson != null) {
    
                    foreach ($arrayJson as $itemJson) {
    
                        if ( $itemJson['Tipo'] == 30 || $itemJson['Tipo'] == 32 || $itemJson['Tipo'] == 33 || $itemJson['Tipo'] == 34 ) { // Factura

                            $numero_documento = $itemJson['Folio'];

                            // Buscar la Factura
                            $facturaAcepta = ArchivoAcepta::where('cargado', 0)
                                                          ->where('id_proveedor', $ncA->id_proveedor)
                                                          ->whereIn('id_tipo_documento', [1,2,8]) // Factura y Re-factura
                                                          ->where('folio', $numero_documento)
                                                          ->where('id_user_rechazo', null)
                                                          ->where('monto_total','>', $ncA->monto_total)
                                                          ->where('nota_credito_menor_100_porciento', 0)
                                                          ->select('id','id_proveedor','id_tipo_documento','folio','monto_total','emision')
                                                          ->first();

                            if ( is_object($facturaAcepta) ) {
                                
                                // Factura tiene nc asociada y con un monto menor al 100% de su valor
                                $facturaAcepta->nota_credito_menor_100_porciento = 1;
                                $facturaAcepta->id_nota_credito_menor_100_porciento = $ncA->id;
                                $facturaAcepta->save();
                                $contador++;
                                break;

                            }
                                
                        }
    
                    }
                }
    
            }

        }

        \Log::info("Cantidad de Archivos Acepta (Facturas) con NC menor al 100% : {$contador}");

        $this->info('set-facturas-con-notas-credito-menor-al-cien-porciento ejecutado correctamente, Facturas con NC menor al 100% : '. $contador);
    }
}
