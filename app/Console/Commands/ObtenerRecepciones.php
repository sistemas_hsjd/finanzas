<?php namespace App\Console\Commands;

use Illuminate\Console\Command;
use GuzzleHttp\Client;
use GuzzleHttp\Exception\RequestException;

use App\WsContrato;
use App\WsOrdenCompra;
use App\WsDocumento;
use App\WsArchivo;
use App\WsItemPresupuestario;

class ObtenerRecepciones extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'obtener-recepciones';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Se traen las recepciones disponibles en SIFCON server';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        set_time_limit(0);
        ini_set('max_execution_time', 0);

        \Log::info("OBTENER RECEPCIONES | Inicio");

        $this->info("OBTENER RECEPCIONES | Inicio");

        // $obtener = true;
        // while ( $obtener != false ) {
        //     sleep(3);
        //     try {

        //         $client = new Client();
        //         $response = $client->request(
        //             'GET',
        //             'http://3.90.166.123/api/recepciones'
        //         );

        //         if ( $response->getStatusCode() === 200 ) {

        //             $statusCode = $response->getStatusCode();
        //             $contentResponse = $response->getBody()->getContents();

        //             $jsonResponse = json_decode($contentResponse);
                    
        //             if ( $jsonResponse != null && $jsonResponse->enviar ) {

        //                 $wsDocumento = WsDocumento::with(['getWsOrdenCompra.getWsContrato','getWsArchivos','getWsItemsPresupuestarios'])
        //                                           ->where('tipo_documento', $jsonResponse->{'documento'}->tipo_documento)
        //                                           ->where('documento', $jsonResponse->{'documento'}->documento)
        //                                           ->where('documento_total', $jsonResponse->{'documento'}->documento_total)
        //                                           ->whereHas('getWsOrdenCompra.getWsContrato',function ($query) use ($jsonResponse) {
        //                                                 $query->where('rut_proveedor', $jsonResponse->{'contrato'}->rut_proveedor);
        //                                             })
        //                                           ->first();

                        
        //                 if ( ! is_object($wsDocumento) ) {
                            
        //                     if ( $jsonResponse->{'contrato'}->nombre_proveedor == null || $jsonResponse->{'contrato'}->rut_proveedor == null ) {
        //                         $this->info("OBTENER RECEPCIONES | Fallo la info del contrato");
        //                         continue;
        //                     }
                            
        //                     if ( $jsonResponse->{'orden_compra'}->fecha_orden_compra == null ||
        //                          $jsonResponse->{'orden_compra'}->fecha_recepcion_orden_compra == null ||
        //                          $jsonResponse->{'orden_compra'}->id_orden_compra == null ||
        //                          $jsonResponse->{'orden_compra'}->numero_orden_compra == null ||
        //                          $jsonResponse->{'orden_compra'}->total_orden_compra == null ||
        //                          $jsonResponse->{'orden_compra'}->tipo_compra == null
        //                        ) {
        //                         $this->info("OBTENER RECEPCIONES | Fallo la info de la orden de compra");
        //                         continue;
        //                     }

        //                     if ( $jsonResponse->{'documento'}->documento == null ||
        //                          $jsonResponse->{'documento'}->tipo_documento == null ||
        //                          $jsonResponse->{'documento'}->nombre_usuario_responsable == null ||
        //                          $jsonResponse->{'documento'}->documento_descuento_total === null ||
        //                          $jsonResponse->{'documento'}->documento_neto === null ||
        //                          $jsonResponse->{'documento'}->documento_iva === null ||
        //                          $jsonResponse->{'documento'}->documento_total === null
        //                        ) {
        //                         $this->info("OBTENER RECEPCIONES | Fallo la info del documento");
        //                         continue;
        //                     }

        //                     $newContrato = new WsContrato( $jsonResponse->{'contrato'} );

        //                     $newOrdenCompra = new WsOrdenCompra( $jsonResponse->{'orden_compra'}, $newContrato->id);

        //                     $newDocumento = new WsDocumento( $jsonResponse->{'documento'}, $newOrdenCompra->id);

        //                     foreach ( $jsonResponse->{'items_presupuestarios'} as $item ) {
        //                         $newItem = new WsItemPresupuestario($item ,$newDocumento->id);
        //                     }

        //                     foreach ( $jsonResponse->{'archivos'} as $archivo) {
        //                         $newArchivo = new WsArchivo($archivo, $newDocumento->id, $newOrdenCompra->id);
        //                     }

        //                     $this->info("OBTENER RECEPCIONES | Se creo el wsDocumento, id: ".$newDocumento->id." | ".$newDocumento->documento." | ".$newDocumento->tipo_documento);

        //                 } else {
        //                     $this->info("OBTENER RECEPCIONES | Esta el wsDocumento, id: ".$wsDocumento->id." | ".$wsDocumento->documento." | ".$wsDocumento->tipo_documento);
        //                     $wsDocumento->codigo_pedido = $jsonResponse->{'documento'}->codigo_pedido;
        //                     $wsDocumento->save();
        //                     $this->info("OBTENER RECEPCIONES | Se actualiza codifo pedido: ".$jsonResponse->{'documento'}->codigo_pedido);
        //                 }

        //             } else {

        //                 $obtener = $jsonResponse == null || $jsonResponse->cantidad_recepciones < 2 ? false : true;
        //                 $this->info("OBTENER RECEPCIONES | Respuesta nula");

        //             }
                    
        //         } else {
        //             $obtener = false;
        //             $this->info("OBTENER RECEPCIONES | Respuesta distinta a 200");
        //         }

        //     } catch (RequestException $e) {
        //         $this->info("OBTENER RECEPCIONES | Exception obteniendo la recepcion : ".$e->getMessage());
        //     }
        // }

        // $recepcionesSifcon = WsDocumento::all();
        $recepcionesSifcon = WsDocumento::where('codigo_pedido', '!=', null)->get();
        

        $recepcionesHermes = \DB::connection('hermes')
                                ->table('ACTA_RECEPCION')
                                ->leftJoin('RESOLUCION', 'ACTA_RECEPCION.ID_RESOLUCION', '=', 'RESOLUCION.ID')
                                ->leftJoin('PJURIDICA', 'RESOLUCION.ID_PROVEEDOR', '=', 'PJURIDICA.ID')
                                ->leftJoin('PERSONA', 'PJURIDICA.ID_PERSONA', '=', 'PERSONA.ID')
                                ->leftJoin('TIPO_DOCUMENTO', 'ACTA_RECEPCION.ID_TIPO_DOCUMENTO', '=', 'TIPO_DOCUMENTO.ID')

                                // no considerar las recepciones en el sistema
                                // ->whereNotIn('ACTA_RECEPCION.CODIGO_DOCUMENTO', $recepcionesSifcon->pluck('documento')->toArray() )
                                ->whereNotIn('ACTA_RECEPCION.CODIGO_PEDIDO', $recepcionesSifcon->pluck('codigo_pedido')->toArray() )
                                ->where('ACTA_RECEPCION.ES_VALIDADO', 1)
                                ->whereYear( 'ACTA_RECEPCION.FECHA_INGRESO','>=', 2019 )

                                ->select(
                                    'ACTA_RECEPCION.*',
                                    'ACTA_RECEPCION.CODIGO_DOCUMENTO as FOLIO_DOCUMENTO',
                                    'PERSONA.RUT_PERSONA',
                                    'PERSONA.DIGITO_VERIFICADOR',
                                    'PJURIDICA.RAZON_SOCIAL',
                                    'TIPO_DOCUMENTO.NOMBRE as NOMBRE_DOCUMENTO',
                                    'TIPO_DOCUMENTO.CODIGO_SII as CODIGO_SII_DOCUMENTO'
                                )
                                ->get();

        \Log::info("OBTENER RECEPCIONES | Recepciones encontradas:  {$recepcionesHermes->count()}");

        $this->info("OBTENER RECEPCIONES | Recepciones encontradas:  {$recepcionesHermes->count()}");

        foreach ( $recepcionesHermes as $recepcionHermes ) {
            
            $this->info("OBTENER RECEPCIONES | Se encuentra la recepcion en hermes:  {$recepcionHermes->CODIGO_PEDIDO}");
            try {

                $client = new Client();
                $response = $client->request(
                    'GET',
                    'http://hsjd.logisticapp.cl/logisticaPublica/integracionFinanzas/integracionFinanzas.php?codigoRecepcion='.$recepcionHermes->CODIGO_PEDIDO.'&token=test_v1'
                );

                if ( $response->getStatusCode() === 200 ) {

                    $contentResponse = $response->getBody()->getContents();
                    $jsonResponse = json_decode($contentResponse);
                    
                    if ( $jsonResponse != null ) {

                        $wsDocumento = WsDocumento::with(['getWsOrdenCompra.getWsContrato','getWsArchivos','getWsItemsPresupuestarios'])
                                            ->where('tipo_documento', $jsonResponse->{'documento'}->tipo_documento)
                                            ->where('documento', $jsonResponse->{'documento'}->documento)
                                            ->where('documento_total', $jsonResponse->{'documento'}->documento_total)
                                            ->whereHas('getWsOrdenCompra.getWsContrato',function ($query) use ($jsonResponse) {
                                                    $query->where('rut_proveedor', $jsonResponse->{'contrato'}->rut_proveedor);
                                                })
                                            ->first();

                        
                        if ( ! is_object($wsDocumento) ) {
                            
                            if ( $jsonResponse->{'contrato'}->nombre_proveedor == null || $jsonResponse->{'contrato'}->rut_proveedor == null ) {
                                $this->info("OBTENER RECEPCIONES | Fallo la info del contrato");
                                continue;
                            }
                            
                            if ( $jsonResponse->{'orden_compra'}->fecha_orden_compra == null ||
                                 $jsonResponse->{'orden_compra'}->fecha_recepcion_orden_compra == null ||
                                 $jsonResponse->{'orden_compra'}->id_orden_compra == null ||
                                 $jsonResponse->{'orden_compra'}->numero_orden_compra == null ||
                                 $jsonResponse->{'orden_compra'}->total_orden_compra == null ||
                                 $jsonResponse->{'orden_compra'}->tipo_compra == null
                               ) {
                                $this->info("OBTENER RECEPCIONES | Fallo la info de la orden de compra fecha orden ". $jsonResponse->{'orden_compra'}->fecha_orden_compra);
                                $this->info("OBTENER RECEPCIONES | Fallo la info de la orden de compra fecha recepcion ". $jsonResponse->{'orden_compra'}->fecha_recepcion_orden_compra);
                                $this->info("OBTENER RECEPCIONES | Fallo la info de la orden de compra id orden ". $jsonResponse->{'orden_compra'}->id_orden_compra);
                                $this->info("OBTENER RECEPCIONES | Fallo la info de la orden de compra numero ". $jsonResponse->{'orden_compra'}->numero_orden_compra);
                                $this->info("OBTENER RECEPCIONES | Fallo la info de la orden de compra total ". $jsonResponse->{'orden_compra'}->total_orden_compra);
                                $this->info("OBTENER RECEPCIONES | Fallo la info de la orden de compra tipo ". $jsonResponse->{'orden_compra'}->tipo_compra);
                                continue;
                            }

                            if ( $jsonResponse->{'documento'}->documento == null ||
                                 $jsonResponse->{'documento'}->tipo_documento == null ||
                                 $jsonResponse->{'documento'}->nombre_usuario_responsable == null ||
                                 $jsonResponse->{'documento'}->documento_descuento_total === null ||
                                 $jsonResponse->{'documento'}->documento_neto === null ||
                                 $jsonResponse->{'documento'}->documento_iva === null ||
                                 $jsonResponse->{'documento'}->documento_total === null
                               ) {
                                $this->info("OBTENER RECEPCIONES | Fallo la info del documento documento : ". $jsonResponse->{'documento'}->documento);
                                $this->info("OBTENER RECEPCIONES | Fallo la info del documento tipo_documento : ". $jsonResponse->{'documento'}->tipo_documento);
                                $this->info("OBTENER RECEPCIONES | Fallo la info del documento nombre_usuario_responsable : ". $jsonResponse->{'documento'}->nombre_usuario_responsable);
                                $this->info("OBTENER RECEPCIONES | Fallo la info del documento documento_descuento_total : ". $jsonResponse->{'documento'}->documento_descuento_total);
                                $this->info("OBTENER RECEPCIONES | Fallo la info del documento documento_neto : ". $jsonResponse->{'documento'}->documento_neto);
                                $this->info("OBTENER RECEPCIONES | Fallo la info del documento documento_iva : ". $jsonResponse->{'documento'}->documento_iva);
                                $this->info("OBTENER RECEPCIONES | Fallo la info del documento documento_total : ". $jsonResponse->{'documento'}->documento_total);
                                // $this->info("OBTENER RECEPCIONES | Fallo la info del documento");
                                continue;
                            }

                            $newContrato = new WsContrato( $jsonResponse->{'contrato'} );

                            $newOrdenCompra = new WsOrdenCompra( $jsonResponse->{'orden_compra'}, $newContrato->id);

                            $newDocumento = new WsDocumento( $jsonResponse->{'documento'}, $newOrdenCompra->id, $recepcionHermes->CODIGO_PEDIDO);

                            foreach ( $jsonResponse->{'items_presupuestarios'} as $item ) {
                                $newItem = new WsItemPresupuestario($item ,$newDocumento->id);
                            }

                            foreach ( $jsonResponse->{'archivos'} as $archivo) {
                                $newArchivo = new WsArchivo($archivo, $newDocumento->id, $newOrdenCompra->id);
                            }

                            $this->info("OBTENER RECEPCIONES | Se creo el newDocumento, id: ".$newDocumento->id." | 
                                        ".$newDocumento->documento." | ".$newDocumento->tipo_documento);

                        } else {

                            $this->info("OBTENER RECEPCIONES | Esta el wsDocumento, id: ".$wsDocumento->id." | 
                                        ".$wsDocumento->documento." | ".$wsDocumento->tipo_documento);
                            if ( $wsDocumento->codigo_pedido == null ) {
                                $wsDocumento->codigo_pedido = $recepcionHermes->CODIGO_PEDIDO;
                                $wsDocumento->save();
                                $this->info("OBTENER RECEPCIONES | Se actualiza codigo pedido {$recepcionHermes->CODIGO_PEDIDO}");
                            } else {
                                $this->info("OBTENER RECEPCIONES | NO se actualiza codigo pedido {$recepcionHermes->CODIGO_PEDIDO}");
                            }
                            
                        }

                    } else {
                        $this->info("OBTENER RECEPCIONES | Respuesta null, codigo pedido:  {$recepcionHermes->CODIGO_PEDIDO}");
                    }
                    
                } else {
                    $this->info("OBTENER RECEPCIONES | Codigo de respuesta: ".$response->getStatusCode() );
                }

            } catch (RequestException $e) {
                $this->info("OBTENER RECEPCIONES | Problema al consumier webservicios de recepción");
            }
                
        }

    }
}
