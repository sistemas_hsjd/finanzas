<?php namespace App\Console\Commands;

use Illuminate\Console\Command;
use App\DatoGeneral;

class ArchivoAceptaConRecepcion extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'archivos-acepta-con-recepcion';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Consulta el numero de archivos acepta que tienen recepcion disponible';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        
        set_time_limit(0);
        ini_set('max_execution_time', 0);

        $archivosAceptaConRecepcion = \DB::table('archivos_acepta')
                                          ->join('proveedores','archivos_acepta.id_proveedor','=','proveedores.id')
                                          ->join('tipos_documento','archivos_acepta.id_tipo_documento','tipos_documento.id')
                                          ->join('ws_contrato','proveedores.rut','=','ws_contrato.rut_proveedor')
                                          ->join('ws_orden_compra','ws_contrato.id','=','ws_orden_compra.id_contrato')
                                          ->join('ws_documento','ws_orden_compra.id','=','ws_documento.id_orden_compra')

                                          ->whereColumn('tipos_documento.nombre','LIKE','ws_documento.tipo_documento')
                                          ->whereColumn('archivos_acepta.folio','=','ws_documento.documento')
                                          ->whereColumn('archivos_acepta.monto_total','=','ws_documento.documento_total')
                                          // ->where('archivos_acepta.id_relacion',null)
                                          ->where('ws_documento.id_relacion', null)
                                          ->where('archivos_acepta.cargado', 0)
                                          ->where('archivos_acepta.id_user_rechazo', null)

                                          ->select(
                                              'archivos_acepta.id as id'
                                          )

                                          ->count();

        $datoGeneral = DatoGeneral::find(1);

        if ( ! is_object($datoGeneral) ) {
            $datoGeneral = new DatoGeneral;
        }

        $datoGeneral->archivos_acepta_con_recepcion = $archivosAceptaConRecepcion;
        $datoGeneral->save();

        \Log::info("Cantidad de Archivos Acepta con Recepcion : {$datoGeneral->archivos_acepta_con_recepcion}");

        $this->info('archivos-acepta-con-recepcion ejecutado correctamente');
    }
}
