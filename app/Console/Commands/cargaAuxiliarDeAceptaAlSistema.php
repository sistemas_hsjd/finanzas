<?php namespace App\Console\Commands;

use Illuminate\Console\Command;
use App\ArchivoAcepta;
use App\Documento;
use App\Archivo;

class cargaAuxiliarDeAceptaAlSistema extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'carga-auxiliar-de-acepta-al-sistema';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Busca documentos en el sistema en base a los documentos de acepta.
                              Si encuentra en el sistema guarda los archivos de acepta en el documento del sistema
                              y deja el "documento acepta" como cargado para quitarlo de la grilla';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {

        set_time_limit(0);
        ini_set('max_execution_time', 0);

        $archivosAcepta = ArchivoAcepta::with(['getProveedor','getTipoDocumento'])
                                       ->where('id_proveedor', '<>', null)
                                       ->where('id_tipo_documento', '<>', null)
                                       ->orderBy('created_at', 'desc')
                                    //    ->where('cargado',0)
                                       // ->whereMonth('created_at', date('m'))
                                       ->where('created_at', '>=', '2019-11-01 00:00:00')
                                       ->get();
        $contador = 0;
        $this->info('Obtiene los archivos acepta: '. $archivosAcepta->count());
        \Log::info("Archivos Acepta (PDF), para actualizar archivo del documento  : {$archivosAcepta->count()}");

        foreach ($archivosAcepta as $key => $archivoAcepta) {

            $documento = Documento::where('id_proveedor', $archivoAcepta->getProveedor->id)
                                  ->where('numero_documento', trim($archivoAcepta->folio) ) 
                                  ->where('id_tipo_documento', $archivoAcepta->getTipoDocumento->id)
                                  //->select('id_tipo_documento','deleted_at','numero_documento','id_proveedor','fecha_ingreso')
                                  ->first();

            if ( is_object($documento) ) {

                $archivoFacturaDelDocumento = Archivo::where('id_documento', $documento->id)
                                                     ->where('id_tipo_archivo', 1)
                                                     ->first();
                
                if ( !is_object($archivoFacturaDelDocumento) ) {

                    // Se guarda la Factura (pdf) en el sistema, para el documento
                    $newArchivo = new Archivo();
                    $newArchivo->id_documento = $documento->id;
                    $newArchivo->id_tipo_archivo = 1;
                    $newArchivo->nombre = 'archivoAcepta_'.$archivoAcepta->id.'_'.date('Y_m_d_G_i_s');
                    $newArchivo->nombre_original = $newArchivo->nombre;
                    $newArchivo->ubicacion = "documento/".$documento->id."/".$newArchivo->nombre.".pdf";
                    $newArchivo->extension = "pdf";
                    $newArchivo->peso = 0;
                    $newArchivo->save();

                    $content = file_get_contents('http://windte1806.acepta.com/ca4webv3/PdfViewMedia?url='.$archivoAcepta->uri.'&menuTitle=Papel%2520Carta');
                    if ( !file_exists(public_path().'/documento/'.$documento->id) ) {
                        mkdir(public_path().'/documento/'.$documento->id, 0775, true);
                        chmod(public_path().'/documento/'.$documento->id, 0775);
                    }
                    $file = fopen( public_path().'/documento/'.$documento->id.'/'.$newArchivo->nombre.'.pdf', 'a');
                    fwrite($file, $content);
                    fclose($file);

                    $contador++;

                }

                // $archivoAutorizacionSiiDelDocumento = Archivo::where('id_documento',$documento->id)
                //                            ->where('id_tipo_archivo', 15)
                //                            ->first();

                // if ( !is_object($archivoAutorizacionSiiDelDocumento) ) {
                //     //Autorizacion SII
                //     $newArchivo = new Archivo();
                //     $newArchivo->id_documento = $documento->id;
                //     $newArchivo->id_tipo_archivo = 15;
                //     $newArchivo->nombre = 'archivoAcepta_'.$archivoAcepta->id.'_'.date('Y_m_d_G_i_s').'_sii';
                //     $newArchivo->nombre_original = $newArchivo->nombre;
                //     $newArchivo->ubicacion = "documento/".$documento->id."/".$newArchivo->nombre.".pdf";
                //     $newArchivo->extension = "pdf";
                //     $newArchivo->peso = 0;
                //     $newArchivo->save();

                //     // Se obtiene la información desde SII y se genera un pdf para guardarlo en el sistema
                //     $gl_url = 'https://palena.sii.cl/cgi_dte/UPL/QEstadoDTE?rutQuery=61608204&dvQuery=3&rutCompany='.str_replace(".", "", trim( explode("-",$archivoAcepta->emisor)[0] ) ).'&dvCompany='.explode("-", $archivoAcepta->emisor )[1].'&rutReceiver=61608204&dvReceiver=3&tipoDTE='.$archivoAcepta->tipo.'&folioDTE='.$archivoAcepta->folio.'&fechaDTE='.fecha_dmY_sinSeparador($archivoAcepta->emision).'&montoDTE='.$archivoAcepta->monto_total;
                //     $content = file_get_contents($gl_url);
                //     //str_replace(public_path().'VisBue.gif',"https://palena.sii.cl/dte/UPL/VisBue.gif",$content);
                //     str_replace('',"https://palena.sii.cl/dte/UPL/VisBue.gif",$content);
                    
                //     // Quita espacios
                    
                //     $buscar = array('/\>[^\S ]+/s','/[^\S ]+\</s','/(\s)+/s');
                //     $reemplazar = array('>','<','\\1');
                //     $content = preg_replace($buscar, $reemplazar, $content);
                //     $content = str_replace("> <", "><", $content);
                
                //     // Arregla etiquetas
                    
                //     $content = str_replace('</table></td></tr><tr><td><font face="Arial" size="2"><strong>Documento consultado:</strong></font>','</table><table><tr><td><font face="Arial" size="2"><strong>Documento consultado:</strong></font>',$content);
                //     $content = utf8_encode($content);
                //     $content = mb_convert_encoding($content, 'UTF-8', 'UTF-8');

                //     $mpdf = new Mpdf(['mode' => 'c','tempDir' => public_path() . '/tempMpdf']);
                //     $mpdf->SetDisplayMode('fullpage');
                //     $mpdf->keep_table_proportions = true;
                //     $mpdf->WriteHTML($content);
                //     $mpdf->Output(public_path().'/documento/'.$documento->id.'/'.$newArchivo->nombre.'.pdf','F');
                // }

                $archivoAcepta->cargado = 1;
                $archivoAcepta->save();

            } 
        }

        \Log::info("Archivos Acepta (PDF) guardados en el sistema : {$contador}");

        $this->info('carga-auxiliar-de-acepta-al-sistema ejecutado correctamente, pdfs: '. $contador);
    }
}
