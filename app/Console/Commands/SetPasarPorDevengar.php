<?php namespace App\Console\Commands;

use Illuminate\Console\Command;
use App\Documento;

class SetPasarPorDevengar extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'set-pasar-por-devengar';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Pasa documentos a por devengar';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        // No se ocupa, todos los documentos deben ser devengados, excepto los reclamados
        // set_time_limit(0);
        // ini_set('max_execution_time', 0);

        // $documentos = Documento::doesntHave('getFoliosSigfe')
        //                        ->where('fecha_ingreso', '>=', '2019-06-22 00:00:00')
        //                        ->where('id_relacion', null)
        //                        ->where('directo_por_devengar', null)
        //                        ->get();

        // foreach ( $documentos as $key => $doc ) {

        //     $recepcionHermes = \DB::connection('hermes')
        //                         ->table('ACTA_RECEPCION')
        //                         ->leftJoin('RESOLUCION', 'ACTA_RECEPCION.ID_RESOLUCION', '=', 'RESOLUCION.ID')
        //                         ->leftJoin('PJURIDICA', 'RESOLUCION.ID_PROVEEDOR', '=', 'PJURIDICA.ID')
        //                         ->leftJoin('PERSONA', 'PJURIDICA.ID_PERSONA', '=', 'PERSONA.ID')

        //                         ->where('ACTA_RECEPCION.CODIGO_DOCUMENTO', '=', $doc->numero_documento)
        //                         ->where('PERSONA.RUT_PERSONA', '=', explode('-',$doc->getProveedor->rut)[0])
        //                         ->where('PERSONA.DIGITO_VERIFICADOR', '=', explode('-',$doc->getProveedor->rut)[1])
        //                         ->where('ACTA_RECEPCION.TOTAL', '=', $doc->total_documento)
        //                         ->where('ACTA_RECEPCION.ES_VALIDADO', 1)

        //                         ->select(
        //                             'ACTA_RECEPCION.CODIGO_DOCUMENTO',
        //                             'ACTA_RECEPCION.FECHA_INGRESO',
        //                             'ACTA_RECEPCION.NETO',
        //                             'ACTA_RECEPCION.DESCUENTO',
        //                             'ACTA_RECEPCION.TOTAL',
        //                             'PERSONA.RUT_PERSONA',
        //                             'PERSONA.DIGITO_VERIFICADOR',
        //                             'PJURIDICA.RAZON_SOCIAL'
        //                         )
        //                         ->first();

        //     if ( is_object($recepcionHermes) ) {
                
        //         $doc->directo_por_devengar = 1;
        //         $doc->save();
                
        //     } else {

        //         $recepcionHermes = \DB::connection('hermes')
        //                         ->table('ACTA_RECEPCION')
        //                         ->leftJoin('RESOLUCION', 'ACTA_RECEPCION.ID_RESOLUCION', '=', 'RESOLUCION.ID')
        //                         ->leftJoin('PJURIDICA', 'RESOLUCION.ID_PROVEEDOR', '=', 'PJURIDICA.ID')
        //                         ->leftJoin('PERSONA', 'PJURIDICA.ID_PERSONA', '=', 'PERSONA.ID')

        //                         ->where('ACTA_RECEPCION.CODIGO_DOCUMENTO', '=', $doc->numero_documento)
        //                         ->where('PERSONA.RUT_PERSONA', '=', explode('-',$doc->getProveedor->rut)[0])
        //                         ->where('PERSONA.DIGITO_VERIFICADOR', '=', explode('-',$doc->getProveedor->rut)[1])
        //                         ->where('ACTA_RECEPCION.TOTAL', '=', $doc->total_documento_actualizado)
        //                         ->where('ACTA_RECEPCION.ES_VALIDADO', 1)

        //                         ->select(
        //                             'ACTA_RECEPCION.CODIGO_DOCUMENTO',
        //                             'ACTA_RECEPCION.FECHA_INGRESO',
        //                             'ACTA_RECEPCION.NETO',
        //                             'ACTA_RECEPCION.DESCUENTO',
        //                             'ACTA_RECEPCION.TOTAL',
        //                             'PERSONA.RUT_PERSONA',
        //                             'PERSONA.DIGITO_VERIFICADOR',
        //                             'PJURIDICA.RAZON_SOCIAL'
        //                         )
        //                         ->first();
        //         $recepcionHermes = null;

        //         if ( is_object($recepcionHermes) ) {

        //             $doc->directo_por_devengar = 1;
        //             $doc->save();
                    
        //         } else {
        //             $documentos->forget($key);
        //         }
        //     }

        // }

        // $this->info('listo pasar directo a por devengar (segun hermes). Total Doc: '.$documentos->count());

        // $documentos = Documento::whereIn('id_tipo_informe', [2,3,4,5,6,7,8])
        //                        ->doesntHave('getFoliosSigfe')
        //                        ->where('fecha_ingreso', '>', '2019-06-22 00:00:00')
        //                        ->where('directo_por_devengar', null)
        //                        ->get();

        // foreach ( $documentos as $doc ) {

        //     $doc->directo_por_devengar = 1;
        //     $doc->save();

        // }

        // $this->info('listo pasar directo a por devengar (segun tipo informe). Total Doc: '.$documentos->count());

        // $documentos = Documento::whereIn('id_tipo_documento', [3,9])
        //                        ->doesntHave('getFoliosSigfe')
        //                        ->where('fecha_ingreso', '>', '2019-06-22 00:00:00')
        //                        ->where('directo_por_devengar', null)
        //                        ->get();

        // foreach ( $documentos as $doc ) {

        //     $doc->directo_por_devengar = 1;
        //     $doc->save();

        // }

        // $this->info('listo pasar directo a por devengar (segun boletas). Total Doc: '.$documentos->count());

        // $documentos = Documento::whereIn('id_tipo_documento', [4,5])
        //                        ->with(['getDocumentoRelacionado'])
        //                        ->doesntHave('getFoliosSigfe')
        //                        ->where('fecha_ingreso', '>', '2019-06-22 00:00:00')
        //                        ->where('directo_por_devengar', null)
        //                        ->get();

        // foreach ( $documentos as $doc ) {

        //     $doc->getDocumentoRelacionado->directo_por_devengar = 1;
        //     $doc->getDocumentoRelacionado->save();

        //     $doc->directo_por_devengar = 1;
        //     $doc->save();

        // }

        // $this->info('listo pasar directo a por devengar (segun NC y ND). Total Doc: '.$documentos->count());

        \Log::info("set-pasar-por-devengar ejecutado correctamente");

        $this->info('set-pasar-por-devengar ejecutado correctamente');
    }
}
