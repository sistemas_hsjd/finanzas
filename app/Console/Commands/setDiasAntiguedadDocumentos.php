<?php namespace App\Console\Commands;

use Illuminate\Console\Command;
use App\Documento;

class setDiasAntiguedadDocumentos extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'set-dias-antiguedad-documentos';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Cambia el valor de la columna dias_antiguedad de los documentos.
                              Se cambia segun la diferencia de la fecha de documento y la fecha
                              actual.';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        set_time_limit(0);
        ini_set('max_execution_time', 0);

        $documentos = Documento::withTrashed()->get();

        foreach ( $documentos as $doc ) {
            $doc->dias_antiguedad = diferencia_dias( $doc->fecha_recepcion, date('Y-m-d') ) ;
            $doc->save();
        }

        \Log::info("Documentos actualizados (dias antiguedad) : {$documentos->count()}");

        $this->info('set-dias-antiguedad-documentos ejecutado correctamente, documentos actualizados : '. $documentos->count());
    }
}
