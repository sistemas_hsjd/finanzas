<?php namespace App\Console\Commands;

use Illuminate\Console\Command;
use App\Documento;

class SetValidadoSegunFactura extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'set-validado-segun-factura';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Valida los documentos que dependen de una factura, como las NC y ND.
                              Las valida si la factura está validada.';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $documentos = Documento::with(['getDocumentoRelacionado'])
                                ->whereHas('getDocumentoRelacionado',function ($query) {
                                    $query->where('validado', 1);
                                })
                                ->where('validado', null)
                                ->get();
        
        foreach ( $documentos as $documento ) {

            $documento->validado = 1;
            $documento->fecha_validado = $documento->getDocumentoRelacionado->fecha_validado;
            $documento->id_user_validado = $documento->getDocumentoRelacionado->id_user_validado;
            $documento->save();
            
        }

        \Log::info("lista actualizazon de validación documentos segun valides de factura. Cantidad de documentos: {$documentos->count()}");

        $this->info('lista actualizazon de validación documentos segun valides de factura');
        $this->info("Cantidad de documentos: {$documentos->count()}");
        $this->info('set-validado-segun-factura ejecutado correctamente');
    }
}
