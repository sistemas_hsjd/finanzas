<?php namespace App\Console\Commands;

use Illuminate\Console\Command;
use App\ComprobanteContable;

class setTituloDescripcionComprobantes extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'set-titulo-descripcion-comprobantes';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Llena el titulo y la descripcion de los comprobantes';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        set_time_limit(0);
        ini_set('max_execution_time', 0);

        $comprobantes = ComprobanteContable::with([
                                                'getComprobantesContablesDocumentos.getDocumento',
                                                'getUnComprobanteContableDocumento.getDocumento.getTipoDocumento',
                                                'getUnComprobanteContableDocumento.getDocumento.getProveedor',
                                                'getUnComprobanteContableDocumento'
                                            ])
                                            ->has('getComprobantesContablesDocumentos')
                                            ->where('titulo', null)
                                            ->where('descripcion', null)
                                            ->get();

        foreach ( $comprobantes as $key => $comprobante ) {

            if ( $comprobante->id_tipo_comprobante_contable == 1 ) {
                
                // Egreso
                $comprobante->titulo = 'Cancela ';
                $comprobante->titulo .= $comprobante->getUnComprobanteContableDocumento->getDocumento->getTipoDocumento->nombre;
                $comprobante->titulo .= ' '.$comprobante->getUnComprobanteContableDocumento->getProveedor->nombre;
                
            } elseif ( $comprobante->id_tipo_comprobante_contable == 2 ) {

                // Ingreso
                $comprobante->titulo = 'Percibe ';
                $comprobante->titulo .= $comprobante->getUnComprobanteContableDocumento->getDocumento->getTipoDocumento->nombre;
                $comprobante->titulo .= ' '.$comprobante->getUnComprobanteContableDocumento->getProveedor->nombre;

            } elseif ( $comprobante->id_tipo_comprobante_contable == 3 ) {

                // Traspaso
                if ( $comprobante->getUnComprobanteContableDocumento->id_cuenta_bancaria != 1 ) {
                    $comprobante->titulo = 'Traspaso ';
                    $comprobante->titulo .= $comprobante->getUnComprobanteContableDocumento->getDocumento->getTipoDocumento->nombre;
                    $comprobante->titulo .= ' '.$comprobante->getUnComprobanteContableDocumento->getProveedor->nombre;
                } else {
                    $comprobante->titulo = 'Cancela Regulariza Pago TGR,';
                    // $comprobante->titulo .= $comprobante->getUnComprobanteContableDocumento->getDocumento->getTipoDocumento->nombre;
                    $comprobante->titulo .= ' '.$comprobante->getUnComprobanteContableDocumento->getProveedor->nombre;
                }
                                

            }

            $comprobante->descripcion = 'N° Documento(s): ';
            foreach ( $comprobante->getComprobantesContablesDocumentos as $key2 => $comprobanteContableDocumento ) {

                $comprobante->descripcion .= $comprobanteContableDocumento->getDocumento->numero_documento;

                if ( ($key2 + 1 ) < $comprobante->getComprobantesContablesDocumentos->count() ) {
                    $comprobante->descripcion .= ', ';
                }

            }

            $comprobante->save();

        }

        \Log::info("Comprobantes modificados : {$comprobantes->count()}");

        $this->info('set-titulo-descripcion-comprobantes ejecutado correctamente, Comprobantes modificados : '. $comprobantes->count());
    }
}
