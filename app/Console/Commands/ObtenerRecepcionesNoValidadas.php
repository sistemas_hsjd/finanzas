<?php namespace App\Console\Commands;

use Illuminate\Console\Command;
use Freshwork\ChileanBundle\Rut;
use GuzzleHttp\Client;
use GuzzleHttp\Exception\RequestException;

use App\HermesDocumento;
use App\Proveedor;
use App\WsDocumento;

class ObtenerRecepcionesNoValidadas extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'obtener-recepciones-no-validadas';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Se obtienen las recepciones no validadas desde el servidor SIFCON.
                              Se actualizan o crean las recepciones como HermesDocumento.';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        set_time_limit(0);
        ini_set('max_execution_time', 0);

        \Log::info("OBTENER RECEPCIONES NO VALIDADAS | Inicio");

        $this->info("OBTENER RECEPCIONES NO VALIDADAS | Inicio");

        // try {

        //     $client = new Client();
        //     $response = $client->request(
        //         'GET',
        //         'http://3.90.166.123/api/recepciones_no_validadas'
        //         // 'http://127.0.0.1:8000/api/recepciones_no_validadas'
        //     );

        //     if ( $response->getStatusCode() === 200 ) {

        //         $statusCode = $response->getStatusCode();
        //         $contentResponse = $response->getBody()->getContents();
        //         $jsonResponse = json_decode($contentResponse); 
                
        //         if ( $jsonResponse != null ) {

        //             foreach ( $jsonResponse as $params ) {

        //                 $hermesDocumento = HermesDocumento::where('folio_documento', $params->FOLIO_DOCUMENTO) 
        //                                                   ->where('tipo_documento', $params->NOMBRE_DOCUMENTO)
        //                                                   ->where('documento_neto', $params->NETO)
        //                                                   ->where('documento_total', $params->TOTAL)
        //                                                   ->first();

        //                 if ( is_object($hermesDocumento) ) {
                            
        //                     $this->info("OBTENER RECEPCIONES NO VALIDADAS | Actualizar hermes documento doc: ". $params->FOLIO_DOCUMENTO);
        //                     $hermesDocumento->rut_proveedor = $params->rut_proveedor;
        //                     $hermesDocumento->codigo_pedido = $params->CODIGO_PEDIDO;
        //                     $hermesDocumento->resolucion_contrato = $params->RESOLUCION_CONTRATO;
        //                     $hermesDocumento->save();
                        
        //                 } else {
                            
        //                     $this->info("OBTENER RECEPCIONES NO VALIDADAS | Crear hermes documento codigo_pedido: ". $params->CODIGO_PEDIDO);
        //                     $proveedor = Proveedor::where('rut', $params->rut_proveedor )->first();
        //                     $hermesDocumento = new HermesDocumento($params, is_object($proveedor) ? $proveedor->id : null, $params->rut_proveedor);

        //                 }

        //             }


        //         } else {
        //             $this->info("OBTENER RECEPCIONES NO VALIDADAS | Respuesta nula");
        //         }
                
        //     } else {
        //         $this->info("OBTENER RECEPCIONES NO VALIDADAS | Respuesta diferente a 200, del servidor");
        //     }

        // } catch (RequestException $e) {
        //     $this->info("OBTENER RECEPCIONES NO VALIDADAS | Problema con cunsimir webservices de recepción no validadas: ". $e->getMessage());
        // }

        $recepcionesLocales = HermesDocumento::where('codigo_pedido', null)->get();

        \Log::info("OBTENER RECEPCIONES NO VALIDADAS | Recepciones locales para actualizar :  {$recepcionesLocales->count()}");
        $this->info("OBTENER RECEPCIONES NO VALIDADAS | Recepciones locales para actualizar :  {$recepcionesLocales->count()}");

        /**
         * Se obtienen los datos necesarios en las recepciones, para poder ser utilizadas al ingresar documentos,
         * esperando la validación de la recepcion en hermes.
         */
        foreach ( $recepcionesLocales as $recepcionLocal ) {

            if ( $recepcionLocal->getProveedor || $recepcionLocal->rut_proveedor != null ) {
                $rut = $recepcionLocal->getProveedor ? $recepcionLocal->getProveedor->rut : $recepcionLocal->rut_proveedor;
                $recepcionHermes = \DB::connection('hermes')
                                        ->table('ACTA_RECEPCION')
                                        ->leftJoin('RESOLUCION', 'ACTA_RECEPCION.ID_RESOLUCION', '=', 'RESOLUCION.ID')
                                        ->leftJoin('PJURIDICA', 'RESOLUCION.ID_PROVEEDOR', '=', 'PJURIDICA.ID')
                                        ->leftJoin('PERSONA', 'PJURIDICA.ID_PERSONA', '=', 'PERSONA.ID')
                                        ->leftJoin('TIPO_DOCUMENTO', 'ACTA_RECEPCION.ID_TIPO_DOCUMENTO', '=', 'TIPO_DOCUMENTO.ID')
                                        
                                        ->where('PERSONA.RUT_PERSONA', '=', explode('-', $rut)[0])
                                        ->where('PERSONA.DIGITO_VERIFICADOR', '=', explode('-', $rut)[1])
                                        ->where('ACTA_RECEPCION.CODIGO_DOCUMENTO', $recepcionLocal->folio_documento )
                                        ->where('TIPO_DOCUMENTO.NOMBRE', $recepcionLocal->tipo_documento)

                                        ->select(
                                            'ACTA_RECEPCION.*',
                                            'ACTA_RECEPCION.CODIGO_DOCUMENTO as FOLIO_DOCUMENTO',
                                            'PERSONA.RUT_PERSONA',
                                            'PERSONA.DIGITO_VERIFICADOR',
                                            'PJURIDICA.RAZON_SOCIAL',
                                            'TIPO_DOCUMENTO.NOMBRE as NOMBRE_DOCUMENTO',
                                            'TIPO_DOCUMENTO.CODIGO_SII as CODIGO_SII_DOCUMENTO',
                                            'RESOLUCION.NUMERO_RESOLUCION as RESOLUCION_CONTRATO'
                                        )
                                        ->first();

                if ( is_object($recepcionHermes) ) {

                    $recepcionLocal->rut_proveedor = Rut::parse( $recepcionHermes->RUT_PERSONA.'-'.$recepcionHermes->DIGITO_VERIFICADOR )->format(Rut::FORMAT_WITH_DASH);
                    $recepcionLocal->codigo_pedido = $recepcionHermes->CODIGO_PEDIDO;
                    $recepcionLocal->resolucion_contrato = $recepcionHermes->RESOLUCION_CONTRATO;
                    $recepcionLocal->save();
                    $this->info("OBTENER RECEPCIONES NO VALIDADAS | Se actualizo la recepción :  {$recepcionLocal->id}");
                
                } else {

                    $this->info("OBTENER RECEPCIONES NO VALIDADAS | Problema con la recepcion :  {$recepcionLocal->id}");
                    $this->info("OBTENER RECEPCIONES NO VALIDADAS | folio documento recepcion :  {$recepcionLocal->folio_documento}");
                    $this->info("OBTENER RECEPCIONES NO VALIDADAS | neto documento recepcion :  {$recepcionLocal->documento_neto}");
                    $this->info("OBTENER RECEPCIONES NO VALIDADAS | total documento recepcion :  {$recepcionLocal->documento_total}");
                    $this->info("OBTENER RECEPCIONES NO VALIDADAS | tipo documento recepcion :  {$recepcionLocal->tipo_documento}");
                    $this->info("OBTENER RECEPCIONES NO VALIDADAS | --------------------------------------------------------------");

                }

            }
            

        }

        // Obtenet todas las recepciones en hermes, para utilizar antes de la doble firma
        $recepcionesSifcon = WsDocumento::where('codigo_pedido', '!=', null)->get();
        $recepcionesLocales = HermesDocumento::where('codigo_pedido', '!=', null)->get();

        $recepcionesHermes = \DB::connection('hermes')
                                ->table('ACTA_RECEPCION')
                                ->leftJoin('RESOLUCION', 'ACTA_RECEPCION.ID_RESOLUCION', '=', 'RESOLUCION.ID')
                                ->leftJoin('PJURIDICA', 'RESOLUCION.ID_PROVEEDOR', '=', 'PJURIDICA.ID')
                                ->leftJoin('PERSONA', 'PJURIDICA.ID_PERSONA', '=', 'PERSONA.ID')
                                ->leftJoin('TIPO_DOCUMENTO', 'ACTA_RECEPCION.ID_TIPO_DOCUMENTO', '=', 'TIPO_DOCUMENTO.ID')
                                
                                // ->where('PERSONA.RUT_PERSONA', '=', explode('-', $recepcionLocal->getProveedor->rut)[0])
                                // ->where('PERSONA.DIGITO_VERIFICADOR', '=', explode('-', $recepcionLocal->getProveedor->rut)[1])

                                // no considerar las recepciones en el sistema.
                                ->whereNotIn('ACTA_RECEPCION.CODIGO_PEDIDO', $recepcionesLocales->pluck('codigo_pedido')->toArray() )
                                ->whereNotIn('ACTA_RECEPCION.CODIGO_DOCUMENTO', $recepcionesSifcon->pluck('codigo_pedido')->toArray() )
                                ->whereYear( 'ACTA_RECEPCION.FECHA_INGRESO','>=', 2020 )
                                
                                ->select(
                                    'ACTA_RECEPCION.*',
                                    'ACTA_RECEPCION.CODIGO_DOCUMENTO as FOLIO_DOCUMENTO',
                                    'PERSONA.RUT_PERSONA',
                                    'PERSONA.DIGITO_VERIFICADOR',
                                    'PJURIDICA.RAZON_SOCIAL',
                                    'TIPO_DOCUMENTO.NOMBRE as NOMBRE_DOCUMENTO',
                                    'TIPO_DOCUMENTO.CODIGO_SII as CODIGO_SII_DOCUMENTO',
                                    'RESOLUCION.NUMERO_RESOLUCION as RESOLUCION_CONTRATO'
                                )
                                ->get();

        $this->info("OBTENER RECEPCIONES NO VALIDADAS | ------------------------------");
        $this->info("OBTENER RECEPCIONES NO VALIDADAS | Cantidad de recepionesHermes encontradas para traer : ".$recepcionesHermes->count());

        foreach ( $recepcionesHermes as $recepcionHermes ) {
            // if ( $recepcionHermes->DIGITO_VERIFICADOR == null || $recepcionHermes->DIGITO_VERIFICADOR == '' ) {
            //     $rut = Rut::parse( $recepcionHermes->RUT_PERSONA.''.$recepcionHermes->DIGITO_VERIFICADOR )->format(Rut::FORMAT_WITH_DASH);
            //     dd($recepcionHermes, $rut);
            // }
            
            $rut = Rut::parse( $recepcionHermes->RUT_PERSONA.''.$recepcionHermes->DIGITO_VERIFICADOR )->format(Rut::FORMAT_WITH_DASH);
            $proveedor = Proveedor::where('rut', $rut )->first();
            try {

                $hermesDocumento = new HermesDocumento($recepcionHermes, is_object($proveedor) ? $proveedor->id : null, $rut);
                $this->info("OBTENER RECEPCIONES NO VALIDADAS | Se ha creado el HermesDocumento: ".$hermesDocumento->id." | folio documento: ".$hermesDocumento->folio_documento);

            } catch (\Exception $e) {
                $this->info("OBTENER RECEPCIONES NO VALIDADAS | Problema con recepcion: ".$recepcionHermes->CODIGO_PEDIDO." problema: ".$e->getMessage());
            }

        }

        $this->info("OBTENER RECEPCIONES NO VALIDADAS | FIN");

        $this->info("OBTENER RECEPCIONES NO VALIDADAS | FIN");
    }
    
}
