<?php namespace App\Console\Commands;

use Illuminate\Console\Command;
use App\Documento;
use App\RelacionDocumentoWsDocumento;
use App\HermesDocumento;

use App\WsContrato;
use App\WsOrdenCompra;
use App\WsDocumento;
use App\WsArchivo;
use App\WsItemPresupuestario;

use GuzzleHttp\Client;
use GuzzleHttp\Exception\RequestException;

class CambiarRelacionHermes extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'cambiar-relacion-hermes';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Cambia las relaciones con recepciones hermes a relaciones con
                              recepciones disponibles en el sistema';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        set_time_limit(0);
        ini_set('max_execution_time', 0);

        \Log::info("inicio cambiar relacion hermes");

        $this->info('inicio cambiar relacion hermes');

        $recepciones = \DB::table('hermes_documentos')
                            ->join('ws_documento','hermes_documentos.folio_documento','=','ws_documento.documento')
                            ->join('relacion_documento_hermes_documento', 'hermes_documentos.id','=','relacion_documento_hermes_documento.id_hermes_documento')

                            ->whereColumn('hermes_documentos.tipo_documento','LIKE','ws_documento.tipo_documento')
                            ->whereColumn('hermes_documentos.folio_documento','=','ws_documento.documento')
                            ->whereColumn('hermes_documentos.documento_total','=','ws_documento.documento_total')
                            ->whereColumn('hermes_documentos.codigo_pedido', '=', 'ws_documento.codigo_pedido')

                            ->where('ws_documento.id_relacion', null)

                            ->select(
                                'hermes_documentos.id as id_hermes_documento',
                                'hermes_documentos.folio_documento',
                                'hermes_documentos.documento_total as total_hermes',
                                'ws_documento.id as id_ws_documento',
                                'ws_documento.tipo_documento as ws_tipo_documento',
                                'ws_documento.documento as ws_numero_documento',
                                'ws_documento.documento_total'
                            )
                            ->get();

        $relacionesCambiadas = 0;
        $this->info('relaciones para cambiar : '.$recepciones->count());
        foreach ( $recepciones as $recepcion ) {
            $this->info('cambiando...');
            $hermesDocumento = HermesDocumento::with(['getRelacion.getDocumento'])->has('getRelacion.getDocumento')->findOrFail( $recepcion->id_hermes_documento );

            $documento = Documento::findOrFail( $hermesDocumento->getRelacion->id_documento );
            $wsDocumento = WsDocumento::findOrFail( $recepcion->id_ws_documento );

            $relacionAux = RelacionDocumentoWsDocumento::where('id_documento', $documento->id)
                                                        ->where('id_ws_documento', $wsDocumento->id)
                                                        ->first();

            if ( ! is_object($relacionAux) ) {

                // Se crea la relacion
                $relacion = new RelacionDocumentoWsDocumento();
                $relacion->id_documento = $documento->id;
                $relacion->id_ws_documento = $wsDocumento->id;
                $relacion->id_user_relacion = NULL;
                $relacion->updated_at = NULL;
                $relacion->save();

                // Se paasa el id correspondiente a cada documento para la conexion
                $documento->timestamps = false;
                $documento->id_relacion = $relacion->id;
                $documento->save();
                $documento->validar();
                $documento->actualizaValidacionDocumentosRelacionados();

                $wsDocumento->timestamps = false;
                $wsDocumento->id_relacion = $relacion->id;
                $wsDocumento->save();
                $relacionesCambiadas++;

            }
        
            $hermesDocumento->getRelacion->delete();
            $hermesDocumento->delete();
            
            $hermesDocumento->getRelacion->delete();
            $hermesDocumento->delete();

        }


        \Log::info("listo cambiar relacion hermes. Cantidad de recepciones cambiadas:  {$relacionesCambiadas}");

        $this->info('listo cambiar relacion hermes');
        $this->info("Cantidad de recepciones cambiadas:  {$relacionesCambiadas}");

    }
}
