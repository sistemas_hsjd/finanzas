<?php namespace App\Console\Commands;

use Illuminate\Console\Command;
use App\ArchivoAcepta;
use App\AceptaMotivoRechazo;
use Auth;

class rechazoAutomaticoFacturasAndNotasCredito extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'rechazo-automatico-facturas-and-notas-credito';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Rechaza automaticamente las facturas con notas de credito al 100%, ademas rechaza
                              las notas de credito que ya tienen la factura rechazada.
                              Los archivos no deben estar cargados en el sistema y deben ser del mismo proveedor.';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {

        set_time_limit(0);
        ini_set('max_execution_time', 0);
        
        /**
         * Para rechazar Facturas con Notas de credito al 100% 
         */
        $notasCreditoAcepta = ArchivoAcepta::where('cargado', 0)
                                           ->where('id_tipo_documento', 4)
                                           ->where('referencias','<>', null)
                                        //    ->where('id_user_rechazo', null)
                                           ->select('id','referencias','monto_total','id_proveedor','folio')
                                           ->get();


        $contador = 0;

        foreach ( $notasCreditoAcepta as $ncA ) {
            

            if ($ncA->referencias != "" && $ncA->referencias != null) {

                $numero_documento = null;
                $arrayJson = json_decode($ncA->referencias, true);
                if ($arrayJson != null) {
    
                    foreach ($arrayJson as $itemJson) {
    
                        if ( $itemJson['Tipo'] == 30 || $itemJson['Tipo'] == 32 || $itemJson['Tipo'] == 33 || $itemJson['Tipo'] == 34 ) { // Factura

                            $numero_documento = $itemJson['Folio'];

                            // Buscar la Factura
                            $facturaAcepta = ArchivoAcepta::where('cargado', 0)
                                                          ->where('id_proveedor', $ncA->id_proveedor)
                                                          ->whereIn('id_tipo_documento', [1,2,8]) // Factura y Re-factura
                                                          ->where('folio', $numero_documento)
                                                          ->where('id_user_rechazo', null)
                                                          ->where('monto_total', $ncA->monto_total)
                                                          ->select('id','id_proveedor','id_tipo_documento','folio','monto_total','emision')
                                                          ->first();

                            if ( is_object($facturaAcepta) ) {

                                // Rechazar Factura
                                $facturaAcepta->observacion_rechazo = 'Documento rechazado automáticamente por el sistema.';
                                $facturaAcepta->observacion_rechazo .= PHP_EOL.'Tiene una Nota de Crédito por el total del monto.';
                                $facturaAcepta->observacion_rechazo .= PHP_EOL.'Folio de la Nota de Crédito : '.$ncA->folio;
                                $facturaAcepta->id_user_rechazo = 85;
                                $facturaAcepta->save();

                                $auxMovitoRechazo = AceptaMotivoRechazo::where('id_archivo_acepta', $facturaAcepta->id)
                                                                       ->where('id_motivo_rechazo', 5)
                                                                       ->first();
                                if ( ! is_object($auxMovitoRechazo) ) {

                                    $aceptaMotivo = new AceptaMotivoRechazo();
                                    $aceptaMotivo->id_archivo_acepta = $facturaAcepta->id;
                                    $aceptaMotivo->id_motivo_rechazo = 5;
                                    $aceptaMotivo->save();

                                }
                                

                                // Rechazar Nota de Credito
                                $ncA->observacion_rechazo = 'Documento rechazado automáticamente por el sistema.';
                                $ncA->observacion_rechazo .= PHP_EOL.'Anula una Factura, la Nota de Crédito es por el total del monto.';
                                $ncA->observacion_rechazo .= PHP_EOL.'Folio de la Factura : '.$facturaAcepta->folio;
                                $ncA->id_user_rechazo = 85;
                                $ncA->save();

                                $auxMovitoRechazo = AceptaMotivoRechazo::where('id_archivo_acepta', $ncA->id)
                                                                       ->where('id_motivo_rechazo', 5)
                                                                       ->first();

                                if ( ! is_object($auxMovitoRechazo) ) {
                                    $aceptaMotivo = new AceptaMotivoRechazo();
                                    $aceptaMotivo->id_archivo_acepta = $ncA->id;
                                    $aceptaMotivo->id_motivo_rechazo = 5;
                                    $aceptaMotivo->save();
                                }

                                $contador++;
                                break;

                            } else {

                                /**
                                 * Para rechazar la nota de credito en caso de que la factura esté rechazada
                                 */

                                $facturaAcepta = ArchivoAcepta::where('cargado', 0)
                                                              ->where('id_proveedor', $ncA->id_proveedor)
                                                              ->whereIn('id_tipo_documento', [1,2,8]) // Factura y Re-factura
                                                              ->where('folio', $numero_documento)
                                                              ->where('id_user_rechazo','<>' ,null)
                                                              ->select('id','id_proveedor','id_tipo_documento','folio','monto_total','emision')
                                                              ->first();

                                if ( is_object($facturaAcepta) ) {

                                    // Rechazar Factura
                                    $facturaAcepta->observacion_rechazo = 'Documento rechazado automáticamente por el sistema.';
                                    $facturaAcepta->observacion_rechazo .= PHP_EOL.'Tiene una Nota de Crédito por el total del monto.';
                                    $facturaAcepta->observacion_rechazo .= PHP_EOL.'Folio de la Nota de Crédito : '.$ncA->folio;
                                    // $facturaAcepta->id_user_rechazo = 1;
                                    $facturaAcepta->save();

                                    $auxMovitoRechazo = AceptaMotivoRechazo::where('id_archivo_acepta', $facturaAcepta->id)
                                                                           ->where('id_motivo_rechazo', 5)
                                                                           ->first();
                                    if ( ! is_object($auxMovitoRechazo) ) {

                                        $aceptaMotivo = new AceptaMotivoRechazo();
                                        $aceptaMotivo->id_archivo_acepta = $facturaAcepta->id;
                                        $aceptaMotivo->id_motivo_rechazo = 5;
                                        $aceptaMotivo->save();

                                    }

                                    // Rechazar Nota de Credito
                                    $ncA->observacion_rechazo = 'Documento rechazado automáticamente por el sistema.';
                                    $ncA->observacion_rechazo .= PHP_EOL.'Aplica a una Factura que ya esta rechazada.';
                                    $ncA->observacion_rechazo .= PHP_EOL.'Folio de la Factura : '.$facturaAcepta->folio;
                                    $ncA->id_user_rechazo = 85;
                                    $ncA->save();

                                    $auxMovitoRechazo = AceptaMotivoRechazo::where('id_archivo_acepta', $ncA->id)
                                                                           ->where('id_motivo_rechazo', 5)
                                                                           ->first();

                                    if ( ! is_object($auxMovitoRechazo) ) {
                                        
                                        $aceptaMotivo = new AceptaMotivoRechazo();
                                        $aceptaMotivo->id_archivo_acepta = $ncA->id;
                                        $aceptaMotivo->id_motivo_rechazo = 5;
                                        $aceptaMotivo->save();

                                    }

                                    $contador++;
                                    break;

                                }
                                
                            }
                              
                        }
    
                    }
                }
    
            }

        }

        \Log::info("Cantidad de Archivos Acepta rechazados automaticamente : {$contador}");

        $this->info('rechazo-automatico-facturas-and-notas-credito ejecutado correctamente, rechazos : '. $contador);

    }
}
