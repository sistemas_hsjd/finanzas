<?php namespace App\Console\Commands;

use Illuminate\Console\Command;
use App\Documento;
use App\WsDocumento;
use App\RelacionDocumentoWsDocumento;

class ValidarRelacionesConRecepciones extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'validar-relaciones-con-recepciones';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Valida automaticamente todas las relaciones de las facturas con las recepciones';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $documentos = \DB::table('documentos')
                        ->join('archivos','documentos.id','=','archivos.id_documento')
                        ->join('proveedores','documentos.id_proveedor','=','proveedores.id')
                        ->join('tipos_documento','documentos.id_tipo_documento','=','tipos_documento.id')
                        ->join('ws_contrato','proveedores.rut','=','ws_contrato.rut_proveedor')
                        ->join('ws_orden_compra','ws_contrato.id','=','ws_orden_compra.id_contrato')
                        ->join('ws_documento','ws_orden_compra.id','=','ws_documento.id_orden_compra')

                        ->whereColumn('tipos_documento.nombre','LIKE','ws_documento.tipo_documento')
                        ->whereColumn('documentos.numero_documento','=','ws_documento.documento')
                        ->whereColumn('documentos.total_documento_actualizado','=','ws_documento.documento_total')

                        ->where('archivos.id_tipo_archivo', 1) // Factura
                        ->where('documentos.id_relacion', null)
                        ->where('ws_documento.id_relacion', null)

                        ->select(
                            'documentos.id as id_documento',
                            'documentos.numero_documento',
                            'documentos.total_documento_actualizado',
                            'documentos.documento_compra',
                            'proveedores.rut as rut_proveedor',
                            'proveedores.nombre as nombre_proveedor',
                            'tipos_documento.nombre as nombre_tipo_documento',
                            'ws_contrato.nombre_proveedor as ws_nombre_proveedor',
                            'ws_contrato.rut_proveedor as ws_rut_proveedor',
                            'ws_contrato.id as id_ws_contrato',
                            'ws_orden_compra.id as id_ws_orden_compra',
                            'ws_documento.id as id_ws_documento',
                            'ws_documento.tipo_documento as ws_tipo_documento',
                            'ws_documento.documento as ws_numero_documento',
                            'ws_documento.documento_total'
                        )
                    ->get();

        foreach ($documentos as $documentoConectar) {

            $documento = Documento::findOrFail( $documentoConectar->id_documento );
            $wsDocumento = WsDocumento::findOrFail( $documentoConectar->id_ws_documento );

            $relacionAux = RelacionDocumentoWsDocumento::where('id_documento', $documentoConectar->id_documento)
                                                       ->where('id_ws_documento', $documentoConectar->id_ws_documento)
                                                       ->first();

            if ( ! is_object($relacionAux) ) {

                // Se crea la relacion
                $relacion = new RelacionDocumentoWsDocumento();
                $relacion->id_documento = $documento->id;
                $relacion->id_ws_documento = $wsDocumento->id;
                $relacion->id_user_relacion = NULL;
                $relacion->updated_at = NULL;
                $relacion->save();

                // Se paasa el id correspondiente a cada documento para la conexion
                $documento->timestamps = false;
                $documento->id_relacion = $relacion->id;
                $documento->validar();
                $documento->save();
                $documento->actualizaValidacionDocumentosRelacionados();

                $wsDocumento->timestamps = false;
                $wsDocumento->id_relacion = $relacion->id;
                $wsDocumento->save();

            }

        }

        \Log::info("lista validacion automatica de relaciones. Cantidad de documentos: {$documentos->count()}");

        $this->info('lista validacion automatica de relaciones');
        $this->info("Cantidad de documentos: {$documentos->count()}");
    }
}
