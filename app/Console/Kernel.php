<?php

namespace App\Console;

use Illuminate\Console\Scheduling\Schedule;
use Illuminate\Foundation\Console\Kernel as ConsoleKernel;

class Kernel extends ConsoleKernel
{
    /**
     * The Artisan commands provided by your application.
     *
     * @var array
     */
    protected $commands = [
        //
    ];

    /**
     * Define the application's command schedule.
     *
     * @param  \Illuminate\Console\Scheduling\Schedule  $schedule
     * @return void
     */
    protected function schedule(Schedule $schedule)
    {

        // $schedule->command('carga-auxiliar-de-acepta-al-sistema')->everyThirtyMinutes();
        $schedule->command('carga-auxiliar-de-acepta-al-sistema')->twiceDaily(12, 17)->withoutOverlapping(); // guarda en el proyecto los pdf de las facturas ingresadas desde acepta

        $schedule->command('archivos-acepta-con-recepcion')->everyThirtyMinutes()->withoutOverlapping();
        
        // $schedule->command('rechazo-automatico-facturas-and-notas-credito')->hourly();
        $schedule->command('rechazo-automatico-facturas-and-notas-credito')->twiceDaily(10, 15)->withoutOverlapping();

        // $schedule->command('set-facturas-con-notas-credito-menor-al-cien-porciento')->hourly();
        $schedule->command('set-facturas-con-notas-credito-menor-al-cien-porciento')->twiceDaily(9, 13)->withoutOverlapping();
        
        $schedule->command('set-dias-antiguedad-documentos')->daily()->withoutOverlapping();

        $schedule->command('set-titulo-descripcion-comprobantes')->hourly()->withoutOverlapping();

       
        // $schedule->command('set-pasar-por-devengar')->everyTenMinutes();  // se comenta porque todos los documentos ingresados deben ser devengados, excepto los reclamados

        // $schedule->command('set-validado-segun-factura')->everyTenMinutes();
        $schedule->command('set-validado-segun-factura')->twiceDaily(8, 18)->withoutOverlapping();

        // $schedule->command('validar-relaciones-con-recepciones')->everyTenMinutes();
        $schedule->command('validar-relaciones-con-recepciones')->twiceDaily(7, 19)->withoutOverlapping();


        /**
         * Necesarias para recepciones
         */
        // $schedule->command('obtener-recepciones')->everyThirtyMinutes();
        $schedule->command('obtener-recepciones')->twiceDaily(7, 12, 14)->withoutOverlapping();

        // $schedule->command('cambiar-relacion-hermes')->everyThirtyMinutes();
        $schedule->command('cambiar-relacion-hermes')->twiceDaily(9, 13, 15)->withoutOverlapping();

        // $schedule->command('obtener-recepciones-no-validadas')->hourly();
        $schedule->command('obtener-recepciones-no-validadas')->twiceDaily(8, 11, 16)->withoutOverlapping();
       
    }

    /**
     * Register the commands for the application.
     *
     * @return void
     */
    protected function commands()
    {
        $this->load(__DIR__.'/Commands');

        require base_path('routes/console.php');
    }
}
