<?php namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class TipoInforme extends Model
{
    use SoftDeletes;
    protected $table = 'tipos_informe';

    public function __construct( $request = null, array $attributes = array())
    {
        parent::__construct($attributes);
        if ($request != null ) {
            $this->nombre = trim($request->input('nombre'));
            
            $this->id_user_created = \Auth::user()->id;
            $this->updated_at = NULL;
            $this->save();
        }
    }

    public function editTipoInforme($request)
    {
        $this->nombre = trim($request->input('nombre'));

        $this->id_user_updated = \Auth::user()->id;
        $this->save();
    }
}
