<?php namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class ReferenteTecnico extends Model
{
    use SoftDeletes;
    protected $table = 'referentes_tecnicos';


    public function __construct( $request = null, array $attributes = array())
    {
        parent::__construct($attributes);
        if ($request != null ) {
            $this->id_user = $request->input('usuario');
            $this->rut = str_replace('.','',trim($request->input('rut')));
            $this->nombre = trim($request->input('cargo'));
            $this->responsable = trim($request->input('responsable'));

            if ($request->input('responsable_finanzas') == 1) {
                $this->origen = 1;
            } else {
                $this->origen = 0;
            }
            
            $this->id_user_created = \Auth::user()->id;
            $this->updated_at = NULL;
            $this->save();
        }
    }

    public function editReferenteTecnico($request)
    {
        $this->id_user = $request->input('usuario');
        $this->rut = str_replace('.','',trim($request->input('rut')));
        $this->nombre = trim($request->input('cargo'));
        $this->responsable = trim($request->input('responsable'));

        if ($request->input('responsable_finanzas') == 1) {
            $this->origen = 1;
        } else {
            $this->origen = 0;
        }

        $this->id_user_updated = \Auth::user()->id;
        $this->save();
    }

    public function formatRut()
    {
        if ($this->rut != '' && $this->rut != null) {
            return \Rut::parse($this->rut)->format();
        } else {
            return '';
        }
    }

    public function getUser()
    {
        return $this->belongsTo('App\User','id_user');
    }
}
