<?php namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class MotivoRezagar extends Model
{
    use SoftDeletes;
    protected $table = 'motivos_rezagar';

    public function __construct( $request = null, array $attributes = array())
    {
        parent::__construct($attributes);
        if ($request != null ) {
            $this->nombre = trim($request->get('nombre'));
            $this->save();
        }
    }

    public function editMotivoRezagar($request)
    {
        $this->nombre = trim($request->get('nombre'));
        $this->save();
    }

    public function getAceptaMotivosRechazo()
    {
        return $this->hasMany('App\AceptaMotivoRezagar','id_motivo_rezagar','id');
    }
}
