<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class RelacionDocumentoHermesDocumento extends Model
{
    protected $table = 'relacion_documento_hermes_documento';

    public function getUsuarioResponsable()
    {
        return $this->belongsTo('App\User','id_user_relacion');
    }

    public function getDocumento()
    {
        return $this->belongsTo('App\Documento','id_documento');
    }

    public function getHermesDocumento()
    {
        return $this->belongsTo('App\HermesDocumento','id_hermes_documento');
    }
}
