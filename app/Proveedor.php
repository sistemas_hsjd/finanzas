<?php namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Proveedor extends Model
{
    use SoftDeletes;
    protected $table = 'proveedores';
    //protected $table = 'ft_proveedor';

    public function __construct( $request = null, array $attributes = array())
    {
        parent::__construct($attributes);
        if ( $request != null ) {
            $this->rut = str_replace('.','',trim($request->input('rut')));
            $this->nombre = trim($request->input('nombre_razon_social'));
            if ($request->input('direccion') == null) {
                $this->direccion = null;
            } else {
                $this->direccion = trim($request->input('direccion'));
            }
            
            if ($request->input('telefono') == null) {
                $this->telefono = null;
            } else {
                $this->telefono = trim($request->input('telefono'));
            }

            if ($request->input('email') == null) {
                $this->email = null;
            } else {
                $this->email = trim($request->input('email'));
            }

            if ($request->input('contacto') == null) {
                $this->contacto = null;
            } else {
                $this->contacto = trim($request->input('contacto'));
            }

            $this->id_banco = $request->input('banco');
            $this->id_tipo_cuenta = $request->input('tipo_cuenta');

            if ($request->input('numero_cuenta') == null) {
                $this->numero_cuenta = null;
            } else {
                $this->numero_cuenta = trim($request->input('numero_cuenta'));
            }

            if ($request->input('transferencia') == 1) {
                $this->transferencia = 1;
            } else {
                $this->transferencia = 0;
            }

            if ($request->input('activo') == 1) {
                $this->activo = 1;
            } else {
                $this->activo = 0;
            }
            
            $this->id_user_created = \Auth::user()->id;
            $this->updated_at = NULL;
            $this->save();

            /**
             * Para los proveedores que se fusionan y son parte del proveedor.
             */
            if ( $request->get('proveedores_fusion') ) {

                foreach ( $request->get('proveedores_fusion') as $idProv ) {
                    $provFusion = Proveedor::findOrFail($idProv);
                    $provFusion->id_proveedor_maestro = $this->id;
                    $provFusion->id_user_updated = \Auth::user()->id;
                    $provFusion->save();
                }

            }

        }
        
    }

    public function getTipoCuenta()
    {
        return $this->belongsTo('App\TipoCuenta','id_tipo_cuenta');
    }

    public function getProveedorMaestro()
    {
        return $this->belongsTo('App\Proveedor','id_proveedor_maestro');
    }

    public function getProveedoresFusion()
    {
        return $this->hasMany('App\Proveedor','id_proveedor_maestro','id');
    }

    public function getTodosLosRut()
    {
        $ruts = [];

        $ruts[] = $this->rut;

        if ( $this->getProveedoresFusion->count() > 0 ) {
            foreach ( $this->getProveedoresFusion as $provFusion) {
                $ruts[] = $provFusion->rut;
            }
        }

        return $ruts;
    }

    public function getBanco()
    {
        return $this->belongsTo('App\Banco','id_banco');
    }

    public function formatRut()
    {
        return \Rut::parse($this->rut)->format();
    }

    public function editProveedor($request)
    {
        $this->rut = str_replace('.','',trim($request->input('rut')));
        $this->nombre = trim($request->input('nombre_razon_social'));
        if ($request->input('direccion') == null) {
            $this->direccion = null;
        } else {
            $this->direccion = trim($request->input('direccion'));
        }
        
        if ($request->input('telefono') == null) {
            $this->telefono = null;
        } else {
            $this->telefono = trim($request->input('telefono'));
        }

        if ($request->input('email') == null) {
            $this->email = null;
        } else {
            $this->email = trim($request->input('email'));
        }

        if ($request->input('contacto') == null) {
            $this->contacto = null;
        } else {
            $this->contacto = trim($request->input('contacto'));
        }

        $this->id_banco = $request->input('banco');
        $this->id_tipo_cuenta = $request->input('tipo_cuenta');

        if ($request->input('numero_cuenta') == null) {
            $this->numero_cuenta = null;
        } else {
            $this->numero_cuenta = trim($request->input('numero_cuenta'));
        }

        if ($request->input('transferencia') == 1) {
            $this->transferencia = 1;
        } else {
            $this->transferencia = 0;
        }

        if ($request->input('activo') == 1) {
            $this->activo = 1;
        } else {
            $this->activo = 0;
        }

        /**
         * Para los proveedores que se fusionan y son parte del proveedor.
         */
        if ( $request->get('proveedores_fusion') ) {

            $proveedoresFueraFusion = Proveedor::whereNotIn('id',$request->get('proveedores_fusion'))
                                               ->where('id_proveedor_maestro', $this->id)
                                               ->get();
                                            
            if ( $proveedoresFueraFusion->count() > 0 ) {
                foreach ( $proveedoresFueraFusion as $provFuera ) {
                    $provFuera->id_proveedor_maestro = null;
                    $provFuera->id_user_updated = \Auth::user()->id;
                    $provFuera->save();
                }
            }

            foreach ( $request->get('proveedores_fusion') as $idProv ) {
                $provFusion = Proveedor::findOrFail($idProv);
                $provFusion->id_proveedor_maestro = $this->id;
                $provFusion->id_user_updated = \Auth::user()->id;
                $provFusion->save();
            }

        } else {

            // no se selecciona proveedores de fusion
            $proveedoresFueraFusion = Proveedor::where('id_proveedor_maestro', $this->id)
                                               ->get();

            if ( $proveedoresFueraFusion->count() > 0 ) {
                foreach ( $proveedoresFueraFusion as $provFuera ) {
                    $provFuera->id_proveedor_maestro = null;
                    $provFuera->id_user_updated = \Auth::user()->id;
                    $provFuera->save();
                }
            }

        }
         
        
        $this->id_user_updated = \Auth::user()->id;
        $this->save();
    }

    public function getDataForIndex()
    {

        $botones = '';
        $botones .= '<div class="btn-group">';

        // Ver
        if ( \Entrust::can('ver-proveedor') ) {
            $botones .= '<button class="btn btn-success btn-xs" title="Ver Proveedor" onclick="ver('. $this->id .');">';
            $botones .= '<i class="fa fa-eye"></i></button>';
        }
        
        // Editar
        if ( \Entrust::can('editar-proveedor') ) {
            $botones .= '<button type="button" class="btn btn-warning btn-xs" title="Editar Proveedor" onclick="editar('. $this->id .');">';
            $botones .= '<i class="fas fa-edit"></i></button>';
        }

        // Eliminar Documento
        if ( \Entrust::can('eliminar-proveedor') && ! $this->getProveedorMaestro ) {
            $botones .= '<button type="button" class="btn btn-danger btn-xs" title="Eliminar Proveedor" onclick="eliminar('. $this->id .');">';
            $botones .= '<i class="fa fa-trash"></i></button>';
        }

        $botones .= '</div>';

        if ( $this->transferencia == 0 ) {
            $transferencia = 'No';
        } else {
            $transferencia = 'Si';
        }

        if ( $this->activo == 0 ) {
            $activo = 'No';
        } else {
            $activo = 'Si';
        }

        
        $nombre = '';
        if ( $this->getProveedoresFusion->count() > 0 ) {
            $nombre .= '<strong>'.$this->nombre.'</strong>';
        } else {
            $nombre .= $this->nombre;
        }

        $docJson = array(
            'DT_RowID' => $this->id,
            $this->rut,
            $nombre,
            $this->email,
            $this->contacto,
            $transferencia,
            $activo,
            $botones
        );

        return $docJson;

    }

    public function getContratos()
    {
        return $this->hasMany('App\Contrato','id_proveedor','id');
    }

}
