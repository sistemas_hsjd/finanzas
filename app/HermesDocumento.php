<?php namespace App;

use Illuminate\Database\Eloquent\Model;

class HermesDocumento extends Model
{
    protected $table = 'hermes_documentos';

    public function __construct( $params = null, $idProveedor = null, $rutProveedor = null ,array $attributes = array())
    {
        parent::__construct($attributes);

        if ( $params != null && ( $idProveedor != null || $rutProveedor != null ) ) {
            
            $this->id_proveedor = $idProveedor;
            $this->rut_proveedor = $rutProveedor;
            $this->folio_documento = $params->FOLIO_DOCUMENTO;
            $this->tipo_documento = $params->NOMBRE_DOCUMENTO;
            $this->documento_neto = formato_entero( formatoMiles($params->NETO) );
            $this->documento_total = formato_entero( formatoMiles($params->TOTAL) );
            $this->fecha_ingreso_recepcion = $params->FECHA_INGRESO;
            $this->fecha_validacion_recepcion = $params->FECHA_VALIDACION;
            $this->codigo_pedido = $params->CODIGO_PEDIDO;
            $this->resolucion_contrato = $params->RESOLUCION_CONTRATO;
            $this->save();
            
        }
        
    }

    public function getRelacion()
    {    
        return $this->hasOne('App\RelacionDocumentoHermesDocumento','id_hermes_documento','id');
    }

    public function getProveedor()
    {
        return $this->belongsTo('App\Proveedor','id_proveedor');
    }
}
