<?php namespace App;

use Illuminate\Database\Eloquent\Model;

class WsDocumento extends Model
{
    protected $table = 'ws_documento';

    public function __construct( $paramsDocumento = null, $idOrdenCompra = null, $codigoPedido = null ,array $attributes = array())
    {
        parent::__construct($attributes);
        if ( $paramsDocumento != null ) {
            $this->codigo_pedido = $codigoPedido;
            $this->id_orden_compra = $idOrdenCompra;
            // Datos Requeridos
            $this->documento = $paramsDocumento->documento;
            $this->tipo_documento = $paramsDocumento->tipo_documento;
            $this->nombre_usuario_responsable = $paramsDocumento->nombre_usuario_responsable;
            $this->documento_descuento_total = $paramsDocumento->documento_descuento_total;
            $this->documento_neto = $paramsDocumento->documento_neto;
            $this->documento_iva = $paramsDocumento->documento_iva;
            $this->documento_total = $paramsDocumento->documento_total;
            $this->save();
        }
        
    }

    public function actualizaDocumento($paramsDocumento, $idOrdenCompra)
    {
        $this->id_orden_compra = $idOrdenCompra;
        // Datos Requeridos
        $this->documento = $paramsDocumento->documento;
        $this->tipo_documento = $paramsDocumento->tipo_documento;
        $this->nombre_usuario_responsable = $paramsDocumento->nombre_usuario_responsable;
        $this->documento_descuento_total = $paramsDocumento->documento_descuento_total;
        $this->documento_neto = $paramsDocumento->documento_neto;
        $this->documento_iva = $paramsDocumento->documento_iva;
        $this->documento_total = $paramsDocumento->documento_total;
        $this->save();
    }

    public function getWsOrdenCompra()
    {        
        return $this->belongsTo('App\WsOrdenCompra','id_orden_compra');
    }

    public function getWsArchivos()
    {        
        return $this->hasMany('App\WsArchivo','id_documento','id');
    }

    public function tieneArchvioOrdenCompra()
    {
        $tieneArchivoOC = false;

        if ($this->getWsArchivos) {
            foreach ($this->getWsArchivos as $archivo) {
                // Factura
                if ( strpos($archivo->nombre_archivo,'OrdenCompra') !== false ) {
                    $tieneArchivoOC = true;
                    break;
                }
            }
        }

        return $tieneArchivoOC;
    }

    public function getWsItemsPresupuestarios()
    {        
        return $this->hasMany('App\WsItemPresupuestario','id_documento','id');
    }

    public function getRelacion()
    {    
        return $this->hasOne('App\RelacionDocumentoWsDocumento','id_ws_documento','id');
    }

    /**
     * Retorna un array de datos del documento y su orden de compra.
     * Se utiliza al momento de seleccionar una recepcion al ingresar un documento al sistema
     */
    public function getDatosDocumentoAndOrdenCompra()
    {
        $datos = array(
            'numeroDocumento' => $this->documento,
            'valorDocumento'  => $this->documento_total,
            'numeroOrdenCompra' => $this->getWsOrdenCompra->numero_orden_compra_mercado_publico,
            'fechaOrdenCompra'  => fecha_dmy($this->getWsOrdenCompra->fecha_orden_compra_mercado_publico),
            'montoMaximoOrdenCompra' => formatoMiles($this->getWsOrdenCompra->total_orden_compra),
            'saldoOrdenCompra' => formatoMiles($this->getWsOrdenCompra->saldo_orden_compra),
        );
        $datosJson = htmlspecialchars(json_encode($datos), ENT_QUOTES, 'UTF-8');
        return $datosJson;
    }

    public function getDatosRecepciones()
    {
        $rutProveedor = '';
        $nombreProveedor = '';

        if ( $this->getWsOrdenCompra->getWsContrato ) {
            $rutProveedor = $this->getWsOrdenCompra->getWsContrato->rut_proveedor;
            $nombreProveedor = $this->getWsOrdenCompra->getWsContrato->nombre_proveedor;
        }

        $totalDocumento = '$ '.formatoMiles($this->documento_total);
        $numOc =  $this->getWsOrdenCompra->numero_orden_compra;

        // $archivoOc = "<i class='fas fa-clipboard-check fa-lg' style='color:{$this->getWsOrdenCompra->colorPorFirmas()} !important'></i> No";
        $archivoOc = "No";
        if ( $this->tieneArchvioOrdenCompra() ) {
            // $archivoOc = "<i class='fas fa-clipboard-check fa-lg' style='color:{$this->getWsOrdenCompra->colorPorFirmas()} !important'></i> Si";
            $archivoOc = "Si";
        }

        $numOcMercadoPublico = '';
        if ( $this->getWsOrdenCompra ) {
            $numOcMercadoPublico = $this->getWsOrdenCompra->numero_orden_compra_mercado_publico;
        }

        $fechaCarga = fecha_dmY($this->fecha_carga);

        $botones = "<div class='btn-group'>";

        if ( \Entrust::can('ver-documentos-recepcionados') ) {
            
            $botones .= "<button class='btn btn-success btn-xs' title='Ver Documento' onclick='ver({$this->id});' >";
            $botones .= "<i class='fas fa-eye'></i></button>";
            
        }

        if ( \Entrust::can('ver-archivo-documento') ) {

            foreach ($this->getWsArchivos as $archivo) {

                $botones .= "<a class='btn btn-info btn-xs' ";

                if ( $archivo->cargado == 0 ) {

                    $ubicacion = asset($archivo->ubicacion);
                    $botones .= " href='{$ubicacion}' ";

                } else {

                    $ubicacion = $archivo->ubicacion;
                    $botones .= " href='http://{$ubicacion}' ";

                }
                $title = explode('_',$archivo->nombre_archivo)[0];
                $botones .= "title='Ver Archivo {$title}' ";
                $botones .= "target='_blank' ><i class='fas fa-clipboard-list'></i></a>";

            }

        }

        $botones .= "</div>";
        
        $docJson = array(
            'DT_RowID' => $this->id,
            $this->tipo_documento,
            $rutProveedor,
            $nombreProveedor,
            $this->documento,
            $totalDocumento,
            $numOc,
            $archivoOc,
            $numOcMercadoPublico,
            $fechaCarga,
            $botones
        );

        return $docJson;
    }

    public function getDatosRecepcionesMatchPendiente($documentos, $archivosAcepta)
    {
        $rutProveedor = '';
        $nombreProveedor = '';

        if ( $this->getWsOrdenCompra->getWsContrato ) {
            $rutProveedor = $this->getWsOrdenCompra->getWsContrato->rut_proveedor;
            $nombreProveedor = $this->getWsOrdenCompra->getWsContrato->nombre_proveedor;
        }

        $totalDocumento = '$ '.formatoMiles($this->documento_total);

        if ( in_array($this->id, $documentos) ) {
            $this->ubicacion = 'Documentos';
        }

        if ( in_array($this->id, $archivosAcepta) ) {
            $this->ubicacion = 'Archivos Acepta';
        }

        // $archivoOc = "<i class='fas fa-clipboard-check fa-lg' style='color:{$this->getWsOrdenCompra->colorPorFirmas()} !important'></i> No";
        $archivoOc = "No";
        if ( $this->tieneArchvioOrdenCompra() ) {
            // $archivoOc = "<i class='fas fa-clipboard-check fa-lg' style='color:{$this->getWsOrdenCompra->colorPorFirmas()} !important'></i> Si";
            $archivoOc = "Si";
        }

        $numOcMercadoPublico = '';
        if ( $this->getWsOrdenCompra ) {
            $numOcMercadoPublico = $this->getWsOrdenCompra->numero_orden_compra_mercado_publico;
        }

        $fechaCarga = fecha_dmY($this->fecha_carga);

        $botones = "<div class='btn-group'>";

        if ( \Entrust::can('ver-documentos-recepcionados') ) {
            
            $botones .= "<button class='btn btn-success btn-xs' title='Ver Documento' onclick='ver({$this->id});' >";
            $botones .= "<i class='fas fa-eye'></i></button>";
            
        }

        if ( \Entrust::can('ver-archivo-documento') ) {

            foreach ($this->getWsArchivos as $archivo) {

                $botones .= "<a class='btn btn-info btn-xs' ";

                if ( $archivo->cargado == 0 ) {

                    $ubicacion = asset($archivo->ubicacion);
                    $botones .= " href='{$ubicacion}' ";

                } else {

                    $ubicacion = $archivo->ubicacion;
                    $botones .= " href='http://{$ubicacion}' ";

                }
                $title = explode('_',$archivo->nombre_archivo)[0];
                $botones .= "title='Ver Archivo {$title}' ";
                $botones .= "target='_blank' ><i class='fas fa-clipboard-list'></i></a>";

            }

        }

        $botones .= "</div>";
        
        $docJson = array(
            'DT_RowID' => $this->id,
            $this->tipo_documento,
            $rutProveedor,
            $nombreProveedor,
            $this->documento,
            $totalDocumento,
            $this->ubicacion,
            $archivoOc,
            $numOcMercadoPublico,
            $fechaCarga,
            $botones
        );

        return $docJson;
    }

    public function getDatosRecepcionesSinDocumento()
    {
        $rutProveedor = '';
        $nombreProveedor = '';

        if ( $this->getWsOrdenCompra->getWsContrato ) {
            $rutProveedor = $this->getWsOrdenCompra->getWsContrato->rut_proveedor;
            $nombreProveedor = $this->getWsOrdenCompra->getWsContrato->nombre_proveedor;
        }

        $totalDocumento = '$ '.formatoMiles($this->documento_total);


        // $archivoOc = "<i class='fas fa-clipboard-check fa-lg' style='color:{$this->getWsOrdenCompra->colorPorFirmas()} !important'></i> No";
        $archivoOc = "No";
        if ( $this->tieneArchvioOrdenCompra() ) {
            // $archivoOc = "<i class='fas fa-clipboard-check fa-lg' style='color:{$this->getWsOrdenCompra->colorPorFirmas()} !important'></i> Si";
            $archivoOc = "Si";
        }

        $numOcMercadoPublico = '';
        if ( $this->getWsOrdenCompra ) {
            $numOcMercadoPublico = $this->getWsOrdenCompra->numero_orden_compra_mercado_publico;
        }

        $fechaCarga = fecha_dmY($this->fecha_carga);

        $botones = "<div class='btn-group'>";

        if ( \Entrust::can('ver-documentos-recepcionados') ) {

            if ( $this->comentario_error == null ) {

                $botones .= "<button class='btn btn-success btn-xs' title='Ver Documento' onclick='ver({$this->id});' >";
                $botones .= "<i class='fas fa-eye'></i></button>";

            } else {

                $botones .= "<button class='btn btn-danger btn-xs' title='Ver Documento' onclick='ver({$this->id});' >";
                $botones .= "<i class='fas fa-eye fa-pulso'></i></button>";

            }
            
        }

        if ( \Entrust::can('comentar-error-recepcion') ) {

            $botones .= "<button class='btn btn-warning btn-xs' title='Añadir Comentario de Error a la Recepción' onclick='error({$this->id});' >";
            $botones .= "<i class='fas fa-comment'></i></button>";
            
        }

        if ( \Entrust::can('ver-archivo-documento') ) {

            foreach ($this->getWsArchivos as $archivo) {

                $botones .= "<a class='btn btn-info btn-xs' ";

                if ( $archivo->cargado == 0 ) {

                    $ubicacion = asset($archivo->ubicacion);
                    $botones .= " href='{$ubicacion}' ";

                } else {

                    $ubicacion = $archivo->ubicacion;
                    $botones .= " href='http://{$ubicacion}' ";

                }
                $title = explode('_',$archivo->nombre_archivo)[0];
                $botones .= "title='Ver Archivo {$title}' ";
                $botones .= "target='_blank' ><i class='fas fa-clipboard-list'></i></a>";

            }

        }

        $botones .= "</div>";
        
        $docJson = array(
            'DT_RowID' => $this->id,
            $this->tipo_documento,
            $rutProveedor,
            $nombreProveedor,
            $this->documento,
            $totalDocumento,
            $archivoOc,
            $numOcMercadoPublico,
            $fechaCarga,
            $botones
        );

        return $docJson;
    }

}
