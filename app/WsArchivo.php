<?php namespace App;

use Illuminate\Database\Eloquent\Model;

class WsArchivo extends Model
{
    protected $table = 'ws_archivo';

    public function __construct( $paramsArchivos = null, $idDocumento = null, $idOc = null, array $attributes = array())
    {
        
        parent::__construct($attributes);
        if ( $paramsArchivos != null ) {
            $content = '';
            
            if ( $idDocumento != null) {
                $this->id_documento = $idDocumento;
            } else {
                // En caso que la OC no tenga recepción
                $this->id_documento = 0;
            }
            
            $this->nombre_archivo = $paramsArchivos->nombre_archivo.'_'.date('Y_m_d_G_i_s');
        
            if ( $paramsArchivos->archivo_base64 != null && $paramsArchivos->archivo_base64 != '' ) {
                $auxData = str_replace(' ','+',$paramsArchivos->archivo_base64);
                // $content = base64_decode($paramsArchivos->archivo_base64);
                $content = base64_decode($auxData);
            }    
            
            if ( @$paramsArchivos->url_externa != null && @$paramsArchivos->url_externa != '' ) {

                if ( false !== ( $content = @file_get_contents($paramsArchivos->url_externa) ) ) {
                    $content = file_get_contents($paramsArchivos->url_externa);
                }

            }
            
            if ( $content != '' ) {
                
                if ( !file_exists(public_path().'/archivobodega/'.$this->id_documento) ) {
                    mkdir(public_path().'/archivobodega/'.$this->id_documento, 0775, true); // Se crea la carpeta
                    chmod(public_path().'/archivobodega/'.$this->id_documento, 0775);
                }

                $file = fopen( public_path().'/archivobodega/'.$this->id_documento.'/'.$this->nombre_archivo.'.pdf', 'a');
                fwrite($file, $content);
                fclose($file);
                $this->ubicacion = 'archivobodega/'.$this->id_documento.'/'.$this->nombre_archivo.'.pdf';

                // $this->base64 = $paramsArchivos->archivo_base64;
                // Nuevos Datos
                $this->id_ws_orden_compra = $idOc;

                //para guardar el id tipo archivo correctamente
                if ( strpos($paramsArchivos->nombre_archivo,'OrdenCompra') !== false ) {
                    $this->id_tipo_archivo = 3;
                }

                if ( strpos($paramsArchivos->nombre_archivo,'ActaRecepcion') !== false ) {
                    $this->id_tipo_archivo = 5;
                }

                // Datos Opcionales
                if ( $this->id_tipo_archivo == null ) {
                    if ( @$paramsArchivos->id_tipo_archivo != '' && @$paramsArchivos->id_tipo_archivo != null && @$paramsArchivos->id_tipo_archivo != 0) {
                        $this->id_tipo_archivo = $paramsArchivos->id_tipo_archivo;
                    }
                }
                
                $this->save();
            }

        }        
    }

    public function actualizarArchivo($paramsArchivo)
    {
        // en caso de ser OC, se elimina (para actualizar el archivo)
        // en caso de ser ActaRecepcion también se elimina
        if ( strpos($this->nombre_archivo,'OrdenCompra') !== false && strpos($paramsArchivo->nombre_archivo,'OrdenCompra') !== false ) {
            if ( file_exists(public_path().'/'.$this->ubicacion) ) {
                unlink(public_path().'/'.$this->ubicacion); // elimina archivo
            }
        }

        if ( strpos($this->nombre_archivo,'ActaRecepcion') !== false && strpos($paramsArchivo->nombre_archivo,'ActaRecepcion') !== false ) {
        
            if ( file_exists(public_path().'/'.$this->ubicacion) ) {
                unlink(public_path().'/'.$this->ubicacion); // elimina archivo
            }
        }
        

        $this->nombre_archivo = $paramsArchivo->nombre_archivo.'_'.date('Y_m_d_G_i_s');
            
        if ( $paramsArchivo->archivo_base64 != null && $paramsArchivo->archivo_base64 != '' ) {
            $auxData = str_replace(' ','+',$paramsArchivo->archivo_base64);
            // $content = base64_decode($paramsArchivo->archivo_base64);
            $content = base64_decode($auxData);
        }    
        
        if ( @$paramsArchivo->url_externa != null && @$paramsArchivo->url_externa != '' ) {
            $content = file_get_contents($paramsArchivo->url_externa);
        }

        if ( !file_exists(public_path().'/archivobodega/'.$this->id_documento) ) {
            mkdir(public_path().'/archivobodega/'.$this->id_documento, 0775, true);
            chmod(public_path().'/archivobodega/'.$this->id_documento, 0775);
        }

        $file = fopen( public_path().'/archivobodega/'.$this->id_documento.'/'.$this->nombre_archivo.'.pdf', 'a');
        fwrite($file, $content);
        fclose($file);
        $this->ubicacion = 'archivobodega/'.$this->id_documento.'/'.$this->nombre_archivo.'.pdf';

        $this->save();
    }

    public function eliminarArchivo()
    {
        if ( file_exists(public_path().'/'.$this->ubicacion) ) {
            unlink(public_path().'/'.$this->ubicacion); // elimina archivo
        }

        $this->delete();
    }

    public function getTipoArchivo()
    {
        return $this->belongsTo('App\TipoArchivo','id_tipo_archivo');
    }
}
