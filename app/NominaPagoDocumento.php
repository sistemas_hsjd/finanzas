<?php namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class NominaPagoDocumento extends Model
{
    use SoftDeletes;
    protected $table = 'nominas_pago_documentos';

    public function getDocumento()
    {
        return $this->hasOne('App\Documento','id','id_documento');
    }

    public function getNominaPago()
    {
        return $this->hasOne('App\NominaPago','id','id_nomina_pago');
    }

}
