<?php

use Illuminate\Database\Seeder;
use App\TipoComprobanteContable;

class TiposComprobantesContablesSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $egreso = new TipoComprobanteContable();
        $egreso->nombre = 'Egreso';
        $egreso->save();

        $ingreso = new TipoComprobanteContable();
        $ingreso->nombre = 'Ingreso';
        $ingreso->save();

        $traspaso = new TipoComprobanteContable();
        $traspaso->nombre = 'Traspaso';
        $traspaso->save();
    }
}
