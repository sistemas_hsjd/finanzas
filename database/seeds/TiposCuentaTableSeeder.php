<?php

use Illuminate\Database\Seeder;
use App\TipoCuenta;

class TiposCuentaTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $newTipoCuenta = new TipoCuenta();
        $newTipoCuenta->nombre = "Cuenta Corriente";
        $newTipoCuenta->save();

        $newTipoCuenta = new TipoCuenta();
        $newTipoCuenta->nombre = "Chequera Electrónica";
        $newTipoCuenta->save();

        $newTipoCuenta = new TipoCuenta();
        $newTipoCuenta->nombre = "Cuenta RUT";
        $newTipoCuenta->save();

        $newTipoCuenta = new TipoCuenta();
        $newTipoCuenta->nombre = "Cuenta de Ahorro";
        $newTipoCuenta->save();

        $newTipoCuenta = new TipoCuenta();
        $newTipoCuenta->nombre = "Cuenta de Gastos";
        $newTipoCuenta->save();

    }
}
