<?php

use Illuminate\Database\Seeder;
use App\TipoAdjudicacion;

class TiposAdjudicacionTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $newTipoAdjudicacion = new TipoAdjudicacion();
        $newTipoAdjudicacion->nombre = 'Convenio marco';
        $newTipoAdjudicacion->save();

        $newTipoAdjudicacion = new TipoAdjudicacion();
        $newTipoAdjudicacion->nombre = 'Convenio cenabast';
        $newTipoAdjudicacion->save();

        $newTipoAdjudicacion = new TipoAdjudicacion();
        $newTipoAdjudicacion->nombre = 'Licitación';
        $newTipoAdjudicacion->save();

        $newTipoAdjudicacion = new TipoAdjudicacion();
        $newTipoAdjudicacion->nombre = 'Trato directo';
        $newTipoAdjudicacion->save();

        $newTipoAdjudicacion = new TipoAdjudicacion();
        $newTipoAdjudicacion->nombre = 'O/C en tramite';
        $newTipoAdjudicacion->save();

        $newTipoAdjudicacion = new TipoAdjudicacion();
        $newTipoAdjudicacion->nombre = 'Carta de resguardo';
        $newTipoAdjudicacion->save();

        $newTipoAdjudicacion = new TipoAdjudicacion();
        $newTipoAdjudicacion->nombre = 'Resolución exenta';
        $newTipoAdjudicacion->save();

        $newTipoAdjudicacion = new TipoAdjudicacion();
        $newTipoAdjudicacion->nombre = 'Consumo Basico';
        $newTipoAdjudicacion->save();
    }
}
