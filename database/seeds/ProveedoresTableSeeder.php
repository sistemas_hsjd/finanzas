<?php

use Illuminate\Database\Seeder;
use App\Proveedor;

class ProveedoresTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $newProveedor = new Proveedor();
        $newProveedor->nombre = "WALTER AVENDAÑO JARA";
        $newProveedor->rut = "10026841-8";
        $newProveedor->transferencia = 0;
        $newProveedor->activo = 1;
        $newProveedor->save();

        $newProveedor = new Proveedor();
        $newProveedor->nombre = "GARAY VENEGAS LUIS ALFREDO";
        $newProveedor->rut = "10033071-7";
        $newProveedor->transferencia = 0;
        $newProveedor->activo = 1;
        $newProveedor->save();

        $newProveedor = new Proveedor();
        $newProveedor->nombre = "AGENCIA DE ADUANA EDUARDO IVAN";
        $newProveedor->rut = "10105041-6";
        $newProveedor->transferencia = 0;
        $newProveedor->activo = 1;
        $newProveedor->save();

        $newProveedor = new Proveedor();
        $newProveedor->nombre = "MARIA JOSE DIEZ CASTELLANOS";
        $newProveedor->rut = "10401270-1";
        $newProveedor->transferencia = 0;
        $newProveedor->activo = 1;
        $newProveedor->save();

        $newProveedor = new Proveedor();
        $newProveedor->nombre = "JORGE ARDILA REYES";
        $newProveedor->rut = "10456790-8";
        $newProveedor->transferencia = 0;
        $newProveedor->activo = 1;
        $newProveedor->save();

        $newProveedor = new Proveedor();
        $newProveedor->nombre = "HECTOR MUÑOZ TOBAR";
        $newProveedor->rut = "10485305-6";
        $newProveedor->transferencia = 0;
        $newProveedor->activo = 1;
        $newProveedor->save();

        $newProveedor = new Proveedor();
        $newProveedor->nombre = "DECINTI WEISS EMILIO";
        $newProveedor->rut = "10488254-4";
        $newProveedor->transferencia = 0;
        $newProveedor->activo = 1;
        $newProveedor->save();

        $newProveedor = new Proveedor();
        $newProveedor->nombre = "SANCHEZ VALENZUELA JUAN EDUARDO";
        $newProveedor->rut = "10491114-5";
        $newProveedor->transferencia = 0;
        $newProveedor->activo = 1;
        $newProveedor->save();

        $newProveedor = new Proveedor();
        $newProveedor->nombre = "GONZALEZ FERNANDEZ CHRISTIAN";
        $newProveedor->rut = "10633870-1";
        $newProveedor->transferencia = 0;
        $newProveedor->activo = 1;
        $newProveedor->save();

        $newProveedor = new Proveedor();
        $newProveedor->nombre = "PALOMA DEL PILAR RODRIGUEZ ALVAREZ";
        $newProveedor->rut = "10655492-7";
        $newProveedor->transferencia = 0;
        $newProveedor->activo = 1;
        $newProveedor->save();

        $newProveedor = new Proveedor();
        $newProveedor->nombre = "SOTO GAVILAN JUAN JOSE";
        $newProveedor->rut = "10695899-8";
        $newProveedor->transferencia = 0;
        $newProveedor->activo = 1;
        $newProveedor->save();

        $newProveedor = new Proveedor();
        $newProveedor->nombre = "PABLO EXEQUIEL BRAVO FIGUEROA";
        $newProveedor->rut = "10925124-0";
        $newProveedor->transferencia = 0;
        $newProveedor->activo = 1;
        $newProveedor->save();

        $newProveedor = new Proveedor();
        $newProveedor->nombre = "VASQUEZ VALENZUELA ANA";
        $newProveedor->rut = "10943933-9";
        $newProveedor->transferencia = 0;
        $newProveedor->activo = 1;
        $newProveedor->save();

        $newProveedor = new Proveedor();
        $newProveedor->nombre = "LEONEL VELASQUEZ";
        $newProveedor->rut = "11268222-8";
        $newProveedor->transferencia = 0;
        $newProveedor->activo = 1;
        $newProveedor->save();

        $newProveedor = new Proveedor();
        $newProveedor->nombre = "MARCELA DEL CARMEN WODEHOUSE ESPINOZA";
        $newProveedor->rut = "11297950-6";
        $newProveedor->email = "WYPIMPRESORES@GMAIL.COM";
        $newProveedor->transferencia = 0;
        $newProveedor->activo = 1;
        $newProveedor->save();

        $newProveedor = new Proveedor();
        $newProveedor->nombre = "CLAUDIO TORO DUBO";
        $newProveedor->rut = "11381284-2";
        $newProveedor->transferencia = 0;
        $newProveedor->activo = 1;
        $newProveedor->save();

        $newProveedor = new Proveedor();
        $newProveedor->nombre = "ANDRES ENRIQUE ESCOBAR JARA";
        $newProveedor->rut = "11793554-K";
        $newProveedor->transferencia = 0;
        $newProveedor->activo = 1;
        $newProveedor->save();

        $newProveedor = new Proveedor();
        $newProveedor->nombre = "HERNANDEZ GALVEZ CAROLINA";
        $newProveedor->rut = "11861535-2";
        $newProveedor->transferencia = 0;
        $newProveedor->activo = 1;
        $newProveedor->save();

        $newProveedor = new Proveedor();
        $newProveedor->nombre = "REINALDO HERNAN ESPELETA SANTIS";
        $newProveedor->rut = "12043806-9";
        $newProveedor->email = "ITUR@RESMAIN.CL";
        $newProveedor->transferencia = 0;
        $newProveedor->activo = 1;
        $newProveedor->save();

        $newProveedor = new Proveedor();
        $newProveedor->nombre = "JORGE ESPINOZA CARDENAS";
        $newProveedor->rut = "12167046-1";
        $newProveedor->transferencia = 0;
        $newProveedor->activo = 1;
        $newProveedor->save();

        $newProveedor = new Proveedor();
        $newProveedor->nombre = "LUIGI ENZO STEFANO BRUZZO DIAZ";
        $newProveedor->rut = "12466049-1";
        $newProveedor->email = "jpacheco@banpro.cl";
        $newProveedor->transferencia = 0;
        $newProveedor->activo = 1;
        $newProveedor->save();

        $newProveedor = new Proveedor();
        $newProveedor->nombre = "HUGO AVENDANO MEJIAS";
        $newProveedor->rut = "12516798-5";
        $newProveedor->transferencia = 0;
        $newProveedor->activo = 1;
        $newProveedor->save();

        $newProveedor = new Proveedor();
        $newProveedor->nombre = "GUILLERMINA CANIGUANTE BARCO";
        $newProveedor->rut = "12578089-K";
        $newProveedor->transferencia = 0;
        $newProveedor->activo = 1;
        $newProveedor->save();

        $newProveedor = new Proveedor();
        $newProveedor->nombre = "XIMENA ESPINOZA RAGLIANTI";
        $newProveedor->rut = "12903978-7";
        $newProveedor->transferencia = 0;
        $newProveedor->activo = 1;
        $newProveedor->save();

        $newProveedor = new Proveedor();
        $newProveedor->nombre = "MARTA IRENE HUENCHULLAN FUENTES";
        $newProveedor->rut = "12908371-9";
        $newProveedor->email = "ETIQCINT@ETIQCINT.CL";
        $newProveedor->transferencia = 0;
        $newProveedor->activo = 1;
        $newProveedor->save();

        $newProveedor = new Proveedor();
        $newProveedor->nombre = "SUSANA MONICA HIGUERA ESCOBAR";
        $newProveedor->rut = "12910386-8";
        $newProveedor->email = "magasuequipos@gmail.com";
        $newProveedor->transferencia = 0;
        $newProveedor->activo = 1;
        $newProveedor->save();

        $newProveedor = new Proveedor();
        $newProveedor->nombre = "MARICELA VILLALOBOS URRUTIA";
        $newProveedor->rut = "12963520-7";
        $newProveedor->transferencia = 0;
        $newProveedor->activo = 1;
        $newProveedor->save();

        $newProveedor = new Proveedor();
        $newProveedor->nombre = "CARLOS SANTIBAÑEZ CASTRO";
        $newProveedor->rut = "13055993-K";
        $newProveedor->transferencia = 0;
        $newProveedor->activo = 1;
        $newProveedor->save();

        $newProveedor = new Proveedor();
        $newProveedor->nombre = "ARANCIBIA CAMPOS  RICARDO";
        $newProveedor->rut = "13102346-4";
        $newProveedor->transferencia = 0;
        $newProveedor->activo = 1;
        $newProveedor->save();

        $newProveedor = new Proveedor();
        $newProveedor->nombre = "OSES VERGARA CLAUDIO";
        $newProveedor->rut = "13233255-K";
        $newProveedor->transferencia = 0;
        $newProveedor->activo = 1;
        $newProveedor->save();

        $newProveedor = new Proveedor();
        $newProveedor->nombre = "SUBERCASEUX VERGARA FERNANDO";
        $newProveedor->rut = "13240862-9";
        $newProveedor->transferencia = 0;
        $newProveedor->activo = 1;
        $newProveedor->save();

        $newProveedor = new Proveedor();
        $newProveedor->nombre = "ARANCIBIA HENRIQUEZ JOSE MIGUEL";
        $newProveedor->rut = "13242007-6";
        $newProveedor->transferencia = 0;
        $newProveedor->activo = 1;
        $newProveedor->save();

        $newProveedor = new Proveedor();
        $newProveedor->nombre = "FELIPE CASTILLO VALLE";
        $newProveedor->rut = "13300013-5";
        $newProveedor->email = "COTIZACIONELCASTILLO@GMAIL.COM";
        $newProveedor->transferencia = 0;
        $newProveedor->activo = 1;
        $newProveedor->save();

        $newProveedor = new Proveedor();
        $newProveedor->nombre = "CAROLINA SALGADO MANZANO";
        $newProveedor->rut = "13434867-4";
        $newProveedor->transferencia = 0;
        $newProveedor->activo = 1;
        $newProveedor->save();

        $newProveedor = new Proveedor();
        $newProveedor->nombre = "CARRASCO  MUÑOZ TAMARA";
        $newProveedor->rut = "13435227-2";
        $newProveedor->transferencia = 0;
        $newProveedor->activo = 1;
        $newProveedor->save();

        $newProveedor = new Proveedor();
        $newProveedor->nombre = "MARJOLAINE INDO BUSTAMANTE";
        $newProveedor->rut = "13451310-1";
        $newProveedor->transferencia = 0;
        $newProveedor->activo = 1;
        $newProveedor->save();

        $newProveedor = new Proveedor();
        $newProveedor->nombre = "RECART APFELBECK CLAUDIA";
        $newProveedor->rut = "13458458-0";
        $newProveedor->transferencia = 0;
        $newProveedor->activo = 1;
        $newProveedor->save();

        $newProveedor = new Proveedor();
        $newProveedor->nombre = "MORA JOFRE DANIEL";
        $newProveedor->rut = "13510479-5";
        $newProveedor->transferencia = 0;
        $newProveedor->activo = 1;
        $newProveedor->save();

        $newProveedor = new Proveedor();
        $newProveedor->nombre = "LUIS FUENTEALBA LEON";
        $newProveedor->rut = "13541590-1";
        $newProveedor->transferencia = 0;
        $newProveedor->activo = 1;
        $newProveedor->save();

        $newProveedor = new Proveedor();
        $newProveedor->nombre = "MAURICIO CHANG SALAZAR";
        $newProveedor->rut = "13602721-2";
        $newProveedor->transferencia = 0;
        $newProveedor->activo = 1;
        $newProveedor->save();

        $newProveedor = new Proveedor();
        $newProveedor->nombre = "ZYNTZYJA ESCOBAR FIDALGO";
        $newProveedor->rut = "13641046-6";
        $newProveedor->transferencia = 0;
        $newProveedor->activo = 1;
        $newProveedor->save();

        $newProveedor = new Proveedor();
        $newProveedor->nombre = "BULBOA FUENTES ALONSO";
        $newProveedor->rut = "13672903-9";
        $newProveedor->transferencia = 0;
        $newProveedor->activo = 1;
        $newProveedor->save();

        $newProveedor = new Proveedor();
        $newProveedor->nombre = "JUAN TRUJILLO SILVA";
        $newProveedor->rut = "13871394-6";
        $newProveedor->transferencia = 0;
        $newProveedor->activo = 1;
        $newProveedor->save();

        $newProveedor = new Proveedor();
        $newProveedor->nombre = "ALARCON ORTUZAR ALEJANDRO";
        $newProveedor->rut = "13891013-K";
        $newProveedor->transferencia = 0;
        $newProveedor->activo = 1;
        $newProveedor->save();

        $newProveedor = new Proveedor();
        $newProveedor->nombre = "ANDRADE JARA INGRID";
        $newProveedor->rut = "13924123-1";
        $newProveedor->transferencia = 0;
        $newProveedor->activo = 1;
        $newProveedor->save();

        $newProveedor = new Proveedor();
        $newProveedor->nombre = "MAGALY DEL CARMEN ROJAS CORTES";
        $newProveedor->rut = "13964232-5";
        $newProveedor->email = "MROJAS@TECNOCAM.CL";
        $newProveedor->transferencia = 0;
        $newProveedor->activo = 1;
        $newProveedor->save();

        $newProveedor = new Proveedor();
        $newProveedor->nombre = "JUAN MUÑOZ ARIAS";
        $newProveedor->rut = "14000747-1";
        $newProveedor->transferencia = 0;
        $newProveedor->activo = 1;
        $newProveedor->save();

        $newProveedor = new Proveedor();
        $newProveedor->nombre = "SHEILA EILEEN CABA QUEZADA";
        $newProveedor->rut = "14062050-5";
        $newProveedor->transferencia = 0;
        $newProveedor->activo = 1;
        $newProveedor->save();

        $newProveedor = new Proveedor();
        $newProveedor->nombre = "YARELA GOMEZ VERGARA";
        $newProveedor->rut = "14086698-9";
        $newProveedor->transferencia = 0;
        $newProveedor->activo = 1;
        $newProveedor->save();

        $newProveedor = new Proveedor();
        $newProveedor->nombre = "DE LA VEGA VALENZUELA CLAUDIO";
        $newProveedor->rut = "14120557-9";
        $newProveedor->transferencia = 0;
        $newProveedor->activo = 1;
        $newProveedor->save();

        $newProveedor = new Proveedor();
        $newProveedor->nombre = "DEMPSTER LETELIER RONALD";
        $newProveedor->rut = "14123295-9";
        $newProveedor->transferencia = 0;
        $newProveedor->activo = 1;
        $newProveedor->save();

        $newProveedor = new Proveedor();
        $newProveedor->nombre = "RICARDO CARVAJAL VEAS";
        $newProveedor->rut = "14133066-7";
        $newProveedor->transferencia = 0;
        $newProveedor->activo = 1;
        $newProveedor->save();

        $newProveedor = new Proveedor();
        $newProveedor->nombre = "GUAMAN ORTEGA RODRIGO";
        $newProveedor->rut = "14143073-4";
        $newProveedor->transferencia = 0;
        $newProveedor->activo = 1;
        $newProveedor->save();

        $newProveedor = new Proveedor();
        $newProveedor->nombre = "RICARDO ESCOBAR GOMEZ";
        $newProveedor->rut = "14165129-3";
        $newProveedor->transferencia = 0;
        $newProveedor->activo = 1;
        $newProveedor->save();

        $newProveedor = new Proveedor();
        $newProveedor->nombre = "CROVO VALLE EMIIA";
        $newProveedor->rut = "14207365-K";
        $newProveedor->transferencia = 0;
        $newProveedor->activo = 1;
        $newProveedor->save();

        $newProveedor = new Proveedor();
        $newProveedor->nombre = "JARA UNDA JOSE LUIS";
        $newProveedor->rut = "14212391-6";
        $newProveedor->transferencia = 0;
        $newProveedor->activo = 1;
        $newProveedor->save();

        $newProveedor = new Proveedor();
        $newProveedor->nombre = "MAURICIO SALAZAR CARRILLO";
        $newProveedor->rut = "14224520-5";
        $newProveedor->transferencia = 0;
        $newProveedor->activo = 1;
        $newProveedor->save();

        $newProveedor = new Proveedor();
        $newProveedor->nombre = "DAVID GALLARDO PEREZ";
        $newProveedor->rut = "14309201-1";
        $newProveedor->transferencia = 0;
        $newProveedor->activo = 1;
        $newProveedor->save();

        $newProveedor = new Proveedor();
        $newProveedor->nombre = "MUÑOZ MORALES ALEX";
        $newProveedor->rut = "14360549-3";
        $newProveedor->transferencia = 0;
        $newProveedor->activo = 1;
        $newProveedor->save();

        $newProveedor = new Proveedor();
        $newProveedor->nombre = "SERGIO ANDRE LEGUA KOC";
        $newProveedor->rut = "14525229-6";
        $newProveedor->transferencia = 0;
        $newProveedor->activo = 1;
        $newProveedor->save();

        $newProveedor = new Proveedor();
        $newProveedor->nombre = "GAETE COVARRUBIAS JUAN";
        $newProveedor->rut = "14572019-2";
        $newProveedor->transferencia = 0;
        $newProveedor->activo = 1;
        $newProveedor->save();

        $newProveedor = new Proveedor();
        $newProveedor->nombre = "SEBASTIAN LUIS LAYERA RAMOS";
        $newProveedor->rut = "14572754-5";
        $newProveedor->transferencia = 0;
        $newProveedor->activo = 1;
        $newProveedor->save();

        $newProveedor = new Proveedor();
        $newProveedor->nombre = "TOMASA MARTINEZ OTANO";
        $newProveedor->rut = "14673674-2";
        $newProveedor->transferencia = 0;
        $newProveedor->activo = 1;
        $newProveedor->save();

        $newProveedor = new Proveedor();
        $newProveedor->nombre = "KJERSTI NES";
        $newProveedor->rut = "14686672-7";
        $newProveedor->transferencia = 0;
        $newProveedor->activo = 1;
        $newProveedor->save();

        $newProveedor = new Proveedor();
        $newProveedor->nombre = "AGUIRRE TITUANA RICHAR";
        $newProveedor->rut = "14715281-7";
        $newProveedor->transferencia = 0;
        $newProveedor->activo = 1;
        $newProveedor->save();

        $newProveedor = new Proveedor();
        $newProveedor->nombre = "ESTEBAN VASQUEZ SEMPERTEGUI";
        $newProveedor->rut = "14725159-9";
        $newProveedor->transferencia = 0;
        $newProveedor->activo = 1;
        $newProveedor->save();

        $newProveedor = new Proveedor();
        $newProveedor->nombre = "ZOILA SLTOS GILER";
        $newProveedor->rut = "14739877-8";
        $newProveedor->transferencia = 0;
        $newProveedor->activo = 1;
        $newProveedor->save();

        $newProveedor = new Proveedor();
        $newProveedor->nombre = "KARLA GOMEZ RAMOS";
        $newProveedor->rut = "15008151-3";
        $newProveedor->transferencia = 0;
        $newProveedor->activo = 1;
        $newProveedor->save();

        $newProveedor = new Proveedor();
        $newProveedor->nombre = "PENA MANUBENS MARIA FERNANDA";
        $newProveedor->rut = "15070207-0";
        $newProveedor->transferencia = 0;
        $newProveedor->activo = 1;
        $newProveedor->save();

        $newProveedor = new Proveedor();
        $newProveedor->nombre = "PEÑA MANUBENS MARIA";
        $newProveedor->rut = "15070207-8";
        $newProveedor->transferencia = 0;
        $newProveedor->activo = 1;
        $newProveedor->save();

        $newProveedor = new Proveedor();
        $newProveedor->nombre = "ARELLANO IBARRA MARIO";
        $newProveedor->rut = "15126325-9";
        $newProveedor->transferencia = 0;
        $newProveedor->activo = 1;
        $newProveedor->save();

        $newProveedor = new Proveedor();
        $newProveedor->nombre = "ZAMBRANO GUTIERREZ NATALIA";
        $newProveedor->rut = "15181845-5";
        $newProveedor->transferencia = 0;
        $newProveedor->activo = 1;
        $newProveedor->save();

        $newProveedor = new Proveedor();
        $newProveedor->nombre = "VALLEJOS JIMENEZ GUSTAVO";
        $newProveedor->rut = "15194666-6";
        $newProveedor->transferencia = 0;
        $newProveedor->activo = 1;
        $newProveedor->save();

        $newProveedor = new Proveedor();
        $newProveedor->nombre = "TORRES FLORES PEDRO";
        $newProveedor->rut = "15206712-7";
        $newProveedor->transferencia = 0;
        $newProveedor->activo = 1;
        $newProveedor->save();

        $newProveedor = new Proveedor();
        $newProveedor->nombre = "CHILE TECNOLOGICO";
        $newProveedor->rut = "15221815-K";
        $newProveedor->email = "ASISTENTECOMERCIAL@CHILETECNOLOGICO.CL";
        $newProveedor->transferencia = 0;
        $newProveedor->activo = 1;
        $newProveedor->save();

        $newProveedor = new Proveedor();
        $newProveedor->nombre = "BARBA LILLO FABIOLA";
        $newProveedor->rut = "15314132-0";
        $newProveedor->transferencia = 0;
        $newProveedor->activo = 1;
        $newProveedor->save();

        $newProveedor = new Proveedor();
        $newProveedor->nombre = "FRANCISCO OLGUIN COLLAO";
        $newProveedor->rut = "15330589-7";
        $newProveedor->transferencia = 0;
        $newProveedor->activo = 1;
        $newProveedor->save();

        $newProveedor = new Proveedor();
        $newProveedor->nombre = "SEPULVEDA JAQUI LUIS FELIPE";
        $newProveedor->rut = "15333195-2";
        $newProveedor->transferencia = 0;
        $newProveedor->activo = 1;
        $newProveedor->save();

        $newProveedor = new Proveedor();
        $newProveedor->nombre = "PASTEN CASTRO GONZALO";
        $newProveedor->rut = "15340362-7";
        $newProveedor->transferencia = 0;
        $newProveedor->activo = 1;
        $newProveedor->save();

        $newProveedor = new Proveedor();
        $newProveedor->nombre = "PEDRO CARRASCO GALLEGUILLOS";
        $newProveedor->rut = "15344942-2";
        $newProveedor->transferencia = 0;
        $newProveedor->activo = 1;
        $newProveedor->save();

        $newProveedor = new Proveedor();
        $newProveedor->nombre = "LEON MALDONADO NILDA";
        $newProveedor->rut = "15354806-4";
        $newProveedor->transferencia = 0;
        $newProveedor->activo = 1;
        $newProveedor->save();

        $newProveedor = new Proveedor();
        $newProveedor->nombre = "PAULINA NUÑEZ FIGUEROA";
        $newProveedor->rut = "15358924-0";
        $newProveedor->transferencia = 0;
        $newProveedor->activo = 1;
        $newProveedor->save();

        $newProveedor = new Proveedor();
        $newProveedor->nombre = "MARDONES PARGA MARCELO";
        $newProveedor->rut = "15363833-0";
        $newProveedor->transferencia = 0;
        $newProveedor->activo = 1;
        $newProveedor->save();

        $newProveedor = new Proveedor();
        $newProveedor->nombre = "DANAE UBILLA PAREJA";
        $newProveedor->rut = "15374217-0";
        $newProveedor->transferencia = 0;
        $newProveedor->activo = 1;
        $newProveedor->save();

        $newProveedor = new Proveedor();
        $newProveedor->nombre = "JAVIERA ROSSI GUZMAN";
        $newProveedor->rut = "15374552-8";
        $newProveedor->transferencia = 0;
        $newProveedor->activo = 1;
        $newProveedor->save();

        $newProveedor = new Proveedor();
        $newProveedor->nombre = "RIVAS QUEDADA MAURICIO";
        $newProveedor->rut = "15378689-5";
        $newProveedor->transferencia = 0;
        $newProveedor->activo = 1;
        $newProveedor->save();

        $newProveedor = new Proveedor();
        $newProveedor->nombre = "GARCIA CAMPO MARIA PILAR";
        $newProveedor->rut = "15382133-K";
        $newProveedor->transferencia = 0;
        $newProveedor->activo = 1;
        $newProveedor->save();

        $newProveedor = new Proveedor();
        $newProveedor->nombre = "CATALINA LARRAIN SOLER";
        $newProveedor->rut = "15382164-K";
        $newProveedor->transferencia = 0;
        $newProveedor->activo = 1;
        $newProveedor->save();

        $newProveedor = new Proveedor();
        $newProveedor->nombre = "PHILIPPE SCHLESNGER MENDEL";
        $newProveedor->rut = "15382335-9";
        $newProveedor->transferencia = 0;
        $newProveedor->activo = 1;
        $newProveedor->save();

        $newProveedor = new Proveedor();
        $newProveedor->nombre = "VALLADARES ARELLANO SEBATIAN";
        $newProveedor->rut = "15383055-K";
        $newProveedor->transferencia = 0;
        $newProveedor->activo = 1;
        $newProveedor->save();

        $newProveedor = new Proveedor();
        $newProveedor->nombre = "FERNANDEZ ROJAS DIANA";
        $newProveedor->rut = "15434119-6";
        $newProveedor->transferencia = 0;
        $newProveedor->activo = 1;
        $newProveedor->save();

        $newProveedor = new Proveedor();
        $newProveedor->nombre = "ELISANETH EMILIA CORTEZ MUÑOZ";
        $newProveedor->rut = "15457235-K";
        $newProveedor->email = "ELIMCORTEZ@HOTMAIL.COM";
        $newProveedor->transferencia = 0;
        $newProveedor->activo = 1;
        $newProveedor->save();

        $newProveedor = new Proveedor();
        $newProveedor->nombre = "ALENA GAJARDO KONG";
        $newProveedor->rut = "15513995-1";
        $newProveedor->transferencia = 0;
        $newProveedor->activo = 1;
        $newProveedor->save();

        $newProveedor = new Proveedor();
        $newProveedor->nombre = "OMAR ORELLANA ESPINOZA";
        $newProveedor->rut = "15565518-6";
        $newProveedor->transferencia = 0;
        $newProveedor->activo = 1;
        $newProveedor->save();

        $newProveedor = new Proveedor();
        $newProveedor->nombre = "JOHANA COFRE JARA";
        $newProveedor->rut = "15569041-0";
        $newProveedor->transferencia = 0;
        $newProveedor->activo = 1;
        $newProveedor->save();

        $newProveedor = new Proveedor();
        $newProveedor->nombre = "CERDA BARGETTO JOAQUIN";
        $newProveedor->rut = "15580608-7";
        $newProveedor->transferencia = 0;
        $newProveedor->activo = 1;
        $newProveedor->save();

        $newProveedor = new Proveedor();
        $newProveedor->nombre = "RUTH HELENA SEGUEL CARTES";
        $newProveedor->rut = "15592049-1";
        $newProveedor->transferencia = 0;
        $newProveedor->activo = 1;
        $newProveedor->save();

        $newProveedor = new Proveedor();
        $newProveedor->nombre = "DOMINGUEZ PARRA MARIA";
        $newProveedor->rut = "15592105-6";
        $newProveedor->transferencia = 0;
        $newProveedor->activo = 1;
        $newProveedor->save();

        $newProveedor = new Proveedor();
        $newProveedor->nombre = "LEONARDO BECERRA RAMIREZ";
        $newProveedor->rut = "15638526-3";
        $newProveedor->transferencia = 0;
        $newProveedor->activo = 1;
        $newProveedor->save();

        $newProveedor = new Proveedor();
        $newProveedor->nombre = "CRISTIAN WILKENS ROMO";
        $newProveedor->rut = "15640479-9";
        $newProveedor->transferencia = 0;
        $newProveedor->activo = 1;
        $newProveedor->save();

        $newProveedor = new Proveedor();
        $newProveedor->nombre = "GERMAN GOMEZ ALVAREZ";
        $newProveedor->rut = "15645597-0";
        $newProveedor->transferencia = 0;
        $newProveedor->activo = 1;
        $newProveedor->save();

        $newProveedor = new Proveedor();
        $newProveedor->nombre = "RAMIREZ MACHUCA GERMAN";
        $newProveedor->rut = "15679671-9";
        $newProveedor->transferencia = 0;
        $newProveedor->activo = 1;
        $newProveedor->save();

        $newProveedor = new Proveedor();
        $newProveedor->nombre = "ANDREA CAROLINA URRA FUENZALIDA";
        $newProveedor->rut = "15698616-K";
        $newProveedor->transferencia = 0;
        $newProveedor->activo = 1;
        $newProveedor->save();

        $newProveedor = new Proveedor();
        $newProveedor->nombre = "GUILLERMO AVILA LOPEZ";
        $newProveedor->rut = "15709798-9";
        $newProveedor->transferencia = 0;
        $newProveedor->activo = 1;
        $newProveedor->save();

        $newProveedor = new Proveedor();
        $newProveedor->nombre = "CHRISTIAN MARIANO DONOSO ALBORNOZ";
        $newProveedor->rut = "15715485-0";
        $newProveedor->transferencia = 0;
        $newProveedor->activo = 1;
        $newProveedor->save();

        $newProveedor = new Proveedor();
        $newProveedor->nombre = "GALVAN ESCOBAR GONZALO";
        $newProveedor->rut = "15716433-3";
        $newProveedor->transferencia = 0;
        $newProveedor->activo = 1;
        $newProveedor->save();

        $newProveedor = new Proveedor();
        $newProveedor->nombre = "NOGUEIRA ESPINOZA MARCELO";
        $newProveedor->rut = "15726045-6";
        $newProveedor->transferencia = 0;
        $newProveedor->activo = 1;
        $newProveedor->save();

        $newProveedor = new Proveedor();
        $newProveedor->nombre = "CAMPOS  MORALES XIMENA";
        $newProveedor->rut = "15726192-4";
        $newProveedor->transferencia = 0;
        $newProveedor->activo = 1;
        $newProveedor->save();

        $newProveedor = new Proveedor();
        $newProveedor->nombre = "GONZALEZ  ROJAS FELIPE";
        $newProveedor->rut = "15726422-2";
        $newProveedor->transferencia = 0;
        $newProveedor->activo = 1;
        $newProveedor->save();

        $newProveedor = new Proveedor();
        $newProveedor->nombre = "BRAVO PINARES OSCAR";
        $newProveedor->rut = "15747785-4";
        $newProveedor->transferencia = 0;
        $newProveedor->activo = 1;
        $newProveedor->save();

        $newProveedor = new Proveedor();
        $newProveedor->nombre = "UBILLA SAEZ FELIPE";
        $newProveedor->rut = "15757006-4";
        $newProveedor->transferencia = 0;
        $newProveedor->activo = 1;
        $newProveedor->save();

        $newProveedor = new Proveedor();
        $newProveedor->nombre = "MARIOLI VARAS LEIVA";
        $newProveedor->rut = "15760837-1";
        $newProveedor->transferencia = 0;
        $newProveedor->activo = 1;
        $newProveedor->save();

        $newProveedor = new Proveedor();
        $newProveedor->nombre = "FARIAS VALENZUELA HETOR";
        $newProveedor->rut = "15774659-6";
        $newProveedor->transferencia = 0;
        $newProveedor->activo = 1;
        $newProveedor->save();

        $newProveedor = new Proveedor();
        $newProveedor->nombre = "DAVID BARRERO VIRONNEAU";
        $newProveedor->rut = "15783805-9";
        $newProveedor->transferencia = 0;
        $newProveedor->activo = 1;
        $newProveedor->save();

        $newProveedor = new Proveedor();
        $newProveedor->nombre = "NESTOR ROJAS RIOS";
        $newProveedor->rut = "15805700-K";
        $newProveedor->transferencia = 0;
        $newProveedor->activo = 1;
        $newProveedor->save();

        $newProveedor = new Proveedor();
        $newProveedor->nombre = "FUENTES MARQUEZ CLAUDIO";
        $newProveedor->rut = "15813457-8";
        $newProveedor->transferencia = 0;
        $newProveedor->activo = 1;
        $newProveedor->save();

        $newProveedor = new Proveedor();
        $newProveedor->nombre = "JULIAN SEPULVEDA TELLERIAS";
        $newProveedor->rut = "15821553-5";
        $newProveedor->transferencia = 0;
        $newProveedor->activo = 1;
        $newProveedor->save();

        $newProveedor = new Proveedor();
        $newProveedor->nombre = "BRAVO BRAVO FELIPE";
        $newProveedor->rut = "15837808-6";
        $newProveedor->transferencia = 0;
        $newProveedor->activo = 1;
        $newProveedor->save();

        $newProveedor = new Proveedor();
        $newProveedor->nombre = "LOPEZ BARRENECHEA PAULINA";
        $newProveedor->rut = "15878315-0";
        $newProveedor->transferencia = 0;
        $newProveedor->activo = 1;
        $newProveedor->save();

        $newProveedor = new Proveedor();
        $newProveedor->nombre = "KARLA VERGARA ROBLES";
        $newProveedor->rut = "15940474-9";
        $newProveedor->transferencia = 0;
        $newProveedor->activo = 1;
        $newProveedor->save();

        $newProveedor = new Proveedor();
        $newProveedor->nombre = "SILVA PANTOJA MONICA";
        $newProveedor->rut = "15951082-4";
        $newProveedor->transferencia = 0;
        $newProveedor->activo = 1;
        $newProveedor->save();

        $newProveedor = new Proveedor();
        $newProveedor->nombre = "PAMELA DEL CARMEN BRAVO MEIER";
        $newProveedor->rut = "15959992-2";
        $newProveedor->transferencia = 0;
        $newProveedor->activo = 1;
        $newProveedor->save();

        $newProveedor = new Proveedor();
        $newProveedor->nombre = "AYACH OLIVARES PATRICIA";
        $newProveedor->rut = "15960510-8";
        $newProveedor->transferencia = 0;
        $newProveedor->activo = 1;
        $newProveedor->save();

        $newProveedor = new Proveedor();
        $newProveedor->nombre = "FELIPE ZUMAETA VALENZUELA";
        $newProveedor->rut = "16015882-4";
        $newProveedor->transferencia = 0;
        $newProveedor->activo = 1;
        $newProveedor->save();

        $newProveedor = new Proveedor();
        $newProveedor->nombre = "ESPINOZA FUENTES MACARENA";
        $newProveedor->rut = "16020949-6";
        $newProveedor->transferencia = 0;
        $newProveedor->activo = 1;
        $newProveedor->save();

        $newProveedor = new Proveedor();
        $newProveedor->nombre = "JOSEFINA MEJIAS SMITH";
        $newProveedor->rut = "16096690-4";
        $newProveedor->transferencia = 0;
        $newProveedor->activo = 1;
        $newProveedor->save();

        $newProveedor = new Proveedor();
        $newProveedor->nombre = "MUÑOZ FUENTES OSCAR";
        $newProveedor->rut = "16122226-7";
        $newProveedor->transferencia = 0;
        $newProveedor->activo = 1;
        $newProveedor->save();

        $newProveedor = new Proveedor();
        $newProveedor->nombre = "ESPINOZA OVALLE MARIA LUISA";
        $newProveedor->rut = "16155150-3";
        $newProveedor->transferencia = 0;
        $newProveedor->activo = 1;
        $newProveedor->save();

        $newProveedor = new Proveedor();
        $newProveedor->nombre = "LORENA BASTIDAS LEAL";
        $newProveedor->rut = "16160040-7";
        $newProveedor->transferencia = 0;
        $newProveedor->activo = 1;
        $newProveedor->save();

        $newProveedor = new Proveedor();
        $newProveedor->nombre = "PRZYBYSZEWSKI JOPIA INGRID";
        $newProveedor->rut = "16203666-1";
        $newProveedor->transferencia = 0;
        $newProveedor->activo = 1;
        $newProveedor->save();

        $newProveedor = new Proveedor();
        $newProveedor->nombre = "HENRIQUEZ PARADA EDUARDO";
        $newProveedor->rut = "16210691-0";
        $newProveedor->transferencia = 0;
        $newProveedor->activo = 1;
        $newProveedor->save();

        $newProveedor = new Proveedor();
        $newProveedor->nombre = "GONZALO BRICEÑO MAYORGA";
        $newProveedor->rut = "16224345-4";
        $newProveedor->transferencia = 0;
        $newProveedor->activo = 1;
        $newProveedor->save();

        $newProveedor = new Proveedor();
        $newProveedor->nombre = "TORRES ENCINA JOSE";
        $newProveedor->rut = "16245087-5";
        $newProveedor->transferencia = 0;
        $newProveedor->activo = 1;
        $newProveedor->save();

        $newProveedor = new Proveedor();
        $newProveedor->nombre = "VEGA SALAS JAVIER";
        $newProveedor->rut = "16301769-5";
        $newProveedor->transferencia = 0;
        $newProveedor->activo = 1;
        $newProveedor->save();

        $newProveedor = new Proveedor();
        $newProveedor->nombre = "OÑATE FUENTES GINGER";
        $newProveedor->rut = "16327952-5";
        $newProveedor->transferencia = 0;
        $newProveedor->activo = 1;
        $newProveedor->save();

        $newProveedor = new Proveedor();
        $newProveedor->nombre = "MIRANDA SILVA DANIEL";
        $newProveedor->rut = "16346325-3";
        $newProveedor->transferencia = 0;
        $newProveedor->activo = 1;
        $newProveedor->save();

        $newProveedor = new Proveedor();
        $newProveedor->nombre = "NATALIA CONTRERAS BOCIC";
        $newProveedor->rut = "16358074-8";
        $newProveedor->transferencia = 0;
        $newProveedor->activo = 1;
        $newProveedor->save();

        $newProveedor = new Proveedor();
        $newProveedor->nombre = "JIMENEZ GUZMAN CLAUDIA";
        $newProveedor->rut = "16367772-5";
        $newProveedor->transferencia = 0;
        $newProveedor->activo = 1;
        $newProveedor->save();

        $newProveedor = new Proveedor();
        $newProveedor->nombre = "PAZ VALENTINA AHUMADA DROGUETT";
        $newProveedor->rut = "16369974-5";
        $newProveedor->transferencia = 0;
        $newProveedor->activo = 1;
        $newProveedor->save();

        $newProveedor = new Proveedor();
        $newProveedor->nombre = "PIZARRO LARENAS NICOLAS";
        $newProveedor->rut = "16384525-3";
        $newProveedor->transferencia = 0;
        $newProveedor->activo = 1;
        $newProveedor->save();

        $newProveedor = new Proveedor();
        $newProveedor->nombre = "ARAVENA CASTRO OSCAR";
        $newProveedor->rut = "16427563-9";
        $newProveedor->transferencia = 0;
        $newProveedor->activo = 1;
        $newProveedor->save();

        $newProveedor = new Proveedor();
        $newProveedor->nombre = "DONOSO  ADRIASOLA  CARLOS";
        $newProveedor->rut = "16430091-9";
        $newProveedor->transferencia = 0;
        $newProveedor->activo = 1;
        $newProveedor->save();

        $newProveedor = new Proveedor();
        $newProveedor->nombre = "DYNABIO S.A.";
        $newProveedor->rut = "16460001-7";
        $newProveedor->transferencia = 0;
        $newProveedor->activo = 1;
        $newProveedor->save();

        $newProveedor = new Proveedor();
        $newProveedor->nombre = "SANDOVAL VALENZUELA ANDRESA";
        $newProveedor->rut = "16473688-1";
        $newProveedor->transferencia = 0;
        $newProveedor->activo = 1;
        $newProveedor->save();

        $newProveedor = new Proveedor();
        $newProveedor->nombre = "CRISTIAN MORALES SIERRA";
        $newProveedor->rut = "16489325-1";
        $newProveedor->transferencia = 0;
        $newProveedor->activo = 1;
        $newProveedor->save();

        $newProveedor = new Proveedor();
        $newProveedor->nombre = "JIMENEZ GUZMAN CLAUDIA";
        $newProveedor->rut = "16489488-6";
        $newProveedor->transferencia = 0;
        $newProveedor->activo = 1;
        $newProveedor->save();

        $newProveedor = new Proveedor();
        $newProveedor->nombre = "LOPEZ CACERES MIGUEL";
        $newProveedor->rut = "16517570-0";
        $newProveedor->transferencia = 0;
        $newProveedor->activo = 1;
        $newProveedor->save();

        $newProveedor = new Proveedor();
        $newProveedor->nombre = "HAUWAY ISLA ERNESTO";
        $newProveedor->rut = "16526532-7";
        $newProveedor->transferencia = 0;
        $newProveedor->activo = 1;
        $newProveedor->save();

        $newProveedor = new Proveedor();
        $newProveedor->nombre = "VARAS HERRERA CRISTIAN";
        $newProveedor->rut = "16570872-5";
        $newProveedor->transferencia = 0;
        $newProveedor->activo = 1;
        $newProveedor->save();

        $newProveedor = new Proveedor();
        $newProveedor->nombre = "MACHIAVELLO THEODULOZ STFEANO";
        $newProveedor->rut = "16593273-0";
        $newProveedor->transferencia = 0;
        $newProveedor->activo = 1;
        $newProveedor->save();

        $newProveedor = new Proveedor();
        $newProveedor->nombre = "JOSE FERNADEZ URETA";
        $newProveedor->rut = "16606514-3";
        $newProveedor->transferencia = 0;
        $newProveedor->activo = 1;
        $newProveedor->save();

        $newProveedor = new Proveedor();
        $newProveedor->nombre = "VERA GIGLIO VALENTINA  PAZ";
        $newProveedor->rut = "16607163-1";
        $newProveedor->transferencia = 0;
        $newProveedor->activo = 1;
        $newProveedor->save();

        $newProveedor = new Proveedor();
        $newProveedor->nombre = "MUNDACA STUARDO EDUARDO";
        $newProveedor->rut = "16611782-8";
        $newProveedor->transferencia = 0;
        $newProveedor->activo = 1;
        $newProveedor->save();

        $newProveedor = new Proveedor();
        $newProveedor->nombre = "CHACON URREJOLA CARLOS";
        $newProveedor->rut = "16647263-6";
        $newProveedor->transferencia = 0;
        $newProveedor->activo = 1;
        $newProveedor->save();

        $newProveedor = new Proveedor();
        $newProveedor->nombre = "CARLOS ROBLES MORALES";
        $newProveedor->rut = "16650743-K";
        $newProveedor->transferencia = 0;
        $newProveedor->activo = 1;
        $newProveedor->save();

        $newProveedor = new Proveedor();
        $newProveedor->nombre = "LOBO VILLARROEL NELSON";
        $newProveedor->rut = "16651883-0";
        $newProveedor->transferencia = 0;
        $newProveedor->activo = 1;
        $newProveedor->save();

        $newProveedor = new Proveedor();
        $newProveedor->nombre = "ALONSO BLANCH ZELADA";
        $newProveedor->rut = "16660594-6";
        $newProveedor->transferencia = 0;
        $newProveedor->activo = 1;
        $newProveedor->save();

        $newProveedor = new Proveedor();
        $newProveedor->nombre = "ANTONIO ROBERT TOCORNAL";
        $newProveedor->rut = "16660757-4";
        $newProveedor->transferencia = 0;
        $newProveedor->activo = 1;
        $newProveedor->save();

        $newProveedor = new Proveedor();
        $newProveedor->nombre = "DANIELA TORRES LARA";
        $newProveedor->rut = "16718848-6";
        $newProveedor->transferencia = 0;
        $newProveedor->activo = 1;
        $newProveedor->save();

        $newProveedor = new Proveedor();
        $newProveedor->nombre = "NAVARRETE MEDINA WILFREDO";
        $newProveedor->rut = "16783035-8";
        $newProveedor->transferencia = 0;
        $newProveedor->activo = 1;
        $newProveedor->save();

        $newProveedor = new Proveedor();
        $newProveedor->nombre = "KAROL NINOSKA GONZALEZ ROMO";
        $newProveedor->rut = "16814676-0";
        $newProveedor->transferencia = 0;
        $newProveedor->activo = 1;
        $newProveedor->save();

        $newProveedor = new Proveedor();
        $newProveedor->nombre = "COUCHOT SILVA BENEDICTO";
        $newProveedor->rut = "16856165-2";
        $newProveedor->transferencia = 0;
        $newProveedor->activo = 1;
        $newProveedor->save();

        $newProveedor = new Proveedor();
        $newProveedor->nombre = "GEORGECARREÑO CLAUDIO";
        $newProveedor->rut = "16882091-7";
        $newProveedor->transferencia = 0;
        $newProveedor->activo = 1;
        $newProveedor->save();

        $newProveedor = new Proveedor();
        $newProveedor->nombre = "JIMENEZ GUZMAN CLAUDIA";
        $newProveedor->rut = "16894488-6";
        $newProveedor->transferencia = 0;
        $newProveedor->activo = 1;
        $newProveedor->save();

        $newProveedor = new Proveedor();
        $newProveedor->nombre = "PALOMA CASSANDRA RUIZ LOPEZ";
        $newProveedor->rut = "16911679-2";
        $newProveedor->transferencia = 0;
        $newProveedor->activo = 1;
        $newProveedor->save();

        $newProveedor = new Proveedor();
        $newProveedor->nombre = "MISAED SANCHEZ SOTO";
        $newProveedor->rut = "16914922-4";
        $newProveedor->transferencia = 0;
        $newProveedor->activo = 1;
        $newProveedor->save();

        $newProveedor = new Proveedor();
        $newProveedor->nombre = "SIMON TOGNARELLI SANTIAGO";
        $newProveedor->rut = "16939892-5";
        $newProveedor->transferencia = 0;
        $newProveedor->activo = 1;
        $newProveedor->save();

        $newProveedor = new Proveedor();
        $newProveedor->nombre = "KLEIN SANDOVAL MACARENA";
        $newProveedor->rut = "16994610-8";
        $newProveedor->transferencia = 0;
        $newProveedor->activo = 1;
        $newProveedor->save();

        $newProveedor = new Proveedor();
        $newProveedor->nombre = "CASTRO GAETE NICOLAS";
        $newProveedor->rut = "17025854-1";
        $newProveedor->transferencia = 0;
        $newProveedor->activo = 1;
        $newProveedor->save();

        $newProveedor = new Proveedor();
        $newProveedor->nombre = "GERALDINE WELLS NEGRON";
        $newProveedor->rut = "17043573-7";
        $newProveedor->transferencia = 0;
        $newProveedor->activo = 1;
        $newProveedor->save();

        $newProveedor = new Proveedor();
        $newProveedor->nombre = "DANIEL LAZO PRADO";
        $newProveedor->rut = "17083472-0";
        $newProveedor->transferencia = 0;
        $newProveedor->activo = 1;
        $newProveedor->save();

        $newProveedor = new Proveedor();
        $newProveedor->nombre = "MARIA PONCE GARCIA";
        $newProveedor->rut = "17085817-4";
        $newProveedor->transferencia = 0;
        $newProveedor->activo = 1;
        $newProveedor->save();

        $newProveedor = new Proveedor();
        $newProveedor->nombre = "LLANQUILEO MEYER ANDRES";
        $newProveedor->rut = "17149957-7";
        $newProveedor->transferencia = 0;
        $newProveedor->activo = 1;
        $newProveedor->save();

        $newProveedor = new Proveedor();
        $newProveedor->nombre = "VICENCIO SURA ISMAEL";
        $newProveedor->rut = "17222222-6";
        $newProveedor->transferencia = 0;
        $newProveedor->activo = 1;
        $newProveedor->save();

        $newProveedor = new Proveedor();
        $newProveedor->nombre = "PELAEZ GARCIA NATALHIE";
        $newProveedor->rut = "17241311-0";
        $newProveedor->transferencia = 0;
        $newProveedor->activo = 1;
        $newProveedor->save();

        $newProveedor = new Proveedor();
        $newProveedor->nombre = "CRISTIAN PARADA GODOY";
        $newProveedor->rut = "17244134-3";
        $newProveedor->transferencia = 0;
        $newProveedor->activo = 1;
        $newProveedor->save();

        $newProveedor = new Proveedor();
        $newProveedor->nombre = "ARIADNA MARUZZELLA VIDELA VARGAS";
        $newProveedor->rut = "17268540-4";
        $newProveedor->transferencia = 0;
        $newProveedor->activo = 1;
        $newProveedor->save();

        $newProveedor = new Proveedor();
        $newProveedor->nombre = "MARTA TAPIA ARAYA";
        $newProveedor->rut = "17295485-5";
        $newProveedor->transferencia = 0;
        $newProveedor->activo = 1;
        $newProveedor->save();

        $newProveedor = new Proveedor();
        $newProveedor->nombre = "GABRIELA SOFIA LIZANA CIUDAD";
        $newProveedor->rut = "17309910-K";
        $newProveedor->transferencia = 0;
        $newProveedor->activo = 1;
        $newProveedor->save();

        $newProveedor = new Proveedor();
        $newProveedor->nombre = "VERONICA ANDREA PEREZ HIDALGO";
        $newProveedor->rut = "17345527-5";
        $newProveedor->transferencia = 0;
        $newProveedor->activo = 1;
        $newProveedor->save();

        $newProveedor = new Proveedor();
        $newProveedor->nombre = "IVONNE PEÑA ALVAREZ";
        $newProveedor->rut = "17364286-5";
        $newProveedor->transferencia = 0;
        $newProveedor->activo = 1;
        $newProveedor->save();

        $newProveedor = new Proveedor();
        $newProveedor->nombre = "FRANCISCA DEL PILAR GONZALEZ SERRANO";
        $newProveedor->rut = "17391580-2";
        $newProveedor->transferencia = 0;
        $newProveedor->activo = 1;
        $newProveedor->save();

        $newProveedor = new Proveedor();
        $newProveedor->nombre = "IVONNE CINAHI ARANEDA NAVEAS";
        $newProveedor->rut = "17485370-3";
        $newProveedor->transferencia = 0;
        $newProveedor->activo = 1;
        $newProveedor->save();

        $newProveedor = new Proveedor();
        $newProveedor->nombre = "YUMHA ESTAY DANIEL";
        $newProveedor->rut = "17596762-1";
        $newProveedor->transferencia = 0;
        $newProveedor->activo = 1;
        $newProveedor->save();

        $newProveedor = new Proveedor();
        $newProveedor->nombre = "SAN MARTIN MEZA EVELYN";
        $newProveedor->rut = "17601052-5";
        $newProveedor->transferencia = 0;
        $newProveedor->activo = 1;
        $newProveedor->save();

        $newProveedor = new Proveedor();
        $newProveedor->nombre = "CORRALES BRAVO MARIA JOSE";
        $newProveedor->rut = "17602144-6";
        $newProveedor->transferencia = 0;
        $newProveedor->activo = 1;
        $newProveedor->save();

        $newProveedor = new Proveedor();
        $newProveedor->nombre = "FUENTES GARCES CAMILA";
        $newProveedor->rut = "17622831-8";
        $newProveedor->transferencia = 0;
        $newProveedor->activo = 1;
        $newProveedor->save();

        $newProveedor = new Proveedor();
        $newProveedor->nombre = "CARLOS CORDERO LOPEZ";
        $newProveedor->rut = "17645592-6";
        $newProveedor->transferencia = 0;
        $newProveedor->activo = 1;
        $newProveedor->save();

        $newProveedor = new Proveedor();
        $newProveedor->nombre = "MILLACURA MENESES CAROLINA";
        $newProveedor->rut = "17747002-3";
        $newProveedor->transferencia = 0;
        $newProveedor->activo = 1;
        $newProveedor->save();

        $newProveedor = new Proveedor();
        $newProveedor->nombre = "DENISE VEGA ROJAS";
        $newProveedor->rut = "17797990-2";
        $newProveedor->transferencia = 0;
        $newProveedor->activo = 1;
        $newProveedor->save();

        $newProveedor = new Proveedor();
        $newProveedor->nombre = "HECTOR JARA ORTEGA";
        $newProveedor->rut = "17875775-K";
        $newProveedor->transferencia = 0;
        $newProveedor->activo = 1;
        $newProveedor->save();

        $newProveedor = new Proveedor();
        $newProveedor->nombre = "OLIVA GUERRERO CAROLINA";
        $newProveedor->rut = "17961907-5";
        $newProveedor->transferencia = 0;
        $newProveedor->activo = 1;
        $newProveedor->save();

        $newProveedor = new Proveedor();
        $newProveedor->nombre = "GALLARDO  PAREDES DIEGO";
        $newProveedor->rut = "18184877-4";
        $newProveedor->transferencia = 0;
        $newProveedor->activo = 1;
        $newProveedor->save();

        $newProveedor = new Proveedor();
        $newProveedor->nombre = "DANTE RIVERA ORTIZ";
        $newProveedor->rut = "18906874-3";
        $newProveedor->transferencia = 0;
        $newProveedor->activo = 1;
        $newProveedor->save();

        $newProveedor = new Proveedor();
        $newProveedor->nombre = "RETAMAL EVANS JOSE";
        $newProveedor->rut = "18954323-9";
        $newProveedor->transferencia = 0;
        $newProveedor->activo = 1;
        $newProveedor->save();

        $newProveedor = new Proveedor();
        $newProveedor->nombre = "CAZAR PACHECO FERNANDO";
        $newProveedor->rut = "21325770-6";
        $newProveedor->transferencia = 0;
        $newProveedor->activo = 1;
        $newProveedor->save();

        $newProveedor = new Proveedor();
        $newProveedor->nombre = "MERA REYES SANDRA";
        $newProveedor->rut = "21608134-K";
        $newProveedor->transferencia = 0;
        $newProveedor->activo = 1;
        $newProveedor->save();

        $newProveedor = new Proveedor();
        $newProveedor->nombre = "CASTELLANOS MORA CARLOS";
        $newProveedor->rut = "21661460-7";
        $newProveedor->transferencia = 0;
        $newProveedor->activo = 1;
        $newProveedor->save();

        $newProveedor = new Proveedor();
        $newProveedor->nombre = "MORAN ESPINOZA CARLOS";
        $newProveedor->rut = "22146944-5";
        $newProveedor->transferencia = 0;
        $newProveedor->activo = 1;
        $newProveedor->save();

        $newProveedor = new Proveedor();
        $newProveedor->nombre = "ORTEGA MOSQUEIRA JUAN";
        $newProveedor->rut = "22409582-1";
        $newProveedor->transferencia = 0;
        $newProveedor->activo = 1;
        $newProveedor->save();

        $newProveedor = new Proveedor();
        $newProveedor->nombre = "PINEDA PINEDO JAVIER";
        $newProveedor->rut = "22554359-3";
        $newProveedor->transferencia = 0;
        $newProveedor->activo = 1;
        $newProveedor->save();

        $newProveedor = new Proveedor();
        $newProveedor->nombre = "RODRIGUEZ MONTOYA RONALD";
        $newProveedor->rut = "22610915-3";
        $newProveedor->transferencia = 0;
        $newProveedor->activo = 1;
        $newProveedor->save();

        $newProveedor = new Proveedor();
        $newProveedor->nombre = "ROSA SALAS BRIONES";
        $newProveedor->rut = "22719454-5";
        $newProveedor->transferencia = 0;
        $newProveedor->activo = 1;
        $newProveedor->save();

        $newProveedor = new Proveedor();
        $newProveedor->nombre = "CRISTIAN GONZALEZ DIAZ";
        $newProveedor->rut = "22751086-2";
        $newProveedor->transferencia = 0;
        $newProveedor->activo = 1;
        $newProveedor->save();

        $newProveedor = new Proveedor();
        $newProveedor->nombre = "JARA ALARCON HENDRIX";
        $newProveedor->rut = "22937652-7";
        $newProveedor->transferencia = 0;
        $newProveedor->activo = 1;
        $newProveedor->save();

        $newProveedor = new Proveedor();
        $newProveedor->nombre = "JANNET VASONEZ JACOME";
        $newProveedor->rut = "23114458-7";
        $newProveedor->transferencia = 0;
        $newProveedor->activo = 1;
        $newProveedor->save();

        $newProveedor = new Proveedor();
        $newProveedor->nombre = "ORTEGA EPALZA CRISTHIAN";
        $newProveedor->rut = "23134497-7";
        $newProveedor->transferencia = 0;
        $newProveedor->activo = 1;
        $newProveedor->save();

        $newProveedor = new Proveedor();
        $newProveedor->nombre = "REY HERNANDEZ MICHEL";
        $newProveedor->rut = "23135517-0";
        $newProveedor->transferencia = 0;
        $newProveedor->activo = 1;
        $newProveedor->save();

        $newProveedor = new Proveedor();
        $newProveedor->nombre = "BUELE GALVEZ LORENA";
        $newProveedor->rut = "23141764-8";
        $newProveedor->transferencia = 0;
        $newProveedor->activo = 1;
        $newProveedor->save();

        $newProveedor = new Proveedor();
        $newProveedor->nombre = "ERAZO ERAZO KATHERINE";
        $newProveedor->rut = "23158824-8";
        $newProveedor->transferencia = 0;
        $newProveedor->activo = 1;
        $newProveedor->save();

        $newProveedor = new Proveedor();
        $newProveedor->nombre = "MARIN QUINAPALLO MARIA";
        $newProveedor->rut = "23183175-4";
        $newProveedor->transferencia = 0;
        $newProveedor->activo = 1;
        $newProveedor->save();

        $newProveedor = new Proveedor();
        $newProveedor->nombre = "JUAN CARLOS ROMERO VARGAS";
        $newProveedor->rut = "23361751-2";
        $newProveedor->transferencia = 0;
        $newProveedor->activo = 1;
        $newProveedor->save();

        $newProveedor = new Proveedor();
        $newProveedor->nombre = "CARLOS AGUILAR MARTINEZ";
        $newProveedor->rut = "23428335-9";
        $newProveedor->transferencia = 0;
        $newProveedor->activo = 1;
        $newProveedor->save();

        $newProveedor = new Proveedor();
        $newProveedor->nombre = "LUIS SANTA CRUZ MENDOZA";
        $newProveedor->rut = "23663810-3";
        $newProveedor->transferencia = 0;
        $newProveedor->activo = 1;
        $newProveedor->save();

        $newProveedor = new Proveedor();
        $newProveedor->nombre = "YEISON HERNANDEZ MENDOZA";
        $newProveedor->rut = "24132787-6";
        $newProveedor->transferencia = 0;
        $newProveedor->activo = 1;
        $newProveedor->save();

        $newProveedor = new Proveedor();
        $newProveedor->nombre = "PEREIRA HECHEVERRIA  RICARDO";
        $newProveedor->rut = "24227955-7";
        $newProveedor->transferencia = 0;
        $newProveedor->activo = 1;
        $newProveedor->save();

        $newProveedor = new Proveedor();
        $newProveedor->nombre = "PAMELA INVERNIZZI BENAVENTE";
        $newProveedor->rut = "24356526-K";
        $newProveedor->transferencia = 0;
        $newProveedor->activo = 1;
        $newProveedor->save();

        $newProveedor = new Proveedor();
        $newProveedor->nombre = "FELIX LAVAYEN BENITES";
        $newProveedor->rut = "24591685-K";
        $newProveedor->transferencia = 0;
        $newProveedor->activo = 1;
        $newProveedor->save();

        $newProveedor = new Proveedor();
        $newProveedor->nombre = "GAMBOA MURCIA JOHN";
        $newProveedor->rut = "24618604-9";
        $newProveedor->transferencia = 0;
        $newProveedor->activo = 1;
        $newProveedor->save();

        $newProveedor = new Proveedor();
        $newProveedor->nombre = "GRACIELA PEREIRA QUINTERO";
        $newProveedor->rut = "24758247-9";
        $newProveedor->transferencia = 0;
        $newProveedor->activo = 1;
        $newProveedor->save();

        $newProveedor = new Proveedor();
        $newProveedor->nombre = "JENNY VILATUNA SARZOSA";
        $newProveedor->rut = "24936292-1";
        $newProveedor->transferencia = 0;
        $newProveedor->activo = 1;
        $newProveedor->save();

        $newProveedor = new Proveedor();
        $newProveedor->nombre = "ANTONIO PALMA ALTAMAR";
        $newProveedor->rut = "24943490-6";
        $newProveedor->transferencia = 0;
        $newProveedor->activo = 1;
        $newProveedor->save();

        $newProveedor = new Proveedor();
        $newProveedor->nombre = "EDMUNDO ERAZO ERAZO";
        $newProveedor->rut = "25084423-9";
        $newProveedor->transferencia = 0;
        $newProveedor->activo = 1;
        $newProveedor->save();

        $newProveedor = new Proveedor();
        $newProveedor->nombre = "ALFREDO FRANCISCO FARIAS PINO";
        $newProveedor->rut = "2970275-6";
        $newProveedor->transferencia = 0;
        $newProveedor->activo = 1;
        $newProveedor->save();

        $newProveedor = new Proveedor();
        $newProveedor->nombre = "COLLEGE OF AMERICAN PATHOLOGISTS (CAP)";
        $newProveedor->rut = "36211832-8";
        $newProveedor->transferencia = 0;
        $newProveedor->activo = 1;
        $newProveedor->save();

        $newProveedor = new Proveedor();
        $newProveedor->nombre = "NORA MORA DONOSO";
        $newProveedor->rut = "3804743-4";
        $newProveedor->transferencia = 0;
        $newProveedor->activo = 1;
        $newProveedor->save();

        $newProveedor = new Proveedor();
        $newProveedor->nombre = "QUIMICOLOR LTDA";
        $newProveedor->rut = "50196460-3";
        $newProveedor->transferencia = 0;
        $newProveedor->activo = 1;
        $newProveedor->save();

        $newProveedor = new Proveedor();
        $newProveedor->nombre = "ELECTROMED";
        $newProveedor->rut = "50267960-0";
        $newProveedor->email = "ADMINISTRACION@ELECTROMED.CL";
        $newProveedor->transferencia = 0;
        $newProveedor->activo = 1;
        $newProveedor->save();

        $newProveedor = new Proveedor();
        $newProveedor->nombre = "CHILEPROVEEDORES";
        $newProveedor->rut = "51084200-6";
        $newProveedor->email = "CHP4@ATENTOCHILE.CL";
        $newProveedor->transferencia = 0;
        $newProveedor->activo = 1;
        $newProveedor->save();

        $newProveedor = new Proveedor();
        $newProveedor->nombre = "PRESTADORA DE SERVICIO DE CAPACITACION LTDA";
        $newProveedor->rut = "52001622-8";
        $newProveedor->email = "CRECEROTEC@GMAIL.COM";
        $newProveedor->transferencia = 0;
        $newProveedor->activo = 1;
        $newProveedor->save();

        $newProveedor = new Proveedor();
        $newProveedor->nombre = "JUAN FRANCISCO CABRERA PARADA COMPUTACION E I R L";
        $newProveedor->rut = "52002636-3";
        $newProveedor->transferencia = 0;
        $newProveedor->activo = 1;
        $newProveedor->save();

        $newProveedor = new Proveedor();
        $newProveedor->nombre = "DPLUS SERVICIOS DE CAPAC. LTDA.";
        $newProveedor->rut = "52003901-5";
        $newProveedor->transferencia = 0;
        $newProveedor->activo = 1;
        $newProveedor->save();

        $newProveedor = new Proveedor();
        $newProveedor->nombre = "ANA RAMIREZ BARRAZA";
        $newProveedor->rut = "5315335-6";
        $newProveedor->transferencia = 0;
        $newProveedor->activo = 1;
        $newProveedor->save();

        $newProveedor = new Proveedor();
        $newProveedor->nombre = "GENERAL ELECTRIC INTERNATIONAL INC.";
        $newProveedor->rut = "59010820-0";
        $newProveedor->email = "VICTORIA.URDANIZ@GE.COM";
        $newProveedor->transferencia = 0;
        $newProveedor->activo = 1;
        $newProveedor->save();

        $newProveedor = new Proveedor();
        $newProveedor->nombre = "MERCK SHARP & DOHME (I.A.) CORP.";
        $newProveedor->rut = "59043540-6";
        $newProveedor->email = "CVILLALOBOS@FASTCO.CL";
        $newProveedor->transferencia = 0;
        $newProveedor->activo = 1;
        $newProveedor->save();

        $newProveedor = new Proveedor();
        $newProveedor->nombre = "BECTON DICKINSON DE CHILE";
        $newProveedor->rut = "59051480-2";
        $newProveedor->email = "ELIANA.CASTRO@BD.COM";
        $newProveedor->transferencia = 0;
        $newProveedor->activo = 1;
        $newProveedor->save();

        $newProveedor = new Proveedor();
        $newProveedor->nombre = "NIPRO MEDICAL CORPORATION";
        $newProveedor->rut = "59077290-9";
        $newProveedor->email = "KARLAC@NIPROMED.COM";
        $newProveedor->transferencia = 0;
        $newProveedor->activo = 1;
        $newProveedor->save();

        $newProveedor = new Proveedor();
        $newProveedor->nombre = "GLOBAL HEALTHCARE CHILE";
        $newProveedor->rut = "59106780-K";
        $newProveedor->transferencia = 0;
        $newProveedor->activo = 1;
        $newProveedor->save();

        $newProveedor = new Proveedor();
        $newProveedor->nombre = "JUAN ZAPATA ZAPATA";
        $newProveedor->rut = "5923094-8";
        $newProveedor->transferencia = 0;
        $newProveedor->activo = 1;
        $newProveedor->save();

        $newProveedor = new Proveedor();
        $newProveedor->nombre = "EMPRESA DE CORREOS DE CHILE";
        $newProveedor->rut = "60503000-9";
        $newProveedor->transferencia = 0;
        $newProveedor->activo = 1;
        $newProveedor->save();

        $newProveedor = new Proveedor();
        $newProveedor->nombre = "SERVICIO DE IMPUESTOS INTERNOS";
        $newProveedor->rut = "60803000-K";
        $newProveedor->transferencia = 0;
        $newProveedor->activo = 1;
        $newProveedor->save();

        $newProveedor = new Proveedor();
        $newProveedor->nombre = "CASA DE MONEDA DE CHILE SA";
        $newProveedor->rut = "60806000-6";
        $newProveedor->email = "MAGUIRRE@CASAMONEDA.CL";
        $newProveedor->transferencia = 0;
        $newProveedor->activo = 1;
        $newProveedor->save();

        $newProveedor = new Proveedor();
        $newProveedor->nombre = "UNIVERSIDAD DE CHILE";
        $newProveedor->rut = "60910000-1";
        $newProveedor->email = "ahernand@uchile.cl";
        $newProveedor->transferencia = 0;
        $newProveedor->activo = 1;
        $newProveedor->save();

        $newProveedor = new Proveedor();
        $newProveedor->nombre = "EJERCITO DE CHILE HOSP MILITAR DE STGO";
        $newProveedor->rut = "61101030-3";
        $newProveedor->email = "VVILLANUE@HMS.CL";
        $newProveedor->transferencia = 0;
        $newProveedor->activo = 1;
        $newProveedor->save();

        $newProveedor = new Proveedor();
        $newProveedor->nombre = "HOSPITAL CLINICO FUERZA AEREA DE CHILE";
        $newProveedor->rut = "61103007-K";
        $newProveedor->email = "NPARRAGUIRRE@FACH.MIL.CL";
        $newProveedor->transferencia = 0;
        $newProveedor->activo = 1;
        $newProveedor->save();

        $newProveedor = new Proveedor();
        $newProveedor->nombre = "INSTITUTO DE SALUD PÚBLICA DE CHILE";
        $newProveedor->rut = "61605000-1";
        $newProveedor->email = "MFARFAN@ISPCH.CL";
        $newProveedor->transferencia = 0;
        $newProveedor->activo = 1;
        $newProveedor->save();

        $newProveedor = new Proveedor();
        $newProveedor->nombre = "SERV DE SALUD NORTE HOSP ROBERTO DEL RIO";
        $newProveedor->rut = "61608004-0";
        $newProveedor->transferencia = 0;
        $newProveedor->activo = 1;
        $newProveedor->save();

        $newProveedor = new Proveedor();
        $newProveedor->nombre = "INSTITUTO NACIONAL DEL TÓRAX";
        $newProveedor->rut = "61608402-K";
        $newProveedor->transferencia = 0;
        $newProveedor->activo = 1;
        $newProveedor->save();

        $newProveedor = new Proveedor();
        $newProveedor->nombre = "CENTRAL DE ABASTECIMIENTO";
        $newProveedor->rut = "61608700-2";
        $newProveedor->email = "PBAHAMONDES@CENABAST.CL";
        $newProveedor->transferencia = 0;
        $newProveedor->activo = 1;
        $newProveedor->save();

        $newProveedor = new Proveedor();
        $newProveedor->nombre = "AGUAS ANDINAS S.A.";
        $newProveedor->rut = "61808000-5";
        $newProveedor->transferencia = 0;
        $newProveedor->activo = 1;
        $newProveedor->save();

        $newProveedor = new Proveedor();
        $newProveedor->nombre = "AMALIA CONCHA NEGRETE";
        $newProveedor->rut = "6284702-6";
        $newProveedor->transferencia = 0;
        $newProveedor->activo = 1;
        $newProveedor->save();

        $newProveedor = new Proveedor();
        $newProveedor->nombre = "JOSE ATILIO RAMIREZ NAVIA";
        $newProveedor->rut = "6775947-8";
        $newProveedor->transferencia = 0;
        $newProveedor->activo = 1;
        $newProveedor->save();

        $newProveedor = new Proveedor();
        $newProveedor->nombre = "JACINTO ONOFRE ARAYA MACHUCA";
        $newProveedor->rut = "6904241-4";
        $newProveedor->transferencia = 0;
        $newProveedor->activo = 1;
        $newProveedor->save();

        $newProveedor = new Proveedor();
        $newProveedor->nombre = "I. MUNICIPALIDAD DE SANTIAGO";
        $newProveedor->rut = "69070100-6";
        $newProveedor->transferencia = 0;
        $newProveedor->activo = 1;
        $newProveedor->save();

        $newProveedor = new Proveedor();
        $newProveedor->nombre = "MARIA HERMAN OSSANDÓN";
        $newProveedor->rut = "6989602-2";
        $newProveedor->transferencia = 0;
        $newProveedor->activo = 1;
        $newProveedor->save();

        $newProveedor = new Proveedor();
        $newProveedor->nombre = "LORETO OVALLE ANDRADE";
        $newProveedor->rut = "6994009-9";
        $newProveedor->transferencia = 0;
        $newProveedor->activo = 1;
        $newProveedor->save();

        $newProveedor = new Proveedor();
        $newProveedor->nombre = "FUNDACIÓN JOSEFINA MARTINEZ DE FERRARI";
        $newProveedor->rut = "70012601-3";
        $newProveedor->email = "HSEPULVEDA@HJM.CL";
        $newProveedor->transferencia = 0;
        $newProveedor->activo = 1;
        $newProveedor->save();

        $newProveedor = new Proveedor();
        $newProveedor->nombre = "CORPORACIÓN NACIONAL DEL CÁNCER";
        $newProveedor->rut = "70095900-7";
        $newProveedor->email = "COBRANZAS@CONAC.CL";
        $newProveedor->transferencia = 0;
        $newProveedor->activo = 1;
        $newProveedor->save();

        $newProveedor = new Proveedor();
        $newProveedor->nombre = "FUNDACION ARTURO LOPEZ PEREZ";
        $newProveedor->rut = "70377400-8";
        $newProveedor->email = "RODRIGO.ZEPEDA@FALP.ORG";
        $newProveedor->transferencia = 0;
        $newProveedor->activo = 1;
        $newProveedor->save();

        $newProveedor = new Proveedor();
        $newProveedor->nombre = "SEREMI DE SALUD REGIÓN METROPOLITANA";
        $newProveedor->rut = "70940000-2";
        $newProveedor->transferencia = 0;
        $newProveedor->activo = 1;
        $newProveedor->save();

        $newProveedor = new Proveedor();
        $newProveedor->nombre = "PABLO GOMEZ LEIGHTON";
        $newProveedor->rut = "7202088-K";
        $newProveedor->transferencia = 0;
        $newProveedor->activo = 1;
        $newProveedor->save();

        $newProveedor = new Proveedor();
        $newProveedor->nombre = "HOSPITAL CLÍNICO UNIVERSIDAD DE CHILE";
        $newProveedor->rut = "72252300-8";
        $newProveedor->transferencia = 0;
        $newProveedor->activo = 1;
        $newProveedor->save();

        $newProveedor = new Proveedor();
        $newProveedor->nombre = "JORGE RUBIO POVEDA";
        $newProveedor->rut = "7252203-6";
        $newProveedor->transferencia = 0;
        $newProveedor->activo = 1;
        $newProveedor->save();

        $newProveedor = new Proveedor();
        $newProveedor->nombre = "JORGE DANTE JAÑA LAGOS";
        $newProveedor->rut = "7313159-6";
        $newProveedor->transferencia = 0;
        $newProveedor->activo = 1;
        $newProveedor->save();

        $newProveedor = new Proveedor();
        $newProveedor->nombre = "FUNDACION PROFESOR ANIBAL ARIZTIA";
        $newProveedor->rut = "73187600-2";
        $newProveedor->transferencia = 0;
        $newProveedor->activo = 1;
        $newProveedor->save();

        $newProveedor = new Proveedor();
        $newProveedor->nombre = "CORPORACIÓN DE AYUDA AL NIÑO ENFERMO RENAL MATER";
        $newProveedor->rut = "73653000-7";
        $newProveedor->email = "JSOTO@CORPORACIONMATER.CL";
        $newProveedor->transferencia = 0;
        $newProveedor->activo = 1;
        $newProveedor->save();

        $newProveedor = new Proveedor();
        $newProveedor->nombre = "MARY LINA ARCE GUTIERREZ";
        $newProveedor->rut = "7415604-5";
        $newProveedor->transferencia = 0;
        $newProveedor->activo = 1;
        $newProveedor->save();

        $newProveedor = new Proveedor();
        $newProveedor->nombre = "MONICA TORRENS PARRAQUEZ";
        $newProveedor->rut = "7433015-0";
        $newProveedor->transferencia = 0;
        $newProveedor->activo = 1;
        $newProveedor->save();

        $newProveedor = new Proveedor();
        $newProveedor->nombre = "EDGAR ESPINOZA REYES";
        $newProveedor->rut = "7475593-3";
        $newProveedor->transferencia = 0;
        $newProveedor->activo = 1;
        $newProveedor->save();

        $newProveedor = new Proveedor();
        $newProveedor->nombre = "FUND PROTEJAMOS EL CEREBRO DEL NINO";
        $newProveedor->rut = "74936800-4";
        $newProveedor->transferencia = 0;
        $newProveedor->activo = 1;
        $newProveedor->save();

        $newProveedor = new Proveedor();
        $newProveedor->nombre = "PATRICIO ENRIQUE SANHUEZA CARDEMIL";
        $newProveedor->rut = "7509163-K";
        $newProveedor->transferencia = 0;
        $newProveedor->activo = 1;
        $newProveedor->save();

        $newProveedor = new Proveedor();
        $newProveedor->nombre = "CLAUDIO MARTIN VALENZUELA 0";
        $newProveedor->rut = "7578684-0";
        $newProveedor->transferencia = 0;
        $newProveedor->activo = 1;
        $newProveedor->save();

        $newProveedor = new Proveedor();
        $newProveedor->nombre = "CENTRO AUDIOLOGICO INTEGRAL S.A.";
        $newProveedor->rut = "76004615-9";
        $newProveedor->email = "MSOLEDAD.CRUZ@PHONAK.CL";
        $newProveedor->transferencia = 0;
        $newProveedor->activo = 1;
        $newProveedor->save();

        $newProveedor = new Proveedor();
        $newProveedor->nombre = "IMPORTADORA MEGA MARKET LTDA.";
        $newProveedor->rut = "76012551-2";
        $newProveedor->email = "C.HONORATO@MEGAMARKETCHILE.CL";
        $newProveedor->transferencia = 0;
        $newProveedor->activo = 1;
        $newProveedor->save();

        $newProveedor = new Proveedor();
        $newProveedor->nombre = "BIOSYNTEC S.A.";
        $newProveedor->rut = "76015382-6";
        $newProveedor->email = "DEPARTAMENTO.COBRANZA@BIOSYNTECLABS.CL";
        $newProveedor->transferencia = 0;
        $newProveedor->activo = 1;
        $newProveedor->save();

        $newProveedor = new Proveedor();
        $newProveedor->nombre = "PRODUCTOS MEDICOS STARMED LTDA.";
        $newProveedor->rut = "76015451-2";
        $newProveedor->transferencia = 0;
        $newProveedor->activo = 1;
        $newProveedor->save();

        $newProveedor = new Proveedor();
        $newProveedor->nombre = "SYLEX CHILE LTDA";
        $newProveedor->rut = "76015781-3";
        $newProveedor->email = "RARAVENA@SYLEX.CL";
        $newProveedor->transferencia = 0;
        $newProveedor->activo = 1;
        $newProveedor->save();

        $newProveedor = new Proveedor();
        $newProveedor->nombre = "MEDES LTDA";
        $newProveedor->rut = "76016388-0";
        $newProveedor->email = "INFO@MEDES.CL";
        $newProveedor->transferencia = 0;
        $newProveedor->activo = 1;
        $newProveedor->save();

        $newProveedor = new Proveedor();
        $newProveedor->nombre = "CENTRO DE ESTUDIOS CLINICOS STGO ORIENTE Y COMPAÑI";
        $newProveedor->rut = "76018199-4";
        $newProveedor->transferencia = 0;
        $newProveedor->activo = 1;
        $newProveedor->save();

        $newProveedor = new Proveedor();
        $newProveedor->nombre = "COMERCIALIZADORA ENDOSISTEMAS LTDA.";
        $newProveedor->rut = "76019997-4";
        $newProveedor->email = "ADMIN@ENDOSISTEMAS.CL";
        $newProveedor->transferencia = 0;
        $newProveedor->activo = 1;
        $newProveedor->save();

        $newProveedor = new Proveedor();
        $newProveedor->nombre = "BOSTON MEDICAL DEVICE DE CHILE S.A.";
        $newProveedor->rut = "76020266-5";
        $newProveedor->email = "FELIPE.CACERES@CONVATEC.COM";
        $newProveedor->transferencia = 0;
        $newProveedor->activo = 1;
        $newProveedor->save();

        $newProveedor = new Proveedor();
        $newProveedor->nombre = "INGENIERIA E IMPORTACIONES M & M LTDA.";
        $newProveedor->rut = "76021485-K";
        $newProveedor->transferencia = 0;
        $newProveedor->activo = 1;
        $newProveedor->save();

        $newProveedor = new Proveedor();
        $newProveedor->nombre = "DIARIO DIGITAL LTDA.";
        $newProveedor->rut = "76023530-K";
        $newProveedor->email = "MAVENDANO@DOE.CL";
        $newProveedor->transferencia = 0;
        $newProveedor->activo = 1;
        $newProveedor->save();

        $newProveedor = new Proveedor();
        $newProveedor->nombre = "AMBULANCIA GENESIS LTDA";
        $newProveedor->rut = "76023890-2";
        $newProveedor->email = "contabilidad@ambulanciasgenesis.cl";
        $newProveedor->transferencia = 0;
        $newProveedor->activo = 1;
        $newProveedor->save();

        $newProveedor = new Proveedor();
        $newProveedor->nombre = "IMPORTADORA ROMAN LIMITADA";
        $newProveedor->rut = "76027077-6";
        $newProveedor->transferencia = 0;
        $newProveedor->activo = 1;
        $newProveedor->save();

        $newProveedor = new Proveedor();
        $newProveedor->nombre = "COMERCIAL FENIX LTDA";
        $newProveedor->rut = "76029126-9";
        $newProveedor->email = "RODRIGOMARMOLEJO@GMAIL.COM";
        $newProveedor->transferencia = 0;
        $newProveedor->activo = 1;
        $newProveedor->save();

        $newProveedor = new Proveedor();
        $newProveedor->nombre = "COMERCIALIZADORA DE EQUIPOS E INSUMOS MEDICOS SPA";
        $newProveedor->rut = "76029695-3";
        $newProveedor->transferencia = 0;
        $newProveedor->activo = 1;
        $newProveedor->save();

        $newProveedor = new Proveedor();
        $newProveedor->nombre = "COMERCIAL FALC CHILE LTDA.";
        $newProveedor->rut = "76029807-7";
        $newProveedor->email = "FALCLIMITADA@GMAIL.COM";
        $newProveedor->transferencia = 0;
        $newProveedor->activo = 1;
        $newProveedor->save();

        $newProveedor = new Proveedor();
        $newProveedor->nombre = "FARMACEUTICA BIOFORMULA LTDA.";
        $newProveedor->rut = "76030398-4";
        $newProveedor->email = "LPINTO@BIOFORMULA.CL";
        $newProveedor->transferencia = 0;
        $newProveedor->activo = 1;
        $newProveedor->save();

        $newProveedor = new Proveedor();
        $newProveedor->nombre = "SYNTHON CHILE LIMITADA";
        $newProveedor->rut = "76032097-8";
        $newProveedor->email = "DANIEL.REYES@SYNTHON.COM";
        $newProveedor->transferencia = 0;
        $newProveedor->activo = 1;
        $newProveedor->save();

        $newProveedor = new Proveedor();
        $newProveedor->nombre = "FARMACIAS Y DROGUERIAS DE CHILE LTDA.";
        $newProveedor->rut = "76032335-7";
        $newProveedor->transferencia = 0;
        $newProveedor->activo = 1;
        $newProveedor->save();

        $newProveedor = new Proveedor();
        $newProveedor->nombre = "DRAGER CHILE LTDA.";
        $newProveedor->rut = "76033880-K";
        $newProveedor->email = "LILIANA.GUAJARDO@DRAEGER.COM";
        $newProveedor->transferencia = 0;
        $newProveedor->activo = 1;
        $newProveedor->save();

        $newProveedor = new Proveedor();
        $newProveedor->nombre = "LAYNER SPA";
        $newProveedor->rut = "76034985-2";
        $newProveedor->email = "REMUNERACIONES@LAYNER.CL";
        $newProveedor->transferencia = 0;
        $newProveedor->activo = 1;
        $newProveedor->save();

        $newProveedor = new Proveedor();
        $newProveedor->nombre = "COMERCIALIZADORA P&E SOLUCIONES INDUSTRIALES LTDA.";
        $newProveedor->rut = "76035740-5";
        $newProveedor->email = "MC@GRUPOPYE.COM";
        $newProveedor->transferencia = 0;
        $newProveedor->activo = 1;
        $newProveedor->save();

        $newProveedor = new Proveedor();
        $newProveedor->nombre = "ING Y CAPAC BOBADILLA Y LUCIC";
        $newProveedor->rut = "76035920-3";
        $newProveedor->transferencia = 0;
        $newProveedor->activo = 1;
        $newProveedor->save();

        $newProveedor = new Proveedor();
        $newProveedor->nombre = "GEOTERMICA INGENIERÍA LTDA.";
        $newProveedor->rut = "76037678-7";
        $newProveedor->email = "JCASTILLO@GEOTERMICALTDA.CL";
        $newProveedor->transferencia = 0;
        $newProveedor->activo = 1;
        $newProveedor->save();

        $newProveedor = new Proveedor();
        $newProveedor->nombre = "SOCIEDAD PRODIAGNÓSTICA LIMITADA";
        $newProveedor->rut = "76040480-2";
        $newProveedor->email = "INFO@PRODIAGNOSTICA.CL";
        $newProveedor->transferencia = 0;
        $newProveedor->activo = 1;
        $newProveedor->save();

        $newProveedor = new Proveedor();
        $newProveedor->nombre = "COMERCIALIZADORA DE INSUMOS MEDICOS SPA";
        $newProveedor->rut = "76042903-1";
        $newProveedor->email = "LACUNA@MEDDICA.CL";
        $newProveedor->transferencia = 0;
        $newProveedor->activo = 1;
        $newProveedor->save();

        $newProveedor = new Proveedor();
        $newProveedor->nombre = "PLUS S.A";
        $newProveedor->rut = "76044316-6";
        $newProveedor->transferencia = 0;
        $newProveedor->activo = 1;
        $newProveedor->save();

        $newProveedor = new Proveedor();
        $newProveedor->nombre = "ARLAB S.A.";
        $newProveedor->rut = "76044959-8";
        $newProveedor->email = "LLAUNAY@ARLAB.CL";
        $newProveedor->transferencia = 0;
        $newProveedor->activo = 1;
        $newProveedor->save();

        $newProveedor = new Proveedor();
        $newProveedor->nombre = "CREAR DE MEDICA URCOLA SPA";
        $newProveedor->rut = "76045884-8";
        $newProveedor->email = "SANDRA.AGUILERA@MEDICAURCOLA.CL";
        $newProveedor->transferencia = 0;
        $newProveedor->activo = 1;
        $newProveedor->save();

        $newProveedor = new Proveedor();
        $newProveedor->nombre = "COMERCIAL OCTAVA LTDA.";
        $newProveedor->rut = "76046716-2";
        $newProveedor->transferencia = 0;
        $newProveedor->activo = 1;
        $newProveedor->save();

        $newProveedor = new Proveedor();
        $newProveedor->nombre = "PLASTICOS BESSALLE LTDA";
        $newProveedor->rut = "76047525-4";
        $newProveedor->email = "PLASTICOS@ENTELCHILE.NET";
        $newProveedor->transferencia = 0;
        $newProveedor->activo = 1;
        $newProveedor->save();

        $newProveedor = new Proveedor();
        $newProveedor->nombre = "COMERCIAL SITEC LTDA.";
        $newProveedor->rut = "76049600-6";
        $newProveedor->email = "SITECCOM@ENTELCHILE.NET";
        $newProveedor->transferencia = 0;
        $newProveedor->activo = 1;
        $newProveedor->save();

        $newProveedor = new Proveedor();
        $newProveedor->nombre = "CSS CONSULTORA SEGURIDAD SERVICIOS LTDA.";
        $newProveedor->rut = "76050680-K";
        $newProveedor->email = "MMARINP@SERVICIOSMPS.CL";
        $newProveedor->transferencia = 0;
        $newProveedor->activo = 1;
        $newProveedor->save();

        $newProveedor = new Proveedor();
        $newProveedor->nombre = "LAB. GENZYME CHILE LTDA.";
        $newProveedor->rut = "76051550-7";
        $newProveedor->email = "MANUEL.MELLA@SANOFIPASTEUR.COM";
        $newProveedor->transferencia = 0;
        $newProveedor->activo = 1;
        $newProveedor->save();

        $newProveedor = new Proveedor();
        $newProveedor->nombre = "SOC. DE REPRESENTACIÓN IMP. Y COM. RAPID TEST";
        $newProveedor->rut = "76057398-1";
        $newProveedor->email = "INFO@RAPIDTEST.CL";
        $newProveedor->transferencia = 0;
        $newProveedor->activo = 1;
        $newProveedor->save();

        $newProveedor = new Proveedor();
        $newProveedor->nombre = "SOCIEDAD COMERCIAL IL PASO LTDA.";
        $newProveedor->rut = "76057497-K";
        $newProveedor->email = "SANDRO@ILPASO.CL";
        $newProveedor->transferencia = 0;
        $newProveedor->activo = 1;
        $newProveedor->save();

        $newProveedor = new Proveedor();
        $newProveedor->nombre = "COMER. ANDRES ANTONIO OTERO URIBE E.I.R.L";
        $newProveedor->rut = "76058391-K";
        $newProveedor->email = "ELIZABETH@EDUPLAYS.CL";
        $newProveedor->transferencia = 0;
        $newProveedor->activo = 1;
        $newProveedor->save();

        $newProveedor = new Proveedor();
        $newProveedor->nombre = "LIMPIEZA VERDE SPA";
        $newProveedor->rut = "76059183-1";
        $newProveedor->email = "CMD@CMD.CL";
        $newProveedor->transferencia = 0;
        $newProveedor->activo = 1;
        $newProveedor->save();

        $newProveedor = new Proveedor();
        $newProveedor->nombre = "CASTTELAIN Y SOTOMAYOR CIA LTDA";
        $newProveedor->rut = "76059321-4";
        $newProveedor->transferencia = 0;
        $newProveedor->activo = 1;
        $newProveedor->save();

        $newProveedor = new Proveedor();
        $newProveedor->nombre = "ACTIVACM";
        $newProveedor->rut = "76059325-7";
        $newProveedor->transferencia = 0;
        $newProveedor->activo = 1;
        $newProveedor->save();

        $newProveedor = new Proveedor();
        $newProveedor->nombre = "COMERCIAL 3 AIRES LTDA";
        $newProveedor->rut = "76061008-9";
        $newProveedor->email = "ISABERRIOS@REYSER.CL";
        $newProveedor->transferencia = 0;
        $newProveedor->activo = 1;
        $newProveedor->save();

        $newProveedor = new Proveedor();
        $newProveedor->nombre = "EDGE SPA";
        $newProveedor->rut = "76067326-9";
        $newProveedor->transferencia = 0;
        $newProveedor->activo = 1;
        $newProveedor->save();

        $newProveedor = new Proveedor();
        $newProveedor->nombre = "MARCOTEC SERV COMPLTDA";
        $newProveedor->rut = "76068574-7";
        $newProveedor->email = "EGONZALEZ@ASINFOR.CL";
        $newProveedor->transferencia = 0;
        $newProveedor->activo = 1;
        $newProveedor->save();

        $newProveedor = new Proveedor();
        $newProveedor->nombre = "GAVILAN Y COMPAÑÍA LTDA ALFONSO URZUA";
        $newProveedor->rut = "76069151-8";
        $newProveedor->transferencia = 0;
        $newProveedor->activo = 1;
        $newProveedor->save();

        $newProveedor = new Proveedor();
        $newProveedor->nombre = "PRESTACIONES MEDICAS AY C LIMITADA";
        $newProveedor->rut = "76070911-5";
        $newProveedor->transferencia = 0;
        $newProveedor->activo = 1;
        $newProveedor->save();

        $newProveedor = new Proveedor();
        $newProveedor->nombre = "SIMA";
        $newProveedor->rut = "76072677-K";
        $newProveedor->email = "SECRETARIA@SIMAEQUIPOSMEDICOS.CL";
        $newProveedor->transferencia = 0;
        $newProveedor->activo = 1;
        $newProveedor->save();

        $newProveedor = new Proveedor();
        $newProveedor->nombre = "FUENTES Y LONCOMIL LTDA";
        $newProveedor->rut = "76077525-8";
        $newProveedor->transferencia = 0;
        $newProveedor->activo = 1;
        $newProveedor->save();

        $newProveedor = new Proveedor();
        $newProveedor->nombre = "IMERLAB SOCIEDAD COMERCIAL LIMITADA";
        $newProveedor->rut = "76079592-5";
        $newProveedor->email = "jcanetec@bancochile.cl";
        $newProveedor->transferencia = 0;
        $newProveedor->activo = 1;
        $newProveedor->save();

        $newProveedor = new Proveedor();
        $newProveedor->nombre = "WINPHARM SPA";
        $newProveedor->rut = "76079782-0";
        $newProveedor->email = "JHUANEL@WINPHARM.CL";
        $newProveedor->transferencia = 0;
        $newProveedor->activo = 1;
        $newProveedor->save();

        $newProveedor = new Proveedor();
        $newProveedor->nombre = "GADOR LTDA.";
        $newProveedor->rut = "76084945-6";
        $newProveedor->email = "COBRANZA@GADOR.CL";
        $newProveedor->transferencia = 0;
        $newProveedor->activo = 1;
        $newProveedor->save();

        $newProveedor = new Proveedor();
        $newProveedor->nombre = "CAISSA S.A.";
        $newProveedor->rut = "76087971-1";
        $newProveedor->email = "COBRANZAS@CAISSA.CL";
        $newProveedor->transferencia = 0;
        $newProveedor->activo = 1;
        $newProveedor->save();

        $newProveedor = new Proveedor();
        $newProveedor->nombre = "DUMAR S.A";
        $newProveedor->rut = "76088231-3";
        $newProveedor->email = "VENTAS@DUMAR.CL";
        $newProveedor->transferencia = 0;
        $newProveedor->activo = 1;
        $newProveedor->save();

        $newProveedor = new Proveedor();
        $newProveedor->nombre = "POSITRONPHARMA S.A";
        $newProveedor->rut = "76089296-3";
        $newProveedor->email = "AMYANEZ@POSITRONPHARMA.CL";
        $newProveedor->transferencia = 0;
        $newProveedor->activo = 1;
        $newProveedor->save();

        $newProveedor = new Proveedor();
        $newProveedor->nombre = "LABORATORIOS BIOPAS S.A.";
        $newProveedor->rut = "76091301-4";
        $newProveedor->transferencia = 0;
        $newProveedor->activo = 1;
        $newProveedor->save();

        $newProveedor = new Proveedor();
        $newProveedor->nombre = "SERVICIOS MEDICOS S.A. (BIOSUD)";
        $newProveedor->rut = "76091620-K";
        $newProveedor->transferencia = 0;
        $newProveedor->activo = 1;
        $newProveedor->save();

        $newProveedor = new Proveedor();
        $newProveedor->nombre = "DIST MULTIMARCA LTDA";
        $newProveedor->rut = "76093253-1";
        $newProveedor->transferencia = 0;
        $newProveedor->activo = 1;
        $newProveedor->save();

        $newProveedor = new Proveedor();
        $newProveedor->nombre = "ESPACIO Y EQUIPAMIENTO LIMITADA";
        $newProveedor->rut = "76096222-8";
        $newProveedor->transferencia = 0;
        $newProveedor->activo = 1;
        $newProveedor->save();

        $newProveedor = new Proveedor();
        $newProveedor->nombre = "INSUMEDICA COMERCIAL LTDA.";
        $newProveedor->rut = "76096232-5";
        $newProveedor->email = "VENTAS@INSUMEDICA.CL";
        $newProveedor->transferencia = 0;
        $newProveedor->activo = 1;
        $newProveedor->save();

        $newProveedor = new Proveedor();
        $newProveedor->nombre = "NUÑEZ & HIJOS LIMITADA";
        $newProveedor->rut = "76097660-1";
        $newProveedor->transferencia = 0;
        $newProveedor->activo = 1;
        $newProveedor->save();

        $newProveedor = new Proveedor();
        $newProveedor->nombre = "SOCIEDAD COMERCIAL SOLO FRESCO LIMITADA";
        $newProveedor->rut = "76102347-0";
        $newProveedor->transferencia = 0;
        $newProveedor->activo = 1;
        $newProveedor->save();

        $newProveedor = new Proveedor();
        $newProveedor->nombre = "BUSTOS Y BUSTOS CAPACITACION";
        $newProveedor->rut = "76108611-1";
        $newProveedor->transferencia = 0;
        $newProveedor->activo = 1;
        $newProveedor->save();

        $newProveedor = new Proveedor();
        $newProveedor->nombre = "ASESORIA E INGENIERIA DRJ LTDA.";
        $newProveedor->rut = "76110429-2";
        $newProveedor->transferencia = 0;
        $newProveedor->activo = 1;
        $newProveedor->save();

        $newProveedor = new Proveedor();
        $newProveedor->nombre = "AKI PHARMACHILE S.A.";
        $newProveedor->rut = "76111593-6";
        $newProveedor->email = "CDUARTE@DAOGROUP.CL";
        $newProveedor->transferencia = 0;
        $newProveedor->activo = 1;
        $newProveedor->save();

        $newProveedor = new Proveedor();
        $newProveedor->nombre = "EQUIPOS MÉDICOS ZEPEDA Y CIA. LTDA.";
        $newProveedor->rut = "76112212-6";
        $newProveedor->email = "LMARIANGEL@ZEPEDA.CL";
        $newProveedor->transferencia = 0;
        $newProveedor->activo = 1;
        $newProveedor->save();

        $newProveedor = new Proveedor();
        $newProveedor->nombre = "SIERRA PHARMA";
        $newProveedor->rut = "76112757-8";
        $newProveedor->transferencia = 0;
        $newProveedor->activo = 1;
        $newProveedor->save();

        $newProveedor = new Proveedor();
        $newProveedor->nombre = "PHARMATECH CHILE SPA";
        $newProveedor->rut = "76113734-4";
        $newProveedor->email = "ccea@pharmatech.cl";
        $newProveedor->transferencia = 0;
        $newProveedor->activo = 1;
        $newProveedor->save();

        $newProveedor = new Proveedor();
        $newProveedor->nombre = "ROXANA CEA Y COMPAÑÍA LIMITADA";
        $newProveedor->rut = "76114460-K";
        $newProveedor->email = "TECNOPLANTA@TECNOPLANTA.CL";
        $newProveedor->transferencia = 0;
        $newProveedor->activo = 1;
        $newProveedor->save();

        $newProveedor = new Proveedor();
        $newProveedor->nombre = "VESALIUS PHARMA S.A";
        $newProveedor->rut = "76120786-5";
        $newProveedor->email = "GERENCIA@VESALIUSPHARMA.CL";
        $newProveedor->transferencia = 0;
        $newProveedor->activo = 1;
        $newProveedor->save();

        $newProveedor = new Proveedor();
        $newProveedor->nombre = "SERVICIOS MEDICOS BICENTENARIO";
        $newProveedor->rut = "76124062-5";
        $newProveedor->email = "HECTOR.GODOY@CLIBISA.CL";
        $newProveedor->transferencia = 0;
        $newProveedor->activo = 1;
        $newProveedor->save();

        $newProveedor = new Proveedor();
        $newProveedor->nombre = "GRAFICA PUBLICITARIA LIMITADA";
        $newProveedor->rut = "76125632-7";
        $newProveedor->email = "VENTAS@PROGRAFIK.CL";
        $newProveedor->transferencia = 0;
        $newProveedor->activo = 1;
        $newProveedor->save();

        $newProveedor = new Proveedor();
        $newProveedor->nombre = "POLYCOMPANY INTERNATIONAL GROUP S.A.";
        $newProveedor->rut = "76125662-9";
        $newProveedor->email = "BUSINESS.POLYCOMPANY@GMAIL.COM";
        $newProveedor->transferencia = 0;
        $newProveedor->activo = 1;
        $newProveedor->save();

        $newProveedor = new Proveedor();
        $newProveedor->nombre = "TREMA DENTAL LTDA.";
        $newProveedor->rut = "76128840-7";
        $newProveedor->email = "COBRANZAS@TREMA.CL";
        $newProveedor->transferencia = 0;
        $newProveedor->activo = 1;
        $newProveedor->save();

        $newProveedor = new Proveedor();
        $newProveedor->nombre = "MEDCORP S.A.";
        $newProveedor->rut = "76131542-0";
        $newProveedor->email = "MCUESTA@MEDCORP.CL";
        $newProveedor->transferencia = 0;
        $newProveedor->activo = 1;
        $newProveedor->save();

        $newProveedor = new Proveedor();
        $newProveedor->nombre = "COMERCIAL LUIS PRIETO";
        $newProveedor->rut = "76135292-K";
        $newProveedor->transferencia = 0;
        $newProveedor->activo = 1;
        $newProveedor->save();

        $newProveedor = new Proveedor();
        $newProveedor->nombre = "CERTEL S.A.";
        $newProveedor->rut = "76137961-5";
        $newProveedor->transferencia = 0;
        $newProveedor->activo = 1;
        $newProveedor->save();

        $newProveedor = new Proveedor();
        $newProveedor->nombre = "SOCIEDAD ANESTESIOLOGIA HYPNOS SPA";
        $newProveedor->rut = "76141298-1";
        $newProveedor->email = "hypnos.anestesia@gmail.com";
        $newProveedor->transferencia = 0;
        $newProveedor->activo = 1;
        $newProveedor->save();

        $newProveedor = new Proveedor();
        $newProveedor->nombre = "ESPEX SOLUCIONES TECNOLOGICAS SPA";
        $newProveedor->rut = "76142134-4";
        $newProveedor->transferencia = 0;
        $newProveedor->activo = 1;
        $newProveedor->save();

        $newProveedor = new Proveedor();
        $newProveedor->nombre = "COM. GEMCO GRAL. MACHINERY CO.";
        $newProveedor->rut = "76142730-K";
        $newProveedor->email = "medico@gemco.cl";
        $newProveedor->transferencia = 0;
        $newProveedor->activo = 1;
        $newProveedor->save();

        $newProveedor = new Proveedor();
        $newProveedor->nombre = "IDEA MARKET SPA";
        $newProveedor->rut = "76148288-2";
        $newProveedor->email = "LOLAVEP70@GMAIL.COM";
        $newProveedor->transferencia = 0;
        $newProveedor->activo = 1;
        $newProveedor->save();

        $newProveedor = new Proveedor();
        $newProveedor->nombre = "COMERCIALIZADORA JMJ LIMITADA";
        $newProveedor->rut = "76148317-K";
        $newProveedor->transferencia = 0;
        $newProveedor->activo = 1;
        $newProveedor->save();

        $newProveedor = new Proveedor();
        $newProveedor->nombre = "COMERCIAL JUAN CASTILLO EIRL";
        $newProveedor->rut = "76148981-K";
        $newProveedor->transferencia = 0;
        $newProveedor->activo = 1;
        $newProveedor->save();

        $newProveedor = new Proveedor();
        $newProveedor->nombre = "SOCIEDAD ENERSET LIMITADA";
        $newProveedor->rut = "76156939-2";
        $newProveedor->email = "CSAAVEDRA@ENERSET.CL";
        $newProveedor->transferencia = 0;
        $newProveedor->activo = 1;
        $newProveedor->save();

        $newProveedor = new Proveedor();
        $newProveedor->nombre = "PROTECCIÓN RADIOLÓGICA LTDA.";
        $newProveedor->rut = "76160859-2";
        $newProveedor->email = "INFO@PROTECCIONRADIOLOGICA.CL";
        $newProveedor->transferencia = 0;
        $newProveedor->activo = 1;
        $newProveedor->save();

        $newProveedor = new Proveedor();
        $newProveedor->nombre = "SOC. VENT MEDICAL LTDA.";
        $newProveedor->rut = "76164399-1";
        $newProveedor->email = "INFO@VENTMEDICAL.CL";
        $newProveedor->transferencia = 0;
        $newProveedor->activo = 1;
        $newProveedor->save();

        $newProveedor = new Proveedor();
        $newProveedor->nombre = "MAURICIO ALFARO ALEGRÍA PRODUCTOS MÉDICOS EIRL";
        $newProveedor->rut = "76174812-2";
        $newProveedor->email = "VELGUETA@LABOFAR.CL";
        $newProveedor->transferencia = 0;
        $newProveedor->activo = 1;
        $newProveedor->save();

        $newProveedor = new Proveedor();
        $newProveedor->nombre = "ASCEND LABORATORIES SPA";
        $newProveedor->rut = "76175092-5";
        $newProveedor->email = "RAFAEL.WALKER@ALKEM.COM";
        $newProveedor->transferencia = 0;
        $newProveedor->activo = 1;
        $newProveedor->save();

        $newProveedor = new Proveedor();
        $newProveedor->nombre = "CENTRAL DE RESTAURANTES ARAMARK LTDA.";
        $newProveedor->rut = "76178360-2";
        $newProveedor->email = "SAITZ-LUIS@ARAMARK.CL";
        $newProveedor->transferencia = 0;
        $newProveedor->activo = 1;
        $newProveedor->save();

        $newProveedor = new Proveedor();
        $newProveedor->nombre = "CENTRAL DE RESTAURANTES ARAMARK MULTISERVICIOS LTDA.";
        $newProveedor->rut = "76178390-4";
        $newProveedor->email = "VALENZUELA-JEAN@ARAMARK.CL";
        $newProveedor->transferencia = 0;
        $newProveedor->activo = 1;
        $newProveedor->save();

        $newProveedor = new Proveedor();
        $newProveedor->nombre = "BSN MEDICAL SPA";
        $newProveedor->rut = "76186732-6";
        $newProveedor->email = "HEGON.CLAVERO@BSNMEDICAL.COM";
        $newProveedor->transferencia = 0;
        $newProveedor->activo = 1;
        $newProveedor->save();

        $newProveedor = new Proveedor();
        $newProveedor->nombre = "FARMACIA BUFFOR";
        $newProveedor->rut = "76186755-5";
        $newProveedor->email = "GENOVEVA@FARMACIABULFOR.CL";
        $newProveedor->transferencia = 0;
        $newProveedor->activo = 1;
        $newProveedor->save();

        $newProveedor = new Proveedor();
        $newProveedor->nombre = "RED DE SALUD AMBULATORIA NACIONAL S.A";
        $newProveedor->rut = "76188643-6";
        $newProveedor->email = "JUANPABLO.ARETIO@ONCOVIDACHILE.CL";
        $newProveedor->transferencia = 0;
        $newProveedor->activo = 1;
        $newProveedor->save();

        $newProveedor = new Proveedor();
        $newProveedor->nombre = "SERVICIO Y MAQUILA SERVICE LIMITADA";
        $newProveedor->rut = "76191389-1";
        $newProveedor->email = "MARIETA@CEGAMED.CL";
        $newProveedor->transferencia = 0;
        $newProveedor->activo = 1;
        $newProveedor->save();

        $newProveedor = new Proveedor();
        $newProveedor->nombre = "COMERCIAL MOTORSHOP LTDA";
        $newProveedor->rut = "76193188-1";
        $newProveedor->email = "VENTAS@MOTORSHOP.CL";
        $newProveedor->transferencia = 0;
        $newProveedor->activo = 1;
        $newProveedor->save();

        $newProveedor = new Proveedor();
        $newProveedor->nombre = "ACREDITA NORTE CHILE LIMITADA";
        $newProveedor->rut = "76195605-1";
        $newProveedor->transferencia = 0;
        $newProveedor->activo = 1;
        $newProveedor->save();

        $newProveedor = new Proveedor();
        $newProveedor->nombre = "REPRESENTACIONES GEMARKPHARMA LTDA.";
        $newProveedor->rut = "76204763-2";
        $newProveedor->email = "HAPM.GERENCIA@GEMARKPHARMA.CL";
        $newProveedor->transferencia = 0;
        $newProveedor->activo = 1;
        $newProveedor->save();

        $newProveedor = new Proveedor();
        $newProveedor->nombre = "PROVEEDORES INTEGRALES CHILE LTDA";
        $newProveedor->rut = "76205213-K";
        $newProveedor->transferencia = 0;
        $newProveedor->activo = 1;
        $newProveedor->save();

        $newProveedor = new Proveedor();
        $newProveedor->nombre = "ALCO SUR ING. LTDA.";
        $newProveedor->rut = "76206907-5";
        $newProveedor->transferencia = 0;
        $newProveedor->activo = 1;
        $newProveedor->save();

        $newProveedor = new Proveedor();
        $newProveedor->nombre = "ESPEC SEGURIDAD INDUSTRIAL SPA";
        $newProveedor->rut = "76207442-7";
        $newProveedor->transferencia = 0;
        $newProveedor->activo = 1;
        $newProveedor->save();

        $newProveedor = new Proveedor();
        $newProveedor->nombre = "ABBVIE PRODUCTOS FARMACÉUTICOS LTDA.";
        $newProveedor->rut = "76212732-6";
        $newProveedor->email = "TCHIGUAI@FASTCO.CL";
        $newProveedor->transferencia = 0;
        $newProveedor->activo = 1;
        $newProveedor->save();

        $newProveedor = new Proveedor();
        $newProveedor->nombre = "INVERSIONES CHM";
        $newProveedor->rut = "76214406-9";
        $newProveedor->transferencia = 0;
        $newProveedor->activo = 1;
        $newProveedor->save();

        $newProveedor = new Proveedor();
        $newProveedor->nombre = "NEMO CHILE S.A.";
        $newProveedor->rut = "76215260-6";
        $newProveedor->email = "PATRICIA.SEGURA@NEMOCHILE.CL";
        $newProveedor->transferencia = 0;
        $newProveedor->activo = 1;
        $newProveedor->save();

        $newProveedor = new Proveedor();
        $newProveedor->nombre = "CAPACITACION EMPRESARIAL MARIA SOLEDAD DEL PILAR";
        $newProveedor->rut = "76215505-2";
        $newProveedor->email = "CAPACITACION.PROGRESA@GMAIL.COM";
        $newProveedor->transferencia = 0;
        $newProveedor->activo = 1;
        $newProveedor->save();

        $newProveedor = new Proveedor();
        $newProveedor->nombre = "RESCATELIFE";
        $newProveedor->rut = "76217441-3";
        $newProveedor->email = "ventas@rescatelife.cl";
        $newProveedor->transferencia = 0;
        $newProveedor->activo = 1;
        $newProveedor->save();

        $newProveedor = new Proveedor();
        $newProveedor->nombre = "SAPACHILE CONSULTORES";
        $newProveedor->rut = "76221442-3";
        $newProveedor->transferencia = 0;
        $newProveedor->activo = 1;
        $newProveedor->save();

        $newProveedor = new Proveedor();
        $newProveedor->nombre = "COM. VENTA EMPRESAS CHILE LTDA. (VECH)";
        $newProveedor->rut = "76226129-4";
        $newProveedor->transferencia = 0;
        $newProveedor->activo = 1;
        $newProveedor->save();

        $newProveedor = new Proveedor();
        $newProveedor->nombre = "LUIS VALDES LYON";
        $newProveedor->rut = "76231391-K";
        $newProveedor->email = "TZUNIGA@LUVALY.CL";
        $newProveedor->transferencia = 0;
        $newProveedor->activo = 1;
        $newProveedor->save();

        $newProveedor = new Proveedor();
        $newProveedor->nombre = "HOSPIRA CHILE LTDA.";
        $newProveedor->rut = "76236180-9";
        $newProveedor->email = "PAGOS.CL@HOSPIRA.COM";
        $newProveedor->transferencia = 0;
        $newProveedor->activo = 1;
        $newProveedor->save();

        $newProveedor = new Proveedor();
        $newProveedor->nombre = "LABORATORIOS ANDROMACO S.A.";
        $newProveedor->rut = "76237266-5";
        $newProveedor->email = "VICTORIA.OSSES@GRUNENTHAL.COM";
        $newProveedor->transferencia = 0;
        $newProveedor->activo = 1;
        $newProveedor->save();

        $newProveedor = new Proveedor();
        $newProveedor->nombre = "SOCIEDAD COMERCIAL TECNOUTILES LTDA.";
        $newProveedor->rut = "76239681-5";
        $newProveedor->email = "VENTAS@TECNOUTILES.CL";
        $newProveedor->transferencia = 0;
        $newProveedor->activo = 1;
        $newProveedor->save();

        $newProveedor = new Proveedor();
        $newProveedor->nombre = "IGESTEC COMERCIALIZADORA LTDA.,";
        $newProveedor->rut = "76241351-5";
        $newProveedor->email = "JOSE.PEDANO@HOTMAIL.COM";
        $newProveedor->transferencia = 0;
        $newProveedor->activo = 1;
        $newProveedor->save();

        $newProveedor = new Proveedor();
        $newProveedor->nombre = "INDUSLAB SPA";
        $newProveedor->rut = "76242249-2";
        $newProveedor->transferencia = 0;
        $newProveedor->activo = 1;
        $newProveedor->save();

        $newProveedor = new Proveedor();
        $newProveedor->nombre = "LIMPIEZA INDUSTRIAL ALFARO Y GODOY LTDA.";
        $newProveedor->rut = "76254354-0";
        $newProveedor->transferencia = 0;
        $newProveedor->activo = 1;
        $newProveedor->save();

        $newProveedor = new Proveedor();
        $newProveedor->nombre = "SOC. DE PROFESIONALES ZEUS LTDA.";
        $newProveedor->rut = "76254355-9";
        $newProveedor->transferencia = 0;
        $newProveedor->activo = 1;
        $newProveedor->save();

        $newProveedor = new Proveedor();
        $newProveedor->nombre = "COM. RAFMEDICAL ELECTR. LTDA.";
        $newProveedor->rut = "76261928-8";
        $newProveedor->transferencia = 0;
        $newProveedor->activo = 1;
        $newProveedor->save();

        $newProveedor = new Proveedor();
        $newProveedor->nombre = "COMERCIALIZADORA DE PRODUCTOS DE ASEO RENHET";
        $newProveedor->rut = "76268728-3";
        $newProveedor->email = "CMUNOZ@RENHETCHILE.CL";
        $newProveedor->transferencia = 0;
        $newProveedor->activo = 1;
        $newProveedor->save();

        $newProveedor = new Proveedor();
        $newProveedor->nombre = "ASEO Y SEGURIDAD INDUSTIAL HYGEN PSA";
        $newProveedor->rut = "76268735-6";
        $newProveedor->email = "CMUNOZ@RENHETCHILE.CL";
        $newProveedor->transferencia = 0;
        $newProveedor->activo = 1;
        $newProveedor->save();

        $newProveedor = new Proveedor();
        $newProveedor->nombre = "COLMAR TECNOLOGIA LTDA.";
        $newProveedor->rut = "76268792-5";
        $newProveedor->transferencia = 0;
        $newProveedor->activo = 1;
        $newProveedor->save();

        $newProveedor = new Proveedor();
        $newProveedor->nombre = "JESUS GRACIA Y CIA LTDA";
        $newProveedor->rut = "76270519-2";
        $newProveedor->email = "SERGIO.RUBIO@BASFLEX.COM";
        $newProveedor->transferencia = 0;
        $newProveedor->activo = 1;
        $newProveedor->save();

        $newProveedor = new Proveedor();
        $newProveedor->nombre = "MAYORDENT DENTAL LTDA.";
        $newProveedor->rut = "76271360-8";
        $newProveedor->email = "LURIBE@MAYORDENT.CL";
        $newProveedor->transferencia = 0;
        $newProveedor->activo = 1;
        $newProveedor->save();

        $newProveedor = new Proveedor();
        $newProveedor->nombre = "MAGENS S.A.";
        $newProveedor->rut = "76271597-K";
        $newProveedor->email = "convenio@magens.cl";
        $newProveedor->transferencia = 0;
        $newProveedor->activo = 1;
        $newProveedor->save();

        $newProveedor = new Proveedor();
        $newProveedor->nombre = "COMERCIAL ROIAN STORE LTDA";
        $newProveedor->rut = "76276399-0";
        $newProveedor->email = "VENTAS@ROIAN.CL";
        $newProveedor->transferencia = 0;
        $newProveedor->activo = 1;
        $newProveedor->save();

        $newProveedor = new Proveedor();
        $newProveedor->nombre = "EXTERILIZA S.A";
        $newProveedor->rut = "76282756-5";
        $newProveedor->email = "MC@GRUPOPYE.COM";
        $newProveedor->transferencia = 0;
        $newProveedor->activo = 1;
        $newProveedor->save();

        $newProveedor = new Proveedor();
        $newProveedor->nombre = "GRUPO NARBACH ASESORIAS Y EQUIPOS SPA";
        $newProveedor->rut = "76282779-4";
        $newProveedor->email = "LBANAREZ@GRUPONARBACH.CL";
        $newProveedor->transferencia = 0;
        $newProveedor->activo = 1;
        $newProveedor->save();

        $newProveedor = new Proveedor();
        $newProveedor->nombre = "PRONOMED";
        $newProveedor->rut = "76286670-6";
        $newProveedor->email = "info@pronomed.cl";
        $newProveedor->transferencia = 0;
        $newProveedor->activo = 1;
        $newProveedor->save();

        $newProveedor = new Proveedor();
        $newProveedor->nombre = "COMERCIAL AGUSTIN LTDA";
        $newProveedor->rut = "76287853-4";
        $newProveedor->email = "KATHERINE.FLORES@COMERCIALAGUSTIN.CL";
        $newProveedor->transferencia = 0;
        $newProveedor->activo = 1;
        $newProveedor->save();

        $newProveedor = new Proveedor();
        $newProveedor->nombre = "COMERCIAL COMPARO LIMITADA";
        $newProveedor->rut = "76290943-K";
        $newProveedor->email = "PAULO.CAMPOS@COMERCIALCOMPARO.CL";
        $newProveedor->transferencia = 0;
        $newProveedor->activo = 1;
        $newProveedor->save();

        $newProveedor = new Proveedor();
        $newProveedor->nombre = "INSUMOS MEDICOS Y DENTALES CLINMEDICAL LTDA";
        $newProveedor->rut = "76293397-7";
        $newProveedor->email = "FRANCISCA.RAVET@CLINMEDICAL.CL";
        $newProveedor->transferencia = 0;
        $newProveedor->activo = 1;
        $newProveedor->save();

        $newProveedor = new Proveedor();
        $newProveedor->nombre = "RAMIREZ SAGREDO Y COMPAÑÍA";
        $newProveedor->rut = "76293757-3";
        $newProveedor->transferencia = 0;
        $newProveedor->activo = 1;
        $newProveedor->save();

        $newProveedor = new Proveedor();
        $newProveedor->nombre = "COMERC.DE MUEBLES Y SILLAS MAR DEL VALLE SPA";
        $newProveedor->rut = "76296429-5";
        $newProveedor->transferencia = 0;
        $newProveedor->activo = 1;
        $newProveedor->save();

        $newProveedor = new Proveedor();
        $newProveedor->nombre = "PLÁSTICOS MIGUEL IGNACIO JIMENEZ LARA EIRL";
        $newProveedor->rut = "76300380-9";
        $newProveedor->transferencia = 0;
        $newProveedor->activo = 1;
        $newProveedor->save();

        $newProveedor = new Proveedor();
        $newProveedor->nombre = "ANUK MOBILIARIOS COMUNICACIONES LTDA";
        $newProveedor->rut = "76308840-5";
        $newProveedor->transferencia = 0;
        $newProveedor->activo = 1;
        $newProveedor->save();

        $newProveedor = new Proveedor();
        $newProveedor->nombre = "MEDTRONIC CHILE SPA";
        $newProveedor->rut = "76309869-9";
        $newProveedor->transferencia = 0;
        $newProveedor->activo = 1;
        $newProveedor->save();

        $newProveedor = new Proveedor();
        $newProveedor->nombre = "AMEDOPH CHILE";
        $newProveedor->rut = "76310090-1";
        $newProveedor->email = "AURIBE@AMEDOPHCHILE.CL";
        $newProveedor->transferencia = 0;
        $newProveedor->activo = 1;
        $newProveedor->save();

        $newProveedor = new Proveedor();
        $newProveedor->nombre = "MAINZ MEDICAL SPA";
        $newProveedor->rut = "76311356-6";
        $newProveedor->email = "cvergara@mainzmedical.cl";
        $newProveedor->transferencia = 0;
        $newProveedor->activo = 1;
        $newProveedor->save();

        $newProveedor = new Proveedor();
        $newProveedor->nombre = "CAPAC. VIVIAN PAVEZ E I R L";
        $newProveedor->rut = "76315690-7";
        $newProveedor->transferencia = 0;
        $newProveedor->activo = 1;
        $newProveedor->save();

        $newProveedor = new Proveedor();
        $newProveedor->nombre = "DISTRIBUIDORA HEALTHCARE SPA";
        $newProveedor->rut = "76317229-5";
        $newProveedor->transferencia = 0;
        $newProveedor->activo = 1;
        $newProveedor->save();

        $newProveedor = new Proveedor();
        $newProveedor->nombre = "COMERCIAL 2050 SPA";
        $newProveedor->rut = "76324469-5";
        $newProveedor->email = "contacto@2050.cl";
        $newProveedor->transferencia = 0;
        $newProveedor->activo = 1;
        $newProveedor->save();

        $newProveedor = new Proveedor();
        $newProveedor->nombre = "COMERCIALIZADORA E INVERSIONES SURGICORP LTDA";
        $newProveedor->rut = "76327703-8";
        $newProveedor->transferencia = 0;
        $newProveedor->activo = 1;
        $newProveedor->save();

        $newProveedor = new Proveedor();
        $newProveedor->nombre = "ACCESORIOS EQUIPOS Y SUMINISTROS DE OFICINA LDA.";
        $newProveedor->rut = "76327925-1";
        $newProveedor->transferencia = 0;
        $newProveedor->activo = 1;
        $newProveedor->save();

        $newProveedor = new Proveedor();
        $newProveedor->nombre = "ASPEN CHILE S.A.";
        $newProveedor->rut = "76328242-2";
        $newProveedor->email = "HERNAN.ORELLANA@ASPENLATAM.COM";
        $newProveedor->transferencia = 0;
        $newProveedor->activo = 1;
        $newProveedor->save();

        $newProveedor = new Proveedor();
        $newProveedor->nombre = "IMV INGENIERIA LIMITADA";
        $newProveedor->rut = "76330700-K";
        $newProveedor->email = "PLOPEZ@IMVINGENIERIA.CL";
        $newProveedor->transferencia = 0;
        $newProveedor->activo = 1;
        $newProveedor->save();

        $newProveedor = new Proveedor();
        $newProveedor->nombre = "VISIONAMPLIA CAPACITACIONES SPA";
        $newProveedor->rut = "76339636-3";
        $newProveedor->transferencia = 0;
        $newProveedor->activo = 1;
        $newProveedor->save();

        $newProveedor = new Proveedor();
        $newProveedor->nombre = "INSUMOS ORTOPEDICOS Y COMPAÑÍA LIMITADA";
        $newProveedor->rut = "76346004-5";
        $newProveedor->transferencia = 0;
        $newProveedor->activo = 1;
        $newProveedor->save();

        $newProveedor = new Proveedor();
        $newProveedor->nombre = "COMERCIALIZADORA REICOL SPA";
        $newProveedor->rut = "76356855-5";
        $newProveedor->email = "barbaram@reicol.cl";
        $newProveedor->transferencia = 0;
        $newProveedor->activo = 1;
        $newProveedor->save();

        $newProveedor = new Proveedor();
        $newProveedor->nombre = "PORTAL SALUD SPA";
        $newProveedor->rut = "76363205-9";
        $newProveedor->transferencia = 0;
        $newProveedor->activo = 1;
        $newProveedor->save();

        $newProveedor = new Proveedor();
        $newProveedor->nombre = "MEDEX";
        $newProveedor->rut = "76364166-K";
        $newProveedor->transferencia = 0;
        $newProveedor->activo = 1;
        $newProveedor->save();

        $newProveedor = new Proveedor();
        $newProveedor->nombre = "INVERSIONES JSN SPA";
        $newProveedor->rut = "76368858-5";
        $newProveedor->email = "FELICIANAPAVEZ@GMAIL.COM";
        $newProveedor->transferencia = 0;
        $newProveedor->activo = 1;
        $newProveedor->save();

        $newProveedor = new Proveedor();
        $newProveedor->nombre = "OFISILLAS LTDA";
        $newProveedor->rut = "76374069-2";
        $newProveedor->transferencia = 0;
        $newProveedor->activo = 1;
        $newProveedor->save();

        $newProveedor = new Proveedor();
        $newProveedor->nombre = "REPARACIONES BBCC";
        $newProveedor->rut = "76376530-K";
        $newProveedor->email = "gestioncobranza@bbcc.cl";
        $newProveedor->transferencia = 0;
        $newProveedor->activo = 1;
        $newProveedor->save();

        $newProveedor = new Proveedor();
        $newProveedor->nombre = "DROGUERIA GLOBAL PHARMA SPA.";
        $newProveedor->rut = "76389383-9";
        $newProveedor->transferencia = 0;
        $newProveedor->activo = 1;
        $newProveedor->save();

        $newProveedor = new Proveedor();
        $newProveedor->nombre = "INVERSIONES AUSTRALIS HEALTHCARE GROUP S.A.";
        $newProveedor->rut = "76391935-8";
        $newProveedor->email = "PSALDIVAR@AUSTRALISHG.COM";
        $newProveedor->transferencia = 0;
        $newProveedor->activo = 1;
        $newProveedor->save();

        $newProveedor = new Proveedor();
        $newProveedor->nombre = "GESTION CAPITAL CAPAC. LTDA.";
        $newProveedor->rut = "76402345-5";
        $newProveedor->transferencia = 0;
        $newProveedor->activo = 1;
        $newProveedor->save();

        $newProveedor = new Proveedor();
        $newProveedor->nombre = "PEDRO IBAÑEZ E HIJOS LTDA.";
        $newProveedor->rut = "76405630-2";
        $newProveedor->email = "VENTASIBALIM@GMAIL.COM";
        $newProveedor->transferencia = 0;
        $newProveedor->activo = 1;
        $newProveedor->save();

        $newProveedor = new Proveedor();
        $newProveedor->nombre = "RADIOLOGO MIRKO PAPIC VLADILO E.R.L";
        $newProveedor->rut = "76411284-9";
        $newProveedor->transferencia = 0;
        $newProveedor->activo = 1;
        $newProveedor->save();

        $newProveedor = new Proveedor();
        $newProveedor->nombre = "SERV.ODONT Y ANEST LOPEZ Y CIA LTDA";
        $newProveedor->rut = "76416741-4";
        $newProveedor->transferencia = 0;
        $newProveedor->activo = 1;
        $newProveedor->save();

        $newProveedor = new Proveedor();
        $newProveedor->nombre = "CRISTIAN RODRIGO ORTIZ VILLARROEL";
        $newProveedor->rut = "76420162-0";
        $newProveedor->email = "CORTIZVILLA@GMAIL.COM";
        $newProveedor->transferencia = 0;
        $newProveedor->activo = 1;
        $newProveedor->save();

        $newProveedor = new Proveedor();
        $newProveedor->nombre = "COMERCIALIZADORA CARLOS VALDIVIA LTDA.";
        $newProveedor->rut = "76422630-5";
        $newProveedor->email = "INFO@AUDIOMEDICAL.CL";
        $newProveedor->transferencia = 0;
        $newProveedor->activo = 1;
        $newProveedor->save();

        $newProveedor = new Proveedor();
        $newProveedor->nombre = "EVENCO LTDA.";
        $newProveedor->rut = "76427850-K";
        $newProveedor->transferencia = 0;
        $newProveedor->activo = 1;
        $newProveedor->save();

        $newProveedor = new Proveedor();
        $newProveedor->nombre = "COMERCIAL BAILEY SPA";
        $newProveedor->rut = "76428624-3";
        $newProveedor->transferencia = 0;
        $newProveedor->activo = 1;
        $newProveedor->save();

        $newProveedor = new Proveedor();
        $newProveedor->nombre = "SERVICIO DIAG. CLINICA ALEMANA DE SANTIAGO LTDA.";
        $newProveedor->rut = "76436880-0";
        $newProveedor->email = "EBURGOS@ALEMANA.CL";
        $newProveedor->transferencia = 0;
        $newProveedor->activo = 1;
        $newProveedor->save();

        $newProveedor = new Proveedor();
        $newProveedor->nombre = "ASTRAZENECA S.A.";
        $newProveedor->rut = "76447530-5";
        $newProveedor->email = "MANUEL.OLIVEROS@NOVOFARMA.COM";
        $newProveedor->transferencia = 0;
        $newProveedor->activo = 1;
        $newProveedor->save();

        $newProveedor = new Proveedor();
        $newProveedor->nombre = "JAIME DEIK Y CIA LIMITADA";
        $newProveedor->rut = "76450030-K";
        $newProveedor->transferencia = 0;
        $newProveedor->activo = 1;
        $newProveedor->save();

        $newProveedor = new Proveedor();
        $newProveedor->nombre = "SURTI VENTAS LTDA";
        $newProveedor->rut = "76462500-5";
        $newProveedor->email = "GSILVAC@SURTIVENTAS.CL";
        $newProveedor->transferencia = 0;
        $newProveedor->activo = 1;
        $newProveedor->save();

        $newProveedor = new Proveedor();
        $newProveedor->nombre = "COMERCIAL CMS MEDICAL";
        $newProveedor->rut = "76462810-1";
        $newProveedor->email = "alejandro.alcayaga@cmsmedical.cl";
        $newProveedor->transferencia = 0;
        $newProveedor->activo = 1;
        $newProveedor->save();

        $newProveedor = new Proveedor();
        $newProveedor->nombre = "COSTANERA NORTE";
        $newProveedor->rut = "76496130-7";
        $newProveedor->transferencia = 0;
        $newProveedor->activo = 1;
        $newProveedor->save();

        $newProveedor = new Proveedor();
        $newProveedor->nombre = "SOCIEDAD COMERCIAL SNAPOLI LTDA.";
        $newProveedor->rut = "76512530-8";
        $newProveedor->transferencia = 0;
        $newProveedor->activo = 1;
        $newProveedor->save();

        $newProveedor = new Proveedor();
        $newProveedor->nombre = "SERVICIOS Y GESTION S.A.";
        $newProveedor->rut = "76519210-2";
        $newProveedor->transferencia = 0;
        $newProveedor->activo = 1;
        $newProveedor->save();

        $newProveedor = new Proveedor();
        $newProveedor->nombre = "COMERCIAL SIMED LTDA";
        $newProveedor->rut = "76532670-2";
        $newProveedor->email = "GERARDO SALGADO";
        $newProveedor->transferencia = 0;
        $newProveedor->activo = 1;
        $newProveedor->save();

        $newProveedor = new Proveedor();
        $newProveedor->nombre = "LW CAPACITACIONES LTDA.";
        $newProveedor->rut = "76559960-1";
        $newProveedor->transferencia = 0;
        $newProveedor->activo = 1;
        $newProveedor->save();

        $newProveedor = new Proveedor();
        $newProveedor->nombre = "C&C IMP. Y EXP. LTDA.";
        $newProveedor->rut = "76564000-8";
        $newProveedor->email = "ACONTARDO@CCIMPEX.CL";
        $newProveedor->transferencia = 0;
        $newProveedor->activo = 1;
        $newProveedor->save();

        $newProveedor = new Proveedor();
        $newProveedor->nombre = "SEGURA Y ROMERO LTDA.";
        $newProveedor->rut = "76580130-3";
        $newProveedor->email = "INGE-CAPLTDA@HOTMAIL.COM";
        $newProveedor->transferencia = 0;
        $newProveedor->activo = 1;
        $newProveedor->save();

        $newProveedor = new Proveedor();
        $newProveedor->nombre = "CARLOS ALBERTO PALMA RIVERA Y OTRO LTDA.";
        $newProveedor->rut = "76596570-5";
        $newProveedor->email = "COBRANZAS@ALCA.CL";
        $newProveedor->transferencia = 0;
        $newProveedor->activo = 1;
        $newProveedor->save();

        $newProveedor = new Proveedor();
        $newProveedor->nombre = "ACSA CAPACITACIÓN LTDA.";
        $newProveedor->rut = "76620950-5";
        $newProveedor->email = "CAPACITA@ACSACONSULTORES.CL";
        $newProveedor->transferencia = 0;
        $newProveedor->activo = 1;
        $newProveedor->save();

        $newProveedor = new Proveedor();
        $newProveedor->nombre = "IMPORTADORA Y COMERCIALIZADORA RE-MED. LTDA.";
        $newProveedor->rut = "76628610-0";
        $newProveedor->email = "CONTABILIDAD@RE-MED.CL";
        $newProveedor->transferencia = 0;
        $newProveedor->activo = 1;
        $newProveedor->save();

        $newProveedor = new Proveedor();
        $newProveedor->nombre = "GLOBAL MED";
        $newProveedor->rut = "76631490-2";
        $newProveedor->email = "ADMINISTRACION@GLOBAL-MED.CL";
        $newProveedor->transferencia = 0;
        $newProveedor->activo = 1;
        $newProveedor->save();

        $newProveedor = new Proveedor();
        $newProveedor->nombre = "LABVITALIS S.A.";
        $newProveedor->rut = "76642770-7";
        $newProveedor->transferencia = 0;
        $newProveedor->activo = 1;
        $newProveedor->save();

        $newProveedor = new Proveedor();
        $newProveedor->nombre = "BIOSERVICE LTDA.";
        $newProveedor->rut = "76644150-5";
        $newProveedor->email = "ORIFFO@BIOSERVICELTDA.COM";
        $newProveedor->transferencia = 0;
        $newProveedor->activo = 1;
        $newProveedor->save();

        $newProveedor = new Proveedor();
        $newProveedor->nombre = "SOC COM IMPROLAB LTDA";
        $newProveedor->rut = "76667510-7";
        $newProveedor->email = "INFO@IMPROLAB.CL";
        $newProveedor->transferencia = 0;
        $newProveedor->activo = 1;
        $newProveedor->save();

        $newProveedor = new Proveedor();
        $newProveedor->nombre = "OPKO CHILE S.A.";
        $newProveedor->rut = "76669630-9";
        $newProveedor->email = "XFUENTES@OPKO.COM";
        $newProveedor->transferencia = 0;
        $newProveedor->activo = 1;
        $newProveedor->save();

        $newProveedor = new Proveedor();
        $newProveedor->nombre = "IMP. Y COM. HOFMANN MEDICAL LTDA.";
        $newProveedor->rut = "76695020-5";
        $newProveedor->email = "MACOSTA@HOFMANN.CL";
        $newProveedor->transferencia = 0;
        $newProveedor->activo = 1;
        $newProveedor->save();

        $newProveedor = new Proveedor();
        $newProveedor->nombre = "NOVO NORDISK FARMACÉUTICA LTDA.";
        $newProveedor->rut = "76711330-7";
        $newProveedor->email = "MCGB@NOVONORDISK.COM";
        $newProveedor->transferencia = 0;
        $newProveedor->activo = 1;
        $newProveedor->save();

        $newProveedor = new Proveedor();
        $newProveedor->nombre = "MACROCAP CAPACITACIÓN LIMITADA";
        $newProveedor->rut = "76725410-5";
        $newProveedor->email = "ADMINISTRACION@MACROCAP-OTEC.COM";
        $newProveedor->transferencia = 0;
        $newProveedor->activo = 1;
        $newProveedor->save();

        $newProveedor = new Proveedor();
        $newProveedor->nombre = "NOVOSALUD SPA";
        $newProveedor->rut = "76768780-K";
        $newProveedor->email = "JOTAROLA@NOVOSALUD.CL";
        $newProveedor->transferencia = 0;
        $newProveedor->activo = 1;
        $newProveedor->save();

        $newProveedor = new Proveedor();
        $newProveedor->nombre = "CARESTREAM HEALTH CHILE LTDA.";
        $newProveedor->rut = "76773210-4";
        $newProveedor->transferencia = 0;
        $newProveedor->activo = 1;
        $newProveedor->save();

        $newProveedor = new Proveedor();
        $newProveedor->nombre = "BUSCH CHILE S.A";
        $newProveedor->rut = "76774170-7";
        $newProveedor->transferencia = 0;
        $newProveedor->activo = 1;
        $newProveedor->save();

        $newProveedor = new Proveedor();
        $newProveedor->nombre = "ALEJANDRO RUIZ-TAGLE DE LA CERDA";
        $newProveedor->rut = "76777110-K";
        $newProveedor->email = "ARUIZTAGLE@REDCONSULTORES.CL";
        $newProveedor->transferencia = 0;
        $newProveedor->activo = 1;
        $newProveedor->save();

        $newProveedor = new Proveedor();
        $newProveedor->nombre = "COMERCIALIZADORA WILLIAM VILLALOBOS";
        $newProveedor->rut = "76794670-8";
        $newProveedor->email = "JHERNANDEZ@MADAWISYSTEM.CL";
        $newProveedor->transferencia = 0;
        $newProveedor->activo = 1;
        $newProveedor->save();

        $newProveedor = new Proveedor();
        $newProveedor->nombre = "COMERCIALIZADORA SP DIGITAL LTDA";
        $newProveedor->rut = "76799430-3";
        $newProveedor->transferencia = 0;
        $newProveedor->activo = 1;
        $newProveedor->save();

        $newProveedor = new Proveedor();
        $newProveedor->nombre = "SERVICIOS DE FUMIGACIÓN SIN PLAGA LTDA.";
        $newProveedor->rut = "76811010-7";
        $newProveedor->email = "ISILVA@FUMIGACIONCHILE.CL";
        $newProveedor->transferencia = 0;
        $newProveedor->activo = 1;
        $newProveedor->save();

        $newProveedor = new Proveedor();
        $newProveedor->nombre = "DISTRIBUIDORA DE PRODUCTOS MASIVOS";
        $newProveedor->rut = "76814370-6";
        $newProveedor->email = "INFO@GRV.CL";
        $newProveedor->transferencia = 0;
        $newProveedor->activo = 1;
        $newProveedor->save();

        $newProveedor = new Proveedor();
        $newProveedor->nombre = "SILMAG CHILE S.A";
        $newProveedor->rut = "76817870-4";
        $newProveedor->email = "VENTAS@SILMAGCHILE.CL";
        $newProveedor->transferencia = 0;
        $newProveedor->activo = 1;
        $newProveedor->save();

        $newProveedor = new Proveedor();
        $newProveedor->nombre = "FARMACÉUTICA CARIBEAN LIMITADA";
        $newProveedor->rut = "76830090-9";
        $newProveedor->email = "MARLENET@CARIBEANPHARMA.CL";
        $newProveedor->transferencia = 0;
        $newProveedor->activo = 1;
        $newProveedor->save();

        $newProveedor = new Proveedor();
        $newProveedor->nombre = "FAYMO SA";
        $newProveedor->rut = "76837310-8";
        $newProveedor->email = "ADIAZ@FAYMO.CL";
        $newProveedor->transferencia = 0;
        $newProveedor->activo = 1;
        $newProveedor->save();

        $newProveedor = new Proveedor();
        $newProveedor->nombre = "ANA MARIA COLLEMANN CACERES";
        $newProveedor->rut = "7684209-4";
        $newProveedor->transferencia = 0;
        $newProveedor->activo = 1;
        $newProveedor->save();

        $newProveedor = new Proveedor();
        $newProveedor->nombre = "SERVICIOS ELECTRONICOS MECANICOS Y TERMICOS LTDA.";
        $newProveedor->rut = "76844900-7";
        $newProveedor->email = "LORENA@SERMETER.CL";
        $newProveedor->transferencia = 0;
        $newProveedor->activo = 1;
        $newProveedor->save();

        $newProveedor = new Proveedor();
        $newProveedor->nombre = "LABORATORIOS RIDER LTDA";
        $newProveedor->rut = "76845190-7";
        $newProveedor->email = "SUSSIE.GALVEZ@SYNTHON.COM";
        $newProveedor->transferencia = 0;
        $newProveedor->activo = 1;
        $newProveedor->save();

        $newProveedor = new Proveedor();
        $newProveedor->nombre = "ESPECIALISTAS EN ESTERILIZACION Y ENVASE CHILE SA";
        $newProveedor->rut = "76845730-1";
        $newProveedor->email = "MARIANELA.PAILALEF@GRUPOEEE.COM";
        $newProveedor->transferencia = 0;
        $newProveedor->activo = 1;
        $newProveedor->save();

        $newProveedor = new Proveedor();
        $newProveedor->nombre = "SOC. DE SERV. DE TRANSPORTE DE PACIENTES MOVICARE SPA";
        $newProveedor->rut = "76847600-4";
        $newProveedor->email = "JARRUE@MOVICARE.CL";
        $newProveedor->transferencia = 0;
        $newProveedor->activo = 1;
        $newProveedor->save();

        $newProveedor = new Proveedor();
        $newProveedor->nombre = "BIOSCOM CHILE LTDA.";
        $newProveedor->rut = "76919840-7";
        $newProveedor->transferencia = 0;
        $newProveedor->activo = 1;
        $newProveedor->save();

        $newProveedor = new Proveedor();
        $newProveedor->nombre = "COM CROVETTO CHILE LTDA.";
        $newProveedor->rut = "76922930-2";
        $newProveedor->transferencia = 0;
        $newProveedor->activo = 1;
        $newProveedor->save();

        $newProveedor = new Proveedor();
        $newProveedor->nombre = "ETHON PHARMACEUTICALS COM. IMP. EXP. Y DIST. SPA";
        $newProveedor->rut = "76956140-4";
        $newProveedor->email = "ETHONCOBRANZA@ETHONPHARMACEUTICALS.COM";
        $newProveedor->transferencia = 0;
        $newProveedor->activo = 1;
        $newProveedor->save();

        $newProveedor = new Proveedor();
        $newProveedor->nombre = "JUAN AGUSTIN LARRAGIBEL BORQUEZ";
        $newProveedor->rut = "7695706-1";
        $newProveedor->transferencia = 0;
        $newProveedor->activo = 1;
        $newProveedor->save();

        $newProveedor = new Proveedor();
        $newProveedor->nombre = "SOMA S.A";
        $newProveedor->rut = "76976440-2";
        $newProveedor->email = "FBRAVO@SOMACIRUGIA.CL";
        $newProveedor->transferencia = 0;
        $newProveedor->activo = 1;
        $newProveedor->save();

        $newProveedor = new Proveedor();
        $newProveedor->nombre = "FLAVIA WOLSZYN Y CIA";
        $newProveedor->rut = "77005780-9";
        $newProveedor->email = "CONTACTO@GREENMEDICAL.CL";
        $newProveedor->transferencia = 0;
        $newProveedor->activo = 1;
        $newProveedor->save();

        $newProveedor = new Proveedor();
        $newProveedor->nombre = "ASESORIAS EN SEGURIDAD Y PREVENCION LIMITADA";
        $newProveedor->rut = "77006750-2";
        $newProveedor->transferencia = 0;
        $newProveedor->activo = 1;
        $newProveedor->save();

        $newProveedor = new Proveedor();
        $newProveedor->nombre = "COMERCIAL RED OFFICE LIMITADA";
        $newProveedor->rut = "77012870-6";
        $newProveedor->email = "COBRANZASANTIAGO@REDOFFICE.CL";
        $newProveedor->transferencia = 0;
        $newProveedor->activo = 1;
        $newProveedor->save();

        $newProveedor = new Proveedor();
        $newProveedor->nombre = "GUIÑAZU TRANSFER LIMITADA";
        $newProveedor->rut = "77018550-5";
        $newProveedor->transferencia = 0;
        $newProveedor->activo = 1;
        $newProveedor->save();

        $newProveedor = new Proveedor();
        $newProveedor->nombre = "COMERCIAL LOGROÑO LIMITADA";
        $newProveedor->rut = "77024990-2";
        $newProveedor->email = "COMLOGRONO@LOGRONO.TIE.CL";
        $newProveedor->transferencia = 0;
        $newProveedor->activo = 1;
        $newProveedor->save();

        $newProveedor = new Proveedor();
        $newProveedor->nombre = "SOCIEDAD SALSA COLOR IMPRESIONES LIMITADA";
        $newProveedor->rut = "77030480-6";
        $newProveedor->email = "SALSACOLOR@SALSACOLOR.CL";
        $newProveedor->transferencia = 0;
        $newProveedor->activo = 1;
        $newProveedor->save();

        $newProveedor = new Proveedor();
        $newProveedor->nombre = "AMARAL Y COMPAÑÍA LTDA.";
        $newProveedor->rut = "77049910-0";
        $newProveedor->email = "VENTAS@POSITRONPHARMA.CL";
        $newProveedor->transferencia = 0;
        $newProveedor->activo = 1;
        $newProveedor->save();

        $newProveedor = new Proveedor();
        $newProveedor->nombre = "IMP Y COMERCIALIZADORA PARACARE LTDA";
        $newProveedor->rut = "77102620-6";
        $newProveedor->email = "CARRIAGADAT@REHACARE.CL";
        $newProveedor->transferencia = 0;
        $newProveedor->activo = 1;
        $newProveedor->save();

        $newProveedor = new Proveedor();
        $newProveedor->nombre = "PERKIN ELMER CHILE LTDA.";
        $newProveedor->rut = "77116540-0";
        $newProveedor->email = "OLGA.RACERO@PERKINELMER.COM";
        $newProveedor->transferencia = 0;
        $newProveedor->activo = 1;
        $newProveedor->save();

        $newProveedor = new Proveedor();
        $newProveedor->nombre = "SOCIEDAD IMPORTADORA OPTIVISION LTDA.";
        $newProveedor->rut = "77190880-2";
        $newProveedor->email = "OPTIVISION@OPTIVISION.CL";
        $newProveedor->transferencia = 0;
        $newProveedor->activo = 1;
        $newProveedor->save();

        $newProveedor = new Proveedor();
        $newProveedor->nombre = "SERVICIOS MÉDICOS SANTA MARIA";
        $newProveedor->rut = "77200240-8";
        $newProveedor->email = "mfernandez@clinicasantamaria.cl";
        $newProveedor->transferencia = 0;
        $newProveedor->activo = 1;
        $newProveedor->save();

        $newProveedor = new Proveedor();
        $newProveedor->nombre = "RICARDO CID TECNOLOGIAS";
        $newProveedor->rut = "77200690-K";
        $newProveedor->email = "VENTASPROVI@RCTEC.CL";
        $newProveedor->transferencia = 0;
        $newProveedor->activo = 1;
        $newProveedor->save();

        $newProveedor = new Proveedor();
        $newProveedor->nombre = "DANILLA LACRAMPETE LTDA";
        $newProveedor->rut = "77202390-1";
        $newProveedor->transferencia = 0;
        $newProveedor->activo = 1;
        $newProveedor->save();

        $newProveedor = new Proveedor();
        $newProveedor->nombre = "COMERCIAL KENDALL CHILE LIMITADA";
        $newProveedor->rut = "77237150-0";
        $newProveedor->email = "CRISTIANRODOLFO.SAEZ@COVIDIEN.COM";
        $newProveedor->transferencia = 0;
        $newProveedor->activo = 1;
        $newProveedor->save();

        $newProveedor = new Proveedor();
        $newProveedor->nombre = "SERV. PROF. DE CAPACITACION LTDA.";
        $newProveedor->rut = "77237960-9";
        $newProveedor->transferencia = 0;
        $newProveedor->activo = 1;
        $newProveedor->save();

        $newProveedor = new Proveedor();
        $newProveedor->nombre = "MURUA Y ASOCIADOS LIMITADA";
        $newProveedor->rut = "77239430-6";
        $newProveedor->email = "MURUAYASOCIADOSLTDA@YAHOO.ES";
        $newProveedor->transferencia = 0;
        $newProveedor->activo = 1;
        $newProveedor->save();

        $newProveedor = new Proveedor();
        $newProveedor->nombre = "SOCIEDAD COMERCIAL NEIRA Y FONSECA SPA";
        $newProveedor->rut = "77252670-9";
        $newProveedor->email = "EDILAB@EDILAB.CL";
        $newProveedor->transferencia = 0;
        $newProveedor->activo = 1;
        $newProveedor->save();

        $newProveedor = new Proveedor();
        $newProveedor->nombre = "ENVASES PROGRESO LIMITADA";
        $newProveedor->rut = "77261630-9";
        $newProveedor->transferencia = 0;
        $newProveedor->activo = 1;
        $newProveedor->save();

        $newProveedor = new Proveedor();
        $newProveedor->nombre = "HERNANDEZ Y PEREGALLO LIMITADA";
        $newProveedor->rut = "77300980-5";
        $newProveedor->transferencia = 0;
        $newProveedor->activo = 1;
        $newProveedor->save();

        $newProveedor = new Proveedor();
        $newProveedor->nombre = "MGM PRODUCTOS MÉDICOS";
        $newProveedor->rut = "77325170-3";
        $newProveedor->email = "VENTAS@MGM.CL";
        $newProveedor->transferencia = 0;
        $newProveedor->activo = 1;
        $newProveedor->save();

        $newProveedor = new Proveedor();
        $newProveedor->nombre = "TRANSPORTES GONZALEZ LTDA.";
        $newProveedor->rut = "77330440-8";
        $newProveedor->transferencia = 0;
        $newProveedor->activo = 1;
        $newProveedor->save();

        $newProveedor = new Proveedor();
        $newProveedor->nombre = "DISTRIBUIDORA DE ESPUMA Y FIELTROS Y CÍA";
        $newProveedor->rut = "77338960-8";
        $newProveedor->email = "FIBRAPLAS7@GMAIL.COM";
        $newProveedor->transferencia = 0;
        $newProveedor->activo = 1;
        $newProveedor->save();

        $newProveedor = new Proveedor();
        $newProveedor->nombre = "COMERCIAL SERCODATA";
        $newProveedor->rut = "77339180-7";
        $newProveedor->email = "SERCODATA@TIE.CL";
        $newProveedor->transferencia = 0;
        $newProveedor->activo = 1;
        $newProveedor->save();

        $newProveedor = new Proveedor();
        $newProveedor->nombre = "SERVICIOS CLÍNICA ALEMANA LTDA.";
        $newProveedor->rut = "77413290-2";
        $newProveedor->email = "EBURGOS@ALEMANA.CL";
        $newProveedor->transferencia = 0;
        $newProveedor->activo = 1;
        $newProveedor->save();

        $newProveedor = new Proveedor();
        $newProveedor->nombre = "CORDERO Y MICHAUD LIMITADA";
        $newProveedor->rut = "77420940-9";
        $newProveedor->transferencia = 0;
        $newProveedor->activo = 1;
        $newProveedor->save();

        $newProveedor = new Proveedor();
        $newProveedor->nombre = "LIFE TECHNOLIGIES CHILE SPA";
        $newProveedor->rut = "77453910-7";
        $newProveedor->email = "mabel.berrios@thermofisher.com";
        $newProveedor->transferencia = 0;
        $newProveedor->activo = 1;
        $newProveedor->save();

        $newProveedor = new Proveedor();
        $newProveedor->nombre = "DIAMED EQUIPOS MÉDICOS LTDA.";
        $newProveedor->rut = "77454490-9";
        $newProveedor->transferencia = 0;
        $newProveedor->activo = 1;
        $newProveedor->save();

        $newProveedor = new Proveedor();
        $newProveedor->nombre = "CORP MEDICAL CHILE LTDA.";
        $newProveedor->rut = "77461630-6";
        $newProveedor->email = "CORPMEDICAL@CORPMEDICAL.CL";
        $newProveedor->transferencia = 0;
        $newProveedor->activo = 1;
        $newProveedor->save();

        $newProveedor = new Proveedor();
        $newProveedor->nombre = "VERMED EQUIPOS MÉDICOS LTDA.";
        $newProveedor->rut = "77463270-0";
        $newProveedor->transferencia = 0;
        $newProveedor->activo = 1;
        $newProveedor->save();

        $newProveedor = new Proveedor();
        $newProveedor->nombre = "FRESENIUS KABI CHILE LTDA.";
        $newProveedor->rut = "77478120-K";
        $newProveedor->email = "COBRANZAS@FRESENIUS-KABI.CL";
        $newProveedor->transferencia = 0;
        $newProveedor->activo = 1;
        $newProveedor->save();

        $newProveedor = new Proveedor();
        $newProveedor->nombre = "IMPORTADORA Y EXPORTADORA CAMIR LTDA.";
        $newProveedor->rut = "77487960-9";
        $newProveedor->email = "CAROLINA.PINTO@CAMIR.CL";
        $newProveedor->transferencia = 0;
        $newProveedor->activo = 1;
        $newProveedor->save();

        $newProveedor = new Proveedor();
        $newProveedor->nombre = "INDUSTRIAL Y COMERCIAL MEIGGS 51";
        $newProveedor->rut = "77558540-4";
        $newProveedor->transferencia = 0;
        $newProveedor->activo = 1;
        $newProveedor->save();

        $newProveedor = new Proveedor();
        $newProveedor->nombre = "LABORATORIO CHILE S.A.";
        $newProveedor->rut = "77596940-7";
        $newProveedor->email = "IVX@LABORATORIOCHILE.CL";
        $newProveedor->transferencia = 0;
        $newProveedor->activo = 1;
        $newProveedor->save();

        $newProveedor = new Proveedor();
        $newProveedor->nombre = "COMERCIALIZADORA CORDILLERA";
        $newProveedor->rut = "77599020-1";
        $newProveedor->email = "JCROJAS@CPC-CORDILLERA.CL";
        $newProveedor->transferencia = 0;
        $newProveedor->activo = 1;
        $newProveedor->save();

        $newProveedor = new Proveedor();
        $newProveedor->nombre = "SERVICIOS ASESORIAS Y SOLUCIONES FINANCIERAS LTDA.";
        $newProveedor->rut = "77606220-0";
        $newProveedor->email = "JARENAS@SASF.CL";
        $newProveedor->transferencia = 0;
        $newProveedor->activo = 1;
        $newProveedor->save();

        $newProveedor = new Proveedor();
        $newProveedor->nombre = "INTEGRAL SERVICE LTDA";
        $newProveedor->rut = "77662190-0";
        $newProveedor->email = "GLORIA.YAMCA@INTEGRALSERVICE.CL";
        $newProveedor->transferencia = 0;
        $newProveedor->activo = 1;
        $newProveedor->save();

        $newProveedor = new Proveedor();
        $newProveedor->nombre = "INDUSTRIAL Y COMERCIAL SAN DIEGO LTDA.";
        $newProveedor->rut = "77662300-8";
        $newProveedor->email = "ICSD.VENTAS@GMAIL.COM";
        $newProveedor->transferencia = 0;
        $newProveedor->activo = 1;
        $newProveedor->save();

        $newProveedor = new Proveedor();
        $newProveedor->nombre = "TEKNICOMLN SERVELECTRICOS COMUNICY OBRAS CIVIL";
        $newProveedor->rut = "77670170-K";
        $newProveedor->email = "VENTAS@TEKNICOM.LN.CL";
        $newProveedor->transferencia = 0;
        $newProveedor->activo = 1;
        $newProveedor->save();

        $newProveedor = new Proveedor();
        $newProveedor->nombre = "LUISA REYES CASTRO";
        $newProveedor->rut = "7768006-3";
        $newProveedor->transferencia = 0;
        $newProveedor->activo = 1;
        $newProveedor->save();

        $newProveedor = new Proveedor();
        $newProveedor->nombre = "COMERCIALIZADORA MEDISAN LTDA.";
        $newProveedor->rut = "77693540-9";
        $newProveedor->email = "VENTAS@MEDISAN.CL";
        $newProveedor->transferencia = 0;
        $newProveedor->activo = 1;
        $newProveedor->save();

        $newProveedor = new Proveedor();
        $newProveedor->nombre = "TECNOLOGIA DE EMPRESAS LTDA.";
        $newProveedor->rut = "77694450-5";
        $newProveedor->transferencia = 0;
        $newProveedor->activo = 1;
        $newProveedor->save();

        $newProveedor = new Proveedor();
        $newProveedor->nombre = "AMILAB ARTICULOS MEDICOS Y DE LABORATORIOS LTDA";
        $newProveedor->rut = "77700690-8";
        $newProveedor->transferencia = 0;
        $newProveedor->activo = 1;
        $newProveedor->save();

        $newProveedor = new Proveedor();
        $newProveedor->nombre = "COMERCIALIZADORA TELENET LTDA";
        $newProveedor->rut = "77700780-7";
        $newProveedor->transferencia = 0;
        $newProveedor->activo = 1;
        $newProveedor->save();

        $newProveedor = new Proveedor();
        $newProveedor->nombre = "PROJECTA SERV. E INSTALACIONES INDS. Y MEDICI.";
        $newProveedor->rut = "77720240-5";
        $newProveedor->email = "CONTACTO@PROJECTA.CL";
        $newProveedor->transferencia = 0;
        $newProveedor->activo = 1;
        $newProveedor->save();

        $newProveedor = new Proveedor();
        $newProveedor->nombre = "MÜNCHEN MEDICAL LIMITADA";
        $newProveedor->rut = "77725450-2";
        $newProveedor->email = "INFO@MUNCHENMEDICAL.CL";
        $newProveedor->transferencia = 0;
        $newProveedor->activo = 1;
        $newProveedor->save();

        $newProveedor = new Proveedor();
        $newProveedor->nombre = "LOPEZ Y SALVARANI ASOC. LTDA.";
        $newProveedor->rut = "77732380-6";
        $newProveedor->transferencia = 0;
        $newProveedor->activo = 1;
        $newProveedor->save();

        $newProveedor = new Proveedor();
        $newProveedor->nombre = "ING. INFORMATICA PARA LA SALUD LTDA";
        $newProveedor->rut = "77737630-6";
        $newProveedor->email = "ENAVARRETE@REBSOL.CL";
        $newProveedor->transferencia = 0;
        $newProveedor->activo = 1;
        $newProveedor->save();

        $newProveedor = new Proveedor();
        $newProveedor->nombre = "CORTES Y CORTES LTDA";
        $newProveedor->rut = "77739850-4";
        $newProveedor->email = "CONTACTO@MEDELA.CL";
        $newProveedor->transferencia = 0;
        $newProveedor->activo = 1;
        $newProveedor->save();

        $newProveedor = new Proveedor();
        $newProveedor->nombre = "COM. DE PROD. MEDICOS Y DEPORTIVOS PTM CHILE LTDA.";
        $newProveedor->rut = "77749210-1";
        $newProveedor->email = "claudio.salinas@ptm.cl";
        $newProveedor->transferencia = 0;
        $newProveedor->activo = 1;
        $newProveedor->save();

        $newProveedor = new Proveedor();
        $newProveedor->nombre = "GOOD HEALTH COMPANY";
        $newProveedor->rut = "77772550-5";
        $newProveedor->email = "GHC@TIE.CL";
        $newProveedor->transferencia = 0;
        $newProveedor->activo = 1;
        $newProveedor->save();

        $newProveedor = new Proveedor();
        $newProveedor->nombre = "DROGUERÍA FARMOQUÍMICA DEL PACÍFICO LIMITADA";
        $newProveedor->rut = "77781470-2";
        $newProveedor->email = "NSOTO@FQP.CL";
        $newProveedor->transferencia = 0;
        $newProveedor->activo = 1;
        $newProveedor->save();

        $newProveedor = new Proveedor();
        $newProveedor->nombre = "DOLPHIN MEDICAL Y CIA. LTDA.";
        $newProveedor->rut = "77783690-0";
        $newProveedor->email = "CVARAS@DOLPHINMEDICAL.CL";
        $newProveedor->transferencia = 0;
        $newProveedor->activo = 1;
        $newProveedor->save();

        $newProveedor = new Proveedor();
        $newProveedor->nombre = "RECETARIO MAGISTRAL ENDOVENOSO S.A.";
        $newProveedor->rut = "77807840-6";
        $newProveedor->email = "COBRANZAS@FRESENIUS-KABI.CL";
        $newProveedor->transferencia = 0;
        $newProveedor->activo = 1;
        $newProveedor->save();

        $newProveedor = new Proveedor();
        $newProveedor->nombre = "ASYSTEC LTDA.";
        $newProveedor->rut = "77833790-8";
        $newProveedor->email = "INFO@ASYSTEC.TIE.CL";
        $newProveedor->transferencia = 0;
        $newProveedor->activo = 1;
        $newProveedor->save();

        $newProveedor = new Proveedor();
        $newProveedor->nombre = "CANEO Y ASOCIADOS LTDA.";
        $newProveedor->rut = "77844630-8";
        $newProveedor->email = "CANEOASOCIADOS@MEDIKA.TIE.CL";
        $newProveedor->transferencia = 0;
        $newProveedor->activo = 1;
        $newProveedor->save();

        $newProveedor = new Proveedor();
        $newProveedor->nombre = "INVERSIONES E INMOBILIARIA MARTÍN Y RAMÍREZ LTDA.";
        $newProveedor->rut = "77866770-3";
        $newProveedor->email = "ECONCHA@MAKOMED.CL";
        $newProveedor->transferencia = 0;
        $newProveedor->activo = 1;
        $newProveedor->save();

        $newProveedor = new Proveedor();
        $newProveedor->nombre = "COMERCIAL GENERAL BIOTEC CHILE LTDA";
        $newProveedor->rut = "77872090-6";
        $newProveedor->email = "YSUWALSKY@GENERALBIOTEC.CL";
        $newProveedor->transferencia = 0;
        $newProveedor->activo = 1;
        $newProveedor->save();

        $newProveedor = new Proveedor();
        $newProveedor->nombre = "COMERCIAL GRAFIMERCK LTDA";
        $newProveedor->rut = "77896110-3";
        $newProveedor->transferencia = 0;
        $newProveedor->activo = 1;
        $newProveedor->save();

        $newProveedor = new Proveedor();
        $newProveedor->nombre = "COMERCIALIZADORA SMARTVISION";
        $newProveedor->rut = "77911810-K";
        $newProveedor->email = "INFO@SVSM.CL";
        $newProveedor->transferencia = 0;
        $newProveedor->activo = 1;
        $newProveedor->save();

        $newProveedor = new Proveedor();
        $newProveedor->nombre = "SOCCOMERINSUPROMED LTDA";
        $newProveedor->rut = "77961550-2";
        $newProveedor->email = "MARCELAPINTO@INSUPROMED.CL";
        $newProveedor->transferencia = 0;
        $newProveedor->activo = 1;
        $newProveedor->save();

        $newProveedor = new Proveedor();
        $newProveedor->nombre = "GESTION FINANCIERA LTDA.";
        $newProveedor->rut = "77970490-4";
        $newProveedor->transferencia = 0;
        $newProveedor->activo = 1;
        $newProveedor->save();

        $newProveedor = new Proveedor();
        $newProveedor->nombre = "EQUIPOS Y TRATAMIENTOS DE AGUA LTDA.";
        $newProveedor->rut = "78004340-7";
        $newProveedor->transferencia = 0;
        $newProveedor->activo = 1;
        $newProveedor->save();

        $newProveedor = new Proveedor();
        $newProveedor->nombre = "PROTEX";
        $newProveedor->rut = "78017820-5";
        $newProveedor->email = "GLORIA.PROTEX@GMAIL.COM";
        $newProveedor->transferencia = 0;
        $newProveedor->activo = 1;
        $newProveedor->save();

        $newProveedor = new Proveedor();
        $newProveedor->nombre = "ETEX FARMACEUTICA LTDA";
        $newProveedor->rut = "78026330-K";
        $newProveedor->email = "GERALD.A.PACHECO@GSK.COM";
        $newProveedor->transferencia = 0;
        $newProveedor->activo = 1;
        $newProveedor->save();

        $newProveedor = new Proveedor();
        $newProveedor->nombre = "LABORATORIOS LBC LTDA.";
        $newProveedor->rut = "78028610-5";
        $newProveedor->email = "DGOMEZ@BARTUCEVIC.CL";
        $newProveedor->transferencia = 0;
        $newProveedor->activo = 1;
        $newProveedor->save();

        $newProveedor = new Proveedor();
        $newProveedor->nombre = "MUEBLES TIMAUKEL LTDA";
        $newProveedor->rut = "78042830-9";
        $newProveedor->email = "COBRANZA@MUEBLESTIMAUKEL.CL";
        $newProveedor->transferencia = 0;
        $newProveedor->activo = 1;
        $newProveedor->save();

        $newProveedor = new Proveedor();
        $newProveedor->nombre = "SERVICIOS MÉDICOS TABANCURA S.A";
        $newProveedor->rut = "78053560-1";
        $newProveedor->email = "RHERNANDEZ@CLINICATABANCURA.CL";
        $newProveedor->transferencia = 0;
        $newProveedor->activo = 1;
        $newProveedor->save();

        $newProveedor = new Proveedor();
        $newProveedor->nombre = "IMPORTADORA DE PRODUCTOS MEDICOS LTDA.";
        $newProveedor->rut = "78060400-K";
        $newProveedor->email = "COBRANZA@EXXIMMED.CL";
        $newProveedor->transferencia = 0;
        $newProveedor->activo = 1;
        $newProveedor->save();

        $newProveedor = new Proveedor();
        $newProveedor->nombre = "OFTOMED LTDA.";
        $newProveedor->rut = "78075900-3";
        $newProveedor->email = "BFUENTES@OFTOMED.CL";
        $newProveedor->transferencia = 0;
        $newProveedor->activo = 1;
        $newProveedor->save();

        $newProveedor = new Proveedor();
        $newProveedor->nombre = "INS. MEDICOS HOSPITALARIOS (I.M.H. S.A.)";
        $newProveedor->rut = "78083940-6";
        $newProveedor->email = "CPEREZ@IMH.CL";
        $newProveedor->transferencia = 0;
        $newProveedor->activo = 1;
        $newProveedor->save();

        $newProveedor = new Proveedor();
        $newProveedor->nombre = "SOC DISTRIBUIDORA LAS PATAGUAS LTDA. (DISPAL)";
        $newProveedor->rut = "78094280-0";
        $newProveedor->email = "DISTRI_LASPATAGUAS@YAHOO.COM";
        $newProveedor->transferencia = 0;
        $newProveedor->activo = 1;
        $newProveedor->save();

        $newProveedor = new Proveedor();
        $newProveedor->nombre = "PSICUS CONSULTORES PROF LTDA";
        $newProveedor->rut = "78115670-1";
        $newProveedor->transferencia = 0;
        $newProveedor->activo = 1;
        $newProveedor->save();

        $newProveedor = new Proveedor();
        $newProveedor->nombre = "COM. Y DIST. MEDICA LTDA.";
        $newProveedor->rut = "78124770-7";
        $newProveedor->email = "PAGOS@CODIMED.CL";
        $newProveedor->transferencia = 0;
        $newProveedor->activo = 1;
        $newProveedor->save();

        $newProveedor = new Proveedor();
        $newProveedor->nombre = "CENTRO DE FORMACIÓN CEFAV";
        $newProveedor->rut = "78167310-2";
        $newProveedor->email = "CAPACITACION@CEFAV.CL";
        $newProveedor->transferencia = 0;
        $newProveedor->activo = 1;
        $newProveedor->save();

        $newProveedor = new Proveedor();
        $newProveedor->nombre = "ROLAND VORWERK Y CIA.";
        $newProveedor->rut = "78178530-K";
        $newProveedor->email = "CMATURANA@OFFICEPRO.CL";
        $newProveedor->transferencia = 0;
        $newProveedor->activo = 1;
        $newProveedor->save();

        $newProveedor = new Proveedor();
        $newProveedor->nombre = "METALURGICA MUNCHEN LIMITADA";
        $newProveedor->rut = "78188080-9";
        $newProveedor->transferencia = 0;
        $newProveedor->activo = 1;
        $newProveedor->save();

        $newProveedor = new Proveedor();
        $newProveedor->nombre = "TECNOLOGIAS EN IMAGENES MEDICAS CHILE S.A.";
        $newProveedor->rut = "78196790-4";
        $newProveedor->email = "cobranza@timed.cl";
        $newProveedor->transferencia = 0;
        $newProveedor->activo = 1;
        $newProveedor->save();

        $newProveedor = new Proveedor();
        $newProveedor->nombre = "DEIRA COMPUTACION Y SERVICIOS";
        $newProveedor->rut = "78198200-8";
        $newProveedor->transferencia = 0;
        $newProveedor->activo = 1;
        $newProveedor->save();

        $newProveedor = new Proveedor();
        $newProveedor->nombre = "HOSPITALIA PRODUCTOS MÉDICOS LTDA.";
        $newProveedor->rut = "78233420-4";
        $newProveedor->email = "AORTIZ@HOSPITALIA.CL";
        $newProveedor->transferencia = 0;
        $newProveedor->activo = 1;
        $newProveedor->save();

        $newProveedor = new Proveedor();
        $newProveedor->nombre = "TEXTIL SUPER LIMITADA";
        $newProveedor->rut = "78238490-2";
        $newProveedor->email = "CLAUDIO@TEXTILSUPER.COM";
        $newProveedor->transferencia = 0;
        $newProveedor->activo = 1;
        $newProveedor->save();

        $newProveedor = new Proveedor();
        $newProveedor->nombre = "MEDICINA TECNICA LTDA.";
        $newProveedor->rut = "78297510-2";
        $newProveedor->email = "V.ALTAMIRANO@MEDTEC.CL";
        $newProveedor->transferencia = 0;
        $newProveedor->activo = 1;
        $newProveedor->save();

        $newProveedor = new Proveedor();
        $newProveedor->nombre = "ROBINSON Y WULF LIMITADA (DOCTOR S CHOICE)";
        $newProveedor->rut = "78322700-2";
        $newProveedor->transferencia = 0;
        $newProveedor->activo = 1;
        $newProveedor->save();

        $newProveedor = new Proveedor();
        $newProveedor->nombre = "CENTRO DE ESPECIALIDADES MÉDICAS LTDA.";
        $newProveedor->rut = "78366460-7";
        $newProveedor->email = "rstern@iram.cl";
        $newProveedor->transferencia = 0;
        $newProveedor->activo = 1;
        $newProveedor->save();

        $newProveedor = new Proveedor();
        $newProveedor->nombre = "INDUSTRIAL Y COMERCIAL BAXTER DE CHILE LTDA.";
        $newProveedor->rut = "78366970-6";
        $newProveedor->email = "CARLOS_OLIVA@BAXTER.COM";
        $newProveedor->transferencia = 0;
        $newProveedor->activo = 1;
        $newProveedor->save();

        $newProveedor = new Proveedor();
        $newProveedor->nombre = "DISTRIBDE MAT QUIRÚRGICO MAQUIR";
        $newProveedor->rut = "78372730-7";
        $newProveedor->email = "GGUIDETTI@VTR.NET";
        $newProveedor->transferencia = 0;
        $newProveedor->activo = 1;
        $newProveedor->save();

        $newProveedor = new Proveedor();
        $newProveedor->nombre = "COMERCIAL EXPRESS DENT LTDA";
        $newProveedor->rut = "78378160-3";
        $newProveedor->email = "CHILECOMPRA@EXPRESSDENT.CL";
        $newProveedor->transferencia = 0;
        $newProveedor->activo = 1;
        $newProveedor->save();

        $newProveedor = new Proveedor();
        $newProveedor->nombre = "FERMELO S.A.";
        $newProveedor->rut = "78436430-5";
        $newProveedor->transferencia = 0;
        $newProveedor->activo = 1;
        $newProveedor->save();

        $newProveedor = new Proveedor();
        $newProveedor->nombre = "PRODUCTOS MÉDICOS PROMEDON CHILE S.A";
        $newProveedor->rut = "78566250-4";
        $newProveedor->email = "MIGUEL.INOSTROZA@PROMEDON.COM";
        $newProveedor->transferencia = 0;
        $newProveedor->activo = 1;
        $newProveedor->save();

        $newProveedor = new Proveedor();
        $newProveedor->nombre = "SOCIEDAD DE INSUMOS MÉDICOS ( UIO";
        $newProveedor->rut = "78587050-6";
        $newProveedor->email = "CPRADO@AMTEC.CL";
        $newProveedor->transferencia = 0;
        $newProveedor->activo = 1;
        $newProveedor->save();

        $newProveedor = new Proveedor();
        $newProveedor->nombre = "ASCENSORES CHILE";
        $newProveedor->rut = "78595640-0";
        $newProveedor->email = "ASCCHILE@ASCENSORESCHILE.CL";
        $newProveedor->transferencia = 0;
        $newProveedor->activo = 1;
        $newProveedor->save();

        $newProveedor = new Proveedor();
        $newProveedor->nombre = "OPTICA E INVERSIONES TRENTO SPA";
        $newProveedor->rut = "78606890-8";
        $newProveedor->email = "GERENCIA@OPTICATRENTO.CL";
        $newProveedor->transferencia = 0;
        $newProveedor->activo = 1;
        $newProveedor->save();

        $newProveedor = new Proveedor();
        $newProveedor->nombre = "C.D. COMP. S.A.";
        $newProveedor->rut = "78611770-4";
        $newProveedor->email = "ASISGERENCIA@CDCOMP.CL";
        $newProveedor->transferencia = 0;
        $newProveedor->activo = 1;
        $newProveedor->save();

        $newProveedor = new Proveedor();
        $newProveedor->nombre = "DIAGNÓSTICOS POR IMÁGENES";
        $newProveedor->rut = "78623600-2";
        $newProveedor->transferencia = 0;
        $newProveedor->activo = 1;
        $newProveedor->save();

        $newProveedor = new Proveedor();
        $newProveedor->nombre = "SOC. DIST. COMERCIAL E INVERSIONES LAURA CARE LTDA.";
        $newProveedor->rut = "78634170-1";
        $newProveedor->email = "SMARTINEZ@LAURACARE.CL";
        $newProveedor->transferencia = 0;
        $newProveedor->activo = 1;
        $newProveedor->save();

        $newProveedor = new Proveedor();
        $newProveedor->nombre = "SOC COMERCIAL DICER LTDA";
        $newProveedor->rut = "78715730-0";
        $newProveedor->transferencia = 0;
        $newProveedor->activo = 1;
        $newProveedor->save();

        $newProveedor = new Proveedor();
        $newProveedor->nombre = "BARRON - VIEYRA INTERNATIONAL LTDA";
        $newProveedor->rut = "78723570-0";
        $newProveedor->email = "AGUERRERO@BARRONVIEYRA.CL";
        $newProveedor->transferencia = 0;
        $newProveedor->activo = 1;
        $newProveedor->save();

        $newProveedor = new Proveedor();
        $newProveedor->nombre = "REHACARE SPA";
        $newProveedor->rut = "78724310-K";
        $newProveedor->email = "CARRIAGADAT@REHACARE.CL";
        $newProveedor->transferencia = 0;
        $newProveedor->activo = 1;
        $newProveedor->save();

        $newProveedor = new Proveedor();
        $newProveedor->nombre = "SOC DE PROFESIONALES CONTRERAS Y SEITZ LTDA";
        $newProveedor->rut = "78735300-2";
        $newProveedor->transferencia = 0;
        $newProveedor->activo = 1;
        $newProveedor->save();

        $newProveedor = new Proveedor();
        $newProveedor->nombre = "RECBEN XENERICS FARMACEUTICA LTDA";
        $newProveedor->rut = "78740450-2";
        $newProveedor->transferencia = 0;
        $newProveedor->activo = 1;
        $newProveedor->save();

        $newProveedor = new Proveedor();
        $newProveedor->nombre = "SAAHER ASCENSORES S.A";
        $newProveedor->rut = "78778520-4";
        $newProveedor->email = "RECEPCION@SAAHER.CL";
        $newProveedor->transferencia = 0;
        $newProveedor->activo = 1;
        $newProveedor->save();

        $newProveedor = new Proveedor();
        $newProveedor->nombre = "M. KAPLAN Y CIA. LTDA.";
        $newProveedor->rut = "78800870-8";
        $newProveedor->email = "XAPIOLAZA@KAPLAN.CL";
        $newProveedor->transferencia = 0;
        $newProveedor->activo = 1;
        $newProveedor->save();

        $newProveedor = new Proveedor();
        $newProveedor->nombre = "SOC.PROF.ODONTOLOGICA LTDA";
        $newProveedor->rut = "78822310-2";
        $newProveedor->transferencia = 0;
        $newProveedor->activo = 1;
        $newProveedor->save();

        $newProveedor = new Proveedor();
        $newProveedor->nombre = "OLMOS Y CIA. LTDA.";
        $newProveedor->rut = "78828310-5";
        $newProveedor->email = "INSUMOS.ACCESORIOS@OLMOSMED.CL";
        $newProveedor->transferencia = 0;
        $newProveedor->activo = 1;
        $newProveedor->save();

        $newProveedor = new Proveedor();
        $newProveedor->nombre = "IMPORTADORA E INVERSIONES PROLAB LTDA.";
        $newProveedor->rut = "78835470-3";
        $newProveedor->email = "PROLAB@PROLAB.CL";
        $newProveedor->transferencia = 0;
        $newProveedor->activo = 1;
        $newProveedor->save();

        $newProveedor = new Proveedor();
        $newProveedor->nombre = "INVERSIONES ALCAMED LTDA.";
        $newProveedor->rut = "78861590-6";
        $newProveedor->email = "PLEIVA@ALCAMED.CL";
        $newProveedor->transferencia = 0;
        $newProveedor->activo = 1;
        $newProveedor->save();

        $newProveedor = new Proveedor();
        $newProveedor->nombre = "STRYKER CORPORATION (CHILE) Y CIA. LTDA.";
        $newProveedor->rut = "78874470-6";
        $newProveedor->transferencia = 0;
        $newProveedor->activo = 1;
        $newProveedor->save();

        $newProveedor = new Proveedor();
        $newProveedor->nombre = "COMERCIAL GRAFIMERK LTDA.";
        $newProveedor->rut = "78896110-3";
        $newProveedor->email = "GAPGRAFIMERK@GMAIL.COM";
        $newProveedor->transferencia = 0;
        $newProveedor->activo = 1;
        $newProveedor->save();

        $newProveedor = new Proveedor();
        $newProveedor->nombre = "COM MUÑOZ Y CIA LTDA";
        $newProveedor->rut = "78906980-8";
        $newProveedor->email = "CMD@CMD.CL";
        $newProveedor->transferencia = 0;
        $newProveedor->activo = 1;
        $newProveedor->save();

        $newProveedor = new Proveedor();
        $newProveedor->nombre = "SALLE, ZAPATA Y COMPAÑÍA LIMITADA";
        $newProveedor->rut = "78914950-K";
        $newProveedor->email = "PROJAS@PHARMAMERICA.CL";
        $newProveedor->transferencia = 0;
        $newProveedor->activo = 1;
        $newProveedor->save();

        $newProveedor = new Proveedor();
        $newProveedor->nombre = "NEXTEL S.A.";
        $newProveedor->rut = "78921690-8";
        $newProveedor->transferencia = 0;
        $newProveedor->activo = 1;
        $newProveedor->save();

        $newProveedor = new Proveedor();
        $newProveedor->nombre = "INMOBILIARIA E INVERSIONES TAXCO LTDA.";
        $newProveedor->rut = "78928740-6";
        $newProveedor->email = "ANAMARIA.ACOSTA@MICROBAC.CL";
        $newProveedor->transferencia = 0;
        $newProveedor->activo = 1;
        $newProveedor->save();

        $newProveedor = new Proveedor();
        $newProveedor->nombre = "COMERCIAL E INDUSTRIAL INQUINAT CHILE LTDA";
        $newProveedor->rut = "78931720-8";
        $newProveedor->email = "INQUINAT@INQUINAT.CL";
        $newProveedor->transferencia = 0;
        $newProveedor->activo = 1;
        $newProveedor->save();

        $newProveedor = new Proveedor();
        $newProveedor->nombre = "NEUMANN LTDA.";
        $newProveedor->rut = "78936310-2";
        $newProveedor->email = "TESORERIA@CIRUMED.CL";
        $newProveedor->transferencia = 0;
        $newProveedor->activo = 1;
        $newProveedor->save();

        $newProveedor = new Proveedor();
        $newProveedor->nombre = "SOC CESPEDES Y ROLDAN LIMITADA";
        $newProveedor->rut = "78970170-9";
        $newProveedor->email = "SILLASCYR@GMAIL.COM";
        $newProveedor->transferencia = 0;
        $newProveedor->activo = 1;
        $newProveedor->save();

        $newProveedor = new Proveedor();
        $newProveedor->nombre = "GARAFULLIC Y COMPAÑÍA LTDA.";
        $newProveedor->rut = "78996500-5";
        $newProveedor->transferencia = 0;
        $newProveedor->activo = 1;
        $newProveedor->save();

        $newProveedor = new Proveedor();
        $newProveedor->nombre = "INVERSIONES Y SERVICIOS BALDER LTDA";
        $newProveedor->rut = "79510190-K";
        $newProveedor->email = "WALTER.CAMINO@BALDER.CL";
        $newProveedor->transferencia = 0;
        $newProveedor->activo = 1;
        $newProveedor->save();

        $newProveedor = new Proveedor();
        $newProveedor->nombre = "SOC.INGENIERIA HOGG Y SERRANO";
        $newProveedor->rut = "79555420-3";
        $newProveedor->email = "PAGOS@HOSER.CL";
        $newProveedor->transferencia = 0;
        $newProveedor->activo = 1;
        $newProveedor->save();

        $newProveedor = new Proveedor();
        $newProveedor->nombre = "VALTEK S.A.";
        $newProveedor->rut = "79568850-1";
        $newProveedor->email = "SLOPEZ@VALTEK.CL";
        $newProveedor->transferencia = 0;
        $newProveedor->activo = 1;
        $newProveedor->save();

        $newProveedor = new Proveedor();
        $newProveedor->nombre = "DIFEM LABORATORIOS S.A.";
        $newProveedor->rut = "79581120-6";
        $newProveedor->email = "VIVIAN.SALDIAS@ANASAC.CL";
        $newProveedor->transferencia = 0;
        $newProveedor->activo = 1;
        $newProveedor->save();

        $newProveedor = new Proveedor();
        $newProveedor->nombre = "ESMAX DISTRIBUCION LTDA.";
        $newProveedor->rut = "79588870-5";
        $newProveedor->transferencia = 0;
        $newProveedor->activo = 1;
        $newProveedor->save();

        $newProveedor = new Proveedor();
        $newProveedor->nombre = "DENTAL LAVAL LTDA";
        $newProveedor->rut = "79595850-9";
        $newProveedor->email = "CFLORES@DENTAL-LAVAL.CL";
        $newProveedor->transferencia = 0;
        $newProveedor->activo = 1;
        $newProveedor->save();

        $newProveedor = new Proveedor();
        $newProveedor->nombre = "GALÉNICA S.A.";
        $newProveedor->rut = "79622060-0";
        $newProveedor->email = "INFO@GALENICA.CL";
        $newProveedor->transferencia = 0;
        $newProveedor->activo = 1;
        $newProveedor->save();

        $newProveedor = new Proveedor();
        $newProveedor->nombre = "ASTORGA ROINE S DE RESP LTDA PROMEDAR";
        $newProveedor->rut = "79653020-0";
        $newProveedor->email = "COBRANZAS@PROMEDAR.COM";
        $newProveedor->transferencia = 0;
        $newProveedor->activo = 1;
        $newProveedor->save();

        $newProveedor = new Proveedor();
        $newProveedor->nombre = "JAIME CELUME Y CÍA LTDA";
        $newProveedor->rut = "79653750-7";
        $newProveedor->email = "jvalderrama@segurycel.cl";
        $newProveedor->transferencia = 0;
        $newProveedor->activo = 1;
        $newProveedor->save();

        $newProveedor = new Proveedor();
        $newProveedor->nombre = "MASTERCARE S.A.";
        $newProveedor->rut = "79657680-4";
        $newProveedor->email = "CARLOS.VALENCIA@GRUNENTHAL.COM";
        $newProveedor->transferencia = 0;
        $newProveedor->activo = 1;
        $newProveedor->save();

        $newProveedor = new Proveedor();
        $newProveedor->nombre = "POLARIS LIMITADA";
        $newProveedor->rut = "79709900-7";
        $newProveedor->email = "POLARISLTDA@HOTMAIL.COM";
        $newProveedor->transferencia = 0;
        $newProveedor->activo = 1;
        $newProveedor->save();

        $newProveedor = new Proveedor();
        $newProveedor->nombre = "FARMALATINA LTDA.";
        $newProveedor->rut = "79728570-6";
        $newProveedor->email = "ALEX.MONTERO@FARMALATINA.CL";
        $newProveedor->transferencia = 0;
        $newProveedor->activo = 1;
        $newProveedor->save();

        $newProveedor = new Proveedor();
        $newProveedor->nombre = "BLUNDING S.A.";
        $newProveedor->rut = "79744580-0";
        $newProveedor->email = "COBRANZAS@BLUNDING.COM";
        $newProveedor->transferencia = 0;
        $newProveedor->activo = 1;
        $newProveedor->save();

        $newProveedor = new Proveedor();
        $newProveedor->nombre = "FARMACIA IBERIA";
        $newProveedor->rut = "79769700-1";
        $newProveedor->email = "FARMAIBERIA@YAHOO.ES";
        $newProveedor->transferencia = 0;
        $newProveedor->activo = 1;
        $newProveedor->save();

        $newProveedor = new Proveedor();
        $newProveedor->nombre = "EUROFARMA CHILE S.A.";
        $newProveedor->rut = "79802770-0";
        $newProveedor->email = "COBRANZAS@LABEUROMED.CL";
        $newProveedor->transferencia = 0;
        $newProveedor->activo = 1;
        $newProveedor->save();

        $newProveedor = new Proveedor();
        $newProveedor->nombre = "GAETE PRIMO LTDA.";
        $newProveedor->rut = "79857900-2";
        $newProveedor->email = "GAETEPRIMOLTDA@VTR.NET";
        $newProveedor->transferencia = 0;
        $newProveedor->activo = 1;
        $newProveedor->save();

        $newProveedor = new Proveedor();
        $newProveedor->nombre = "COMERCIAL HIDROBOMBAS LTDA.";
        $newProveedor->rut = "79869510-K";
        $newProveedor->transferencia = 0;
        $newProveedor->activo = 1;
        $newProveedor->save();

        $newProveedor = new Proveedor();
        $newProveedor->nombre = "LABORATORIO BIOTOSCANA FARMA SPA";
        $newProveedor->rut = "79873270-6";
        $newProveedor->email = "NATALIA.GONZALEZ@BIOTOSCANA.COM";
        $newProveedor->transferencia = 0;
        $newProveedor->activo = 1;
        $newProveedor->save();

        $newProveedor = new Proveedor();
        $newProveedor->nombre = "PV EQUIP S.A.";
        $newProveedor->rut = "79895670-1";
        $newProveedor->email = "ANA.ROMERO@PVEQUIP.CL";
        $newProveedor->transferencia = 0;
        $newProveedor->activo = 1;
        $newProveedor->save();

        $newProveedor = new Proveedor();
        $newProveedor->nombre = "DIAGNÓSTICO POR IMAGEN SAN VICENTE DE PAUL LTDA.";
        $newProveedor->rut = "79904980-5";
        $newProveedor->email = "INFOIMAGEN@SANVICENTEDEPAUL.CL";
        $newProveedor->transferencia = 0;
        $newProveedor->activo = 1;
        $newProveedor->save();

        $newProveedor = new Proveedor();
        $newProveedor->nombre = "METALURGICA SILCOSIL LTDA";
        $newProveedor->rut = "79909150-K";
        $newProveedor->email = "MMUNOZ@SILCOSIL.CL";
        $newProveedor->transferencia = 0;
        $newProveedor->activo = 1;
        $newProveedor->save();

        $newProveedor = new Proveedor();
        $newProveedor->nombre = "SOC.COMERCIAL IMPLEMED";
        $newProveedor->rut = "79921850-K";
        $newProveedor->email = "IMPLEMEDCHILE@GMAIL.COM";
        $newProveedor->transferencia = 0;
        $newProveedor->activo = 1;
        $newProveedor->save();

        $newProveedor = new Proveedor();
        $newProveedor->nombre = "COMERCIAL TOTALPACK LTDA.";
        $newProveedor->rut = "79948840-K";
        $newProveedor->transferencia = 0;
        $newProveedor->activo = 1;
        $newProveedor->save();

        $newProveedor = new Proveedor();
        $newProveedor->nombre = "SERVICIOS INTEGRADOS DE SALUD LTDA";
        $newProveedor->rut = "79980070-5";
        $newProveedor->transferencia = 0;
        $newProveedor->activo = 1;
        $newProveedor->save();

        $newProveedor = new Proveedor();
        $newProveedor->nombre = "MUNNICH PHARMA MEDICAL LTDA.";
        $newProveedor->rut = "80447400-5";
        $newProveedor->email = "PAGOS.SOCOFAR@SOCOFAR.CL";
        $newProveedor->transferencia = 0;
        $newProveedor->activo = 1;
        $newProveedor->save();

        $newProveedor = new Proveedor();
        $newProveedor->nombre = "MERCK S.A.";
        $newProveedor->rut = "80621200-8";
        $newProveedor->email = "CONTACTO@INTERCOBRANZAS.COM";
        $newProveedor->transferencia = 0;
        $newProveedor->activo = 1;
        $newProveedor->save();

        $newProveedor = new Proveedor();
        $newProveedor->nombre = "OPPICI S.A.";
        $newProveedor->rut = "80695500-0";
        $newProveedor->transferencia = 0;
        $newProveedor->activo = 1;
        $newProveedor->save();

        $newProveedor = new Proveedor();
        $newProveedor->nombre = "J RAVERA Y CIA LTDA";
        $newProveedor->rut = "80780200-3";
        $newProveedor->email = "ROMINA@FERRETERIARAVERA.CL";
        $newProveedor->transferencia = 0;
        $newProveedor->activo = 1;
        $newProveedor->save();

        $newProveedor = new Proveedor();
        $newProveedor->nombre = "CARMEN ELENA CONCHA VALLE";
        $newProveedor->rut = "8097724-7";
        $newProveedor->transferencia = 0;
        $newProveedor->activo = 1;
        $newProveedor->save();

        $newProveedor = new Proveedor();
        $newProveedor->nombre = "REUTTER S.A.";
        $newProveedor->rut = "81210400-4";
        $newProveedor->email = "CMONTERO@REUTTER.CL";
        $newProveedor->transferencia = 0;
        $newProveedor->activo = 1;
        $newProveedor->save();

        $newProveedor = new Proveedor();
        $newProveedor->nombre = "CIA .TRATAMIENTOS DE AGUA Y COMBUSTIÓN LTDA.";
        $newProveedor->rut = "81286300-2";
        $newProveedor->email = "EARAVENA@COTACO.CL";
        $newProveedor->transferencia = 0;
        $newProveedor->activo = 1;
        $newProveedor->save();

        $newProveedor = new Proveedor();
        $newProveedor->nombre = "GRUNENTHAL CHILENA LTDA";
        $newProveedor->rut = "81323800-4";
        $newProveedor->email = "VICTORIA.OSSES@GRUNENTHAL.COM";
        $newProveedor->transferencia = 0;
        $newProveedor->activo = 1;
        $newProveedor->save();

        $newProveedor = new Proveedor();
        $newProveedor->nombre = "IMED S.A.";
        $newProveedor->rut = "81338000-5";
        $newProveedor->email = "JOYARZO@INMED-MEDICAL.COM";
        $newProveedor->transferencia = 0;
        $newProveedor->activo = 1;
        $newProveedor->save();

        $newProveedor = new Proveedor();
        $newProveedor->nombre = "ABBOTT LABORATORIES DE CHILE LTDA.";
        $newProveedor->rut = "81378300-2";
        $newProveedor->email = "servicioalclientechile@abbott.com";
        $newProveedor->transferencia = 0;
        $newProveedor->activo = 1;
        $newProveedor->save();

        $newProveedor = new Proveedor();
        $newProveedor->nombre = "ROJAS DE CANTO CONSTANZA";
        $newProveedor->rut = "8139711-3";
        $newProveedor->transferencia = 0;
        $newProveedor->activo = 1;
        $newProveedor->save();

        $newProveedor = new Proveedor();
        $newProveedor->nombre = "ALGODONES BETA LTDA.";
        $newProveedor->rut = "81502600-4";
        $newProveedor->email = "ECORVILLON@ESTRELLASDELSUR.CL";
        $newProveedor->transferencia = 0;
        $newProveedor->activo = 1;
        $newProveedor->save();

        $newProveedor = new Proveedor();
        $newProveedor->nombre = "PONTIFICIA UNIVERSIDAD CATOLICA DE CHILE";
        $newProveedor->rut = "81698900-0";
        $newProveedor->email = "PCASTROC@MED.PUC.CL";
        $newProveedor->transferencia = 0;
        $newProveedor->activo = 1;
        $newProveedor->save();

        $newProveedor = new Proveedor();
        $newProveedor->nombre = "HUMBERTO GARETTO E HIJOS LIMITADA";
        $newProveedor->rut = "81771100-6";
        $newProveedor->email = "INFOR@GARETTO.CL";
        $newProveedor->transferencia = 0;
        $newProveedor->activo = 1;
        $newProveedor->save();

        $newProveedor = new Proveedor();
        $newProveedor->nombre = "AGUSTIN OMAR SILVA DEL VALLE";
        $newProveedor->rut = "8208638-2";
        $newProveedor->transferencia = 0;
        $newProveedor->activo = 1;
        $newProveedor->save();

        $newProveedor = new Proveedor();
        $newProveedor->nombre = "COMISIÓN CHILENA DE ENERGÍA NUCLEAR";
        $newProveedor->rut = "82983100-7";
        $newProveedor->email = "JMARTINEZ@CCHEN.CL";
        $newProveedor->transferencia = 0;
        $newProveedor->activo = 1;
        $newProveedor->save();

        $newProveedor = new Proveedor();
        $newProveedor->nombre = "ROCHE CHILE LTDA.";
        $newProveedor->rut = "82999400-3";
        $newProveedor->email = "REINALDO.CORNEJO@ROCHE.COM";
        $newProveedor->transferencia = 0;
        $newProveedor->activo = 1;
        $newProveedor->save();

        $newProveedor = new Proveedor();
        $newProveedor->nombre = "NOVARTIS CHILE S.A.";
        $newProveedor->rut = "83002400-K";
        $newProveedor->email = "patricia.saldivia@novartis.com";
        $newProveedor->transferencia = 0;
        $newProveedor->activo = 1;
        $newProveedor->save();

        $newProveedor = new Proveedor();
        $newProveedor->nombre = "DONOSO Y COMPAÑÍA LTDA.";
        $newProveedor->rut = "83067300-8";
        $newProveedor->email = "ADMINISTRACION@DONOSOMUEBLES.CL";
        $newProveedor->transferencia = 0;
        $newProveedor->activo = 1;
        $newProveedor->save();

        $newProveedor = new Proveedor();
        $newProveedor->nombre = "HALES HERMANOS Y COMPAÑÍA LTDA";
        $newProveedor->rut = "83535000-2";
        $newProveedor->email = "SECRETARIA@HALES.CL";
        $newProveedor->transferencia = 0;
        $newProveedor->activo = 1;
        $newProveedor->save();

        $newProveedor = new Proveedor();
        $newProveedor->nombre = "INDUSTRIA METALÚRGICA ACONCAGUA LTDA";
        $newProveedor->rut = "83732700-8";
        $newProveedor->transferencia = 0;
        $newProveedor->activo = 1;
        $newProveedor->save();

        $newProveedor = new Proveedor();
        $newProveedor->nombre = "MARIA HENRIQUEZ HOFTER";
        $newProveedor->rut = "8425525-4";
        $newProveedor->transferencia = 0;
        $newProveedor->activo = 1;
        $newProveedor->save();

        $newProveedor = new Proveedor();
        $newProveedor->nombre = "PABLO LOPEZ ROJAS";
        $newProveedor->rut = "8439498-K";
        $newProveedor->transferencia = 0;
        $newProveedor->activo = 1;
        $newProveedor->save();

        $newProveedor = new Proveedor();
        $newProveedor->nombre = "MADEGOM S.A.";
        $newProveedor->rut = "84609600-0";
        $newProveedor->email = "RVARAS@HEVEA.CL";
        $newProveedor->transferencia = 0;
        $newProveedor->activo = 1;
        $newProveedor->save();

        $newProveedor = new Proveedor();
        $newProveedor->nombre = "GLAXOSMITHKLINE CHILE";
        $newProveedor->rut = "85025700-0";
        $newProveedor->email = "IRIS.M.SALDIAS@GSK.COM";
        $newProveedor->transferencia = 0;
        $newProveedor->activo = 1;
        $newProveedor->save();

        $newProveedor = new Proveedor();
        $newProveedor->nombre = "SOCIEDAD IMPORTADORA PROVIDENCIA Y CÍA";
        $newProveedor->rut = "85060900-4";
        $newProveedor->email = "AFUENTES@IMPORTA.CL";
        $newProveedor->transferencia = 0;
        $newProveedor->activo = 1;
        $newProveedor->save();

        $newProveedor = new Proveedor();
        $newProveedor->nombre = "REICH DE COMERCIO EXTERIOR SPA";
        $newProveedor->rut = "85218000-5";
        $newProveedor->email = "MPARAYA@REICH.CL";
        $newProveedor->transferencia = 0;
        $newProveedor->activo = 1;
        $newProveedor->save();

        $newProveedor = new Proveedor();
        $newProveedor->nombre = "BUHOS SOCIEDAD COMERCIAL INDUSTRIAL LTDA";
        $newProveedor->rut = "85462700-7";
        $newProveedor->email = "COBRANZAS3@BUHOSCHILE.CL";
        $newProveedor->transferencia = 0;
        $newProveedor->activo = 1;
        $newProveedor->save();

        $newProveedor = new Proveedor();
        $newProveedor->nombre = "IND. METALÚRGICA PROCESA S.A.";
        $newProveedor->rut = "85506400-6";
        $newProveedor->transferencia = 0;
        $newProveedor->activo = 1;
        $newProveedor->save();

        $newProveedor = new Proveedor();
        $newProveedor->nombre = "EDAPISA";
        $newProveedor->rut = "85541900-9";
        $newProveedor->email = "JMVILLASECA@EDAPI.CL";
        $newProveedor->transferencia = 0;
        $newProveedor->activo = 1;
        $newProveedor->save();

        $newProveedor = new Proveedor();
        $newProveedor->nombre = "DEMARKA S.A.";
        $newProveedor->rut = "86132100-2";
        $newProveedor->email = "COBRANZA@DEMARKA.CL";
        $newProveedor->transferencia = 0;
        $newProveedor->activo = 1;
        $newProveedor->save();

        $newProveedor = new Proveedor();
        $newProveedor->nombre = "ADELA DEL CARMEN LEON IBARRA";
        $newProveedor->rut = "8613361-K";
        $newProveedor->transferencia = 0;
        $newProveedor->activo = 1;
        $newProveedor->save();

        $newProveedor = new Proveedor();
        $newProveedor->nombre = "MEDIPLEX S.A.";
        $newProveedor->rut = "86383300-0";
        $newProveedor->email = "VENTAS@MEDIPLEX.CL";
        $newProveedor->transferencia = 0;
        $newProveedor->activo = 1;
        $newProveedor->save();

        $newProveedor = new Proveedor();
        $newProveedor->nombre = "DIPROMED S.A.";
        $newProveedor->rut = "86397000-8";
        $newProveedor->email = "COBRANZAS@DIPROMED.CL";
        $newProveedor->transferencia = 0;
        $newProveedor->activo = 1;
        $newProveedor->save();

        $newProveedor = new Proveedor();
        $newProveedor->nombre = "ALCON LABORATORIOS CHILE LTDA.";
        $newProveedor->rut = "86537600-6";
        $newProveedor->email = "CRISTIAN.PERALTA@NOVARTIS.COM";
        $newProveedor->transferencia = 0;
        $newProveedor->activo = 1;
        $newProveedor->save();

        $newProveedor = new Proveedor();
        $newProveedor->nombre = "COMERCIAL GRÁFICA MILLANTUE";
        $newProveedor->rut = "86654600-2";
        $newProveedor->email = "MILLANTUE@MILLANTUE.CL";
        $newProveedor->transferencia = 0;
        $newProveedor->activo = 1;
        $newProveedor->save();

        $newProveedor = new Proveedor();
        $newProveedor->nombre = "INMED DROGUERIA LTDA";
        $newProveedor->rut = "86821000-1";
        $newProveedor->email = "VENTAS@INMED-MEDICAL.COM";
        $newProveedor->transferencia = 0;
        $newProveedor->activo = 1;
        $newProveedor->save();

        $newProveedor = new Proveedor();
        $newProveedor->nombre = "SISTEMAS MEDICOS LIMITADA";
        $newProveedor->rut = "87536700-5";
        $newProveedor->email = "SECRETARIA@SMEDICOS.CL";
        $newProveedor->transferencia = 0;
        $newProveedor->activo = 1;
        $newProveedor->save();

        $newProveedor = new Proveedor();
        $newProveedor->nombre = "INGRID DEL CARMEN RIQUELME TOBAR";
        $newProveedor->rut = "8758031-8";
        $newProveedor->transferencia = 0;
        $newProveedor->activo = 1;
        $newProveedor->save();

        $newProveedor = new Proveedor();
        $newProveedor->nombre = "COMERCIALIZADORA TERUMO CHILE LTDA.";
        $newProveedor->rut = "87590600-3";
        $newProveedor->email = "BERNARDITA.SEPULVEDA@TERUMOCHILE.CL";
        $newProveedor->transferencia = 0;
        $newProveedor->activo = 1;
        $newProveedor->save();

        $newProveedor = new Proveedor();
        $newProveedor->nombre = "LABORATORIO PASTEUR S.A.";
        $newProveedor->rut = "87674400-7";
        $newProveedor->email = "SOTOR.RICARDO@GMAIL.COM";
        $newProveedor->transferencia = 0;
        $newProveedor->activo = 1;
        $newProveedor->save();

        $newProveedor = new Proveedor();
        $newProveedor->nombre = "ANGELICA NOVOA ZUÑIGA";
        $newProveedor->rut = "8774859-6";
        $newProveedor->transferencia = 0;
        $newProveedor->activo = 1;
        $newProveedor->save();

        $newProveedor = new Proveedor();
        $newProveedor->nombre = "JUAN PALACIOS CORTES";
        $newProveedor->rut = "8816689-2";
        $newProveedor->transferencia = 0;
        $newProveedor->activo = 1;
        $newProveedor->save();

        $newProveedor = new Proveedor();
        $newProveedor->nombre = "DEMARCO S.A.";
        $newProveedor->rut = "88277600-K";
        $newProveedor->email = "VLEYTON@STARCO.CL";
        $newProveedor->transferencia = 0;
        $newProveedor->activo = 1;
        $newProveedor->save();

        $newProveedor = new Proveedor();
        $newProveedor->nombre = "DISTRIBUIDORA 'CM' LTDA";
        $newProveedor->rut = "88400600-7";
        $newProveedor->email = "DISTRIBUIDORACMLTDA@GMAIL.COM";
        $newProveedor->transferencia = 0;
        $newProveedor->activo = 1;
        $newProveedor->save();

        $newProveedor = new Proveedor();
        $newProveedor->nombre = "TECNOFARMA S.A";
        $newProveedor->rut = "88466300-8";
        $newProveedor->transferencia = 0;
        $newProveedor->activo = 1;
        $newProveedor->save();

        $newProveedor = new Proveedor();
        $newProveedor->nombre = "LABORATORIO BIOSANO S.A.";
        $newProveedor->rut = "88597500-3";
        $newProveedor->email = "MMORALES@BIOSANO.CL";
        $newProveedor->transferencia = 0;
        $newProveedor->activo = 1;
        $newProveedor->save();

        $newProveedor = new Proveedor();
        $newProveedor->nombre = "INTERNATIONAL CLINICS S.A.";
        $newProveedor->rut = "88900200-K";
        $newProveedor->email = "ICORDERO@ICLINICS.CL";
        $newProveedor->transferencia = 0;
        $newProveedor->activo = 1;
        $newProveedor->save();

        $newProveedor = new Proveedor();
        $newProveedor->nombre = "MARCELA FERNANDEZ MALDONADO";
        $newProveedor->rut = "8923884-6";
        $newProveedor->transferencia = 0;
        $newProveedor->activo = 1;
        $newProveedor->save();

        $newProveedor = new Proveedor();
        $newProveedor->nombre = "LIBRERÍA REY -SER Y COMPAÑÍA";
        $newProveedor->rut = "89293800-8";
        $newProveedor->email = "LIBRERIAS@REYSER.CL";
        $newProveedor->transferencia = 0;
        $newProveedor->activo = 1;
        $newProveedor->save();

        $newProveedor = new Proveedor();
        $newProveedor->nombre = "REPETUR Y CIA. LTDA.";
        $newProveedor->rut = "89618500-4";
        $newProveedor->email = "INFOCONTABILIDAD@IMEDICA.TIE.CL";
        $newProveedor->transferencia = 0;
        $newProveedor->activo = 1;
        $newProveedor->save();

        $newProveedor = new Proveedor();
        $newProveedor->nombre = "INGENIERIA EN ELECTRONICA COMPUTACION Y MEDICINA S.A.";
        $newProveedor->rut = "89630400-3";
        $newProveedor->direccion = "ELIODORO YAÑEZ 1890";
        $newProveedor->telefono = "226555501";
        $newProveedor->email = "REMBERTO.FUENTES@ECM.CL";
        $newProveedor->transferencia = 0;
        $newProveedor->activo = 1;
        $newProveedor->save();

        $newProveedor = new Proveedor();
        $newProveedor->nombre = "DISTRIBUIDORA DE ARTICULOS MEDICOS PEREZ SPA";
        $newProveedor->rut = "89752800-2";
        $newProveedor->email = "VENTAS@PEREZLTDA.CL";
        $newProveedor->transferencia = 0;
        $newProveedor->activo = 1;
        $newProveedor->save();

        $newProveedor = new Proveedor();
        $newProveedor->nombre = "ARTICULOS DEPORTIVOS GACITUA Y COMPAÑÍA LTDA";
        $newProveedor->rut = "89812000-7";
        $newProveedor->transferencia = 0;
        $newProveedor->activo = 1;
        $newProveedor->save();

        $newProveedor = new Proveedor();
        $newProveedor->nombre = "INGENIERÍA Y CONSTRUCCIÓN R. RODRIGUEZ Y CIA LTDA.";
        $newProveedor->rut = "89912300-K";
        $newProveedor->email = "KVERGARA@RICARDORODRIGUEZ.CL";
        $newProveedor->transferencia = 0;
        $newProveedor->activo = 1;
        $newProveedor->save();

        $newProveedor = new Proveedor();
        $newProveedor->nombre = "DIAGNÓSTICOS Y TRATAMIENTOS INTEGRADOS LTDA.";
        $newProveedor->rut = "89921900-7";
        $newProveedor->email = "DTILTDA@ENTELCHILE.NET";
        $newProveedor->transferencia = 0;
        $newProveedor->activo = 1;
        $newProveedor->save();

        $newProveedor = new Proveedor();
        $newProveedor->nombre = "INSTITUTO SANITAS S.A.";
        $newProveedor->rut = "90073000-4";
        $newProveedor->email = "ANGELA.PLEGUEZUELOS@SANITAS.CL";
        $newProveedor->transferencia = 0;
        $newProveedor->activo = 1;
        $newProveedor->save();

        $newProveedor = new Proveedor();
        $newProveedor->nombre = "LINDE GAS S.A.";
        $newProveedor->rut = "90100000-K";
        $newProveedor->email = "KATIA.TOLOZA@LINDE.COM";
        $newProveedor->transferencia = 0;
        $newProveedor->activo = 1;
        $newProveedor->save();

        $newProveedor = new Proveedor();
        $newProveedor->nombre = "EMPRESA EL MERCURIO SA PERIODÍSTICA";
        $newProveedor->rut = "90193000-7";
        $newProveedor->email = "SERGIO.DELPORTE@COLABORADORES.CL";
        $newProveedor->transferencia = 0;
        $newProveedor->activo = 1;
        $newProveedor->save();

        $newProveedor = new Proveedor();
        $newProveedor->nombre = "NESTLÉ CHILE S.A.";
        $newProveedor->rut = "90703000-8";
        $newProveedor->email = "MARCELO.TORRES@CL.NESTLE.COM";
        $newProveedor->transferencia = 0;
        $newProveedor->activo = 1;
        $newProveedor->save();

        $newProveedor = new Proveedor();
        $newProveedor->nombre = "CLÍNICA SANTA MARÍA";
        $newProveedor->rut = "90753000-0";
        $newProveedor->transferencia = 0;
        $newProveedor->activo = 1;
        $newProveedor->save();

        $newProveedor = new Proveedor();
        $newProveedor->nombre = "BIGGI CHILE S.A.";
        $newProveedor->rut = "91462001-5";
        $newProveedor->transferencia = 0;
        $newProveedor->activo = 1;
        $newProveedor->save();

        $newProveedor = new Proveedor();
        $newProveedor->nombre = "BAYER S.A.";
        $newProveedor->rut = "91537000-4";
        $newProveedor->email = "MARIALORETO.JIMENEZ@BAYER.COM";
        $newProveedor->transferencia = 0;
        $newProveedor->activo = 1;
        $newProveedor->save();

        $newProveedor = new Proveedor();
        $newProveedor->nombre = "LABORATORIO SANDERSON S.A.";
        $newProveedor->rut = "91546000-3";
        $newProveedor->email = "COBRANZAS@FRESENIUS-KABI.CL";
        $newProveedor->transferencia = 0;
        $newProveedor->activo = 1;
        $newProveedor->save();

        $newProveedor = new Proveedor();
        $newProveedor->nombre = "SOCOFAR S.A.";
        $newProveedor->rut = "91575000-1";
        $newProveedor->email = "PAGOS.SOCOFAR@SOCOFAR.CL";
        $newProveedor->transferencia = 0;
        $newProveedor->activo = 1;
        $newProveedor->save();

        $newProveedor = new Proveedor();
        $newProveedor->nombre = "LABORATORIOS RECALCINE S.A.";
        $newProveedor->rut = "91637000-8";
        $newProveedor->email = "AALARCONM@DIFRECALCINE.CL";
        $newProveedor->transferencia = 0;
        $newProveedor->activo = 1;
        $newProveedor->save();

        $newProveedor = new Proveedor();
        $newProveedor->nombre = "LABORATORIOS SAVAL S.A.";
        $newProveedor->rut = "91650000-9";
        $newProveedor->email = "JLORCA@SAVAL.CL";
        $newProveedor->transferencia = 0;
        $newProveedor->activo = 1;
        $newProveedor->save();

        $newProveedor = new Proveedor();
        $newProveedor->nombre = "ATLAS COPCO";
        $newProveedor->rut = "91762000-8";
        $newProveedor->transferencia = 0;
        $newProveedor->activo = 1;
        $newProveedor->save();

        $newProveedor = new Proveedor();
        $newProveedor->nombre = "LABORATORIO SILESIA";
        $newProveedor->rut = "91871000-0";
        $newProveedor->email = "CARLOS.VALENCIA@GRUNENTHAL.COM";
        $newProveedor->transferencia = 0;
        $newProveedor->activo = 1;
        $newProveedor->save();

        $newProveedor = new Proveedor();
        $newProveedor->nombre = "INSTITUTO DE DIAGNÓSTICO S.A.(C.INDISA)";
        $newProveedor->rut = "92051000-0";
        $newProveedor->email = "MIGUEL.HEVIA@INDISA.CL";
        $newProveedor->transferencia = 0;
        $newProveedor->activo = 1;
        $newProveedor->save();

        $newProveedor = new Proveedor();
        $newProveedor->nombre = "LABORATORIO MAVER LTDA.(ELIAS ALBALA)";
        $newProveedor->rut = "92121000-0";
        $newProveedor->email = "MAURICIO.MONTERO@MAVER.CL";
        $newProveedor->transferencia = 0;
        $newProveedor->activo = 1;
        $newProveedor->save();

        $newProveedor = new Proveedor();
        $newProveedor->nombre = "MELLAFE Y SALAS SA";
        $newProveedor->rut = "92214000-6";
        $newProveedor->email = "MFARIAS@MELLAFEYSALAS.CL";
        $newProveedor->transferencia = 0;
        $newProveedor->activo = 1;
        $newProveedor->save();

        $newProveedor = new Proveedor();
        $newProveedor->nombre = "SANOFI AVENTIS DE CHILE S.A";
        $newProveedor->rut = "92251000-8";
        $newProveedor->email = "MANUEL.MELLA@SANOFIPASTEUR.COM";
        $newProveedor->transferencia = 0;
        $newProveedor->activo = 1;
        $newProveedor->save();

        $newProveedor = new Proveedor();
        $newProveedor->nombre = "DROGUERIA HOFMANN S.A.C.";
        $newProveedor->rut = "92288000-K";
        $newProveedor->email = "DROGUERIA@HOFMANN.CL";
        $newProveedor->transferencia = 0;
        $newProveedor->activo = 1;
        $newProveedor->save();

        $newProveedor = new Proveedor();
        $newProveedor->nombre = "BRISTOL MYERS SQUIBB DE CHILE AGENC DE E R SQUIBB";
        $newProveedor->rut = "92363000-7";
        $newProveedor->email = "GOTC.LA.AR@BMS.COM";
        $newProveedor->transferencia = 0;
        $newProveedor->activo = 1;
        $newProveedor->save();

        $newProveedor = new Proveedor();
        $newProveedor->nombre = "ANDROMACO S.A.";
        $newProveedor->rut = "92448000-9";
        $newProveedor->email = "ANDROMACO6@FASTCO.CL";
        $newProveedor->transferencia = 0;
        $newProveedor->activo = 1;
        $newProveedor->save();

        $newProveedor = new Proveedor();
        $newProveedor->nombre = "PROMEX S.A. ARTÍCULOS MÉDICOS";
        $newProveedor->rut = "92823000-7";
        $newProveedor->email = "PROMEX.SA@PROMEXSA.CL";
        $newProveedor->transferencia = 0;
        $newProveedor->activo = 1;
        $newProveedor->save();

        $newProveedor = new Proveedor();
        $newProveedor->nombre = "IMPORTADORA Y DISTRIBUIDORA ARQUIMED S.A.";
        $newProveedor->rut = "92999000-5";
        $newProveedor->email = "SARA.LETTER@ARQUIMED.CL";
        $newProveedor->transferencia = 0;
        $newProveedor->activo = 1;
        $newProveedor->save();

        $newProveedor = new Proveedor();
        $newProveedor->nombre = "TECNIGEN S.A.";
        $newProveedor->rut = "93020000-K";
        $newProveedor->email = "SFUENTES@TECNIGEN.CL";
        $newProveedor->transferencia = 0;
        $newProveedor->activo = 1;
        $newProveedor->save();

        $newProveedor = new Proveedor();
        $newProveedor->nombre = "PRAXAIR CHILE LTDA";
        $newProveedor->rut = "93059000-2";
        $newProveedor->email = "SERVICIOALCLIENTECHILE@PRAXAIR.COM";
        $newProveedor->transferencia = 0;
        $newProveedor->activo = 1;
        $newProveedor->save();

        $newProveedor = new Proveedor();
        $newProveedor->nombre = "COMERCIAL LBF LIMITADA";
        $newProveedor->rut = "93366000-1";
        $newProveedor->email = "GFUENZALIDA@LBF.CL";
        $newProveedor->transferencia = 0;
        $newProveedor->activo = 1;
        $newProveedor->save();

        $newProveedor = new Proveedor();
        $newProveedor->nombre = "PABLO ALARCON QUINTEROS";
        $newProveedor->rut = "9340736-9";
        $newProveedor->transferencia = 0;
        $newProveedor->activo = 1;
        $newProveedor->save();

        $newProveedor = new Proveedor();
        $newProveedor->nombre = "ASCENSORES SCHINDLER S.A.";
        $newProveedor->rut = "93565000-3";
        $newProveedor->transferencia = 0;
        $newProveedor->activo = 1;
        $newProveedor->save();

        $newProveedor = new Proveedor();
        $newProveedor->nombre = "DISTRIBUIDORA PERKINS CHILENA S.A.C.(DIPERK)";
        $newProveedor->rut = "93641000-6";
        $newProveedor->transferencia = 0;
        $newProveedor->activo = 1;
        $newProveedor->save();

        $newProveedor = new Proveedor();
        $newProveedor->nombre = "JOHNSON Y JOHNSON DE CHILE S.A.";
        $newProveedor->rut = "93745000-1";
        $newProveedor->email = "CVILLAL5@ITS.JNJ.COM";
        $newProveedor->transferencia = 0;
        $newProveedor->activo = 1;
        $newProveedor->save();

        $newProveedor = new Proveedor();
        $newProveedor->nombre = "CLINICA LAS CONDES S.A";
        $newProveedor->rut = "93930000-7";
        $newProveedor->email = "SFADEL@CLINICALASCONDES.CL";
        $newProveedor->transferencia = 0;
        $newProveedor->activo = 1;
        $newProveedor->save();

        $newProveedor = new Proveedor();
        $newProveedor->nombre = "BEST HOUSE SA IC PRODUCTOS PARA BEBES";
        $newProveedor->rut = "94037000-0";
        $newProveedor->email = "V.TRONCOSO@BESTHOUSE.CL";
        $newProveedor->transferencia = 0;
        $newProveedor->activo = 1;
        $newProveedor->save();

        $newProveedor = new Proveedor();
        $newProveedor->nombre = "SCA CHILE S.A.";
        $newProveedor->rut = "94282000-3";
        $newProveedor->transferencia = 0;
        $newProveedor->activo = 1;
        $newProveedor->save();

        $newProveedor = new Proveedor();
        $newProveedor->nombre = "SCA CHILE S.A.";
        $newProveedor->rut = "942822000-3";
        $newProveedor->transferencia = 0;
        $newProveedor->activo = 1;
        $newProveedor->save();

        $newProveedor = new Proveedor();
        $newProveedor->nombre = "PHARMA INVESTI DE CHILE S.A.";
        $newProveedor->rut = "94544000-7";
        $newProveedor->email = "PROJAS@PHARMAINVESTI.CL";
        $newProveedor->transferencia = 0;
        $newProveedor->activo = 1;
        $newProveedor->save();

        $newProveedor = new Proveedor();
        $newProveedor->nombre = "FARMACIAS RECCIUS S. A.";
        $newProveedor->rut = "94593000-4";
        $newProveedor->transferencia = 0;
        $newProveedor->activo = 1;
        $newProveedor->save();

        $newProveedor = new Proveedor();
        $newProveedor->nombre = "MARIA SANCHEZ BEROIZA";
        $newProveedor->rut = "9483240-3";
        $newProveedor->transferencia = 0;
        $newProveedor->activo = 1;
        $newProveedor->save();

        $newProveedor = new Proveedor();
        $newProveedor->nombre = "SIEMENS S. A.";
        $newProveedor->rut = "94995000-K";
        $newProveedor->email = "SIEMES3@NETCPAG.CL";
        $newProveedor->transferencia = 0;
        $newProveedor->activo = 1;
        $newProveedor->save();

        $newProveedor = new Proveedor();
        $newProveedor->nombre = "CHEMOPHARMA S.A.";
        $newProveedor->rut = "96026000-7";
        $newProveedor->email = "CHEMOPHARMA@CHEMOPHARMA.CL";
        $newProveedor->transferencia = 0;
        $newProveedor->activo = 1;
        $newProveedor->save();

        $newProveedor = new Proveedor();
        $newProveedor->nombre = "STUEDEMANN S.A.";
        $newProveedor->rut = "96502540-5";
        $newProveedor->email = "APASSALACQUA@OFIMUNDO.CL";
        $newProveedor->transferencia = 0;
        $newProveedor->activo = 1;
        $newProveedor->save();

        $newProveedor = new Proveedor();
        $newProveedor->nombre = "BARRENA HERMANOS SAIC";
        $newProveedor->rut = "96511560-9";
        $newProveedor->email = "BARRENAHNOS@VTR.NET";
        $newProveedor->transferencia = 0;
        $newProveedor->activo = 1;
        $newProveedor->save();

        $newProveedor = new Proveedor();
        $newProveedor->nombre = "CENTRAL DE COMPRAS DEL EXTRASISTEMA S.A.";
        $newProveedor->rut = "96515660-7";
        $newProveedor->email = "CENCOMEX@CENCOMEX.CL";
        $newProveedor->transferencia = 0;
        $newProveedor->activo = 1;
        $newProveedor->save();

        $newProveedor = new Proveedor();
        $newProveedor->nombre = "ALPES CHEMIE S.A.";
        $newProveedor->rut = "96517170-3";
        $newProveedor->email = "BNAYA@ALPESLABS.CL";
        $newProveedor->transferencia = 0;
        $newProveedor->activo = 1;
        $newProveedor->save();

        $newProveedor = new Proveedor();
        $newProveedor->nombre = "BESTPHARMA S.A.";
        $newProveedor->rut = "96519830-K";
        $newProveedor->email = "MARIOFARIAS@GESCOB.CL";
        $newProveedor->transferencia = 0;
        $newProveedor->activo = 1;
        $newProveedor->save();

        $newProveedor = new Proveedor();
        $newProveedor->nombre = "CLINICA DÁVILA Y SERVICIOS MÉDICOS S.A.";
        $newProveedor->rut = "96530470-3";
        $newProveedor->email = "KBORQUEZ@DAVILA.CL";
        $newProveedor->transferencia = 0;
        $newProveedor->activo = 1;
        $newProveedor->save();

        $newProveedor = new Proveedor();
        $newProveedor->nombre = "INFO WORLD S.A.";
        $newProveedor->rut = "96532020-2";
        $newProveedor->transferencia = 0;
        $newProveedor->activo = 1;
        $newProveedor->save();

        $newProveedor = new Proveedor();
        $newProveedor->nombre = "HEMISFERIO SUR S.A.";
        $newProveedor->rut = "96533330-4";
        $newProveedor->email = "MCESPEDES@HEMISFERIOSUR.CL";
        $newProveedor->transferencia = 0;
        $newProveedor->activo = 1;
        $newProveedor->save();

        $newProveedor = new Proveedor();
        $newProveedor->nombre = "GRUPOBIOS S.A";
        $newProveedor->rut = "96540690-5";
        $newProveedor->email = "COBRANZA@GRUPOBIOS.CL";
        $newProveedor->transferencia = 0;
        $newProveedor->activo = 1;
        $newProveedor->save();

        $newProveedor = new Proveedor();
        $newProveedor->nombre = "LABORATORIOS SBM FARMA S.A.";
        $newProveedor->rut = "96544130-1";
        $newProveedor->email = "MAGALLARDO@SMBFARMA.CL";
        $newProveedor->transferencia = 0;
        $newProveedor->activo = 1;
        $newProveedor->save();

        $newProveedor = new Proveedor();
        $newProveedor->nombre = "PROVEEDORES INTEGRALES PRISA S.A.";
        $newProveedor->rut = "96556940-5";
        $newProveedor->email = "AFIGUEROAP@PRISA.CL";
        $newProveedor->transferencia = 0;
        $newProveedor->activo = 1;
        $newProveedor->save();

        $newProveedor = new Proveedor();
        $newProveedor->nombre = "COMERCIAL A & B  S.A.";
        $newProveedor->rut = "96560900-8";
        $newProveedor->email = "COBRANZAS@COMERCIALAYB.CL";
        $newProveedor->transferencia = 0;
        $newProveedor->activo = 1;
        $newProveedor->save();

        $newProveedor = new Proveedor();
        $newProveedor->nombre = "GASCO GLP S.A.";
        $newProveedor->rut = "96568740-8";
        $newProveedor->email = "AMHENRIQUEZ@GASCO.CL";
        $newProveedor->transferencia = 0;
        $newProveedor->activo = 1;
        $newProveedor->save();

        $newProveedor = new Proveedor();
        $newProveedor->nombre = "MEDICAL INT. LABORATORIES ( MIHTLAB )";
        $newProveedor->rut = "96581370-5";
        $newProveedor->email = "CONTACTENOS@MINTLAB.CL";
        $newProveedor->transferencia = 0;
        $newProveedor->activo = 1;
        $newProveedor->save();

        $newProveedor = new Proveedor();
        $newProveedor->nombre = "GRIFOLS CHILE S.A.";
        $newProveedor->rut = "96582310-7";
        $newProveedor->email = "MAURICIO.RUBIO@GRIFOLS.COM";
        $newProveedor->transferencia = 0;
        $newProveedor->activo = 1;
        $newProveedor->save();

        $newProveedor = new Proveedor();
        $newProveedor->nombre = "VIGATEC";
        $newProveedor->rut = "96587380-5";
        $newProveedor->email = "VENTAS@VIGATEC.CL";
        $newProveedor->transferencia = 0;
        $newProveedor->activo = 1;
        $newProveedor->save();

        $newProveedor = new Proveedor();
        $newProveedor->nombre = "AIR LIQUIDE CHILE S.A.";
        $newProveedor->rut = "96590530-8";
        $newProveedor->email = "RICARDO_RIVERA@ASSETS.CL";
        $newProveedor->transferencia = 0;
        $newProveedor->activo = 1;
        $newProveedor->save();

        $newProveedor = new Proveedor();
        $newProveedor->nombre = "ECOLAB S.A.";
        $newProveedor->rut = "96604460-8";
        $newProveedor->email = "CAROLINA.CARRERA@ECOLAB.CL";
        $newProveedor->transferencia = 0;
        $newProveedor->activo = 1;
        $newProveedor->save();

        $newProveedor = new Proveedor();
        $newProveedor->nombre = "INTERNATIONAL MEDICAL PRODUCTS S.A.";
        $newProveedor->rut = "96612620-5";
        $newProveedor->email = "FESPINOZA@IMPCHILE.CL";
        $newProveedor->transferencia = 0;
        $newProveedor->activo = 1;
        $newProveedor->save();

        $newProveedor = new Proveedor();
        $newProveedor->nombre = "LABORATORIO RAFFO S.A.";
        $newProveedor->rut = "96617060-3";
        $newProveedor->email = "VREBOLLEDO@LABRAFFO.CL";
        $newProveedor->transferencia = 0;
        $newProveedor->activo = 1;
        $newProveedor->save();

        $newProveedor = new Proveedor();
        $newProveedor->nombre = "MADELEINE ABRILOT LOPEZ";
        $newProveedor->rut = "9662379-8";
        $newProveedor->transferencia = 0;
        $newProveedor->activo = 1;
        $newProveedor->save();

        $newProveedor = new Proveedor();
        $newProveedor->nombre = "COMERCIAL INFORMATION TECHNOLOGY";
        $newProveedor->rut = "96625160-3";
        $newProveedor->transferencia = 0;
        $newProveedor->activo = 1;
        $newProveedor->save();

        $newProveedor = new Proveedor();
        $newProveedor->nombre = "ANDOVER ALIANZA MÉDICA S.A.";
        $newProveedor->rut = "96625550-1";
        $newProveedor->email = "finanzas@andover.cl";
        $newProveedor->transferencia = 0;
        $newProveedor->activo = 1;
        $newProveedor->save();

        $newProveedor = new Proveedor();
        $newProveedor->nombre = "TECNIKA S.A.";
        $newProveedor->rut = "96625950-7";
        $newProveedor->email = "LUCIA.SEPULVEDA@TECNIKA.CL";
        $newProveedor->transferencia = 0;
        $newProveedor->activo = 1;
        $newProveedor->save();

        $newProveedor = new Proveedor();
        $newProveedor->nombre = "MARIO FABIAN REINOSO VARGAS";
        $newProveedor->rut = "9662976-1";
        $newProveedor->transferencia = 0;
        $newProveedor->activo = 1;
        $newProveedor->save();

        $newProveedor = new Proveedor();
        $newProveedor->nombre = "SERVICIOS INTEGRADOS DE SALUD (INDISA DIAGNÓSTICO)";
        $newProveedor->rut = "96631140-1";
        $newProveedor->email = "MIGUEL.HEVIA@INDISA.CL";
        $newProveedor->transferencia = 0;
        $newProveedor->activo = 1;
        $newProveedor->save();

        $newProveedor = new Proveedor();
        $newProveedor->nombre = "BIOSONDA S.A.";
        $newProveedor->rut = "96632300-0";
        $newProveedor->transferencia = 0;
        $newProveedor->activo = 1;
        $newProveedor->save();

        $newProveedor = new Proveedor();
        $newProveedor->nombre = "ALLMEDICA SOCIEDAD ANÓNIMA";
        $newProveedor->rut = "96636310-K";
        $newProveedor->email = "COBRANZAS@ALLMEDICA.CL";
        $newProveedor->transferencia = 0;
        $newProveedor->activo = 1;
        $newProveedor->save();

        $newProveedor = new Proveedor();
        $newProveedor->nombre = "PENTAFARMA S.A.";
        $newProveedor->rut = "96640350-0";
        $newProveedor->email = "ERNA.VARAS@FMC-AG.COM";
        $newProveedor->transferencia = 0;
        $newProveedor->activo = 1;
        $newProveedor->save();

        $newProveedor = new Proveedor();
        $newProveedor->nombre = "ALBIA S.A";
        $newProveedor->rut = "96656430-K";
        $newProveedor->email = "TESORERIA@ALBIA.CL";
        $newProveedor->transferencia = 0;
        $newProveedor->activo = 1;
        $newProveedor->save();

        $newProveedor = new Proveedor();
        $newProveedor->nombre = "BIOMERIEUX CHILE SPA.";
        $newProveedor->rut = "96659920-0";
        $newProveedor->email = "DIEGO.BASTIDAS@EXT.BIOMERIEUX.COM";
        $newProveedor->transferencia = 0;
        $newProveedor->activo = 1;
        $newProveedor->save();

        $newProveedor = new Proveedor();
        $newProveedor->nombre = "SERV. COMP.GLOBAL S.A.";
        $newProveedor->rut = "96661420-K";
        $newProveedor->email = "PAMELA.GUZMAN@SCGLOBAL.CL";
        $newProveedor->transferencia = 0;
        $newProveedor->activo = 1;
        $newProveedor->save();

        $newProveedor = new Proveedor();
        $newProveedor->nombre = "DIMERC S.A.";
        $newProveedor->rut = "96670840-9";
        $newProveedor->email = "JNMORALES@DIMERC.CL";
        $newProveedor->transferencia = 0;
        $newProveedor->activo = 1;
        $newProveedor->save();

        $newProveedor = new Proveedor();
        $newProveedor->nombre = "COMPUTACION INTEGRAL";
        $newProveedor->rut = "96689970-0";
        $newProveedor->email = "COBRANZA@CINTEGRAL.CL";
        $newProveedor->transferencia = 0;
        $newProveedor->activo = 1;
        $newProveedor->save();

        $newProveedor = new Proveedor();
        $newProveedor->nombre = "COMPUTACIÓN E INGENIERÍA S.A.";
        $newProveedor->rut = "96693120-5";
        $newProveedor->transferencia = 0;
        $newProveedor->activo = 1;
        $newProveedor->save();

        $newProveedor = new Proveedor();
        $newProveedor->nombre = "MEDICALTEK CHILE S.A.";
        $newProveedor->rut = "96696000-0";
        $newProveedor->email = "AROJASM@MEDICALTEK.CL";
        $newProveedor->transferencia = 0;
        $newProveedor->activo = 1;
        $newProveedor->save();

        $newProveedor = new Proveedor();
        $newProveedor->nombre = "PROCESOS SANITARIOS S.A";
        $newProveedor->rut = "96697710-8";
        $newProveedor->email = "XGOMEZ@STERICYCLE.CL";
        $newProveedor->transferencia = 0;
        $newProveedor->activo = 1;
        $newProveedor->save();

        $newProveedor = new Proveedor();
        $newProveedor->nombre = "ALTA TECNOLOGÍA MEDICA S.A.";
        $newProveedor->rut = "96705150-0";
        $newProveedor->email = "GROJAS@ATM.CL";
        $newProveedor->transferencia = 0;
        $newProveedor->activo = 1;
        $newProveedor->save();

        $newProveedor = new Proveedor();
        $newProveedor->nombre = "CIENTEC SA";
        $newProveedor->rut = "96709790-K";
        $newProveedor->email = "AHERNANDEZ@CIENTECINSTRUMENTOS.CL";
        $newProveedor->transferencia = 0;
        $newProveedor->activo = 1;
        $newProveedor->save();

        $newProveedor = new Proveedor();
        $newProveedor->nombre = "METROGAS";
        $newProveedor->rut = "96722460-K";
        $newProveedor->transferencia = 0;
        $newProveedor->activo = 1;
        $newProveedor->save();

        $newProveedor = new Proveedor();
        $newProveedor->nombre = "INVERSIONES E INMOBILIARIA RIO BLANCO S.A";
        $newProveedor->rut = "96726310-9";
        $newProveedor->transferencia = 0;
        $newProveedor->activo = 1;
        $newProveedor->save();

        $newProveedor = new Proveedor();
        $newProveedor->nombre = "CHILEMAT S.A";
        $newProveedor->rut = "96726970-0";
        $newProveedor->email = "MARIALORETO.AGUILERA@CHILEMAT.CL";
        $newProveedor->transferencia = 0;
        $newProveedor->activo = 1;
        $newProveedor->save();

        $newProveedor = new Proveedor();
        $newProveedor->nombre = "BIOSCAN S.A. (EMPRESA DE BIOTECNOLOGIA)";
        $newProveedor->rut = "96729480-2";
        $newProveedor->email = "RECEPCION@BIOSCAN.CL";
        $newProveedor->transferencia = 0;
        $newProveedor->activo = 1;
        $newProveedor->save();

        $newProveedor = new Proveedor();
        $newProveedor->nombre = "CHILEXPRESS";
        $newProveedor->rut = "96756430-3";
        $newProveedor->transferencia = 0;
        $newProveedor->activo = 1;
        $newProveedor->save();

        $newProveedor = new Proveedor();
        $newProveedor->nombre = "B. BRAUN MEDICAL SPA";
        $newProveedor->rut = "96756540-7";
        $newProveedor->email = "COBRANZAS.CL@BBRAUN.COM";
        $newProveedor->transferencia = 0;
        $newProveedor->activo = 1;
        $newProveedor->save();

        $newProveedor = new Proveedor();
        $newProveedor->nombre = "CGM NUCLEAR S.A.";
        $newProveedor->rut = "96757080-K";
        $newProveedor->email = "ADMINISTRACION@CGMNUCLEAR.CL";
        $newProveedor->transferencia = 0;
        $newProveedor->activo = 1;
        $newProveedor->save();

        $newProveedor = new Proveedor();
        $newProveedor->nombre = "IVENS S.A.";
        $newProveedor->rut = "96764340-8";
        $newProveedor->email = "ALAZO@IVENS.CL";
        $newProveedor->transferencia = 0;
        $newProveedor->activo = 1;
        $newProveedor->save();

        $newProveedor = new Proveedor();
        $newProveedor->nombre = "CLÍNICA ALEMANA DE SANTIAGO S.A.";
        $newProveedor->rut = "96770100-9";
        $newProveedor->email = "EBURGOS@ALEMANA.CL";
        $newProveedor->transferencia = 0;
        $newProveedor->activo = 1;
        $newProveedor->save();

        $newProveedor = new Proveedor();
        $newProveedor->nombre = "EDENRED CHILE SA";
        $newProveedor->rut = "96781350-8";
        $newProveedor->email = "MARIA.URETA@EDENRED.COM";
        $newProveedor->transferencia = 0;
        $newProveedor->activo = 1;
        $newProveedor->save();

        $newProveedor = new Proveedor();
        $newProveedor->nombre = "CLÍNICA COLONIAL S.A.";
        $newProveedor->rut = "96790040-0";
        $newProveedor->email = "ALEON@CLINICACOLONIAL.CL";
        $newProveedor->transferencia = 0;
        $newProveedor->activo = 1;
        $newProveedor->save();

        $newProveedor = new Proveedor();
        $newProveedor->nombre = "ENEL DISTRIBUCION CHILE S.A.";
        $newProveedor->rut = "96800570-7";
        $newProveedor->transferencia = 0;
        $newProveedor->activo = 1;
        $newProveedor->save();

        $newProveedor = new Proveedor();
        $newProveedor->nombre = "TRANSVE S.A.";
        $newProveedor->rut = "96802280-6";
        $newProveedor->email = "COBRANZAS@TRANSVE.CL";
        $newProveedor->transferencia = 0;
        $newProveedor->activo = 1;
        $newProveedor->save();

        $newProveedor = new Proveedor();
        $newProveedor->nombre = "ENTEL PCS TELECOMUNICACIONES S.A.";
        $newProveedor->rut = "96806980-2";
        $newProveedor->transferencia = 0;
        $newProveedor->activo = 1;
        $newProveedor->save();

        $newProveedor = new Proveedor();
        $newProveedor->nombre = "FASA CHILE S.A.";
        $newProveedor->rut = "96809530-7";
        $newProveedor->transferencia = 0;
        $newProveedor->activo = 1;
        $newProveedor->save();

        $newProveedor = new Proveedor();
        $newProveedor->nombre = "TEAM GRAFF SOCIEDAD ANONIMA";
        $newProveedor->rut = "96815490-7";
        $newProveedor->transferencia = 0;
        $newProveedor->activo = 1;
        $newProveedor->save();

        $newProveedor = new Proveedor();
        $newProveedor->nombre = "BERTONATI S.A";
        $newProveedor->rut = "96825800-1";
        $newProveedor->transferencia = 0;
        $newProveedor->activo = 1;
        $newProveedor->save();

        $newProveedor = new Proveedor();
        $newProveedor->nombre = "GAES S.A.";
        $newProveedor->rut = "96848260-2";
        $newProveedor->transferencia = 0;
        $newProveedor->activo = 1;
        $newProveedor->save();

        $newProveedor = new Proveedor();
        $newProveedor->nombre = "LIBRA CHILE S.A.";
        $newProveedor->rut = "96859930-5";
        $newProveedor->email = "MFERRER@LABLIBRA.COM";
        $newProveedor->transferencia = 0;
        $newProveedor->activo = 1;
        $newProveedor->save();

        $newProveedor = new Proveedor();
        $newProveedor->nombre = "CLIMABEST LTDA.";
        $newProveedor->rut = "96872110-0";
        $newProveedor->transferencia = 0;
        $newProveedor->activo = 1;
        $newProveedor->save();

        $newProveedor = new Proveedor();
        $newProveedor->nombre = "ITF LABOMED FARMACEUTICA LTDA.";
        $newProveedor->rut = "96884770-8";
        $newProveedor->email = "SRETAMALES@ITF-LABOMED.CL";
        $newProveedor->transferencia = 0;
        $newProveedor->activo = 1;
        $newProveedor->save();

        $newProveedor = new Proveedor();
        $newProveedor->nombre = "CLÍNICA BICENTENARIO";
        $newProveedor->rut = "96885930-7";
        $newProveedor->email = "COBRANZAS@SERPROVENT.CL";
        $newProveedor->transferencia = 0;
        $newProveedor->activo = 1;
        $newProveedor->save();

        $newProveedor = new Proveedor();
        $newProveedor->nombre = "TAZ SA";
        $newProveedor->rut = "96891420-0";
        $newProveedor->email = "HILIAL@TAZ.CL";
        $newProveedor->transferencia = 0;
        $newProveedor->activo = 1;
        $newProveedor->save();

        $newProveedor = new Proveedor();
        $newProveedor->nombre = "SILPAK S.A.";
        $newProveedor->rut = "96894230-1";
        $newProveedor->transferencia = 0;
        $newProveedor->activo = 1;
        $newProveedor->save();

        $newProveedor = new Proveedor();
        $newProveedor->nombre = "DISTRIBUIDORA COMERCIAL PHARMASAN LTDA";
        $newProveedor->rut = "96895110-6";
        $newProveedor->email = "EGUERRERO@DROGUERIANUNOA.CL";
        $newProveedor->transferencia = 0;
        $newProveedor->activo = 1;
        $newProveedor->save();

        $newProveedor = new Proveedor();
        $newProveedor->nombre = "DISTRIBUIDORA MANZANO S.A.";
        $newProveedor->rut = "96908760-K";
        $newProveedor->transferencia = 0;
        $newProveedor->activo = 1;
        $newProveedor->save();

        $newProveedor = new Proveedor();
        $newProveedor->nombre = "ABATTE PRODUCTOS PARA OFICINA S.A.";
        $newProveedor->rut = "96909950-0";
        $newProveedor->email = "INFO@ABATTE.CL";
        $newProveedor->transferencia = 0;
        $newProveedor->activo = 1;
        $newProveedor->save();

        $newProveedor = new Proveedor();
        $newProveedor->nombre = "BIOCLIN";
        $newProveedor->rut = "96910280-3";
        $newProveedor->email = "BIOCLIN.SA@123.CL";
        $newProveedor->transferencia = 0;
        $newProveedor->activo = 1;
        $newProveedor->save();

        $newProveedor = new Proveedor();
        $newProveedor->nombre = "CLÍNICA LOS COIHUES";
        $newProveedor->rut = "96921660-4";
        $newProveedor->email = "AGENTECOBRO@CLINICALOSCOIHUES.CL";
        $newProveedor->transferencia = 0;
        $newProveedor->activo = 1;
        $newProveedor->save();

        $newProveedor = new Proveedor();
        $newProveedor->nombre = "VESPUCIO NORTE AUTOPISTA";
        $newProveedor->rut = "96922030-1";
        $newProveedor->transferencia = 0;
        $newProveedor->activo = 1;
        $newProveedor->save();

        $newProveedor = new Proveedor();
        $newProveedor->nombre = "CLINICA TABANCURA SA";
        $newProveedor->rut = "96923250-2";
        $newProveedor->email = "RHERNANDEZ@CLINICATABANCURA.CL";
        $newProveedor->transferencia = 0;
        $newProveedor->activo = 1;
        $newProveedor->save();

        $newProveedor = new Proveedor();
        $newProveedor->nombre = "TERUMO BCT CHILE S.A.";
        $newProveedor->rut = "96929230-0";
        $newProveedor->transferencia = 0;
        $newProveedor->activo = 1;
        $newProveedor->save();

        $newProveedor = new Proveedor();
        $newProveedor->nombre = "S.CONCESIONARIA AUTOPISTA CENTRAL";
        $newProveedor->rut = "96945440-8";
        $newProveedor->transferencia = 0;
        $newProveedor->activo = 1;
        $newProveedor->save();

        $newProveedor = new Proveedor();
        $newProveedor->nombre = "NOVOFARMA SERVICE S.A.";
        $newProveedor->rut = "96945670-2";
        $newProveedor->email = "JUAN.MORENO@NOVOFARMA.COM";
        $newProveedor->transferencia = 0;
        $newProveedor->activo = 1;
        $newProveedor->save();

        $newProveedor = new Proveedor();
        $newProveedor->nombre = "AUT. VESPUCIO SUR";
        $newProveedor->rut = "96972300-K";
        $newProveedor->transferencia = 0;
        $newProveedor->activo = 1;
        $newProveedor->save();

        $newProveedor = new Proveedor();
        $newProveedor->nombre = "INTERMED S.A";
        $newProveedor->rut = "96976780-5";
        $newProveedor->email = "HFAUNDEZ.INTERMED@GMAIL.COM";
        $newProveedor->transferencia = 0;
        $newProveedor->activo = 1;
        $newProveedor->save();

        $newProveedor = new Proveedor();
        $newProveedor->nombre = "PFIZER CHILE S.A.";
        $newProveedor->rut = "96981250-9";
        $newProveedor->email = "FRANCISCO.J.INOSTROZA@PFIZER.COM";
        $newProveedor->transferencia = 0;
        $newProveedor->activo = 1;
        $newProveedor->save();

        $newProveedor = new Proveedor();
        $newProveedor->nombre = "PARACLINICS S.A";
        $newProveedor->rut = "96986070-8";
        $newProveedor->email = "ICORDERO@ICLINICS.CL";
        $newProveedor->transferencia = 0;
        $newProveedor->activo = 1;
        $newProveedor->save();

        $newProveedor = new Proveedor();
        $newProveedor->nombre = "VESPUCIO NORTE EXPRESS";
        $newProveedor->rut = "96992030-1";
        $newProveedor->transferencia = 0;
        $newProveedor->activo = 1;
        $newProveedor->save();

        $newProveedor = new Proveedor();
        $newProveedor->nombre = "BANCO ESTADO DE CHILE";
        $newProveedor->rut = "97030000-7";
        $newProveedor->email = "operacionfactoring@bancoestado.cl";
        $newProveedor->transferencia = 0;
        $newProveedor->activo = 1;
        $newProveedor->save();

        $newProveedor = new Proveedor();
        $newProveedor->nombre = "INGIBORG ANDREA ARAYA CALERO";
        $newProveedor->rut = "9765988-5";
        $newProveedor->transferencia = 0;
        $newProveedor->activo = 1;
        $newProveedor->save();

        $newProveedor = new Proveedor();
        $newProveedor->nombre = "ANA PAOLA GOMEZ ARRIAGADA";
        $newProveedor->rut = "9831243-9";
        $newProveedor->transferencia = 0;
        $newProveedor->activo = 1;
        $newProveedor->save();

        $newProveedor = new Proveedor();
        $newProveedor->nombre = "BONZI HIDALGO DENISSE";
        $newProveedor->rut = "9909348-K";
        $newProveedor->transferencia = 0;
        $newProveedor->activo = 1;
        $newProveedor->save();

        $newProveedor = new Proveedor();
        $newProveedor->nombre = "NEPHROCARE CHILE S.A.";
        $newProveedor->rut = "99507130-4";
        $newProveedor->email = "MARIACAROLINA.GOMEZ@FMC-AG.COM";
        $newProveedor->transferencia = 0;
        $newProveedor->activo = 1;
        $newProveedor->save();

        $newProveedor = new Proveedor();
        $newProveedor->nombre = "I-MED S.A";
        $newProveedor->rut = "99509000-7";
        $newProveedor->email = "DCONTRERAS@AUTENTIA.CL";
        $newProveedor->transferencia = 0;
        $newProveedor->activo = 1;
        $newProveedor->save();

        $newProveedor = new Proveedor();
        $newProveedor->nombre = "UMS UNITED MEDICAL SYSTEMS CHILE";
        $newProveedor->rut = "99512950-7";
        $newProveedor->email = "VNUNEZ@UMSCHILE.COM";
        $newProveedor->transferencia = 0;
        $newProveedor->activo = 1;
        $newProveedor->save();

        $newProveedor = new Proveedor();
        $newProveedor->nombre = "LABORATORIO D&M PHARMA LTDA.";
        $newProveedor->rut = "99522620-0";
        $newProveedor->email = "CONTABILIDAD@DYMPHARMA.CL";
        $newProveedor->transferencia = 0;
        $newProveedor->activo = 1;
        $newProveedor->save();

        $newProveedor = new Proveedor();
        $newProveedor->nombre = "QUALIX BIOMEDICAL SA";
        $newProveedor->rut = "99539530-4";
        $newProveedor->transferencia = 0;
        $newProveedor->activo = 1;
        $newProveedor->save();

        $newProveedor = new Proveedor();
        $newProveedor->nombre = "INVERSIONES PMG S.A.";
        $newProveedor->rut = "99541890-8";
        $newProveedor->email = "BOLIVA@PMGPHARMA.COM";
        $newProveedor->transferencia = 0;
        $newProveedor->activo = 1;
        $newProveedor->save();

        $newProveedor = new Proveedor();
        $newProveedor->nombre = "INTERPHARMA S.A";
        $newProveedor->rut = "99543190-4";
        $newProveedor->email = "ALEJANDRO.MARTINEZ@IPHSA.CL";
        $newProveedor->transferencia = 0;
        $newProveedor->activo = 1;
        $newProveedor->save();

        $newProveedor = new Proveedor();
        $newProveedor->nombre = "ERGOTEC MUEBLES S.A.";
        $newProveedor->rut = "99546270-2";
        $newProveedor->transferencia = 0;
        $newProveedor->activo = 1;
        $newProveedor->save();

        $newProveedor = new Proveedor();
        $newProveedor->nombre = "LABORATORIOS KAMPAR S.A.";
        $newProveedor->rut = "99565040-1";
        $newProveedor->email = "LABORATORIOKAMPAR@KAMPAR.CL";
        $newProveedor->transferencia = 0;
        $newProveedor->activo = 1;
        $newProveedor->save();

        $newProveedor = new Proveedor();
        $newProveedor->nombre = "UC CHRISTUS SERVICIOS CLINICOS SPA";
        $newProveedor->rut = "99573490-7";
        $newProveedor->email = "FAGUAYO@MED.PUC.CL";
        $newProveedor->transferencia = 0;
        $newProveedor->activo = 1;
        $newProveedor->save();

        $newProveedor = new Proveedor();
        $newProveedor->nombre = "EXPRO S.A";
        $newProveedor->rut = "99574460-0";
        $newProveedor->email = "CONTABSCL@EXPRODENTAL.CL";
        $newProveedor->transferencia = 0;
        $newProveedor->activo = 1;
        $newProveedor->save();

        $newProveedor = new Proveedor();
        $newProveedor->nombre = "SUR MEDICAL SPA";
        $newProveedor->rut = "99576080-0";
        $newProveedor->email = "COBRANZA@SURMEDICAL.CL";
        $newProveedor->transferencia = 0;
        $newProveedor->activo = 1;
        $newProveedor->save();

        $newProveedor = new Proveedor();
        $newProveedor->nombre = "SERVICIOS CLINICOS SAN CARLOS DE APOQUINDO SA";
        $newProveedor->rut = "99582020-K";
        $newProveedor->email = "FAGUAYO@MED.PUC.CL";
        $newProveedor->transferencia = 0;
        $newProveedor->activo = 1;
        $newProveedor->save();

        $newProveedor = new Proveedor();
        $newProveedor->nombre = "CEGAMED CHILE S.A.";
        $newProveedor->rut = "99593170-2";
        $newProveedor->email = "JEANNETTE@CEGAMED.CL";
        $newProveedor->transferencia = 0;
        $newProveedor->activo = 1;
        $newProveedor->save();

        $newProveedor = new Proveedor();
        $newProveedor->nombre = "SOCIEDAD ONCOVIDA SA";
        $newProveedor->rut = "99598070-3";
        $newProveedor->transferencia = 0;
        $newProveedor->activo = 1;
        $newProveedor->save();

        $newProveedor = new Proveedor();
        $newProveedor->nombre = "VICTOR IGNACIO URIBARRI TOBAR";
        $newProveedor->rut = "7230780-1";
        $newProveedor->transferencia = 0;
        $newProveedor->activo = 1;
        $newProveedor->save();

        $newProveedor = new Proveedor();
        $newProveedor->nombre = "GERMAN BERNARDO BEHNKE GUTIERREZ";
        $newProveedor->rut = "5892120-3";
        $newProveedor->transferencia = 0;
        $newProveedor->activo = 1;
        $newProveedor->save();

        $newProveedor = new Proveedor();
        $newProveedor->nombre = "SWEDPHARM LIMITADA";
        $newProveedor->rut = "96850190-9";
        $newProveedor->transferencia = 0;
        $newProveedor->activo = 1;
        $newProveedor->save();

        $newProveedor = new Proveedor();
        $newProveedor->nombre = "EQUIPOS E INSUMOS MÉDICOS Y DE LAB NEW PATH CHILE SPA";
        $newProveedor->rut = "77899260-4";
        $newProveedor->transferencia = 0;
        $newProveedor->activo = 1;
        $newProveedor->save();

        $newProveedor = new Proveedor();
        $newProveedor->nombre = "BEROIZA SEGUROS SPA";
        $newProveedor->rut = "76564985-4";
        $newProveedor->email = "isabel_seguros@hotmail.com";
        $newProveedor->transferencia = 0;
        $newProveedor->activo = 1;
        $newProveedor->save();

        $newProveedor = new Proveedor();
        $newProveedor->nombre = "EMPRESA DE SERVICIOS H&H LIMITADA";
        $newProveedor->rut = "76554699-0";
        $newProveedor->email = "hcid@servicioshyh.cl";
        $newProveedor->transferencia = 0;
        $newProveedor->activo = 1;
        $newProveedor->save();

        $newProveedor = new Proveedor();
        $newProveedor->nombre = "MARGARITA ANDRADE CUADRA";
        $newProveedor->rut = "5803818-0";
        $newProveedor->transferencia = 0;
        $newProveedor->activo = 1;
        $newProveedor->save();

        $newProveedor = new Proveedor();
        $newProveedor->nombre = "FRANCISCO MAURO FARIAS VILCHES";
        $newProveedor->rut = "9981005-K";
        $newProveedor->direccion = "EMBAJADOR NOEL 487, SAN JOAQUIN";
        $newProveedor->telefono = "225546017";
        $newProveedor->email = "I.AFFARIP@GMAIL.COM";
        $newProveedor->transferencia = 0;
        $newProveedor->activo = 1;
        $newProveedor->save();

        $newProveedor = new Proveedor();
        $newProveedor->nombre = "IMPORTADORA MEGAMARKET SPA";
        $newProveedor->rut = "76426458-4";
        $newProveedor->direccion = "impor";
        $newProveedor->email = "c.honorato@megamarketchile.cl";
        $newProveedor->transferencia = 0;
        $newProveedor->activo = 1;
        $newProveedor->save();

        $newProveedor = new Proveedor();
        $newProveedor->nombre = "SOCIEDAD SOLUCIONES INTEGRALES SPA";
        $newProveedor->rut = "76540457-6";
        $newProveedor->transferencia = 0;
        $newProveedor->activo = 1;
        $newProveedor->save();

        $newProveedor = new Proveedor();
        $newProveedor->nombre = "DOMINGO SEGUNDO JOFRE BRAVO";
        $newProveedor->rut = "4750381-7";
        $newProveedor->transferencia = 0;
        $newProveedor->activo = 1;
        $newProveedor->save();

        $newProveedor = new Proveedor();
        $newProveedor->nombre = "LABORATORIO LIVIO BARNAFI S.A.";
        $newProveedor->rut = "88987500-3";
        $newProveedor->transferencia = 0;
        $newProveedor->activo = 1;
        $newProveedor->save();

        $newProveedor = new Proveedor();
        $newProveedor->nombre = "COMERCIAL PEREZ Y NIKLITSCHEK LTDA.";
        $newProveedor->rut = "84456700-6";
        $newProveedor->transferencia = 0;
        $newProveedor->activo = 1;
        $newProveedor->save();

        $newProveedor = new Proveedor();
        $newProveedor->nombre = "DANIEL ALEJANDRO PARRA MORALES";
        $newProveedor->rut = "15069725-5";
        $newProveedor->email = "dparram@gmail.com";
        $newProveedor->transferencia = 0;
        $newProveedor->activo = 1;
        $newProveedor->save();

        $newProveedor = new Proveedor();
        $newProveedor->nombre = "bioheis spa";
        $newProveedor->rut = "76532722-9";
        $newProveedor->email = "helia.espejo@bioheis.cl";
        $newProveedor->transferencia = 0;
        $newProveedor->activo = 1;
        $newProveedor->save();

        $newProveedor = new Proveedor();
        $newProveedor->nombre = "MARMEDICAL SPA";
        $newProveedor->rut = "76609571-2";
        $newProveedor->email = "rgonzalez@marmedical.cl";
        $newProveedor->transferencia = 0;
        $newProveedor->activo = 1;
        $newProveedor->save();

        $newProveedor = new Proveedor();
        $newProveedor->nombre = "MIGUEL MORALES PARDO Y COMPAÑIA LIMITADA";
        $newProveedor->rut = "77579350-3";
        $newProveedor->email = "destrada@midacor.cl";
        $newProveedor->transferencia = 0;
        $newProveedor->activo = 1;
        $newProveedor->save();

        $newProveedor = new Proveedor();
        $newProveedor->nombre = "ACRUX LABS S.A.";
        $newProveedor->rut = "76146075-7";
        $newProveedor->email = "ventas@acruxlabs.cl";
        $newProveedor->transferencia = 0;
        $newProveedor->activo = 1;
        $newProveedor->save();

        $newProveedor = new Proveedor();
        $newProveedor->nombre = "SERVICIOS INTEGRADOS SIDAPT LTDA.";
        $newProveedor->rut = "76299358-9";
        $newProveedor->email = "EGIDIO.VILLA@GMAIL.COM";
        $newProveedor->transferencia = 0;
        $newProveedor->activo = 1;
        $newProveedor->save();

        $newProveedor = new Proveedor();
        $newProveedor->nombre = "LABORATORIO LKM CHILE SPA";
        $newProveedor->rut = "76377981-5";
        $newProveedor->email = "eduardo.bolt@grupobiotoscana.com";
        $newProveedor->transferencia = 0;
        $newProveedor->activo = 1;
        $newProveedor->save();

        $newProveedor = new Proveedor();
        $newProveedor->nombre = "PISA FARMACEUTICA DE CHILE SPA";
        $newProveedor->rut = "76423281-K";
        $newProveedor->transferencia = 0;
        $newProveedor->activo = 1;
        $newProveedor->save();

        $newProveedor = new Proveedor();
        $newProveedor->nombre = "ECOFFICE COMPUTACION LTDA.";
        $newProveedor->rut = "76293503-1";
        $newProveedor->transferencia = 0;
        $newProveedor->activo = 1;
        $newProveedor->save();

        $newProveedor = new Proveedor();
        $newProveedor->nombre = "SEVEN PHARMA CHILE SPA";
        $newProveedor->rut = "76437991-8";
        $newProveedor->email = "german.v@sevenpharma.net";
        $newProveedor->transferencia = 0;
        $newProveedor->activo = 1;
        $newProveedor->save();

        $newProveedor = new Proveedor();
        $newProveedor->nombre = "VESTUARIOS INDUSTRIALES S.A";
        $newProveedor->rut = "76449260-9";
        $newProveedor->transferencia = 0;
        $newProveedor->activo = 1;
        $newProveedor->save();

        $newProveedor = new Proveedor();
        $newProveedor->nombre = "CAVIMED CHILE SPA";
        $newProveedor->rut = "76496294-K";
        $newProveedor->email = "info@cavimeddechilespa.cl";
        $newProveedor->transferencia = 0;
        $newProveedor->activo = 1;
        $newProveedor->save();

        $newProveedor = new Proveedor();
        $newProveedor->nombre = "AZIMUTH MEDICAL DEVICES SPA";
        $newProveedor->rut = "76498795-0";
        $newProveedor->email = "esteban@azimuthmd.com";
        $newProveedor->transferencia = 0;
        $newProveedor->activo = 1;
        $newProveedor->save();

        $newProveedor = new Proveedor();
        $newProveedor->nombre = "IQ S.A";
        $newProveedor->rut = "76585618-3";
        $newProveedor->email = "LBAO@IQ-SA.CL";
        $newProveedor->transferencia = 0;
        $newProveedor->activo = 1;
        $newProveedor->save();

        $newProveedor = new Proveedor();
        $newProveedor->nombre = "EXELTIS CHILE SPA";
        $newProveedor->rut = "76383221-K";
        $newProveedor->transferencia = 0;
        $newProveedor->activo = 1;
        $newProveedor->save();

        $newProveedor = new Proveedor();
        $newProveedor->nombre = "COMERCIALIZADORA MATCHING DEALS LTDA.";
        $newProveedor->rut = "76387797-3";
        $newProveedor->transferencia = 0;
        $newProveedor->activo = 1;
        $newProveedor->save();

        $newProveedor = new Proveedor();
        $newProveedor->nombre = "PUNTO GRAFIX SPA";
        $newProveedor->rut = "76426465-7";
        $newProveedor->transferencia = 0;
        $newProveedor->activo = 1;
        $newProveedor->save();

        $newProveedor = new Proveedor();
        $newProveedor->nombre = "VITAMINA WORK LIFE S.A";
        $newProveedor->rut = "76407810-1";
        $newProveedor->transferencia = 0;
        $newProveedor->activo = 1;
        $newProveedor->save();

        $newProveedor = new Proveedor();
        $newProveedor->nombre = "COMERCIAL FIRST SPA";
        $newProveedor->rut = "76433824-3";
        $newProveedor->transferencia = 0;
        $newProveedor->activo = 1;
        $newProveedor->save();

        $newProveedor = new Proveedor();
        $newProveedor->nombre = "VITALSEC SPA";
        $newProveedor->rut = "76325278-7";
        $newProveedor->transferencia = 0;
        $newProveedor->activo = 1;
        $newProveedor->save();

        $newProveedor = new Proveedor();
        $newProveedor->nombre = "COMERCIAL I-SENS CHILE SPA";
        $newProveedor->rut = "76440218-9";
        $newProveedor->transferencia = 0;
        $newProveedor->activo = 1;
        $newProveedor->save();

        $newProveedor = new Proveedor();
        $newProveedor->nombre = "SERV. Y COM. DE INSUMOS MEDICOS SPA";
        $newProveedor->rut = "77125800-K";
        $newProveedor->email = "sjana@medicalchoice.cl";
        $newProveedor->transferencia = 0;
        $newProveedor->activo = 1;
        $newProveedor->save();

        $newProveedor = new Proveedor();
        $newProveedor->nombre = "LACROSSE INTERVENTIONAL CHILE SPA";
        $newProveedor->rut = "76115259-9";
        $newProveedor->email = "factoring@lacrosse.cl";
        $newProveedor->transferencia = 0;
        $newProveedor->activo = 1;
        $newProveedor->save();

        $newProveedor = new Proveedor();
        $newProveedor->nombre = "pharmavita sociedad  anonima";
        $newProveedor->rut = "77709540-4";
        $newProveedor->transferencia = 0;
        $newProveedor->activo = 1;
        $newProveedor->save();

        $newProveedor = new Proveedor();
        $newProveedor->nombre = "SANDRA GIOCONDA TELLO LOPEZ";
        $newProveedor->rut = "8966563-9";
        $newProveedor->transferencia = 0;
        $newProveedor->activo = 1;
        $newProveedor->save();

        $newProveedor = new Proveedor();
        $newProveedor->nombre = "LABORATORIO WYETH LLC.";
        $newProveedor->rut = "82496800-4";
        $newProveedor->transferencia = 0;
        $newProveedor->activo = 1;
        $newProveedor->save();

        $newProveedor = new Proveedor();
        $newProveedor->nombre = "ADVANTAGE COMPUTACION LTDA.";
        $newProveedor->rut = "77879090-4";
        $newProveedor->email = "ddomenech@advantage.cl";
        $newProveedor->transferencia = 0;
        $newProveedor->activo = 1;
        $newProveedor->save();

        $newProveedor = new Proveedor();
        $newProveedor->nombre = "DIST SICFARMA CHILE SPA";
        $newProveedor->rut = "76409952-4";
        $newProveedor->email = "contacto@sicmafarma.cl";
        $newProveedor->transferencia = 0;
        $newProveedor->activo = 1;
        $newProveedor->save();

        $newProveedor = new Proveedor();
        $newProveedor->nombre = "SCHONFELDT VIDAMEDICA SPA";
        $newProveedor->rut = "76352414-0";
        $newProveedor->transferencia = 0;
        $newProveedor->activo = 1;
        $newProveedor->save();

        $newProveedor = new Proveedor();
        $newProveedor->nombre = "COMERCIAL TEXMED LIMITADA";
        $newProveedor->rut = "76151052-5";
        $newProveedor->email = "contacto@texmed.cl";
        $newProveedor->transferencia = 0;
        $newProveedor->activo = 1;
        $newProveedor->save();

        $newProveedor = new Proveedor();
        $newProveedor->nombre = "C & J PRINTING  LIMITADA";
        $newProveedor->rut = "76428421-6";
        $newProveedor->email = "RARAOS@synergy.cl";
        $newProveedor->transferencia = 0;
        $newProveedor->activo = 1;
        $newProveedor->save();

        $newProveedor = new Proveedor();
        $newProveedor->nombre = "KAUFMANN S.A";
        $newProveedor->rut = "92475000-6";
        $newProveedor->transferencia = 0;
        $newProveedor->activo = 1;
        $newProveedor->save();

        $newProveedor = new Proveedor();
        $newProveedor->nombre = "UPGRADE (CHILE) S.A.";
        $newProveedor->rut = "96522220-0";
        $newProveedor->email = "lmonroy@upgrade.cl";
        $newProveedor->transferencia = 0;
        $newProveedor->activo = 1;
        $newProveedor->save();

        $newProveedor = new Proveedor();
        $newProveedor->nombre = "COMERCIALIZADORA ABBA SOC .DE RESPONSABILIDADES LTDA.";
        $newProveedor->rut = "76158074-4";
        $newProveedor->transferencia = 0;
        $newProveedor->activo = 1;
        $newProveedor->save();

        $newProveedor = new Proveedor();
        $newProveedor->nombre = "INNOVA MEDICAL Y COMPAÑIA LIMITADA";
        $newProveedor->rut = "76365446-K";
        $newProveedor->transferencia = 0;
        $newProveedor->activo = 1;
        $newProveedor->save();

        $newProveedor = new Proveedor();
        $newProveedor->nombre = "DIMACOFI S.A";
        $newProveedor->rut = "92083000-5";
        $newProveedor->email = "gpalacios@dimacofi.cl";
        $newProveedor->transferencia = 0;
        $newProveedor->activo = 1;
        $newProveedor->save();

        $newProveedor = new Proveedor();
        $newProveedor->nombre = "LORENZO JIMENEZ E HIJO LTDA.";
        $newProveedor->rut = "79636400-9";
        $newProveedor->transferencia = 0;
        $newProveedor->activo = 1;
        $newProveedor->save();

        $newProveedor = new Proveedor();
        $newProveedor->nombre = "ART MEDICOS Y QUIRURGICOS CHILE S.A.";
        $newProveedor->rut = "76209836-9";
        $newProveedor->email = "ISIDORA.GARRIGA@SECURITY.CL";
        $newProveedor->transferencia = 0;
        $newProveedor->activo = 1;
        $newProveedor->save();

        $newProveedor = new Proveedor();
        $newProveedor->nombre = "MEDBIOTEC SPA";
        $newProveedor->rut = "76280494-8";
        $newProveedor->email = "projas@labofar.cl";
        $newProveedor->transferencia = 0;
        $newProveedor->activo = 1;
        $newProveedor->save();

        $newProveedor = new Proveedor();
        $newProveedor->nombre = "COMERCIALIZADORA DE INSUMOS PARA CIRUGIA  PLASTICA Y REPARADORA MEDPLAS";
        $newProveedor->rut = "76819200-6";
        $newProveedor->transferencia = 0;
        $newProveedor->activo = 1;
        $newProveedor->save();

        $newProveedor = new Proveedor();
        $newProveedor->nombre = "SOC COMERCIAL BJ SPA";
        $newProveedor->rut = "96969310-0";
        $newProveedor->transferencia = 0;
        $newProveedor->activo = 1;
        $newProveedor->save();

        $newProveedor = new Proveedor();
        $newProveedor->nombre = "SOC. DE PROFESIONALES PINO Y CORDOVA LTDA.";
        $newProveedor->rut = "76278020-8";
        $newProveedor->direccion = "JUAN MOYA Nº121-2";
        $newProveedor->telefono = "22-2715056";
        $newProveedor->email = "roaraya@gmail.com";
        $newProveedor->transferencia = 0;
        $newProveedor->activo = 1;
        $newProveedor->save();

        $newProveedor = new Proveedor();
        $newProveedor->nombre = "SERVICIO TECNICO EN ELEVACION LIMITADA";
        $newProveedor->rut = "76442721-1";
        $newProveedor->transferencia = 0;
        $newProveedor->activo = 1;
        $newProveedor->save();

        $newProveedor = new Proveedor();
        $newProveedor->nombre = "AUDITRON CHILE S.A.";
        $newProveedor->rut = "77707390-7";
        $newProveedor->transferencia = 0;
        $newProveedor->activo = 1;
        $newProveedor->save();

        $newProveedor = new Proveedor();
        $newProveedor->nombre = "SOC.COMERCIAL FREDES Y COYDAN LTDA";
        $newProveedor->rut = "76171739-1";
        $newProveedor->transferencia = 0;
        $newProveedor->activo = 1;
        $newProveedor->save();

        $newProveedor = new Proveedor();
        $newProveedor->nombre = "KINEARS SPA";
        $newProveedor->rut = "76542336-8";
        $newProveedor->transferencia = 0;
        $newProveedor->activo = 1;
        $newProveedor->save();

        $newProveedor = new Proveedor();
        $newProveedor->nombre = "DIPROVET DIST DE PROD VETERINARIOS SPA";
        $newProveedor->rut = "76080982-9";
        $newProveedor->transferencia = 0;
        $newProveedor->activo = 1;
        $newProveedor->save();

        $newProveedor = new Proveedor();
        $newProveedor->nombre = "COM. INTERMEDICA LTDA";
        $newProveedor->rut = "76205137-0";
        $newProveedor->transferencia = 0;
        $newProveedor->activo = 1;
        $newProveedor->save();

        $newProveedor = new Proveedor();
        $newProveedor->nombre = "SERV DE INGENIERÍA EN AGUAS CLÍNICAS SIAC LTDA";
        $newProveedor->rut = "76679310-K";
        $newProveedor->transferencia = 0;
        $newProveedor->activo = 1;
        $newProveedor->save();

        $newProveedor = new Proveedor();
        $newProveedor->nombre = "ARANCIBIA Y WEISS QUIMICOS LTDA";
        $newProveedor->rut = "76339652-5";
        $newProveedor->transferencia = 0;
        $newProveedor->activo = 1;
        $newProveedor->save();

        $newProveedor = new Proveedor();
        $newProveedor->nombre = "ACCUVISION SPA";
        $newProveedor->rut = "76349842-5";
        $newProveedor->email = "mackarena.gallardo@orsan.cl";
        $newProveedor->transferencia = 0;
        $newProveedor->activo = 1;
        $newProveedor->save();

        $newProveedor = new Proveedor();
        $newProveedor->nombre = "CLINICA DE TRATAMIENTO DE HERIDAS CONSAN S.A.";
        $newProveedor->rut = "76095233-8";
        $newProveedor->email = "info@clinicaconsan.cl";
        $newProveedor->transferencia = 0;
        $newProveedor->activo = 1;
        $newProveedor->save();

        $newProveedor = new Proveedor();
        $newProveedor->nombre = "FACTORIZA S.A.";
        $newProveedor->rut = "77722760-2";
        $newProveedor->email = "pmunoz@factoriza.cl";
        $newProveedor->transferencia = 0;
        $newProveedor->activo = 1;
        $newProveedor->save();

        $newProveedor = new Proveedor();
        $newProveedor->nombre = "CONST E INM R&R SPA";
        $newProveedor->rut = "76499797-2";
        $newProveedor->email = "CONSTRUCTORA.RYRS@GMAIL.COM";
        $newProveedor->transferencia = 0;
        $newProveedor->activo = 1;
        $newProveedor->save();

        $newProveedor = new Proveedor();
        $newProveedor->nombre = "MEDITERRANEO AUTOMOTORES S.A.";
        $newProveedor->rut = "96889440-4";
        $newProveedor->email = "emaulen@circuloautos.cl";
        $newProveedor->transferencia = 0;
        $newProveedor->activo = 1;
        $newProveedor->save();

        $newProveedor = new Proveedor();
        $newProveedor->nombre = "DIALISIS AGUDA MOVIL SPA";
        $newProveedor->rut = "76580789-1";
        $newProveedor->transferencia = 0;
        $newProveedor->activo = 1;
        $newProveedor->save();

        $newProveedor = new Proveedor();
        $newProveedor->nombre = "MORENO ASOCIADOS LTDA";
        $newProveedor->rut = "78762910-5";
        $newProveedor->transferencia = 0;
        $newProveedor->activo = 1;
        $newProveedor->save();

        $newProveedor = new Proveedor();
        $newProveedor->nombre = "MAX EDUARDO SARAVIA SUAREZ";
        $newProveedor->rut = "13962630-3";
        $newProveedor->transferencia = 0;
        $newProveedor->activo = 1;
        $newProveedor->save();

        $newProveedor = new Proveedor();
        $newProveedor->nombre = "CLIMATIZACION BERNARDO HERNANDEZ REYES EMPRESA INDIVIDUAL RESPONSAB";
        $newProveedor->rut = "76813906-7";
        $newProveedor->email = "bernardohr@gmail.com";
        $newProveedor->transferencia = 0;
        $newProveedor->activo = 1;
        $newProveedor->save();

        $newProveedor = new Proveedor();
        $newProveedor->nombre = "INSTITUTO OFTALMOLOGICO PROFESOR ARENTSEN S.A (IOPA)";
        $newProveedor->rut = "93915000-5";
        $newProveedor->transferencia = 0;
        $newProveedor->activo = 1;
        $newProveedor->save();

        $newProveedor = new Proveedor();
        $newProveedor->nombre = "MEDINOVA LIMITADA";
        $newProveedor->rut = "76099325-5";
        $newProveedor->transferencia = 0;
        $newProveedor->activo = 1;
        $newProveedor->save();

        $newProveedor = new Proveedor();
        $newProveedor->nombre = "IDENTICARD SPA";
        $newProveedor->rut = "96750760-1";
        $newProveedor->transferencia = 0;
        $newProveedor->activo = 1;
        $newProveedor->save();

        $newProveedor = new Proveedor();
        $newProveedor->nombre = "CIMEDICAL SPA";
        $newProveedor->rut = "76485494-2";
        $newProveedor->transferencia = 0;
        $newProveedor->activo = 1;
        $newProveedor->save();

        $newProveedor = new Proveedor();
        $newProveedor->nombre = "C Y D LIMITADA";
        $newProveedor->rut = "76394174-4";
        $newProveedor->email = "ecid@cydpharma.cl";
        $newProveedor->transferencia = 0;
        $newProveedor->activo = 1;
        $newProveedor->save();

        $newProveedor = new Proveedor();
        $newProveedor->nombre = "UNIFARMA SPA";
        $newProveedor->rut = "76416055-K";
        $newProveedor->email = "ecid@unifarma.cl";
        $newProveedor->transferencia = 0;
        $newProveedor->activo = 1;
        $newProveedor->save();

        $newProveedor = new Proveedor();
        $newProveedor->nombre = "FAES FARMA CHILE SALUD Y NUTRICION LTDA.";
        $newProveedor->rut = "76065775-1";
        $newProveedor->email = "PDONOSO@FAESFARMA.COM";
        $newProveedor->transferencia = 0;
        $newProveedor->activo = 1;
        $newProveedor->save();

        $newProveedor = new Proveedor();
        $newProveedor->nombre = "SOCIEDAD ZERENE MARTINEZ Y COMPAÑIA LIMITADA";
        $newProveedor->rut = "76321180-0";
        $newProveedor->transferencia = 0;
        $newProveedor->activo = 1;
        $newProveedor->save();

        $newProveedor = new Proveedor();
        $newProveedor->nombre = "MERCK SHARP & DOHME CHILE LTDA.";
        $newProveedor->rut = "80865300-1";
        $newProveedor->email = "freddy.olmedo@merck.com";
        $newProveedor->transferencia = 0;
        $newProveedor->activo = 1;
        $newProveedor->save();

        $newProveedor = new Proveedor();
        $newProveedor->nombre = "AGUA PURIFICADA VAI VERI LTDA.";
        $newProveedor->rut = "76286634-K";
        $newProveedor->transferencia = 0;
        $newProveedor->activo = 1;
        $newProveedor->save();

        $newProveedor = new Proveedor();
        $newProveedor->nombre = "LABORATORIO HOSPIFARMA CHILE LTDA.";
        $newProveedor->rut = "76133312-7";
        $newProveedor->transferencia = 0;
        $newProveedor->activo = 1;
        $newProveedor->save();

        $newProveedor = new Proveedor();
        $newProveedor->nombre = "COMERCIALIZADORA LIZETTE FAUNDEZ MARTINEZ EIRL";
        $newProveedor->rut = "76080334-0";
        $newProveedor->transferencia = 0;
        $newProveedor->activo = 1;
        $newProveedor->save();

        $newProveedor = new Proveedor();
        $newProveedor->nombre = "FARMA STORAGE SPA";
        $newProveedor->rut = "96699730-3";
        $newProveedor->email = "priscila.perez@grunenthal.com";
        $newProveedor->transferencia = 0;
        $newProveedor->activo = 1;
        $newProveedor->save();

        $newProveedor = new Proveedor();
        $newProveedor->nombre = "AMARA  NATURAL PRODUCTS DISTRIBUIDORA LTDA.";
        $newProveedor->rut = "76070033-9";
        $newProveedor->transferencia = 0;
        $newProveedor->activo = 1;
        $newProveedor->save();

        $newProveedor = new Proveedor();
        $newProveedor->nombre = "CLINICA VESPUCIO S.A.";
        $newProveedor->rut = "96898980-4";
        $newProveedor->transferencia = 0;
        $newProveedor->activo = 1;
        $newProveedor->save();

        $newProveedor = new Proveedor();
        $newProveedor->nombre = "TTW SAKE SPA";
        $newProveedor->rut = "76222476-3";
        $newProveedor->email = "jurzua@biomedcare.cl";
        $newProveedor->transferencia = 0;
        $newProveedor->activo = 1;
        $newProveedor->save();

        $newProveedor = new Proveedor();
        $newProveedor->nombre = "PHARMA TRADE S.A.";
        $newProveedor->rut = "96670640-6";
        $newProveedor->email = "paola.ponce@laboratoriochile.cl";
        $newProveedor->transferencia = 0;
        $newProveedor->activo = 1;
        $newProveedor->save();

        $newProveedor = new Proveedor();
        $newProveedor->nombre = "OPTICAS PUNTO VISUAL LIMITADA";
        $newProveedor->rut = "76209854-7";
        $newProveedor->transferencia = 0;
        $newProveedor->activo = 1;
        $newProveedor->save();

        $newProveedor = new Proveedor();
        $newProveedor->nombre = "SOC DE PRODUCTOS CLINICOS Y OTROS LIMITADA";
        $newProveedor->rut = "76023372-2";
        $newProveedor->transferencia = 0;
        $newProveedor->activo = 1;
        $newProveedor->save();

        $newProveedor = new Proveedor();
        $newProveedor->nombre = "IMP Y DISTRIBUIDORA NOVAMEDICAL CHILE LTDA.";
        $newProveedor->rut = "76298809-7";
        $newProveedor->transferencia = 0;
        $newProveedor->activo = 1;
        $newProveedor->save();

        $newProveedor = new Proveedor();
        $newProveedor->nombre = "COMERCIAL MULTIBRANDS LTDA.";
        $newProveedor->rut = "76118886-0";
        $newProveedor->email = "jhutter@hofmann.cl";
        $newProveedor->transferencia = 0;
        $newProveedor->activo = 1;
        $newProveedor->save();

        $newProveedor = new Proveedor();
        $newProveedor->nombre = "MONICA DE LA FUENTES FARIAS";
        $newProveedor->rut = "15771608-5";
        $newProveedor->transferencia = 0;
        $newProveedor->activo = 1;
        $newProveedor->save();

        $newProveedor = new Proveedor();
        $newProveedor->nombre = "SIEMENS HEALTHCARE EQUIPOS MEDICOS SPA";
        $newProveedor->rut = "76481921-7";
        $newProveedor->transferencia = 0;
        $newProveedor->activo = 1;
        $newProveedor->save();

        $newProveedor = new Proveedor();
        $newProveedor->nombre = "ALEXA BELTRAN CATALDO";
        $newProveedor->rut = "15063765-1";
        $newProveedor->transferencia = 0;
        $newProveedor->activo = 1;
        $newProveedor->save();

        $newProveedor = new Proveedor();
        $newProveedor->nombre = "CONSTRUCCION Y REMODELACIONES DETAL LTDA";
        $newProveedor->rut = "76161342-1";
        $newProveedor->email = "contacto@detal.cl";
        $newProveedor->transferencia = 0;
        $newProveedor->activo = 1;
        $newProveedor->save();

        $newProveedor = new Proveedor();
        $newProveedor->nombre = "SUPREME VISION CHELLARANI KUMARASAMY EMPRESA INDIVIDUAL DE RESPONSABILIDAD LTDA";
        $newProveedor->rut = "76354792-2";
        $newProveedor->transferencia = 0;
        $newProveedor->activo = 1;
        $newProveedor->save();

        $newProveedor = new Proveedor();
        $newProveedor->nombre = "SOCIEDAD COMERCIAL AQUAOMEGA EQUIPOS LIMITADA";
        $newProveedor->rut = "76201107-7";
        $newProveedor->email = "rjara@omegatemuco.cl";
        $newProveedor->transferencia = 0;
        $newProveedor->activo = 1;
        $newProveedor->save();

        $newProveedor = new Proveedor();
        $newProveedor->nombre = "SOCIEDAD DE INVERSIONES RUDGOLD LIMITADA";
        $newProveedor->rut = "76188286-4";
        $newProveedor->transferencia = 0;
        $newProveedor->activo = 1;
        $newProveedor->save();

        $newProveedor = new Proveedor();
        $newProveedor->nombre = "TELECOMUNICACIONES REDES E INGENIERIA HECTOR GUILLERMO PEÑA FLORES E.";
        $newProveedor->rut = "76752984-8";
        $newProveedor->email = "hgpf71@gmail.com";
        $newProveedor->transferencia = 0;
        $newProveedor->activo = 1;
        $newProveedor->save();

        $newProveedor = new Proveedor();
        $newProveedor->nombre = "WIDEX CHILE SPA";
        $newProveedor->rut = "76136334-4";
        $newProveedor->transferencia = 0;
        $newProveedor->activo = 1;
        $newProveedor->save();

        $newProveedor = new Proveedor();
        $newProveedor->nombre = "SERVICIOS DE INGENIERIA AMARKATEC LIMITADA";
        $newProveedor->rut = "76508009-6";
        $newProveedor->transferencia = 0;
        $newProveedor->activo = 1;
        $newProveedor->save();

        $newProveedor = new Proveedor();
        $newProveedor->nombre = "GRAFHIKA COPY CENTER LDA";
        $newProveedor->rut = "78953430-6";
        $newProveedor->transferencia = 0;
        $newProveedor->activo = 1;
        $newProveedor->save();

        $newProveedor = new Proveedor();
        $newProveedor->nombre = "MEDICA TEC DE CHILE S.A.";
        $newProveedor->rut = "96851720-1";
        $newProveedor->email = "clinica@medica-tec.cl";
        $newProveedor->transferencia = 0;
        $newProveedor->activo = 1;
        $newProveedor->save();

        $newProveedor = new Proveedor();
        $newProveedor->nombre = "LEONEL PASCUAL VARGAS MONTOYA";
        $newProveedor->rut = "7143445-1";
        $newProveedor->email = "leo.misterpack@gmail.com";
        $newProveedor->transferencia = 0;
        $newProveedor->activo = 1;
        $newProveedor->save();

        $newProveedor = new Proveedor();
        $newProveedor->nombre = "ECOCARE SPA";
        $newProveedor->rut = "76709214-8";
        $newProveedor->email = "info@ecocarechile.cl";
        $newProveedor->transferencia = 0;
        $newProveedor->activo = 1;
        $newProveedor->save();

        $newProveedor = new Proveedor();
        $newProveedor->nombre = "DEUTSCHE PHARMA S.A.";
        $newProveedor->rut = "76018782-8";
        $newProveedor->email = "fernando.cabrera@deutschepharma.cl";
        $newProveedor->transferencia = 0;
        $newProveedor->activo = 1;
        $newProveedor->save();

        $newProveedor = new Proveedor();
        $newProveedor->nombre = "INVERSIONES PHARMAVISAN S.A";
        $newProveedor->rut = "76055804-4";
        $newProveedor->transferencia = 0;
        $newProveedor->activo = 1;
        $newProveedor->save();

        $newProveedor = new Proveedor();
        $newProveedor->nombre = "SERVICIOS DE ADMINISTRACION Y GESTION RADO SPA";
        $newProveedor->rut = "76178813-2";
        $newProveedor->transferencia = 0;
        $newProveedor->activo = 1;
        $newProveedor->save();

        $newProveedor = new Proveedor();
        $newProveedor->nombre = "SODEXO SOLUCIONES DE MOTIVACION CHILE";
        $newProveedor->rut = "96556930-8";
        $newProveedor->transferencia = 0;
        $newProveedor->activo = 1;
        $newProveedor->save();

        $newProveedor = new Proveedor();
        $newProveedor->nombre = "SOPORTE Y PROYECTOS EN COMPUTACION LIMITADA";
        $newProveedor->rut = "79710740-9";
        $newProveedor->email = "lespina@spcchile.cl";
        $newProveedor->transferencia = 0;
        $newProveedor->activo = 1;
        $newProveedor->save();

        $newProveedor = new Proveedor();
        $newProveedor->nombre = "JAIME INOSTROZA SARMIENTO Y COMPAÑIA LIMITADA";
        $newProveedor->rut = "78892370-8";
        $newProveedor->transferencia = 0;
        $newProveedor->activo = 1;
        $newProveedor->save();

        $newProveedor = new Proveedor();
        $newProveedor->nombre = "LABORATORIOS STIEFEL DE CHILE Y CIA LTDA";
        $newProveedor->rut = "79541150-K";
        $newProveedor->transferencia = 0;
        $newProveedor->activo = 1;
        $newProveedor->save();

        $newProveedor = new Proveedor();
        $newProveedor->nombre = "LABORATORIO VALMA S.A.";
        $newProveedor->rut = "80048900-8";
        $newProveedor->transferencia = 0;
        $newProveedor->activo = 1;
        $newProveedor->save();

        $newProveedor = new Proveedor();
        $newProveedor->nombre = "AEDOXIGENO LIMITADA";
        $newProveedor->rut = "77417520-2";
        $newProveedor->transferencia = 0;
        $newProveedor->activo = 1;
        $newProveedor->save();

        $newProveedor = new Proveedor();
        $newProveedor->nombre = "COMERCIAL HUERTOS DEL SUR LIMITADA";
        $newProveedor->rut = "76359104-2";
        $newProveedor->transferencia = 0;
        $newProveedor->activo = 1;
        $newProveedor->save();

        $newProveedor = new Proveedor();
        $newProveedor->nombre = "WENCESLAO PEREZ LEWENS";
        $newProveedor->rut = "8754277-7";
        $newProveedor->transferencia = 0;
        $newProveedor->activo = 1;
        $newProveedor->save();

        $newProveedor = new Proveedor();
        $newProveedor->nombre = "DISTRIBUIDORA PAPELES INDUSTRIALES S.A.";
        $newProveedor->rut = "93558000-5";
        $newProveedor->transferencia = 0;
        $newProveedor->activo = 1;
        $newProveedor->save();

        $newProveedor = new Proveedor();
        $newProveedor->nombre = "PROFESIONALES CARDIOVASCULARES PROCARDIO  S.A.";
        $newProveedor->rut = "76307226-6";
        $newProveedor->email = "carlosorfali@gmail.com";
        $newProveedor->transferencia = 0;
        $newProveedor->activo = 1;
        $newProveedor->save();

        $newProveedor = new Proveedor();
        $newProveedor->nombre = "SERVICION DE CAPACITACION LIMITADA";
        $newProveedor->rut = "76427116-5";
        $newProveedor->email = "crecerotec@gmail.com";
        $newProveedor->transferencia = 0;
        $newProveedor->activo = 1;
        $newProveedor->save();

        $newProveedor = new Proveedor();
        $newProveedor->nombre = "TOP ROLLER SPA";
        $newProveedor->rut = "76297385-5";
        $newProveedor->transferencia = 0;
        $newProveedor->activo = 1;
        $newProveedor->save();

        $newProveedor = new Proveedor();
        $newProveedor->nombre = "BDT CAPACITACION LIMITADA";
        $newProveedor->rut = "76297358-8";
        $newProveedor->transferencia = 0;
        $newProveedor->activo = 1;
        $newProveedor->save();

        $newProveedor = new Proveedor();
        $newProveedor->nombre = "E-SIGN S.A.";
        $newProveedor->rut = "99551740-K";
        $newProveedor->direccion = "AV. APOQUINDO Nº6550 OF 501";
        $newProveedor->telefono = "22 4331500";
        $newProveedor->transferencia = 0;
        $newProveedor->activo = 1;
        $newProveedor->save();

        $newProveedor = new Proveedor();
        $newProveedor->nombre = "IMP Y DIST AUKINKO LIMITADA";
        $newProveedor->rut = "86442000-1";
        $newProveedor->transferencia = 0;
        $newProveedor->activo = 1;
        $newProveedor->save();

        $newProveedor = new Proveedor();
        $newProveedor->nombre = "BIOTECNOLOGIA E INNOVACION SPA";
        $newProveedor->rut = "76214197-3";
        $newProveedor->email = "Jvalenzuela@ebisa.cl";
        $newProveedor->transferencia = 0;
        $newProveedor->activo = 1;
        $newProveedor->save();

        $newProveedor = new Proveedor();
        $newProveedor->nombre = "CARLOS SILVIO LAFFERTE CESPEDES";
        $newProveedor->rut = "6384362-8";
        $newProveedor->transferencia = 0;
        $newProveedor->activo = 1;
        $newProveedor->save();

        $newProveedor = new Proveedor();
        $newProveedor->nombre = "COMERCILIZADORA DE ARTICULOS MEDICOS S.A.";
        $newProveedor->rut = "77990690-6";
        $newProveedor->transferencia = 0;
        $newProveedor->activo = 1;
        $newProveedor->save();

        $newProveedor = new Proveedor();
        $newProveedor->nombre = "ASTORGA AOCIADOS CONSULTORES LIMITADA";
        $newProveedor->rut = "76260840-5";
        $newProveedor->email = "contacto@nuevaperspectiva.cl";
        $newProveedor->transferencia = 0;
        $newProveedor->activo = 1;
        $newProveedor->save();

        $newProveedor = new Proveedor();
        $newProveedor->nombre = "COMPAÑIA DE PETROLEOS DE CHILE (COPEC) S.A.";
        $newProveedor->rut = "99520000-7";
        $newProveedor->transferencia = 0;
        $newProveedor->activo = 1;
        $newProveedor->save();

        $newProveedor = new Proveedor();
        $newProveedor->nombre = "A TORRES Y COMPAÑIA LIMITADA";
        $newProveedor->rut = "78968870-2";
        $newProveedor->email = "pmeneses@altofem.cl";
        $newProveedor->transferencia = 0;
        $newProveedor->activo = 1;
        $newProveedor->save();

        $newProveedor = new Proveedor();
        $newProveedor->nombre = "RD SOLUTIONS SPA";
        $newProveedor->rut = "76827302-2";
        $newProveedor->email = "rodrigo.delgado.r@outlook.cl";
        $newProveedor->transferencia = 0;
        $newProveedor->activo = 1;
        $newProveedor->save();

        $newProveedor = new Proveedor();
        $newProveedor->nombre = "INDURA S.A.";
        $newProveedor->rut = "76150343-K";
        $newProveedor->transferencia = 0;
        $newProveedor->activo = 1;
        $newProveedor->save();

        $newProveedor = new Proveedor();
        $newProveedor->nombre = "TESORERO MUNICIPAL DE ESTACION CENTRAL";
        $newProveedor->rut = "69254300-9";
        $newProveedor->transferencia = 0;
        $newProveedor->activo = 1;
        $newProveedor->save();

        $newProveedor = new Proveedor();
        $newProveedor->nombre = "JUAN ANDRES TEJEDA ARELLANO";
        $newProveedor->rut = "11948787-0";
        $newProveedor->email = "contacto@labdelivery.cl";
        $newProveedor->transferencia = 0;
        $newProveedor->activo = 1;
        $newProveedor->save();

        $newProveedor = new Proveedor();
        $newProveedor->nombre = "REPRESENTACIONES BIOTEC LIMITADA.";
        $newProveedor->rut = "76062131-5";
        $newProveedor->transferencia = 0;
        $newProveedor->activo = 1;
        $newProveedor->save();

        $newProveedor = new Proveedor();
        $newProveedor->nombre = "OPTISERV LIMITADA";
        $newProveedor->rut = "76014683-8";
        $newProveedor->email = "OPTISERV@TERRA.CL";
        $newProveedor->transferencia = 0;
        $newProveedor->activo = 1;
        $newProveedor->save();

        $newProveedor = new Proveedor();
        $newProveedor->nombre = "SOC COMERCIAL AMW S.A";
        $newProveedor->rut = "77714930-K";
        $newProveedor->transferencia = 0;
        $newProveedor->activo = 1;
        $newProveedor->save();

        $newProveedor = new Proveedor();
        $newProveedor->nombre = "CONSULTORIA Y CAPACITACION D C G LIMITADA";
        $newProveedor->rut = "78926980-7";
        $newProveedor->transferencia = 0;
        $newProveedor->activo = 1;
        $newProveedor->save();

        $newProveedor = new Proveedor();
        $newProveedor->nombre = "ORGANIZACION DE SERVICIOS M Y A LTDA";
        $newProveedor->rut = "78410860-0";
        $newProveedor->email = "marcelacanas@gmail.com";
        $newProveedor->transferencia = 0;
        $newProveedor->activo = 1;
        $newProveedor->save();

        $newProveedor = new Proveedor();
        $newProveedor->nombre = "COMERCIAL NEW LEADER S.P.A";
        $newProveedor->rut = "81238500-3";
        $newProveedor->transferencia = 0;
        $newProveedor->activo = 1;
        $newProveedor->save();

        $newProveedor = new Proveedor();
        $newProveedor->nombre = "INSTITUTO DE RADIOMEDICINA LTDA. (CLINICA IRAN)";
        $newProveedor->rut = "85493600-K";
        $newProveedor->transferencia = 0;
        $newProveedor->activo = 1;
        $newProveedor->save();

        $newProveedor = new Proveedor();
        $newProveedor->nombre = "DUARTE GUTIERREZ E HIJOS LIMITADA";
        $newProveedor->rut = "88496600-0";
        $newProveedor->transferencia = 0;
        $newProveedor->activo = 1;
        $newProveedor->save();

        $newProveedor = new Proveedor();
        $newProveedor->nombre = "SUSAN JEANNETTE DIAZ RAMIREZ";
        $newProveedor->rut = "7437410-7";
        $newProveedor->transferencia = 0;
        $newProveedor->activo = 1;
        $newProveedor->save();

        $newProveedor = new Proveedor();
        $newProveedor->nombre = "IMP.COMERCIALIZADORA BLUEMEDICAL LIMITADA";
        $newProveedor->rut = "76116604-2";
        $newProveedor->email = "info@bluemedical.cl";
        $newProveedor->transferencia = 0;
        $newProveedor->activo = 1;
        $newProveedor->save();

        $newProveedor = new Proveedor();
        $newProveedor->nombre = "CIENPORCIENTO CAPACITACION LIMITADA";
        $newProveedor->rut = "76046221-7";
        $newProveedor->email = "caravena@cienporciento.cl";
        $newProveedor->transferencia = 0;
        $newProveedor->activo = 1;
        $newProveedor->save();

        $newProveedor = new Proveedor();
        $newProveedor->nombre = "ALVARO JOSE DEL CAMPO SAEZ";
        $newProveedor->rut = "7191242-6";
        $newProveedor->transferencia = 0;
        $newProveedor->activo = 1;
        $newProveedor->save();

        $newProveedor = new Proveedor();
        $newProveedor->nombre = "SOC COM DEL CANTO LTDA.";
        $newProveedor->rut = "77141380-3";
        $newProveedor->email = "delcantomedical@gmail.com";
        $newProveedor->transferencia = 0;
        $newProveedor->activo = 1;
        $newProveedor->save();

        $newProveedor = new Proveedor();
        $newProveedor->nombre = "SERV. DE REFRIGERACION Y CLIMATIZACION ISABEL GARRIDO EIRL";
        $newProveedor->rut = "76146972-K";
        $newProveedor->transferencia = 0;
        $newProveedor->activo = 1;
        $newProveedor->save();

        $newProveedor = new Proveedor();
        $newProveedor->nombre = "SOC.LABORATORIO DEL DESARROLLO S.A.";
        $newProveedor->rut = "76044889-3";
        $newProveedor->email = "kespinoza@udd.cl";
        $newProveedor->transferencia = 0;
        $newProveedor->activo = 1;
        $newProveedor->save();

        $newProveedor = new Proveedor();
        $newProveedor->nombre = "SALGADO Y COMPAÑIA LIMITADA";
        $newProveedor->rut = "78833980-1";
        $newProveedor->transferencia = 0;
        $newProveedor->activo = 1;
        $newProveedor->save();

        $newProveedor = new Proveedor();
        $newProveedor->nombre = "LABORATORIOS AC FARMA SPA";
        $newProveedor->rut = "76121056-4";
        $newProveedor->email = "acfarmaspa@acfarma.com";
        $newProveedor->transferencia = 0;
        $newProveedor->activo = 1;
        $newProveedor->save();

        $newProveedor = new Proveedor();
        $newProveedor->nombre = "GEAMED S.A";
        $newProveedor->rut = "76727397-5";
        $newProveedor->email = "cristian.gonzalez@geamed.cl";
        $newProveedor->transferencia = 0;
        $newProveedor->activo = 1;
        $newProveedor->save();

        $newProveedor = new Proveedor();
        $newProveedor->nombre = "CARDIOTEC VASCULAR LIMITADA";
        $newProveedor->rut = "762641380";
        $newProveedor->transferencia = 0;
        $newProveedor->activo = 1;
        $newProveedor->save();

        $newProveedor = new Proveedor();
        $newProveedor->nombre = "CONSTRUCTORA PTS SPA";
        $newProveedor->rut = "76198064-5";
        $newProveedor->transferencia = 0;
        $newProveedor->activo = 1;
        $newProveedor->save();

        $newProveedor = new Proveedor();
        $newProveedor->nombre = "IMP.Y EXP.ZAVALA Y HERRERA LTDA";
        $newProveedor->rut = "79807250-1";
        $newProveedor->transferencia = 0;
        $newProveedor->activo = 1;
        $newProveedor->save();

        $newProveedor = new Proveedor();
        $newProveedor->nombre = "ORTHO CLINICAL DIAGNOSTICS CHILE SPA";
        $newProveedor->rut = "76553731-2";
        $newProveedor->email = "Pilar.salek@orthoclinicaldiagnostics.com";
        $newProveedor->transferencia = 0;
        $newProveedor->activo = 1;
        $newProveedor->save();

        $newProveedor = new Proveedor();
        $newProveedor->nombre = "ESCUELA NACIONAL DE ADMINISTRACION PUBLICA LIMITADA.";
        $newProveedor->rut = "77871950-9";
        $newProveedor->transferencia = 0;
        $newProveedor->activo = 1;
        $newProveedor->save();

        $newProveedor = new Proveedor();
        $newProveedor->nombre = "SERVICIOS COMERCIALES CHILE LTDA";
        $newProveedor->rut = "780592605";
        $newProveedor->transferencia = 0;
        $newProveedor->activo = 1;
        $newProveedor->save();

        $newProveedor = new Proveedor();
        $newProveedor->nombre = "GLORIA BEATRIZ SOTO MARTINEZ";
        $newProveedor->rut = "10248741-9";
        $newProveedor->transferencia = 0;
        $newProveedor->activo = 1;
        $newProveedor->save();

        $newProveedor = new Proveedor();
        $newProveedor->nombre = "LUXYPHARM SPA";
        $newProveedor->rut = "76105305-1";
        $newProveedor->email = "epinilla@luxypharm.cl";
        $newProveedor->transferencia = 0;
        $newProveedor->activo = 1;
        $newProveedor->save();

        $newProveedor = new Proveedor();
        $newProveedor->nombre = "DURAN SAN MARTIN COMPAÑIA LIMITADA";
        $newProveedor->rut = "77278950-5";
        $newProveedor->email = "rmontecinos@duransanmartin.cl";
        $newProveedor->transferencia = 0;
        $newProveedor->activo = 1;
        $newProveedor->save();

        $newProveedor = new Proveedor();
        $newProveedor->nombre = "ALGODONERA VARAS COMPAÑIA LTDA";
        $newProveedor->rut = "89516100-4";
        $newProveedor->transferencia = 0;
        $newProveedor->activo = 1;
        $newProveedor->save();

        $newProveedor = new Proveedor();
        $newProveedor->nombre = "SOC COMERCIALIZADORA Y DIST DE PRODUCTOS DE SALUD Y CIENCIA V";
        $newProveedor->rut = "76032872-3";
        $newProveedor->transferencia = 0;
        $newProveedor->activo = 1;
        $newProveedor->save();

        $newProveedor = new Proveedor();
        $newProveedor->nombre = "SOC DE INVERSIONES GRUPO SAN OSVALDO LIMITADA .";
        $newProveedor->rut = "76422496-5";
        $newProveedor->transferencia = 0;
        $newProveedor->activo = 1;
        $newProveedor->save();

        $newProveedor = new Proveedor();
        $newProveedor->nombre = "CARMEN CONCHA VALLE";
        $newProveedor->rut = "76492677-3";
        $newProveedor->email = "ccvmedica@yahoo.es";
        $newProveedor->transferencia = 0;
        $newProveedor->activo = 1;
        $newProveedor->save();

        $newProveedor = new Proveedor();
        $newProveedor->nombre = "RUDOLF CHILE S.A";
        $newProveedor->rut = "96924310-5";
        $newProveedor->transferencia = 0;
        $newProveedor->activo = 1;
        $newProveedor->save();

        $newProveedor = new Proveedor();
        $newProveedor->nombre = "SOCIEDAD COMERCIAL SERI LTDA";
        $newProveedor->rut = "76148628-4";
        $newProveedor->transferencia = 0;
        $newProveedor->activo = 1;
        $newProveedor->save();

        $newProveedor = new Proveedor();
        $newProveedor->nombre = "DHL EXPRESS CHILE LTDA";
        $newProveedor->rut = "86966100-7";
        $newProveedor->transferencia = 0;
        $newProveedor->activo = 1;
        $newProveedor->save();

        $newProveedor = new Proveedor();
        $newProveedor->nombre = "ANA MARIA ASPEE PLAZA";
        $newProveedor->rut = "10531322-5";
        $newProveedor->transferencia = 0;
        $newProveedor->activo = 1;
        $newProveedor->save();

        $newProveedor = new Proveedor();
        $newProveedor->nombre = "BIOTECH LIMITADA";
        $newProveedor->rut = "76006366-5";
        $newProveedor->email = "jcarcelen@biotechchile.cl";
        $newProveedor->transferencia = 0;
        $newProveedor->activo = 1;
        $newProveedor->save();

        $newProveedor = new Proveedor();
        $newProveedor->nombre = "SOC. COMER. MIHOVILOVIC HNOS Y OTRO LTDA";
        $newProveedor->rut = "77647010-4";
        $newProveedor->transferencia = 0;
        $newProveedor->activo = 1;
        $newProveedor->save();

        $newProveedor = new Proveedor();
        $newProveedor->nombre = "IMP. Y EXPORTADORA CORELLA LTDA";
        $newProveedor->rut = "77771940-8";
        $newProveedor->transferencia = 0;
        $newProveedor->activo = 1;
        $newProveedor->save();

        $newProveedor = new Proveedor();
        $newProveedor->nombre = "VISALMED VIDA Y SALUD LTDA";
        $newProveedor->rut = "76595497-5";
        $newProveedor->email = "ventasvisalmed@visalmed.cl";
        $newProveedor->transferencia = 0;
        $newProveedor->activo = 1;
        $newProveedor->save();

        $newProveedor = new Proveedor();
        $newProveedor->nombre = "CIDEF COMERCIAL S.A";
        $newProveedor->rut = "79780600-5";
        $newProveedor->transferencia = 0;
        $newProveedor->activo = 1;
        $newProveedor->save();

        $newProveedor = new Proveedor();
        $newProveedor->nombre = "INTERNATIONAL TELEMEDICAL SYSTEM CHILE SPA";
        $newProveedor->rut = "96995590-3";
        $newProveedor->transferencia = 0;
        $newProveedor->activo = 1;
        $newProveedor->save();

        $newProveedor = new Proveedor();
        $newProveedor->nombre = "SERVICIO DE SALUD INTEGRADO S.A";
        $newProveedor->rut = "96809780-6";
        $newProveedor->transferencia = 0;
        $newProveedor->activo = 1;
        $newProveedor->save();

        $newProveedor = new Proveedor();
        $newProveedor->nombre = "CICLO MED CHILE SPA";
        $newProveedor->rut = "76453086-1";
        $newProveedor->transferencia = 0;
        $newProveedor->activo = 1;
        $newProveedor->save();

        $newProveedor = new Proveedor();
        $newProveedor->nombre = "MEDILAND SPA";
        $newProveedor->rut = "99550230-5";
        $newProveedor->email = "patricia.canales@surmedical.cl";
        $newProveedor->transferencia = 0;
        $newProveedor->activo = 1;
        $newProveedor->save();

        $newProveedor = new Proveedor();
        $newProveedor->nombre = "COMERCIALIZADORA DIMED LTDA";
        $newProveedor->rut = "76514913-4";
        $newProveedor->email = "dimed.beltran@gmail.com";
        $newProveedor->transferencia = 0;
        $newProveedor->activo = 1;
        $newProveedor->save();

        $newProveedor = new Proveedor();
        $newProveedor->nombre = "PAPELERA DIMAR S.A";
        $newProveedor->rut = "93734000-1";
        $newProveedor->transferencia = 0;
        $newProveedor->activo = 1;
        $newProveedor->save();

        $newProveedor = new Proveedor();
        $newProveedor->nombre = "DICTUC S.A";
        $newProveedor->rut = "96691330-4";
        $newProveedor->transferencia = 0;
        $newProveedor->activo = 1;
        $newProveedor->save();

        $newProveedor = new Proveedor();
        $newProveedor->nombre = "UNIVERSIDAD DE LOS ANDES";
        $newProveedor->rut = "71614000-8";
        $newProveedor->email = "scorrea@clinicauandes.cl";
        $newProveedor->transferencia = 0;
        $newProveedor->activo = 1;
        $newProveedor->save();

        $newProveedor = new Proveedor();
        $newProveedor->nombre = "SOCIEDAD COMERCIAL FSN SPA";
        $newProveedor->rut = "76368859-3";
        $newProveedor->transferencia = 0;
        $newProveedor->activo = 1;
        $newProveedor->save();

        $newProveedor = new Proveedor();
        $newProveedor->nombre = "ALLERGAN LABORATORIOS LTDA";
        $newProveedor->rut = "78411950-5";
        $newProveedor->email = "carbone_mariella@allergan.com";
        $newProveedor->transferencia = 0;
        $newProveedor->activo = 1;
        $newProveedor->save();

        $newProveedor = new Proveedor();
        $newProveedor->nombre = "COMERCIAL TRALHUEN LTDA";
        $newProveedor->rut = "76158488-K";
        $newProveedor->transferencia = 0;
        $newProveedor->activo = 1;
        $newProveedor->save();

        $newProveedor = new Proveedor();
        $newProveedor->nombre = "PEDRO ANGEL DEL C. GUTIERREZ LAGOS";
        $newProveedor->rut = "7512495-3";
        $newProveedor->transferencia = 0;
        $newProveedor->activo = 1;
        $newProveedor->save();

        $newProveedor = new Proveedor();
        $newProveedor->nombre = "COMERCIALIZADORA DE CORTINAS Y PERSIANAS";
        $newProveedor->rut = "76511562-0";
        $newProveedor->transferencia = 0;
        $newProveedor->activo = 1;
        $newProveedor->save();

        $newProveedor = new Proveedor();
        $newProveedor->nombre = "CISCA CONSTRUCCION SERV. E INMOBILIARIA LTDA";
        $newProveedor->rut = "76434873-7";
        $newProveedor->transferencia = 0;
        $newProveedor->activo = 1;
        $newProveedor->save();

        $newProveedor = new Proveedor();
        $newProveedor->nombre = "DISTRIBUIDORA DE MATERIALES DE CONSTRUCCION";
        $newProveedor->rut = "96792430-K";
        $newProveedor->transferencia = 0;
        $newProveedor->activo = 1;
        $newProveedor->save();

        $newProveedor = new Proveedor();
        $newProveedor->nombre = "CLINICAL MARKET S.A";
        $newProveedor->rut = "76111113-2";
        $newProveedor->transferencia = 0;
        $newProveedor->activo = 1;
        $newProveedor->save();

        $newProveedor = new Proveedor();
        $newProveedor->nombre = "LEMARE CHILE S.A";
        $newProveedor->rut = "96772080-1";
        $newProveedor->transferencia = 0;
        $newProveedor->activo = 1;
        $newProveedor->save();

        $newProveedor = new Proveedor();
        $newProveedor->nombre = "INMOBILIARIA E INVERSIONES SURIA LTDA";
        $newProveedor->rut = "77827520-1";
        $newProveedor->email = "surialtda@yahoo.com";
        $newProveedor->transferencia = 0;
        $newProveedor->activo = 1;
        $newProveedor->save();

        $newProveedor = new Proveedor();
        $newProveedor->nombre = "JOSE WENCESLAO KLAIBER ESPERGUEL";
        $newProveedor->rut = "6811617-1";
        $newProveedor->transferencia = 0;
        $newProveedor->activo = 1;
        $newProveedor->save();

        $newProveedor = new Proveedor();
        $newProveedor->nombre = "QUALITEC FARMACEUTICA LTDA";
        $newProveedor->rut = "76389452-5";
        $newProveedor->email = "ventas@qualytecfarmaceutica.cl";
        $newProveedor->transferencia = 0;
        $newProveedor->activo = 1;
        $newProveedor->save();

        $newProveedor = new Proveedor();
        $newProveedor->nombre = "DISPOSITIVOS MEDICOS SPA";
        $newProveedor->rut = "76573521-1";
        $newProveedor->email = "mcarroz@dinmed.cl";
        $newProveedor->transferencia = 0;
        $newProveedor->activo = 1;
        $newProveedor->save();

        $newProveedor = new Proveedor();
        $newProveedor->nombre = "COMERCIAL SALAMANCA Y CIA LTDA";
        $newProveedor->rut = "79814830-3";
        $newProveedor->transferencia = 0;
        $newProveedor->activo = 1;
        $newProveedor->save();

        $newProveedor = new Proveedor();
        $newProveedor->nombre = "STRENUUS MEDICAL LTDA";
        $newProveedor->rut = "76307605-9";
        $newProveedor->email = "steve@strenuusmarketing.com";
        $newProveedor->transferencia = 0;
        $newProveedor->activo = 1;
        $newProveedor->save();

        $newProveedor = new Proveedor();
        $newProveedor->nombre = "TECHNO SYSTEMS CHILE LTDA";
        $newProveedor->rut = "96678350-8";
        $newProveedor->transferencia = 0;
        $newProveedor->activo = 1;
        $newProveedor->save();

        $newProveedor = new Proveedor();
        $newProveedor->nombre = "ANDESCO SPA";
        $newProveedor->rut = "76423377-8";
        $newProveedor->email = "eherrera@factoriza.cl";
        $newProveedor->transferencia = 0;
        $newProveedor->activo = 1;
        $newProveedor->save();

        $newProveedor = new Proveedor();
        $newProveedor->nombre = "INDUSTRIA METALURGICA URSUS TROTTER S.A";
        $newProveedor->rut = "92065000-7";
        $newProveedor->transferencia = 0;
        $newProveedor->activo = 1;
        $newProveedor->save();

        $newProveedor = new Proveedor();
        $newProveedor->nombre = "CSL BEHRING SPA";
        $newProveedor->rut = "76.432.529-K";
        $newProveedor->transferencia = 0;
        $newProveedor->activo = 0;
        $newProveedor->save();
        $newProveedor->delete();

        $newProveedor = new Proveedor();
        $newProveedor->nombre = "CSL BEHRING SPA";
        $newProveedor->rut = "76432529-K";
        $newProveedor->email = "juan.ambar@cslbehring.com";
        $newProveedor->transferencia = 0;
        $newProveedor->activo = 1;
        $newProveedor->save();

        $newProveedor = new Proveedor();
        $newProveedor->nombre = "INTEGRADORES DE TECNOLOGIA Y SISTEMAS S.A";
        $newProveedor->rut = "78159800-3";
        $newProveedor->email = "jennifer.mendezc@grupogtd.com";
        $newProveedor->transferencia = 0;
        $newProveedor->activo = 1;
        $newProveedor->save();

        $newProveedor = new Proveedor();
        $newProveedor->nombre = "IMPORTADORA, COMERCIALIZADORA Y DISTRIBUIDORA DE PRODUCTOS LTDA";
        $newProveedor->rut = "76371206-0";
        $newProveedor->email = "aperez@stkchile.cl";
        $newProveedor->transferencia = 0;
        $newProveedor->activo = 1;
        $newProveedor->save();

        $newProveedor = new Proveedor();
        $newProveedor->nombre = "IMPORTADORA DE EQUIPOS MEDICOS LTDA";
        $newProveedor->rut = "78615850-8";
        $newProveedor->transferencia = 0;
        $newProveedor->activo = 1;
        $newProveedor->save();

        $newProveedor = new Proveedor();
        $newProveedor->nombre = "EKIPOTEL CHILE S.A";
        $newProveedor->rut = "79844080-2";
        $newProveedor->transferencia = 0;
        $newProveedor->activo = 1;
        $newProveedor->save();

        $newProveedor = new Proveedor();
        $newProveedor->nombre = "LUIS FELIPE FANTINI GUERRERO SPA";
        $newProveedor->rut = "52000282-0";
        $newProveedor->transferencia = 0;
        $newProveedor->activo = 1;
        $newProveedor->save();

        $newProveedor = new Proveedor();
        $newProveedor->nombre = "BIOQUIMERA SPA";
        $newProveedor->rut = "76508818-6";
        $newProveedor->email = "vsantander@bioquimera.cl";
        $newProveedor->transferencia = 0;
        $newProveedor->activo = 1;
        $newProveedor->save();

        $newProveedor = new Proveedor();
        $newProveedor->nombre = "CHILEFRESH S.A";
        $newProveedor->rut = "99544900-5";
        $newProveedor->transferencia = 0;
        $newProveedor->activo = 1;
        $newProveedor->save();

        $newProveedor = new Proveedor();
        $newProveedor->nombre = "FERRETERIA AMUNATEGUI S.A";
        $newProveedor->rut = "95229000-2";
        $newProveedor->transferencia = 0;
        $newProveedor->activo = 1;
        $newProveedor->save();

        $newProveedor = new Proveedor();
        $newProveedor->nombre = "FARMACIAS CRUZ VERDE S.A";
        $newProveedor->rut = "89807200-2";
        $newProveedor->transferencia = 0;
        $newProveedor->activo = 1;
        $newProveedor->save();

        $newProveedor = new Proveedor();
        $newProveedor->nombre = "JORGE ESVEILE CAFATI Y CIA LTDA";
        $newProveedor->rut = "78801960-2";
        $newProveedor->email = "ranwax@ranwax.cl";
        $newProveedor->transferencia = 0;
        $newProveedor->activo = 1;
        $newProveedor->save();

        $newProveedor = new Proveedor();
        $newProveedor->nombre = "ECHODATA MEDICAL SYSTEMS CHILE S.P.A.";
        $newProveedor->rut = "76388854-1";
        $newProveedor->email = "franz.perez@echodatams.cl";
        $newProveedor->transferencia = 0;
        $newProveedor->activo = 1;
        $newProveedor->save();

        $newProveedor = new Proveedor();
        $newProveedor->nombre = "DANIELA VILLALOBOS CACERES";
        $newProveedor->rut = "15155505-5";
        $newProveedor->transferencia = 0;
        $newProveedor->activo = 1;
        $newProveedor->save();

        $newProveedor = new Proveedor();
        $newProveedor->nombre = "SERICK MAX MEDICA LTDA";
        $newProveedor->rut = "76292956-2";
        $newProveedor->transferencia = 0;
        $newProveedor->activo = 1;
        $newProveedor->save();

        $newProveedor = new Proveedor();
        $newProveedor->nombre = "MEDLA SPA";
        $newProveedor->rut = "76420916-8";
        $newProveedor->email = "mapadilla@medla.cl";
        $newProveedor->transferencia = 0;
        $newProveedor->activo = 1;
        $newProveedor->save();

        $newProveedor = new Proveedor();
        $newProveedor->nombre = "BANCO CONSORCIO";
        $newProveedor->rut = "99500410-0";
        $newProveedor->email = "kbravo@bancoconsorcio.cl";
        $newProveedor->contacto = "COBRANZAS@BANCOCONSORCIO.CL";
        $newProveedor->transferencia = 0;
        $newProveedor->activo = 1;
        $newProveedor->save();

        $newProveedor = new Proveedor();
        $newProveedor->nombre = "GO FACTORING S.A";
        $newProveedor->rut = "76111453-0";
        $newProveedor->email = "JSILVAC@GOFACTORING.CL";
        $newProveedor->contacto = "CONTABILIDAD@GOFACTORING.CL";
        $newProveedor->transferencia = 0;
        $newProveedor->activo = 1;
        $newProveedor->save();

        $newProveedor = new Proveedor();
        $newProveedor->nombre = "BCI FACTORING";
        $newProveedor->rut = "96720830-2";
        $newProveedor->email = "aecfactoring@bci.cl";
        $newProveedor->contacto = "PAGOSFACTORING@BCI.CL";
        $newProveedor->transferencia = 0;
        $newProveedor->activo = 1;
        $newProveedor->save();

        $newProveedor = new Proveedor();
        $newProveedor->nombre = "CBP FINANCIA CAPITAL FACTORING S.A.";
        $newProveedor->rut = "76197101-8";
        $newProveedor->email = "KSANDOVAL@FINANCIACAPITAL.CL";
        $newProveedor->contacto = "COBRANZA@FINANCIACAPITAL.CL";
        $newProveedor->transferencia = 0;
        $newProveedor->activo = 1;
        $newProveedor->save();

        $newProveedor = new Proveedor();
        $newProveedor->nombre = "CAPITAL EXPRESS SERVICIOS FINACIEROS S.A.";
        $newProveedor->rut = "76083507-2";
        $newProveedor->email = "cflores@capitalexpress.cl";
        $newProveedor->contacto = "RAYALA@CAPITALEXPRESS.CL";
        $newProveedor->transferencia = 0;
        $newProveedor->activo = 1;
        $newProveedor->save();

        $newProveedor = new Proveedor();
        $newProveedor->nombre = "TANNER";
        $newProveedor->rut = "96667560-8";
        $newProveedor->email = "venta.spf@tanner.cl";
        $newProveedor->transferencia = 0;
        $newProveedor->activo = 1;
        $newProveedor->save();

        $newProveedor = new Proveedor();
        $newProveedor->nombre = "COMREN S.A.";
        $newProveedor->rut = "76135267-9";
        $newProveedor->email = "GUILLERMO.TORRES@COMREN.CL";
        $newProveedor->transferencia = 0;
        $newProveedor->activo = 1;
        $newProveedor->save();

        $newProveedor = new Proveedor();
        $newProveedor->nombre = "TECNOLOGIAS FENWAY LTDA";
        $newProveedor->rut = "76049315-5";
        $newProveedor->transferencia = 0;
        $newProveedor->activo = 1;
        $newProveedor->save();

        $newProveedor = new Proveedor();
        $newProveedor->nombre = "ECODATA S.A";
        $newProveedor->rut = "78299070-5";
        $newProveedor->transferencia = 0;
        $newProveedor->activo = 1;
        $newProveedor->save();

        $newProveedor = new Proveedor();
        $newProveedor->nombre = "COMERCIALIZADORA LUIS ALEJANDRO LTDA";
        $newProveedor->rut = "76442274-0";
        $newProveedor->transferencia = 0;
        $newProveedor->activo = 1;
        $newProveedor->save();

        $newProveedor = new Proveedor();
        $newProveedor->nombre = "CENTRO TECHSALUD LTDA";
        $newProveedor->rut = "76399485-6";
        $newProveedor->transferencia = 0;
        $newProveedor->activo = 1;
        $newProveedor->save();

        $newProveedor = new Proveedor();
        $newProveedor->nombre = "GONZALO RUBIO STUARDO";
        $newProveedor->rut = "8128777-5";
        $newProveedor->transferencia = 0;
        $newProveedor->activo = 1;
        $newProveedor->save();

        $newProveedor = new Proveedor();
        $newProveedor->nombre = "INGENIERIA Y EQUIPOS PARA TRATAMIENTO DE AGUAS SPA";
        $newProveedor->rut = "76269230-9";
        $newProveedor->email = "jcbeckel@streamwater.cl";
        $newProveedor->transferencia = 0;
        $newProveedor->activo = 1;
        $newProveedor->save();

        $newProveedor = new Proveedor();
        $newProveedor->nombre = "DIAGNOSTICO POR IMAGENES";
        $newProveedor->rut = "78849790-3";
        $newProveedor->transferencia = 0;
        $newProveedor->activo = 1;
        $newProveedor->save();

        $newProveedor = new Proveedor();
        $newProveedor->nombre = "INSUMOS Y SERVICIOS MEDICOS S.A.";
        $newProveedor->rut = "77497400-8";
        $newProveedor->email = "facturacion@ismsa.cl";
        $newProveedor->transferencia = 0;
        $newProveedor->activo = 1;
        $newProveedor->save();

        $newProveedor = new Proveedor();
        $newProveedor->nombre = "LIA RAQUEL GRANT CORTES";
        $newProveedor->rut = "8112942-8";
        $newProveedor->transferencia = 0;
        $newProveedor->activo = 1;
        $newProveedor->save();

        $newProveedor = new Proveedor();
        $newProveedor->nombre = "JUAN PABLO MATUS CORNEJO";
        $newProveedor->rut = "14316586-8";
        $newProveedor->transferencia = 0;
        $newProveedor->activo = 1;
        $newProveedor->save();

        $newProveedor = new Proveedor();
        $newProveedor->nombre = "SOCIEDAD DE NUTRICIONISTAS NUTRIHOUSE LTDA";
        $newProveedor->rut = "76095459-4";
        $newProveedor->email = "nutrihousemk@gmail.com";
        $newProveedor->transferencia = 0;
        $newProveedor->activo = 1;
        $newProveedor->save();

        $newProveedor = new Proveedor();
        $newProveedor->nombre = "COMERCIALIZADORA DE MUEBLES HP LTDA";
        $newProveedor->rut = "76058118-6";
        $newProveedor->transferencia = 0;
        $newProveedor->activo = 1;
        $newProveedor->save();

        $newProveedor = new Proveedor();
        $newProveedor->nombre = "COMERCIAL BETA SPA";
        $newProveedor->rut = "76764133-8";
        $newProveedor->email = "ecorvillon@cbeta.cl";
        $newProveedor->transferencia = 0;
        $newProveedor->activo = 1;
        $newProveedor->save();

        $newProveedor = new Proveedor();
        $newProveedor->nombre = "LABORATORIO LAFI LTDA";
        $newProveedor->rut = "94398000-4";
        $newProveedor->email = "maria.montenegro@abbott.com";
        $newProveedor->transferencia = 0;
        $newProveedor->activo = 1;
        $newProveedor->save();

        $newProveedor = new Proveedor();
        $newProveedor->nombre = "ARSYSTEM SPA";
        $newProveedor->rut = "76344609-3";
        $newProveedor->email = "natalia.rojas@arsystem.cl";
        $newProveedor->transferencia = 0;
        $newProveedor->activo = 1;
        $newProveedor->save();

        $newProveedor = new Proveedor();
        $newProveedor->nombre = "COMERCIAL HAGELIN LTDA";
        $newProveedor->rut = "76102918-5";
        $newProveedor->transferencia = 0;
        $newProveedor->activo = 1;
        $newProveedor->save();

        $newProveedor = new Proveedor();
        $newProveedor->nombre = "COMERCIALIZADORA E IMPORTADORA REDVITAL SPA";
        $newProveedor->rut = "76183929-2";
        $newProveedor->email = "Humanachile@gmail.com";
        $newProveedor->transferencia = 0;
        $newProveedor->activo = 1;
        $newProveedor->save();

        $newProveedor = new Proveedor();
        $newProveedor->nombre = "FINAMERIS SERVICIOS FINANCIEROS";
        $newProveedor->rut = "76621380-4";
        $newProveedor->email = "ASAENZ@FINAMERIS.CL";
        $newProveedor->contacto = "225808200";
        $newProveedor->transferencia = 0;
        $newProveedor->activo = 1;
        $newProveedor->save();

        $newProveedor = new Proveedor();
        $newProveedor->nombre = "BANCO CONSORCIO";
        $newProveedor->rut = "99.500.410-0";
        $newProveedor->contacto = "COBRANZAS@BANCOCONSORCIO.CL";
        $newProveedor->transferencia = 0;
        $newProveedor->activo = 0;
        $newProveedor->save();
        $newProveedor->delete();

        $newProveedor = new Proveedor();
        $newProveedor->nombre = "BANCO DE CHILE";
        $newProveedor->rut = "97.004.000-5";
        $newProveedor->contacto = "COBRANZAFACTORING@BANCOCHILE.CL";
        $newProveedor->transferencia = 0;
        $newProveedor->activo = 0;
        $newProveedor->save();
        $newProveedor->delete();

        $newProveedor = new Proveedor();
        $newProveedor->nombre = "INCOFIN S.A";
        $newProveedor->rut = "96.626.570-1";
        $newProveedor->contacto = "COBRANZAS@INCOFIN.CL";
        $newProveedor->transferencia = 0;
        $newProveedor->activo = 0;
        $newProveedor->save();
        $newProveedor->delete();

        $newProveedor = new Proveedor();
        $newProveedor->nombre = "ZETACE CAPACITACION LTDA";
        $newProveedor->rut = "77889160-3";
        $newProveedor->transferencia = 0;
        $newProveedor->activo = 1;
        $newProveedor->save();

        $newProveedor = new Proveedor();
        $newProveedor->nombre = "BANCO DE CHILE";
        $newProveedor->rut = "97004000-5";
        $newProveedor->email = "jcanetec@bancochile.cl";
        $newProveedor->contacto = "COBRANZAFACTORING@BANCOCHILE.CL";
        $newProveedor->transferencia = 0;
        $newProveedor->activo = 1;
        $newProveedor->save();

        $newProveedor = new Proveedor();
        $newProveedor->nombre = "MUNDOTEC LTDA";
        $newProveedor->rut = "76475540-5";
        $newProveedor->transferencia = 0;
        $newProveedor->activo = 1;
        $newProveedor->save();

        $newProveedor = new Proveedor();
        $newProveedor->nombre = "BEMIS MANUFACTURING COMPANY CHILE LTDA";
        $newProveedor->rut = "77188130-0";
        $newProveedor->email = "jose.barrientos@bemismfg.com";
        $newProveedor->transferencia = 0;
        $newProveedor->activo = 1;
        $newProveedor->save();

        $newProveedor = new Proveedor();
        $newProveedor->nombre = "COMERCIAL KANTENAH LIMITADA";
        $newProveedor->rut = "76426917-9";
        $newProveedor->transferencia = 0;
        $newProveedor->activo = 1;
        $newProveedor->save();

        $newProveedor = new Proveedor();
        $newProveedor->nombre = "1 ACTIVA PUBLICIDAD SPA";
        $newProveedor->rut = "76428299-K";
        $newProveedor->transferencia = 0;
        $newProveedor->activo = 1;
        $newProveedor->save();

        $newProveedor = new Proveedor();
        $newProveedor->nombre = "OPCIONES SISTEMAS DE INFORMACION";
        $newProveedor->rut = "96523180-3";
        $newProveedor->email = "vanessa.marin@opciones.cl";
        $newProveedor->transferencia = 0;
        $newProveedor->activo = 1;
        $newProveedor->save();

        $newProveedor = new Proveedor();
        $newProveedor->nombre = "OMEGA MEDICA CHILE LTDA";
        $newProveedor->rut = "76165999-5";
        $newProveedor->email = "ventas@omegamedica.cl";
        $newProveedor->transferencia = 0;
        $newProveedor->activo = 1;
        $newProveedor->save();

        $newProveedor = new Proveedor();
        $newProveedor->nombre = "STIFTUNG FUR PATHOBIOCHEMIE UND MOLEKULARE DIAGNOSTIK";
        $newProveedor->rut = "20182018-9";
        $newProveedor->transferencia = 0;
        $newProveedor->activo = 1;
        $newProveedor->save();

        $newProveedor = new Proveedor();
        $newProveedor->nombre = "CARLOS GUILLERMO DEL VILLAR DE LA JARA";
        $newProveedor->rut = "8027552-8";
        $newProveedor->transferencia = 0;
        $newProveedor->activo = 1;
        $newProveedor->save();

        $newProveedor = new Proveedor();
        $newProveedor->nombre = "SOC DE MANUFACTURA DE EQUIPOS DENTALES LTDA.";
        $newProveedor->rut = "85025400-1";
        $newProveedor->direccion = "EXEQUIEL FERNANDEZ 2841";
        $newProveedor->telefono = "27958900";
        $newProveedor->email = "jessica.sanchez@mdent.cl";
        $newProveedor->contacto = "JESSICA SANCHEZ";
        $newProveedor->transferencia = 0;
        $newProveedor->activo = 1;
        $newProveedor->save();

        $newProveedor = new Proveedor();
        $newProveedor->nombre = "COMERCIALIZADORA E IMPORTADORA GRUPO RAVC SPA";
        $newProveedor->rut = "76364678-5";
        $newProveedor->direccion = "AV. VICUÑA MACKENNA PONIENTE 6843 OF 602";
        $newProveedor->telefono = "228403862";
        $newProveedor->email = "RODRIGO.VIDAL@GRUPORAVC.CL";
        $newProveedor->transferencia = 0;
        $newProveedor->activo = 1;
        $newProveedor->save();

        $newProveedor = new Proveedor();
        $newProveedor->nombre = "SOCIEDAD COMERCIAL ISPLUS LTDA";
        $newProveedor->rut = "76075901-5";
        $newProveedor->email = "g.dubost@lechner.cl";
        $newProveedor->transferencia = 0;
        $newProveedor->activo = 1;
        $newProveedor->save();

        $newProveedor = new Proveedor();
        $newProveedor->nombre = "BANCO SANTANDER CHILE";
        $newProveedor->rut = "76.050.680-K";
        $newProveedor->transferencia = 0;
        $newProveedor->activo = 0;
        $newProveedor->save();
        $newProveedor->delete();

        $newProveedor = new Proveedor();
        $newProveedor->nombre = "BANCO SANTANDER CHILE";
        $newProveedor->rut = "97.036.000-K";
        $newProveedor->transferencia = 0;
        $newProveedor->activo = 0;
        $newProveedor->save();
        $newProveedor->delete();

        $newProveedor = new Proveedor();
        $newProveedor->nombre = "BANPRO FACTORING S.A";
        $newProveedor->rut = "76.163.106-3";
        $newProveedor->contacto = "CBOWN@BANPRO.CL";
        $newProveedor->transferencia = 0;
        $newProveedor->activo = 0;
        $newProveedor->save();
        $newProveedor->delete();

        $newProveedor = new Proveedor();
        $newProveedor->nombre = "COMERCIAL BARRIENTOS LTDA";
        $newProveedor->rut = "76097178-2";
        $newProveedor->email = "contacto@combar.cl";
        $newProveedor->transferencia = 0;
        $newProveedor->activo = 1;
        $newProveedor->save();

        $newProveedor = new Proveedor();
        $newProveedor->nombre = "ATEM INTEGRACION TECNOLOGICA LTDA";
        $newProveedor->rut = "76086318-1";
        $newProveedor->email = "contacto@atem.cl";
        $newProveedor->transferencia = 0;
        $newProveedor->activo = 1;
        $newProveedor->save();

        $newProveedor = new Proveedor();
        $newProveedor->nombre = "COMUNIDAD HOSPITAL DEL PROFESOR";
        $newProveedor->rut = "53125850-9";
        $newProveedor->transferencia = 0;
        $newProveedor->activo = 1;
        $newProveedor->save();

        $newProveedor = new Proveedor();
        $newProveedor->nombre = "PRODUCTORA DE TARJETAS DE IDENTIFICACIÓN E-MACH CARD LTDA";
        $newProveedor->rut = "77500240-9";
        $newProveedor->email = "pstengel@emach.cl";
        $newProveedor->transferencia = 0;
        $newProveedor->activo = 1;
        $newProveedor->save();

        $newProveedor = new Proveedor();
        $newProveedor->nombre = "GALEMICUM HEALTH CHILE SPA";
        $newProveedor->rut = "76285229-2";
        $newProveedor->email = "fac@galenicum.com";
        $newProveedor->transferencia = 0;
        $newProveedor->activo = 1;
        $newProveedor->save();

        $newProveedor = new Proveedor();
        $newProveedor->nombre = "FACTORING SII";
        $newProveedor->rut = "12345678-5";
        $newProveedor->transferencia = 0;
        $newProveedor->activo = 1;
        $newProveedor->save();

        $newProveedor = new Proveedor();
        $newProveedor->nombre = "SOCIEDAD COMERCIAL Y DE INVERSIONES CORP PREMIER LTDA";
        $newProveedor->rut = "76473108-5";
        $newProveedor->transferencia = 0;
        $newProveedor->activo = 1;
        $newProveedor->save();

        $newProveedor = new Proveedor();
        $newProveedor->nombre = "INVERSIONES TI SPA";
        $newProveedor->rut = "76441153-6";
        $newProveedor->email = "contacto@2050.cl";
        $newProveedor->transferencia = 0;
        $newProveedor->activo = 1;
        $newProveedor->save();

        $newProveedor = new Proveedor();
        $newProveedor->nombre = "JUAN CARLOS PIÑEIRO SALINAS";
        $newProveedor->rut = "15708531-K";
        $newProveedor->transferencia = 0;
        $newProveedor->activo = 1;
        $newProveedor->save();

        $newProveedor = new Proveedor();
        $newProveedor->nombre = "WEI CHILE S.A";
        $newProveedor->rut = "96775870-1";
        $newProveedor->transferencia = 0;
        $newProveedor->activo = 1;
        $newProveedor->save();

        $newProveedor = new Proveedor();
        $newProveedor->nombre = "SOCIEDAD DE INVERSIONES AMC SPA";
        $newProveedor->rut = "76767731-6";
        $newProveedor->email = "AMELLA@FOOTGLOBAL.CL";
        $newProveedor->transferencia = 0;
        $newProveedor->activo = 1;
        $newProveedor->save();

        $newProveedor = new Proveedor();
        $newProveedor->nombre = "PLUSMEDICAL SPA";
        $newProveedor->rut = "76520087-3";
        $newProveedor->transferencia = 0;
        $newProveedor->activo = 1;
        $newProveedor->save();

        $newProveedor = new Proveedor();
        $newProveedor->nombre = "AM-INVERSIONES S.A";
        $newProveedor->rut = "76026331-1";
        $newProveedor->email = "mviguera@am-inversiones.cl";
        $newProveedor->transferencia = 0;
        $newProveedor->activo = 1;
        $newProveedor->save();

        $newProveedor = new Proveedor();
        $newProveedor->nombre = "ANDES IMPORTADORA Y EXPORTADORA LTDA";
        $newProveedor->rut = "77304460-0";
        $newProveedor->transferencia = 0;
        $newProveedor->activo = 1;
        $newProveedor->save();

        $newProveedor = new Proveedor();
        $newProveedor->nombre = "SIETE CUMBRE FACTORIGN SPA";
        $newProveedor->rut = "76693083-2";
        $newProveedor->email = "FELIPE.PADRUNO@GMAIL.COM";
        $newProveedor->transferencia = 0;
        $newProveedor->activo = 1;
        $newProveedor->save();

        $newProveedor = new Proveedor();
        $newProveedor->nombre = "BANCO SANTANDER CHILE";
        $newProveedor->rut = "97036000-K";
        $newProveedor->email = "mmontero@santander.cl";
        $newProveedor->transferencia = 0;
        $newProveedor->activo = 1;
        $newProveedor->save();

        $newProveedor = new Proveedor();
        $newProveedor->nombre = "FACTOTAL SA";
        $newProveedor->rut = "96660790-4";
        $newProveedor->email = "mmaturana@ft.cl";
        $newProveedor->transferencia = 0;
        $newProveedor->activo = 1;
        $newProveedor->save();

        $newProveedor = new Proveedor();
        $newProveedor->nombre = "BANFACTORING S.A.";
        $newProveedor->rut = "76299831-9";
        $newProveedor->email = "mpinto@banfactoring.cl";
        $newProveedor->transferencia = 0;
        $newProveedor->activo = 1;
        $newProveedor->save();

        $newProveedor = new Proveedor();
        $newProveedor->nombre = "FACTORING SECURITY S A";
        $newProveedor->rut = "96655860-1";
        $newProveedor->email = "paulina.barros@security.cl";
        $newProveedor->transferencia = 0;
        $newProveedor->activo = 1;
        $newProveedor->save();

        $newProveedor = new Proveedor();
        $newProveedor->nombre = "BANPRO FACTORING S.A";
        $newProveedor->rut = "76163106-3";
        $newProveedor->email = "cesion@banpro.cl;CBOWN@BANPRO.CL";
        $newProveedor->transferencia = 0;
        $newProveedor->activo = 1;
        $newProveedor->save();

        $newProveedor = new Proveedor();
        $newProveedor->nombre = "PRIMUS CAPITAL SPA";
        $newProveedor->rut = "76360977-4";
        $newProveedor->email = "cesiones@primuscapital.cl";
        $newProveedor->transferencia = 0;
        $newProveedor->activo = 1;
        $newProveedor->save();

        $newProveedor = new Proveedor();
        $newProveedor->nombre = "EUROCAPITAL S.A.";
        $newProveedor->rut = "96861280-8";
        $newProveedor->email = "rptec@eurocapital.cl";
        $newProveedor->transferencia = 0;
        $newProveedor->activo = 1;
        $newProveedor->save();

        $newProveedor = new Proveedor();
        $newProveedor->nombre = "EUROAMERICA SERVICIOS FINANCIEROS";
        $newProveedor->rut = "76532190-5";
        $newProveedor->email = "Matias.ramirez@euroamerica.cl";
        $newProveedor->transferencia = 0;
        $newProveedor->activo = 1;
        $newProveedor->save();

        $newProveedor = new Proveedor();
        $newProveedor->nombre = "INCOFIN S A";
        $newProveedor->rut = "96626570-1";
        $newProveedor->email = "Sebastian.aguilera@incofin.cl;COBRANZAS@INCOFIN.CL";
        $newProveedor->transferencia = 0;
        $newProveedor->activo = 1;
        $newProveedor->save();

        $newProveedor = new Proveedor();
        $newProveedor->nombre = "PYMER FACTOR S.A.";
        $newProveedor->rut = "76562839-3";
        $newProveedor->email = "yuruena@pymerfactor.cl";
        $newProveedor->transferencia = 0;
        $newProveedor->activo = 1;
        $newProveedor->save();

        $newProveedor = new Proveedor();
        $newProveedor->nombre = "ORSAN FACTORING SA";
        $newProveedor->rut = "96949020-K";
        $newProveedor->email = "maria.catalan@orsan.cl";
        $newProveedor->transferencia = 0;
        $newProveedor->activo = 1;
        $newProveedor->save();

        $newProveedor = new Proveedor();
        $newProveedor->nombre = "BICE FACTORING S.A.";
        $newProveedor->rut = "76562786-9";
        $newProveedor->email = "juan.riedel@bice.cl";
        $newProveedor->transferencia = 0;
        $newProveedor->activo = 1;
        $newProveedor->save();

        $newProveedor = new Proveedor();
        $newProveedor->nombre = "CONCRECES FACTORING S.A.";
        $newProveedor->rut = "76043103-6";
        $newProveedor->email = "jbriverosm@concreces.cl";
        $newProveedor->transferencia = 0;
        $newProveedor->activo = 1;
        $newProveedor->save();

        $newProveedor = new Proveedor();
        $newProveedor->nombre = "BANCO INTERNACIONAL";
        $newProveedor->rut = "97011000-3";
        $newProveedor->email = "bancointernacional@facturanet.cl";
        $newProveedor->transferencia = 0;
        $newProveedor->activo = 1;
        $newProveedor->save();

        $newProveedor = new Proveedor();
        $newProveedor->nombre = "SERVICIOS FINANCIEROS SUMAR SPA";
        $newProveedor->rut = "76472151-9";
        $newProveedor->email = "DGALICIA@FINANSU.CL";
        $newProveedor->transferencia = 0;
        $newProveedor->activo = 1;
        $newProveedor->save();

        $newProveedor = new Proveedor();
        $newProveedor->nombre = "FACTORING BANINTER S.A.";
        $newProveedor->rut = "76072472-6";
        $newProveedor->email = "folivares@baninterfactoring.cl";
        $newProveedor->transferencia = 0;
        $newProveedor->activo = 1;
        $newProveedor->save();

        $newProveedor = new Proveedor();
        $newProveedor->nombre = "LOGROS FACTORING SPA";
        $newProveedor->rut = "96657890-4";
        $newProveedor->email = "alejandra.zuniga@logros.cl";
        $newProveedor->transferencia = 0;
        $newProveedor->activo = 1;
        $newProveedor->save();

        $newProveedor = new Proveedor();
        $newProveedor->nombre = "INTERFACTOR S.A";
        $newProveedor->rut = "76381570-6";
        $newProveedor->email = "nmadariaga@interfactor.cl";
        $newProveedor->transferencia = 0;
        $newProveedor->activo = 1;
        $newProveedor->save();

        $newProveedor = new Proveedor();
        $newProveedor->nombre = "ACF CAPITAL SA";
        $newProveedor->rut = "99580240-6";
        $newProveedor->email = "factura@acfcapital.cl";
        $newProveedor->transferencia = 0;
        $newProveedor->activo = 1;
        $newProveedor->save();

        $newProveedor = new Proveedor();
        $newProveedor->nombre = "ITAU CORPBANCA";
        $newProveedor->rut = "97023000-9";
        $newProveedor->email = "paula.mateo@itau.cl";
        $newProveedor->transferencia = 0;
        $newProveedor->activo = 1;
        $newProveedor->save();

        $newProveedor = new Proveedor();
        $newProveedor->nombre = "FACTOCHILE S.A.";
        $newProveedor->rut = "76457580-6";
        $newProveedor->email = "cmarin@factochile.cl";
        $newProveedor->transferencia = 0;
        $newProveedor->activo = 1;
        $newProveedor->save();

        $newProveedor = new Proveedor();
        $newProveedor->nombre = "ADDWISE SERVICIOS FINANCIEROS SA";
        $newProveedor->rut = "76434365-4";
        $newProveedor->email = "ALEXIS.ALLENDE@ADDFACTORING.CL";
        $newProveedor->transferencia = 0;
        $newProveedor->activo = 1;
        $newProveedor->save();

        $newProveedor = new Proveedor();
        $newProveedor->nombre = "MACAPITAL S.A.";
        $newProveedor->rut = "76800593-1";
        $newProveedor->email = "efinol@masaval.cl";
        $newProveedor->transferencia = 0;
        $newProveedor->activo = 1;
        $newProveedor->save();

        $newProveedor = new Proveedor();
        $newProveedor->nombre = "COVAL FACTORING SA";
        $newProveedor->rut = "77356020-K";
        $newProveedor->email = "kretamales@coval.cl";
        $newProveedor->transferencia = 0;
        $newProveedor->activo = 1;
        $newProveedor->save();

        $newProveedor = new Proveedor();
        $newProveedor->nombre = "TRANSPORTES DE PASAJEROS Y TURISMO JUAN CORDERO MINO EMPRESA INDIVIDUA";
        $newProveedor->rut = "76099981-4";
        $newProveedor->email = "JCORDERO@TRANSCORDERO.CL";
        $newProveedor->transferencia = 0;
        $newProveedor->activo = 1;
        $newProveedor->save();

        $newProveedor = new Proveedor();
        $newProveedor->nombre = "CONTEMPORA FACTORING S.A.";
        $newProveedor->rut = "99562370-6";
        $newProveedor->email = "scl_op@contempora.com";
        $newProveedor->transferencia = 0;
        $newProveedor->activo = 1;
        $newProveedor->save();

        $newProveedor = new Proveedor();
        $newProveedor->nombre = "CHITA SPA";
        $newProveedor->rut = "76596744-9";
        $newProveedor->email = "comercial@chita.cl";
        $newProveedor->transferencia = 0;
        $newProveedor->activo = 1;
        $newProveedor->save();

        $newProveedor = new Proveedor();
        $newProveedor->nombre = "SOLVENTA FACTORING LIMITADA";
        $newProveedor->rut = "76145987-2";
        $newProveedor->email = "luis.aguayo@centralsae.cl";
        $newProveedor->transferencia = 0;
        $newProveedor->activo = 1;
        $newProveedor->save();

        $newProveedor = new Proveedor();
        $newProveedor->nombre = "CRECER FACTORING S.A.";
        $newProveedor->rut = "96710640-2";
        $newProveedor->email = "afavre@crecer.cl";
        $newProveedor->transferencia = 0;
        $newProveedor->activo = 1;
        $newProveedor->save();

        $newProveedor = new Proveedor();
        $newProveedor->nombre = "FINTERRA SERVICIOS FINANCIEROS S.A.";
        $newProveedor->rut = "76311552-6";
        $newProveedor->email = "CESION@FINTERRA.CL";
        $newProveedor->transferencia = 0;
        $newProveedor->activo = 1;
        $newProveedor->save();

        $newProveedor = new Proveedor();
        $newProveedor->nombre = "ST CAPITAL S.A.";
        $newProveedor->rut = "76389992-6";
        $newProveedor->email = "fynpal-app-notif-st-capital@fynpal.com";
        $newProveedor->transferencia = 0;
        $newProveedor->activo = 1;
        $newProveedor->save();

        $newProveedor = new Proveedor();
        $newProveedor->nombre = "PHILIPS CHILENA S.A";
        $newProveedor->rut = "90761000-4";
        $newProveedor->email = "licitaciones.chile@philips.com";
        $newProveedor->transferencia = 0;
        $newProveedor->activo = 1;
        $newProveedor->save();

        $newProveedor = new Proveedor();
        $newProveedor->nombre = "SOCIEDAD AGUAS FOSAS LTDA";
        $newProveedor->rut = "76488317-9";
        $newProveedor->transferencia = 0;
        $newProveedor->activo = 1;
        $newProveedor->save();

        $newProveedor = new Proveedor();
        $newProveedor->nombre = "INDURA S.A";
        $newProveedor->rut = "91335000-6";
        $newProveedor->transferencia = 0;
        $newProveedor->activo = 1;
        $newProveedor->save();

        $newProveedor = new Proveedor();
        $newProveedor->nombre = "ARIEL ANDRES RIQUELME RETAMALES";
        $newProveedor->rut = "18126466-7";
        $newProveedor->transferencia = 0;
        $newProveedor->activo = 1;
        $newProveedor->save();

        $newProveedor = new Proveedor();
        $newProveedor->nombre = "XIMENA CECILIA SOLAR SIGALA";
        $newProveedor->rut = "15638404-6";
        $newProveedor->transferencia = 0;
        $newProveedor->activo = 1;
        $newProveedor->save();

        $newProveedor = new Proveedor();
        $newProveedor->nombre = "RITA DEL CARMEN SANHUEZA VICENTELO";
        $newProveedor->rut = "16763536-9";
        $newProveedor->transferencia = 0;
        $newProveedor->activo = 1;
        $newProveedor->save();

        $newProveedor = new Proveedor();
        $newProveedor->nombre = "XIMENA ALEJANDRA LASCANI MONSALVE";
        $newProveedor->rut = "18085809-1";
        $newProveedor->transferencia = 0;
        $newProveedor->activo = 1;
        $newProveedor->save();

        $newProveedor = new Proveedor();
        $newProveedor->nombre = "FELIPE JAVIER ARANEDA ALCAINO";
        $newProveedor->rut = "15145087-3";
        $newProveedor->transferencia = 0;
        $newProveedor->activo = 1;
        $newProveedor->save();

        $newProveedor = new Proveedor();
        $newProveedor->nombre = "PILAR ANDREA ACUNA AGUAYO";
        $newProveedor->rut = "15316482-7";
        $newProveedor->transferencia = 0;
        $newProveedor->activo = 1;
        $newProveedor->save();

        $newProveedor = new Proveedor();
        $newProveedor->nombre = "MARIA ELISA TAPIA CESPEDES";
        $newProveedor->rut = "15331201-K";
        $newProveedor->transferencia = 0;
        $newProveedor->activo = 1;
        $newProveedor->save();

        $newProveedor = new Proveedor();
        $newProveedor->nombre = "RUSSETH ANN MEZA MONTECINOS";
        $newProveedor->rut = "15420547-0";
        $newProveedor->transferencia = 0;
        $newProveedor->activo = 1;
        $newProveedor->save();

        $newProveedor = new Proveedor();
        $newProveedor->nombre = "ROBERTO ALFREDO ROJAS OYARZO";
        $newProveedor->rut = "12357080-4";
        $newProveedor->transferencia = 0;
        $newProveedor->activo = 1;
        $newProveedor->save();

        $newProveedor = new Proveedor();
        $newProveedor->nombre = "SANDRA LORENA GONZALEZ BOBADILLA";
        $newProveedor->rut = "16304100-6";
        $newProveedor->transferencia = 0;
        $newProveedor->activo = 1;
        $newProveedor->save();

        $newProveedor = new Proveedor();
        $newProveedor->nombre = "LISSETTE FRANCISCA DEL CARMEN GRAMUSSET HEPP";
        $newProveedor->rut = "16354399-0";
        $newProveedor->transferencia = 0;
        $newProveedor->activo = 1;
        $newProveedor->save();

        $newProveedor = new Proveedor();
        $newProveedor->nombre = "JUAN ALEJANDRO CONTRERAS LEVICOY";
        $newProveedor->rut = "13850134-5";
        $newProveedor->transferencia = 0;
        $newProveedor->activo = 1;
        $newProveedor->save();

        $newProveedor = new Proveedor();
        $newProveedor->nombre = "CONSTANZA ISABEL MICOLICH VERGARA";
        $newProveedor->rut = "16239918-7";
        $newProveedor->transferencia = 0;
        $newProveedor->activo = 1;
        $newProveedor->save();

        $newProveedor = new Proveedor();
        $newProveedor->nombre = "CHRISTIAN ANDRES BACKHOUSE QUINTANA";
        $newProveedor->rut = "16010203-9";
        $newProveedor->transferencia = 0;
        $newProveedor->activo = 1;
        $newProveedor->save();

        $newProveedor = new Proveedor();
        $newProveedor->nombre = "FABIAN HUMBERTO TORRES ARRIAGADA";
        $newProveedor->rut = "18357303-9";
        $newProveedor->transferencia = 0;
        $newProveedor->activo = 1;
        $newProveedor->save();

        $newProveedor = new Proveedor();
        $newProveedor->nombre = "FRANCISCA ANDREA SWENSON ITURRA";
        $newProveedor->rut = "18005989-K";
        $newProveedor->transferencia = 0;
        $newProveedor->activo = 1;
        $newProveedor->save();

        $newProveedor = new Proveedor();
        $newProveedor->nombre = "VALENTINA JAVIERA ECHEVERRIA ABARCA";
        $newProveedor->rut = "17189026-8";
        $newProveedor->transferencia = 0;
        $newProveedor->activo = 1;
        $newProveedor->save();

        $newProveedor = new Proveedor();
        $newProveedor->nombre = "RICARDO FRANCISCO GARCES LAGOS";
        $newProveedor->rut = "9379553-9";
        $newProveedor->transferencia = 0;
        $newProveedor->activo = 1;
        $newProveedor->save();

        $newProveedor = new Proveedor();
        $newProveedor->nombre = "MARCELO ANDREAS CANALES NAVARRETE";
        $newProveedor->rut = "10771740-4";
        $newProveedor->transferencia = 0;
        $newProveedor->activo = 1;
        $newProveedor->save();

        $newProveedor = new Proveedor();
        $newProveedor->nombre = "MAURICIO ANDRES QUEZADA GALLEGOS";
        $newProveedor->rut = "10339912-2";
        $newProveedor->transferencia = 0;
        $newProveedor->activo = 1;
        $newProveedor->save();

        $newProveedor = new Proveedor();
        $newProveedor->nombre = "ALEJANDRO LUIS CEBALLOS CARRASCO";
        $newProveedor->rut = "15193267-3";
        $newProveedor->transferencia = 0;
        $newProveedor->activo = 1;
        $newProveedor->save();

        $newProveedor = new Proveedor();
        $newProveedor->nombre = "CAMILA MIRANDA RUIZ";
        $newProveedor->rut = "18731038-5";
        $newProveedor->transferencia = 0;
        $newProveedor->activo = 1;
        $newProveedor->save();

        $newProveedor = new Proveedor();
        $newProveedor->nombre = "JUAN SEBASTIAN INOSTROZA CHUAQUI";
        $newProveedor->rut = "17678480-6";
        $newProveedor->transferencia = 0;
        $newProveedor->activo = 1;
        $newProveedor->save();

        $newProveedor = new Proveedor();
        $newProveedor->nombre = "CATALINA MARINA ARAYA MORALES";
        $newProveedor->rut = "19022520-8";
        $newProveedor->transferencia = 0;
        $newProveedor->activo = 1;
        $newProveedor->save();

        $newProveedor = new Proveedor();
        $newProveedor->nombre = "KAREN NEVENKA LOPEZ BERNET";
        $newProveedor->rut = "18314180-5";
        $newProveedor->transferencia = 0;
        $newProveedor->activo = 1;
        $newProveedor->save();

        $newProveedor = new Proveedor();
        $newProveedor->nombre = "FERNANDA AHUMADA KONG";
        $newProveedor->rut = "13893131-5";
        $newProveedor->transferencia = 0;
        $newProveedor->activo = 1;
        $newProveedor->save();

        $newProveedor = new Proveedor();
        $newProveedor->nombre = "CESAR ANTONIO VARGAS AYANCAN";
        $newProveedor->rut = "13971161-0";
        $newProveedor->transferencia = 0;
        $newProveedor->activo = 1;
        $newProveedor->save();

        $newProveedor = new Proveedor();
        $newProveedor->nombre = "CAMILA BELEN VENEGAS VERGARA";
        $newProveedor->rut = "16480524-7";
        $newProveedor->transferencia = 0;
        $newProveedor->activo = 1;
        $newProveedor->save();

        $newProveedor = new Proveedor();
        $newProveedor->nombre = "ALEJANDRO ANDRES ABARZUA LOYOLA";
        $newProveedor->rut = "16348131-6";
        $newProveedor->transferencia = 0;
        $newProveedor->activo = 1;
        $newProveedor->save();

        $newProveedor = new Proveedor();
        $newProveedor->nombre = "SERGIO ANTONIO CARRASCO ARAYA";
        $newProveedor->rut = "13190377-4";
        $newProveedor->transferencia = 0;
        $newProveedor->activo = 1;
        $newProveedor->save();

        $newProveedor = new Proveedor();
        $newProveedor->nombre = "BIOMEDIKA SPA";
        $newProveedor->rut = "76732365-4";
        $newProveedor->email = "pcorbalan@biomedika.cl";
        $newProveedor->transferencia = 0;
        $newProveedor->activo = 1;
        $newProveedor->save();

        $newProveedor = new Proveedor();
        $newProveedor->nombre = "ASPER CAPACITACION S.A";
        $newProveedor->rut = "79840540-3";
        $newProveedor->transferencia = 0;
        $newProveedor->activo = 1;
        $newProveedor->save();

        $newProveedor = new Proveedor();
        $newProveedor->nombre = "CARRASCO E HIJOS LTDA";
        $newProveedor->rut = "76293470-1";
        $newProveedor->transferencia = 0;
        $newProveedor->activo = 1;
        $newProveedor->save();

        $newProveedor = new Proveedor();
        $newProveedor->nombre = "ANNY MALAVER CARRASCO";
        $newProveedor->rut = "25957763-2";
        $newProveedor->transferencia = 0;
        $newProveedor->activo = 1;
        $newProveedor->save();

        $newProveedor = new Proveedor();
        $newProveedor->nombre = "MIGUEL GRAU CADALSO";
        $newProveedor->rut = "25680058-6";
        $newProveedor->transferencia = 0;
        $newProveedor->activo = 1;
        $newProveedor->save();

        $newProveedor = new Proveedor();
        $newProveedor->nombre = "JAVIER BAQUEDANO LOBOS";
        $newProveedor->rut = "17176198-0";
        $newProveedor->transferencia = 0;
        $newProveedor->activo = 1;
        $newProveedor->save();

        $newProveedor = new Proveedor();
        $newProveedor->nombre = "VICTOR PEÑALOZA CASTILLO";
        $newProveedor->rut = "25831444-1";
        $newProveedor->transferencia = 0;
        $newProveedor->activo = 1;
        $newProveedor->save();

        $newProveedor = new Proveedor();
        $newProveedor->nombre = "FRANCISCA FIGUEROA LIRA";
        $newProveedor->rut = "17167707-6";
        $newProveedor->transferencia = 0;
        $newProveedor->activo = 1;
        $newProveedor->save();

        $newProveedor = new Proveedor();
        $newProveedor->nombre = "SOCIEDAD DE INVERSIONES ANTAR LTDA";
        $newProveedor->rut = "77807670-5";
        $newProveedor->transferencia = 0;
        $newProveedor->activo = 1;
        $newProveedor->save();

        $newProveedor = new Proveedor();
        $newProveedor->nombre = "LUIS BRAVO ALMARZA";
        $newProveedor->rut = "17728660-5";
        $newProveedor->transferencia = 0;
        $newProveedor->activo = 1;
        $newProveedor->save();

        $newProveedor = new Proveedor();
        $newProveedor->nombre = "DANIEL RODRIGUEZ ANDRADE";
        $newProveedor->rut = "17927766-2";
        $newProveedor->transferencia = 0;
        $newProveedor->activo = 1;
        $newProveedor->save();

        $newProveedor = new Proveedor();
        $newProveedor->nombre = "MARISOL ARRIAZA FIGUEROA";
        $newProveedor->rut = "19229831-8";
        $newProveedor->transferencia = 0;
        $newProveedor->activo = 1;
        $newProveedor->save();

        $newProveedor = new Proveedor();
        $newProveedor->nombre = "STEPHANNIE FIGUEROA ZAVALA";
        $newProveedor->rut = "17565627-8";
        $newProveedor->transferencia = 0;
        $newProveedor->activo = 1;
        $newProveedor->save();

        $newProveedor = new Proveedor();
        $newProveedor->nombre = "CARLOS GARCIA BUENO";
        $newProveedor->rut = "17698709-K";
        $newProveedor->transferencia = 0;
        $newProveedor->activo = 1;
        $newProveedor->save();

        $newProveedor = new Proveedor();
        $newProveedor->nombre = "ANDRES ACOSTA HURTUBIA";
        $newProveedor->rut = "14090249-7";
        $newProveedor->transferencia = 0;
        $newProveedor->activo = 1;
        $newProveedor->save();

        $newProveedor = new Proveedor();
        $newProveedor->nombre = "PAULINA LECAROS CASAS-CORDERO";
        $newProveedor->rut = "16427559-0";
        $newProveedor->transferencia = 0;
        $newProveedor->activo = 1;
        $newProveedor->save();

        $newProveedor = new Proveedor();
        $newProveedor->nombre = "BLANCA ORTIZ GUZMAN";
        $newProveedor->rut = "17962579-2";
        $newProveedor->transferencia = 0;
        $newProveedor->activo = 1;
        $newProveedor->save();

        $newProveedor = new Proveedor();
        $newProveedor->nombre = "CONSUELO DIAZ GALLARDO";
        $newProveedor->rut = "16041920-2";
        $newProveedor->transferencia = 0;
        $newProveedor->activo = 1;
        $newProveedor->save();

        $newProveedor = new Proveedor();
        $newProveedor->nombre = "AUGUSTO BELLET PACHECO";
        $newProveedor->rut = "5010521-0";
        $newProveedor->transferencia = 0;
        $newProveedor->activo = 1;
        $newProveedor->save();

        $newProveedor = new Proveedor();
        $newProveedor->nombre = "JUAN MEDINA MORALES";
        $newProveedor->rut = "17564241-2";
        $newProveedor->transferencia = 0;
        $newProveedor->activo = 1;
        $newProveedor->save();

        $newProveedor = new Proveedor();
        $newProveedor->nombre = "DANIELA JARA RIQUELME";
        $newProveedor->rut = "17896453-4";
        $newProveedor->transferencia = 0;
        $newProveedor->activo = 1;
        $newProveedor->save();

        $newProveedor = new Proveedor();
        $newProveedor->nombre = "LUIS PASTEN CARRASCO";
        $newProveedor->rut = "16037993-6";
        $newProveedor->transferencia = 0;
        $newProveedor->activo = 1;
        $newProveedor->save();

        $newProveedor = new Proveedor();
        $newProveedor->nombre = "DANIELA RODRIGUEZ RAMIREZ";
        $newProveedor->rut = "25113166-K";
        $newProveedor->transferencia = 0;
        $newProveedor->activo = 1;
        $newProveedor->save();

        $newProveedor = new Proveedor();
        $newProveedor->nombre = "KARLA HERNANDEZ GONZALEZ";
        $newProveedor->rut = "25007996-6";
        $newProveedor->transferencia = 0;
        $newProveedor->activo = 1;
        $newProveedor->save();

        $newProveedor = new Proveedor();
        $newProveedor->nombre = "JUAN GAVILANES ESPINAR";
        $newProveedor->rut = "25420393-9";
        $newProveedor->transferencia = 0;
        $newProveedor->activo = 1;
        $newProveedor->save();

        $newProveedor = new Proveedor();
        $newProveedor->nombre = "HERNAN RODRIGUEZ HERRERA";
        $newProveedor->rut = "24297743-2";
        $newProveedor->transferencia = 0;
        $newProveedor->activo = 1;
        $newProveedor->save();

        $newProveedor = new Proveedor();
        $newProveedor->nombre = "CAROLINA ESPINOZA ASTUDILLO";
        $newProveedor->rut = "25238776-5";
        $newProveedor->transferencia = 0;
        $newProveedor->activo = 1;
        $newProveedor->save();

        $newProveedor = new Proveedor();
        $newProveedor->nombre = "SOLANGE VALENZUELA VALENZUELA";
        $newProveedor->rut = "13550409-2";
        $newProveedor->transferencia = 0;
        $newProveedor->activo = 1;
        $newProveedor->save();

        $newProveedor = new Proveedor();
        $newProveedor->nombre = "NICOLAS ALEXIS FUENZALIDA BAHAMONDES";
        $newProveedor->rut = "17315908-0";
        $newProveedor->transferencia = 0;
        $newProveedor->activo = 1;
        $newProveedor->save();

        $newProveedor = new Proveedor();
        $newProveedor->nombre = "COMERCIALIZADORA RODRIGO RUIZ";
        $newProveedor->rut = "7034236-7";
        $newProveedor->email = "rodrigojorge@vtr.net";
        $newProveedor->transferencia = 0;
        $newProveedor->activo = 1;
        $newProveedor->save();

        $newProveedor = new Proveedor();
        $newProveedor->nombre = "GLADYS NORMA GAUBERT BORQUEZ";
        $newProveedor->rut = "12116429-9";
        $newProveedor->email = "mauriciom@plasgubert.cl";
        $newProveedor->transferencia = 0;
        $newProveedor->activo = 1;
        $newProveedor->save();

        $newProveedor = new Proveedor();
        $newProveedor->nombre = "PHARMA NETWORK SPA";
        $newProveedor->rut = "76857605-K";
        $newProveedor->transferencia = 0;
        $newProveedor->activo = 1;
        $newProveedor->save();

        $newProveedor = new Proveedor();
        $newProveedor->nombre = "NET SECURE INFORMATICA S.A.";
        $newProveedor->rut = "78933990-2";
        $newProveedor->email = "ecarrasco@netsecure.cl";
        $newProveedor->transferencia = 0;
        $newProveedor->activo = 1;
        $newProveedor->save();

        $newProveedor = new Proveedor();
        $newProveedor->nombre = "SANTIAGO ARTURO VEGA CANDIA";
        $newProveedor->rut = "17085936-7";
        $newProveedor->transferencia = 0;
        $newProveedor->activo = 1;
        $newProveedor->save();

        $newProveedor = new Proveedor();
        $newProveedor->nombre = "DOS PUNTOS EVENTOS LIMITADA";
        $newProveedor->rut = "76379913-1";
        $newProveedor->transferencia = 0;
        $newProveedor->activo = 1;
        $newProveedor->save();

        $newProveedor = new Proveedor();
        $newProveedor->nombre = "INNOVATIVE MEDICINES S.A.";
        $newProveedor->rut = "59144890-0";
        $newProveedor->transferencia = 0;
        $newProveedor->activo = 1;
        $newProveedor->save();

        $newProveedor = new Proveedor();
        $newProveedor->nombre = "MUEBLES ORBI SPA";
        $newProveedor->rut = "76428946-3";
        $newProveedor->email = "contacto.orbi@gmail.com";
        $newProveedor->transferencia = 0;
        $newProveedor->activo = 1;
        $newProveedor->save();

        $newProveedor = new Proveedor();
        $newProveedor->nombre = "CARDIOTEC VASCULAR LTDA";
        $newProveedor->rut = "76264138-0";
        $newProveedor->transferencia = 0;
        $newProveedor->activo = 1;
        $newProveedor->save();

        $newProveedor = new Proveedor();
        $newProveedor->nombre = "NUEVO CAPITAL S.A";
        $newProveedor->rut = "76261789-7";
        $newProveedor->email = "vnavarrete@nuevocapital.cl";
        $newProveedor->transferencia = 0;
        $newProveedor->activo = 1;
        $newProveedor->save();

        $newProveedor = new Proveedor();
        $newProveedor->nombre = "CAMILA ROSAS ORREGO";
        $newProveedor->rut = "17087698-9";
        $newProveedor->transferencia = 0;
        $newProveedor->activo = 1;
        $newProveedor->save();

        $newProveedor = new Proveedor();
        $newProveedor->nombre = "MARCELINO GONZALEZ TOLOZA";
        $newProveedor->rut = "25380780-6";
        $newProveedor->transferencia = 0;
        $newProveedor->activo = 1;
        $newProveedor->save();

        $newProveedor = new Proveedor();
        $newProveedor->nombre = "LUIS PINEDA CALVACHE";
        $newProveedor->rut = "25275219-6";
        $newProveedor->transferencia = 0;
        $newProveedor->activo = 1;
        $newProveedor->save();

        $newProveedor = new Proveedor();
        $newProveedor->nombre = "PATRICIO INDO BUSTAMANTE";
        $newProveedor->rut = "16390266-4";
        $newProveedor->transferencia = 0;
        $newProveedor->activo = 1;
        $newProveedor->save();

        $newProveedor = new Proveedor();
        $newProveedor->nombre = "LUIS BARBERAN GARCIA";
        $newProveedor->rut = "23187897-1";
        $newProveedor->transferencia = 0;
        $newProveedor->activo = 1;
        $newProveedor->save();

        $newProveedor = new Proveedor();
        $newProveedor->nombre = "LISBETH GOMEZ DEPABLOS";
        $newProveedor->rut = "25452601-0";
        $newProveedor->transferencia = 0;
        $newProveedor->activo = 1;
        $newProveedor->save();

        $newProveedor = new Proveedor();
        $newProveedor->nombre = "VALENTINA VALENZUELA QUIROZ";
        $newProveedor->rut = "17523894-8";
        $newProveedor->transferencia = 0;
        $newProveedor->activo = 1;
        $newProveedor->save();

        $newProveedor = new Proveedor();
        $newProveedor->nombre = "ALEJANDRA PEREZ ALVARADO";
        $newProveedor->rut = "18394957-8";
        $newProveedor->transferencia = 0;
        $newProveedor->activo = 1;
        $newProveedor->save();

        $newProveedor = new Proveedor();
        $newProveedor->nombre = "ARANSSA DUMAS ALUCEMA";
        $newProveedor->rut = "18063894-6";
        $newProveedor->transferencia = 0;
        $newProveedor->activo = 1;
        $newProveedor->save();

        $newProveedor = new Proveedor();
        $newProveedor->nombre = "JOSE CASTRO ROJAS";
        $newProveedor->rut = "18262768-2";
        $newProveedor->transferencia = 0;
        $newProveedor->activo = 1;
        $newProveedor->save();

        $newProveedor = new Proveedor();
        $newProveedor->nombre = "MARIA CARRERA GUERRERO";
        $newProveedor->rut = "17024941-0";
        $newProveedor->transferencia = 0;
        $newProveedor->activo = 1;
        $newProveedor->save();

        $newProveedor = new Proveedor();
        $newProveedor->nombre = "CONSTANZA NAVARRETE BARRIL";
        $newProveedor->rut = "18060260-7";
        $newProveedor->transferencia = 0;
        $newProveedor->activo = 1;
        $newProveedor->save();

        $newProveedor = new Proveedor();
        $newProveedor->nombre = "CLAUDIO SILVA REYES";
        $newProveedor->rut = "17000447-7";
        $newProveedor->transferencia = 0;
        $newProveedor->activo = 1;
        $newProveedor->save();

        $newProveedor = new Proveedor();
        $newProveedor->nombre = "TAMARA LEAL RAVANAL";
        $newProveedor->rut = "18624360-9";
        $newProveedor->transferencia = 0;
        $newProveedor->activo = 1;
        $newProveedor->save();

        $newProveedor = new Proveedor();
        $newProveedor->nombre = "PAULA VALENZUELA JERIA";
        $newProveedor->rut = "17860039-7";
        $newProveedor->transferencia = 0;
        $newProveedor->activo = 1;
        $newProveedor->save();

        $newProveedor = new Proveedor();
        $newProveedor->nombre = "VALENTINA VALERO MOYANO";
        $newProveedor->rut = "18113622-7";
        $newProveedor->transferencia = 0;
        $newProveedor->activo = 1;
        $newProveedor->save();

        $newProveedor = new Proveedor();
        $newProveedor->nombre = "ITALO LIZANA RINCON";
        $newProveedor->rut = "18326953-4";
        $newProveedor->transferencia = 0;
        $newProveedor->activo = 1;
        $newProveedor->save();

        $newProveedor = new Proveedor();
        $newProveedor->nombre = "KARINA DOCMAC SILVA";
        $newProveedor->rut = "17442763-1";
        $newProveedor->transferencia = 0;
        $newProveedor->activo = 1;
        $newProveedor->save();

        $newProveedor = new Proveedor();
        $newProveedor->nombre = "KAREN VILLARROEL IASALVATORE";
        $newProveedor->rut = "17919778-2";
        $newProveedor->transferencia = 0;
        $newProveedor->activo = 1;
        $newProveedor->save();

        $newProveedor = new Proveedor();
        $newProveedor->nombre = "GUISELLE BARRA AGUIRRE";
        $newProveedor->rut = "16740885-0";
        $newProveedor->transferencia = 0;
        $newProveedor->activo = 1;
        $newProveedor->save();

        $newProveedor = new Proveedor();
        $newProveedor->nombre = "RICARDO SIMONET PENA";
        $newProveedor->rut = "9580764-K";
        $newProveedor->transferencia = 0;
        $newProveedor->activo = 1;
        $newProveedor->save();

        $newProveedor = new Proveedor();
        $newProveedor->nombre = "ROBERTO HORMAZABAL ALCAYAGA";
        $newProveedor->rut = "7982473-9";
        $newProveedor->transferencia = 0;
        $newProveedor->activo = 1;
        $newProveedor->save();

        $newProveedor = new Proveedor();
        $newProveedor->nombre = "VIVIANA ORTEGA BALBI";
        $newProveedor->rut = "16290997-5";
        $newProveedor->transferencia = 0;
        $newProveedor->activo = 1;
        $newProveedor->save();

        $newProveedor = new Proveedor();
        $newProveedor->nombre = "MAYRA ANDREA PRETER GALARCE";
        $newProveedor->rut = "20332838-9";
        $newProveedor->transferencia = 0;
        $newProveedor->activo = 1;
        $newProveedor->save();

        $newProveedor = new Proveedor();
        $newProveedor->nombre = "KARLA POBLETE RIQUELME";
        $newProveedor->rut = "16648775-7";
        $newProveedor->transferencia = 0;
        $newProveedor->activo = 1;
        $newProveedor->save();

        $newProveedor = new Proveedor();
        $newProveedor->nombre = "CAMILA VITAGLIANO MOLINA";
        $newProveedor->rut = "18637045-7";
        $newProveedor->transferencia = 0;
        $newProveedor->activo = 1;
        $newProveedor->save();

        $newProveedor = new Proveedor();
        $newProveedor->nombre = "ANGELO VALLEJOS SANCHEZ";
        $newProveedor->rut = "16885379-3";
        $newProveedor->transferencia = 0;
        $newProveedor->activo = 1;
        $newProveedor->save();

        $newProveedor = new Proveedor();
        $newProveedor->nombre = "FELIPE ESPINOZA OYARCE";
        $newProveedor->rut = "16310976-K";
        $newProveedor->transferencia = 0;
        $newProveedor->activo = 1;
        $newProveedor->save();

        $newProveedor = new Proveedor();
        $newProveedor->nombre = "CRISTIAN ROA CACERES";
        $newProveedor->rut = "16387278-1";
        $newProveedor->transferencia = 0;
        $newProveedor->activo = 1;
        $newProveedor->save();

        $newProveedor = new Proveedor();
        $newProveedor->nombre = "TAMARA SANTIBAÑEZ MUÑOZ";
        $newProveedor->rut = "16285723-1";
        $newProveedor->transferencia = 0;
        $newProveedor->activo = 1;
        $newProveedor->save();

        $newProveedor = new Proveedor();
        $newProveedor->nombre = "JOHANNA INARRA MOREIRA";
        $newProveedor->rut = "24149851-4";
        $newProveedor->transferencia = 0;
        $newProveedor->activo = 1;
        $newProveedor->save();

        $newProveedor = new Proveedor();
        $newProveedor->nombre = "OLGA CASANOVA GONOGORA";
        $newProveedor->rut = "24842955-0";
        $newProveedor->transferencia = 0;
        $newProveedor->activo = 1;
        $newProveedor->save();

        $newProveedor = new Proveedor();
        $newProveedor->nombre = "ELENA LAGOS BAEZ";
        $newProveedor->rut = "15799393-3";
        $newProveedor->transferencia = 0;
        $newProveedor->activo = 1;
        $newProveedor->save();

        $newProveedor = new Proveedor();
        $newProveedor->nombre = "IZASKUN VERGARA ROJAS";
        $newProveedor->rut = "17401905-3";
        $newProveedor->transferencia = 0;
        $newProveedor->activo = 1;
        $newProveedor->save();

        $newProveedor = new Proveedor();
        $newProveedor->nombre = "RONALD FUENTES POVEDA";
        $newProveedor->rut = "14380630-8";
        $newProveedor->transferencia = 0;
        $newProveedor->activo = 1;
        $newProveedor->save();

        $newProveedor = new Proveedor();
        $newProveedor->nombre = "MARCO AYALA VILLALOBOS";
        $newProveedor->rut = "18300649-5";
        $newProveedor->transferencia = 0;
        $newProveedor->activo = 1;
        $newProveedor->save();

        $newProveedor = new Proveedor();
        $newProveedor->nombre = "MARIA SEEMANN ALFARO";
        $newProveedor->rut = "16937793-6";
        $newProveedor->transferencia = 0;
        $newProveedor->activo = 1;
        $newProveedor->save();

        $newProveedor = new Proveedor();
        $newProveedor->nombre = "JULIO SARMIENTO MACHADO";
        $newProveedor->rut = "21306081-3";
        $newProveedor->transferencia = 0;
        $newProveedor->activo = 1;
        $newProveedor->save();

        $newProveedor = new Proveedor();
        $newProveedor->nombre = "ESTEBAN GONZALEZ VILLALOBOS";
        $newProveedor->rut = "18316257-8";
        $newProveedor->transferencia = 0;
        $newProveedor->activo = 1;
        $newProveedor->save();

        $newProveedor = new Proveedor();
        $newProveedor->nombre = "URSULA HERNANDEZ VILLARROEL";
        $newProveedor->rut = "13181313-9";
        $newProveedor->transferencia = 0;
        $newProveedor->activo = 1;
        $newProveedor->save();

        $newProveedor = new Proveedor();
        $newProveedor->nombre = "RODRIGO VEGA DIAZ";
        $newProveedor->rut = "16419397-7";
        $newProveedor->transferencia = 0;
        $newProveedor->activo = 1;
        $newProveedor->save();

        $newProveedor = new Proveedor();
        $newProveedor->nombre = "FRANCISCO MARINGER CASAS";
        $newProveedor->rut = "18170155-2";
        $newProveedor->transferencia = 0;
        $newProveedor->activo = 1;
        $newProveedor->save();

        $newProveedor = new Proveedor();
        $newProveedor->nombre = "JHON HERNANDEZ SERNA";
        $newProveedor->rut = "25333110-0";
        $newProveedor->transferencia = 0;
        $newProveedor->activo = 1;
        $newProveedor->save();

        $newProveedor = new Proveedor();
        $newProveedor->nombre = "CLAUDIA LEIVA GREZ";
        $newProveedor->rut = "17311358-7";
        $newProveedor->transferencia = 0;
        $newProveedor->activo = 1;
        $newProveedor->save();

        $newProveedor = new Proveedor();
        $newProveedor->nombre = "RODRIGO RIFFO RUBIO";
        $newProveedor->rut = "10859498-5";
        $newProveedor->transferencia = 0;
        $newProveedor->activo = 1;
        $newProveedor->save();

        $newProveedor = new Proveedor();
        $newProveedor->nombre = "MIRNA AVILES GALAZ";
        $newProveedor->rut = "13909965-6";
        $newProveedor->transferencia = 0;
        $newProveedor->activo = 1;
        $newProveedor->save();

        $newProveedor = new Proveedor();
        $newProveedor->nombre = "FELIPE OSSES LEYTON";
        $newProveedor->rut = "17811642-8";
        $newProveedor->transferencia = 0;
        $newProveedor->activo = 1;
        $newProveedor->save();

        $newProveedor = new Proveedor();
        $newProveedor->nombre = "PAOLO REYES BRAVO";
        $newProveedor->rut = "18115104-8";
        $newProveedor->transferencia = 0;
        $newProveedor->activo = 1;
        $newProveedor->save();

        $newProveedor = new Proveedor();
        $newProveedor->nombre = "VALENTINA ROMAN MATAMALA";
        $newProveedor->rut = "17920947-0";
        $newProveedor->transferencia = 0;
        $newProveedor->activo = 1;
        $newProveedor->save();

        $newProveedor = new Proveedor();
        $newProveedor->nombre = "CATALINA MARTINEZ SEPULVEDA";
        $newProveedor->rut = "18730108-4";
        $newProveedor->transferencia = 0;
        $newProveedor->activo = 1;
        $newProveedor->save();

        $newProveedor = new Proveedor();
        $newProveedor->nombre = "CAMILA NEGRETE LABARCA";
        $newProveedor->rut = "16915567-4";
        $newProveedor->transferencia = 0;
        $newProveedor->activo = 1;
        $newProveedor->save();

        $newProveedor = new Proveedor();
        $newProveedor->nombre = "MONSERRAT TOLEDO PAVEZ";
        $newProveedor->rut = "18929432-8";
        $newProveedor->transferencia = 0;
        $newProveedor->activo = 1;
        $newProveedor->save();

        $newProveedor = new Proveedor();
        $newProveedor->nombre = "HADER SOTO MONTES";
        $newProveedor->rut = "25126654-9";
        $newProveedor->transferencia = 0;
        $newProveedor->activo = 1;
        $newProveedor->save();

        $newProveedor = new Proveedor();
        $newProveedor->nombre = "VALERIA TOBAR CARVAJAL";
        $newProveedor->rut = "18461466-9";
        $newProveedor->transferencia = 0;
        $newProveedor->activo = 1;
        $newProveedor->save();

        $newProveedor = new Proveedor();
        $newProveedor->nombre = "ROBERTO FERNANDEZ GARCIA";
        $newProveedor->rut = "18356019-0";
        $newProveedor->transferencia = 0;
        $newProveedor->activo = 1;
        $newProveedor->save();

        $newProveedor = new Proveedor();
        $newProveedor->nombre = "MARIA ZEPEDA HENRIQUEZ";
        $newProveedor->rut = "17705023-7";
        $newProveedor->transferencia = 0;
        $newProveedor->activo = 1;
        $newProveedor->save();

        $newProveedor = new Proveedor();
        $newProveedor->nombre = "ISIDORA OTAROLA ESCOBAR";
        $newProveedor->rut = "17527898-2";
        $newProveedor->transferencia = 0;
        $newProveedor->activo = 1;
        $newProveedor->save();

        $newProveedor = new Proveedor();
        $newProveedor->nombre = "JUAN WERLINGER VASQUEZ";
        $newProveedor->rut = "16525351-5";
        $newProveedor->transferencia = 0;
        $newProveedor->activo = 1;
        $newProveedor->save();

        $newProveedor = new Proveedor();
        $newProveedor->nombre = "FABIO ARMANDO PAOLETTO";
        $newProveedor->rut = "14657115-8";
        $newProveedor->transferencia = 0;
        $newProveedor->activo = 1;
        $newProveedor->save();

        $newProveedor = new Proveedor();
        $newProveedor->nombre = "JESSICA MUÑOZ CORTES";
        $newProveedor->rut = "11650195-3";
        $newProveedor->transferencia = 0;
        $newProveedor->activo = 1;
        $newProveedor->save();

        $newProveedor = new Proveedor();
        $newProveedor->nombre = "MANUFACTURAS RAC LTDA";
        $newProveedor->rut = "77676860-K";
        $newProveedor->email = "fmiranda@racchile.cl";
        $newProveedor->transferencia = 0;
        $newProveedor->activo = 1;
        $newProveedor->save();

        $newProveedor = new Proveedor();
        $newProveedor->nombre = "MANDOMEDIO.COM S.A";
        $newProveedor->rut = "77600820-6";
        $newProveedor->transferencia = 0;
        $newProveedor->activo = 1;
        $newProveedor->save();

        $newProveedor = new Proveedor();
        $newProveedor->nombre = "ESPACIO BIPOLAR COMUNICACIONES LTDA";
        $newProveedor->rut = "76201267-7";
        $newProveedor->email = "contacto@mygroup.cl";
        $newProveedor->transferencia = 0;
        $newProveedor->activo = 1;
        $newProveedor->save();

        $newProveedor = new Proveedor();
        $newProveedor->nombre = "HUDTWALCKER MEDICAL LTDA";
        $newProveedor->rut = "76815665-4";
        $newProveedor->email = "hw_dte@hwmedical.cl";
        $newProveedor->transferencia = 0;
        $newProveedor->activo = 1;
        $newProveedor->save();

        $newProveedor = new Proveedor();
        $newProveedor->nombre = "DIAZ SZIKLAI Y COMPAÑIA LTDA";
        $newProveedor->rut = "79945530-7";
        $newProveedor->transferencia = 0;
        $newProveedor->activo = 1;
        $newProveedor->save();

        $newProveedor = new Proveedor();
        $newProveedor->nombre = "SEA SALUD LTDA";
        $newProveedor->rut = "76120802-0";
        $newProveedor->transferencia = 0;
        $newProveedor->activo = 1;
        $newProveedor->save();

        $newProveedor = new Proveedor();
        $newProveedor->nombre = "JMO INTERNATIONAL LTDA";
        $newProveedor->rut = "79778630-6";
        $newProveedor->email = "kitty@jmo.cl";
        $newProveedor->transferencia = 0;
        $newProveedor->activo = 1;
        $newProveedor->save();

        $newProveedor = new Proveedor();
        $newProveedor->nombre = "HOSPITAL & MEDICAL SOLUTIONS S.A";
        $newProveedor->rut = "76382851-4";
        $newProveedor->email = "facturacion@facturachile.cl";
        $newProveedor->transferencia = 0;
        $newProveedor->activo = 1;
        $newProveedor->save();

        $newProveedor = new Proveedor();
        $newProveedor->nombre = "SOCIEDAD COMERCIAL NEGAMED SPA.";
        $newProveedor->rut = "76219408-2";
        $newProveedor->email = "administracion@negamed.cl";
        $newProveedor->transferencia = 0;
        $newProveedor->activo = 1;
        $newProveedor->save();

        $newProveedor = new Proveedor();
        $newProveedor->nombre = "SERVICIOS MEDICOS Y HOSPITALARIOS EDUSALUD LTDA";
        $newProveedor->rut = "77933700-6";
        $newProveedor->transferencia = 0;
        $newProveedor->activo = 1;
        $newProveedor->save();

        $newProveedor = new Proveedor();
        $newProveedor->nombre = "BHV CAPITAL SERVICIOS FINANCIEROS SPA";
        $newProveedor->rut = "76377454-6";
        $newProveedor->email = "cobranza@bhvcapital.cl";
        $newProveedor->transferencia = 0;
        $newProveedor->activo = 1;
        $newProveedor->save();

        $newProveedor = new Proveedor();
        $newProveedor->nombre = "MIRNA GUTIERREZ CONTRERAS";
        $newProveedor->rut = "9472854-1";
        $newProveedor->email = "mincotec@gmail.com";
        $newProveedor->transferencia = 0;
        $newProveedor->activo = 1;
        $newProveedor->save();

        $newProveedor = new Proveedor();
        $newProveedor->nombre = "VICTOR MORALES ACEVEDO IMPORTACIONES Y HERRAMIENTAS E.I.R.L";
        $newProveedor->rut = "76597170-5";
        $newProveedor->email = "ventas@victormorales.cl";
        $newProveedor->transferencia = 0;
        $newProveedor->activo = 1;
        $newProveedor->save();

        $newProveedor = new Proveedor();
        $newProveedor->nombre = "CONSTRUCTORA, INMOBILIARIA Y COMERCIALIZADORA AKCURA LTDA";
        $newProveedor->rut = "76113077-3";
        $newProveedor->email = "ematamala@akcura.cl";
        $newProveedor->transferencia = 0;
        $newProveedor->activo = 1;
        $newProveedor->save();

        $newProveedor = new Proveedor();
        $newProveedor->nombre = "COMERCIALIZADORA DE PRODUCTOS Y SERVICIOS ARCALAUQUEN S.A";
        $newProveedor->rut = "77315780-4";
        $newProveedor->transferencia = 0;
        $newProveedor->activo = 1;
        $newProveedor->save();

        $newProveedor = new Proveedor();
        $newProveedor->nombre = "ORTOMEDICA LIFANTE Y CIA LTDA";
        $newProveedor->rut = "79785840-4";
        $newProveedor->email = "ylifante@lifante.cl";
        $newProveedor->transferencia = 0;
        $newProveedor->activo = 1;
        $newProveedor->save();

        $newProveedor = new Proveedor();
        $newProveedor->nombre = "DR. REDDYS LABORATORIES CHILE SPA";
        $newProveedor->rut = "76754308-5";
        $newProveedor->email = "ventaschile@drredys.com";
        $newProveedor->transferencia = 0;
        $newProveedor->activo = 1;
        $newProveedor->save();

        $newProveedor = new Proveedor();
        $newProveedor->nombre = "CAMILA PAZ SOTO RAMIREZ DISENO, VENTA Y ARRIENDO DE EQUIPAMIENTOS E INSUMOS, EMPRESA INDIVIDUAL DE RESPONSABILIDAD LIMITADA";
        $newProveedor->rut = "76102508-2";
        $newProveedor->email = "info@partovertical.cl";
        $newProveedor->transferencia = 0;
        $newProveedor->activo = 1;
        $newProveedor->save();

        $newProveedor = new Proveedor();
        $newProveedor->nombre = "MUEBLES ANDROMEDA SPA";
        $newProveedor->rut = "76202596-5";
        $newProveedor->email = "jesenia@andromeda.cl";
        $newProveedor->transferencia = 0;
        $newProveedor->activo = 1;
        $newProveedor->save();

        $newProveedor = new Proveedor();
        $newProveedor->nombre = "XAVIER CARBALLAL ROMERO";
        $newProveedor->rut = "16301046-1";
        $newProveedor->transferencia = 0;
        $newProveedor->activo = 1;
        $newProveedor->save();

        $newProveedor = new Proveedor();
        $newProveedor->nombre = "KARINA INGRID CARVAJAL CARVAJAL";
        $newProveedor->rut = "14170832-5";
        $newProveedor->transferencia = 0;
        $newProveedor->activo = 1;
        $newProveedor->save();

        $newProveedor = new Proveedor();
        $newProveedor->nombre = "INVERSIONES SIETE CUMBRES LTDA.";
        $newProveedor->rut = "76475492-1";
        $newProveedor->email = "P.ZEBALLOS@7CUMBRES.CL";
        $newProveedor->transferencia = 0;
        $newProveedor->activo = 1;
        $newProveedor->save();

        $newProveedor = new Proveedor();
        $newProveedor->nombre = "PENTA FINANCIERO S.A";
        $newProveedor->rut = "99501480-7";
        $newProveedor->email = "cvillaseca@pentafinanciero.cl";
        $newProveedor->transferencia = 0;
        $newProveedor->activo = 1;
        $newProveedor->save();

        $newProveedor = new Proveedor();
        $newProveedor->nombre = "INVERSIONES G SPA";
        $newProveedor->rut = "76210356-7";
        $newProveedor->email = "pcisternas@innovafin.cl";
        $newProveedor->transferencia = 0;
        $newProveedor->activo = 1;
        $newProveedor->save();

        $newProveedor = new Proveedor();
        $newProveedor->nombre = "COMERCIALIZADORA JOSE ALBERTO MATAMALA PEREIRA E.I.R.L";
        $newProveedor->rut = "76310298-K";
        $newProveedor->email = "jmatamala@peniwenchile.cl";
        $newProveedor->transferencia = 0;
        $newProveedor->activo = 1;
        $newProveedor->save();

        $newProveedor = new Proveedor();
        $newProveedor->nombre = "COMERCIAL BELTCHILE SPA";
        $newProveedor->rut = "76377858-4";
        $newProveedor->email = "claudiacarrasco@beltchile.com";
        $newProveedor->transferencia = 0;
        $newProveedor->activo = 1;
        $newProveedor->save();

        $newProveedor = new Proveedor();
        $newProveedor->nombre = "SELCAP S.A";
        $newProveedor->rut = "76056004-9";
        $newProveedor->email = "ventas@selcap.cl";
        $newProveedor->transferencia = 0;
        $newProveedor->activo = 1;
        $newProveedor->save();

        $newProveedor = new Proveedor();
        $newProveedor->nombre = "COMERCIAL CADE GROUP SPA";
        $newProveedor->rut = "76117724-9";
        $newProveedor->email = "angie.perez@cadegroup.cl";
        $newProveedor->transferencia = 0;
        $newProveedor->activo = 1;
        $newProveedor->save();

        $newProveedor = new Proveedor();
        $newProveedor->nombre = "COMERCIAL TERMOLAM LTDA";
        $newProveedor->rut = "76007089-0";
        $newProveedor->email = "gerencia@termolam.cl";
        $newProveedor->transferencia = 0;
        $newProveedor->activo = 1;
        $newProveedor->save();

        $newProveedor = new Proveedor();
        $newProveedor->nombre = "COMERCIAL REUTTER S.A.";
        $newProveedor->rut = "76150425-8";
        $newProveedor->email = "jherrera@reutter.cl";
        $newProveedor->transferencia = 0;
        $newProveedor->activo = 1;
        $newProveedor->save();

        $newProveedor = new Proveedor();
        $newProveedor->nombre = "ISB SPA";
        $newProveedor->rut = "76369116-0";
        $newProveedor->transferencia = 0;
        $newProveedor->activo = 1;
        $newProveedor->save();

        $newProveedor = new Proveedor();
        $newProveedor->nombre = "COMERCIAL REUTTER S.A.";
        $newProveedor->rut = "761504258";
        $newProveedor->transferencia = 0;
        $newProveedor->activo = 0;
        $newProveedor->save();

        $newProveedor = new Proveedor();
        $newProveedor->nombre = "COMERCIALIZADORA  APRO";
        $newProveedor->rut = "86887200-4";
        $newProveedor->transferencia = 0;
        $newProveedor->activo = 1;
        $newProveedor->save();

        $newProveedor = new Proveedor();
        $newProveedor->nombre = "GARMENDIA MACUS S.A.";
        $newProveedor->rut = "96889950-3";
        $newProveedor->email = "portales@garmendia.cl";
        $newProveedor->transferencia = 0;
        $newProveedor->activo = 1;
        $newProveedor->save();

        $newProveedor = new Proveedor();
        $newProveedor->nombre = "TAJAMAR";
        $newProveedor->rut = "99592160-K";
        $newProveedor->email = "SVARGAS@INVERBRAC.CL";
        $newProveedor->transferencia = 0;
        $newProveedor->activo = 1;
        $newProveedor->save();

        $newProveedor = new Proveedor();
        $newProveedor->nombre = "EQUIPOS CONTRA INCENDIO SUPER LTDA";
        $newProveedor->rut = "76206680-7";
        $newProveedor->email = "MACARENA@ECIS.CL";
        $newProveedor->transferencia = 0;
        $newProveedor->activo = 1;
        $newProveedor->save();

        $newProveedor = new Proveedor();
        $newProveedor->nombre = "CORDERO S.A";
        $newProveedor->rut = "89435600-6";
        $newProveedor->transferencia = 0;
        $newProveedor->activo = 1;
        $newProveedor->save();

        $newProveedor = new Proveedor();
        $newProveedor->nombre = "ALPHA PHARMA SPA.";
        $newProveedor->rut = "76479314-5";
        $newProveedor->email = "GANDRASANDEEPREDDY@GMAIL.COM";
        $newProveedor->transferencia = 0;
        $newProveedor->activo = 1;
        $newProveedor->save();

        $newProveedor = new Proveedor();
        $newProveedor->nombre = "TONALLI DE CHILE SPA";
        $newProveedor->rut = "76263017-6";
        $newProveedor->email = "szarur@tonalli.cl";
        $newProveedor->transferencia = 0;
        $newProveedor->activo = 1;
        $newProveedor->save();

        $newProveedor = new Proveedor();
        $newProveedor->nombre = "MERCEDES CERDA ITURRIAGA SERV.TEC MANT.";
        $newProveedor->rut = "76167614-8";
        $newProveedor->email = "hidraulicostotales@gmail.com";
        $newProveedor->transferencia = 0;
        $newProveedor->activo = 1;
        $newProveedor->save();

        $newProveedor = new Proveedor();
        $newProveedor->nombre = "EUROAMERICA ADMINISTRADORA GENERAL DE FONDOS SA";
        $newProveedor->rut = "77750920-9";
        $newProveedor->email = "tesoreriafactoring@euroamerica.cl";
        $newProveedor->transferencia = 0;
        $newProveedor->activo = 1;
        $newProveedor->save();

        $newProveedor = new Proveedor();
        $newProveedor->nombre = "JOSE ANTONIO SAINZ RIVERON";
        $newProveedor->rut = "25533782-3";
        $newProveedor->transferencia = 0;
        $newProveedor->activo = 1;
        $newProveedor->save();

        $newProveedor = new Proveedor();
        $newProveedor->nombre = "BRUNO ANDRES PENA BRAVO";
        $newProveedor->rut = "16751340-9";
        $newProveedor->transferencia = 0;
        $newProveedor->activo = 1;
        $newProveedor->save();

        $newProveedor = new Proveedor();
        $newProveedor->nombre = "ALVARO BENJAMIN ALARCON DEL CAMPO";
        $newProveedor->rut = "17403977-1";
        $newProveedor->transferencia = 0;
        $newProveedor->activo = 1;
        $newProveedor->save();

        $newProveedor = new Proveedor();
        $newProveedor->nombre = "VICTOR MANUEL TERCERO VALDES VASQUEZ";
        $newProveedor->rut = "16653299-K";
        $newProveedor->transferencia = 0;
        $newProveedor->activo = 1;
        $newProveedor->save();

        $newProveedor = new Proveedor();
        $newProveedor->nombre = "CARLO LORENZO PAOLETTO";
        $newProveedor->rut = "14659586-3";
        $newProveedor->transferencia = 0;
        $newProveedor->activo = 1;
        $newProveedor->save();

        $newProveedor = new Proveedor();
        $newProveedor->nombre = "FRANCISCA ANDREA TORRES SANCHEZ";
        $newProveedor->rut = "17186025-3";
        $newProveedor->transferencia = 0;
        $newProveedor->activo = 1;
        $newProveedor->save();

        $newProveedor = new Proveedor();
        $newProveedor->nombre = "NATALIA SOLEDAD ITURRA ROA";
        $newProveedor->rut = "17365346-8";
        $newProveedor->transferencia = 0;
        $newProveedor->activo = 1;
        $newProveedor->save();

        $newProveedor = new Proveedor();
        $newProveedor->nombre = "DANIELA ALEJANDRA BUSTOS CARRENO";
        $newProveedor->rut = "17984700-0";
        $newProveedor->transferencia = 0;
        $newProveedor->activo = 1;
        $newProveedor->save();

        $newProveedor = new Proveedor();
        $newProveedor->nombre = "LEONARDO ALEXIS CARMONA LOPEZ";
        $newProveedor->rut = "25227795-1";
        $newProveedor->transferencia = 0;
        $newProveedor->activo = 1;
        $newProveedor->save();

        $newProveedor = new Proveedor();
        $newProveedor->nombre = "JAVIER ALEJANDRO MORAGA CONCHA";
        $newProveedor->rut = "17176960-4";
        $newProveedor->transferencia = 0;
        $newProveedor->activo = 1;
        $newProveedor->save();

        $newProveedor = new Proveedor();
        $newProveedor->nombre = "CLAUDIO AVELINO CORDERO MOLINA";
        $newProveedor->rut = "8969303-9";
        $newProveedor->transferencia = 0;
        $newProveedor->activo = 1;
        $newProveedor->save();

        $newProveedor = new Proveedor();
        $newProveedor->nombre = "GUILLERMO FELIPE CORTES LIRA";
        $newProveedor->rut = "18167126-2";
        $newProveedor->transferencia = 0;
        $newProveedor->activo = 1;
        $newProveedor->save();

        $newProveedor = new Proveedor();
        $newProveedor->nombre = "SEBASTIAN WALKER SEPULVEDA";
        $newProveedor->rut = "16890325-1";
        $newProveedor->transferencia = 0;
        $newProveedor->activo = 1;
        $newProveedor->save();

        $newProveedor = new Proveedor();
        $newProveedor->nombre = "FRANCISCO JAVIER RIVERA ROA";
        $newProveedor->rut = "16354439-3";
        $newProveedor->transferencia = 0;
        $newProveedor->activo = 1;
        $newProveedor->save();

        $newProveedor = new Proveedor();
        $newProveedor->nombre = "YANDRI ROBERTO PICO BRIONES";
        $newProveedor->rut = "21742850-5";
        $newProveedor->transferencia = 0;
        $newProveedor->activo = 1;
        $newProveedor->save();

        $newProveedor = new Proveedor();
        $newProveedor->nombre = "KATHERINE VALESKA VENEGAS BARRERA";
        $newProveedor->rut = "15605023-7";
        $newProveedor->transferencia = 0;
        $newProveedor->activo = 1;
        $newProveedor->save();

        $newProveedor = new Proveedor();
        $newProveedor->nombre = "JUAN CARLOS GARCIA HERNANDEZ";
        $newProveedor->rut = "24583766-6";
        $newProveedor->transferencia = 0;
        $newProveedor->activo = 1;
        $newProveedor->save();

        $newProveedor = new Proveedor();
        $newProveedor->nombre = "COMERCIALIZADORA DE PRODUCTOS PARA LABORATORIOS E INDUSTRIA LTDA.";
        $newProveedor->rut = "77814080-2";
        $newProveedor->email = "ainostroza@inmunodiagnostico.cl";
        $newProveedor->transferencia = 0;
        $newProveedor->activo = 1;
        $newProveedor->save();

        $newProveedor = new Proveedor();
        $newProveedor->nombre = "MARIA ANTONIA MENA AMIGO";
        $newProveedor->rut = "16211012-8";
        $newProveedor->transferencia = 0;
        $newProveedor->activo = 1;
        $newProveedor->save();

        $newProveedor = new Proveedor();
        $newProveedor->nombre = "RIGOBERTO VALECILLOS GOITIA";
        $newProveedor->rut = "24606285-4";
        $newProveedor->transferencia = 0;
        $newProveedor->activo = 1;
        $newProveedor->save();

        $newProveedor = new Proveedor();
        $newProveedor->nombre = "MARCELA PAZ MACHUCA PINTO";
        $newProveedor->rut = "16358083-7";
        $newProveedor->transferencia = 0;
        $newProveedor->activo = 1;
        $newProveedor->save();

        $newProveedor = new Proveedor();
        $newProveedor->nombre = "ALVARO ANDRES HERRERA ALCAINO";
        $newProveedor->rut = "17314807-0";
        $newProveedor->transferencia = 0;
        $newProveedor->activo = 1;
        $newProveedor->save();

        $newProveedor = new Proveedor();
        $newProveedor->nombre = "CRISTIAN PABLO ANDRES PAULSEN UGALDE";
        $newProveedor->rut = "17849992-0";
        $newProveedor->transferencia = 0;
        $newProveedor->activo = 1;
        $newProveedor->save();

        $newProveedor = new Proveedor();
        $newProveedor->nombre = "NICOLAS IVAN AVILES ESPINOZA";
        $newProveedor->rut = "15333104-9";
        $newProveedor->transferencia = 0;
        $newProveedor->activo = 1;
        $newProveedor->save();

        $newProveedor = new Proveedor();
        $newProveedor->nombre = "ALVARO DANIEL VIDAL FAUNE";
        $newProveedor->rut = "10311708-9";
        $newProveedor->transferencia = 0;
        $newProveedor->activo = 1;
        $newProveedor->save();

        $newProveedor = new Proveedor();
        $newProveedor->nombre = "DENISE MADELAINE DE BAEREMAECKER QUIROZ";
        $newProveedor->rut = "14006365-7";
        $newProveedor->transferencia = 0;
        $newProveedor->activo = 1;
        $newProveedor->save();

        $newProveedor = new Proveedor();
        $newProveedor->nombre = "ANDRAS GYORGY EITLER HECK";
        $newProveedor->rut = "15711521-9";
        $newProveedor->transferencia = 0;
        $newProveedor->activo = 1;
        $newProveedor->save();

        $newProveedor = new Proveedor();
        $newProveedor->nombre = "MATIAS IGNACIO BRAVO DANOBEITIA";
        $newProveedor->rut = "17024522-9";
        $newProveedor->transferencia = 0;
        $newProveedor->activo = 1;
        $newProveedor->save();

        $newProveedor = new Proveedor();
        $newProveedor->nombre = "JOSE RONALDI ARANDA VERA";
        $newProveedor->rut = "13023850-5";
        $newProveedor->transferencia = 0;
        $newProveedor->activo = 1;
        $newProveedor->save();

        $newProveedor = new Proveedor();
        $newProveedor->nombre = "DANIELA TAMARA DE LA BARRA ESCOBAR";
        $newProveedor->rut = "15900615-8";
        $newProveedor->transferencia = 0;
        $newProveedor->activo = 1;
        $newProveedor->save();

        $newProveedor = new Proveedor();
        $newProveedor->nombre = "DENISE ISABELLE SALLES MUÑOZ";
        $newProveedor->rut = "17317397-0";
        $newProveedor->transferencia = 0;
        $newProveedor->activo = 1;
        $newProveedor->save();

        $newProveedor = new Proveedor();
        $newProveedor->nombre = "DANIELA ANDREA RIBBECK BARAIBAR";
        $newProveedor->rut = "16605782-5";
        $newProveedor->transferencia = 0;
        $newProveedor->activo = 1;
        $newProveedor->save();

        $newProveedor = new Proveedor();
        $newProveedor->nombre = "MARIA DOLORES MARCHANT ROMERO";
        $newProveedor->rut = "18229361-K";
        $newProveedor->transferencia = 0;
        $newProveedor->activo = 1;
        $newProveedor->save();

        $newProveedor = new Proveedor();
        $newProveedor->nombre = "PABLO ANDRES DELGADO ZAMBRANO";
        $newProveedor->rut = "15551293-8";
        $newProveedor->transferencia = 0;
        $newProveedor->activo = 1;
        $newProveedor->save();

        $newProveedor = new Proveedor();
        $newProveedor->nombre = "SERVICIO DE SALUD SUR HOSPITAL LUCIO CORDOVA";
        $newProveedor->rut = "61608104-7";
        $newProveedor->email = "sandra.velasquez@redsalud.gov.cl";
        $newProveedor->transferencia = 0;
        $newProveedor->activo = 1;
        $newProveedor->save();

        $newProveedor = new Proveedor();
        $newProveedor->nombre = "GENETICA Y TECNOLOGIA SPA";
        $newProveedor->rut = "78340830-9";
        $newProveedor->transferencia = 0;
        $newProveedor->activo = 1;
        $newProveedor->save();

        $newProveedor = new Proveedor();
        $newProveedor->nombre = "LIFE FITNESS";
        $newProveedor->rut = "78241170-5";
        $newProveedor->transferencia = 0;
        $newProveedor->activo = 1;
        $newProveedor->save();

        $newProveedor = new Proveedor();
        $newProveedor->nombre = "FUNDACION CLINICA SAN PEDRO";
        $newProveedor->rut = "65047141-5";
        $newProveedor->transferencia = 0;
        $newProveedor->activo = 1;
        $newProveedor->save();

        $newProveedor = new Proveedor();
        $newProveedor->nombre = "NO USAR";
        $newProveedor->rut = "174019053";
        $newProveedor->transferencia = 0;
        $newProveedor->activo = 1;
        $newProveedor->save();

        $newProveedor = new Proveedor();
        $newProveedor->nombre = "NO USAR";
        $newProveedor->rut = "170245229";
        $newProveedor->transferencia = 0;
        $newProveedor->activo = 1;
        $newProveedor->save();

        $newProveedor = new Proveedor();
        $newProveedor->nombre = "IM IMPORTACIONES MEDICAS S.A";
        $newProveedor->rut = "77939920-6";
        $newProveedor->transferencia = 0;
        $newProveedor->activo = 1;
        $newProveedor->save();

        $newProveedor = new Proveedor();
        $newProveedor->nombre = "FARMACEUTICA XENIUS LIMITADA";
        $newProveedor->rut = "76247477-8";
        $newProveedor->transferencia = 0;
        $newProveedor->activo = 1;
        $newProveedor->save();

        $newProveedor = new Proveedor();
        $newProveedor->nombre = "INVERSIONES Y ASESORIAS BRAC LIMIDATA";
        $newProveedor->rut = "76007628-7";
        $newProveedor->transferencia = 0;
        $newProveedor->activo = 1;
        $newProveedor->save();

        $newProveedor = new Proveedor();
        $newProveedor->nombre = "INFORMATICA Y MARKETING SPA";
        $newProveedor->rut = "76127194-6";
        $newProveedor->transferencia = 0;
        $newProveedor->activo = 1;
        $newProveedor->save();

        $newProveedor = new Proveedor();
        $newProveedor->nombre = "SOCIEDAD COMERCIAL IMPROFAR LIMITADA";
        $newProveedor->rut = "76071981-1";
        $newProveedor->email = "aperez@improfar.cl";
        $newProveedor->transferencia = 0;
        $newProveedor->activo = 1;
        $newProveedor->save();

        $newProveedor = new Proveedor();
        $newProveedor->nombre = "AMINORTE S.A";
        $newProveedor->rut = "99533780-0";
        $newProveedor->transferencia = 0;
        $newProveedor->activo = 1;
        $newProveedor->save();

        $newProveedor = new Proveedor();
        $newProveedor->nombre = "TECNOCLASS SPA";
        $newProveedor->rut = "76398639-K";
        $newProveedor->transferencia = 0;
        $newProveedor->activo = 1;
        $newProveedor->save();

        $newProveedor = new Proveedor();
        $newProveedor->nombre = "COMERCIALIZADORA TODOTABLET SPA";
        $newProveedor->rut = "76292976-7";
        $newProveedor->transferencia = 0;
        $newProveedor->activo = 1;
        $newProveedor->save();

        $newProveedor = new Proveedor();
        $newProveedor->nombre = "INGENIERIA Y CONSTRUCCION TOTEM LIMITADA";
        $newProveedor->rut = "76382302-4";
        $newProveedor->email = "scarmona@itotem.cl";
        $newProveedor->transferencia = 0;
        $newProveedor->activo = 1;
        $newProveedor->save();

        $newProveedor = new Proveedor();
        $newProveedor->nombre = "FACTORCLICK SA";
        $newProveedor->rut = "76146246-6";
        $newProveedor->email = "contacto@factorclick.cl";
        $newProveedor->transferencia = 0;
        $newProveedor->activo = 1;
        $newProveedor->save();

        $newProveedor = new Proveedor();
        $newProveedor->nombre = "VELIA INES SALDIAS HARGREAVES";
        $newProveedor->rut = "15378835-9";
        $newProveedor->transferencia = 0;
        $newProveedor->activo = 1;
        $newProveedor->save();

        $newProveedor = new Proveedor();
        $newProveedor->nombre = "PAMELA GONZALEZ BUGUENO";
        $newProveedor->rut = "15932047-2";
        $newProveedor->transferencia = 0;
        $newProveedor->activo = 1;
        $newProveedor->save();

        $newProveedor = new Proveedor();
        $newProveedor->nombre = "VAICO MEDICAL";
        $newProveedor->rut = "76116217-9";
        $newProveedor->transferencia = 0;
        $newProveedor->activo = 1;
        $newProveedor->save();

        $newProveedor = new Proveedor();
        $newProveedor->nombre = "MARIA ISABEL AHUMADA REAL";
        $newProveedor->rut = "3874994-3";
        $newProveedor->direccion = "SAN GUMERCINDO 350";
        $newProveedor->transferencia = 0;
        $newProveedor->activo = 1;
        $newProveedor->save();

        $newProveedor = new Proveedor();
        $newProveedor->nombre = "CONCENTRADORES Y EJECUTORES DE NEGOCIOS SPA";
        $newProveedor->rut = "76361137-K";
        $newProveedor->transferencia = 0;
        $newProveedor->activo = 1;
        $newProveedor->save();

        $newProveedor = new Proveedor();
        $newProveedor->nombre = "BIOMUNDO SPA";
        $newProveedor->rut = "76261586-K";
        $newProveedor->transferencia = 0;
        $newProveedor->activo = 1;
        $newProveedor->save();

        $newProveedor = new Proveedor();
        $newProveedor->nombre = "ECOSER S.A";
        $newProveedor->rut = "96729820-4";
        $newProveedor->transferencia = 0;
        $newProveedor->activo = 1;
        $newProveedor->save();

        $newProveedor = new Proveedor();
        $newProveedor->nombre = "FERRETERIA COMERCIAL L&J LTDA";
        $newProveedor->rut = "76258116-7";
        $newProveedor->transferencia = 0;
        $newProveedor->activo = 1;
        $newProveedor->save();

        $newProveedor = new Proveedor();
        $newProveedor->nombre = "LABORATORIO BAMBERG LTDA";
        $newProveedor->rut = "76296704-9";
        $newProveedor->transferencia = 0;
        $newProveedor->activo = 1;
        $newProveedor->save();

        $newProveedor = new Proveedor();
        $newProveedor->nombre = "SOC COM MACSOL LTDA";
        $newProveedor->rut = "76150876-8";
        $newProveedor->transferencia = 0;
        $newProveedor->activo = 1;
        $newProveedor->save();

        $newProveedor = new Proveedor();
        $newProveedor->nombre = "PUNTO CAPITAL SERVICIOS FINANCIEROS S.A";
        $newProveedor->rut = "76118452-0";
        $newProveedor->direccion = "ROSARIO NORTE";
        $newProveedor->email = "lcifuentes@puntocapital.cl";
        $newProveedor->transferencia = 0;
        $newProveedor->activo = 1;
        $newProveedor->save();

        $newProveedor = new Proveedor();
        $newProveedor->nombre = "COMERCIAL RM SPA";
        $newProveedor->rut = "76810668-1";
        $newProveedor->transferencia = 0;
        $newProveedor->activo = 1;
        $newProveedor->save();

        $newProveedor = new Proveedor();
        $newProveedor->nombre = "ARRAYAN FACTORING";
        $newProveedor->rut = "76865845-5";
        $newProveedor->email = "fcisternas@arrayanfinanciero.cl";
        $newProveedor->transferencia = 0;
        $newProveedor->activo = 1;
        $newProveedor->save();

        $newProveedor = new Proveedor();
        $newProveedor->nombre = "FONDO INVERSION PRIVADO CAPITAL 1";
        $newProveedor->rut = "76598556-0";
        $newProveedor->email = "notificaciones-fynpal@fondodyf.cl";
        $newProveedor->transferencia = 0;
        $newProveedor->activo = 1;
        $newProveedor->save();

        $newProveedor = new Proveedor();
        $newProveedor->nombre = "COMER.Y DISTRIB. ORTOPEDIC Y CIA LTDA.";
        $newProveedor->rut = "77765630-9";
        $newProveedor->transferencia = 0;
        $newProveedor->activo = 1;
        $newProveedor->save();

        $newProveedor = new Proveedor();
        $newProveedor->nombre = "HDI SEGUROS S.A";
        $newProveedor->rut = "99231000-6";
        $newProveedor->transferencia = 0;
        $newProveedor->activo = 1;
        $newProveedor->save();

        $newProveedor = new Proveedor();
        $newProveedor->nombre = "CIBERTEC RETAIL S.A";
        $newProveedor->rut = "76793590-0";
        $newProveedor->transferencia = 0;
        $newProveedor->activo = 1;
        $newProveedor->save();

        $newProveedor = new Proveedor();
        $newProveedor->nombre = "UC CHRISTUS SERVICIOS AMBULATORIOS SPA";
        $newProveedor->rut = "76754097-3";
        $newProveedor->transferencia = 0;
        $newProveedor->activo = 1;
        $newProveedor->save();

        $newProveedor = new Proveedor();
        $newProveedor->nombre = "W REICHMANN Y CIA LTDA";
        $newProveedor->rut = "80783200-K";
        $newProveedor->transferencia = 0;
        $newProveedor->activo = 1;
        $newProveedor->save();

        $newProveedor = new Proveedor();
        $newProveedor->nombre = "DISTRIBUIDORA DE ARTICULOS DE OFICINA ASEO Y LIMPIEZA AYPRA SPA.";
        $newProveedor->rut = "76546360-2";
        $newProveedor->transferencia = 0;
        $newProveedor->activo = 1;
        $newProveedor->save();

        $newProveedor = new Proveedor();
        $newProveedor->nombre = "EQUIMED ELECTRONICA LIMITADA";
        $newProveedor->rut = "77017950-5";
        $newProveedor->email = "XIMENA.HERNANDEZ@EQUIMED.CL";
        $newProveedor->transferencia = 0;
        $newProveedor->activo = 1;
        $newProveedor->save();

        $newProveedor = new Proveedor();
        $newProveedor->nombre = "EVENTOS Y REPOSTERIA CREATIVA MAURICIO HENRIQUEZ SOTO";
        $newProveedor->rut = "76482349-4";
        $newProveedor->transferencia = 0;
        $newProveedor->activo = 1;
        $newProveedor->save();

        $newProveedor = new Proveedor();
        $newProveedor->nombre = "SOC. CONC AUTOPISTA NUEVA VESPUCIO SUR S.A.";
        $newProveedor->rut = "76052927-3";
        $newProveedor->transferencia = 0;
        $newProveedor->activo = 1;
        $newProveedor->save();

        $newProveedor = new Proveedor();
        $newProveedor->nombre = "SERVICIOS DE CATERING MARLY VALESKA VERGARA BARRERA E.I.R.L.";
        $newProveedor->rut = "76385471-K";
        $newProveedor->transferencia = 0;
        $newProveedor->activo = 1;
        $newProveedor->save();

        $newProveedor = new Proveedor();
        $newProveedor->nombre = "COMERCIAL E INVERSIONES HERNANDEZ LTDA";
        $newProveedor->rut = "76070402-4";
        $newProveedor->transferencia = 0;
        $newProveedor->activo = 1;
        $newProveedor->save();

    }
}
