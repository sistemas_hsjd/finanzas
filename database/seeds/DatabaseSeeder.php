<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        $this->call(UsersTableSeeder::class);
        // $this->call(ProveedoresTableSeeder::class);
        $this->call(TiposArchivoTableSeeder::class);
        $this->call(TiposDocumentoTableSeeder::class);
        $this->call(ModalidadesCompraTableSeeder::class);
        $this->call(TiposAdjudicacionTableSeeder::class);
        $this->call(TiposInformeTableSeeder::class);
        $this->call(ReferentesTecnicosTableSeeder::class);
        // $this->call(ItemsPresupuestariosTableSeeder::class);
        $this->call(ClasificacionesTableSeeder::class);
        $this->call(TiposCuentaTableSeeder::class);
        $this->call(BancosTableSeeder::class);
        $this->call(PermissionsTableSeeder::class);
    }
}
