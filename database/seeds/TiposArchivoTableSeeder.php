<?php

use Illuminate\Database\Seeder;
use App\TipoArchivo;

class TiposArchivoTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $newTipoArchivo = new TipoArchivo();
        $newTipoArchivo->nombre = 'Factura';
        $newTipoArchivo->save();

        $newTipoArchivo = new TipoArchivo();
        $newTipoArchivo->nombre = 'Boleta de Honorarios';
        $newTipoArchivo->save();

        $newTipoArchivo = new TipoArchivo();
        $newTipoArchivo->nombre = 'Orden de Compra';
        $newTipoArchivo->save();

        $newTipoArchivo = new TipoArchivo();
        $newTipoArchivo->nombre = 'Memo Visto Bueno';
        $newTipoArchivo->save();

        $newTipoArchivo = new TipoArchivo();
        $newTipoArchivo->nombre = 'Recepción';
        $newTipoArchivo->save();

        $newTipoArchivo = new TipoArchivo();
        $newTipoArchivo->nombre = 'Contrato-Resolución';
        $newTipoArchivo->save();

        $newTipoArchivo = new TipoArchivo();
        $newTipoArchivo->nombre = 'Licitación';
        $newTipoArchivo->save();

        $newTipoArchivo = new TipoArchivo();
        $newTipoArchivo->nombre = 'Boleta de Garantía';
        $newTipoArchivo->save();

        $newTipoArchivo = new TipoArchivo();
        $newTipoArchivo->nombre = 'Documento Factoring';
        $newTipoArchivo->save();

        $newTipoArchivo = new TipoArchivo();
        $newTipoArchivo->nombre = 'Memo Autorización de Retiro';
        $newTipoArchivo->save();

        $newTipoArchivo = new TipoArchivo();
        $newTipoArchivo->nombre = 'Nota de Crédito';
        $newTipoArchivo->save();

        $newTipoArchivo = new TipoArchivo();
        $newTipoArchivo->nombre = 'Nota de Debito';
        $newTipoArchivo->save();

        $newTipoArchivo = new TipoArchivo();
        $newTipoArchivo->nombre = 'Boleta de Pago';
        $newTipoArchivo->save();

        $newTipoArchivo = new TipoArchivo();
        $newTipoArchivo->nombre = 'Guía de Despacho';
        $newTipoArchivo->save();

        $newTipoArchivo = new TipoArchivo();
        $newTipoArchivo->nombre = 'Autorización SII';
        $newTipoArchivo->save();
    }
}
