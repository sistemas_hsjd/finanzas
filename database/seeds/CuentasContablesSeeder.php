<?php

use Illuminate\Database\Seeder;
use App\CuentaContable;

class CuentasContablesSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        
        $cuenta = new CuentaContable;
        $cuenta->codigo = '21522';
        $cuenta->glosa = 'Cuentas por Pagar - Bienes y Servicios de Consumo';
        $cuenta->saldo = 0;
        $cuenta->save();

        // $cuenta = new CuentaContable;
        // $cuenta->codigo = '21.11.003.1';
        // $cuenta->glosa = 'Segunda cuenta de test';
        // $cuenta->saldo = 2000000;
        // $cuenta->save();

        // $cuenta = new CuentaContable;
        // $cuenta->codigo = '13.03.100.5';
        // $cuenta->glosa = 'Tercera cuenta de test';
        // $cuenta->saldo = 3000000;
        // $cuenta->save();

        // $cuenta = new CuentaContable;
        // $cuenta->codigo = '11.04.203.8';
        // $cuenta->glosa = 'Cuarta cuenta de test';
        // $cuenta->saldo = 4000000;
        // $cuenta->save();

    }
}
