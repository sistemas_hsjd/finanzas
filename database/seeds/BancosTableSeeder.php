<?php

use Illuminate\Database\Seeder;
use App\Banco;

class BancosTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $newBanco = new Banco();
        $newBanco->nombre = "BANCO ESTADO";
        $newBanco->save();

        $newBanco = new Banco();
        $newBanco->nombre = "BANCO DE CHILE";
        $newBanco->save();

        $newBanco = new Banco();
        $newBanco->nombre = "BANCO INTERNACIONAL";
        $newBanco->save();

        $newBanco = new Banco();
        $newBanco->nombre = "SCOTIABANK-DESARROLLO";
        $newBanco->save();

        $newBanco = new Banco();
        $newBanco->nombre = "BANCO DE CREDITO E INVERSIONES";
        $newBanco->save();

        $newBanco = new Banco();
        $newBanco->nombre = "CORP-BANCA";
        $newBanco->save();

        $newBanco = new Banco();
        $newBanco->nombre = "BICE";
        $newBanco->save();

        $newBanco = new Banco();
        $newBanco->nombre = "HSBC BANK CHILE";
        $newBanco->save();

        $newBanco = new Banco();
        $newBanco->nombre = "BANCO SANTANDER";
        $newBanco->save();

        $newBanco = new Banco();
        $newBanco->nombre = "BANCO ITAU";
        $newBanco->save();

        $newBanco = new Banco();
        $newBanco->nombre = "THE BANK OF TOKYO-MITSUBISHI";
        $newBanco->save();

        $newBanco = new Banco();
        $newBanco->nombre = "BANCO SECURITY";
        $newBanco->save();

        $newBanco = new Banco();
        $newBanco->nombre = "BANCO FALABELLA";
        $newBanco->save();

        $newBanco = new Banco();
        $newBanco->nombre = "BANCO RIPLEY";
        $newBanco->save();

        $newBanco = new Banco();
        $newBanco->nombre = "BANCO CONSORCIO";
        $newBanco->save();

        $newBanco = new Banco();
        $newBanco->nombre = "BANCO PARIS";
        $newBanco->save();

        $newBanco = new Banco();
        $newBanco->nombre = "BANCO BBVA";
        $newBanco->save();

        $newBanco = new Banco();
        $newBanco->nombre = "COOPEUCH";
        $newBanco->save();
    }
}
