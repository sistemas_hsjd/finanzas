<?php

use Illuminate\Database\Seeder;
use App\TipoInforme;

class TiposInformeTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $newTipoInforme = new TipoInforme();
        $newTipoInforme->nombre = 'Programa';
        $newTipoInforme->save();

        $newTipoInforme = new TipoInforme();
        $newTipoInforme->nombre = 'Cenabast';
        $newTipoInforme->save();

        $newTipoInforme = new TipoInforme();
        $newTipoInforme->nombre = 'Clínicas';
        $newTipoInforme->save();

        $newTipoInforme = new TipoInforme();
        $newTipoInforme->nombre = 'Convenio';
        $newTipoInforme->save();

        $newTipoInforme = new TipoInforme();
        $newTipoInforme->nombre = 'Consumos básicos';
        $newTipoInforme->save();

        $newTipoInforme = new TipoInforme();
        $newTipoInforme->nombre = 'Honorarios';
        $newTipoInforme->save();

        $newTipoInforme = new TipoInforme();
        $newTipoInforme->nombre = 'Fondo fijo';
        $newTipoInforme->save();

        $newTipoInforme = new TipoInforme();
        $newTipoInforme->nombre = 'Liquida Anticipo';
        $newTipoInforme->save();

        $newTipoInforme = new TipoInforme();
        $newTipoInforme->nombre = 'Gob Transparente';
        $newTipoInforme->save();

        $newTipoInforme = new TipoInforme();
        $newTipoInforme->nombre = 'Electricidad Portales';
        $newTipoInforme->save();

        $newTipoInforme = new TipoInforme();
        $newTipoInforme->nombre = 'Electricidad Chacabuco';
        $newTipoInforme->save();

        $newTipoInforme = new TipoInforme();
        $newTipoInforme->nombre = 'Electricidad Huérfanos';
        $newTipoInforme->save();

        $newTipoInforme = new TipoInforme();
        $newTipoInforme->nombre = 'Electricidad Chacabuco Helen Lee';
        $newTipoInforme->save();

        $newTipoInforme = new TipoInforme();
        $newTipoInforme->nombre = 'Agua Portales';
        $newTipoInforme->save();

        $newTipoInforme = new TipoInforme();
        $newTipoInforme->nombre = 'Agua Chacabuco';
        $newTipoInforme->save();

        $newTipoInforme = new TipoInforme();
        $newTipoInforme->nombre = 'Agua Huérfanos';
        $newTipoInforme->save();

        $newTipoInforme = new TipoInforme();
        $newTipoInforme->nombre = 'Agua Chacabuco Helen Lee';
        $newTipoInforme->save();

        $newTipoInforme = new TipoInforme();
        $newTipoInforme->nombre = 'Compra de Servicios';
        $newTipoInforme->save();
    }
}
