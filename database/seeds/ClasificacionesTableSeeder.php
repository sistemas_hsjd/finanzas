<?php

use Illuminate\Database\Seeder;
use App\Clasificacion;

class ClasificacionesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $newClasificacion = new Clasificacion();
        $newClasificacion->nombre = "Servicios Básicos";
        $newClasificacion->save();

        $newClasificacion = new Clasificacion();
        $newClasificacion->nombre = "Mantenimiento y reparaciones";
        $newClasificacion->save();

        $newClasificacion = new Clasificacion();
        $newClasificacion->nombre = "Alimentación";
        $newClasificacion->save();

        $newClasificacion = new Clasificacion();
        $newClasificacion->nombre = "Arriendo y Seguros";
        $newClasificacion->save();

        $newClasificacion = new Clasificacion();
        $newClasificacion->nombre = "Lavandería";
        $newClasificacion->save();

        $newClasificacion = new Clasificacion();
        $newClasificacion->nombre = "Combustibles y lubricantes";
        $newClasificacion->save();

        $newClasificacion = new Clasificacion();
        $newClasificacion->nombre = "Materiales y útiles de oficina";
        $newClasificacion->save();

        $newClasificacion = new Clasificacion();
        $newClasificacion->nombre = "Vestuario y calzado";
        $newClasificacion->save();

        $newClasificacion = new Clasificacion();
        $newClasificacion->nombre = "Capacitación";
        $newClasificacion->save();

        $newClasificacion = new Clasificacion();
        $newClasificacion->nombre = "Otros gastos Fijos";
        $newClasificacion->save();

        $newClasificacion = new Clasificacion();
        $newClasificacion->nombre = "Productos farmacéuticos";
        $newClasificacion->save();

        $newClasificacion = new Clasificacion();
        $newClasificacion->nombre = "Materiales y Útiles Quirúrgicos";
        $newClasificacion->save();

        $newClasificacion = new Clasificacion();
        $newClasificacion->nombre = "Productos Químicos";
        $newClasificacion->save();

        $newClasificacion = new Clasificacion();
        $newClasificacion->nombre = "DFL-36";
        $newClasificacion->save();

        $newClasificacion = new Clasificacion();
        $newClasificacion->nombre = "Rebases";
        $newClasificacion->save();

        $newClasificacion = new Clasificacion();
        $newClasificacion->nombre = "Servicios de intermediación CENABAST";
        $newClasificacion->save();

        $newClasificacion = new Clasificacion();
        $newClasificacion->nombre = "Asesorías y estudios";
        $newClasificacion->save();

        $newClasificacion = new Clasificacion();
        $newClasificacion->nombre = "Compra de prestaciones";
        $newClasificacion->save();
        
    }
}
