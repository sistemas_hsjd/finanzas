<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\Hash;
use App\User;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $newUser = new User();
        $newUser->name = 'INFORMÁTICA';
        $newUser->rut = '44444444-4';
        $newUser->password = Hash::make('info123');
        $newUser->email = 'admin@admin.cl';
        $newUser->iniciales = 'adm';
        $newUser->save();

        $newUser = new User();
        $newUser->name = 'ANA MARIA RAMIREZ SOTO';
        $newUser->rut = '11975531-K';
        $newUser->password = Hash::make('11975531');
        $newUser->iniciales = 'ars';
        $newUser->responsable = 1;
        $newUser->save();

        $newUser = new User();
        $newUser->name = 'CAROLINA GATICA VILAZA';
        $newUser->rut = '14258158-2';
        $newUser->password = Hash::make('14258158');
        $newUser->iniciales = 'cgv';
        $newUser->responsable = 1;
        $newUser->save();

        $newUser = new User();
        $newUser->name = 'CAROLINA VARGAS ARENAS';
        $newUser->rut = '15800134-9';
        $newUser->password = Hash::make('15800134');
        $newUser->iniciales = 'cva';
        $newUser->responsable = 1;
        $newUser->save();

        $newUser = new User();
        $newUser->name = 'DANIELA VILLALOBOS CACERES';
        $newUser->rut = '15155505-5';
        $newUser->password = Hash::make('15155505');
        $newUser->iniciales = 'dvc';
        $newUser->responsable = 1;
        $newUser->save();

        $newUser = new User();
        $newUser->name = 'DENISSE BUSTOS TRONCOSO';
        $newUser->rut = '16716212-6';
        $newUser->password = Hash::make('16716212');
        $newUser->iniciales = 'dbt';
        $newUser->responsable = 1;
        $newUser->save();

        $newUser = new User();
        $newUser->name = 'ENEDINA INAPAIMILLA VERGARA';
        $newUser->rut = '8572289-1';
        $newUser->password = Hash::make('8572289');
        $newUser->iniciales = 'eiv';
        $newUser->responsable = 1;
        $newUser->save();

        $newUser = new User();
        $newUser->name = 'FERNANDO ARANEDA SAA';
        $newUser->rut = '14140479-2';
        $newUser->password = Hash::make('14140479');
        $newUser->iniciales = 'fas';
        $newUser->responsable = 1;
        $newUser->save();

        $newUser = new User();
        $newUser->name = 'FERNANDO CAVADA NARVAEZ';
        $newUser->rut = '12407751-6';
        $newUser->password = Hash::make('12407751');
        $newUser->iniciales = 'fcn';
        $newUser->responsable = 1;
        $newUser->save();

        $newUser = new User();
        $newUser->name = 'JESSICA AVILES VILLAR';
        $newUser->rut = '16175869-8';
        $newUser->password = Hash::make('16175869');
        $newUser->iniciales = 'jav';
        $newUser->responsable = 1;
        $newUser->save();

        // 11
        $newUser = new User();
        $newUser->name = 'JUAN CARLOS PIÑEIRO SALINAS';
        $newUser->rut = '15708531-K';
        $newUser->password = Hash::make('15708531');
        $newUser->iniciales = 'jps';
        $newUser->responsable = 1;
        $newUser->save();

        // 12
        $newUser = new User();
        $newUser->name = 'KELLY IBAÑEZ PAVEZ';
        $newUser->rut = '19345120-9';
        $newUser->password = Hash::make('19345120');
        $newUser->iniciales = 'kip';
        $newUser->responsable = 1;
        $newUser->save();

        // 13
        $newUser = new User();
        $newUser->name = 'LORETO FLORES RIVERA';
        $newUser->rut = '11839056-3';
        $newUser->password = Hash::make('11839056');
        $newUser->iniciales = 'lfr';
        $newUser->responsable = 1;
        $newUser->save();

        // 14
        $newUser = new User();
        $newUser->name = 'MANUEL SEPULVEDA CAMPOS';
        $newUser->rut = '9707668-5';
        $newUser->password = Hash::make('9707668');
        $newUser->iniciales = 'msc';
        $newUser->responsable = 1;
        $newUser->save();

        // 15
        $newUser = new User();
        $newUser->name = 'MANUEL YAÑEZ PASTRAN';
        $newUser->rut = '18836707-0';
        $newUser->password = Hash::make('18836707');
        $newUser->iniciales = 'myp';
        $newUser->responsable = 1;
        $newUser->save();

        // 16
        $newUser = new User();
        $newUser->name = 'MARIA CRISTINA ALVAREZ BENAVIDES';
        $newUser->rut = '16953333-4';
        $newUser->password = Hash::make('16953333');
        $newUser->iniciales = 'mab';
        $newUser->responsable = 1;
        $newUser->save();

        // 17
        $newUser = new User();
        $newUser->name = 'PABLO OSORIO CARRASCO';
        $newUser->rut = '15328899-2';
        $newUser->password = Hash::make('15328899');
        $newUser->iniciales = 'poc';
        $newUser->responsable = 1;
        $newUser->save();

        // 18
        $newUser = new User();
        $newUser->name = 'RODRIGO BRAVO GAJARDO';
        $newUser->rut = '16553260-0';
        $newUser->password = Hash::make('16553260');
        $newUser->email = 'user@user.cl';
        $newUser->iniciales = 'rbg';
        $newUser->responsable = 1;
        $newUser->save();

        $newUser = new User();
        $newUser->name = 'SUSANA VELOSO AZOCAR';
        $newUser->rut = '8454142-7';
        $newUser->password = Hash::make('8454142');
        $newUser->iniciales = 'sva';
        $newUser->responsable = 1;
        $newUser->save();

        $newUser = new User();
        $newUser->name = 'WALDO MOLINA GATTO';
        $newUser->rut = '11261122-3';
        $newUser->password = Hash::make('11261122');
        $newUser->iniciales = 'wmg';
        $newUser->responsable = 0;
        $newUser->save();

        $newUser = new User();
        $newUser->name = 'DANTE ANTHONY VEGA MUÑOZ';
        $newUser->rut = '19839634-6';
        $newUser->password = Hash::make('19839634');
        $newUser->iniciales = 'dvm';
        $newUser->responsable = 1;
        $newUser->save();

        $newUser = new User();
        $newUser->name = 'OLGA JESSICA ALFARO FUENTES';
        $newUser->rut = '13374305-7';
        $newUser->password = Hash::make('13374305');
        $newUser->iniciales = 'oaf';
        $newUser->responsable = 0;
        $newUser->save();

        $newUser = new User();
        $newUser->name = 'JUAN IGNACIO BARROS VARAS';
        $newUser->rut = '18730427-K';
        $newUser->password = Hash::make('18730427');
        $newUser->iniciales = 'jbv';
        $newUser->responsable = 1;
        $newUser->save();

        $newUser = new User();
        $newUser->name = 'JAVIERA MELLA GONZALEZ';
        $newUser->rut = '19585673-7';
        $newUser->password = Hash::make('19585673');
        $newUser->iniciales = 'jmg';
        $newUser->responsable = 1;
        $newUser->save();

        $newUser = new User();
        $newUser->name = 'CECILIA DIAZ OSORIO';
        $newUser->rut = '18079946-K';
        $newUser->password = Hash::make('18079946');
        $newUser->iniciales = 'cdo';
        $newUser->responsable = 1;
        $newUser->save();

        $newUser = new User();
        $newUser->name = 'EVELYN CECILIA ALARCÓN CÁRDENAS';
        $newUser->rut = '8928502-K';
        $newUser->password = Hash::make('8928502');
        $newUser->iniciales = 'eac';
        $newUser->responsable = 1;
        $newUser->save();

        $newUser = new User();
        $newUser->name = 'Isabel Martínez Rubilar';
        $newUser->rut = '8868194-0';
        $newUser->password = Hash::make('8868194');
        $newUser->iniciales = 'imr';
        $newUser->responsable = 0;
        $newUser->save();

        $newUser = new User();
        $newUser->name = 'Ximena Henríquez Iturriaga';
        $newUser->rut = '9806074-K';
        $newUser->password = Hash::make('9806074');
        $newUser->iniciales = 'xhi';
        $newUser->responsable = 0;
        $newUser->save();

        $newUser = new User();
        $newUser->name = 'Guillermina Oviedo Alarcón';
        $newUser->rut = '7618439-9';
        $newUser->password = Hash::make('7618439');
        $newUser->iniciales = 'goa';
        $newUser->responsable = 0;
        $newUser->save();

        $newUser = new User();
        $newUser->name = 'JUAN CRISTIAN MENA DAZA';
        $newUser->rut = '12177209-4';
        $newUser->password = Hash::make('12177209');
        $newUser->iniciales = 'jmd';
        $newUser->responsable = 0;
        $newUser->save();

        $newUser = new User();
        $newUser->name = 'PATRICIA SOLEDAD CANDIA CONTRERAS';
        $newUser->rut = '11475573-7';
        $newUser->password = Hash::make('11475573');
        $newUser->iniciales = 'pcc';
        $newUser->responsable = 0;
        $newUser->save();
        
        $newUser = new User();
        $newUser->name = 'Valeska Díaz Merino';
        $newUser->rut = '15603566-1';
        $newUser->password = Hash::make('15603566');
        $newUser->iniciales = 'vdm';
        $newUser->responsable = 0;
        $newUser->save();

        $newUser = new User();
        $newUser->name = 'CLAUDIA ARAYA PEREZ';
        $newUser->rut = '14329399-8';
        $newUser->password = Hash::make('14329399');
        $newUser->email = 'claudia.arayap@redsalud.gov.cl';
        $newUser->iniciales = 'cap';
        $newUser->responsable = 0;
        $newUser->save();

        $newUser = new User();
        $newUser->name = 'RAUL NUÑEZ ESPINOZA';
        $newUser->rut = '19283181-4';
        $newUser->password = Hash::make('19283181');
        $newUser->email = 'RAUL.NUNEZ.E@REDSALUD.GOV.CL';
        $newUser->iniciales = 'RNE';
        $newUser->responsable = 1;
        $newUser->save();

        // 35
        $newUser = new User();
        $newUser->name = 'IRMA MATUS DE LA PARRA';
        $newUser->rut = '10559531-K';
        $newUser->password = Hash::make('10559531');
        $newUser->email = 'irma.matusdelaparra@redsalud.gov.cl';
        $newUser->iniciales = 'imp';
        $newUser->save();

        // 36
        $newUser = new User();
        $newUser->name = 'JAVIER VERA';
        $newUser->rut = null;
        $newUser->password = Hash::make('123456789');
        $newUser->save();

        // 37
        $newUser = new User();
        $newUser->name = 'ALEJANDRA MAKUC SIERRALTA';
        $newUser->rut = '11827051-7';
        $newUser->password = Hash::make('11827051');
        $newUser->iniciales = 'ams';
        $newUser->save();

        // 38
        $newUser = new User();
        $newUser->name = 'KAREN FLORES ZÚÑIGA';
        $newUser->rut = '15417173-8';
        $newUser->password = Hash::make('15417173');
        $newUser->iniciales = 'kfz';
        $newUser->save();

        // 39
        $newUser = new User();
        $newUser->name = 'ROLANDO SÁEZ DÍAZ';
        $newUser->rut = '13456826-7';
        $newUser->password = Hash::make('13456826');
        $newUser->iniciales = 'rsd';
        $newUser->save();

        // 40
        $newUser = new User();
        $newUser->name = 'LORENA POBLETE OCARANZA';
        $newUser->rut = '11723814-8';
        $newUser->password = Hash::make('11723814');
        $newUser->iniciales = 'lpo';
        $newUser->save();

        // 41
        $newUser = new User();
        $newUser->name = 'FRANCISCO BERMEJO M';
        $newUser->rut = null;
        $newUser->password = Hash::make('123456789');
        $newUser->iniciales = 'fbm';
        $newUser->save();

        // 42
        $newUser = new User();
        $newUser->name = 'VERONICA PEREZ';
        $newUser->rut = '13065085-6';
        $newUser->password = Hash::make('13065085');
        $newUser->iniciales = 'vp';
        $newUser->save();

        // 43
        $newUser = new User();
        $newUser->name = 'CESAR JIMENEZ';
        $newUser->rut = '12872594-6';
        $newUser->password = Hash::make('12872594');
        $newUser->iniciales = 'cj';
        $newUser->save();

        // 44
        $newUser = new User();
        $newUser->name = 'GUIDO GONZALEZ';
        $newUser->rut = '10037421-8';
        $newUser->password = Hash::make('10037421');
        $newUser->iniciales = 'gg';
        $newUser->save();

        // 45
        $newUser = new User();
        $newUser->name = 'JACQUELINE PETERS R';
        $newUser->rut = '12526844-7';
        $newUser->password = Hash::make('12526844');
        $newUser->iniciales = 'jpr';
        $newUser->save();

        // 46
        $newUser = new User();
        $newUser->name = 'JORGE CARO';
        $newUser->rut = '14144155-8';
        $newUser->password = Hash::make('14144155');
        $newUser->iniciales = 'jc';
        $newUser->save();

        // 47
        $newUser = new User();
        $newUser->name = 'CLAUDIA MOLINA';
        $newUser->rut = '10691629-2';
        $newUser->password = Hash::make('10691629');
        $newUser->iniciales = 'cm';
        $newUser->save();

        // 48
        $newUser = new User();
        $newUser->name = 'ANTONIA BARRERA MUÑOZ';
        $newUser->rut = '12236715-0';
        $newUser->password = Hash::make('12236715');
        $newUser->iniciales = 'abm';
        $newUser->save();

        // 49
        $newUser = new User();
        $newUser->name = 'DANITZA CALZADILLA';
        $newUser->rut = '17558118-9';
        $newUser->password = Hash::make('17558118');
        $newUser->iniciales = 'dc';
        $newUser->save();

        // 50
        $newUser = new User();
        $newUser->name = 'ANA MARIA ASPEE PLAZA';
        $newUser->rut = '10531322-5';
        $newUser->password = Hash::make('10531322');
        $newUser->iniciales = 'dc';
        $newUser->save();

        // 51
        $newUser = new User();
        $newUser->name = 'FRANCISCA GONZALEZ P';
        $newUser->rut = null;
        $newUser->password = Hash::make('123456789');
        $newUser->iniciales = 'fgp';
        $newUser->save();

        // 52
        $newUser = new User();
        $newUser->name = 'MARIA EUGENIA VILLARROEL CANDIA';
        $newUser->rut = null;
        $newUser->password = Hash::make('123456789');
        $newUser->iniciales = 'mevc';
        $newUser->save();

        // 53
        $newUser = new User();
        $newUser->name = 'WALTER AVENDAÑO JARA';
        $newUser->rut = '10026841-8';
        $newUser->password = Hash::make('10026841');
        $newUser->iniciales = 'waj';
        $newUser->save();

        // 54
        $newUser = new User();
        $newUser->name = 'María de los Angeles Alarcón Arratia';
        $newUser->rut = null;
        $newUser->password = Hash::make('123456789');
        $newUser->iniciales = 'mc';
        $newUser->save();

        // 55
        $newUser = new User();
        $newUser->name = 'MARCIA CONCHA';
        $newUser->rut = null;
        $newUser->password = Hash::make('123456789');
        $newUser->iniciales = 'mc';
        $newUser->save();

        // 56
        $newUser = new User();
        $newUser->name = 'SEBASTIAN HENRIQUEZ ANTIL';
        $newUser->rut = null;
        $newUser->password = Hash::make('123456789');
        $newUser->iniciales = 'sha';
        $newUser->save();

        // 57
        $newUser = new User();
        $newUser->name = 'BERNARDITA LEIVA';
        $newUser->rut = null;
        $newUser->password = Hash::make('123456789');
        $newUser->iniciales = 'bl';
        $newUser->save();

        // 58
        $newUser = new User();
        $newUser->name = 'VERONICA PEREZ GUTIERREZ';
        $newUser->rut = null;
        $newUser->password = Hash::make('123456789');
        $newUser->iniciales = 'vpg';
        $newUser->save();

        // 59
        $newUser = new User();
        $newUser->name = 'CLAUDIO SALDIVAR M';
        $newUser->rut = null;
        $newUser->password = Hash::make('123456789');
        $newUser->iniciales = 'cs';
        $newUser->save();

    }
}
