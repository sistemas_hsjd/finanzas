<?php

use Illuminate\Database\Seeder;
use App\ModalidadCompra;

class ModalidadesCompraTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $newModalidadCompra = new ModalidadCompra();
        $newModalidadCompra->nombre = 'MercadoPublico';
        $newModalidadCompra->save();

        $newModalidadCompra = new ModalidadCompra();
        $newModalidadCompra->nombre = 'Cenabast';
        $newModalidadCompra->predeterminado = 'Intermediación;Programa Ministerial';
        $newModalidadCompra->save();

        $newModalidadCompra = new ModalidadCompra();
        $newModalidadCompra->nombre = 'Autorización de Pago';
        $newModalidadCompra->save();

        $newModalidadCompra = new ModalidadCompra();
        $newModalidadCompra->nombre = 'Consumo Basico';
        $newModalidadCompra->predeterminado = 'Electricidad;Agua;Gas;Correo;Autopista';
        $newModalidadCompra->save();

        $newModalidadCompra = new ModalidadCompra();
        $newModalidadCompra->nombre = 'Fondo Fijo';
        $newModalidadCompra->save();

        $newModalidadCompra = new ModalidadCompra();
        $newModalidadCompra->nombre = 'Resolución Exenta';
        $newModalidadCompra->save();
    }
}
