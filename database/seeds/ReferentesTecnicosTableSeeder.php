<?php

use Illuminate\Database\Seeder;
use App\ReferenteTecnico;

class ReferentesTecnicosTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        // 1
        $newReferenteTecnico = new ReferenteTecnico();
        $newReferenteTecnico->id = 1;
        $newReferenteTecnico->id_user = 35;
        $newReferenteTecnico->rut = "10559531-K";
        $newReferenteTecnico->nombre = "JEFE DE INFORMATICA";
        $newReferenteTecnico->responsable = "SRA. IRMA MATUS DE LA PARRA";
        $newReferenteTecnico->origen = "0";
        $newReferenteTecnico->url_firma = '/firmas/firma_irma.PNG';
        $newReferenteTecnico->save();

        // 2
        $newReferenteTecnico = new ReferenteTecnico();
        $newReferenteTecnico->id = 2;
        $newReferenteTecnico->id_user = 36;
        $newReferenteTecnico->nombre = "JEFE (S) CR LOGISTICA";
        $newReferenteTecnico->responsable = "SR. JAVIER VERA";
        $newReferenteTecnico->origen = "0";
        $newReferenteTecnico->save();

        // 5
        $newReferenteTecnico = new ReferenteTecnico();
        $newReferenteTecnico->id = 5;
        $newReferenteTecnico->id_user = 37;
        $newReferenteTecnico->rut = "11827051-7";
        $newReferenteTecnico->nombre = "JEFE HOSPITALIZACIÓN DOMICILIARIA";
        $newReferenteTecnico->responsable = "SRA. ALEJANDRA MAKUC SIERRALTA";
        $newReferenteTecnico->origen = "0";
        $newReferenteTecnico->save();

        // 6
        $newReferenteTecnico = new ReferenteTecnico();
        $newReferenteTecnico->id = 6;
        $newReferenteTecnico->rut = "16365739-2";
        $newReferenteTecnico->nombre = "SUBDIRECTORA DE DESARROLLO DE LAS PERSONAS (S)";
        $newReferenteTecnico->responsable = "SRA. GABRIELA HOFFMANN";
        $newReferenteTecnico->origen = "0";
        $newReferenteTecnico->save();
        $newReferenteTecnico->delete();

        // 7
        $newReferenteTecnico = new ReferenteTecnico();
        $newReferenteTecnico->id = 7;
        $newReferenteTecnico->id_user = 38;
        $newReferenteTecnico->rut = "15417173-8";
        $newReferenteTecnico->nombre = "CONVENIOS MÉDICOS";
        $newReferenteTecnico->responsable = "SRA. KAREN FLORES ZÚÑIGA";
        $newReferenteTecnico->origen = "0";
        $newReferenteTecnico->save();

        // 8
        $newReferenteTecnico = new ReferenteTecnico();
        $newReferenteTecnico->id = 8;
        $newReferenteTecnico->id_user = 39;
        $newReferenteTecnico->rut = "13456826-7";
        $newReferenteTecnico->nombre = "JEFE ROPERÍA";
        $newReferenteTecnico->responsable = "SR. ROLANDO SÁEZ DÍAZ";
        $newReferenteTecnico->origen = "0";
        $newReferenteTecnico->save();

        // 9
        $newReferenteTecnico = new ReferenteTecnico();
        $newReferenteTecnico->id = 9;
        $newReferenteTecnico->id_user = 40;
        $newReferenteTecnico->rut = "11723814-8";
        $newReferenteTecnico->nombre = "JEFE CAPACITACIÓN";
        $newReferenteTecnico->responsable = "SRA. LORENA POBLETE OCARANZA";
        $newReferenteTecnico->origen = "0";
        $newReferenteTecnico->save();

        // 10
        $newReferenteTecnico = new ReferenteTecnico();
        $newReferenteTecnico->id = 10;
        $newReferenteTecnico->id_user = 11;
        $newReferenteTecnico->rut = "15708531-K";
        $newReferenteTecnico->nombre = "JEFE RECAUDACIÓN Y FACTURACIÓN";
        $newReferenteTecnico->responsable = "SR. JUAN CARLOS PIÑEIRO SALINAS";
        $newReferenteTecnico->origen = "0";
        $newReferenteTecnico->save();

        // 12
        $newReferenteTecnico = new ReferenteTecnico();
        $newReferenteTecnico->id = 12;
        $newReferenteTecnico->nombre = "JEFE ASUNTOS INSTITUCIONALES";
        $newReferenteTecnico->responsable = "SR.";
        $newReferenteTecnico->origen = "0";
        $newReferenteTecnico->save();
        $newReferenteTecnico->delete();

        // 13
        $newReferenteTecnico = new ReferenteTecnico();
        $newReferenteTecnico->id = 13;
        $newReferenteTecnico->id_user = 41;
        $newReferenteTecnico->rut = "7235523-7";
        $newReferenteTecnico->nombre = "JEFE DE BODEGA";
        $newReferenteTecnico->responsable = "FRANCISCO BERMEJO M";
        $newReferenteTecnico->origen = "0";
        $newReferenteTecnico->save();

        // 14
        $newReferenteTecnico = new ReferenteTecnico();
        $newReferenteTecnico->id = 14;
        $newReferenteTecnico->id_user = 42;
        $newReferenteTecnico->rut = "13065085-6";
        $newReferenteTecnico->nombre = "JEFA ATENCIÓN SOCIAL AL PERSONAL";
        $newReferenteTecnico->responsable = "SRA. VERONICA PEREZ";
        $newReferenteTecnico->origen = "0";
        $newReferenteTecnico->save();

        // 15
        $newReferenteTecnico = new ReferenteTecnico();
        $newReferenteTecnico->id = 15;
        $newReferenteTecnico->id_user = 43;
        $newReferenteTecnico->rut = "12872594-6";
        $newReferenteTecnico->nombre = "JEFE MEDICINA NUCLEAR";
        $newReferenteTecnico->responsable = "SR. CESAR JIMENEZ";
        $newReferenteTecnico->origen = "0";
        $newReferenteTecnico->save();

        // 17
        $newReferenteTecnico = new ReferenteTecnico();
        $newReferenteTecnico->id = 17;
        $newReferenteTecnico->rut = "8454142-7";
        $newReferenteTecnico->nombre = "JEFE CR. FINANZAS (S)";
        $newReferenteTecnico->responsable = "SRA. SUSANA VELOSO AZOCAR";
        $newReferenteTecnico->origen = "0";
        $newReferenteTecnico->save();
        $newReferenteTecnico->delete();

        // 18
        $newReferenteTecnico = new ReferenteTecnico();
        $newReferenteTecnico->id = 18;
        $newReferenteTecnico->id_user = 44;
        $newReferenteTecnico->rut = "10037421-8";
        $newReferenteTecnico->nombre = "JEFE UNIDAD DE IMAGENOLOGÍA";
        $newReferenteTecnico->responsable = "SR. GUIDO GONZALEZ";
        $newReferenteTecnico->origen = "0";
        $newReferenteTecnico->save();

        // 19
        $newReferenteTecnico = new ReferenteTecnico();
        $newReferenteTecnico->id = 19;
        $newReferenteTecnico->id_user = 45;
        $newReferenteTecnico->rut = "12526844-7";
        $newReferenteTecnico->nombre = "JEFE LABORATORIO CLINICO";
        $newReferenteTecnico->responsable = "JACQUELINE PETERS R";
        $newReferenteTecnico->origen = "0";
        $newReferenteTecnico->save();

        // 20
        $newReferenteTecnico = new ReferenteTecnico();
        $newReferenteTecnico->id = 20;
        $newReferenteTecnico->id_user = 46;
        $newReferenteTecnico->rut = "14144155-8";
        $newReferenteTecnico->nombre = "JEFE DE PREVENCION DE RIESGO";
        $newReferenteTecnico->responsable = "JORGE CARO";
        $newReferenteTecnico->origen = "0";
        $newReferenteTecnico->save();

        // 21
        $newReferenteTecnico = new ReferenteTecnico();
        $newReferenteTecnico->id = 21;
        $newReferenteTecnico->id_user = 47;
        $newReferenteTecnico->rut = "10691629-2";
        $newReferenteTecnico->nombre = "JEFE BANCO DE SANDRE";
        $newReferenteTecnico->responsable = "CLAUDIA MOLINA";
        $newReferenteTecnico->origen = "0";
        $newReferenteTecnico->save();

        // 22
        $newReferenteTecnico = new ReferenteTecnico();
        $newReferenteTecnico->id = 22;
        $newReferenteTecnico->rut = "13907067-4";
        $newReferenteTecnico->nombre = "JEFA CALIDAD DE VIDA";
        $newReferenteTecnico->responsable = "MARIELA GALLARDO VIÑALS";
        $newReferenteTecnico->origen = "0";
        $newReferenteTecnico->save();
        $newReferenteTecnico->delete();

        // 23 
        $newReferenteTecnico = new ReferenteTecnico();
        $newReferenteTecnico->id = 23;
        $newReferenteTecnico->rut = "9622805-8";
        $newReferenteTecnico->nombre = "JEFE DE PROYECTOS";
        $newReferenteTecnico->responsable = "MARIA ANGELICA MORALES";
        $newReferenteTecnico->origen = "0";
        $newReferenteTecnico->save();
        $newReferenteTecnico->delete();

        // 24 
        $newReferenteTecnico = new ReferenteTecnico();
        $newReferenteTecnico->id = 24;
        $newReferenteTecnico->nombre = "JEFE DE IMAGENOLOGIA";
        $newReferenteTecnico->responsable = "GUIDO GONZALEZ";
        $newReferenteTecnico->origen = "0";
        $newReferenteTecnico->save();
        $newReferenteTecnico->delete();

        // 25
        $newReferenteTecnico = new ReferenteTecnico();
        $newReferenteTecnico->id = 25;
        $newReferenteTecnico->id_user = 48;
        $newReferenteTecnico->rut = "12236715-0";
        $newReferenteTecnico->nombre = "JEFE DE MOVILIZACION";
        $newReferenteTecnico->responsable = "ANTONIA BARRERA MUÑOZ";
        $newReferenteTecnico->origen = "0";
        $newReferenteTecnico->save();

        // 26
        $newReferenteTecnico = new ReferenteTecnico();
        $newReferenteTecnico->id = 26;
        $newReferenteTecnico->id_user = 18;
        $newReferenteTecnico->rut = "16553260-0";
        $newReferenteTecnico->nombre = "JEFE CR. FINANZAS";
        $newReferenteTecnico->responsable = "SR. RODRIGO BRAVO GAJARDO";
        $newReferenteTecnico->origen = "1";
        $newReferenteTecnico->url_firma = '/firmas/KHerrera.jpg';
        $newReferenteTecnico->save();

        // 27
        $newReferenteTecnico = new ReferenteTecnico();
        $newReferenteTecnico->id = 27;
        $newReferenteTecnico->rut = "16660518-0";
        $newReferenteTecnico->nombre = "JEFE DE INVESTIGACION Y DOCENCIA";
        $newReferenteTecnico->responsable = "SILVANA SUAREZ";
        $newReferenteTecnico->origen = "0";
        $newReferenteTecnico->save();
        $newReferenteTecnico->delete();

        // 28
        $newReferenteTecnico = new ReferenteTecnico();
        $newReferenteTecnico->id = 28;
        $newReferenteTecnico->id_user = 49;
        $newReferenteTecnico->rut = "17558118-9";
        $newReferenteTecnico->nombre = "JEFE DE ESTERILIZACION";
        $newReferenteTecnico->responsable = "DANITZA CALZADILLA";
        $newReferenteTecnico->origen = "0";
        $newReferenteTecnico->save();

        // 29
        $newReferenteTecnico = new ReferenteTecnico();
        $newReferenteTecnico->id = 29;
        $newReferenteTecnico->rut = "12167046-1";
        $newReferenteTecnico->nombre = "JEFE DE OPERACIONES";
        $newReferenteTecnico->responsable = "JORGE ESPINOZA CARDENAS";
        $newReferenteTecnico->origen = "0";
        $newReferenteTecnico->save();
        $newReferenteTecnico->delete();

        // 30
        $newReferenteTecnico = new ReferenteTecnico();
        $newReferenteTecnico->id = 30;
        $newReferenteTecnico->id_user = 50;
        $newReferenteTecnico->rut = "10531322-5";
        $newReferenteTecnico->nombre = "ANA MARIA ASPEE PLAZA";
        $newReferenteTecnico->responsable = "OFICINA DE PARTES";
        $newReferenteTecnico->origen = "0";
        $newReferenteTecnico->save();

        // 31
        $newReferenteTecnico = new ReferenteTecnico();
        $newReferenteTecnico->id = 31;
        $newReferenteTecnico->id_user = 51;
        $newReferenteTecnico->nombre = "JEFA UNIDAD SATISFACCION USUARIA";
        $newReferenteTecnico->responsable = "SRA. FRANCISCA GONZALEZ P.";
        $newReferenteTecnico->origen = "0";
        $newReferenteTecnico->save();

        // 33
        $newReferenteTecnico = new ReferenteTecnico();
        $newReferenteTecnico->id = 33;
        $newReferenteTecnico->id_user = 52;
        $newReferenteTecnico->nombre = "SUBDIRECTORA ADMINISTRATIVA";
        $newReferenteTecnico->responsable = "SRTA MARIA EUGENIA VILLARROEL CANDIA";
        $newReferenteTecnico->origen = "0";
        $newReferenteTecnico->save();

        // 34
        $newReferenteTecnico = new ReferenteTecnico();
        $newReferenteTecnico->id = 34;
        $newReferenteTecnico->id_user = 53;
        $newReferenteTecnico->rut = "10026841-8";
        $newReferenteTecnico->nombre = "JEFE EQUIPOS MEDICOS (S)";
        $newReferenteTecnico->responsable = "WALTER AVENDAÑO JARA";
        $newReferenteTecnico->origen = "0";
        $newReferenteTecnico->save();

        // 35
        $newReferenteTecnico = new ReferenteTecnico();
        $newReferenteTecnico->id = 35;
        $newReferenteTecnico->id_user = 54;
        $newReferenteTecnico->nombre = "EU policlínico de pediatría";
        $newReferenteTecnico->responsable = "María de los Angeles Alarcón Arratia";
        $newReferenteTecnico->origen = "0";
        $newReferenteTecnico->save();

        // 36
        $newReferenteTecnico = new ReferenteTecnico();
        $newReferenteTecnico->id_user = 55;
        $newReferenteTecnico->nombre = "SECRETARIA DE DIRECCION";
        $newReferenteTecnico->responsable = "MARCIA CONCHA";
        $newReferenteTecnico->origen = "0";
        $newReferenteTecnico->save();

        // 37
        $newReferenteTecnico = new ReferenteTecnico();
        $newReferenteTecnico->id = 37;
        $newReferenteTecnico->nombre = "JEFE CR. FINANZAS (S)";
        $newReferenteTecnico->responsable = "JUAN CARLOS PIÑEIRO SALINAS";
        $newReferenteTecnico->origen = "0";
        $newReferenteTecnico->save();
        $newReferenteTecnico->delete();

        // 38
        $newReferenteTecnico = new ReferenteTecnico();
        $newReferenteTecnico->id = 38;
        $newReferenteTecnico->id_user = 56;
        $newReferenteTecnico->nombre = "JEFE GESTION DE LAS PERSONAS (S)";
        $newReferenteTecnico->responsable = "SEBASTIAN HENRIQUEZ ANTIL";
        $newReferenteTecnico->origen = "0";
        $newReferenteTecnico->save();

        // 39
        $newReferenteTecnico = new ReferenteTecnico();
        $newReferenteTecnico->id = 39;
        $newReferenteTecnico->id_user = 57;
        $newReferenteTecnico->nombre = "JEFA GESTION DE LAS PERSONAS";
        $newReferenteTecnico->responsable = "SRTA. BERNARDITA LEIVA";
        $newReferenteTecnico->origen = "0";
        $newReferenteTecnico->save();

        // 40
        $newReferenteTecnico = new ReferenteTecnico();
        $newReferenteTecnico->id = 40;
        $newReferenteTecnico->id_user = 58;
        $newReferenteTecnico->nombre = "JEFA (S) CALIDAD DE VIDA";
        $newReferenteTecnico->responsable = "VERONICA PEREZ GUTIERREZ";
        $newReferenteTecnico->origen = "0";
        $newReferenteTecnico->save();

        // 41
        $newReferenteTecnico = new ReferenteTecnico();
        $newReferenteTecnico->id = 41;
        $newReferenteTecnico->id_user = 59;
        $newReferenteTecnico->nombre = "SUBDIRECTOR APOYO CLINICO";
        $newReferenteTecnico->responsable = "CLAUDIO SALDIVAR M";
        $newReferenteTecnico->origen = "0";
        $newReferenteTecnico->save();
    }
}
