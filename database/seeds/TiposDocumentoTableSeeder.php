<?php

use Illuminate\Database\Seeder;
use App\TipoDocumento;

class TiposDocumentoTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $newTipoDocumento = new TipoDocumento();
        $newTipoDocumento->nombre = 'Factura Electrónica';
        $newTipoDocumento->sigla = 'FE';
        $newTipoDocumento->save();

        $newTipoDocumento = new TipoDocumento();
        $newTipoDocumento->nombre = 'Factura';
        $newTipoDocumento->sigla = 'F';
        $newTipoDocumento->save();

        $newTipoDocumento = new TipoDocumento();
        $newTipoDocumento->nombre = 'Boleta de Honorarios Afecta';
        $newTipoDocumento->sigla = 'BA';
        $newTipoDocumento->save();

        $newTipoDocumento = new TipoDocumento();
        $newTipoDocumento->nombre = 'Nota de Credito';
        $newTipoDocumento->sigla = 'NC';
        $newTipoDocumento->save();

        $newTipoDocumento = new TipoDocumento();
        $newTipoDocumento->nombre = 'Nota de Debito';
        $newTipoDocumento->sigla = 'ND';
        $newTipoDocumento->save();

        $newTipoDocumento = new TipoDocumento();
        $newTipoDocumento->nombre = 'Rendición de fondo fijo';
        $newTipoDocumento->sigla = 'FF';
        $newTipoDocumento->save();

        $newTipoDocumento = new TipoDocumento();
        $newTipoDocumento->nombre = 'Boleta de Venta y Servicio';
        $newTipoDocumento->sigla = 'VS';
        $newTipoDocumento->save();

        $newTipoDocumento = new TipoDocumento();
        $newTipoDocumento->nombre = 'Re-Facturacion';
        $newTipoDocumento->sigla = 'RF';
        $newTipoDocumento->save();

        $newTipoDocumento = new TipoDocumento();
        $newTipoDocumento->nombre = 'Boleta de Honorarios Exenta';
        $newTipoDocumento->sigla = 'BE';
        $newTipoDocumento->save();

        $newTipoDocumento = new TipoDocumento();
        $newTipoDocumento->nombre = 'Carta Certificada Credito';
        $newTipoDocumento->sigla = 'CC';
        $newTipoDocumento->save();

        $newTipoDocumento = new TipoDocumento();
        $newTipoDocumento->nombre = 'Carta Certificada Debito';
        $newTipoDocumento->sigla = 'CD';
        $newTipoDocumento->save();
    }
}
