<?php

use Illuminate\Database\Seeder;
use App\Permission;
use App\Role;
use App\User;

class PermissionsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        /* Mantenedores */

        $crearUser = new Permission();
        $crearUser->name         = 'crear-user';
        $crearUser->display_name = 'Crear Usuario'; // optional
        // Permite al usuario...
        $crearUser->description  = 'Crear usuarios'; // optional
        $crearUser->save();
        
        $editarUser = new Permission();
        $editarUser->name         = 'editar-user';
        $editarUser->display_name = 'Editar Usuario'; // optional
        // Permite que un usuario...
        $editarUser->description  = 'Editar usuarios'; // optional
        $editarUser->save();

        $eliminarUser = new Permission();
        $eliminarUser->name         = 'eliminar-user';
        $eliminarUser->display_name = 'Eliminar Usuario'; // optional
        // Permite que un usuario...
        $eliminarUser->description  = 'Eliminar usuarios'; // optional
        $eliminarUser->save();

        $verUser = new Permission();
        $verUser->name         = 'ver-user';
        $verUser->display_name = 'Ver Usuario'; // optional
        // Permite que un usuario...
        $verUser->description  = 'Ver usuarios'; // optional
        $verUser->save();


        $verGeneralUser = new Permission();
        $verGeneralUser->name         = 'ver-general-user';
        $verGeneralUser->display_name = 'Ver Listado Usuarios'; // optional
        // Permite que un usuario...
        $verGeneralUser->description  = 'Ver listado de usuarios'; // optional
        $verGeneralUser->save();

        $crearProveedor = new Permission();
        $crearProveedor->name         = 'crear-proveedor';
        $crearProveedor->display_name = 'Crear Proveedor'; // optional
        // Permite que un usuario...
        $crearProveedor->description  = 'Crear proveedores'; // optional
        $crearProveedor->save();

        $editarProveedor = new Permission();
        $editarProveedor->name         = 'editar-proveedor';
        $editarProveedor->display_name = 'Editar Proveedor'; // optional
        // Permite que un usuario...
        $editarProveedor->description  = 'Editar proveedores'; // optional
        $editarProveedor->save();

        $eliminarProveedor = new Permission();
        $eliminarProveedor->name         = 'eliminar-proveedor';
        $eliminarProveedor->display_name = 'Eliminar Proveedor'; // optional
        // Permite que un usuario...
        $eliminarProveedor->description  = 'Eliminar proveedores'; // optional
        $eliminarProveedor->save();

        $verProveedor = new Permission();
        $verProveedor->name         = 'ver-proveedor';
        $verProveedor->display_name = 'Ver Proveedor'; // optional
        // Permite que un usuario...
        $verProveedor->description  = 'Ver proveedores'; // optional
        $verProveedor->save();

        $verGeneralProveedor = new Permission();
        $verGeneralProveedor->name         = 'ver-general-proveedor';
        $verGeneralProveedor->display_name = 'Ver Listado Proveedor'; // optional
        // Permite que un usuario...
        $verGeneralProveedor->description  = 'Ver listado de proveedores'; // optional
        $verGeneralProveedor->save();


        /***************************************************************************************************************/

        $crearTipoDocumento = new Permission();
        $crearTipoDocumento->name         = 'crear-tipo-documento';
        $crearTipoDocumento->display_name = 'Crear Tipo Documento'; // optional
        // Permite que un usuario...
        $crearTipoDocumento->description  = 'Crear tipos de documentos'; // optional
        $crearTipoDocumento->save();

        $editarTipoDocumento = new Permission();
        $editarTipoDocumento->name         = 'editar-tipo-documento';
        $editarTipoDocumento->display_name = 'Editar Tipo Documento'; // optional
        // Permite que un usuario...
        $editarTipoDocumento->description  = 'Editar tipos de documentos'; // optional
        $editarTipoDocumento->save();

        $eliminarTipoDocumento = new Permission();
        $eliminarTipoDocumento->name         = 'eliminar-tipo-documento';
        $eliminarTipoDocumento->display_name = 'Eliminar Tipo Documento'; // optional
        // Permite que un usuario...
        $eliminarTipoDocumento->description  = 'Eliminar tipos de documentos'; // optional
        $eliminarTipoDocumento->save();

        $verTipoDocumento = new Permission();
        $verTipoDocumento->name         = 'ver-tipo-documento';
        $verTipoDocumento->display_name = 'Ver Tipo Documento'; // optional
        // Permite que un usuario...
        $verTipoDocumento->description  = 'Ver tipos de documentos'; // optional
        $verTipoDocumento->save();

        $verGeneralTipoDocumento = new Permission();
        $verGeneralTipoDocumento->name         = 'ver-general-tipo-documento';
        $verGeneralTipoDocumento->display_name = 'Ver Listado Tipo Documento'; // optional
        // Permite que un usuario...
        $verGeneralTipoDocumento->description  = 'Ver listado de tipos de documentos'; // optional
        $verGeneralTipoDocumento->save();

        /***************************************************************************************************************/

        $crearItemPresupuestario = new Permission();
        $crearItemPresupuestario->name         = 'crear-item-presupuestario';
        $crearItemPresupuestario->display_name = 'Crear Item Presupuestario'; // optional
        // Permite que un usuario...
        $crearItemPresupuestario->description  = 'Crear items presupuestarios'; // optional
        $crearItemPresupuestario->save();

        $editarItemPresupuestario = new Permission();
        $editarItemPresupuestario->name         = 'editar-item-presupuestario';
        $editarItemPresupuestario->display_name = 'Editar Item Presupuestario'; // optional
        // Permite que un usuario...
        $editarItemPresupuestario->description  = 'Editar items presupuestarios'; // optional
        $editarItemPresupuestario->save();

        $eliminarItemPresupuestario = new Permission();
        $eliminarItemPresupuestario->name         = 'eliminar-item-presupuestario';
        $eliminarItemPresupuestario->display_name = 'Eliminar Item Presupuestario'; // optional
        // Permite que un usuario...
        $eliminarItemPresupuestario->description  = 'Eliminar items presupuestarios'; // optional
        $eliminarItemPresupuestario->save();

        $verItemPresupuestario = new Permission();
        $verItemPresupuestario->name         = 'ver-item-presupuestario';
        $verItemPresupuestario->display_name = 'Ver Item Presupuestario'; // optional
        // Permite que un usuario...
        $verItemPresupuestario->description  = 'Ver items presupuestarios'; // optional
        $verItemPresupuestario->save();

        $verGeneralItemPresupuestario = new Permission();
        $verGeneralItemPresupuestario->name         = 'ver-general-item-presupuestario';
        $verGeneralItemPresupuestario->display_name = 'Ver Listado Item Presupuestario'; // optional
        // Permite que un usuario...
        $verGeneralItemPresupuestario->description  = 'Ver listado de items presupuestarios'; // optional
        $verGeneralItemPresupuestario->save();

        /***************************************************************************************************************/

         $crearModalidadCompra = new Permission();
         $crearModalidadCompra->name         = 'crear-modalidad-compra';
         $crearModalidadCompra->display_name = 'Crear Modalidad Compra'; // optional
         // Permite que un usuario...
         $crearModalidadCompra->description  = 'Crear modalidades de compra'; // optional
         $crearModalidadCompra->save();
 
         $editarModalidadCompra = new Permission();
         $editarModalidadCompra->name         = 'editar-modalidad-compra';
         $editarModalidadCompra->display_name = 'Editar Modalidad Compra'; // optional
         // Permite que un usuario...
         $editarModalidadCompra->description  = 'Editar modalidades de compra'; // optional
         $editarModalidadCompra->save();
 
         $eliminarModalidadCompra = new Permission();
         $eliminarModalidadCompra->name         = 'eliminar-modalidad-compra';
         $eliminarModalidadCompra->display_name = 'Eliminar Modalidad Compra'; // optional
         // Permite que un usuario...
         $eliminarModalidadCompra->description  = 'Eliminar modalidades de compra'; // optional
         $eliminarModalidadCompra->save();
 
         $verModalidadCompra = new Permission();
         $verModalidadCompra->name         = 'ver-modalidad-compra';
         $verModalidadCompra->display_name = 'Ver Modalidad Compra'; // optional
         // Permite que un usuario...
         $verModalidadCompra->description  = 'Ver modalidades de compra'; // optional
         $verModalidadCompra->save();
 
         $verGeneralModalidadCompra = new Permission();
         $verGeneralModalidadCompra->name         = 'ver-general-modalidad-compra';
         $verGeneralModalidadCompra->display_name = 'Ver Listado Modalidad Compra'; // optional
         // Permite que un usuario...
         $verGeneralModalidadCompra->description  = 'ver listado de modalidades de compra'; // optional
         $verGeneralModalidadCompra->save();

        /***************************************************************************************************************/

        $crearTipoAdjudicacion = new Permission();
        $crearTipoAdjudicacion->name         = 'crear-tipo-adjudicacion';
        $crearTipoAdjudicacion->display_name = 'Crear Tipo Adjudicacion'; // optional
        // Permite que un usuario...
        $crearTipoAdjudicacion->description  = 'Crear tipos de adjudicacion'; // optional
        $crearTipoAdjudicacion->save();

        $editarTipoAdjudicacion = new Permission();
        $editarTipoAdjudicacion->name         = 'editar-tipo-adjudicacion';
        $editarTipoAdjudicacion->display_name = 'Editar Tipo Adjudicacion'; // optional
        // Permite que un usuario...
        $editarTipoAdjudicacion->description  = 'Editar tipos de adjudicacion'; // optional
        $editarTipoAdjudicacion->save();

        $eliminarTipoAdjudicacion = new Permission();
        $eliminarTipoAdjudicacion->name         = 'eliminar-tipo-adjudicacion';
        $eliminarTipoAdjudicacion->display_name = 'Eliminar Tipo Adjudicacion'; // optional
        // Permite que un usuario...
        $eliminarTipoAdjudicacion->description  = 'Eliminar tipos de adjudicacion'; // optional
        $eliminarTipoAdjudicacion->save();

        $verTipoAdjudicacion = new Permission();
        $verTipoAdjudicacion->name         = 'ver-tipo-adjudicacion';
        $verTipoAdjudicacion->display_name = 'Ver Tipo Adjudicacion'; // optional
        // Permite que un usuario...
        $verTipoAdjudicacion->description  = 'Ver tipos de adjudicacion'; // optional
        $verTipoAdjudicacion->save();

        $verGeneralTipoAdjudicacion = new Permission();
        $verGeneralTipoAdjudicacion->name         = 'ver-general-tipo-adjudicacion';
        $verGeneralTipoAdjudicacion->display_name = 'Ver Listado Tipo Adjudicacion'; // optional
        // Permite que un usuario...
        $verGeneralTipoAdjudicacion->description  = 'Ver listado de tipos de adjudicacion'; // optional
        $verGeneralTipoAdjudicacion->save();

        /***************************************************************************************************************/

        $crearTipoInforme = new Permission();
        $crearTipoInforme->name         = 'crear-tipo-informe';
        $crearTipoInforme->display_name = 'Crear Tipo Informe'; // optional
        // Permite que un usuario...
        $crearTipoInforme->description  = 'Crear tipos de informe'; // optional
        $crearTipoInforme->save();

        $editarTipoInforme = new Permission();
        $editarTipoInforme->name         = 'editar-tipo-informe';
        $editarTipoInforme->display_name = 'Editar Tipo Informe'; // optional
        // Permite que un usuario...
        $editarTipoInforme->description  = 'Editar tipos de informe'; // optional
        $editarTipoInforme->save();

        $eliminarTipoInforme = new Permission();
        $eliminarTipoInforme->name         = 'eliminar-tipo-informe';
        $eliminarTipoInforme->display_name = 'Eliminar Tipo Informe'; // optional
        // Permite que un usuario...
        $eliminarTipoInforme->description  = 'Eliminar tipos de informe'; // optional
        $eliminarTipoInforme->save();

        $verTipoInforme = new Permission();
        $verTipoInforme->name         = 'ver-tipo-informe';
        $verTipoInforme->display_name = 'Ver Tipo Informe'; // optional
        // Permite que un usuario...
        $verTipoInforme->description  = 'Ver tipos de informe'; // optional
        $verTipoInforme->save();

        $verGeneralTipoInforme = new Permission();
        $verGeneralTipoInforme->name         = 'ver-general-tipo-informe';
        $verGeneralTipoInforme->display_name = 'Ver Listado Tipo Informe'; // optional
        // Permite que un usuario...
        $verGeneralTipoInforme->description  = 'Ver listado de tipos de informe'; // optional
        $verGeneralTipoInforme->save();

        /***************************************************************************************************************/

        $crearTipoArchivo = new Permission();
        $crearTipoArchivo->name         = 'crear-tipo-archivo';
        $crearTipoArchivo->display_name = 'Crear Tipo Archivo'; // optional
        // Permite que un usuario...
        $crearTipoArchivo->description  = 'Crear tipos de archivo'; // optional
        $crearTipoArchivo->save();

        $editarTipoArchivo = new Permission();
        $editarTipoArchivo->name         = 'editar-tipo-archivo';
        $editarTipoArchivo->display_name = 'Editar Tipo Archivo'; // optional
        // Permite que un usuario...
        $editarTipoArchivo->description  = 'Editar tipos de archivo'; // optional
        $editarTipoArchivo->save();

        $eliminarTipoArchivo = new Permission();
        $eliminarTipoArchivo->name         = 'eliminar-tipo-archivo';
        $eliminarTipoArchivo->display_name = 'Eliminar Tipo Archivo'; // optional
        // Permite que un usuario...
        $eliminarTipoArchivo->description  = 'Eliminar tipos de archivo'; // optional
        $eliminarTipoArchivo->save();

        $verTipoArchivo = new Permission();
        $verTipoArchivo->name         = 'ver-tipo-archivo';
        $verTipoArchivo->display_name = 'Ver Tipo Archivo'; // optional
        // Permite que un usuario...
        $verTipoArchivo->description  = 'Ver tipos de archivo'; // optional
        $verTipoArchivo->save();

        $verGeneralTipoArchivo = new Permission();
        $verGeneralTipoArchivo->name         = 'ver-general-tipo-archivo';
        $verGeneralTipoArchivo->display_name = 'Ver Listado Tipo Archivo'; // optional
        // Permite que un usuario...
        $verGeneralTipoArchivo->description  = 'Ver listado de tipos de archivo'; // optional
        $verGeneralTipoArchivo->save();

        /***************************************************************************************************************/

        $crearReferenteTecnico = new Permission();
        $crearReferenteTecnico->name         = 'crear-referente-tecnico';
        $crearReferenteTecnico->display_name = 'Crear Referente Tecnico'; // optional
        // Permite que un usuario...
        $crearReferenteTecnico->description  = 'Crear referentes tecnicos'; // optional
        $crearReferenteTecnico->save();

        $editarReferenteTecnico = new Permission();
        $editarReferenteTecnico->name         = 'editar-referente-tecnico';
        $editarReferenteTecnico->display_name = 'Editar Referente Tecnico'; // optional
        // Permite que un usuario...
        $editarReferenteTecnico->description  = 'Editar referentes tecnicos'; // optional
        $editarReferenteTecnico->save();

        $eliminarReferenteTecnico = new Permission();
        $eliminarReferenteTecnico->name         = 'eliminar-referente-tecnico';
        $eliminarReferenteTecnico->display_name = 'Eliminar Referente Tecnico'; // optional
        // Permite que un usuario...
        $eliminarReferenteTecnico->description  = 'Eliminar referentes tecnicos'; // optional
        $eliminarReferenteTecnico->save();

        $verReferenteTecnico = new Permission();
        $verReferenteTecnico->name         = 'ver-referente-tecnico';
        $verReferenteTecnico->display_name = 'Ver Referente Tecnico'; // optional
        // Permite que un usuario...
        $verReferenteTecnico->description  = 'Ver referentes tecnicos'; // optional
        $verReferenteTecnico->save();

        $verGeneralReferenteTecnico = new Permission();
        $verGeneralReferenteTecnico->name         = 'ver-general-referente-tecnico';
        $verGeneralReferenteTecnico->display_name = 'Ver Listado Referente Tecnico'; // optional
        // Permite que un usuario...
        $verGeneralReferenteTecnico->description  = 'Ver listado de referentes tecnicos'; // optional
        $verGeneralReferenteTecnico->save();

        /***************************************************************************************************************/
        /**Documentos */

        $crearDocumento = new Permission();
        $crearDocumento->name         = 'crear-documento';
        $crearDocumento->display_name = 'Crear Documento'; // optional
        // Permite que un usuario...
        $crearDocumento->description  = 'Crear documentos'; // optional
        $crearDocumento->save();

        $editarDocumento = new Permission();
        $editarDocumento->name         = 'editar-documento';
        $editarDocumento->display_name = 'Editar Documento'; // optional
        // Permite que un usuario...
        $editarDocumento->description  = 'Editar documentos'; // optional
        $editarDocumento->save();

        $eliminarDocumento = new Permission();
        $eliminarDocumento->name         = 'eliminar-documento';
        $eliminarDocumento->display_name = 'Eliminar Documento'; // optional
        // Permite que un usuario...
        $eliminarDocumento->description  = 'Eliminar documentos'; // optional
        $eliminarDocumento->save();

        $verDocumento = new Permission();
        $verDocumento->name         = 'ver-documento';
        $verDocumento->display_name = 'Ver Documento'; // optional
        // Permite que un usuario...
        $verDocumento->description  = 'Ver documentos'; // optional
        $verDocumento->save();

        $verGeneralDocumento = new Permission();
        $verGeneralDocumento->name         = 'ver-general-documento';
        $verGeneralDocumento->display_name = 'Ver Listado Documentos'; // optional
        // Permite que un usuario...
        $verGeneralDocumento->description  = 'Ver listado general de documentos'; // optional
        $verGeneralDocumento->save();

        $verRecepcionPorConfirmar = new Permission();
        $verRecepcionPorConfirmar->name         = 'ver-documentos-con-recepcion-por-confirmar';
        $verRecepcionPorConfirmar->display_name = 'Ver Documentos con Recepcion por Confirmar'; // optional
        // Permite que un usuario...
        $verRecepcionPorConfirmar->description  = 'Ver listado de documentos con recepción por confirmar'; // optional
        $verRecepcionPorConfirmar->save();

        $matchDocumentoConRecepcion = new Permission();
        $matchDocumentoConRecepcion->name         = 'match-documentos-con-recepcion';
        $matchDocumentoConRecepcion->display_name = 'Match Documentos con Recepcion'; // optional
        // Permite que un usuario...
        $matchDocumentoConRecepcion->description  = 'Confirmar Match de documento con recepción'; // optional
        $matchDocumentoConRecepcion->save();

        //////
        $verRecepcionConfirmada = new Permission();
        $verRecepcionConfirmada->name         = 'ver-documentos-con-recepcion-confirmada';
        $verRecepcionConfirmada->display_name = 'Ver Documentos con Recepcion Confirmada'; // optional
        // Permite que un usuario...
        $verRecepcionConfirmada->description  = 'Ver listado de documentos con recepción confirmada'; // optional
        $verRecepcionConfirmada->save();

        $desvincularDocumentoConRecepcion = new Permission();
        $desvincularDocumentoConRecepcion->name         = 'desvincular-documentos-con-recepcion';
        $desvincularDocumentoConRecepcion->display_name = 'Desvincular Documentos con Recepcion'; // optional
        // Permite que un usuario...
        $desvincularDocumentoConRecepcion->description  = 'Desvincular Match de documento con recepción'; // optional
        $desvincularDocumentoConRecepcion->save();

        //////
        $verDocumentosSinRecepcion = new Permission();
        $verDocumentosSinRecepcion->name         = 'ver-documentos-sin-recepcion';
        $verDocumentosSinRecepcion->display_name = 'Ver Documentos sin Recepcion'; // optional
        // Permite que un usuario...
        $verDocumentosSinRecepcion->description  = 'Ver listado de documentos sin recepción'; // optional
        $verDocumentosSinRecepcion->save();

        //////
        $verDocumentosPendienteCuadratura = new Permission();
        $verDocumentosPendienteCuadratura->name         = 'ver-documentos-pendiente-cuadratura';
        $verDocumentosPendienteCuadratura->display_name = 'Ver Documentos pendientes de cuadratura'; // optional
        // Permite que un usuario...
        $verDocumentosPendienteCuadratura->description  = 'Ver Documentos pendientes de cuadratura'; // optional
        $verDocumentosPendienteCuadratura->save();

        $cuadrarDocumento = new Permission();
        $cuadrarDocumento->name         = 'cuadrar-documento';
        $cuadrarDocumento->display_name = 'Cuadrar Documento'; // optional
        // Permite que un usuario...
        $cuadrarDocumento->description  = 'Cuadre un documento mediante validación'; // optional
        $cuadrarDocumento->save();

        //////
        $verDocumentosCuadrados = new Permission();
        $verDocumentosCuadrados->name         = 'ver-documentos-cuadrados';
        $verDocumentosCuadrados->display_name = 'Ver Documentos cuadrados'; // optional
        // Permite que un usuario...
        $verDocumentosCuadrados->description  = 'Ver Documentos cuadrados'; // optional
        $verDocumentosCuadrados->save();

        $invalidarDocumento = new Permission();
        $invalidarDocumento->name         = 'invalidar-documento';
        $invalidarDocumento->display_name = 'Invalidar Documento'; // optional
        // Permite que un usuario...
        $invalidarDocumento->description  = 'Invalida un documento cuadrado'; // optional
        $invalidarDocumento->save();

        /***************************************************************************************************************************** */
        /*Documentos Bodega(Recepcion)*/
        //////
        $verGeneralDocumentoRecepcionado = new Permission();
        $verGeneralDocumentoRecepcionado->name         = 'ver-general-documento-recepcionado';
        $verGeneralDocumentoRecepcionado->display_name = 'Ver Listado Documentos Recepcionado'; // optional
        // Permite que un usuario...
        $verGeneralDocumentoRecepcionado->description  = 'Ver listado documentos recepcionado'; // optional
        $verGeneralDocumentoRecepcionado->save();

        $verDocumentosRecepcionados = new Permission();
        $verDocumentosRecepcionados->name         = 'ver-documentos-recepcionados';
        $verDocumentosRecepcionados->display_name = 'Ver Documentos recepcionados'; // optional
        // Permite que un usuario...
        $verDocumentosRecepcionados->description  = 'Ver Documentos recepcionados'; // optional
        $verDocumentosRecepcionados->save();

        $verArchivoDocumentoRecepcionado = new Permission();
        $verArchivoDocumentoRecepcionado->name         = 'ver-archivo-documento';
        $verArchivoDocumentoRecepcionado->display_name = 'Ver Archivo Documento'; // optional
        // Permite que un usuario...
        $verArchivoDocumentoRecepcionado->description  = 'Ver archivo Documento'; // optional
        $verArchivoDocumentoRecepcionado->save();

        $verGeneralRecepcionSinDocumento = new Permission();
        $verGeneralRecepcionSinDocumento->name         = 'ver-general-recepcion-sin-documento';
        $verGeneralRecepcionSinDocumento->display_name = 'Ver Listado Recepcion sin Documento'; // optional
        // Permite que un usuario...
        $verGeneralRecepcionSinDocumento->description  = 'Ver listado recepcion sin documento'; // optional
        $verGeneralRecepcionSinDocumento->save();

        $comentarErrorRecepcion = new Permission();
        $comentarErrorRecepcion->name         = 'comentar-error-recepcion';
        $comentarErrorRecepcion->display_name = 'Comentrar error sin recepción'; // optional
        // Permite que un usuario...
        $comentarErrorRecepcion->description  = 'Comentrar error sin recepción'; // optional
        $comentarErrorRecepcion->save();
        /***************************************************************************************************************************** */
        /*Acepta */
        $verGeneralExcelAcepta = new Permission();
        $verGeneralExcelAcepta->name         = 'ver-general-excel-acepta';
        $verGeneralExcelAcepta->display_name = 'Ver Listado general excel acepta'; // optional
        // Permite que un usuario...
        $verGeneralExcelAcepta->description  = 'Ver listado general excel acepta'; // optional
        $verGeneralExcelAcepta->save();

        $verExcelAcepta = new Permission();
        $verExcelAcepta->name         = 'ver-excel-acepta';
        $verExcelAcepta->display_name = 'Ver excel acepta'; // optional
        // Permite que un usuario...
        $verExcelAcepta->description  = 'Ver excel acepta'; // optional
        $verExcelAcepta->save();

        ///
        $verGrillaAcepta = new Permission();
        $verGrillaAcepta->name         = 'ver-grilla-acepta';
        $verGrillaAcepta->display_name = 'Ver Grilla Acepta'; // optional
        // Permite que un usuario...
        $verGrillaAcepta->description  = 'Ver grilla acepta'; // optional
        $verGrillaAcepta->save();

        $verFacturaDocumentoAcepta = new Permission();
        $verFacturaDocumentoAcepta->name         = 'ver-factura-documento-acepta';
        $verFacturaDocumentoAcepta->display_name = 'Ver Factura Documento Acepta'; // optional
        // Permite que un usuario...
        $verFacturaDocumentoAcepta->description  = 'Ver factura documento acepta'; // optional
        $verFacturaDocumentoAcepta->save();

        $cargaDocumentoAceptaAlSistema = new Permission();
        $cargaDocumentoAceptaAlSistema->name         = 'carga-documento-acepta-al-sistema';
        $cargaDocumentoAceptaAlSistema->display_name = 'Carga Documento Acepta al Sistema'; // optional
        // Permite que un usuario...
        $cargaDocumentoAceptaAlSistema->description  = 'Carga documento acepta al sistema'; // optional
        $cargaDocumentoAceptaAlSistema->save();

        $verListadoAceptaConRecepcion = new Permission();
        $verListadoAceptaConRecepcion->name         = 'ver-listado-acepta-con-recepcion';
        $verListadoAceptaConRecepcion->display_name = 'Ver Listado Acepta con Recepcion'; // optional
        // Permite que un usuario...
        $verListadoAceptaConRecepcion->description  = 'Ver listado acepta con recepcion'; // optional
        $verListadoAceptaConRecepcion->save();

        $posibleConexion = new Permission();
        $posibleConexion->name         = 'ver-posible-conexion-acepta-recepcion';
        $posibleConexion->display_name = 'Ver Posible Conexion'; // optional
        // Permite que un usuario...
        $posibleConexion->description  = 'Ver posible conexion del documento acepta con alguna recepcion'; // optional
        $posibleConexion->save();

        /***************************************************************************************************************************** */
        /*Factoring */
        $cargaIndidualFactoring = new Permission();
        $cargaIndidualFactoring->name         = 'carga-individual-factoring';
        $cargaIndidualFactoring->display_name = 'Carga Individual del factoring'; // optional
        // Permite que un usuario...
        $cargaIndidualFactoring->description  = 'Carga individual de factoring'; // optional
        $cargaIndidualFactoring->save();

        $cargaMasivaFactoring = new Permission();
        $cargaMasivaFactoring->name         = 'carga-masiva-factoring';
        $cargaMasivaFactoring->display_name = 'Carga Marsiva del factoring'; // optional
        // Permite que un usuario...
        $cargaMasivaFactoring->description  = 'Carga Marsiva de factoring'; // optional
        $cargaMasivaFactoring->save();

        $grillaFactoring = new Permission();
        $grillaFactoring->name         = 'grilla-factoring';
        $grillaFactoring->display_name = 'Grilla factoring'; // optional
        // Permite que un usuario...
        $grillaFactoring->description  = 'Grilla factoring'; // optional
        $grillaFactoring->save();

        $eliminarFactoring = new Permission();
        $eliminarFactoring->name         = 'eliminar-factoring';
        $eliminarFactoring->display_name = 'Eliminar Factoring'; // optional
        // Permite que un usuario...
        $eliminarFactoring->description  = 'Eliminar Factoring'; // optional
        $eliminarFactoring->save();

        $grillaNoFactura = new Permission();
        $grillaNoFactura->name         = 'grilla-no-factura';
        $grillaNoFactura->display_name = 'Grilla no Factura'; // optional
        // Permite que un usuario...
        $grillaNoFactura->description  = 'Grilla no Factura'; // optional
        $grillaNoFactura->save();

        $verFactoring = new Permission();
        $verFactoring->name         = 'ver-factoring';
        $verFactoring->display_name = 'Ver Factoring'; // optional
        // Permite que un usuario...
        $verFactoring->description  = 'Ver Factoring'; // optional
        $verFactoring->save();

        $editarFactoring = new Permission();
        $editarFactoring->name         = 'editar-factoring';
        $editarFactoring->display_name = 'Editar Factoring'; // optional
        // Permite que un usuario...
        $editarFactoring->description  = 'Editar Factoring'; // optional
        $editarFactoring->save();

        $grillaNoFactoringPAgado = new Permission();
        $grillaNoFactoringPAgado->name         = 'grilla-factoring-pagado';
        $grillaNoFactoringPAgado->display_name = 'Grilla Factoring Pagado'; // optional
        // Permite que un usuario...
        $grillaNoFactoringPAgado->description  = 'Grilla Factoring Pagado'; // optional
        $grillaNoFactoringPAgado->save();

        /***************************************************************************************************************************** */
        /* Devengo */
        $asignarParaDevengar = new Permission();
        $asignarParaDevengar->name         = 'asignar-por-devengar';
        $asignarParaDevengar->display_name = 'Asignar por devengar'; // optional
        // Permite que un usuario...
        $asignarParaDevengar->description  = 'Asignar documentos para devengar'; // optional
        $asignarParaDevengar->save();

        $reasignarParaDevengar = new Permission();
        $reasignarParaDevengar->name         = 'reasignar-por-devengar';
        $reasignarParaDevengar->display_name = 'Reasignar por devengar'; // optional
        // Permite que un usuario...
        $reasignarParaDevengar->description  = 'Reasignar documentos para devengar'; // optional
        $reasignarParaDevengar->save();

        $listadoPorDevengar = new Permission();
        $listadoPorDevengar->name         = 'listado-por-devengar';
        $listadoPorDevengar->display_name = 'Listado por devengar'; // optional
        // Permite que un usuario...
        $listadoPorDevengar->description  = 'Ver el listado de documentos por devengar'; // optional
        $listadoPorDevengar->save();

        $Devengar = new Permission();
        $Devengar->name         = 'devengar-documento';
        $Devengar->display_name = 'devengar documento'; // optional
        // Permite que un usuario...
        $Devengar->description  = 'Devengar Documentos'; // optional
        $Devengar->save();

        $listadoDevengado = new Permission();
        $listadoDevengado->name         = 'listado-devengado';
        $listadoDevengado->display_name = 'Listado devengado'; // optional
        // Permite que un usuario...
        $listadoDevengado->description  = 'Ver el listado de documentos devengados'; // optional
        $listadoDevengado->save();

        $editarDevengo = new Permission();
        $editarDevengo->name         = 'editar-devengo';
        $editarDevengo->display_name = 'editar Devengo'; // optional
        // Permite que un usuario...
        $editarDevengo->description  = 'Ver el listado de documentos devengados'; // optional
        $editarDevengo->save();

        /***************************************************************************************************************************** */
        /* Referente Tecnico, opciones */
        $solicitarVistoBueno = new Permission();
        $solicitarVistoBueno->name         = 'solicitar-visto-bueno';
        $solicitarVistoBueno->display_name = 'solicitar visto bueno'; // optional
        // Permite que un usuario...
        $solicitarVistoBueno->description  = 'Solicite un visto bueno a referente tecnico'; // optional
        $solicitarVistoBueno->save();

        $verMemoSolicitarVistoBueno = new Permission();
        $verMemoSolicitarVistoBueno->name         = 'ver-memo-solicitud-visto-bueno';
        $verMemoSolicitarVistoBueno->display_name = 'ver memo solicitud visto bueno'; // optional
        // Permite que un usuario...
        $verMemoSolicitarVistoBueno->description  = 'Vea Memo solicitud de visto bueno a referente tecnico'; // optional
        $verMemoSolicitarVistoBueno->save();

        $editarSolicitudVistoBueno = new Permission();
        $editarSolicitudVistoBueno->name         = 'editar-solicitud-visto-bueno';
        $editarSolicitudVistoBueno->display_name = 'editar solicitud visto bueno'; // optional
        // Permite que un usuario...
        $editarSolicitudVistoBueno->description  = 'Editar solicitud visto bueno a referente tecnico'; // optional
        $editarSolicitudVistoBueno->save();

        $registroVistoBueno = new Permission();
        $registroVistoBueno->name         = 'registro-visto-bueno';
        $registroVistoBueno->display_name = 'registro visto bueno'; // optional
        // Permite que un usuario...
        $registroVistoBueno->description  = 'Registre un visto bueno a una solicitud'; // optional
        $registroVistoBueno->save();

        $editarRegistroVistoBueno = new Permission();
        $editarRegistroVistoBueno->name         = 'editar-registro-visto-bueno';
        $editarRegistroVistoBueno->display_name = 'editar registro visto bueno'; // optional
        // Permite que un usuario...
        $editarRegistroVistoBueno->description  = 'Editar un visto bueno'; // optional
        $editarRegistroVistoBueno->save();
        /***********************************************************************************************************/
        /**
         * Se crean los perfiles
         */
        $propietario = new Role();
        $propietario->name         = 'propietario';
        $propietario->display_name = 'Propietario Sistema'; // optional
        // $propietario->description  = 'User is the owner of a given project'; // optional
        $propietario->save();

        $aministrador = new Role();
        $aministrador->name         = 'administrador';
        $aministrador->display_name = 'Administrador'; // optional
        // $admin->description  = 'User is allowed to manage and edit other users'; // optional
        $aministrador->save();

        $referente_tecnico = new Role();
        $referente_tecnico->name         = 'referente-tecnico';
        $referente_tecnico->display_name = 'Referente Técnico'; // optional
        // $referente_tecnico->description  = 'User is the owner of a given project'; // optional
        $referente_tecnico->save();

        $administrador_devengo = new Role();
        $administrador_devengo->name         = 'administrador-devengo';
        $administrador_devengo->display_name = 'Administrador Devengo'; // optional
        // $administrador_devengo->description  = 'User is the owner of a given project'; // optional
        $administrador_devengo->save();

        $userGeneral = new Role();
        $userGeneral->name         = 'usuario-general';
        $userGeneral->display_name = 'Usuario General'; // optional
        // $userGeneral->description  = 'User is the owner of a given project'; // optional
        $userGeneral->save();

        $visita = new Role();
        $visita->name         = 'visita';
        $visita->display_name = 'Visita'; // optional
        // $visita->description  = 'User is the owner of a given project'; // optional
        $visita->save();

        /***********************************************************************************************************/
        /**
         * Se les pasa el perfil a usuarios
         */

        $usarioPropietario = User::findorfail(1); //Informatica
        $usarioPropietario->attachRole($propietario);

        $usuarioAdministrador = User::findorfail(18); //Rodrigo
        $usuarioAdministrador->attachRole($aministrador);
        $usuarioAdministrador = User::findorfail(33); //Claudia Araya
        $usuarioAdministrador->attachRole($aministrador);
        $usuariosRef = User::whereHas('getReferenteTecnico')->whereNotIn('id',[1,18,33,13,16])->get();
        foreach ( $usuariosRef as $userRef ) {
            $userRef->attachRole($referente_tecnico);
        }

        $usuarioAdministradorDevengo = User::findOrFail(13);
        $usuarioAdministradorDevengo->attachRole($administrador_devengo);
        $usuarioAdministradorDevengo = User::findOrFail(16);
        $usuarioAdministradorDevengo->attachRole($administrador_devengo);

        $usuariosGen = User::doesntHave('getReferenteTecnico')->whereNotIn('id',[1,18,33,13,16])->get();
        foreach ( $usuariosGen as $userGen ) {
            $userGen->attachRole($userGeneral);
        }

        /***********************************************************************************************************/
        /**
         * Se les pasan permisos a los perfiles
         */
        // Referente Tecnico
        $referente_tecnico->attachPermissions(array( 87, 89, 90)); // Permisos Referente tecnico (opciones)
        $referente_tecnico->attachPermissions(array( 49, 50)); // Permisos Documentos

        // Administrador Devengo
        $administrador_devengo->attachPermissions(array( 4, 5)); //Permisos Usuario
        $administrador_devengo->attachPermissions(array( 9, 10)); // Permisos Proveedor
        $administrador_devengo->attachPermissions(array( 14, 15)); // Permisos Tipo Documento
        $administrador_devengo->attachPermissions(array( 19, 20)); // Permisos Item Presupuestario
        $administrador_devengo->attachPermissions(array( 24, 25)); // Permisos Modalidad de compra
        $administrador_devengo->attachPermissions(array( 29, 30)); // Permisos Tipo Adjudicacion
        $administrador_devengo->attachPermissions(array( 34, 35)); // Permisos Tipo Informe
        $administrador_devengo->attachPermissions(array( 39, 40)); // Permisos Tipo Archivo
        $administrador_devengo->attachPermissions(array( 44, 45)); // Permisos Referente Tecnico
        $administrador_devengo->attachPermissions(array(46, 47, 49, 50)); // Permisos Documentos
        $administrador_devengo->attachPermissions(array(51, 52)); // Permisos Documentos con Recepcion por confirmar
        $administrador_devengo->attachPermissions(array(53, 54)); // Permisos Documentos con Recepcion confirmada
        $administrador_devengo->attachPermissions(array(55, 56, 57, 58, 59)); // Permisos Documentos sin recepcion,pendiente cuadratura,cuadrar,cuadrados e invalidar
        $administrador_devengo->attachPermissions(array(60, 61, 62, 63, 64)); // Permisos Documentos Bodega
        $administrador_devengo->attachPermissions(array(65, 66, 67, 68, 69, 70, 71)); // Permisos Documentos Acepta
        $administrador_devengo->attachPermissions(array(72, 73, 74, 75, 76, 77, 78, 79)); // Permisos Factoring
        $administrador_devengo->attachPermissions(array(80, 81, 82, 83, 84, 85)); // Permisos Devengo
        $administrador_devengo->attachPermissions(array(86, 87, 88, 89, 90)); // Permisos Referente tecnico (opciones)

        // Usuario General Finanzas
        $userGeneral->attachPermissions(array( 4, 5)); //Permisos Usuario
        $userGeneral->attachPermissions(array( 9, 10)); // Permisos Proveedor
        $userGeneral->attachPermissions(array( 14, 15)); // Permisos Tipo Documento
        $userGeneral->attachPermissions(array( 19, 20)); // Permisos Item Presupuestario
        $userGeneral->attachPermissions(array( 24, 25)); // Permisos Modalidad de compra
        $userGeneral->attachPermissions(array( 29, 30)); // Permisos Tipo Adjudicacion
        $userGeneral->attachPermissions(array( 34, 35)); // Permisos Tipo Informe
        $userGeneral->attachPermissions(array( 39, 40)); // Permisos Tipo Archivo
        $userGeneral->attachPermissions(array( 44, 45)); // Permisos Referente Tecnico
        $userGeneral->attachPermissions(array(46, 47, 49, 50)); // Permisos Documentos
        $userGeneral->attachPermissions(array(51, 52)); // Permisos Documentos con Recepcion por confirmar
        $userGeneral->attachPermissions(array(53, 54)); // Permisos Documentos con Recepcion confirmada
        $userGeneral->attachPermissions(array(55, 56, 57, 58, 59)); // Permisos Documentos sin recepcion,pendiente cuadratura,cuadrar,cuadrados e invalidar
        $userGeneral->attachPermissions(array(60, 61, 62, 63, 64)); // Permisos Documentos Bodega
        $userGeneral->attachPermissions(array(65, 66, 67, 68, 69, 70, 71)); // Permisos Documentos Acepta
        $userGeneral->attachPermissions(array(72, 73, 74, 75, 76, 77, 78, 79)); // Permisos Factoring
        $userGeneral->attachPermissions(array( 82, 83, 84, 85)); // Permisos Devengo
        $userGeneral->attachPermissions(array(86, 87, 88, 89, 90)); // Permisos Referente tecnico (opciones)

        // Visita
        $visita->attachPermissions(array( 4, 5)); //Permisos Usuario
        $visita->attachPermissions(array( 9, 10)); // Permisos Proveedor
        $visita->attachPermissions(array( 14, 15)); // Permisos Tipo Documento
        $visita->attachPermissions(array( 19, 20)); // Permisos Item Presupuestario
        $visita->attachPermissions(array( 24, 25)); // Permisos Modalidad de compra
        $visita->attachPermissions(array( 29, 30)); // Permisos Tipo Adjudicacion
        $visita->attachPermissions(array( 34, 35)); // Permisos Tipo Informe
        $visita->attachPermissions(array( 39, 40)); // Permisos Tipo Archivo
        $visita->attachPermissions(array( 44, 45)); // Permisos Referente Tecnico
        $visita->attachPermissions(array( 49, 50)); // Permisos Documentos
        $visita->attachPermissions(array( 51)); // Permisos Documentos con Recepcion por confirmar
        $visita->attachPermissions(array( 53)); // Permisos Documentos con Recepcion confirmada
        $visita->attachPermissions(array( 55, 56, 58)); // Permisos Documentos sin recepcion,pendiente cuadratura,cuadrar,cuadrados e invalidar
        $visita->attachPermissions(array( 60, 61, 62, 63)); // Permisos Documentos Bodega
        $visita->attachPermissions(array( 65, 66, 67, 68, 70, 71)); // Permisos Documentos Acepta
        $visita->attachPermissions(array( 74, 76, 77, 79)); // Permisos Factoring
        $visita->attachPermissions(array( 82, 84)); // Permisos Devengo
        $visita->attachPermissions(array( 87)); // Permisos Referente tecnico (opciones)

        // Propietario
        /*Permisos de mantenedores */
        $propietario->attachPermissions(array(1, 2, 3, 4, 5)); //Permisos Usuario
        $propietario->attachPermissions(array(6, 7, 8, 9, 10)); // Permisos Proveedor
        $propietario->attachPermissions(array(11, 12, 13, 14, 15)); // Permisos Tipo Documento
        $propietario->attachPermissions(array(16, 17, 18, 19, 20)); // Permisos Item Presupuestario
        $propietario->attachPermissions(array(21, 22, 23, 24, 25)); // Permisos Modalidad de compra
        $propietario->attachPermissions(array(26, 27, 28, 29, 30)); // Permisos Tipo Adjudicacion
        $propietario->attachPermissions(array(31, 32, 33, 34, 35)); // Permisos Tipo Informe
        $propietario->attachPermissions(array(36, 37, 38, 39, 40)); // Permisos Tipo Archivo
        $propietario->attachPermissions(array(41, 42, 43, 44, 45)); // Permisos Referente Tecnico
        /* Documentos y funcionalidades del sistema */
        $propietario->attachPermissions(array(46, 47, 48, 49, 50)); // Permisos Documentos
        $propietario->attachPermissions(array(51, 52)); // Permisos Documentos con Recepcion por confirmar
        $propietario->attachPermissions(array(53, 54)); // Permisos Documentos con Recepcion confirmada
        $propietario->attachPermissions(array(55, 56, 57, 58, 59)); // Permisos Documentos sin recepcion,pendiente cuadratura,cuadrar,cuadrados e invalidar
        $propietario->attachPermissions(array(60, 61, 62, 63, 64)); // Permisos Documentos Bodega
        $propietario->attachPermissions(array(65, 66, 67, 68, 69, 70, 71)); // Permisos Documentos Acepta
        $propietario->attachPermissions(array(72, 73, 74, 75, 76, 77, 78, 79)); // Permisos Factoring
        $propietario->attachPermissions(array(80, 81, 82, 83, 84, 85)); // Permisos Devengo
        $propietario->attachPermissions(array(86, 87, 88, 89, 90)); // Permisos Referente tecnico (opciones)

        // Administrador
        /*Permisos de mantenedores */
        $aministrador->attachPermissions(array(1, 2, 3, 4, 5)); //Permisos Usuario
        $aministrador->attachPermissions(array(6, 7, 8, 9, 10)); // Permisos Proveedor
        $aministrador->attachPermissions(array(11, 12, 13, 14, 15)); // Permisos Tipo Documento
        $aministrador->attachPermissions(array(16, 17, 18, 19, 20)); // Permisos Item Presupuestario
        $aministrador->attachPermissions(array(21, 22, 23, 24, 25)); // Permisos Modalidad de compra
        $aministrador->attachPermissions(array(26, 27, 28, 29, 30)); // Permisos Tipo Adjudicacion
        $aministrador->attachPermissions(array(31, 32, 33, 34, 35)); // Permisos Tipo Informe
        $aministrador->attachPermissions(array(36, 37, 38, 39, 40)); // Permisos Tipo Archivo
        $aministrador->attachPermissions(array(41, 42, 43, 44, 45)); // Permisos Referente Tecnico
        /* Documentos y funcionalidades del sistema */
        $aministrador->attachPermissions(array(46, 47, 48, 49, 50)); // Permisos Documentos
        $aministrador->attachPermissions(array(51, 52)); // Permisos Documentos con Recepcion por confirmar
        $aministrador->attachPermissions(array(53, 54)); // Permisos Documentos con Recepcion confirmada
        $aministrador->attachPermissions(array(55, 56, 57, 58, 59)); // Permisos Documentos sin recepcion,pendiente cuadratura,cuadrar,cuadrados e invalidar
        $aministrador->attachPermissions(array(60, 61, 62, 63, 64)); // Permisos Documentos Bodega
        $aministrador->attachPermissions(array(65, 66, 67, 68, 69, 70, 71)); // Permisos Documentos Acepta
        $aministrador->attachPermissions(array(72, 73, 74, 75, 76, 77, 78, 79)); // Permisos Factoring
        $aministrador->attachPermissions(array(80, 81, 82, 83, 84, 85)); // Permisos Devengo
        $aministrador->attachPermissions(array(86, 87, 88, 89, 90)); // Permisos Referente tecnico (opciones)
    }
}
