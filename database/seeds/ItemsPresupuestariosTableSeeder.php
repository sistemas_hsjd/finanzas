<?php

use Illuminate\Database\Seeder;
use App\ItemPresupuestario;

class ItemsPresupuestariosTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $newItem = new ItemPresupuestario();
        $newItem->titulo = "22";
        $newItem->subtitulo = "05";
        $newItem->item = "001";
        $newItem->clasificador_presupuestario = "Electricidad";
        $newItem->id_clasificacion = "1";
        $newItem->save();

        $newItem = new ItemPresupuestario();
        $newItem->titulo = "22";
        $newItem->subtitulo = "05";
        $newItem->item = "002";
        $newItem->clasificador_presupuestario = "Agua";
        $newItem->id_clasificacion = "1";
        $newItem->save();

        $newItem = new ItemPresupuestario();
        $newItem->titulo = "22";
        $newItem->subtitulo = "05";
        $newItem->item = "003";
        $newItem->clasificador_presupuestario = "Gas";
        $newItem->id_clasificacion = "1";
        $newItem->save();

        $newItem = new ItemPresupuestario();
        $newItem->titulo = "22";
        $newItem->subtitulo = "05";
        $newItem->item = "004";
        $newItem->clasificador_presupuestario = "Correo";
        $newItem->id_clasificacion = "1";
        $newItem->save();

        $newItem = new ItemPresupuestario();
        $newItem->titulo = "22";
        $newItem->subtitulo = "05";
        $newItem->item = "005";
        $newItem->clasificador_presupuestario = "Telefonía Fija";
        $newItem->id_clasificacion = "1";
        $newItem->save();

        $newItem = new ItemPresupuestario();
        $newItem->titulo = "22";
        $newItem->subtitulo = "05";
        $newItem->item = "006";
        $newItem->clasificador_presupuestario = "Telefonía Celular";
        $newItem->id_clasificacion = "1";
        $newItem->save();

        $newItem = new ItemPresupuestario();
        $newItem->titulo = "22";
        $newItem->subtitulo = "05";
        $newItem->item = "007";
        $newItem->clasificador_presupuestario = "Acceso a Internet";
        $newItem->id_clasificacion = "1";
        $newItem->save();

        $newItem = new ItemPresupuestario();
        $newItem->titulo = "22";
        $newItem->subtitulo = "05";
        $newItem->item = "008";
        $newItem->clasificador_presupuestario = "Enlaces de Telecomunicaciones";
        $newItem->id_clasificacion = "1";
        $newItem->save();

        $newItem = new ItemPresupuestario();
        $newItem->titulo = "22";
        $newItem->subtitulo = "05";
        $newItem->item = "009";
        $newItem->clasificador_presupuestario = "Otros";
        $newItem->id_clasificacion = "1";
        $newItem->save();

        $newItem = new ItemPresupuestario();
        $newItem->titulo = "22";
        $newItem->subtitulo = "08";
        $newItem->item = "001";
        $newItem->clasificador_presupuestario = "Servicios de Aseo";
        $newItem->id_clasificacion = "1";
        $newItem->save();

        $newItem = new ItemPresupuestario();
        $newItem->titulo = "22";
        $newItem->subtitulo = "08";
        $newItem->item = "002";
        $newItem->clasificador_presupuestario = "Servicios de Vigilancia";
        $newItem->id_clasificacion = "1";
        $newItem->save();

        $newItem = new ItemPresupuestario();
        $newItem->titulo = "22";
        $newItem->subtitulo = "08";
        $newItem->item = "003";
        $newItem->clasificador_presupuestario = "Servicio de mantención de jardines";
        $newItem->id_clasificacion = "1";
        $newItem->save();

        $newItem = new ItemPresupuestario();
        $newItem->titulo = "22";
        $newItem->subtitulo = "08";
        $newItem->item = "007";
        $newItem->clasificador_presupuestario = "Pasajes, Fletes y Bodegajes";
        $newItem->id_clasificacion = "1";
        $newItem->save();

        $newItem = new ItemPresupuestario();
        $newItem->titulo = "22";
        $newItem->subtitulo = "08";
        $newItem->item = "008";
        $newItem->clasificador_presupuestario = "Salas Cunas y/o Jardines Infantiles";
        $newItem->id_clasificacion = "1";
        $newItem->save();

        $newItem = new ItemPresupuestario();
        $newItem->titulo = "22";
        $newItem->subtitulo = "08";
        $newItem->item = "009";
        $newItem->clasificador_presupuestario = "Servicios de Pago y  Cobranza";
        $newItem->id_clasificacion = "1";
        $newItem->save();

        $newItem = new ItemPresupuestario();
        $newItem->titulo = "22";
        $newItem->subtitulo = "08";
        $newItem->item = "010";
        $newItem->clasificador_presupuestario = "Servicios de Suscripción y Similares";
        $newItem->id_clasificacion = "1";
        $newItem->save();

        $newItem = new ItemPresupuestario();
        $newItem->titulo = "22";
        $newItem->subtitulo = "08";
        $newItem->item = "999";
        $newItem->clasificador_presupuestario = "Otros";
        $newItem->id_clasificacion = "1";
        $newItem->save();

        $newItem = new ItemPresupuestario();
        $newItem->titulo = "22";
        $newItem->subtitulo = "04";
        $newItem->item = "009";
        $newItem->clasificador_presupuestario = "Insumos, Repuestos y Accesorios Computacionales";
        $newItem->id_clasificacion = "2";
        $newItem->save();

        $newItem = new ItemPresupuestario();
        $newItem->titulo = "22";
        $newItem->subtitulo = "04";
        $newItem->item = "010";
        $newItem->clasificador_presupuestario = "Materiales para Mantenimiento y Reparaciones de Inmuebles";
        $newItem->id_clasificacion = "2";
        $newItem->save();

        $newItem = new ItemPresupuestario();
        $newItem->titulo = "22";
        $newItem->subtitulo = "04";
        $newItem->item = "011";
        $newItem->clasificador_presupuestario = "Repuestos y Accesorios para Mantenimiento y Reparaciones de Vehículos";
        $newItem->id_clasificacion = "2";
        $newItem->save();

        $newItem = new ItemPresupuestario();
        $newItem->titulo = "22";
        $newItem->subtitulo = "04";
        $newItem->item = "012";
        $newItem->clasificador_presupuestario = "Otros Materiales, Repuestos y Útiles Diversos";
        $newItem->id_clasificacion = "2";
        $newItem->save();

        $newItem = new ItemPresupuestario();
        $newItem->titulo = "22";
        $newItem->subtitulo = "04";
        $newItem->item = "013";
        $newItem->clasificador_presupuestario = "Equipos Menores";
        $newItem->id_clasificacion = "2";
        $newItem->save();

        $newItem = new ItemPresupuestario();
        $newItem->titulo = "22";
        $newItem->subtitulo = "04";
        $newItem->item = "015";
        $newItem->clasificador_presupuestario = "Productos agropecuarios y forestales";
        $newItem->id_clasificacion = "2";
        $newItem->save();

        $newItem = new ItemPresupuestario();
        $newItem->titulo = "22";
        $newItem->subtitulo = "06";
        $newItem->item = "001";
        $newItem->clasificador_presupuestario = "Mantenimiento y Reparación de Edificaciones";
        $newItem->id_clasificacion = "2";
        $newItem->save();

        $newItem = new ItemPresupuestario();
        $newItem->titulo = "22";
        $newItem->subtitulo = "06";
        $newItem->item = "002";
        $newItem->clasificador_presupuestario = "Mantenimiento y Reparación de Vehículos";
        $newItem->id_clasificacion = "2";
        $newItem->save();

        $newItem = new ItemPresupuestario();
        $newItem->titulo = "22";
        $newItem->subtitulo = "06";
        $newItem->item = "003";
        $newItem->clasificador_presupuestario = "Mantenimiento y Reparación de Mobiliarios y Otros";
        $newItem->id_clasificacion = "2";
        $newItem->save();

        $newItem = new ItemPresupuestario();
        $newItem->titulo = "22";
        $newItem->subtitulo = "06";
        $newItem->item = "004";
        $newItem->clasificador_presupuestario = "Mantenimiento y Reparación de Máquinas y Equipos de Oficina";
        $newItem->id_clasificacion = "2";
        $newItem->save();

        $newItem = new ItemPresupuestario();
        $newItem->titulo = "22";
        $newItem->subtitulo = "06";
        $newItem->item = "005";
        $newItem->clasificador_presupuestario = "Mantenimiento y Reparación de Maquinaria y Equipos de Producción";
        $newItem->id_clasificacion = "2";
        $newItem->save();

        $newItem = new ItemPresupuestario();
        $newItem->titulo = "22";
        $newItem->subtitulo = "06";
        $newItem->item = "007";
        $newItem->clasificador_presupuestario = "Mantenimiento y Reparación de Equipos Informáticos";
        $newItem->id_clasificacion = "2";
        $newItem->save();

        $newItem = new ItemPresupuestario();
        $newItem->titulo = "22";
        $newItem->subtitulo = "06";
        $newItem->item = "999";
        $newItem->clasificador_presupuestario = "Otros";
        $newItem->id_clasificacion = "2";
        $newItem->save();

        $newItem = new ItemPresupuestario();
        $newItem->titulo = "22";
        $newItem->subtitulo = "01";
        $newItem->item = "002";
        $newItem->clasificador_presupuestario = "Para animales";
        $newItem->id_clasificacion = "3";
        $newItem->save();

        $newItem = new ItemPresupuestario();
        $newItem->titulo = "22";
        $newItem->subtitulo = "09";
        $newItem->item = "001";
        $newItem->clasificador_presupuestario = "Arriendo de Terrenos";
        $newItem->id_clasificacion = "4";
        $newItem->save();

        $newItem = new ItemPresupuestario();
        $newItem->titulo = "22";
        $newItem->subtitulo = "09";
        $newItem->item = "002";
        $newItem->clasificador_presupuestario = "Arriendo de Edificios";
        $newItem->id_clasificacion = "4";
        $newItem->save();

        $newItem = new ItemPresupuestario();
        $newItem->titulo = "22";
        $newItem->subtitulo = "09";
        $newItem->item = "003";
        $newItem->clasificador_presupuestario = "Arriendo de Vehículos";
        $newItem->id_clasificacion = "4";
        $newItem->save();

        $newItem = new ItemPresupuestario();
        $newItem->titulo = "22";
        $newItem->subtitulo = "09";
        $newItem->item = "004";
        $newItem->clasificador_presupuestario = "Arriendo de Mobiliario y Otros";
        $newItem->id_clasificacion = "4";
        $newItem->save();

        $newItem = new ItemPresupuestario();
        $newItem->titulo = "22";
        $newItem->subtitulo = "09";
        $newItem->item = "005";
        $newItem->asignacion = "001";
        $newItem->clasificador_presupuestario = "Arriendo de Máquinas y Equipos no médicos";
        $newItem->id_clasificacion = "4";
        $newItem->save();

        $newItem = new ItemPresupuestario();
        $newItem->titulo = "22";
        $newItem->subtitulo = "09";
        $newItem->item = "005";
        $newItem->asignacion = "002";
        $newItem->clasificador_presupuestario = "Arriendo de Máquinas y Equipos Clínicos";
        $newItem->id_clasificacion = "4";
        $newItem->save();

        $newItem = new ItemPresupuestario();
        $newItem->titulo = "22";
        $newItem->subtitulo = "09";
        $newItem->item = "006";
        $newItem->clasificador_presupuestario = "Arriendo de  Equipos Informáticos";
        $newItem->id_clasificacion = "4";
        $newItem->save();

        $newItem = new ItemPresupuestario();
        $newItem->titulo = "22";
        $newItem->subtitulo = "09";
        $newItem->item = "999";
        $newItem->clasificador_presupuestario = "Otros";
        $newItem->id_clasificacion = "4";
        $newItem->save();

        $newItem = new ItemPresupuestario();
        $newItem->titulo = "22";
        $newItem->subtitulo = "10";
        $newItem->item = "001";
        $newItem->clasificador_presupuestario = "Gastos Financieros por Compra y Venta de Títulos y Valores";
        $newItem->id_clasificacion = "4";
        $newItem->save();

        $newItem = new ItemPresupuestario();
        $newItem->titulo = "22";
        $newItem->subtitulo = "10";
        $newItem->item = "002";
        $newItem->clasificador_presupuestario = "Primas y Gastos de Seguros";
        $newItem->id_clasificacion = "4";
        $newItem->save();

        $newItem = new ItemPresupuestario();
        $newItem->titulo = "22";
        $newItem->subtitulo = "10";
        $newItem->item = "003";
        $newItem->clasificador_presupuestario = "Servicio de giros y remesas";
        $newItem->id_clasificacion = "4";
        $newItem->save();

        $newItem = new ItemPresupuestario();
        $newItem->titulo = "22";
        $newItem->subtitulo = "10";
        $newItem->item = "004";
        $newItem->clasificador_presupuestario = "Gastos bancarios";
        $newItem->id_clasificacion = "4";
        $newItem->save();

        $newItem = new ItemPresupuestario();
        $newItem->titulo = "22";
        $newItem->subtitulo = "10";
        $newItem->item = "999";
        $newItem->clasificador_presupuestario = "Otros";
        $newItem->id_clasificacion = "4";
        $newItem->save();

        $newItem = new ItemPresupuestario();
        $newItem->titulo = "22";
        $newItem->subtitulo = "11";
        $newItem->item = "001";
        $newItem->clasificador_presupuestario = "Estudios e Investigaciones";
        $newItem->id_clasificacion = "17";
        $newItem->save();

        $newItem = new ItemPresupuestario();
        $newItem->titulo = "22";
        $newItem->subtitulo = "11";
        $newItem->item = "003";
        $newItem->clasificador_presupuestario = "Servicios Informáticos";
        $newItem->id_clasificacion = "17";
        $newItem->save();

        $newItem = new ItemPresupuestario();
        $newItem->titulo = "22";
        $newItem->subtitulo = "11";
        $newItem->item = "999";
        $newItem->clasificador_presupuestario = "Otros Servicios Técnicos y Profesionales";
        $newItem->id_clasificacion = "17";
        $newItem->save();

        $newItem = new ItemPresupuestario();
        $newItem->titulo = "22";
        $newItem->subtitulo = "04";
        $newItem->item = "007";
        $newItem->asignacion = "001";
        $newItem->clasificador_presupuestario = "Lavandería";
        $newItem->id_clasificacion = "5";
        $newItem->save();

        $newItem = new ItemPresupuestario();
        $newItem->titulo = "22";
        $newItem->subtitulo = "04";
        $newItem->item = "007";
        $newItem->asignacion = "002";
        $newItem->clasificador_presupuestario = "Materiales y Útiles de Aseo";
        $newItem->id_clasificacion = "5";
        $newItem->save();

        $newItem = new ItemPresupuestario();
        $newItem->titulo = "22";
        $newItem->subtitulo = "03";
        $newItem->item = "001";
        $newItem->clasificador_presupuestario = "Para Vehículos";
        $newItem->id_clasificacion = "6";
        $newItem->save();

        $newItem = new ItemPresupuestario();
        $newItem->titulo = "22";
        $newItem->subtitulo = "03";
        $newItem->item = "002";
        $newItem->clasificador_presupuestario = "Para Maquinarias, Equipos de Producción, Tracción y Elevación";
        $newItem->id_clasificacion = "6";
        $newItem->save();

        $newItem = new ItemPresupuestario();
        $newItem->titulo = "22";
        $newItem->subtitulo = "03";
        $newItem->item = "003";
        $newItem->clasificador_presupuestario = "Para Calefacción";
        $newItem->id_clasificacion = "6";
        $newItem->save();

        $newItem = new ItemPresupuestario();
        $newItem->titulo = "22";
        $newItem->subtitulo = "03";
        $newItem->item = "999";
        $newItem->clasificador_presupuestario = "Para Otros";
        $newItem->id_clasificacion = "6";
        $newItem->save();

        $newItem = new ItemPresupuestario();
        $newItem->titulo = "22";
        $newItem->subtitulo = "04";
        $newItem->item = "001";
        $newItem->clasificador_presupuestario = "Materiales de Oficina";
        $newItem->id_clasificacion = "7";
        $newItem->save();

        $newItem = new ItemPresupuestario();
        $newItem->titulo = "22";
        $newItem->subtitulo = "04";
        $newItem->item = "002";
        $newItem->clasificador_presupuestario = "Textos y Otros Materiales de Enseñanza";
        $newItem->id_clasificacion = "7";
        $newItem->save();

        $newItem = new ItemPresupuestario();
        $newItem->titulo = "22";
        $newItem->subtitulo = "04";
        $newItem->item = "008";
        $newItem->clasificador_presupuestario = "Menaje para Oficina, Casino y Otros";
        $newItem->id_clasificacion = "7";
        $newItem->save();

        $newItem = new ItemPresupuestario();
        $newItem->titulo = "22";
        $newItem->subtitulo = "02";
        $newItem->item = "001";
        $newItem->clasificador_presupuestario = "Textiles y Acabados Textiles";
        $newItem->id_clasificacion = "8";
        $newItem->save();

        $newItem = new ItemPresupuestario();
        $newItem->titulo = "22";
        $newItem->subtitulo = "02";
        $newItem->item = "002";
        $newItem->asignacion = "001";
        $newItem->clasificador_presupuestario = "Vestuario, Accesorios y Prendas Diversas-Para el Personal";
        $newItem->id_clasificacion = "8";
        $newItem->save();

        $newItem = new ItemPresupuestario();
        $newItem->titulo = "22";
        $newItem->subtitulo = "02";
        $newItem->item = "002";
        $newItem->asignacion = "002";
        $newItem->clasificador_presupuestario = "Vestuario, Accesorios y Prendas Diversas-Otros Textiles Actividad Hospitalaria";
        $newItem->id_clasificacion = "8";
        $newItem->save();

        $newItem = new ItemPresupuestario();
        $newItem->titulo = "22";
        $newItem->subtitulo = "02";
        $newItem->item = "003";
        $newItem->clasificador_presupuestario = "Calzado";
        $newItem->id_clasificacion = "8";
        $newItem->save();

        $newItem = new ItemPresupuestario();
        $newItem->titulo = "22";
        $newItem->subtitulo = "11";
        $newItem->item = "002";
        $newItem->asignacion = "001";
        $newItem->subasignacion = "01";
        $newItem->clasificador_presupuestario = "Ley 18575 Cursos Cont Con Terceros";
        $newItem->id_clasificacion = "9";
        $newItem->save();

        $newItem = new ItemPresupuestario();
        $newItem->titulo = "22";
        $newItem->subtitulo = "11";
        $newItem->item = "002";
        $newItem->asignacion = "001";
        $newItem->subasignacion = "02";
        $newItem->clasificador_presupuestario = "Ley 19664 Cursos Contratados Con Terceros";
        $newItem->id_clasificacion = "9";
        $newItem->save();

        $newItem = new ItemPresupuestario();
        $newItem->titulo = "22";
        $newItem->subtitulo = "11";
        $newItem->item = "002";
        $newItem->asignacion = "003";
        $newItem->subasignacion = "01";
        $newItem->clasificador_presupuestario = "Ley 18575 Pagos A Prof Y Monitores";
        $newItem->id_clasificacion = "9";
        $newItem->save();

        $newItem = new ItemPresupuestario();
        $newItem->titulo = "22";
        $newItem->subtitulo = "11";
        $newItem->item = "002";
        $newItem->asignacion = "003";
        $newItem->subasignacion = "02";
        $newItem->clasificador_presupuestario = "Ley 19664 Pagos A Profesores y Monitores";
        $newItem->id_clasificacion = "9";
        $newItem->save();

        $newItem = new ItemPresupuestario();
        $newItem->titulo = "22";
        $newItem->subtitulo = "11";
        $newItem->item = "002";
        $newItem->asignacion = "006";
        $newItem->subasignacion = "01";
        $newItem->clasificador_presupuestario = "Ley 18834 Convenios con Universidades";
        $newItem->id_clasificacion = "9";
        $newItem->save();

        $newItem = new ItemPresupuestario();
        $newItem->titulo = "22";
        $newItem->subtitulo = "11";
        $newItem->item = "002";
        $newItem->asignacion = "006";
        $newItem->subasignacion = "02";
        $newItem->clasificador_presupuestario = "Ley 19664 Convenio con Universidades";
        $newItem->id_clasificacion = "9";
        $newItem->save();

        $newItem = new ItemPresupuestario();
        $newItem->titulo = "22";
        $newItem->subtitulo = "04";
        $newItem->item = "006";
        $newItem->clasificador_presupuestario = "Fertilizantes, insecticidas, fungicidas y otros";
        $newItem->id_clasificacion = "10";
        $newItem->save();

        $newItem = new ItemPresupuestario();
        $newItem->titulo = "22";
        $newItem->subtitulo = "04";
        $newItem->item = "014";
        $newItem->clasificador_presupuestario = "Productos elaborados de cuero, caucho y plástico";
        $newItem->id_clasificacion = "10";
        $newItem->save();

        $newItem = new ItemPresupuestario();
        $newItem->titulo = "22";
        $newItem->subtitulo = "04";
        $newItem->item = "999";
        $newItem->clasificador_presupuestario = "Otros";
        $newItem->id_clasificacion = "10";
        $newItem->save();

        $newItem = new ItemPresupuestario();
        $newItem->titulo = "22";
        $newItem->subtitulo = "07";
        $newItem->item = "001";
        $newItem->clasificador_presupuestario = "Servicios de Publicidad";
        $newItem->id_clasificacion = "10";
        $newItem->save();

        $newItem = new ItemPresupuestario();
        $newItem->titulo = "22";
        $newItem->subtitulo = "07";
        $newItem->item = "002";
        $newItem->clasificador_presupuestario = "Servicios de Impresión";
        $newItem->id_clasificacion = "10";
        $newItem->save();

        $newItem = new ItemPresupuestario();
        $newItem->titulo = "22";
        $newItem->subtitulo = "07";
        $newItem->item = "003";
        $newItem->clasificador_presupuestario = "Servicios de encuadernación y empaste";
        $newItem->id_clasificacion = "10";
        $newItem->save();

        $newItem = new ItemPresupuestario();
        $newItem->titulo = "22";
        $newItem->subtitulo = "07";
        $newItem->item = "999";
        $newItem->clasificador_presupuestario = "Otros";
        $newItem->id_clasificacion = "10";
        $newItem->save();

        $newItem = new ItemPresupuestario();
        $newItem->titulo = "22";
        $newItem->subtitulo = "12";
        $newItem->item = "002";
        $newItem->clasificador_presupuestario = "Gastos Menores";
        $newItem->id_clasificacion = "10";
        $newItem->save();

        $newItem = new ItemPresupuestario();
        $newItem->titulo = "22";
        $newItem->subtitulo = "12";
        $newItem->item = "003";
        $newItem->clasificador_presupuestario = "Gastos de Representación, Protocolo y Ceremonial";
        $newItem->id_clasificacion = "10";
        $newItem->save();

        $newItem = new ItemPresupuestario();
        $newItem->titulo = "22";
        $newItem->subtitulo = "12";
        $newItem->item = "004";
        $newItem->clasificador_presupuestario = "Intereses, Multas y Recargos";
        $newItem->id_clasificacion = "10";
        $newItem->save();

        $newItem = new ItemPresupuestario();
        $newItem->titulo = "22";
        $newItem->subtitulo = "12";
        $newItem->item = "005";
        $newItem->clasificador_presupuestario = "Derechos y Tasas";
        $newItem->id_clasificacion = "10";
        $newItem->save();

        $newItem = new ItemPresupuestario();
        $newItem->titulo = "22";
        $newItem->subtitulo = "12";
        $newItem->item = "006";
        $newItem->clasificador_presupuestario = "Contribuciones";
        $newItem->id_clasificacion = "10";
        $newItem->save();

        $newItem = new ItemPresupuestario();
        $newItem->titulo = "22";
        $newItem->subtitulo = "12";
        $newItem->item = "999";
        $newItem->asignacion = "011";
        $newItem->clasificador_presupuestario = "Condonación copagos MAI";
        $newItem->id_clasificacion = "10";
        $newItem->save();

        $newItem = new ItemPresupuestario();
        $newItem->titulo = "22";
        $newItem->subtitulo = "12";
        $newItem->item = "999";
        $newItem->asignacion = "017";
        $newItem->clasificador_presupuestario = "Pago mutualidades de empleadores art. 77 bis";
        $newItem->id_clasificacion = "10";
        $newItem->save();

        $newItem = new ItemPresupuestario();
        $newItem->titulo = "22";
        $newItem->subtitulo = "12";
        $newItem->item = "999";
        $newItem->asignacion = "018";
        $newItem->clasificador_presupuestario = "Otros";
        $newItem->id_clasificacion = "10";
        $newItem->save();

        $newItem = new ItemPresupuestario();
        $newItem->titulo = "22";
        $newItem->subtitulo = "04";
        $newItem->item = "004";
        $newItem->asignacion = "001";
        $newItem->subasignacion = "01";
        $newItem->clasificador_presupuestario = "Farmacia- Compras Intermediadas";
        $newItem->id_clasificacion = "11";
        $newItem->save();

        $newItem = new ItemPresupuestario();
        $newItem->titulo = "22";
        $newItem->subtitulo = "04";
        $newItem->item = "004";
        $newItem->asignacion = "002";
        $newItem->clasificador_presupuestario = "Prod Para Cirugía Dental";
        $newItem->id_clasificacion = "11";
        $newItem->save();

        $newItem = new ItemPresupuestario();
        $newItem->titulo = "22";
        $newItem->subtitulo = "04";
        $newItem->item = "004";
        $newItem->asignacion = "003";
        $newItem->clasificador_presupuestario = "Materiales de curación";
        $newItem->id_clasificacion = "11";
        $newItem->save();

        $newItem = new ItemPresupuestario();
        $newItem->titulo = "22";
        $newItem->subtitulo = "04";
        $newItem->item = "004";
        $newItem->asignacion = "004";
        $newItem->clasificador_presupuestario = "Prótesis";
        $newItem->id_clasificacion = "11";
        $newItem->save();

        $newItem = new ItemPresupuestario();
        $newItem->titulo = "22";
        $newItem->subtitulo = "04";
        $newItem->item = "004";
        $newItem->asignacion = "005";
        $newItem->clasificador_presupuestario = "Productos Para Mecánica Dental";
        $newItem->id_clasificacion = "11";
        $newItem->save();

        $newItem = new ItemPresupuestario();
        $newItem->titulo = "22";
        $newItem->subtitulo = "12";
        $newItem->item = "999";
        $newItem->asignacion = "001";
        $newItem->clasificador_presupuestario = "Compra De Servicios Médicos De Diálisis";
        $newItem->id_clasificacion = "18";
        $newItem->save();

        $newItem = new ItemPresupuestario();
        $newItem->titulo = "22";
        $newItem->subtitulo = "12";
        $newItem->item = "999";
        $newItem->asignacion = "002";
        $newItem->subasignacion = "01";
        $newItem->clasificador_presupuestario = "Compra De Exámenes";
        $newItem->id_clasificacion = "18";
        $newItem->save();

        $newItem = new ItemPresupuestario();
        $newItem->titulo = "22";
        $newItem->subtitulo = "12";
        $newItem->item = "999";
        $newItem->asignacion = "003";
        $newItem->subasignacion = "01";
        $newItem->clasificador_presupuestario = "Intrahospitalarias con Personal Interno";
        $newItem->id_clasificacion = "18";
        $newItem->save();

        $newItem = new ItemPresupuestario();
        $newItem->titulo = "22";
        $newItem->subtitulo = "12";
        $newItem->item = "999";
        $newItem->asignacion = "003";
        $newItem->subasignacion = "02";
        $newItem->clasificador_presupuestario = "Intrahospitalaria con Personal Externo";
        $newItem->id_clasificacion = "18";
        $newItem->save();

        $newItem = new ItemPresupuestario();
        $newItem->titulo = "22";
        $newItem->subtitulo = "12";
        $newItem->item = "999";
        $newItem->asignacion = "003";
        $newItem->subasignacion = "03";
        $newItem->clasificador_presupuestario = "Compra Intervenciones Quirúrgicas clínicas";
        $newItem->id_clasificacion = "18";
        $newItem->save();

        $newItem = new ItemPresupuestario();
        $newItem->titulo = "22";
        $newItem->subtitulo = "12";
        $newItem->item = "999";
        $newItem->asignacion = "004";
        $newItem->clasificador_presupuestario = "Compra De Atención De Urgencia";
        $newItem->id_clasificacion = "18";
        $newItem->save();

        $newItem = new ItemPresupuestario();
        $newItem->titulo = "22";
        $newItem->subtitulo = "12";
        $newItem->item = "999";
        $newItem->asignacion = "006";
        $newItem->clasificador_presupuestario = "Colocación familiar de menores y extra hospitalaria";
        $newItem->id_clasificacion = "18";
        $newItem->save();

        $newItem = new ItemPresupuestario();
        $newItem->titulo = "22";
        $newItem->subtitulo = "12";
        $newItem->item = "999";
        $newItem->asignacion = "007";
        $newItem->clasificador_presupuestario = "Colocación de embarazadas de alto riesgo";
        $newItem->id_clasificacion = "18";
        $newItem->save();

        $newItem = new ItemPresupuestario();
        $newItem->titulo = "22";
        $newItem->subtitulo = "12";
        $newItem->item = "999";
        $newItem->asignacion = "008";
        $newItem->clasificador_presupuestario = "Colocación de adulto mayor riesgo";
        $newItem->id_clasificacion = "18";
        $newItem->save();

        $newItem = new ItemPresupuestario();
        $newItem->titulo = "22";
        $newItem->subtitulo = "12";
        $newItem->item = "999";
        $newItem->asignacion = "010";
        $newItem->clasificador_presupuestario = "Pasaje Y Traslados De Pacientes";
        $newItem->id_clasificacion = "18";
        $newItem->save();

        $newItem = new ItemPresupuestario();
        $newItem->titulo = "22";
        $newItem->subtitulo = "12";
        $newItem->item = "999";
        $newItem->asignacion = "014";
        $newItem->subasignacion = "01";
        $newItem->clasificador_presupuestario = "Compra de camas al extrasistema camas criticas";
        $newItem->id_clasificacion = "18";
        $newItem->save();

        $newItem = new ItemPresupuestario();
        $newItem->titulo = "22";
        $newItem->subtitulo = "12";
        $newItem->item = "999";
        $newItem->asignacion = "014";
        $newItem->subasignacion = "02";
        $newItem->clasificador_presupuestario = "Compra de camas al extrasistema camas no criticas";
        $newItem->id_clasificacion = "18";
        $newItem->save();

        $newItem = new ItemPresupuestario();
        $newItem->titulo = "22";
        $newItem->subtitulo = "12";
        $newItem->item = "999";
        $newItem->asignacion = "015";
        $newItem->clasificador_presupuestario = "Gastos pueblos indígenas";
        $newItem->id_clasificacion = "18";
        $newItem->save();

        $newItem = new ItemPresupuestario();
        $newItem->titulo = "22";
        $newItem->subtitulo = "12";
        $newItem->item = "999";
        $newItem->asignacion = "016";
        $newItem->subasignacion = "01";
        $newItem->clasificador_presupuestario = "Compra de consultas médicas";
        $newItem->id_clasificacion = "18";
        $newItem->save();

        $newItem = new ItemPresupuestario();
        $newItem->titulo = "22";
        $newItem->subtitulo = "12";
        $newItem->item = "999";
        $newItem->asignacion = "016";
        $newItem->subasignacion = "02";
        $newItem->clasificador_presupuestario = "Compra de consultas no médicas";
        $newItem->id_clasificacion = "18";
        $newItem->save();

        $newItem = new ItemPresupuestario();
        $newItem->titulo = "22";
        $newItem->subtitulo = "12";
        $newItem->item = "999";
        $newItem->asignacion = "018";
        $newItem->clasificador_presupuestario = "Otras Compras De Servicios Y Convenios";
        $newItem->id_clasificacion = "18";
        $newItem->save();

        $newItem = new ItemPresupuestario();
        $newItem->titulo = "22";
        $newItem->subtitulo = "12";
        $newItem->item = "999";
        $newItem->asignacion = "020";
        $newItem->clasificador_presupuestario = "Programa adulto mayor canastas dentales";
        $newItem->id_clasificacion = "18";
        $newItem->save();

        $newItem = new ItemPresupuestario();
        $newItem->titulo = "22";
        $newItem->subtitulo = "12";
        $newItem->item = "999";
        $newItem->asignacion = "021";
        $newItem->clasificador_presupuestario = "Servicios de radioterapia";
        $newItem->id_clasificacion = "18";
        $newItem->save();

        $newItem = new ItemPresupuestario();
        $newItem->titulo = "22";
        $newItem->subtitulo = "12";
        $newItem->item = "999";
        $newItem->asignacion = "022";
        $newItem->clasificador_presupuestario = "Colocación pacientes con enfermedades mentales";
        $newItem->id_clasificacion = "18";
        $newItem->save();

        $newItem = new ItemPresupuestario();
        $newItem->titulo = "22";
        $newItem->subtitulo = "04";
        $newItem->item = "005";
        $newItem->asignacion = "001";
        $newItem->clasificador_presupuestario = "Instrumental Quirúrgico";
        $newItem->id_clasificacion = "12";
        $newItem->save();

        $newItem = new ItemPresupuestario();
        $newItem->titulo = "22";
        $newItem->subtitulo = "04";
        $newItem->item = "003";
        $newItem->asignacion = "001";
        $newItem->clasificador_presupuestario = "Oxigeno Y Gases Clínicos";
        $newItem->id_clasificacion = "13";
        $newItem->save();

        $newItem = new ItemPresupuestario();
        $newItem->titulo = "22";
        $newItem->subtitulo = "04";
        $newItem->item = "003";
        $newItem->asignacion = "002";
        $newItem->clasificador_presupuestario = "Otros Químicos";
        $newItem->id_clasificacion = "13";
        $newItem->save();

        $newItem = new ItemPresupuestario();
        $newItem->titulo = "22";
        $newItem->subtitulo = "12";
        $newItem->item = "999";
        $newItem->asignacion = "009";
        $newItem->subasignacion = "01";
        $newItem->clasificador_presupuestario = "Convenios DFL  36 Centralizados";
        $newItem->id_clasificacion = "14";
        $newItem->save();

        $newItem = new ItemPresupuestario();
        $newItem->titulo = "22";
        $newItem->subtitulo = "12";
        $newItem->item = "999";
        $newItem->asignacion = "009";
        $newItem->subasignacion = "02";
        $newItem->clasificador_presupuestario = "Convenio DFL 36 Descentralizados";
        $newItem->id_clasificacion = "14";
        $newItem->save();

        $newItem = new ItemPresupuestario();
        $newItem->titulo = "22";
        $newItem->subtitulo = "12";
        $newItem->item = "999";
        $newItem->asignacion = "005";
        $newItem->clasificador_presupuestario = "Pago Rebases Ley de Urgencia";
        $newItem->id_clasificacion = "15";
        $newItem->save();

        $newItem = new ItemPresupuestario();
        $newItem->titulo = "22";
        $newItem->subtitulo = "12";
        $newItem->item = "999";
        $newItem->asignacion = "019";
        $newItem->clasificador_presupuestario = "Servicios de intermediación CENABAST";
        $newItem->id_clasificacion = "16";
        $newItem->save();

        $newItem = new ItemPresupuestario();
        $newItem->titulo = "22";
        $newItem->subtitulo = "01";
        $newItem->item = "001";
        $newItem->asignacion = "001";
        $newItem->clasificador_presupuestario = "Pacientes";
        $newItem->id_clasificacion = "3";
        $newItem->save();

        $newItem = new ItemPresupuestario();
        $newItem->titulo = "22";
        $newItem->subtitulo = "01";
        $newItem->item = "001";
        $newItem->asignacion = "002";
        $newItem->clasificador_presupuestario = "Funcionarios";
        $newItem->id_clasificacion = "3";
        $newItem->save();

        $newItem = new ItemPresupuestario();
        $newItem->titulo = "22";
        $newItem->subtitulo = "01";
        $newItem->item = "001";
        $newItem->asignacion = "003";
        $newItem->clasificador_presupuestario = "Capacitación";
        $newItem->id_clasificacion = "3";
        $newItem->save();

        $newItem = new ItemPresupuestario();
        $newItem->titulo = "22";
        $newItem->subtitulo = "06";
        $newItem->item = "006";
        $newItem->asignacion = "001";
        $newItem->clasificador_presupuestario = "Mantenimiento Y Reparación Maquina Y Equipo Preven";
        $newItem->id_clasificacion = "2";
        $newItem->save();

        $newItem = new ItemPresupuestario();
        $newItem->titulo = "22";
        $newItem->subtitulo = "06";
        $newItem->item = "006";
        $newItem->asignacion = "002";
        $newItem->clasificador_presupuestario = "Mantenimiento Y Reparación Maquina Y Equipo Correc";
        $newItem->id_clasificacion = "2";
        $newItem->save();

        $newItem = new ItemPresupuestario();
        $newItem->titulo = "29";
        $newItem->subtitulo = "04";
        $newItem->item = "001";
        $newItem->clasificador_presupuestario = "Mobiliario y Otros Inversion de Operación";
        $newItem->id_clasificacion = "1";
        $newItem->save();

        $newItem = new ItemPresupuestario();
        $newItem->titulo = "29";
        $newItem->subtitulo = "05";
        $newItem->item = "001";
        $newItem->asignacion = "001";
        $newItem->clasificador_presupuestario = "Máquinas y Equipos de Oficina Inversion de Operaci";
        $newItem->id_clasificacion = "1";
        $newItem->save();

        $newItem = new ItemPresupuestario();
        $newItem->titulo = "29";
        $newItem->subtitulo = "05";
        $newItem->item = "002";
        $newItem->asignacion = "001";
        $newItem->clasificador_presupuestario = "Maquinarias y Equipos para la Producción Inversion";
        $newItem->id_clasificacion = "1";
        $newItem->save();

        $newItem = new ItemPresupuestario();
        $newItem->titulo = "22";
        $newItem->subtitulo = "03";
        $newItem->item = "003";
        $newItem->asignacion = "001";
        $newItem->clasificador_presupuestario = "Materiales de oficina";
        $newItem->id_clasificacion = "7";
        $newItem->save();

        $newItem = new ItemPresupuestario();
        $newItem->titulo = "22";
        $newItem->subtitulo = "11";
        $newItem->item = "01";
        $newItem->clasificador_presupuestario = "SERVICIOS TECNICOS Y PROFESIONALES";
        $newItem->id_clasificacion = "18";
        $newItem->save();

        $newItem = new ItemPresupuestario();
        $newItem->titulo = "22";
        $newItem->subtitulo = "04";
        $newItem->item = "004";
        $newItem->asignacion = "006";
        $newItem->clasificador_presupuestario = "Ayuda Técnicas - Ortesis";
        $newItem->id_clasificacion = "11";
        $newItem->save();

    }
}
