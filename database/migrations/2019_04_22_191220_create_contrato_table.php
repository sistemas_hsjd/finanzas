<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateContratoTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('contrato', function(Blueprint $table)
		{
			$table->integer('id', true);
			$table->integer('id_digitador');
			$table->timestamp('ingreso')->default(DB::raw('CURRENT_TIMESTAMP'));
			$table->integer('resolucion');
			$table->dateTime('fecha_resolucion');
			$table->integer('id_proveedor');
			$table->integer('id_unidad');
			$table->dateTime('inicio_vigencia');
			$table->dateTime('fin_vigencia');
			$table->boolean('sociedad');
			$table->integer('id_profesion');
			$table->integer('id_especialidad');
			$table->integer('valor_hora');
			$table->integer('id_categoria');
			$table->integer('monto_maximo');
			$table->integer('id_tipo_prestacion');
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('contrato');
	}

}
