<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateOpcionTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('opcion', function(Blueprint $table)
		{
			$table->integer('id', true);
			$table->integer('id_padre')->nullable();
			$table->string('nombre', 50);
			$table->string('url', 100)->nullable();
			$table->string('icono', 50);
			$table->timestamp('fecha_ingreso')->default(DB::raw('CURRENT_TIMESTAMP'));
			$table->boolean('eliminado')->default(0);
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('opcion');
	}

}
