<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AlterArchivosAceptaTableChangeColumnsToBiginteger extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('archivos_acepta', function (Blueprint $table) {
            $table->bigInteger('monto_neto')->change();
            $table->bigInteger('monto_exento')->change();
            $table->bigInteger('monto_iva')->change();
            $table->bigInteger('monto_total')->change();
            $table->bigInteger('folio')->change();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('archivos_acepta', function (Blueprint $table) {
            $table->text('monto_neto', 65535)->change();
            $table->text('monto_exento', 65535)->change();
            $table->text('monto_iva', 65535)->change();
            // $table->text('folio', 20)->change();
        });
    }
}
