<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateContratosTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::dropIfExists('contrato');

        Schema::create('contratos', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->integer('id_tipo_contrato')->index('id_tipo_contrato')->comment('1 = Honorario | 2 = Convenio');
            $table->bigInteger('id_proveedor')->index('id_proveedor');
            $table->integer('id_tipo_adjudicacion')->index('id_tipo_adjudicacion');
            $table->integer('id_referente_tecnico')->index('id_referente_tecnico')->nullable();
            $table->integer('resolucion');
            $table->date('fecha_resolucion');
            $table->date('inicio_vigencia');
            $table->date('fin_vigencia');
            $table->bigInteger('monto_contrato');
            $table->bigInteger('saldo_contrato');
            $table->string('detalle_contrato');
            /**
             * Necesarios para honorarios
             */
            $table->integer('id_unidad')->index('id_unidad')->nullable();
            $table->bigInteger('id_profesion')->index('id_profesion')->nullable();
            $table->integer('id_tipo_prestacion')->index('id_tipo_prestacion')->nullable();
            $table->integer('id_categoria')->index('id_categoria')->nullable();
            $table->bigInteger('addemdum')->nullable();
            $table->date('fecha_addemdum')->nullable();
            $table->string('sueldo_horas')->nullable()->comment('guarda : Sueldo | Horas');
            $table->integer('valor_hora')->nullable();
            $table->boolean('por_procedimiento')->default(0);
            /**
             * Necesarios para convenios
             */
            $table->bigInteger('monto_preventivo')->nullable();
            $table->bigInteger('monto_correctivo')->nullable();
            $table->bigInteger('saldo_preventivo')->nullable();
            $table->bigInteger('saldo_correctivo')->nullable();
            $table->string('licitacion')->nullable();
            $table->boolean('cuotas')->default(0);
            $table->bigInteger('numero_boleta_garantia')->nullable();
            $table->bigInteger('monto_boleta_garantia')->nullable();
            $table->date('inicio_vigencia_boleta_garantia')->nullable();
            $table->date('fin_vigencia_boleta_garantia')->nullable();

            /**
             * Para dar termino al contrato
             */
            $table->integer('id_user_termino')->nullable()->default(null);
            $table->date('fecha_termino')->nullable();
            $table->string('detalle_termino')->nullable();
            $table->boolean('termino')->default(0);

            $table->integer('id_user_created')->nullable()->default(null);
			$table->integer('id_user_updated')->nullable()->default(null);
			$table->integer('id_user_deleted')->nullable()->default(null);

            $table->softDeletes();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('contratos');
    }
}
