<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateVistobuenoTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('vistos_buenos', function(Blueprint $table)
		{	
			// Los siguientes campos son al generar el memo para visto bueno
			$table->integer('id', true);
			$table->string('memo');
			$table->date('fecha_solicitud')->nullable();
			$table->string('informe')->nullable();
			$table->date('fecha_informe')->nullable();
			$table->date('fecha_recepcion_fisica')->nullable();
			$table->integer('id_referente_tecnico')->index('id_referente_tecnico')->nullable(); // ex id_servicio
			$table->integer('id_digitador')->index('id_digitador');
			// Los siguientes 3 campos son del visto bueno
			$table->string('memo_respuesta')->nullable();
			$table->date('fecha_memo_respuesta')->nullable();
			$table->integer('id_registrador_memo_respuesta')->nullable()->default(null);
			$table->string('observacion_memo_respuesta')->nullable()->default(null);
			$table->integer('id_user_created')->nullable()->default(null);
			$table->integer('id_user_updated')->nullable()->default(null);
			$table->integer('id_user_deleted')->nullable()->default(null);
			$table->softDeletes();
			$table->timestamps();
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('vistos_buenos');
	}

}
