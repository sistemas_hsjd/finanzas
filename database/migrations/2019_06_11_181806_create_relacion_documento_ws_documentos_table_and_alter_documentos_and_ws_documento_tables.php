<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateRelacionDocumentoWsDocumentosTableAndAlterDocumentosAndWsDocumentoTables extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('relacion_documentos_ws_documentos', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->bigInteger('id_documento')->index('id_documento');
            $table->bigInteger('id_ws_documento')->index('id_ws_documento');
            $table->integer('id_user_relacion')->index('id_user_relacion');
            // $table->unique(['id_documento','id_ws_documento','deleted_at']);
            $table->softDeletes();
            $table->timestamps();
        });

        Schema::table('documentos', function (Blueprint $table) {
            $table->bigInteger('id_relacion')->after('id_cuadratura')->nullable()->default(null);
        });

        Schema::table('ws_documento', function (Blueprint $table) {
            $table->bigInteger('id_relacion')->after('documento_total')->nullable()->default(null);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('relacion_documentos_ws_documentos');

        Schema::table('documentos', function (Blueprint $table) {
            $table->dropColumn('id_relacion');
        });

        Schema::table('ws_documento', function (Blueprint $table) {
            $table->dropColumn('id_relacion');
        });
    }
}
