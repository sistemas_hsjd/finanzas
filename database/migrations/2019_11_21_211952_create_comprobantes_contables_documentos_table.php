<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateComprobantesContablesDocumentosTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('comprobantes_contables_documentos', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->bigInteger('id_comprobante_contable');
            $table->bigInteger('id_documento');
            $table->bigInteger('id_proveedor');
            $table->bigInteger('monto_comprobante');
            $table->integer('id_cuenta_bancaria')->nullable();
            $table->integer('id_cuenta_contable')->nullable();
            $table->integer('id_medio_pago')->nullable();
            // $table->string('tipo_medio_pago')->nullable();
            $table->bigInteger('numero_documento_pago')->nullable();
            $table->timestamp('fecha_emision_pago')->nullable();
            $table->boolean('reversado')->nullable()->default(0);

            $table->softDeletes();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('comprobantes_contables_documentos');
    }
}
