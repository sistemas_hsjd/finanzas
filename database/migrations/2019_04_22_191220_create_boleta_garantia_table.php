<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateBoletaGarantiaTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('boleta_garantia', function(Blueprint $table)
		{
			$table->integer('id', true);
			$table->integer('id_digitador')->index('id_digitador');
			$table->string('boleta', 100)->unique('cd_boleta');
			$table->integer('id_proveedor')->index('id_proveedor');
			$table->timestamp('registro')->default(DB::raw('CURRENT_TIMESTAMP'));
			$table->date('emision');
			$table->date('vencimiento');
			$table->integer('id_tipo_garantia')->index('id_tipo_garantia');
			$table->integer('id_banco')->index('id_banco');
			$table->integer('monto');
			$table->integer('id_tipo_garantiza')->index('id_tipo_garantiza');
			$table->string('licitacion', 100);
			$table->date('devolucion')->nullable();
			$table->date('termino_licitacion');
			$table->bigInteger('monto_licitacion');
			$table->integer('id_estado_boleta');
			$table->text('observacion', 65535);
			$table->integer('id_digitador_autorizacion')->nullable()->index('id_digitador_autorizacion');
			$table->string('memo', 50)->nullable();
			$table->date('fecha_memo')->nullable();
			$table->integer('id_solicitante_devolucion')->nullable();
			$table->boolean('devuelto')->default(0);
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('boleta_garantia');
	}

}
