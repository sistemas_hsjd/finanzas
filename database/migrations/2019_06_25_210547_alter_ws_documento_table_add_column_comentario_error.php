<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AlterWsDocumentoTableAddColumnComentarioError extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('ws_documento', function (Blueprint $table) {
            $table->string('comentario_error',500)->after('documento_total')->nullable()->default(null);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('ws_documento', function (Blueprint $table) {
            $table->dropColumn('comentario_error');
        });
    }
}
