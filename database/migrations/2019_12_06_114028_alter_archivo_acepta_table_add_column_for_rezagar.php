<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AlterArchivoAceptaTableAddColumnForRezagar extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('archivos_acepta', function (Blueprint $table) {
            $table->text('observacion_rezagar')->nullable()->default(null);
            $table->integer('id_user_rezagar')->nullable()->default(null);
            $table->integer('id_user_quita_rezagar')->nullable()->default(null);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('archivos_acepta', function (Blueprint $table) {
            $table->dropColumn('observacion_rezagar');
            $table->dropColumn('id_user_rezagar');
            $table->dropColumn('id_user_quita_rezagar');
        });
    }
}
