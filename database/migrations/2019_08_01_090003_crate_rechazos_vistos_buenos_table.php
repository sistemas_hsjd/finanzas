<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CrateRechazosVistosBuenosTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('rechazos_vistos_buenos', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->integer('id_visto_bueno')->index('id_visto_bueno');

            $table->integer('id_user_created')->index('id_user_created')->nullable()->default(null);
			$table->integer('id_user_updated')->index('id_user_updated')->nullable()->default(null);
            $table->softDeletes();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('rechazos_vistos_buenos');
    }
}
