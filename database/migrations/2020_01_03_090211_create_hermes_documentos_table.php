<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateHermesDocumentosTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('hermes_documentos', function (Blueprint $table) {
            $table->increments('id');
            $table->bigInteger('id_proveedor');
            $table->bigInteger('folio_documento');
            $table->string('tipo_documento');
            $table->bigInteger('documento_neto');
            $table->bigInteger('documento_total');
            $table->timestamp('fecha_ingreso_recepcion')->comment('fecha de ingreso en Hermes');
            $table->timestamp('fecha_validacion_recepcion')->comment('fecha de validacion en Hermes')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('hermes_documentos');
    }
}
