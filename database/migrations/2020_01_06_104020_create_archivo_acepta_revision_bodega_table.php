<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateArchivoAceptaRevisionBodegaTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('archivo_acepta_revision_bodega', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('id_archivo_acepta');
            $table->boolean('recepcion_total')->default(0);
            $table->boolean('recepcion_parcial')->default(0);
            $table->boolean('sin_recepcion')->default(0);
            $table->text('comentario')->nullable();
            $table->boolean('revisado')->default(0);
            $table->integer('id_user_created')->nullable()->default(null);
            $table->integer('id_user_respuesta')->nullable()->default(null);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('archivo_acepta_revision_bodega');
    }
}
