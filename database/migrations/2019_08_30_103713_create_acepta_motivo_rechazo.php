<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateAceptaMotivoRechazo extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('acepta_motivo_rechazo', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('id_archivo_acepta')->index('id_archivo_acepta');
            $table->integer('id_motivo_rechazo')->index('id_motivo_rechazo');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('acepta_motivo_rechazo');
    }
}
