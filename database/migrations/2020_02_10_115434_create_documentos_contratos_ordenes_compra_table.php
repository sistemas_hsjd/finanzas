<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateDocumentosContratosOrdenesCompraTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('documentos_contratos_ordenes_compra', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->bigInteger('id_documento')->index('id_documento');
            $table->bigInteger('id_contrato')->index('id_contrato')->nullable();
            $table->bigInteger('id_orden_compra')->index('id_orden_compra')->nullable();
            $table->float('horas_boleta')->nullable();
            $table->boolean('ocupa_preventivo')->default(0);
            $table->boolean('ocupa_correctivo')->default(0);
            $table->integer('id_user_created')->nullable()->default(null);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('documentos_contratos_ordenes_compra');
    }
}
