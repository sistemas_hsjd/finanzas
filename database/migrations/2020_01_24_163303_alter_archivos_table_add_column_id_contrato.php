<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AlterArchivosTableAddColumnIdContrato extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('archivos', function (Blueprint $table) {
            $table->integer('id_contrato')->index('id_contrato')->nullable()->after('id_tipo_archivo');
            $table->integer('id_orden_compra')->index('id_orden_compra')->nullable()->after('id_contrato');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('archivos', function (Blueprint $table) {
            $table->dropColumn('id_contrato');
            $table->dropColumn('id_orden_compra');
        });
    }
}
