<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AlterDocumentosTableAddColumnsForMotivosProblema extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('documentos', function (Blueprint $table) {
            $table->text('observacion_problema', 65535)->nullable()->default(null)->after('id_user_para_devengar');
            $table->integer('id_user_problema')->nullable()->default(null)->after('observacion_problema');
            $table->integer('id_user_quita_problema')->nullable()->default(null)->after('id_user_problema');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('documentos', function (Blueprint $table) {
            $table->dropColumn('observacion_problema');
            $table->dropColumn('id_user_problema');
            $table->dropColumn('id_user_quita_problema');
        });
    }
}
