<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateWwMleTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('ww_mle', function(Blueprint $table)
		{
			$table->integer('id', true);
			$table->string('codigo', 20);
			$table->text('nombre', 65535);
			$table->integer('valor');
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('ww_mle');
	}

}
