<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateValorizacionTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('valorizacion', function(Blueprint $table)
		{
			$table->integer('id', true);
			$table->timestamp('fecha_ingreso')->default(DB::raw('CURRENT_TIMESTAMP'));
			$table->integer('id_usuario');
			$table->string('rut', 20);
			$table->string('ficha', 100);
			$table->string('nombre', 200);
			$table->string('paterno', 200);
			$table->string('materno', 200);
			$table->date('fecha_nacimiento');
			$table->string('prevision', 20);
			$table->string('modalidad_atencion', 100);
			$table->date('fecha_desde');
			$table->date('fecha_hasta');
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('valorizacion');
	}

}
