<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateDocumentoMotivoProblemaTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('documento_motivo_problema', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('id_documento')->index('id_documento');
            $table->integer('id_motivo_problema')->index('id_motivo_problema');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('documento_motivo_problema');
    }
}
