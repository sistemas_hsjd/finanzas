<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AlterDevengosTableAddColumnIdItemPresupuestarioPrevio extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('devengos', function (Blueprint $table) {
            $table->bigInteger('id_item_presupuestario_previo')->index('id_item_presupuestario_previo')->nullable()->after('id_item_presupuestario');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('devengos', function (Blueprint $table) {
            $table->dropColumn('id_item_presupuestario_previo');
        });
    }
}
