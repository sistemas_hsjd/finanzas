<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateArchivoSiiTxtTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('archivo_sii_txt', function(Blueprint $table)
		{
			$table->integer('id', true);
			$table->timestamp('ingreso')->default(DB::raw('CURRENT_TIMESTAMP'));
			$table->integer('id_usuario')->index('id_usuario');
			$table->string('nombre', 200);
			$table->string('nombre_original', 200);
			$table->string('ubicacion', 250);
			$table->string('extension', 10);
			$table->bigInteger('peso');
			$table->boolean('eliminado')->default(0);
			
			$table->boolean('cargado')->default(0);
			$table->timestamps();
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('archivo_sii_txt');
	}

}
