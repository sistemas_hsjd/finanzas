<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateNominasPagoTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('nominas_pago', function (Blueprint $table) {
            $table->bigIncrements('id');

            $table->bigInteger('saldo_disponible')->nullable();
            $table->bigInteger('sumatoria_nomina_pago')->nullable();
            $table->bigInteger('saldo_pago')->nullable()->comment('diferencia entre saldo disponible y sumatoria nomina pago');

            $table->date('fecha_nomina');        
            $table->integer('mes_nomina');
            $table->integer('año_nomina');

            $table->softDeletes();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('nominas_pago');
    }
}
