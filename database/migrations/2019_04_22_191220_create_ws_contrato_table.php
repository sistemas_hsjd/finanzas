<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateWsContratoTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('ws_contrato', function(Blueprint $table)
		{
			$table->integer('id', true);
			$table->timestamp('fecha_carga')->default(DB::raw('CURRENT_TIMESTAMP'));
			$table->string('id_licitacion')->nullable();
			$table->date('fecha_ingreso_contrato')->nullable();
			$table->integer('resolucion')->nullable();
			$table->date('fecha_resolucion')->nullable();
			$table->string('nombre_proveedor', 200);
			$table->string('rut_proveedor', 20);
			$table->date('fecha_inicio_contrato')->nullable();
			$table->date('fecha_termino_contrato')->nullable();
			$table->bigInteger('monto_maximo')->nullable();
			$table->timestamps();
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('ws_contrato');
	}

}
