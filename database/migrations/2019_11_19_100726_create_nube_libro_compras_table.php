<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateNubeLibroComprasTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('nube_libros_compras', function (Blueprint $table) {
            $table->increments('id');
            $table->string('nombre');
			$table->string('nombre_original');
			$table->string('ubicacion');
			$table->string('extension', 6);
			$table->bigInteger('peso');

            $table->integer('id_usuario_crea');
            $table->integer('id_usuario_elimina')->nullable();

            $table->softDeletes();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('nube_libros_compras');
    }
}
