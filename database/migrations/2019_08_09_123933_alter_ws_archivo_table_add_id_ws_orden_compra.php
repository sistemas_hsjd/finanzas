<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AlterWsArchivoTableAddIdWsOrdenCompra extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('ws_archivo', function (Blueprint $table) {
            $table->bigInteger('id_ws_orden_compra')->after('id_documento')->nullable()->default(null);
            $table->integer('id_tipo_archivo')->after('id_documento')->nullable()->default(null);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('ws_archivo', function (Blueprint $table) {
            $table->dropColumn('id_ws_orden_compra');
            $table->dropColumn('id_tipo_archivo');
        });
    }
}
