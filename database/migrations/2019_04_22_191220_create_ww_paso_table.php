<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateWwPasoTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('ww_paso', function(Blueprint $table)
		{
			$table->integer('id', true);
			$table->string('codigo', 100);
			$table->string('ca', 100);
			$table->string('nombre', 2000);
			$table->integer('total');
			$table->integer('numero_a');
			$table->integer('numero_b');
			$table->integer('numero_c');
			$table->integer('numero_d');
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('ww_paso');
	}

}
