<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateFoliosSigfeTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('folios_sigfe', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->bigInteger('id_documento')->index('id_documento');
            $table->integer('id_sigfe')->index('id_sigfe');
            $table->date('fecha_sigfe');
            $table->integer('dia_sigfe');
            $table->integer('mes_sigfe');
            $table->integer('year_sigfe');
            $table->text('detalle_sigfe');

            $table->softDeletes();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('folios_sigfe');
    }
}
