<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AlterArchivoAceptaTableAddColumnsForRechazo extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('archivos_acepta', function (Blueprint $table) {
            $table->text('observacion_rechazo', 65535)->nullable()->default(null);
            $table->integer('id_user_rechazo')->nullable()->default(null);
            $table->integer('id_user_quita_rechazo')->nullable()->default(null);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('archivos_acepta', function (Blueprint $table) {
            $table->dropColumn('observacion_rechazo');
            $table->dropColumn('id_user_rechazo');
            $table->dropColumn('id_user_quita_rechazo');
        });
    }
}
