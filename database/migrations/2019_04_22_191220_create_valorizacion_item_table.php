<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateValorizacionItemTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('valorizacion_item', function(Blueprint $table)
		{
			$table->integer('id', true);
			$table->timestamp('fecha_ingreso')->default(DB::raw('CURRENT_TIMESTAMP'));
			$table->integer('id_valorizacion');
			$table->integer('id_item_cobro');
			$table->integer('id_tipo_cobro');
			$table->integer('id_valor_item');
			$table->integer('anio');
			$table->date('fecha_registro');
			$table->integer('valor_unitario');
			$table->integer('cantidad');
			$table->integer('porcentaje_cobro');
			$table->integer('valor_pago');
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('valorizacion_item');
	}

}
