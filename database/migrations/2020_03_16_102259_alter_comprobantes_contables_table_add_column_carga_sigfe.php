<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AlterComprobantesContablesTableAddColumnCargaSigfe extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('comprobantes_contables', function (Blueprint $table) {
            $table->boolean('carga_sigfe')
                  ->after('reversado')
                  ->nullable()
                  ->default(0)
                  ->comment('indica si fue cargado desde SIGFE');
        });

        Schema::table('comprobantes_contables_documentos', function (Blueprint $table) {
            $table->boolean('carga_sigfe')
                  ->after('reversado')
                  ->nullable()
                  ->default(0)
                  ->comment('indica si fue cargado desde SIGFE');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('comprobantes_contables', function (Blueprint $table) {
            $table->dropColumn('carga_sigfe');
        });

        Schema::table('comprobantes_contables_documentos', function (Blueprint $table) {
            $table->dropColumn('carga_sigfe');
        });
    }
}
