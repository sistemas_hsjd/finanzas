<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateCuadraturaTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('cuadratura', function(Blueprint $table)
		{
			$table->integer('id', true);
			$table->integer('id_factura')->index('id_factura');
			$table->boolean('recepcion');
			$table->boolean('contrato');
			$table->boolean('sii');
			$table->boolean('garantia');
			$table->boolean('devengo');
			$table->boolean('orden_compra');
			$table->text('observacion', 65535);
			$table->timestamp('registro')->default(DB::raw('CURRENT_TIMESTAMP'));
			$table->boolean('cuadrado')->default(1);
			$table->boolean('visado')->default(0);
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('cuadratura');
	}

}
