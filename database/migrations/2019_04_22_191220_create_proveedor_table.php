<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateProveedorTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('proveedores', function(Blueprint $table)
		{
			$table->bigIncrements('id');
			$table->string('nombre');
			$table->string('rut', 20);//->unique('rut');
			$table->string('direccion')->nullable();
			$table->string('telefono', 100)->nullable();
			$table->string('email', 100)->nullable();
			$table->string('contacto')->nullable();
			$table->integer('id_banco')->nullable()->default(null);
			$table->integer('id_tipo_cuenta')->nullable()->default(null);
			$table->string('numero_cuenta', 100)->nullable();
			$table->boolean('transferencia')->default(0);
			$table->boolean('activo')->default(0);
			$table->integer('id_user_created')->nullable()->default(null);
			$table->integer('id_user_updated')->nullable()->default(null);
			$table->integer('id_user_deleted')->nullable()->default(null);
			$table->softDeletes();
			$table->timestamps();

			$table->unique(['rut', 'deleted_at']);
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('proveedores');
	}

}
