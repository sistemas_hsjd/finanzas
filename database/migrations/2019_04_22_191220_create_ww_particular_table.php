<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateWwParticularTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('ww_particular', function(Blueprint $table)
		{
			$table->integer('id', true);
			$table->string('codigo', 20);
			$table->text('nombre', 65535);
			$table->integer('valor');
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('ww_particular');
	}

}
