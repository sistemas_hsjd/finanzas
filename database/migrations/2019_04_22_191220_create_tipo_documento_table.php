<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateTipoDocumentoTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('tipos_documento', function(Blueprint $table)
		{
			$table->integer('id', true);
			$table->string('nombre');
			$table->string('sigla', 10);//->unique('sigla');
			$table->integer('id_user_created')->nullable()->default(null);
			$table->integer('id_user_updated')->nullable()->default(null);
			$table->integer('id_user_deleted')->nullable()->default(null);
			$table->softDeletes();
			$table->timestamps();

			$table->unique(['sigla', 'deleted_at']);
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('tipos_documento');
	}

}
