<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateWsArchivoTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('ws_archivo', function(Blueprint $table)
		{
			$table->integer('id', true);
			$table->timestamp('fecha_carga')->default(DB::raw('CURRENT_TIMESTAMP'));
			$table->integer('id_documento');
			$table->string('nombre_archivo');
			$table->string('ubicacion');
			$table->longText('base64');
			$table->timestamps();
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('ws_archivo');
	}

}
