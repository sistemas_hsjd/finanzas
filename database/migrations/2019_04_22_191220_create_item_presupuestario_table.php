<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateItemPresupuestarioTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('items_presupuestarios', function(Blueprint $table)
		{
			$table->bigIncrements('id');
			$table->string('titulo', 11);
			$table->string('subtitulo', 11)->nullable();
			$table->string('item', 11)->nullable();
			$table->string('asignacion', 11)->nullable();
			$table->string('subasignacion', 11)->nullable();
			$table->string('clasificador_presupuestario');
			$table->integer('id_clasificacion')->nullable();
			$table->integer('id_user_created')->nullable()->default(null);
			$table->integer('id_user_updated')->nullable()->default(null);
			$table->integer('id_user_deleted')->nullable()->default(null);
			$table->softDeletes();
			$table->timestamps();
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('items_presupuestarios');
	}

}
