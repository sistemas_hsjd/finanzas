<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AlterWsDocumentoTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('ws_documento', function (Blueprint $table) {
            $table->string('codigo_pedido')
                  ->after('fecha_carga')
                  ->nullable()
                  ->comment('Columna que guarda informacion de tabla acta_recepcion columna CODIGO_PEDIDO');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('ws_documento', function (Blueprint $table) {
            $table->dropColumn('codigo_pedido');
        });
    }
}
