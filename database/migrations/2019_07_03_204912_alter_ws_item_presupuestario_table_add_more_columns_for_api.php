<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AlterWsItemPresupuestarioTableAddMoreColumnsForApi extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('ws_item_presupuestario', function (Blueprint $table) {
            $table->string('descripcion_articulo')->after('item_presupuestario')->nullable()->default(null);
            $table->string('codigo_articulo')->after('descripcion_articulo')->nullable()->default(null);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('ws_item_presupuestario', function (Blueprint $table) {
            $table->dropColumn('descripcion_articulo');
            $table->dropColumn('codigo_articulo');
        });
    }
}
