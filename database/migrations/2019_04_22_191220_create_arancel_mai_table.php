<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateArancelMaiTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('arancel_mai', function(Blueprint $table)
		{
			$table->integer('id', true);
			$table->timestamp('ingreso')->default(DB::raw('CURRENT_TIMESTAMP'));
			$table->string('fonasa', 10);
			$table->string('nombre', 500);
			$table->integer('total');
			$table->integer('pago_a');
			$table->integer('pago_b');
			$table->integer('pago_c');
			$table->integer('pago_d');
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('arancel_mai');
	}

}
