<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateReversasComprobantesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('reversas_comprobantes', function (Blueprint $table) {
            $table->increments('id');
            $table->bigInteger('id_documento')->index('id_documento');
            $table->integer('id_cuenta_contable')->index('id_cuenta_contable')->nullable();
            $table->bigInteger('id_comprobante_contable')->index('id_comprobante_contable')->nullable()
                                                         ->comment('para saber a que comprobante esta asociada la reversa');

            $table->bigInteger('id_comprobante_contable_documento')->index('id_comprobante_contable_documento')->nullable()
                                                         ->comment('para saber a que comprobante esta asociada la reversa');

            $table->bigInteger('monto');
            $table->integer('folio_sigfe')->index('folio_sigfe');
            $table->date('fecha_sigfe');
            $table->integer('dia_sigfe');
            $table->integer('mes_sigfe');
            $table->integer('year_sigfe');
            $table->text('detalle_sigfe');

            $table->softDeletes();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('reversas_comprobantes');
    }
}
