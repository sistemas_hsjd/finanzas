<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateFarmacosPasoTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('farmacos_paso', function(Blueprint $table)
		{
			$table->integer('id', true);
			$table->string('codigo', 100);
			$table->string('nombre', 1000);
			$table->integer('valor_c');
			$table->integer('valor_d');
			$table->integer('valor_particular');
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('farmacos_paso');
	}

}
