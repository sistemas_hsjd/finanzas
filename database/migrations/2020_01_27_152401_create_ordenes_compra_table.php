<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateOrdenesCompraTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('ordenes_compra', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->bigInteger('id_proveedor')->index('id_proveedor');
            $table->bigInteger('id_contrato')->index('id_contrato')->nullable();
            $table->string('numero_oc')->nullable();
            $table->date('fecha_oc')->nullable();

            $table->integer('monto_oc');
            $table->integer('saldo_oc')->nullable();

            $table->string('detalle_oc')->nullable();
            
            $table->date('inicio_vigencia')->nullable();
            $table->date('fin_vigencia')->nullable();

            $table->integer('id_user_created')->nullable()->default(null);
			$table->integer('id_user_updated')->nullable()->default(null);
			$table->integer('id_user_deleted')->nullable()->default(null);

            $table->softDeletes();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('ordenes_compra');
    }
}
