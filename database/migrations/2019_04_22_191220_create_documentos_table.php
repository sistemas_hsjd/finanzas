<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateDocumentosTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		// Ex facturas
		Schema::create('documentos', function(Blueprint $table)
		{
			$table->bigIncrements('id');
			$table->integer('id_estado')->default(1)->index('id_estado');
			$table->timestamp('fecha_ingreso')->default(DB::raw('CURRENT_TIMESTAMP'));
			$table->dateTime('fecha_asignacion')->nullable();
			$table->date('fecha_recepcion');
			$table->bigInteger('id_proveedor')->index('id_proveedor');
			$table->integer('id_factoring')->default(null)->nullable();
			$table->bigInteger('numero_documento')->index('numero_documento');
			$table->integer('id_tipo_documento')->index('id_tipo_documento');
			$table->date('fecha_documento'); //$table->date('fecha_factura');
			$table->integer('total_documento'); //$table->integer('total_factura');
			$table->integer('id_modalidad_compra')->index('id_modalidad_compra');
			$table->integer('id_tipo_adjudicacion')->index('id_tipo_adjudicacion');
			$table->string('documento_compra', 150);
			$table->integer('id_tipo_informe')->index('id_tipo_informe');
			$table->integer('id_responsable')->index('id_responsable');
			$table->integer('id_referente_tecnico')->nullable()->index('id_referente_tecnico'); //ex id_servicio
			$table->string('licitacion', 150)->nullable();
			$table->text('detalle_documento', 65535)->nullable(); //$table->text('detalle_factura', 65535);
			$table->integer('id_relacionado')->nullable();
			$table->bigInteger('id_refactura')->default(null)->nullable();
			$table->integer('id_digitador')->index('id_digitador');
			$table->integer('id_devengador')->nullable()->index('id_devengador');
			$table->integer('id_sigfe')->nullable()->index('id_sigfe');
			$table->date('year_sigfe')->nullable();
			$table->date('fecha_sigfe')->nullable();
			$table->text('observacion', 65535)->nullable();
			$table->text('detalle', 65535)->nullable();
			$table->integer('id_visto_bueno')->nullable()->index('id_visto_bueno');
			$table->string('nota_visto_bueno')->nullable()->default(null);
			$table->integer('id_cuadratura')->nullable()->index('id_cuadratura');
			// $table->boolean('bo_eliminado')->default(0);
			$table->unique(['id_tipo_documento','id_sigfe','year_sigfe'], 'id_sigfe_unico');
			$table->unique(['id_proveedor','numero_documento','id_tipo_documento'], 'id_documento');
			$table->integer('id_user_created')->nullable()->default(null);
			$table->integer('id_user_updated')->nullable()->default(null);
			$table->integer('id_user_deleted')->nullable()->default(null);
			$table->softDeletes();
			$table->timestamps();
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('documentos');
	}

}
