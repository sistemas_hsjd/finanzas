<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AlterDocumentosAndArchivosAceptaTableAddColumnReclamo extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('documentos', function (Blueprint $table) {
            $table->boolean('reclamado')->after('id_user_deleted')->nullable()->default(null);
        });

        Schema::table('archivos_acepta', function (Blueprint $table) {
            $table->boolean('reclamado')->after('id_nota_credito_menor_100_porciento')->nullable()->default(null);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('documentos', function (Blueprint $table) {
            $table->dropColumn('reclamado');
        });

        Schema::table('archivos_acepta', function (Blueprint $table) {
            $table->dropColumn('reclamado');
        });
    }
}
