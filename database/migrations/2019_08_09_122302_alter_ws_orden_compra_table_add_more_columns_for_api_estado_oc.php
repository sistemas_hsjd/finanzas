<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AlterWsOrdenCompraTableAddMoreColumnsForApiEstadoOc extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('ws_orden_compra', function (Blueprint $table) {
            $table->boolean('supera_mil_utm')->after('fecha_orden_compra_mercado_publico')->nullable()->default(null);
            $table->boolean('firma_abastecimiento')->after('supera_mil_utm')->nullable()->default(null);
            $table->boolean('firma_sda')->after('firma_abastecimiento')->nullable()->default(null);
            $table->boolean('firma_direccion')->after('firma_sda')->nullable()->default(null);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('ws_orden_compra', function (Blueprint $table) {
            $table->dropColumn('supera_mil_utm');
            $table->dropColumn('firma_abastecimiento');
            $table->dropColumn('firma_sda');
            $table->dropColumn('firma_direccion');
        });
    }
}
