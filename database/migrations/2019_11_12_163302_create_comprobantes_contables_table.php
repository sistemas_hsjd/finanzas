<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateComprobantesContablesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('comprobantes_contables', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->integer('id_tipo_comprobante_contable');
            $table->bigInteger('numero_comprobante')->nullable();
            $table->bigInteger('folio')->nullable();
            $table->string('titulo')->nullable();
            $table->text('descripcion')->nullable();
            $table->bigInteger('monto_total')->nullable();

            $table->date('fecha_proceso');
            $table->integer('dia_proceso');
            $table->integer('mes_proceso');
            $table->integer('año_proceso');

            $table->integer('id_user_created')->nullable()->default(null);

            $table->boolean('reversado')->nullable()->default(0);
            
            $table->softDeletes();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('comprobantes_contables');
    }
}
