<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateItemCobroTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('item_cobro', function(Blueprint $table)
		{
			$table->integer('id', true);
			$table->string('nombre', 2000);
			$table->string('codigo', 100)->unique('codigo');
			$table->string('identificador', 100);
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('item_cobro');
	}

}
