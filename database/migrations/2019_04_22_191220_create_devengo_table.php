<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateDevengoTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('devengos', function(Blueprint $table)
		{
			$table->integer('id', true);
			$table->bigInteger('id_documento');
			$table->bigInteger('id_item_presupuestario');
			$table->bigInteger('monto');
			$table->timestamp('registro')->default(DB::raw('CURRENT_TIMESTAMP'));
			$table->softDeletes();
			$table->timestamps();
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('devengos');
	}

}
