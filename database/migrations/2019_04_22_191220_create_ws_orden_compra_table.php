<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateWsOrdenCompraTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('ws_orden_compra', function(Blueprint $table)
		{
			$table->integer('id', true);
			$table->timestamp('fecha_carga')->default(DB::raw('CURRENT_TIMESTAMP'));
			$table->integer('id_contrato')->default(null)->nullable();
			$table->integer('id_orden_compra');
			$table->date('fecha_orden_compra');
			$table->date('fecha_recepcion_orden_compra');
			$table->string('tipo_compra');
			$table->string('programa')->default(null)->nullable();
			$table->bigInteger('numero_orden_compra');
			$table->bigInteger('total_orden_compra');
			$table->timestamps();
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('ws_orden_compra');
	}

}
