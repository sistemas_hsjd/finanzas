<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AlterArchivosAceptaTableAddNewColumnsForChargeAcepta extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('archivos_acepta', function (Blueprint $table) {

            $table->text('fmapago', 65535)->nullable()->after('mensaje_nar_2');
            $table->text('controller', 65535)->nullable()->after('fmapago');
            $table->text('estado_reclamo', 65535)->nullable()->after('controller');
            $table->text('fecha_reclamo', 65535)->nullable()->after('estado_reclamo');
            $table->text('mensaje_reclamo', 65535)->nullable()->after('fecha_reclamo');
            $table->text('estado_devengo', 65535)->nullable()->after('mensaje_reclamo');
            $table->text('codigo_devengo', 65535)->nullable()->after('estado_devengo');
            $table->text('folio_oc', 65535)->nullable()->after('codigo_devengo');
            $table->text('fecha_ingreso_oc', 65535)->nullable()->after('folio_oc');
            $table->text('folio_rc', 65535)->nullable()->after('fecha_ingreso_oc');
            $table->text('fecha_ingreso_rc', 65535)->nullable()->after('folio_rc');
            $table->text('ticket_devengo', 65535)->nullable()->after('fecha_ingreso_rc');
            $table->text('folio_sigfe', 65535)->nullable()->after('ticket_devengo');
            $table->text('tarea_actual', 65535)->nullable()->after('folio_sigfe');            

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('archivos_acepta', function (Blueprint $table) {
            
            $table->dropColumn('fecha_arm');
            $table->dropColumn('fmapago');
            $table->dropColumn('controller');
            $table->dropColumn('estado_reclamo');
            $table->dropColumn('fecha_reclamo');
            $table->dropColumn('mensaje_reclamo');
            $table->dropColumn('estado_devengo');
            $table->dropColumn('codigo_devengo');
            $table->dropColumn('folio_oc');
            $table->dropColumn('fecha_ingreso_oc');
            $table->dropColumn('folio_rc');
            $table->dropColumn('fecha_ingreso_rc');
            $table->dropColumn('ticket_devengo');
            $table->dropColumn('folio_sigfe');
            $table->dropColumn('tarea_actual');

        });
    }
}
