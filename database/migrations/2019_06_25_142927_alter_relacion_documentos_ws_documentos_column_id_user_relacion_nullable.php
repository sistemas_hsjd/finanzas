<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AlterRelacionDocumentosWsDocumentosColumnIdUserRelacionNullable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('relacion_documentos_ws_documentos', function (Blueprint $table) {
            //$table->integer('id_user_relacion')->index('id_user_relacion')->nullable();
            DB::statement('ALTER TABLE `relacion_documentos_ws_documentos` MODIFY `id_user_relacion` INTEGER UNSIGNED NULL;');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('relacion_documentos_ws_documentos', function (Blueprint $table) {
            //$table->integer('id_user_relacion')->index('id_user_relacion')->nullable();
            DB::statement('ALTER TABLE `relacion_documentos_ws_documentos` MODIFY `id_user_relacion` INTEGER UNSIGNED NOT NULL;');
        });
    }
}
