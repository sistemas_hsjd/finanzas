<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateValorItemTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('valor_item', function(Blueprint $table)
		{
			$table->integer('id', true);
			$table->integer('id_tipo_cobro');
			$table->integer('id_item_cobro');
			$table->integer('valor');
			$table->integer('anio');
			$table->integer('id_item_contable')->default(0);
			$table->unique(['id_tipo_cobro','id_item_cobro','anio'], 'id_tipo_cobro');
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('valor_item');
	}

}
