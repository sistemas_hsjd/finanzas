<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateRelacionDocumentoHermesDocumentoTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('relacion_documento_hermes_documento', function (Blueprint $table) {
            $table->increments('id');
            $table->bigInteger('id_documento')->index('id_documento');
            $table->bigInteger('id_hermes_documento')->index('id_hermes_documento');
            $table->integer('id_user_relacion')->index('id_user_relacion')->comment('El que crea la relacion');

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('relacion_documento_hermes_documento');
    }
}
