<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateWsItemPresupuestarioTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('ws_item_presupuestario', function(Blueprint $table)
		{
			$table->integer('id', true);
			$table->timestamp('fecha_carga')->default(DB::raw('CURRENT_TIMESTAMP'));
			$table->integer('id_documento');
			$table->integer('id_producto');
			$table->bigInteger('cantidad_recepcionada');
			$table->bigInteger('valor_item_recepcionado');
			$table->string('item_presupuestario', 100);
			$table->timestamps();
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('ws_item_presupuestario');
	}

}
