<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AlterArchivosAceptaTableAddColumnsToNcMenor100Porciento extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('archivos_acepta', function (Blueprint $table) {
            $table->boolean('nota_credito_menor_100_porciento')->nullable()->after('cargado')->default(0);
            $table->integer('id_nota_credito_menor_100_porciento')->nullable()
                    ->after('nota_credito_menor_100_porciento')->default(null);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('archivos_acepta', function (Blueprint $table) {
            $table->dropColumn('nota_credito_menor_100_porciento');
            $table->dropColumn('id_nota_credito_menor_100_porciento');
        });
    }
}
