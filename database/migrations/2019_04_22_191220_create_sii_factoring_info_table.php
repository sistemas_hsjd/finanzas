<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateSiiFactoringInfoTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('sii_factoring_info', function(Blueprint $table)
		{
			$table->integer('id', true);
			$table->timestamp('fecha_ingreso')->default(DB::raw('CURRENT_TIMESTAMP'));
			$table->string('vendedor', 500);
			$table->string('estado_cesion', 500);
			$table->string('deudor', 500);
			$table->string('mail_deudor', 500);
			$table->string('tipo_doc');
			$table->string('nombre_doc', 500);
			$table->string('folio_doc');
			$table->date('fecha_emis_dte');
			$table->string('mnt_total', 500);
			$table->string('cedente');
			$table->string('rz_cedente', 500);
			$table->string('mail_cedente', 500);
			$table->string('cesionario', 500);
			$table->string('razon_cesionario', 500);
			$table->string('mail_cesionario', 500);
			$table->dateTime('fecha_cesion');
			$table->string('mnt_cesion', 500);
			$table->date('fecha_vencimiento');
			$table->integer('id_factura')->default(0);
			$table->boolean('pagado');
			// $table->unique(['tipo_doc','folio_doc','cedente'], 'tipo_doc');

			$table->timestamps();
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('sii_factoring_info');
	}

}
