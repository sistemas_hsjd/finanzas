<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AlterDevengoTableColumnIdDocumentoIsNullable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('devengos', function (Blueprint $table) {
            $table->bigInteger('id_folio_sigfe')->index('id_folio_sigfe')->nullable()->after('id_documento');
            DB::statement('ALTER TABLE `devengos` MODIFY `id_documento` INTEGER UNSIGNED NULL;');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('devengos', function (Blueprint $table) {
            $table->dropColumn('id_folio_sigfe');
            DB::statement('ALTER TABLE `devengos` MODIFY `id_documento` INTEGER UNSIGNED NOT NULL;');
        });
    }
}
