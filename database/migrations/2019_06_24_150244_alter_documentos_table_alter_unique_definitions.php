<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AlterDocumentosTableAlterUniqueDefinitions extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('documentos', function (Blueprint $table) {
            $table->dropUnique('id_sigfe_unico');
            $table->dropUnique('id_documento');
            
            $table->unique(['id_tipo_documento','id_sigfe','year_sigfe','deleted_at'], 'id_sigfe_unico');
			$table->unique(['id_proveedor','numero_documento','id_tipo_documento','deleted_at'], 'id_documento');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('documentos', function (Blueprint $table) {
            $table->dropUnique('id_sigfe_unico');
            $table->dropUnique('id_documento');
            
            $table->unique(['id_tipo_documento','id_sigfe','year_sigfe','deleted_at'], 'id_sigfe_unico');
            $table->unique(['id_proveedor','numero_documento','id_tipo_documento','deleted_at'], 'id_documento');
        });
    }
}
