<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateArchivoAceptaTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('archivos_acepta', function(Blueprint $table)
		{
			$table->integer('id', true);
			$table->integer('id_archivo_excel')->index('id_archivo_excel');
			$table->string('tipo', 10);
			$table->string('tipo_documento');
			$table->integer('id_tipo_documento')->index('id_tipo_documento')->nullable();
			$table->string('folio', 20);
			$table->string('emisor', 20);
			$table->text('razon_social_emisor');
			$table->integer('id_proveedor')->index('id_proveedor')->nullable();
			$table->text('receptor', 65535);
			$table->text('publicacion', 65535);
			$table->text('emision', 65535);
			$table->text('monto_neto', 65535);
			$table->text('monto_exento', 65535);
			$table->text('monto_iva', 65535);
			$table->text('monto_total', 65535);
			$table->text('impuestos', 65535)->nullable();
			$table->text('estado_acepta', 65535);
			$table->text('estado_sii', 65535);
			$table->text('estado_intercambio', 65535)->nullable();
			$table->text('informacion_intercambio', 65535)->nullable();
			$table->text('uri', 65535);
			$table->text('referencias', 65535)->nullable();
			$table->text('mensaje_nar', 65535)->nullable();
			$table->text('uri_nar', 65535)->nullable();
			$table->text('uri_arm', 65535)->nullable();
			$table->text('fecha_arm', 65535)->nullable();
			$table->text('condicion_pago', 65535)->nullable();
			$table->text('fecha_vencimiento', 65535)->nullable();
			$table->text('estado_cesion', 65535)->nullable();
			$table->text('url_correo_cesion', 65535)->nullable();
			$table->text('fecha_cesion', 65535)->nullable();
			$table->text('fecha_recepcion_sii', 65535)->nullable();
			$table->text('estado_reclamo_mercaderia', 65535)->nullable();
			$table->text('fecha_reclamo_mercaderia', 65535)->nullable();
			$table->text('estado_reclamo_contenido', 65535)->nullable();
			$table->text('fecha_reclamo_contenido', 65535)->nullable();
			$table->text('estado_nar', 65535)->nullable();
			$table->text('fecha_nar', 65535)->nullable();
			$table->text('mensaje_nar_2', 65535)->nullable();
			$table->boolean('cargado')->default(0);
			$table->unique(['tipo','folio','emisor'], 'tipo');
			$table->timestamps();
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('archivos_acepta');
	}

}
