<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateReferentesTecnicosTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		// Ex servicios
		Schema::create('referentes_tecnicos', function(Blueprint $table)
		{
			$table->integer('id', true);
			$table->integer('id_user')->index('id_user')->nullable()->default(null);
			$table->string('rut', 20)->default(null)->nullable();
			$table->string('nombre');
			$table->string('url_firma')->nullable()->default(null);
			$table->string('responsable');
			$table->boolean('origen')->default(0);
			$table->integer('id_user_created')->nullable()->default(null);
			$table->integer('id_user_updated')->nullable()->default(null);
			$table->integer('id_user_deleted')->nullable()->default(null);
			$table->softDeletes();
			$table->timestamps();
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('referentes_tecnicos');
	}

}
