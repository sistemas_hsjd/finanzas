<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateArchivoTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('archivos', function(Blueprint $table)
		{
			$table->integer('id', true);
			$table->integer('id_tipo_ingreso')->default(1);
			$table->integer('id_documento')->index('id_documento')->nullable();
			$table->integer('id_visto_bueno')->index('id_visto_bueno')->nullable();
			$table->integer('id_tipo_archivo')->index('id_tipo_archivo');
			$table->string('nombre', 200);
			$table->string('nombre_original', 200);
			$table->string('ubicacion', 250);
			$table->string('extension', 10);
			$table->bigInteger('peso');
			$table->boolean('eliminado')->default(0);
			$table->boolean('cargado')->default(0);
			$table->softDeletes();
			$table->timestamps();
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('archivos');
	}

}
