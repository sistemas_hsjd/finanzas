<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AlterHermesDocumentosTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('hermes_documentos', function (Blueprint $table) {

            $table->bigInteger('id_proveedor')
                  ->nullable()
                  ->change();

            $table->string('rut_proveedor', 20)
                   ->after('id_proveedor')
                   ->nullable();

            $table->string('codigo_pedido')
                  ->after('tipo_documento')
                  ->nullable()
                  ->comment('Columna que guarda informacion de tabla acta_recepcion columna CODIGO_PEDIDO');

            $table->integer('resolucion_contrato')
                  ->after('codigo_pedido')
                  ->nullable()
                  ->comment('guarda la resolucion del contrato de hermes');

            $table->boolean('enviado')
                  ->after('fecha_validacion_recepcion')
                  ->nullable()
                  ->default(0);


        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('hermes_documentos', function (Blueprint $table) {

            $table->dropColumn('rut_proveedor');
            $table->dropColumn('codigo_pedido');
            $table->dropColumn('resolucion_contrato');
            $table->dropColumn('enviado');

        });
    }
}
