<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AlterWsContratoTableAddMoreColumnsForApi extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('ws_contrato', function (Blueprint $table) {
            $table->bigInteger('monto_licitacion')->after('monto_maximo')->nullable()->default(null);
            $table->bigInteger('saldo_licitacion')->after('monto_licitacion')->nullable()->default(null);
            $table->string('unidad_licitacion')->after('saldo_licitacion')->nullable()->default(null);
            $table->string('referente_tecnico')->after('unidad_licitacion')->nullable()->default(null);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('ws_contrato', function (Blueprint $table) {
            $table->dropColumn('monto_licitacion');
            $table->dropColumn('saldo_licitacion');
            $table->dropColumn('unidad_licitacion');
            $table->dropColumn('referente_tecnico');
        });
    }
}
