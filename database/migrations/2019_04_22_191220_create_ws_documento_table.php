<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateWsDocumentoTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('ws_documento', function(Blueprint $table)
		{
			$table->integer('id', true);
			$table->timestamp('fecha_carga')->default(DB::raw('CURRENT_TIMESTAMP'));
			$table->integer('id_orden_compra');
			$table->integer('documento');
			$table->string('tipo_documento', 20);
			$table->string('nombre_usuario_responsable');
			$table->bigInteger('documento_descuento_total');
			$table->bigInteger('documento_neto');
			$table->bigInteger('documento_iva');
			$table->bigInteger('documento_total');
			$table->timestamps();
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('ws_documento');
	}

}
