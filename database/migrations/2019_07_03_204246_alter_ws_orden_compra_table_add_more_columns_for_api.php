<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AlterWsOrdenCompraTableAddMoreColumnsForApi extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('ws_orden_compra', function (Blueprint $table) {
            $table->bigInteger('saldo_orden_compra')->after('total_orden_compra')->nullable()->default(null);
            $table->string('numero_orden_compra_mercado_publico')->after('saldo_orden_compra')->nullable()->default(null);
            $table->date('fecha_orden_compra_mercado_publico')->after('numero_orden_compra_mercado_publico')->nullable()->default(null);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('ws_orden_compra', function (Blueprint $table) {
            $table->dropColumn('saldo_orden_compra');
            $table->dropColumn('numero_orden_compra_mercado_publico');
            $table->dropColumn('fecha_orden_compra_mercado_publico');
        });
    }
}
