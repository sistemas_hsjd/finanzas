<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AlterDocumentosTableAddColumnValidado extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('documentos', function (Blueprint $table) {
            $table->boolean('validado')->nullable()->default(null)->after('id_relacion');
            $table->dateTime('fecha_validado')->nullable()->default(null)->after('validado');
            $table->integer('id_user_validado')->nullable()->default(null)->after('fecha_validado');
            
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('documentos', function (Blueprint $table) {
            $table->dropColumn('validado');
            $table->dropColumn('fecha_validado');
            $table->dropColumn('id_user_validado');
        });
    }
}
