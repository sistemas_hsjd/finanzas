<link href="{{ asset('sistema/css/modales.css') }} " rel="stylesheet" type="text/css"/>
<div class="modal fade" id="modalCrear" role="dialog" aria-labelledby="myModalLabel3" aria-hidden="true">
	<div class="modal-dialog" style="width:1100px;">
		<div class="modal-content">
            <div class="modal-header">
				<button type="button" title="Cerrar" class="close" data-dismiss="modal" aria-hidden="true"></button>
				<h4 class="modal-title" ><strong><i class="fas fa-money-check"></i>&nbsp;&nbsp;Crear Cuenta Bancaria</strong></h4>
            </div>

            <form action="{{ asset('cuenta_bancaria') }}" method="post" class="horizontal-form" id="form" autocomplete="off">
                <div class="modal-body">

                    <div class="form-body">
                        <div class="note note-danger" id="divErrores" style="display:none;">
                            <h4 class="block" style="margin-bottom:5 px;">Debe Completar los siguientes campos: </h4>
                            <ul id="ulErrores"></ul>
                        </div>

                        <div class="form-group row">
                            <label for="codigo" class="col-sm-2 control-label label-form">Código <span class="span-label">*</span></label>
                            <div class="col-sm-3 form-validate">
                                <input type="text" class="form-control" id="codigo" name="codigo" required >
                            </div>

                            <label for="banco" class="col-sm-2 control-label label-form">Banco <span class="span-label">*</span></label>
                            <div class="col-sm-4 form-validate">
                                <select name="banco" id="banco" class="form-control select2" required>
                                <option value=''>Seleccione</option>
                                @foreach ( $bancos as $banco)
                                    <option value="{{ $banco->id }}">{{ $banco->nombre }}</option>
                                @endforeach
                                </select>
                            </div>
                        </div>

                        <div class="form-group row">
                            <label for="saldo" class="col-xs-2 col-sm-2 col-md-2 col-lg-2 control-label label-form">Saldo <span class="span-label">*</span></label>
                            <div class="col-xs-3 col-sm-3 col-md-3 col-lg-3 form-validate">
                                <div class="input-group">
                                    <span class="input-group-addon">$</span>
                                    <input type="text" class="form-control text-right numero_documento" id="saldo" name="saldo" required onkeyup="ingresoPesos(this);" autocomplete="off">
                                </div>
                            </div>
                        </div>

                    </div>
                </div>

                <div class="modal-footer form-actions right">
                    @csrf

                    <button type="button" title="Cancelar" class="btn btn-default" data-dismiss="modal">Cancelar</button>
                    <button type="submit" title="Guardar Cuenta Contable" class="btn btn-success" id="botonGuardar"><i class="far fa-save" style="color:black;"></i> Guardar Cuenta Bancaria</button>
                </div>
            </form>

        </div>
    </div>
</div>

<script>
    function ingresoPesos(input) {
        $("#" + input.id).val(formatMoney(($("#" + input.id).val()).replace(/\./g, '')));
    }

    $(document).ready(function() {
        $(".select2").select2();

        $(".numero_documento").keypress(function (key) {
            // window.console.log(key.charCode); //Descomentar para ver codigo
            if (
                (key.charCode < 48 || key.charCode > 57)//números
                && (key.charCode != 0) //borrar y enter
                && (key.charCode != 44) //coma
                && (key.charCode != 46) //punto
                && (key.charCode != 45) //guion
                && (key.charCode != 43) //suma
                )
                //console.log(key.charCode);
                return false;
        });

        $("#form").validate({
            highlight: function(element) {
                $(element).closest('.form-validate').removeClass('has-success');
                $(element).closest('.form-validate').addClass('has-error');
            },
            unhighlight: function(element) {
                $(element).closest('.form-validate').removeClass('has-error');
                $(element).closest('.form-validate').addClass('has-success');
            },
            errorElement: 'span',
            errorClass: 'help-block',
            errorPlacement: function (error, element) {
                console.log(element);
                if (element.parent('.input-group').length) {
                    error.insertAfter(element.parent());
                } else {
                    error.insertAfter(element);
                }
            },
            rules: {
            },
            messages: {
            },

            //para enviar el formulario por ajax
            submitHandler: function(form) {
                esperandoGuardar();

                let formData = new FormData(form);

                $.ajax({
                    url: form.action,
                    type: form.method,
                    //data: $(form).serialize(),
                    data: formData,
                    processData: false,
                    contentType: false,
                    success: function(respuesta) {
                        console.log(respuesta);

                        if ( respuesta.estado == 'error' ) {
                            toastr.error('No es posible realizar la acción'+'<br><strong>Error: '+ respuesta.mensaje +'</strong>', 'Atención', optionsToastr);
                        } else if ( respuesta.estado == 'success') {
                            
                            toastr.success(respuesta.mensaje, 'Atención', optionsToastr);
                            // Agregar la informacion de la fila a la tabla principal
                            nuevaFilaTabla(respuesta);
                            $("#modalCrear").modal("hide");
                            
                        }
                        
                    }            
                }).fail( function(respuesta) {//fail ajax
                    if ( respuesta.status == 400 ) {
                        mostrarErroresValidator(respuesta);
                    } else if ( respuesta.status == 500 ) {
                        toastr.error('No es posible realizar la acción, error en el servidor', 'Atención', optionsToastr);
                    } else {
                        toastr.error('No es posible realizar la acción', 'Atención', optionsToastr);
                    }
                    
                })
                .always(function() {
                    listoGuardar();
                });//ajax
                
            }

        });

    });

    function esperandoGuardar()
    {   
        $('.page-header-fixed *').css('cursor', 'wait');
        $("#botonGuardar").attr("disabled",true);
    }

    function listoGuardar()
    {
        $('.page-header-fixed *').css('cursor', '');
        $("#botonGuardar").attr("disabled",false);
    }

    function mostrarErroresValidator(respuesta)
    {
        if ( respuesta.responseJSON ) {
            //console.log(respuesta.responseJSON);
            let htmlErrores = '';
            for (let k in respuesta.responseJSON) { 
                //console.log(k, respuesta.responseJSON[k]);
                htmlErrores += '<li>' + respuesta.responseJSON[k] + '</li>';
            }

            $('#ulErrores').html(htmlErrores);
            $('#divErrores').css('display','');
            toastr.error('No es posible realizar la acción'+'<br>'+'Errores:<br> <ul>'+htmlErrores+'</ul>', 'Atención', optionsToastr);
        }
    }

    function nuevaFilaTabla(respuesta)
    {

        let botones = '<td width="10%"><div class="btn-group">';
        {{-- @permission(['ver-cuenta-bancaria']) --}}
        botones += '<button class="btn btn-success btn-xs" title="Ver Cuenta" onclick="ver('+ respuesta.id +');">';
        botones += '<i class="fas fa-eye"></i></button>';
        {{-- @endpermission
         @permission(['editar-cuenta-bancaria']) --}}
        botones += '<button class="btn btn-warning btn-xs" title="Editar Cuenta" onclick="editar('+ respuesta.id +');">';
        botones += '<i class="fas fa-edit"></i></button>';
        {{-- @endpermission
        @permission(['eliminar-cuenta-bancaria']) --}}
        botones += '<button class="btn btn-danger btn-xs" title="Eliminar Cuenta" onclick="eliminar('+ respuesta.id +');">';
        botones += '<i class="fas fa-trash"></i></button>';
        {{-- @endpermission --}}
        botones += '</div></td>';

        let rowNode = tablaPrincipal.row.add ([
            respuesta.id,
            respuesta.codigo,
            respuesta.banco,
            respuesta.saldo,
            botones
        ]).draw().node();

        $(rowNode).find('td').eq(0).addClass('text-center');

        // Se entrega el id del elemento a la fila, para poder eliminar o editar posteriormente
        $( rowNode ).attr("id", 'tr_'+ respuesta.id);
    }

</script>