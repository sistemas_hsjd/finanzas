<link href="{{ asset('sistema/css/modales.css') }} " rel="stylesheet" type="text/css"/>
<div class="modal fade" id="modalVer" role="dialog" aria-labelledby="myModalLabel3" aria-hidden="true">
	<div class="modal-dialog" style="width:1000px;">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" title="Cerrar" class="close" data-dismiss="modal" aria-hidden="true"></button>
				<h4 class="modal-title" ><strong><i class="fas fa-money-check-alt"></i>&nbsp;&nbsp;Ver Cuenta Bancaria</strong></h4>
            </div>
          
            <div class="modal-body">
                <div class="form-group row">
                    <div class="col-xs-12">
                        <label class="col-sm-2 control-label font-bold">Código</label>
                        <label class="col-sm-3 control-label">{{ $cuenta->codigo }}</label>

                        <label class="col-sm-2 control-label font-bold">Banco</label>
                        <label class="col-sm-4 control-label">{{ $cuenta->getBanco->nombre }}</label>
                    </div>
                </div>

                <div class="form-group row">
                    <div class="col-xs-12">
                        <label class="col-sm-2 control-label font-bold">Saldo</label>
                        <label class="col-sm-3 control-label">{{ formatoMiles($cuenta->saldo) }}</label>
                    </div>
                </div>
              
            </div>{{-- /modal-body --}}

            <div class="modal-footer form-actions">
                <div class="btn-group">
                <button type="button" class="btn btn-info" title="Cerrar" data-dismiss="modal" aria-hidden="true">Cerrar</button>
                </div>
            </div>
          	
		</div>
	</div>
</div>

<script type="text/javascript">

 	$(document).ready(function(){
         
  
 	});

    
 </script>