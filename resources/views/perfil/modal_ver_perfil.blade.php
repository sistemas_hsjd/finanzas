<link href="{{ asset('sistema/css/modales.css') }} " rel="stylesheet" type="text/css"/>
<style>
#tabla_documentos thead tr th {
    font-size: 11px;
    font-weight: 600;
}
</style>
<div class="modal fade" id="modalVer" role="dialog" aria-labelledby="myModalLabel3" aria-hidden="true">
	<div class="modal-dialog" style="width:1000px;">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" title="Cerrar" class="close" data-dismiss="modal" aria-hidden="true"></button>
				<h4 class="modal-title" ><strong><i class="fas fa-user-tag"></i>&nbsp;&nbsp;Ver Perfil</strong></h4>
            </div>

            <form action="#" method="post" class="horizontal-form" id="form" autocomplete="off">
                <div class="modal-body">

                    <div class="form-body" >
                        <div class="note note-danger" id="divErrores" style="display:none;">
                            <h4 class="block" style="margin-bottom:5px;">Debe Completar los siguientes campos: </h4>
                            <ul id="ulErrores"></ul>
                        </div>

                        <h4 class="form-section" style="color: #69aa46;margin-top: 0px;">Permisos</h4>
                        <div class="form-group row">
                            <div class="col-xs-12 table-responsive">
                                <table class="table table-striped table-bordered table-hover " id="tabla_documentos">
                                    <thead>
                                        <tr>
                                            <th width="10%">ID</th>
                                            <th width="30%">Nombre</th>
                                            <th width="60%">Permite al usuario</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                    @forelse ($perfil->permissions as $i => $permiso)
                                        <tr>
                                            <td>{{ $permiso->id }}</td>
                                            <td>{{ $permiso->display_name }}</td>
                                            <td>{{ $permiso->description }}</td>
                                        </tr>
                                    @empty

                                    @endforelse
                                    </tbody>
                                </table>
                            </div>
                        </div>
                        
                        <h4 class="form-section" style="color: #69aa46;">Perfil</h4>

                        <div class="form-group row">
                            <label for="nombre_visual" class="col-sm-3 control-label label-form">Nombre Visual </label>
                            <div class="col-sm-4 form-validate">
                                <input type="text" class="form-control" id="nombre_visual" name="nombre_visual" readonly value="{{ $perfil->display_name }}"  >
                            </div>
                        </div>

                        <div class="form-group row">
                            <label for="nombre" class="col-sm-3 control-label label-form">Nombre (Para Código) </label>
                            <div class="col-sm-4 form-validate">
                                <input type="text" class="form-control" id="nombre" name="nombre" value="{{ $perfil->name }}" readonly >
                            </div>
                        </div>

                        <input type="hidden" name="_token" value="{{ csrf_token() }}">
                        <input type="hidden" name="_id" value="{{ $perfil->id }}">
                    </div>  
                </div>{{-- /modal-body --}}

                
                <div class="modal-footer form-actions right ">
                    <button type="button" title="Cerrar" class="btn btn-default" data-dismiss="modal">Cerrar</button>
                </div> {{-- /modal-footer --}}
            </form>
          	
		</div>
	</div>
</div>

 <script type="text/javascript">
 
    

 	$(document).ready(function(){
  
 	});

   
    
 </script>