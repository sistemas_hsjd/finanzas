@extends('master')

@section('title', 'Mantenedor de Perfiles')

@push('css-head')
<!-- BEGIN PAGE LEVEL STYLES -->
<link rel="stylesheet" type="text/css" href="{{asset('assets/plugins/select2/select2_conquer.css')}}"/>
<link rel="stylesheet" href="{{asset('assets/plugins/data-tables/DT_bootstrap.css')}}"/>
<!-- END PAGE LEVEL STYLES -->

@endpush

@section('content')

<!-- BEGIN CONTENT -->

<div class="page-content-wrapper">
	<div class="page-content-wrapper">
		<div class="page-content">

			<!-- BEGIN PAGE CONTENT-->
			<div class="row">
				<div class="col-md-12">
					<!-- BEGIN EXAMPLE TABLE PORTLET-->
					<div class="portlet">
						<div class="portlet-title">
							<div class="caption">
								<i class="fas fa-user-tag"></i> Mantenedor de Perfiles
							</div>
						</div>
						<div class="portlet-body">

                            <div class="table-toolbar">
                                <div class="btn-group">
                                    @role(['propietario','administrador'])
                                    <button title="Agregar Perfil" class="btn btn-success" onclick="nuevo();">
										<i class="fa fa-plus"></i> Agregar Perfil
									</button>
                                    @endrole
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-12">
                                    <div class="panel">
                                    {{-- 
                                        <div class="panel-heading">
                                            <div class="caption">
                                                <strong style="color: #69aa46;">Últimos Documentos Ingresados</strong>
                                            </div>
                                        </div>
                                        --}}
                                        <div class="panel-body">
                                            <div class="table-responsive">
                                                <table class="table table-striped table-bordered table-hover table-full-width" id="tabla_referentes_tecnicos">
                                                    <thead>
                                                    <tr>
                                                        <th width="5%" >ID</th>
                                                        <th width="5%">Nombre</th>
                                                        <th width="5%">Usuarios</th>
                                                        <th width="77%">Permisos</th>
                                                        <th width="8%">
                                                            <i class="fa fa-cog"></i>
                                                        </th>
                                                    </tr>
                                                    </thead>
                                                    <tbody>
                                                    @forelse($perfiles as $perfil)
                                                    <tr id="tr_{{ $perfil->id }}">
                                                        <td width="5%" class="text-center">{{ $perfil->id }}</td>
                                                        <td width="5%">{{ $perfil->display_name }}</td>
                                                        <td width="5%" class="text-center">{{ count($perfil->users) }}</td>
                                                        <td width="80%">
                                                        @foreach ($perfil->permissions as $i => $permiso)
                                                            <strong>{{ $permiso->display_name }}</strong> | @if( $i != 0 &&  $i%5 == 0 )<br>@endif
                                                        @endforeach
                                                        </td>
                                                        <td width="5%">
                                                            <div class="btn-group">
                                                                @role(['propietario','administrador'])
                                                                <button class="btn btn-success btn-xs" title="Ver Perfil" onclick="ver({{ $perfil->id }});">
                                                                    <i class="fas fa-eye"></i>
                                                                </button>
                                                                <button class="btn btn-warning btn-xs" title="Editar Perfil" onclick="editar({{ $perfil->id }});">
                                                                    <i class="fas fa-edit"></i>
                                                                </button>
                                                                @if( $perfil->id != 1 && $perfil->id != 2 )
                                                                    <button class="btn btn-danger btn-xs" title="Eliminar Perfil" onclick="eliminar({{ $perfil->id }});">
                                                                        <i class="fas fa-trash"></i>
                                                                    </button>
                                                                @endif
                                                                @endrole
                                                                
                                                            </div>
                                                        </td>
                                                    </tr>
                                                    @empty

                                                    @endforelse
                                                    </tbody>
                                                </table>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
						</div>
					</div>
					<!-- END EXAMPLE TABLE PORTLET-->
				</div>
			</div>
			<!-- END PAGE CONTENT-->
			
			
			
			
		</div> <!--/div class="page-content" -->
	</div> <!--/div class="page-content-wrapper" -->
</div><!--/div class="page-content-wrapper" -->
<!-- END CONTENT -->
<div id="modal"></div>
<div id="modal2"></div>
@endsection

@push('script-footer')
<!-- BEGIN PAGE LEVEL PLUGINS -->
<script type="text/javascript" src="{{ asset('assets/plugins/select2/select2.min.js') }} "></script>
{{--<script type="text/javascript" src="{{ asset('assets/plugins/data-tables/jquery.dataTables.min.js') }}"></script>--}}
<script type="text/javascript" src="{{ asset('datatables/jquery.dataTables.min.js') }}"></script>
<script type="text/javascript" src="{{ asset('assets/plugins/data-tables/DT_bootstrap.js') }}"></script>

<link rel="stylesheet" type="text/css" href="{{asset('assets/plugins/bootstrap-datepicker/css/datepicker.css')}}"/>

<script type="text/javascript" src="{{asset('assets/plugins/bootstrap-datepicker/js/bootstrap-datepicker.js')}}"></script>
<script type="text/javascript" src="{{asset('assets/plugins/bootstrap-datetimepicker/js/bootstrap-datetimepicker.js')}}"></script>


<!-- END PAGE LEVEL PLUGINS -->
<!-- BEGIN PAGE LEVEL SCRIPTS -->
<script src="{{ asset('assets/scripts/app.js') }} "></script>
<script src="{{ asset('assets/scripts/table-advanced.js') }} "></script>


<script type="text/javascript">

    let tablaPrincipal = $('#tabla_referentes_tecnicos').DataTable({
        "language": {
            "emptyTable":     "No hay datos disponibles en la tabla",
            "info":           "Mostrando registros del _START_ al _END_ de un total de _TOTAL_ registros",
            "infoEmpty":      "Showing 0 to 0 of 0 entries",
            "infoFiltered":   "(filtrado de un total de _MAX_ registros)",
            "infoPostFix":    "",
            "thousands":      ",",
            "lengthMenu":     "Mostrar _MENU_ registros",
            "loadingRecords": "Cargando...",
            "processing":     "Procesando...",
            "search":         "Buscar: ",
            "zeroRecords":    "No matching records found",
            "paginate": {
                "first":      "Primero",
                "last":       "Último",
                "next":       "Siguiente",
                "previous":   "Anterior"
            },
            "aria": {
                "sortAscending":  ": Activar para ordenar la columna de manera ascendente",
                "sortDescending": ": Activar para ordenar la columna de manera descendente"
            }
        },
        "lengthMenu": [
            [10, 15, 20, -1],
            [10, 15, 20, "Todos"] // change per page values here
        ],
        // set the initial value
        "displayLength": 10,
        "paging":   true,
        "ordering": true,
        "info":     true,
        "searching": true
    });

    jQuery('#tabla_referentes_tecnicos_filter input').addClass("form-control input-large"); // modify table search input
    jQuery('#tabla_referentes_tecnicos_wrapper .dataTables_length select').addClass("form-control input-small"); // modify table per page dropdown
    jQuery('#tabla_referentes_tecnicos_wrapper .dataTables_length select').select2(); // initialize select2 dropdown

    $('#tabla_referentes_tecnicos_column_toggler input[type="checkbox"]').change(function() {
        /* Get the DataTables object again - this is not a recreation, just a get of the object */
        var iCol = parseInt($(this).attr("data-column"));
        var bVis = oTable.fnSettings().aoColumns[iCol].bVisible;
        oTable.fnSetColumnVis(iCol, (bVis ? false : true));
    });

	$(document).ready(function() {
		App.init(); // initlayout and core plugins
		//TableAdvanced.init();
        
   		$("#mantenedores").addClass( "active" );
		$("#perfiles-li").addClass( "active" );
		$("#mantenedores-a").append( '<span class="selected"></span>' );

	});

    function ver(id)
    {
        $.get( '{{ url("perfiles/modal/ver") }}/' + id, function( data ) {
            $( "#modal" ).html( data );
            $( "#modalVer" ).modal();
        });
    }

    function editar(id)
    {
        $.get( '{{ url("perfiles/modal/editar") }}/' + id, function( data ) {
            $( "#modal" ).html( data );
            $( "#modalEditar" ).modal();
        });
    }

    function nuevo()
    {
        $.get( '{{ url("perfiles/create") }}', function( data ) {
            $( "#modal" ).html( data );
            $( "#modalCrear" ).modal();
        });
    }

    function eliminar(id)
    {
        $.get( '{{ url("perfiles/modal/eliminar") }}/' + id, function( data ) {
            $( "#modal" ).html( data );
            $( "#modalEliminar" ).modal();
        });
    }

</script>

@endpush