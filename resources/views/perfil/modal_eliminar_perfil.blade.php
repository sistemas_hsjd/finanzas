<link href="{{ asset('sistema/css/modales.css') }} " rel="stylesheet" type="text/css"/>
<style>
#tabla_documentos thead tr th {
    font-size: 11px;
    font-weight: 600;
}
</style>
<div class="modal fade" id="modalEliminar" role="dialog" aria-labelledby="myModalLabel3" aria-hidden="true">
	<div class="modal-dialog" style="width:1000px;">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" title="Cerrar" class="close" data-dismiss="modal" aria-hidden="true"></button>
				<h4 class="modal-title" ><strong><i class="fas fa-user-tag"></i>&nbsp;&nbsp;Eliminar Perfil</strong></h4>
            </div>

            <form action="{{ asset('perfiles/eliminar') }}" method="post" class="horizontal-form" id="form" autocomplete="off">
                <div class="modal-body">

                    <div class="form-body" >
                        <div class="note note-danger" id="divErrores" style="display:none;">
                            <h4 class="block" style="margin-bottom:5px;">Debe Completar los siguientes campos: </h4>
                            <ul id="ulErrores"></ul>
                        </div>

                        <h4 class="form-section" style="color: #69aa46;margin-top: 0px;">Permisos</h4>
                        <div class="form-group row">
                            <div class="col-xs-12 table-responsive">
                                <table class="table table-striped table-bordered table-hover " id="tabla_documentos">
                                    <thead>
                                        <tr>
                                            <th width="10%">ID</th>
                                            <th width="30%">Nombre</th>
                                            <th width="60%">Permite al usuario</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                    @forelse ($perfil->permissions as $i => $permiso)
                                        <tr>
                                            <td>{{ $permiso->id }}</td>
                                            <td>{{ $permiso->display_name }}</td>
                                            <td>{{ $permiso->description }}</td>
                                        </tr>
                                    @empty

                                    @endforelse
                                    </tbody>
                                </table>
                            </div>
                        </div>
                        
                        <h4 class="form-section" style="color: #69aa46;">Perfil</h4>

                        <div class="form-group row">
                            <label for="nombre_visual" class="col-sm-3 control-label label-form font-bold">Nombre Visual </label>
                            <div class="col-sm-4 form-validate">
                                <input type="text" class="form-control" id="nombre_visual" name="nombre_visual" required value="{{ $perfil->display_name }}" readonly >
                            </div>
                        </div>

                        <div class="form-group row">
                            <label for="nombre" class="col-sm-3 control-label label-form font-bold">Nombre (Para Código) </label>
                            <div class="col-sm-4 form-validate">
                                <input type="text" class="form-control" id="nombre" name="nombre" required value="{{ $perfil->name }}" readonly >
                            </div>
                        </div>

                        <input type="hidden" name="_token" value="{{ csrf_token() }}">
                        <input type="hidden" name="_id" value="{{ $perfil->id }}">
                    </div>  
                </div>{{-- /modal-body --}}

                
                <div class="modal-footer form-actions right ">
                    <button type="button" title="Cancelar" class="btn btn-default" data-dismiss="modal">Cancelar</button>
                    <button type="submit" title="Eliminar Perfil" class="btn btn-danger" id="botonGuardar"><i class="fa fa-trash" ></i> Eliminar Perfil</button>
                </div> {{-- /modal-footer --}}
            </form>
          	
		</div>
	</div>
</div>

 <script type="text/javascript">
 
    

 	$(document).ready(function(){

        $(".select2").select2();

        $("#form").validate({
            highlight: function(element) {
                $(element).closest('.form-validate').removeClass('has-success');
                $(element).closest('.form-validate').addClass('has-error');
            },
            unhighlight: function(element) {
                $(element).closest('.form-validate').removeClass('has-error');
                $(element).closest('.form-validate').addClass('has-success');
            },
            errorElement: 'span',
            errorClass: 'help-block',
            errorPlacement: function (error, element) {
                console.log(element);
                if (element.parent('.input-group').length) {
                    error.insertAfter(element.parent());
                } else {
                    error.insertAfter(element);
                }
            },
            rules: {
            },
            messages: {
            },

            //para enviar el formulario por ajax
            submitHandler: function(form) {
                esperandoGuardar();

                let formData = new FormData(form);
                $.ajax({
                    url: form.action,
                    type: form.method,
                    //data: $(form).serialize(),
                    data: formData,
                    processData: false,
                    contentType: false,
                    success: function(respuesta) {
                        console.log(respuesta);

                        if ( respuesta.estado == 'error' ) {
                            toastr.error('No es posible realizar la acción'+'<br><strong>Error: '+ respuesta.mensaje +'</strong>', 'Atención', optionsToastr);
                        } else if ( respuesta.estado == 'success') {
                            
                            toastr.success(respuesta.mensaje, 'Atención', optionsToastr);
                            // Funcion que utiliza la respuesta para actualizar la tabla principal
                            // actualizaElementoTabla(respuesta);
                            location.reload();

                            $("#modalEliminar").modal("hide");
                            
                        }
                        
                    }            
                }).fail( function(respuesta) {//fail ajax
                    if ( respuesta.status == 400 ) {
                        mostrarErroresValidator(respuesta);
                    } else if ( respuesta.status == 500 ) {
                        toastr.error('No es posible realizar la acción, error en el servidor', 'Atención', optionsToastr);
                    } else {
                        toastr.error('No es posible realizar la acción', 'Atención', optionsToastr);
                    }
                    
                })
                .always(function() {
                    listoGuardar();
                });//ajax
                
            }

        });
  
 	});

    function esperandoGuardar() {
        $('.page-header-fixed *').css('cursor', 'wait');
        $("#botonGuardar").attr("disabled", true);
    }

    function listoGuardar() {
        $('.page-header-fixed *').css('cursor', '');
        $("#botonGuardar").attr("disabled", false);
    }

    function mostrarErroresValidator(respuesta) {
        if (respuesta.responseJSON) {
            //console.log(respuesta.responseJSON);
            let htmlErrores = '';
            for (let k in respuesta.responseJSON) {
                //console.log(k, respuesta.responseJSON[k]);
                htmlErrores += '<li>' + respuesta.responseJSON[k] + '</li>';
            }

            $('#ulErrores').html(htmlErrores);
            $('#divErrores').css('display', '');
            toastr.error('No es posible realizar la acción' + '<br>' + 'Errores:<br> <ul>' + htmlErrores + '</ul>', 'Atención', optionsToastr);
        }
    }

    function actualizaElementoTabla(respuesta) {

        let botones = '<td ><div class="btn-group">';
        botones += '<button class="btn btn-info btn-xs " title="Registrar" onclick="registrarVistoBueno(' + respuesta.id + ');">';
        botones += '<i class="fas fa-file-signature fa-lg"></i></button>';
        botones += '<a class="btn btn-danger btn-xs " title="Ver Memo Solicitud Visto Bueno" target="_blank" href="{{ asset('referente_tecnico/generar/pdf_solicitud_visto_bueno/') }}/'+ respuesta.id + '" >';
        botones += '<i class="fas fa-file-pdf fa-lg"></i></a>';
        botones += '<button class="btn btn-warning btn-xs " title="Editar Memo" onclick="editarMemo(' + respuesta.id + ');">';
        botones += '<i class="fas fa-edit fa-lg"></i></button>';
        botones += '</div></td>';

        tablaPrincipal.row('#tr_' + respuesta.id).data([
            respuesta.fechaSolicitud,
            respuesta.memo,
            respuesta.responsable,
            respuesta.referenteTecnico,
            botones
        ]).draw();

    }

    
 </script>