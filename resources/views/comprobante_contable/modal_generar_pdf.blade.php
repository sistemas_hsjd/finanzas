<link href="{{ asset('sistema/css/modales.css') }} " rel="stylesheet" type="text/css"/>
<div class="modal fade" id="modalGenerarPdf" role="dialog" aria-labelledby="myModalLabel3" aria-hidden="true">
	<div class="modal-dialog" style="width:1100px;">
		<div class="modal-content">
            <div class="modal-header">
				<button type="button" title="Cerrar" class="close" data-dismiss="modal" aria-hidden="true"></button>
				<h4 class="modal-title" ><strong><i class="fas fa-file-pdf fa-lg"></i>&nbsp;<i class="fas fa-stamp fa-lg"></i>&nbsp;&nbsp;Generar PDF del Comprobante</strong></h4>
            </div>

            <form action="{{ url('comprobantes/generar_pdf') }}" method="POST" class="horizontal-form" id="form" autocomplete="off" target="_blank">
                <div class="modal-body">
                    <div class="form-body">
                        <div class="note note-danger" id="divErrores" style="display:none;">
                            <h4 class="block" style="margin-bottom:5 px;">Debe Completar los siguientes campos: </h4>
                            <ul id="ulErrores"></ul>
                        </div>

                        @if( $comprobante->folio )
                            <div class="form-group row">
                                <label for="folio_comprobante" class="col-sm-2 control-label label-form">Folio Comprobante </label>
                                <div class="col-sm-3 form-validate">
                                    <input type="text" class="form-control solo_numeros" id="folio_comprobante" 
                                            name="folio_comprobante" required readonly value="{{ $comprobante->folio }}" >
                                </div>
                            </div>
                        @endif

                        <div class="form-group row">

                            <label for="primer_usuario_autoriza" class="col-sm-2 control-label label-form">1° Usuario Autoriza <span class="span-label">*</span></label>
                            <div class="col-sm-4 form-validate">
                                <select name="primer_usuario_autoriza" id="primer_usuario_autoriza" class="form-control select2" required >
                                @foreach ( $usuarios as $usuario )
                                <option value="{{ $usuario->id }}" @if( $usuario->id == 19 ) selected @endif >{{ $usuario->name }}</option>
                                @endforeach
                                
                                </select>
                            </div>

                            <label for="segundo_usuario_autoriza" class="col-sm-2 control-label label-form">2° Usuario Autoriza <span class="span-label">*</span></label>
                            <div class="col-sm-4 form-validate">
                                <select name="segundo_usuario_autoriza" id="segundo_usuario_autoriza" class="form-control select2" required >
                                @foreach ( $usuarios as $usuario )
                                    <option value="{{ $usuario->id }}" @if( $usuario->id == 18 ) selected @endif >{{ $usuario->name }}</option>
                                @endforeach
                                
                                </select>
                            </div>


                        </div>

                        <div class="form-group row">

                            <label for="usuario_revision" class="col-sm-2 control-label label-form">Usuario Revisión</label>
                            <div class="col-sm-4 form-validate">
                                <select name="usuario_revision" id="usuario_revision" class="form-control select2" >
                                    <option value="">Seleccione usuario</option>
                                    @foreach ( $usuariosRevision as $usuarioRev )
                                        <option value="{{ $usuarioRev->id }}" >{{ $usuarioRev->name }}</option>
                                    @endforeach
                                </select>
                            </div>

                        </div>

                    </div>
                </div>

                <div class="modal-footer form-actions right">
                    <input type="hidden" name="_token" value="{{ csrf_token() }}">
                    <input type="hidden" name="_id" value="{{ $comprobante->id }}">

                    <button type="button" title="Cancelar" class="btn btn-default" data-dismiss="modal">Cancelar</button>
                    <button type="submit" title="Grear PDF" class="btn btn-danger" id="botonGuardar"><i class="fas fa-file-pdf fa-lg"></i> Generar PDF</button>
                </div>
            </form>

        </div>
    </div>
</div>


<script>
    $(document).ready(function() {

        $(".solo_numeros").keypress(function (key) {
            // window.console.log(key.charCode); //Descomentar para ver codigo
            if (
                (key.charCode < 48 || key.charCode > 57)//números
                && (key.charCode != 0) //borrar y enter
                && (key.charCode != 44) //coma
                && (key.charCode != 46) //punto
                && (key.charCode != 45) //guion
                && (key.charCode != 43) //suma
                )
                //console.log(key.charCode);
                return false;
        });

        $('#fecha_proceso').datepicker({
            format: 'dd/mm/yyyy',
            endDate: new Date(),
            autoclose: true,
            language: 'es'
        });

        $(".select2").select2();

        $("#form").validate({
            highlight: function(element) {
                $(element).closest('.form-validate').removeClass('has-success');
                $(element).closest('.form-validate').addClass('has-error');
            },
            unhighlight: function(element) {
                $(element).closest('.form-validate').removeClass('has-error');
                $(element).closest('.form-validate').addClass('has-success');
            },
            errorElement: 'span',
            errorClass: 'help-block',
            errorPlacement: function (error, element) {
                console.log(element);
                if (element.parent('.input-group').length) {
                    error.insertAfter(element.parent());
                } else {
                    error.insertAfter(element);
                }
            },
            rules: {
            },
            messages: {
            },

            //para enviar el formulario por ajax
            submitHandler: function(form) {
                esperandoCrear();

                let formData = new FormData(form);
                
                $.ajax({
                    url: form.action,
                    type: form.method,
                    //data: $(form).serialize(),
                    data: formData,
                    processData: false,
                    contentType: false,
                    success: function(respuesta) {
                        console.log(respuesta);

                        if ( respuesta.estado == 'error' ) {
                            toastr.error('No es posible realizar la acción'+'<br><strong>Error: '+ respuesta.mensaje +'</strong>', 'Atención', optionsToastr);
                        } else if ( respuesta.estado == 'success') {
                            
                            toastr.success(respuesta.mensaje, 'Atención', optionsToastr);

                            $("#modalGenerarPdf").modal("hide");

                        }
                        
                    }            
                }).fail( function(respuesta) {//fail ajax
                    if ( respuesta.status == 400 ) {
                        mostrarErroresValidator(respuesta);
                    } else if ( respuesta.status == 500 ) {
                        toastr.error('No es posible realizar la acción, error en el servidor', 'Atención', optionsToastr);
                    } else {
                        toastr.error('No es posible realizar la acción', 'Atención', optionsToastr);
                    }
                    
                })
                .always(function() {
                    listoCrear();
                });//ajax
                
            }

        });

    });

    function esperandoCrear()
    {   
        $('.page-header-fixed *').css('cursor', 'wait');
        $("#botonGuardar").attr("disabled",true);
    }

    function listoCrear()
    {
        $('.page-header-fixed *').css('cursor', '');
        $("#botonGuardar").attr("disabled",false);
    }

    function mostrarErroresValidator(respuesta)
    {
        if ( respuesta.responseJSON ) {
            //console.log(respuesta.responseJSON);
            let htmlErrores = '';
            for (let k in respuesta.responseJSON) { 
                //console.log(k, respuesta.responseJSON[k]);
                htmlErrores += '<li>' + respuesta.responseJSON[k] + '</li>';
            }

            $('#ulErrores').html(htmlErrores);
            $('#divErrores').css('display','');
            toastr.error('No es posible realizar la acción'+'<br>'+'Errores:<br> <ul>'+htmlErrores+'</ul>', 'Atención', optionsToastr);
        }
    }

</script>