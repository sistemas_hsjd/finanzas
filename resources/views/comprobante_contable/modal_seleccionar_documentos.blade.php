<link href="{{ asset('sistema/css/modales.css') }} " rel="stylesheet" type="text/css"/>

<div class="modal fade" id="modalSeleccionDocumentos" role="dialog" aria-labelledby="myModalLabel3" aria-hidden="true">
	<div class="modal-dialog" style="width:90%;">
		<div class="modal-content">
            <div class="modal-header">
				<button type="button" title="Cerrar" class="close" data-dismiss="modal" aria-hidden="true"></button>
				<h4 class="modal-title" ><strong><i class="fas fa-stamp fa-lg"></i>&nbsp;&nbsp;Seleccionar proveedor</strong></h4>
            </div>

            <form class="horizontal-form" id="form-buscador-documentos" autocomplete="off">
                <div class="modal-body">
                    <div class="form-body">

                        <div class="form-group row">

                            <label for="filtro_proveedor_seleccion_documentos" class="col-sm-1 control-label label-form">Proveedor </label>
                            <div class="col-sm-3 form-validate">
                                <input type="text" class="form-control" id="filtro_proveedor_seleccion_documentos" 
                                        name="filtro_proveedor_seleccion_documentos" placeholder="Buscador por Nombre y RUT" >
                            </div>

                            <label for="filtro_tipo_documento_seleccion_documentos" class="col-sm-1 control-label label-form">Tipo Documento</label>
                            <div class="col-sm-3 form-validate">
                                <select name="filtro_tipo_documento_seleccion_documentos[]" id="filtro_tipo_documento_seleccion_documentos" 
                                        class="form-control select2_filtro_seleccion_documentos" multiple="multiple">
                                    @foreach ( $tiposDocumento as $tipoDoc )
                                        <option value="{{ $tipoDoc->id }}" >{{ $tipoDoc->nombre }}</option>
                                    @endforeach
                                </select>
                            </div>

                            <label for="filtro_cuenta_contable_seleccion_documentos" class="col-sm-1 control-label label-form">Cuenta Contable</label>
                            <div class="col-sm-3 form-validate">
                                <select name="filtro_cuenta_contable_seleccion_documentos[]" id="filtro_cuenta_contable_seleccion_documentos" 
                                        class="form-control select2_filtro_seleccion_documentos" multiple="multiple">
                                    @foreach ( $cuentasContables as $cuentaContable )
                                        <option value="{{ $cuentaContable->id }}" >{{ $cuentaContable->codigo }}</option>
                                    @endforeach
                                </select>
                            </div>

                        </div>

                        <div class="form-group row">

                            <label for="filtro_numero_documento_seleccion_documentos" class="col-sm-1 control-label label-form">N° Documento</label>
                            <div class="col-sm-3 form-validate">
                                <input type="text" class="form-control" id="filtro_numero_documento_seleccion_documentos" 
                                        name="filtro_numero_documento_seleccion_documentos" >
                            </div>

                            <div class="col-md-3 col-md-offset-1">
                                <button type="button" class="btn btn-primary" id="buscarDocumentos" >
                                    <i class="fas fa-search" ></i> Buscar documentos</button>
                            </div>
                        </div>

                    </div> <!-- / form-body-->
                </div> <!-- / modal-body -->
            </form>

            <form action="{{ asset('comprobantes/agregar/documentos') }}" method="post" class="horizontal-form" id="form-seleccionar-documentos" autocomplete="off">
                <div class="modal-body">
                    <div class="form-body">

                        <div class="note note-danger" id="divErrores" style="display:none;">
                            <h4 class="block" style="margin-bottom:5 px;">Debe Completar los siguientes campos: </h4>
                            <ul id="ulErrores"></ul>
                        </div>

                        <div class="form-group row">

                            <label for="cuenta_bancaria" class="col-sm-1 control-label label-form">Cuenta Bancaria <span class="span-label">*</span></label>
                            <div class="col-sm-3 form-validate">
                                <select name="cuenta_bancaria" id="cuenta_bancaria" class="form-control select2" required >
                                <option value=''>Seleccione</option>
                                @foreach ( $cuentas as $cuenta)
                                    <option value="{{ $cuenta->id }}">{{ $cuenta->codigo }} {{ $cuenta->getBanco->nombre }}</option>
                                @endforeach
                                </select>
                            </div>

                        </div>

                        <div class="form-group row" id="div_docs_proveedor">
                            <div class="col-xs-12">
                                <table class="table table-striped table-bordered table-hover table-full-width" id="tabla_docs_para_comprobante" >
                                    <thead>
                                        <tr>
                                            <th >
                                                <div class="checkbox">
                                                    <input class="select_all" type="checkbox" id="todos">
                                                </div> 
                                            </th>
                                            <th >Proveedor</th>
                                            <th >Factoring</th>
                                            <th >Tipo Doc.</th>
                                            <th >N° Documento</th>
                                            <th >N° Documento Compra</th>
                                            <th >Fecha Doc.</th>
                                            <th >Tot. Orig.</th>
                                            <th >Tot. Act.</th>
                                            <th class="hidden-xs text-center" >
                                                <i class="fa fa-cog"></i>
                                            </th>
                                        </tr>
                                    </thead>
                                </table>
                            </div>
                        </div>

                    </div> <!-- / form-body-->
                </div> <!-- / modal-body-->

                <div class="modal-footer form-actions right">
                    <input type="hidden" name="_token" value="{{ csrf_token() }}">
                    <input type="hidden" name="idComprobante" value="{{ $idComprobante }}">

                    <button type="button" title="Cerrar" class="btn btn-default" data-dismiss="modal">Cerrar</button>
                    <button type="submit" title="Guardar" class="btn btn-success" id="botonGuardar"><i class="fas fa-donate" style="color:black"></i> Guardar</button>
                </div>
            </form>

        </div>
    </div>
</div>


<script type="text/javascript">

    $('#modalSeleccionDocumentos').on('hidden.bs.modal', function () {
        var tablaDocsParaComprobante = null;
    });

    var tablaDocsParaComprobantes = $('#tabla_docs_para_comprobante').DataTable({
        // "processing": true,
        // "serverSide": true,
        "ajax": {
            "url" : "{{ url('comprobantes/documentos_para_comprobante') }}",
            "type" : "POST",
            "data": function(d){
                d.form = JSON.stringify( $("#form-buscador-documentos").serializeArray() );
                // console.log('se envia:');
                // console.log(d);
            },
        },
        // Set rows IDs
        rowId: function(data) {
            return 'tr_' + data.DT_RowID;
        },
        "deferRender": true,
        "language": {
            "emptyTable":     "No hay datos disponibles en la tabla",
            "info":           "Mostrando registros del _START_ al _END_ de un total de _TOTAL_ registros",
            "infoEmpty":      "No hay datos disponibles para mostrar en la tabla",
            "infoFiltered":   "(filtrado de un total de _MAX_ registros)",
            "infoPostFix":    "",
            "thousands":      ",",
            "lengthMenu":     "Mostrar _MENU_ registros",
            "loadingRecords": "Cargando...",
            "processing":     "Procesando...",
            "search":         "Buscar: ",
            "zeroRecords":    "<i class='far fa-frown fa-lg'></i> No hay datos que coincidan con la búsqueda <i class='far fa-frown fa-lg'></i>",
            "paginate": {
                "first":      "Primero",
                "last":       "Último",
                "next":       "Siguiente",
                "previous":   "Anterior"
            },
            "aria": {
                "sortAscending":  ": Activar para ordenar la columna de manera ascendente",
                "sortDescending": ": Activar para ordenar la columna de manera descendente"
            }
        },
        "lengthMenu": [
            [10, 15, 20, -1],
            [10, 15, 20, "Todos"] // change per page values here
        ],
        // set the initial value
        "displayLength": 10,
        "paging":   true,
        "ordering": true,
        "info":     true,
        "searching": true,
        "orderClasses": true,
        "cache": true,
        "order": [],
        "columnDefs": [
            { "orderable": false, "targets": 0 }
        ]
    });

    $.fn.dataTable.ext.errMode = 'none';//quita los mensajes de error de la datatable ajax

    $('#tabla_docs_para_comprobante').on( 'error.dt', function ( e, settings, techNote, message ) {
		console.log( 'An error has been reported by DataTables: ', message );//para mostrar los mensajes de error en la consola del navegador
	} );


    jQuery('#tabla_docs_para_comprobante_filter input').addClass("form-control input-large"); // modify table search input
    jQuery('#tabla_docs_para_comprobante_wrapper .dataTables_length select').addClass("form-control input-small"); // modify table per page dropdown
    jQuery('#tabla_docs_para_comprobante_wrapper .dataTables_length select').select2(); // initialize select2 dropdown

    $('#tabla_docs_para_comprobante_column_toggler input[type="checkbox"]').change(function() {
        /* Get the DataTables object again - this is not a recreation, just a get of the object */
        var iCol = parseInt($(this).attr("data-column"));
        var bVis = oTable.fnSettings().aoColumns[iCol].bVisible;
        oTable.fnSetColumnVis(iCol, (bVis ? false : true));
    });

    $('#buscarDocumentos').click(function(){
        $("#modalCarga").modal({backdrop: 'static', keyboard: false});
        $('#modalCargaImg').addClass('fa-pulso');
            
        $('#tabla_docs_para_comprobante').DataTable().ajax.reload( function ( json ) {
            $("#modalCarga").modal('toggle');
        });
    });

    $(document).ready(function() {

        $(".select2_filtro_seleccion_documentos").select2({
            allowClear: true,
        });

        $('#todos').on('change', function() {
             if ($(this).is(':checked') ) {
                $( ".select_item" ).prop( "checked", true );
             } else {
                $( ".select_item" ).prop( "checked", false );
            }
        });

        $("#filtro_proveedor_seleccion_documentos").select2({
            ajax: {
                cache: true,
                allowClear: true,
                //hidden : true,
                url : '{{ url('buscador_proveedor/') }}',
                dataType: 'json',
                delay: 250,
                data: function (params,page) {
                    //console.log("soy params : "+params);
                    //console.log("soy page : "+page);
                    var query = {
                        term: params,
                        page: params.page || 1
                    }
                    return query;
                },
                results: function (data) {
                    return {
                    results: data
                    };
                },
            },
            minimumInputLength: 2,
        
        });

        $(".solo_numeros").keypress(function (key) {
            // window.console.log(key.charCode); //Descomentar para ver codigo
            if (
                (key.charCode < 48 || key.charCode > 57)//números
                && (key.charCode != 0) //borrar y enter
                && (key.charCode != 44) //coma
                && (key.charCode != 46) //punto
                && (key.charCode != 45) //guion
                && (key.charCode != 43) //suma
                )
                //console.log(key.charCode);
                return false;
        });

        $('#fecha_proceso').datepicker({
            format: 'dd/mm/yyyy',
            endDate: new Date(),
            autoclose: true,
            language: 'es'
        });

        $(".select2").select2();

        $("#form-seleccionar-documentos").validate({
            highlight: function(element) {
                $(element).closest('.form-validate').removeClass('has-success');
                $(element).closest('.form-validate').addClass('has-error');
            },
            unhighlight: function(element) {
                $(element).closest('.form-validate').removeClass('has-error');
                $(element).closest('.form-validate').addClass('has-success');
            },
            errorElement: 'span',
            errorClass: 'help-block',
            errorPlacement: function (error, element) {
                console.log(element);
                if (element.parent('.input-group').length) {
                    error.insertAfter(element.parent());
                } else {
                    error.insertAfter(element);
                }
            },
            rules: {
            },
            messages: {
            },

            //para enviar el formulario por ajax
            submitHandler: function(form) {
                esperandoCrear();

                let formData = new FormData(form);                
                
                $.ajax({
                    url: form.action,
                    type: form.method,
                    //data: $(form).serialize(),
                    data: formData,
                    processData: false,
                    contentType: false,
                    success: function(respuesta) {
                        console.log(respuesta);
                        // return false;

                        if ( respuesta.estado == 'error' ) {
                            toastr.error('No es posible realizar la acción'+'<br><strong>Error: '+ respuesta.mensaje +'</strong>', 'Atención', optionsToastr);
                        } else if ( respuesta.estado == 'success') {
                            
                            toastr.success(respuesta.mensaje, 'Atención', optionsToastr);
                            $('#monto_total_comprobante').val(respuesta.sumaMontos);
                            $('#docs_nominas').val(respuesta.docsNomina);
                            $('#monto_nominas').val(respuesta.montoNomina);

                            $("#modalCarga").modal({backdrop: 'static', keyboard: false});
                            $('#modalCargaImg').addClass('fa-pulso');
                            $('#tabla_docs_para_comprobante').DataTable().ajax.reload( function ( json ) {
                                $("#modalCarga").modal('toggle');
                            });

                            $('#tabla_documentos_comprobante').DataTable().ajax.reload();
                            // $("#modalSeleccionDocumentos").modal("hide");

                        }
                        
                    }            
                }).fail( function(respuesta) {//fail ajax
                    if ( respuesta.status == 400 ) {
                        mostrarErroresValidator(respuesta);
                    } else if ( respuesta.status == 500 ) {
                        toastr.error('No es posible realizar la acción, error en el servidor', 'Atención', optionsToastr);
                    } else {
                        toastr.error('No es posible realizar la acción', 'Atención', optionsToastr);
                    }
                    
                })
                .always(function() {
                    listoCrear();
                });//ajax
                
            }

        });

    });

    function esperandoCrear()
    {   
        $('.page-header-fixed *').css('cursor', 'wait');
        $("#botonGuardar").attr("disabled",true);
    }

    function listoCrear()
    {
        $('.page-header-fixed *').css('cursor', '');
        $("#botonGuardar").attr("disabled",false);
    }

    function mostrarErroresValidator(respuesta)
    {
        if ( respuesta.responseJSON ) {
            //console.log(respuesta.responseJSON);
            let htmlErrores = '';
            for (let k in respuesta.responseJSON) { 
                //console.log(k, respuesta.responseJSON[k]);
                htmlErrores += '<li>' + respuesta.responseJSON[k] + '</li>';
            }

            $('#form-seleccionar-documentos #ulErrores').html(htmlErrores);
            $('#form-seleccionar-documentos #divErrores').css('display','');
            toastr.error('No es posible realizar la acción'+'<br>'+'Errores:<br> <ul>'+htmlErrores+'</ul>', 'Atención', optionsToastr);
        }
    }

</script>