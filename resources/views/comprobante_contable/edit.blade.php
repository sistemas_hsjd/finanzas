@extends('master')

@section('title', 'Edición comprobante contable')

@push('css-head')
<!-- BEGIN PAGE LEVEL STYLES -->
<link rel="stylesheet" type="text/css" href="{{asset('assets/plugins/select2/select2_conquer.css')}}"/>
<link rel="stylesheet" href="{{asset('assets/plugins/data-tables/DT_bootstrap.css')}}"/>
<!-- END PAGE LEVEL STYLES -->
<style type="text/css">
    .label-form {
        font-weight: bold;
        color: black;
        text-align: right;
    }

    .span-label {
        color: red;
    }

    .fechas {
        cursor: pointer;
    }

    .td_items {
        text-align: right;
        font-weight: bold;
        padding-top: 15px !important;
    }

    .td_items_dos {
        padding-top: 15px !important;
    }

    #tabla_item_presupuestario {
        border-color: white;
    }

    .noresize {
        resize: none;
        height: 100px!important;
    }

    .form-section {
        font-weight: bold !important;
        color: #69aa46;
        border-bottom: 1px solid #cac1c1 !important;
    }

    .custom-checkbox {}

    /* oculto el input */
    .custom-checkbox input[type=checkbox] {
    display: none;
    }

    /* oculto el texto */
    .custom-checkbox span {
    display: none;
    }

    /* si está activo el input */
    .custom-checkbox input[type=checkbox]:checked + span {
    display: inline-block;
    }

    /* si está inactivo el input */
    .custom-checkbox input[type=checkbox]:not(:checked) + span + span {
    display: inline-block;
    }
</style>
@endpush

@section('content')
<!-- BEGIN CONTENT -->
<div class="page-content-wrapper">
	<div class="page-content-wrapper">
		<div class="page-content">
			<!-- BEGIN STYLE CUSTOMIZER -->
			
			<!-- END BEGIN STYLE CUSTOMIZER -->
			<!-- BEGIN PAGE HEADER-->
			<!--<div class="row">
				<div class="col-md-12">
					<h3 class="page-title">
					Usuarios <small>Agregar usuarios</small>
					</h3>
				</div>
			</div>-->
			<!-- END PAGE HEADER-->
			<!-- BEGIN PAGE CONTENT-->
			<div class="row">
				<div class="col-md-12">
					<!-- BEGIN EXAMPLE TABLE PORTLET-->
					<div class="portlet">
						<div class="portlet-title">
							<div class="caption">
								<i class="fas fa-stamp fa-lg"></i> Edición comprobante contable
							</div>
						</div>
						<div class="portlet-body form">

							<!-- BEGIN FORM-->
							<form action="{{ asset('documentos') }}" method="PUT" class="horizontal-form" id="form" autocomplete="off">
								<div class="form-body">
                                    <div class="note note-danger" id="divErrores" style="display:none;">
                                        <h4 class="block" style="margin-bottom:5 px;">Debe Completar los siguientes campos: </h4>
                                        <ul id="ulErrores"></ul>
                                    </div>                                    

                                    <div class="form-group row">
                                        <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12" >
                                            <div class="form-group row" style="margin-bottom: 0px;">
                                                <div class="col-xs-8 col-sm-8 col-md-8 col-lg-8">
                                                    <h4 class="form-section" style="color: #69aa46;">
                                                        @if ( $nuevo != false )
                                                            Nuevo
                                                        @endif
                                                        Comprobante N° {{ $comprobante->getNumeroComprobante(6) }} {{ $comprobante->getTipoComprobante->nombre }}{{ $comprobante->folio ? ', Folio '.$comprobante->folio : '' }}
                                                        <span class="pull-right">Fecha Proceso: {{ fecha_dmY($comprobante->fecha_proceso) }}</span>
                                                    </h4>

                                                    <div class="form-group row">
                            
                                                        <label for="titulo_comprobante" class="col-xs-1 col-sm-1 col-md-1 col-lg-1 control-label label-form">Título</label>
                                                        <div class="col-xs-7 col-sm-7 col-md-7 col-lg-7 form-validate">
                                                            <input type="text" class="form-control" id="titulo_comprobante"
                                                                    name="titulo_comprobante" autocomplete="off" value="{{ $comprobante->titulo }}"  readonly>
                                                        </div>

                                                        <label for="monto_total_comprobante" class="col-xs-1 col-sm-1 col-md-1 col-lg-1 control-label label-form">Monto</label>
                                                        <div class="col-xs-3 col-sm-3 col-md-3 col-lg-3 form-validate">
                                                            <div class="input-group">
                                                                <span class="input-group-addon">$</span>
                                                            <input type="text" class="form-control text-right" id="monto_total_comprobante" name="monto_total_comprobante" required readonly value="{{ formatoMiles($comprobante->monto_total) }}" >
                                                            </div>
                                                        </div>

                                                    </div>

                                                    <div class="form-group row">
                                                        <label for="descripcion_comprobante" class="col-xs-1 col-sm-1 col-md-1 col-lg-1 control-label label-form">Descripción</label>
                                                        <div class="col-sm-11">
                                                            <textarea class="form-control noresize" id="descripcion_comprobante" name="descripcion_comprobante" readonly>{{ $comprobante->descripcion }}</textarea>
                                                        </div>
                                                    </div>

                                                    <div class="form-group row">
                                                        <div class="table-toolbar col-xs-9 col-sm-9 col-md-9 col-lg-9">
                                                            <div class="btn-group">
                                                                <button type="button" onclick="seleccionarDocumentos();" title="Seleccionar documentos" class="btn btn-primary">Seleccionar documentos</button>
                                                                <a href="{{ url('comprobantes') }}" title="Volver" class="btn btn-warning">Volver</a>
                                                                <button type="submit" title="Guardar" class="btn btn-success" name="boton" value="guardar">Guardar</button>
                                                                <button type="button" onclick="generarPdf();" title="Generar PDF" class="btn btn-danger" >
                                                                        <i class="fas fa-file-pdf fa-lg"></i>
                                                                </button>                                                        
                                                            </div>
                                                        </div>
                                                    </div>

                                                    

                                                </div> <!-- / col-xs-8-->

                                                <div class="col-xs-3 col-sm-3 col-md-3 col-lg-3 col-xs-offset-1 col-sm-offset-1 col-md-offset-1 col-lg-offset-1">
                                                    <h4 class="form-section" style="color: #69aa46;">
                                                        Información Nóminas de Pago
                                                        <span class="pull-right"></span>
                                                    </h4>

                                                    <div class="form-group row">
                                                        <label for="docs_nominas" class="col-xs-3 col-sm-3 col-md-3 col-lg-3 control-label label-form">Documentos</label>
                                                        <div class="col-xs-3 col-sm-3 col-md-3 col-lg-3 form-validate">
                                                            <input type="text" class="form-control" id="docs_nominas" name="docs_nominas" readonly value="{{ formatoMiles($nominas->count()) }}" >
                                                        </div>
                                                    </div>

                                                    <div class="form-group row">
                                                        <label for="monto_nominas" class="col-xs-3 col-sm-3 col-md-3 col-lg-3 control-label label-form">Monto</label>
                                                        <div class="col-xs-7 col-sm-7 col-md-7 col-lg-7 form-validate">
                                                            <div class="input-group">
                                                                <span class="input-group-addon">$</span>
                                                            <input type="text" class="form-control text-right" id="monto_nominas" name="monto_nominas" required readonly value="{{ formatoMiles($nominas->sum('total_documento_actualizado')) }}" >
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div> <!-- / col-xs-12-->
                                    </div>

                                    <div class="row">
                                        <div class="col-md-12">
                                            <table class="table table-striped table-bordered table-hover table-full-width" id="tabla_documentos_comprobante">                                                
                                                <thead>
                                                    <tr>
                                                        <th>Proveedor</th>
                                                        <th>Factoring</th>
                                                        <th >Tipo Doc.</th>
                                                        <th >N° Documento</th>
                                                        <th >Informe</th>
                                                        <th >Fecha Doc.</th>
                                                        <th >C.Contable</th>
                                                        <th >C.Bancaria</th>
                                                        <th >Monto del Comprobante</th>
                                                        <th >Tot. Orig.</th>
                                                        <th >Tot. Act.</th>
                                                        <th class="text-center" >
                                                            <i class="fa fa-cog"></i>
                                                        </th>
                                                    </tr>
                                                </thead>
                                                <tbody>

                                                </tbody>
                                            </table>
                                        </div>
                                    </div>

									<input type="hidden" name="_token" value="{{ csrf_token() }}">
								</div>
								{{-- <div class="form-actions right">
									<button  type="submit" title="Guardar" class="btn btn-info" id="botonGuardar"><i class="fa fa-check"></i> Guardar</button>
								</div> --}}
							</form>
							<!-- END FORM-->
						</div>
					</div>
					<!-- END EXAMPLE TABLE PORTLET-->
				</div>
			</div>
			<!-- END PAGE CONTENT-->
		</div>
	</div>
</div>
<!-- END CONTENT -->

<div id="modal"></div>
<div id="modal2"></div>
@endsection

@push('script-footer')
	<!-- BEGIN PAGE LEVEL PLUGINS -->
	<script type="text/javascript" src="{{asset('assets/plugins/select2/select2.min.js')}}"></script>
	{{--<script type="text/javascript" src="{{ asset('assets/plugins/data-tables/jquery.dataTables.min.js') }}"></script>--}}
    <script type="text/javascript" src="{{ asset('datatables/jquery.dataTables.min.js') }}"></script>
	<script type="text/javascript" src="{{asset('assets/plugins/data-tables/DT_bootstrap.js')}}"></script>
	<!-- END PAGE LEVEL PLUGINS -->
	<!-- BEGIN PAGE LEVEL SCRIPTS -->
    <link rel="stylesheet" type="text/css" href="{{asset('assets/plugins/bootstrap-datepicker/css/datepicker.css')}}"/>

    <script type="text/javascript" src="{{asset('assets/plugins/bootstrap-datepicker/js/bootstrap-datepicker.js')}}"></script>
    <script type="text/javascript" src="{{asset('assets/plugins/bootstrap-datetimepicker/js/bootstrap-datetimepicker.js')}}"></script>

	<script>

    function generarPdf() {
        $.ajax({
            type: "GET",
            url: '{{ url("comprobantes/modal/generar_pdf") }}/{{ $comprobante->id }}',
            success: function(respuesta) {

                $( "#modal" ).html( respuesta );
                $( "#modalGenerarPdf" ).modal();
            
            }
        });
    }

    function seleccionarDocumentos() {

        $.ajax({
            type: "GET",
            url: '{{ url("comprobantes/modal/seleccionar_documentos") }}/{{ $comprobante->id }}',
            success: function(respuesta) {

                $( "#modal" ).html( respuesta );
                $( "#modalSeleccionDocumentos" ).modal();
            
            }
        });

    }

    function traza(id)
    {
        $.get( '{{ url("documentos/modal/traza") }}/' + id, function( data ) {
            $( "#modal2" ).html( data );
            $( "#modalTraza" ).modal();
        });
    }

    function eliminarDoc(id)
    {
        $.get( '{{ url("comprobantes/modal/quitar_doc") }}/' + id, function( data ) {
            $( "#modal" ).html( data );
            $( "#modalQuitarDoc" ).modal();
        });
    }

       
    var tablaDocsComprobantes = $('#tabla_documentos_comprobante').DataTable({ 

        "ajax": {
            "url" : "{{ url('comprobantes/documentos_comprobante/') }}/{{ $comprobante->id }}",
            // "type" : "GET",
            // "data": function(d){
            //     d.form = JSON.stringify( $("#filtros-docs").serializeArray() );
            //     // console.log('se envia:');
            //     // console.log(d);
            // },
        },
        // Set rows IDs
        rowId: function(data) {
            return 'tr_' + data.DT_RowID;
        },
        "deferRender": true,
        "language": {
            "url": "{{ asset('datatables/language.json') }}"
        },
        "lengthMenu": [
            [10, 15, 20, -1],
            [10, 15, 20, "Todos"] // change per page values here
        ],
        // set the initial value
        "displayLength": 10,
        "paging":   true,
        "ordering": true,
        "info":     true,
        "searching": true,
        "orderClasses": true,
        "cache": true

    });

    jQuery('#tabla_documentos_comprobante_filter input').addClass("form-control input-large"); // modify table search input
    jQuery('#tabla_documentos_comprobante_wrapper .dataTables_length select').addClass("form-control input-small"); // modify table per page dropdown
    jQuery('#tabla_documentos_comprobante_wrapper .dataTables_length select').select2(); // initialize select2 dropdown

    $('#tabla_documentos_comprobante_column_toggler input[type="checkbox"]').change(function() {
        /* Get the DataTables object again - this is not a recreation, just a get of the object */
        var iCol = parseInt($(this).attr("data-column"));
        var bVis = oTable.fnSettings().aoColumns[iCol].bVisible;
        oTable.fnSetColumnVis(iCol, (bVis ? false : true));
    });

    $(document).ready(function() {
        App.init();

        


        //TableAdvanced.init();

        // $('#fecha_recepcion').datepicker({
        //     format: 'dd/mm/yyyy',
        //     //endDate: new Date(),
        //     //"setDate": new Date(),
        //     //startDate: new Date(),
        //     autoclose: true,
        //     language: 'es'
        // });

        // $(".numero_documento").keypress(function (key) {
        //     // window.console.log(key.charCode); //Descomentar para ver codigo
        //     if (
        //         (key.charCode < 48 || key.charCode > 57)//números
        //         && (key.charCode != 0) //borrar y enter
        //         && (key.charCode != 44) //coma
        //         && (key.charCode != 46) //punto
        //         && (key.charCode != 45) //guion
        //         && (key.charCode != 43) //suma
        //         )
        //         //console.log(key.charCode);
        //         return false;
        // });

        $("#nombre_proveedor").select2({
            ajax: {
                cache: true,
                allowClear: true,
                //hidden : true,
                url : '{{ url('buscador_proveedor/') }}',
                dataType: 'json',
                delay: 250,
                data: function (params,page) {
                    //console.log("soy params : "+params);
                    //console.log("soy page : "+page);
                    var query = {
                        term: params,
                        page: params.page || 1
                    }
                    return query;
                },
                results: function (data) {
                    return {
                    results: data
                    };
                },
            },
            minimumInputLength: 2,
        
        });

        $(".select2").select2();

        $("#tesoreria").addClass( "active" );
        $("#tesoreria-a").append( '<span class="selected"></span>' );
        $("#comprobantes-li").addClass( "active" );


        $("#form").validate({
            highlight: function(element) {
                $(element).closest('.form-validate').removeClass('has-success');
                $(element).closest('.form-validate').addClass('has-error');
            },
            unhighlight: function(element) {
                $(element).closest('.form-validate').removeClass('has-error');
                $(element).closest('.form-validate').addClass('has-success');
            },
            errorElement: 'span',
            errorClass: 'help-block',
            errorPlacement: function (error, element) {
                console.log(element);
                if (element.parent('.input-group').length) {
                    error.insertAfter(element.parent());
                } else {
                    error.insertAfter(element);
                }
            },
            rules: {
            },
            messages: {
            },

            //para enviar el formulario por ajax
            submitHandler: function(form) {
                esperandoIngresoDocumento();
                if ( validaTotalDocumentoConItemsPresupuestario() && validaNumDocumentoAndLicitacion() && validaTotalGuiasConTotalDocumento() ) {
                    let formData = new FormData(form);
                    $.ajax({
                        url: form.action,
                        type: form.method,
                        //data: $(form).serialize(),
                        data: formData,
                        processData: false,
                        contentType: false,
                        success: function(respuesta) {
                            console.log(respuesta);
                            
                            return false;

                            if ( respuesta.estado_documento == 'error' ) {
                                toastr.error('No es posible realizar la acción'+'<br><strong>Error: '+ respuesta.mensaje_documento +'</strong>', 'Atención', optionsToastr);
                            } else if ( respuesta.estado_documento == 'success') {
                                
                                toastr.success(respuesta.mensaje_documento, 'Atención', optionsToastr);
                                // Setear los elementos, dejar todo en blanco, para poder ingresar otro documento
                                setDOM();
                                if ( respuesta.estado_archivo == 'error' ) {
                                    toastr.error('No es posible realizar la acción'+'<br><strong>Error: '+ respuesta.mensaje_archivo +'</strong>', 'Atención', optionsToastr);
                                } else if ( respuesta.estado_archivo == 'succes') {
                                    toastr.success(respuesta.mensaje_archivo, 'Atención', optionsToastr);
                                }
                                
                            }
                            
                        }            
                    }).fail( function(respuesta) {//fail ajax
                        if ( respuesta.status == 400 ) {
                            mostrarErroresValidator(respuesta);
                        } else if ( respuesta.status == 500 ) {
                            toastr.error('No es posible realizar la acción, error en el servidor', 'Atención', optionsToastr);
                        } else {
                            toastr.error('No es posible realizar la acción', 'Atención', optionsToastr);
                        }
                        
                    })
                    .always(function() {
                        listoIngresoDocumento();
                    });//ajax
                } else {
                    listoIngresoDocumento();
                }
                
            }

        });

    });
        
</script>
@endpush
