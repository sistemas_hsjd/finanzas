
<link href="{{ asset('sistema/css/modales.css') }} " rel="stylesheet" type="text/css"/>
<div class="modal fade" id="modalVer" role="dialog" aria-labelledby="myModalLabel3" aria-hidden="true">
	<div class="modal-dialog" style="width:1000px;">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" title="Cerrar" class="close" data-dismiss="modal" aria-hidden="true"></button>
				<h4 class="modal-title" ><strong><i class="fa fa-list-ol fa-lg"></i>&nbsp;&nbsp;Ver Ítem Presupuestario</strong></h4>
            </div>
          
            <div class="modal-body">
                <div class="form-group row">
                    <div class="col-xs-12">
                        <div class="form-group">
                            <label class="col-sm-2 control-label font-bold">Título</label>
                            <label class="col-sm-4 control-label">{{ $item->titulo }}</label>
                            <label class="col-sm-2 control-label font-bold">Sub-Título</label>
                            <label class="col-sm-4 control-label">{{ $item->subtitulo }}</label>
                        </div>
                    </div>
                </div>

                <div class="form-group row">
                    <div class="col-xs-12">
                        <div class="form-group">
                            <label class="col-sm-2 control-label font-bold">Ítem</label>
                            <label class="col-sm-4 control-label">{{ $item->item }}</label>
                            <label class="col-sm-2 control-label font-bold">Asignación</label>
                            <label class="col-sm-4 control-label">{{ $item->asignacion }}</label>
                        </div>
                    </div>
                </div>

                <div class="form-group row">
                    <div class="col-xs-12">
                        <div class="form-group">
                            <label class="col-sm-2 control-label font-bold">Sub-Asignación</label>
                            <label class="col-sm-4 control-label">{{ $item->subasignacion }}</label>
                            <label class="col-sm-2 control-label font-bold">Clasificador Presupuestario</label>
                            <label class="col-sm-4 control-label">{{ $item->clasificador_presupuestario }}</label>
                        </div>
                    </div>
                </div>

                <div class="form-group row">
                    <div class="col-xs-12">
                        <div class="form-group">
                            <label class="col-sm-2 control-label font-bold">Clasificación</label>
                            <label class="col-sm-6 control-label">{{ $item->getClasificacion->nombre }}</label>
                        </div>
                    </div>
                </div>
              
            </div>{{-- /modal-body --}}

            <div class="modal-footer form-actions">
                <div class="btn-group">
                <button type="button" class="btn btn-info" title="Cerrar" data-dismiss="modal" aria-hidden="true">Cerrar</button>
                </div>
            </div>
          	
		</div>
	</div>
</div>

<script type="text/javascript">

 	$(document).ready(function(){
         
  
 	});

    
 </script>