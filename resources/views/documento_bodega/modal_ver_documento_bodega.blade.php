<link href="{{ asset('sistema/css/modales.css') }} " rel="stylesheet" type="text/css"/>
<div class="modal fade" id="modalVer" role="dialog" aria-labelledby="myModalLabel3" aria-hidden="true">
	<div class="modal-dialog" style="width:1000px;">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" title="Cerrar" class="close" data-dismiss="modal" aria-hidden="true"></button>
				<h4 class="modal-title" ><strong><i class="fas fa-file-alt"></i>&nbsp;&nbsp;Ver Documento Recepcionado en Bodega</strong></h4>
            </div>
          
            <div class="modal-body">
                @if ($wsDocumento->comentario_error != null)
                <div class="form-group row">
                    <div class="col-xs-12">
                        <h4 class="headers-view" style="color:#d9534f;">
                            Comentario de Error
                        </h4>
                        <hr>
                        <div class="form-group">
                            <label class="col-sm-12 control-label font-bold">{!! str_replace(PHP_EOL,'</br>',$wsDocumento->comentario_error) !!}</label>
                        </div>
                    </div>
                </div>
                @endif
                <div class="form-group row">
                    <div class="col-xs-12">
                        <h4 class="headers-view">
                            Contrato
                        </h4>
                        <hr>
                        <div class="form-group">
                            <label class="col-sm-3 control-label font-bold">Rut Proveedor</label>
                            <label class="col-sm-9 control-label">@if ($wsDocumento->getWsOrdenCompra->getWsContrato){{ \Rut::parse($wsDocumento->getWsOrdenCompra->getWsContrato->rut_proveedor)->format() }}@endif</label>
                        </div>
                    </div>
                </div>

                <div class="form-group row">
                    <div class="col-xs-12">
                        <label class="col-sm-3 control-label font-bold">Nombre Proveedor</label>
                        <label class="col-sm-9 control-label">@if ($wsDocumento->getWsOrdenCompra->getWsContrato){{ $wsDocumento->getWsOrdenCompra->getWsContrato->nombre_proveedor }}@endif</label>
                    </div>
                </div>

                @if ( $wsDocumento->getWsOrdenCompra->getWsContrato )

                    @if ( $wsDocumento->getWsOrdenCompra->getWsContrato->fecha_ingreso_contrato != null )
                        <div class="form-group row">
                            <div class="col-xs-12">
                                <div>
                                    <label class="col-sm-3 control-label font-bold">Fecha de Ingreso Contrato</label>
                                    <label class="col-sm-2 control-label">
                                        {{ fecha_dmY($wsDocumento->getWsOrdenCompra->getWsContrato->fecha_ingreso_contrato) }}
                                    </label>
                                </div>
                            </div>
                        </div>
                    @endif

                    @if ( $wsDocumento->getWsOrdenCompra->getWsContrato->fecha_inicio_contrato != null || $wsDocumento->getWsOrdenCompra->getWsContrato->fecha_termino_contrato != null )
                        <div class="form-group row">
                            <div class="col-xs-12">
                                <div>
                                    <label class="col-sm-3 control-label font-bold">Fecha Inicio Contrato</label>
                                    <label class="col-sm-2 control-label">
                                        @if ( $wsDocumento->getWsOrdenCompra->getWsContrato->fecha_inicio_contrato != null )
                                            {{ fecha_dmY($wsDocumento->getWsOrdenCompra->getWsContrato->fecha_inicio_contrato) }}
                                        @endif
                                    </label>
                                    <label class="col-sm-3 control-label font-bold">Fecha Termino Contrato</label>
                                    <label class="col-sm-2 control-label">
                                        @if ( $wsDocumento->getWsOrdenCompra->getWsContrato->fecha_termino_contrato != null )
                                            {{ fecha_dmY($wsDocumento->getWsOrdenCompra->getWsContrato->fecha_termino_contrato) }}
                                        @endif
                                    </label>
                                </div>
                            </div>
                        </div>
                    @endif

                    @if ( $wsDocumento->getWsOrdenCompra->getWsContrato->resolucion != null || $wsDocumento->getWsOrdenCompra->getWsContrato->fecha_resolucion != null )
                        <div class="form-group row">
                            <div class="col-xs-12">
                                <div>
                                    @if ( $wsDocumento->getWsOrdenCompra->getWsContrato->resolucion != null )
                                        <label class="col-sm-3 control-label font-bold">N° Resolución</label>
                                        <label class="col-sm-2 control-label">{{ $wsDocumento->getWsOrdenCompra->getWsContrato->resolucion }}</label>
                                    @endif

                                    @if ( $wsDocumento->getWsOrdenCompra->getWsContrato->fecha_resolucion != null )
                                        <label class="col-sm-3 control-label font-bold">Fecha Resolución</label>
                                        <label class="col-sm-4 control-label">
                                            {{ fecha_dmY($wsDocumento->getWsOrdenCompra->getWsContrato->fecha_resolucion) }}
                                        </label>
                                    @endif
                                </div>
                            </div>
                        </div>
                    @endif

                    @if ( $wsDocumento->getWsOrdenCompra->getWsContrato->monto_maximo != null )
                        <div class="form-group row">
                            <div class="col-xs-12">
                                <div>
                                    <label class="col-sm-3 control-label font-bold">Monto Máximo Contratado</label>
                                    <label class="col-sm-4 control-label">
                                        $ {{ formatoMiles($wsDocumento->getWsOrdenCompra->getWsContrato->monto_maximo) }}
                                    </label>
                                </div>
                            </div>
                        </div>
                    @endif
                @endif

                <div class="form-group row">
                    <div class="col-xs-12">
                        <h4 class="headers-view">
                            Orden de Compra
                        </h4>
                        <hr>
                        <div class="form-group">
                            <label class="col-sm-3 control-label font-bold">Fecha Recepción de O/C</label>
                            <label class="col-sm-2 control-label">{{ fecha_dmY($wsDocumento->getWsOrdenCompra->fecha_recepcion_orden_compra) }}</label>
                        </div>
                    </div>
                </div>

                <div class="form-group row">
                    <div class="col-xs-12">
                        <div>
                            <label class="col-sm-3 control-label font-bold">N° O/C</label>
                            <label class="col-sm-2 control-label">{{ $wsDocumento->getWsOrdenCompra->numero_orden_compra_mercado_publico }}</label>
                            <label class="col-sm-3 control-label font-bold">Fecha de O/C</label>
                            <label class="col-sm-4 control-label">{{ fecha_dmY($wsDocumento->getWsOrdenCompra->fecha_orden_compra_mercado_publico) }}</label>
                        </div>
                    </div>
                </div>

                <div class="form-group row">
                    <div class="col-xs-12">
                        <div>
                            <label class="col-sm-3 control-label font-bold">Monto Máximo O/C</label>
                            <label class="col-sm-3 control-label">$ {{ formatoMiles($wsDocumento->getWsOrdenCompra->total_orden_compra) }}</label>
                            @if ( $wsDocumento->getWsOrdenCompra->saldo_orden_compra != null )
                                <label class="col-sm-2 control-label font-bold">Saldo O/C</label>
                                <label class="col-sm-4 control-label">$ {{ formatoMiles($wsDocumento->getWsOrdenCompra->saldo_orden_compra) }}</label>
                            @endif
                        </div>
                    </div>
                </div>

                <div class="form-group row">
                    <div class="col-xs-12">
                        <h4 class="headers-view">
                            Documento
                        </h4>
                        <hr>
                        <div class="form-group">
                            <label class="col-sm-3 control-label font-bold">Tipo Documento</label>
                            <label class="col-sm-9 control-label">{{ $wsDocumento->tipo_documento }}</label>
                        </div>
                    </div>
                </div>

                <div class="form-group row">
                    <div class="col-xs-12">
                        <label class="col-sm-2 control-label font-bold">N° Documento</label>
                        <label class="col-sm-4 control-label">{{ $wsDocumento->documento }}</label>
                    </div>
                </div>

                <div class="form-group row">
                    <div class="col-xs-12">
                        <div>
                            <label class="col-sm-3 control-label font-bold">Responsable</label>
                            <label class="col-sm-9 control-label">{{ $wsDocumento->nombre_usuario_responsable }}</label>
                        </div>
                    </div>
                </div>

                <div class="form-group row">
                    <div class="col-xs-12">
                        <div>
                            <label class="col-sm-2 control-label font-bold">Monto Neto</label>
                            <label class="col-sm-4 control-label">$ {{ formatoMiles($wsDocumento->documento_neto) }}</label>
                            <label class="col-sm-2 control-label font-bold">Monto IVA</label>
                            <label class="col-sm-4 control-label">$ {{ formatoMiles($wsDocumento->documento_iva) }}</label>
                        </div>
                    </div>
                </div>

                <div class="form-group row">
                    <div class="col-xs-12">
                        <div>
                            <label class="col-sm-2 control-label font-bold">Monto Descuento</label>
                            <label class="col-sm-4 control-label">$ {{ formatoMiles($wsDocumento->documento_descuento_total) }}</label>
                            <label class="col-sm-2 control-label font-bold">Monto Total</label>
                            <label class="col-sm-4 control-label">$ {{ formatoMiles($wsDocumento->documento_total) }}</label>
                        </div>
                    </div>
                </div>

                <div class="form-group row">
                    <div class="col-xs-12">
                        <h4 class="headers-view">
                            Documentos adjuntos
                        </h4>
                        <hr>
                        <div class="form-group">
                            <div class="col-xs-12 table-responsive">
                                <table class="table table-striped table-bordered table-hover " id="tabla_info_archivo">
                                    <thead>
                                        <tr>
                                            <th width="90%">Nombre del Archivo</th>
                                            <th width="10%" class="text-center"><i class="fa fa-cog" ></i></th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        @forelse($wsDocumento->getWsArchivos as $archivo)
                                            <tr>
                                                <td>{{ $archivo->nombre_archivo }}</td>
                                                <td>
                                                    <a class="btn btn-success btn-xs" @if($archivo->cargado == 0) href="{{ asset( $archivo->ubicacion ) }}" @else href="http://{{ $archivo->ubicacion }}" @endif
                                                    title="Ver Archivo" target="_blank" ><i class="fa fa-eye"></i></a>
                                                </td>
                                            </tr>
                                        @empty

                                        @endforelse
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="form-group row">
                    <div class="col-xs-12">	
                        <h4 class="headers-view">
                            Ítem Presupuestario
                        </h4>
                        <hr>
                        <div class="form-group">
                            <div class="col-xs-12 table-responsive">
                                <table class="table table-striped table-bordered table-hover " id="tabla_wsitem_presupuestario">
                                    <thead>
                                        <tr>
                                            <th width="30%">Código</th>
                                            <th width="40%">Descripción Producto</th>
                                            <th width="30%">Monto</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        @forelse($wsDocumento->getWsItemsPresupuestarios as $item)
                                            <tr>
                                                <td>{{ $item->item_presupuestario }}</td>
                                                <td>{{ $item->descripcion_articulo }}</td>
                                                <td class="text-right">$ {{ formatoMiles( $item->cantidad_recepcionada * $item->valor_item_recepcionado ) }}</td>
                                            </tr>
                                        @empty

                                        @endforelse
                                        <tr>
                                            <td class="text-right" colspan="2"><strong>Descuento</strong></td>
                                            <td class="text-right">@if( $wsDocumento->documento_descuento_total != '' )$ {{ formatoMiles( $wsDocumento->documento_descuento_total ) }}@endif</td>
                                        </tr>
                                        <tr>
                                            <td class="text-right" colspan="2"><strong>Neto</strong></td>
                                            <td class="text-right">$ {{ formatoMiles( $wsDocumento->documento_neto ) }}</td>
                                        </tr>
                                        <tr>
                                            <td class="text-right" colspan="2"><strong>IVA</strong></td>
                                            <td class="text-right">$ {{ formatoMiles( $wsDocumento->documento_iva ) }}</td>
                                        </tr>
                                        <tr>
                                            <td class="text-right" colspan="2"><strong>Valor Total</strong></td>
                                            <td class="text-right">$ {{ formatoMiles( $wsDocumento->documento_total ) }}</td>
                                        </tr>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
                
            </div>{{-- /modal-body --}}

            <div class="modal-footer form-actions">
                <div class="btn-group">
                <button type="button" class="btn btn-info" title="Cerrar" data-dismiss="modal" aria-hidden="true">Cerrar</button>
                </div>
            </div>
          	
		</div>
	</div>
</div>

<script type="text/javascript">

 	$(document).ready(function(){
         
  
 	});

    
 </script>