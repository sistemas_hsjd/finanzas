<link href="{{ asset('sistema/css/modales.css') }} " rel="stylesheet" type="text/css"/>
<div class="modal fade" id="modalError" role="dialog" aria-labelledby="myModalLabel3" aria-hidden="true">
	<div class="modal-dialog" style="width:1000px;">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" title="Cerrar" class="close" data-dismiss="modal" aria-hidden="true"></button>
				<h4 class="modal-title" ><strong><i class="fas fa-file-alt"></i> <i class="fas fa-comment"></i>&nbsp;&nbsp;Añadir Comentario de Error a Documento Recepcionado en Bodega</strong></h4>
            </div>

            <form action="{{ asset('recepcion_bodega/modal/errores') }}" method="post" class="horizontal-form" id="form" autocomplete="off">
                <div class="modal-body">

                    <div class="form-group row">
                        <div class="col-xs-12">
                            <h4 class="headers-view">
                                Contrato
                            </h4>
                            <hr>
                            <div class="form-group">
                                <label class="col-sm-3 control-label font-bold">Rut Proveedor</label>
                                <label class="col-sm-2 control-label">@if ($wsDocumento->getWsOrdenCompra->getWsContrato){{ \Rut::parse($wsDocumento->getWsOrdenCompra->getWsContrato->rut_proveedor)->format() }}@endif</label>
                            </div>
                        </div>
                    </div>

                    <div class="form-group row">
                        <div class="col-xs-12">
                            <label class="col-sm-3 control-label font-bold">Nombre Proveedor</label>
                            <label class="col-sm-9 control-label">@if ($wsDocumento->getWsOrdenCompra->getWsContrato){{ $wsDocumento->getWsOrdenCompra->getWsContrato->nombre_proveedor }}@endif</label>
                        </div>
                    </div>

                    <div class="form-group row">
                        <div class="col-xs-12">
                            <div>
                                <label class="col-sm-3 control-label font-bold">Fecha de Ingreso Contrato</label>
                                <label class="col-sm-2 control-label">
                                    @if ($wsDocumento->getWsOrdenCompra->getWsContrato && $wsDocumento->getWsOrdenCompra->getWsContrato->fecha_ingreso_contrato != null)
                                        {{ fecha_dmY($wsDocumento->getWsOrdenCompra->getWsContrato->fecha_ingreso_contrato) }}
                                    @endif
                                </label>
                            </div>
                        </div>
                    </div>

                    <div class="form-group row">
                        <div class="col-xs-12">
                            <div>
                                <label class="col-sm-3 control-label font-bold">Fecha Inicio Contrato</label>
                                <label class="col-sm-2 control-label">
                                    @if ($wsDocumento->getWsOrdenCompra->getWsContrato && $wsDocumento->getWsOrdenCompra->getWsContrato->fecha_inicio_contrato != null)
                                        {{ fecha_dmY($wsDocumento->getWsOrdenCompra->getWsContrato->fecha_inicio_contrato) }}
                                    @endif
                                </label>
                                <label class="col-sm-3 control-label font-bold">Fecha Termino Contrato</label>
                                <label class="col-sm-2 control-label">
                                    @if ($wsDocumento->getWsOrdenCompra->getWsContrato && $wsDocumento->getWsOrdenCompra->getWsContrato->fecha_termino_contrato != null)
                                        {{ fecha_dmY($wsDocumento->getWsOrdenCompra->getWsContrato->fecha_termino_contrato) }}
                                    @endif
                                </label>
                            </div>
                        </div>
                    </div>

                    <div class="form-group row">
                        <div class="col-xs-12">
                            <div>
                                <label class="col-sm-3 control-label font-bold">N° Resolución</label>
                                <label class="col-sm-2 control-label">@if ($wsDocumento->getWsOrdenCompra->getWsContrato){{ $wsDocumento->getWsOrdenCompra->getWsContrato->resolucion }}@endif</label>
                                <label class="col-sm-3 control-label font-bold">Fecha Resolución</label>
                                <label class="col-sm-4 control-label">
                                    @if ($wsDocumento->getWsOrdenCompra->getWsContrato && $wsDocumento->getWsOrdenCompra->getWsContrato->fecha_resolucion != null)
                                        {{ fecha_dmY($wsDocumento->getWsOrdenCompra->getWsContrato->fecha_resolucion) }}
                                    @endif
                                </label>
                            </div>
                        </div>
                    </div>

                    <div class="form-group row">
                        <div class="col-xs-12">
                            <div>
                                <label class="col-sm-3 control-label font-bold">Monto Máximo Contratado</label>
                                <label class="col-sm-4 control-label">
                                    @if ($wsDocumento->getWsOrdenCompra->getWsContrato && $wsDocumento->getWsOrdenCompra->getWsContrato->monto_maximo != null)
                                        $ {{ formatoMiles($wsDocumento->getWsOrdenCompra->getWsContrato->monto_maximo) }}
                                    @endif
                                </label>
                            </div>
                        </div>
                    </div>

                    <div class="form-group row">
                        <div class="col-xs-12">
                            <h4 class="headers-view">
                                Orden de Compra
                            </h4>
                            <hr>
                            <div class="form-group">
                                <label class="col-sm-3 control-label font-bold">Fecha Recepción de O/C</label>
                                <label class="col-sm-2 control-label">{{ fecha_dmY($wsDocumento->getWsOrdenCompra->fecha_recepcion_orden_compra) }}</label>
                            </div>
                        </div>
                    </div>

                    <div class="form-group row">
                        <div class="col-xs-12">
                            <div>
                                <label class="col-sm-3 control-label font-bold">N° O/C</label>
                                <label class="col-sm-2 control-label">{{ $wsDocumento->getWsOrdenCompra->numero_orden_compra }}</label>
                                <label class="col-sm-3 control-label font-bold">Fecha de O/C</label>
                                <label class="col-sm-4 control-label">{{ fecha_dmY($wsDocumento->getWsOrdenCompra->fecha_orden_compra) }}</label>
                            </div>
                        </div>
                    </div>

                    <div class="form-group row">
                        <div class="col-xs-12">
                            <div>
                                <label class="col-sm-3 control-label font-bold">Monto Máximo O/C</label>
                                <label class="col-sm-3 control-label">$ {{ formatoMiles($wsDocumento->getWsOrdenCompra->total_orden_compra) }}</label>
                                @if ( $wsDocumento->getWsOrdenCompra->saldo_orden_compra != null )
                                    <label class="col-sm-2 control-label font-bold">Saldo O/C</label>
                                    <label class="col-sm-4 control-label">$ {{ formatoMiles($wsDocumento->getWsOrdenCompra->saldo_orden_compra) }}</label>
                                @endif
                            </div>
                        </div>
                    </div>

                    <div class="form-group row">
                        <div class="col-xs-12">
                            <h4 class="headers-view">
                                Documento
                            </h4>
                            <hr>
                            <div class="form-group">
                                <label class="col-sm-3 control-label font-bold">N° Documento</label>
                                <label class="col-sm-2 control-label">{{ $wsDocumento->documento }}</label>
                                <label class="col-sm-3 control-label font-bold">Tipo Documento</label>
                                <label class="col-sm-2 control-label">{{ $wsDocumento->tipo_documento }}</label>
                            </div>
                        </div>
                    </div>

                    <div class="form-group row">
                        <div class="col-xs-12">
                            <div>
                                <label class="col-sm-3 control-label font-bold">Responsable</label>
                                <label class="col-sm-9 control-label">{{ $wsDocumento->nombre_usuario_responsable }}</label>
                            </div>
                        </div>
                    </div>

                    <div class="form-group row">
                        <div class="col-xs-12">
                            <div>
                                <label class="col-sm-2 control-label font-bold">Monto Neto</label>
                                <label class="col-sm-4 control-label">$ {{ formatoMiles($wsDocumento->documento_neto) }}</label>
                                <label class="col-sm-2 control-label font-bold">Monto IVA</label>
                                <label class="col-sm-4 control-label">$ {{ formatoMiles($wsDocumento->documento_iva) }}</label>
                            </div>
                        </div>
                    </div>

                    <div class="form-group row">
                        <div class="col-xs-12">
                            <div>
                                <label class="col-sm-2 control-label font-bold">Monto Descuento</label>
                                <label class="col-sm-4 control-label">$ {{ formatoMiles($wsDocumento->documento_descuento_total) }}</label>
                                <label class="col-sm-2 control-label font-bold">Monto Total</label>
                                <label class="col-sm-4 control-label">$ {{ formatoMiles($wsDocumento->documento_total) }}</label>
                            </div>
                        </div>
                    </div>

                    <div class="form-group row">
                        <div class="col-xs-12">
                            <h4 class="headers-view">
                                Documentos adjuntos
                            </h4>
                            <hr>
                            <div class="form-group">
                                <div class="col-xs-12 table-responsive">
                                    <table class="table table-striped table-bordered table-hover " id="tabla_info_archivo">
                                        <thead>
                                            <tr>
                                                <th width="90%">Nombre del Archivo</th>
                                                <th width="10%" class="text-center"><i class="fa fa-cog" ></i></th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            @forelse($wsDocumento->getWsArchivos as $archivo)
                                                <tr>
                                                    <td>{{ $archivo->nombre_archivo }}</td>
                                                    <td>
                                                        <a class="btn btn-success btn-xs" @if($archivo->cargado == 0) href="{{ asset( $archivo->ubicacion ) }}" @else href="http://{{ $archivo->ubicacion }}" @endif
                                                        title="Ver Archivo" target="_blank" ><i class="fa fa-eye"></i></a>
                                                    </td>
                                                </tr>
                                            @empty

                                            @endforelse
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="form-group row">
                        <div class="col-xs-12">	
                            <h4 class="headers-view">
                                Ítem Presupuestario
                            </h4>
                            <hr>
                            <div class="form-group">
                                <div class="col-xs-12 table-responsive">
                                    <table class="table table-striped table-bordered table-hover " id="tabla_wsitem_presupuestario">
                                        <thead>
                                            <tr>
                                                <th width="50%">Código</th>
                                                <th width="20%">Cantidad</th>
                                                <th width="30%">Monto</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            @forelse($wsDocumento->getWsItemsPresupuestarios as $item)
                                                <tr>
                                                    <td>{{ $item->item_presupuestario }}</td>
                                                    <td>{{ $item->cantidad_recepcionada }}</td>
                                                    <td class="text-right">$ {{ formatoMiles($item->valor_item_recepcionado) }}</td>
                                                </tr>
                                            @empty

                                            @endforelse
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="note note-danger" id="divErrores" style="display:none;">
                        <h4 class="block" style="margin-bottom:5 px;">Debe Completar los siguientes campos: </h4>
                        <ul id="ulErrores"></ul>
                    </div>
                
                    <h4 class="form-section headers-view">Comentario de Error</h4>
                    <div class="form-group row">
                        <div class="col-sm-12 form-validate">
                            <textarea class="form-control noresize" id="comentario_error" name="comentario_error" required>{{ $wsDocumento->comentario_error }}</textarea>
                        </div>
                    </div>

                    <input type="hidden" name="_token" value="{{ csrf_token() }}">
                    <input type="hidden" name="_id" value="{{ $wsDocumento->id }}">
                    </div>{{-- /modal-body --}}

                    <div class="modal-footer form-actions right">
                        <button type="button" class="btn btn-default" title="Cancelar" data-dismiss="modal" aria-hidden="true">Cancelar</button>
                        <button type="submit" title="Añadir Comentario" class="btn btn-success" id="botonGuardar"><i class="far fa-save" style="color: black;"></i> <i class="fas fa-comment" style="color: black;"></i> Añadir Comentario</button>
                    </div>
                </form>
          	
		</div>
	</div>
</div>

<script type="text/javascript">

 	$(document).ready(function(){
         
         $("#form").validate({
            highlight: function(element) {
                $(element).closest('.form-validate').removeClass('has-success');
                $(element).closest('.form-validate').addClass('has-error');
            },
            unhighlight: function(element) {
                $(element).closest('.form-validate').removeClass('has-error');
                $(element).closest('.form-validate').addClass('has-success');
            },
            errorElement: 'span',
            errorClass: 'help-block',
            errorPlacement: function (error, element) {
                console.log(element);
                if (element.parent('.input-group').length) {
                    error.insertAfter(element.parent());
                } else {
                    error.insertAfter(element);
                }
            },
            rules: {
            },
            messages: {
            },

            //para enviar el formulario por ajax
            submitHandler: function(form) {
                esperandoIngresoDocumento();

                let formData = new FormData(form);
                $.ajax({
                    url: form.action,
                    type: form.method,
                    //data: $(form).serialize(),
                    data: formData,
                    processData: false,
                    contentType: false,
                    success: function(respuesta) {
                        console.log(respuesta);
                        
                        if ( respuesta.estado == 'success') {
                            
                            toastr.success(respuesta.mensaje, 'Atención', optionsToastr);
                            // Funcion que utiliza la respuesta para actualizar la tabla principal
                            actualizaTabla(respuesta);
                            $("#modalError").modal("hide");
                            
                        }
                        
                    }            
                }).fail( function(respuesta) {//fail ajax
                    if ( respuesta.status == 400 ) {
                        mostrarErroresValidator(respuesta);
                    } else if ( respuesta.status == 500 ) {
                        toastr.error('No es posible realizar la acción, error en el servidor', 'Atención', optionsToastr);
                    } else {
                        toastr.error('No es posible realizar la acción', 'Atención', optionsToastr);
                    }
                    
                })
                .always(function() {
                    listoIngresoDocumento();
                });//ajax
                
            }

        });
 	});

    /**
    * Desactiva boton para guardar los cambios
    */
    function esperandoIngresoDocumento() {
        $('.page-header-fixed *').css('cursor', 'wait');
        $("#botonGuardar").attr("disabled", true);
    }

    /**
    * Habilita boton para guardar los cambios
    */
    function listoIngresoDocumento() {
        $('.page-header-fixed *').css('cursor', '');
        $("#botonGuardar").attr("disabled", false);
    }

    /**
    * Muestra los errores del validator de laravel
    */
    function mostrarErroresValidator(respuesta) {
        if (respuesta.responseJSON) {
            //console.log(respuesta.responseJSON);
            let htmlErrores = '';
            for (let k in respuesta.responseJSON) {
                //console.log(k, respuesta.responseJSON[k]);
                htmlErrores += '<li>' + respuesta.responseJSON[k] + '</li>';
            }

            $('#ulErrores').html(htmlErrores);
            $('#divErrores').css('display', '');
            toastr.error('No es posible realizar la acción' + '<br>' + 'Errores:<br> <ul>' + htmlErrores + '</ul>', 'Atención', optionsToastr);
        }
    }

    /**
    * Actualiza la tabla según la accion realizada
    */
    function actualizaTabla(respuesta) {

        let botones = '<div class="btn-group">';
        botones += '<button class="btn btn-danger btn-xs" title="Ver Documento" onclick="ver(' + respuesta.id + ');">';
        botones += '<i class="fas fa-eye fa-pulso" ></i></button>';

        botones += '<button class="btn btn-warning btn-xs" title="Añadir Comentario de Error a la Recepción" onclick="error(' + respuesta.id + ');">';
        botones += '<i class="fas fa-comment"></i></button>';
        botones += '</div>';
        botones += ' ';

        let archivosHtml = '<div class="btn-group">';
        for (i=0; i<respuesta.archivos.length; i++) {
            console.log('recorro archivo' +respuesta.archivos[i].ubicacion );
            archivosHtml += ' <a class="btn btn-info btn-xs" href="'+ respuesta.archivos[i].ubicacion +'" title="Ver Archivo '+ respuesta.archivos[i].nombre +'" target="_blank" ><i class="fas fa-clipboard-list"></i></a>';
        }
        archivosHtml += '</div>';
        botones += archivosHtml;
        

        tablaPrincipal.row('#tr_' + respuesta.id).data([
            respuesta.tipoDocumento,
            respuesta.rutProveedor,
            respuesta.nombreProveedor,
            respuesta.documento,
            respuesta.documentoTotal,
            respuesta.numeroOrdenCompra,
            respuesta.fechaRecepcionOrdenCompra,
            respuesta.resolucion,
            respuesta.fechaResolucion,
            respuesta.errores,
            botones
        ]).draw();

    }

    
 </script>