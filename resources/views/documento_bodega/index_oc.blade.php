@extends('master')

@section('title', 'Ordenes de Compra sin Recepción')

@push('css-head')
<!-- BEGIN PAGE LEVEL STYLES -->
<link rel="stylesheet" type="text/css" href="{{asset('assets/plugins/select2/select2_conquer.css')}}"/>
<link rel="stylesheet" href="{{asset('assets/plugins/data-tables/DT_bootstrap.css')}}"/>
<!-- END PAGE LEVEL STYLES -->
<style>
    .label-form {
        font-weight: bold;
        color: black;
        text-align: right;
    }
</style>
@endpush

@section('content')

<!-- BEGIN CONTENT -->

<div class="page-content-wrapper">
	<div class="page-content-wrapper">
		<div class="page-content">

			<!-- BEGIN PAGE CONTENT-->
			<div class="row">
				<div class="col-md-12">
					<!-- BEGIN EXAMPLE TABLE PORTLET-->
					<div class="portlet">
						<div class="portlet-title">
							<div class="caption">
								<i class="fas fa-th-list"></i> Ordenes de Compra sin Recepción
							</div>
						</div>
						<div class="portlet-body">
                            {{--
                            <form action="{{url('recepcion_bodega/filtrar')}}" method="post" enctype="multipart/form-data" id="filtros-docs" class="form-horizontal" autocomplete="off">
                                <h4 class="form-section" style="color: #69aa46;"><strong>Filtros</strong>
                                <div class="btn-group pull-right">
                                    <button class="btn btn-info btn-xs" title="Filtrar Listado"  type="sumbit">
                                        <i class="fas fa-list-alt"></i> Filtrar
                                    </button>
                                </div>
                                </h4>
                                <div class="form-group row">
                                
                                    <label for="filtro_fecha_inicio" class="col-sm-1 control-label label-form">Fecha Inicio</label>
                                    <div class="col-sm-3">
                                        <div class="input-group">
                                            <input type="text" class="form-control fechas" id="filtro_fecha_inicio" name="filtro_fecha_inicio" @if(isset($filtroFechaInicio)) value="{{ $filtroFechaInicio }}" @else value="{{ date("d/m/Y") }}" @endif >
                                            <span class="input-group-addon" onclick="fechaDocumento('filtro_fecha_inicio');" style="cursor:pointer;"><i class="fa fa-calendar" aria-hidden="true" style="color: black;"></i></span>
                                        </div>
                                    </div>

                                    <label for="filtro_fecha_termino" class="col-sm-1 control-label label-form">Fecha Termino</label>
                                    <div class="col-sm-3">
                                        <div class="input-group">
                                            <input type="text" class="form-control fechas" id="filtro_fecha_termino" name="filtro_fecha_termino" @if(isset($filtroFechaTermino)) value="{{ $filtroFechaTermino }}"  @else value="{{ date("d/m/Y") }}" @endif>
                                            <span class="input-group-addon" onclick="fechaDocumento('filtro_fecha_termino');" style="cursor:pointer;"><i class="fa fa-calendar" aria-hidden="true" style="color: black;"></i></span>
                                        </div>
                                    </div>
                                    

                                    <label for="filtro_proveedor" class="col-sm-1 control-label label-form">Proveedor</label>
                                    <div class="col-sm-3">
                                        <select name="filtro_proveedor[]" id="filtro_proveedor" class="form-control select2_filtro" multiple style="width: 100%">
                                            @foreach ( $proveedores as $proveedor )
                                                <option value="{{ $proveedor->id }}" >{{ $proveedor->rut }} {{ $proveedor->nombre }}</option>
                                            @endforeach
                                        </select>
                                    </div>

                                    <label for="filtro_numero_documento" class="col-sm-1 control-label label-form">N° Documento</label>
                                    <div class="col-sm-3">
                                        <input type="text" class="form-control " id="filtro_numero_documento" name="filtro_numero_documento"
                                        @if(isset($filtroNumeroDocumento)) value="{{ $filtroNumeroDocumento }}"  @endif >
                                    </div>
                                </div>

                                <div class="form-group row">

                                    <label for="filtro_referente_tecnico" class="col-sm-1 control-label label-form">Referente Técnico</label>
                                    <div class="col-sm-3">
                                        <select name="filtro_referente_tecnico[]" id="filtro_referente_tecnico" class="form-control select2_filtro" multiple="multiple">
                                            @foreach ( $referentesTecnicos as $referenteTecnico )
                                                <option value="{{ $referenteTecnico->id }}" >{{ $referenteTecnico->nombre }} - {{ $referenteTecnico->responsable }}</option>
                                            @endforeach
                                        </select>
                                    </div>

                                    <label for="filtro_item_presupuestario" class="col-sm-1 control-label label-form">Ítem</label>
                                    <div class="col-sm-3">
                                        <select class="form-control select2_filtro " id="filtro_item_presupuestario" name="filtro_item_presupuestario[]" multiple="multiple" >
                                            @foreach ( $itemsPresupuestarios as $item )
                                                <option value="{{ $item->id }}" >{{ $item->codigo() }} {{ $item->clasificador_presupuestario }}</option>
                                            @endforeach
                                        </select>
                                    </div>

                                    <label for="filtro_numero_documento_compra" class="col-sm-1 control-label label-form">N° Documento Compra</label>
                                    <div class="col-sm-3">
                                        <input type="text" class="form-control " id="filtro_numero_documento_compra" name="filtro_numero_documento_compra"
                                        @if(isset($filtroNumeroDocumentoCompra)) value="{{ $filtroNumeroDocumentoCompra }}"  @endif>
                                    </div>
                                </div>

                            </form>
                            --}}
                            {{-- 
                            <div class="table-toolbar">
                                <div class="btn-group">
                                    <button title="Agregar Tipo de Archivo" class="btn btn-success" onclick="nuevo();">
										<i class="fa fa-plus"></i> Agregar Tipo de Archivo
									</button>
                                </div>
                            </div>
                            --}}
                            <div class="row">
                                <div class="col-md-12">
                                    <div class="panel">
                                    {{-- 
                                        <div class="panel-heading">
                                            <div class="caption">
                                                <strong style="color: #69aa46;">Últimos Documentos Ingresados</strong>
                                            </div>
                                        </div>
                                        --}}
                                        <div class="panel-body">
                                            <div class="table-responsive">
                                                <table class="table table-striped table-bordered table-hover table-full-width" id="tabla_ws_ocs">
                                                    <thead>
                                                    <tr>
                                                        <th >ID Interno</th>
                                                        <th >N° Intero</th>
                                                        <th >N° Mercado Público</th>
                                                        <th >Total</th>
                                                        <th >Saldo</th>
                                                        <th >Fecha Mercado Público</th>
                                                        <th width="10%" class="text-center">
                                                            <i class="fa fa-cog"></i>
                                                        </th>
                                                    </tr>
                                                    </thead>
                                                    <tbody>
                                                    @forelse($ordenesCompra as $oc)
                                                        <tr id="tr_{{ $oc->id }}">
                                                            <td >{{ $oc->id_orden_compra }}</td>
                                                            <td >{{ $oc->numero_orden_compra }}</td>
                                                            <td >@if ($oc->numero_orden_compra_mercado_publico ){{ $oc->numero_orden_compra_mercado_publico }}@endif</td>
                                                            <td >$ {{ formatoMiles($oc->total_orden_compra) }}</td>
                                                            <td >@if ($oc->saldo_orden_compra)$ {{ formatoMiles($oc->saldo_orden_compra) }}@endif</td>
                                                            <td >@if ($oc->fecha_orden_compra_mercado_publico){{ fecha_dmY($oc->fecha_orden_compra_mercado_publico) }}@endif</td>
                                                            <td >
                                                                {{-- <div class="btn-group">
                                                                    <button class="btn btn-success btn-xs" title="Ver Documento" onclick="ver({{ $documento->id }});">
                                                                        <i class="fas fa-eye"></i>
                                                                    </button>
                                                                </div> --}}
                                                                <div class="btn-group">
                                                                    @forelse($oc->getWsArchivos as $archivo)
                                                                        <a class="btn btn-info btn-xs" href="{{ asset($archivo->ubicacion) }}" 
                                                                            title="Ver Archivo {{ explode('_',$archivo->nombre_archivo)[0] }}" target="_blank" ><i class="fas fa-clipboard-list"></i></a>
                                                                    @empty

                                                                    @endforelse
                                                                </div>
                                                            </td>
                                                        </tr>
                                                    @empty

                                                    @endforelse
                                                    </tbody>
                                                </table>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
						</div>
					</div>
					<!-- END EXAMPLE TABLE PORTLET-->
				</div>
			</div>
			<!-- END PAGE CONTENT-->
			
			
			
			
		</div> <!--/div class="page-content" -->
	</div> <!--/div class="page-content-wrapper" -->
</div><!--/div class="page-content-wrapper" -->
<!-- END CONTENT -->
<div id="modal"></div>
<div id="modal2"></div>
@endsection

@push('script-footer')
<!-- BEGIN PAGE LEVEL PLUGINS -->
<script type="text/javascript" src="{{ asset('assets/plugins/select2/select2.min.js') }} "></script>
{{--<script type="text/javascript" src="{{ asset('assets/plugins/data-tables/jquery.dataTables.min.js') }}"></script>--}}
<script type="text/javascript" src="{{ asset('datatables/jquery.dataTables.min.js') }}"></script>
<script type="text/javascript" src="{{ asset('assets/plugins/data-tables/DT_bootstrap.js') }}"></script>

<link rel="stylesheet" type="text/css" href="{{asset('assets/plugins/bootstrap-datepicker/css/datepicker.css')}}"/>

<script type="text/javascript" src="{{asset('assets/plugins/bootstrap-datepicker/js/bootstrap-datepicker.js')}}"></script>
<script type="text/javascript" src="{{asset('assets/plugins/bootstrap-datetimepicker/js/bootstrap-datetimepicker.js')}}"></script>


<!-- END PAGE LEVEL PLUGINS -->
<!-- BEGIN PAGE LEVEL SCRIPTS -->
<script src="{{ asset('assets/scripts/app.js') }} "></script>
<script src="{{ asset('assets/scripts/table-advanced.js') }} "></script>


<script type="text/javascript">

    let tablaPrincipal = $('#tabla_ws_ocs').DataTable({
        "language": {
            "emptyTable":     "No hay datos disponibles en la tabla",
            "info":           "Mostrando registros del _START_ al _END_ de un total de _TOTAL_ registros",
            "infoEmpty":      "Showing 0 to 0 of 0 entries",
            "infoFiltered":   "(filtrado de un total de _MAX_ registros)",
            "infoPostFix":    "",
            "thousands":      ",",
            "lengthMenu":     "Mostrar _MENU_ registros",
            "loadingRecords": "Cargando...",
            "processing":     "Procesando...",
            "search":         "Buscar: ",
            "zeroRecords":    "No matching records found",
            "paginate": {
                "first":      "Primero",
                "last":       "Último",
                "next":       "Siguiente",
                "previous":   "Anterior"
            },
            "aria": {
                "sortAscending":  ": Activar para ordenar la columna de manera ascendente",
                "sortDescending": ": Activar para ordenar la columna de manera descendente"
            }
        },
        "lengthMenu": [
            [10, 15, 20, -1],
            [10, 15, 20, "Todos"] // change per page values here
        ],
        // set the initial value
        "displayLength": 10,
        "paging":   true,
        "ordering": true,
        "info":     true,
        "searching": true
    });

    jQuery('#tabla_ws_ocs_filter input').addClass("form-control input-large"); // modify table search input
    jQuery('#tabla_ws_ocs_wrapper .dataTables_length select').addClass("form-control input-small"); // modify table per page dropdown
    jQuery('#tabla_ws_ocs_wrapper .dataTables_length select').select2(); // initialize select2 dropdown

    $('#tabla_ws_ocs_column_toggler input[type="checkbox"]').change(function() {
        /* Get the DataTables object again - this is not a recreation, just a get of the object */
        var iCol = parseInt($(this).attr("data-column"));
        var bVis = oTable.fnSettings().aoColumns[iCol].bVisible;
        oTable.fnSetColumnVis(iCol, (bVis ? false : true));
    });

	$(document).ready(function() {
		App.init(); // initlayout and core plugins
		//TableAdvanced.init();
        
   		$("#documentos").addClass( "active" );
        $("#documentos-a").append( '<span class="selected"></span>' );
        $("#bodega").addClass( "active" );
        $("#recepcion-bodega-oc-li").addClass( "active" );
        $("#bodega-a").append( '<span class="selected"></span>' );
		
		$(".select2_filtro").select2({
            allowClear: true,
        });

        @isset($filtroProveedor)
            let Aux1 = new Array();
            @foreach ($filtroProveedor as $proveedor)
                Aux1.push('{{ $proveedor }}');
            @endforeach
            $('#filtro_proveedor').select2('val',Aux1);
        @endisset

	});

    function ver(id)
    {
        $.get( '{{ url("recepcion_bodega/modal/ver") }}/' + id, function( data ) {
            $( "#modal" ).html( data );
            $( "#modalVer" ).modal();
        });
    }

</script>

@endpush