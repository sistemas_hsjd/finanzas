@extends('master')

@section('title', 'Buscador Recepción')

@push('css-head')
<!-- BEGIN PAGE LEVEL STYLES -->
<link rel="stylesheet" type="text/css" href="{{asset('assets/plugins/select2/select2_conquer.css')}}"/>
<link rel="stylesheet" href="{{asset('assets/plugins/data-tables/DT_bootstrap.css')}}"/>
<link rel="stylesheet" href="{{asset('datatables/fixedColumns.dataTables.min.css')}}"/>

<link rel="stylesheet" type="text/css" href="{{asset('assets/plugins/bootstrap-datepicker/css/datepicker.css')}}"/>
<!-- END PAGE LEVEL STYLES -->

<style>
    .label-form {
        font-weight: bold;
        color: black;
        text-align: right;
    }

    .td-bold {
        font-weight: bold;
    }

</style>

@endpush

@section('content')
<!-- BEGIN CONTENT -->

<div class="page-content-wrapper">
	<div class="page-content-wrapper">
		<div class="page-content">

			<!-- BEGIN PAGE CONTENT-->
			<div class="row">
				<div class="col-md-12">
					<!-- BEGIN EXAMPLE TABLE PORTLET-->
					<div class="portlet">
						<div class="portlet-title">
							<div class="caption">
                                <i class="fas fa-search"></i> <i class="fa fa-files-o" style="color: black;"></i> Buscador Recepción
							</div>
						</div>
						<div class="portlet-body">
                            
                            {{-- <div class="table-toolbar">
                                    
                            </div> --}}
                            <div class="row">
                                <div class="col-md-12">
                                    <div class="panel">
                                        <div class="panel-body">
                                            <form action="{{ url('buscador_recepcion') }}" method="post" enctype="multipart/form-data" id="form" class="form-horizontal" autocomplete="off">

                                                <div class="form-group row">
                                                    <div class="note note-danger" id="divErrores" style="display:none;">
                                                        <h4 class="block" style="margin-bottom:5 px;">Debe Completar los siguientes campos: </h4>
                                                        <ul id="ulErrores"></ul>
                                                    </div>
                                                </div>
                                                
                                                <div class="form-group row">
                
                                                    <label for="numero_recepcion" class="col-sm-2 label-form" style="text-align: center;">Número Recepción</label>
                                                    <div class="col-sm-2 form-validate">
                                                        <input type="text" class="form-control " id="numero_recepcion" name="numero_recepcion" required 
                                                                placeholder="Ejemplo: 070896" >
                                                    </div>

                                                    @csrf
                                                    <button class="btn btn-info" title="Buscar" id="botonSubmit" type="submit">
                                                        <i class="fas fa-search" style="color:black;" ></i> Buscar
                                                    </button>
                                                </div>
                                                
                                            </form>

                                            <div id="informacion">
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
						</div>
					</div>
					<!-- END EXAMPLE TABLE PORTLET-->
				</div>
			</div>
			<!-- END PAGE CONTENT-->
			
			
			
			
		</div> <!--/div class="page-content" -->
	</div> <!--/div class="page-content-wrapper" -->
</div><!--/div class="page-content-wrapper" -->
<!-- END CONTENT -->
<div id="modal"></div>
<div id="modal2"></div>
@endsection

@push('script-footer')
<!-- BEGIN PAGE LEVEL PLUGINS -->
<script type="text/javascript" src="{{ asset('assets/plugins/select2/select2.min.js') }} "></script>

<script type="text/javascript" src="{{ asset('datatables/jquery.dataTables.min.js') }}"></script>
<script type="text/javascript" src="{{ asset('datatables/fixedColumns.min.js') }}"></script>
<script type="text/javascript" src="{{ asset('assets/plugins/data-tables/DT_bootstrap.js') }}"></script>

<script type="text/javascript" src="{{ asset('js/moment/moment.js') }}"></script>
<script type="text/javascript" src="{{ asset('js/moment/moment-with-locales.js') }}"></script>

<script type="text/javascript" src="{{asset('assets/plugins/bootstrap-datepicker/js/bootstrap-datepicker.js')}}"></script>
<script type="text/javascript" src="{{asset('assets/plugins/bootstrap-datetimepicker/js/bootstrap-datetimepicker.js')}}"></script>
<script type="text/javascript" src="{{ asset('assets/scripts/app.js') }} "></script>
<!-- END PAGE LEVEL PLUGINS -->

<script type="text/javascript">

	$(document).ready(function() {
        
		App.init(); // initlayout and core plugins

        $("#buscador_recepcion").addClass( "active" );
		$("#buscador-recepcion-a").append( '<span class="selected"></span>' );

        $("#form").validate({
            highlight: function(element) {
                $(element).closest('.form-validate').removeClass('has-success');
                $(element).closest('.form-validate').addClass('has-error');
            },
            unhighlight: function(element) {
                $(element).closest('.form-validate').removeClass('has-error');
                $(element).closest('.form-validate').addClass('has-success');
            },
            errorElement: 'span',
            errorClass: 'help-block',
            errorPlacement: function (error, element) {
                console.log(element);
                if (element.parent('.input-group').length) {
                    error.insertAfter(element.parent());
                } else {
                    error.insertAfter(element);
                }
            },
            rules: {
            },
            messages: {
            },

            //para enviar el formulario por ajax
            submitHandler: function(form) {
                // esperandoSubmit();
                $("#modalCarga").modal({backdrop: 'static', keyboard: false});

                let formData = new FormData(form);

                $.ajax({
                    url: form.action,
                    type: form.method,
                    //data: $(form).serialize(),
                    data: formData,
                    processData: false,
                    contentType: false,
                    success: function(respuesta) {
                        console.log(respuesta);
                        $('#informacion').html(respuesta.mensaje);

                        // if ( respuesta.estado == 'error' ) {
                        //     toastr.error('No es posible realizar la acción'+'<br><strong>Error: '+ respuesta.mensaje +'</strong>', 'Atención', optionsToastr);
                        // } else if ( respuesta.estado == 'success') {
                            
                        //     toastr.success(respuesta.mensaje, 'Atención', optionsToastr);
                        //     // Mostrar resultado de la busqueda en el div informacion
                        //     $('#informacion').html();
                            
                        // }
                        
                    }            
                }).fail( function(respuesta) {//fail ajax
                    if ( respuesta.status == 400 ) {
                        mostrarErroresValidator(respuesta);
                    } else if ( respuesta.status == 500 ) {
                        toastr.error('No es posible realizar la acción, error en el servidor', 'Atención', optionsToastr);
                    } else {
                        toastr.error('No es posible realizar la acción', 'Atención', optionsToastr);
                    }
                    
                })
                .always(function() {
                    // listoSubmit();
                    $("#modalCarga").modal('toggle');
                });//ajax
                
            }

        });
        
	});

function mostrarErroresValidator(respuesta)
{
    if ( respuesta.responseJSON ) {
        //console.log(respuesta.responseJSON);
        let htmlErrores = '';
        for (let k in respuesta.responseJSON) { 
            //console.log(k, respuesta.responseJSON[k]);
            htmlErrores += '<li>' + respuesta.responseJSON[k] + '</li>';
        }

        $('#ulErrores').html(htmlErrores);
        $('#divErrores').css('display','');
        toastr.error('No es posible realizar la acción'+'<br>'+'Errores:<br> <ul>'+htmlErrores+'</ul>', 'Atención', optionsToastr);
    }
}

</script>

@endpush