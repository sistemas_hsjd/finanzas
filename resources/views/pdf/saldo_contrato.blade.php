<!DOCTYPE html>
<html >
	<head>
        <link href="{{asset('sistema/css/base.css')}}" rel="stylesheet" type="text/css"/>
		<link href="{{asset('assets/plugins/bootstrap/css/bootstrap.min.css')}}" rel="stylesheet" type="text/css"/>
		<style type="text/css">
			table.table-bordered{ font-size: 11px; }
			table.table-bordered td{ padding: 3px;border-color: #909090!important;color: #000000!important;}
			table.table-bordered th{ padding: 3px;border-color: #909090!important;}
			.table-bordered tr td{
				font-size: 12px;
			}
			.justify{
				text-align : justify;
			}
			.sin-salto{
				page-break-inside: avoid;
			}

		</style>
		<title>Control Saldo Contrato {{ $contrato->resolucion }} - DEPARTAMENTO FINANZAS - HSJD</title>
	</head>
	<body class="pace-running" >

        <div class="col-xs-12 padding-0" style="margin-bottom:-30px; " >
            <table class="table">
                <tr>
                    <td width="5%">
                        <h4  >
                            <img style="margin-right:0px;" src="logohsjd.png" >
                        </h4>
                    </td>
                    <td style="text-align: left;font-weight: bold;" width="45%">
                        <p>Servicio Salud Metropolitano Occidente</p>
                        <p>Hospital San Juan de Dios - CDT</p>
                        <p>Departamento de Finanzas</p>
                    </td>

                    <td style="text-align: right;" width="45%">
                        <p> </p>
                    </td>
                    <td width="5%">
                        <h4 >
                            <img style="margin-right:0px;" src="logo-finanzas-navA.png" width="25%" >
                        </h4>
                    </td>

                </tr>
            </table>
        </div>

        <div class="col-xs-12 padding-0" style="margin-top: 0px;margin-bottom:-10px;" >
            <div class="table-responsive" >
                <table class="table table-bordered " >
                    <tr>
                        <td align="center" style="background-color: #dddddd; border-color:black; " >
                            <strong>CONTROL SALDO CONTRATO</strong>
                        </td>
                    </tr>
                </table>
            </div>
        </div>

        <div class="col-xs-12 padding-0" >
            <div class="table-responsive" >
                <table class="table table-bordered " >
                    <tr>
                        <td align="center" style="background-color: #dddddd;" >
                            <strong>PROVEEDOR</strong>
                        </td>
                        <td align="center" >
                            <strong>{{ $contrato->getProveedor->nombre }}</strong>
                        </td>
                        <td align="center" style="background-color: #dddddd;" >
                            <strong>RUT</strong>
                        </td>
                        <td align="center" >
                            <strong>{{ $contrato->getProveedor->rut }}</strong>
                        </td>
                    </tr>

                    <tr>
                        <td align="center" style="background-color: #dddddd;" >
                            <strong>NOMBRE CONTRATO</strong>
                        </td>
                        <td align="center" >
                            <strong>{{ $contrato->detalle_contrato }}</strong>
                        </td>
                        <td align="center" style="background-color: #dddddd;" >
                            <strong>TIPO DE CONTRATACIÓN</strong>
                        </td>
                        <td align="center" >
                            <strong>{{ $contrato->getTipoAdjudicacion->nombre }}</strong>
                        </td>
                    </tr>

                    <tr>
                        <td align="center" style="background-color: #dddddd;" >
                            <strong>VIGENCIA CONTRATO</strong>
                        </td>
                        <td align="center" >
                            <strong>{{ cantidadMesesEntreFechas($contrato->inicio_vigencia, $contrato->fin_vigencia) }} MESES | {{ fechaSaldoContrato($contrato->inicio_vigencia) }} - {{ fechaSaldoContrato($contrato->fin_vigencia) }}</strong>
                        </td>
                        <td align="center" style="background-color: #dddddd;" >
                            <strong>N° Y FECHA CONTRATO</strong>
                        </td>
                        <td align="center" >
                            <strong>{{ $contrato->resolucion }} | {{ fecha_dmY($contrato->fecha_resolucion) }}</strong>
                        </td>
                    </tr>

                    <tr>
                        <td align="center" style="background-color: #dddddd;" >
                            <strong>LICITACIÓN</strong>
                        </td>
                        <td align="center" >
                            <strong>{{ $contrato->licitacion }}</strong>
                        </td>
                        <td align="center" style="background-color: #dddddd;" >
                            <strong>REFERENTE TÉCNICO</strong>
                        </td>
                        <td align="center" >
                            <strong>{{ $contrato->getReferenteTecnico ? $contrato->getReferenteTecnico->nombre : '' }}</strong>
                        </td>
                    </tr>
                    @if ( ! $contrato->monto_preventivo )
                        <tr >
                            <td align="center" style="background-color: #dddddd;" >
                                <strong>MONTO TOTAL CONTRATO</strong>
                            </td>
                            <td align="center" >
                                <strong>$ {{ formatoMiles($contrato->monto_contrato) }}</strong>
                            </td>
                            <td align="center" style="background-color: #dddddd;" >
                                <strong>SALDO CONTRATO</strong>
                            </td>
                            <td align="center" >
                                <strong>$ {{ formatoMiles($contrato->saldo_contrato) }}</strong>
                            </td>
                        </tr>
                    @endif

                </table>
            </div>
        </div>

        <div class="col-xs-12 padding-0" style="margin-top:-6.7px; {!! $contrato->monto_preventivo ? '' : 'display:none;'!!}" >
            <div class="table-responsive" style="margin-top: -6.7px">
                <table class="table table-bordered " style="margin-top: -6.7px">
                    <tr>
                        <td align="center" style="background-color: #dddddd;" >
                            <strong>MONTO TOTAL CONTRATO</strong>
                        </td>
                        <td align="center" >
                            <strong>$ {{ formatoMiles($contrato->monto_contrato) }}</strong>
                        </td>

                        <td align="center" style="background-color: #dddddd;" >
                            <strong>MONTO PREVENTIVO</strong>
                        </td>
                        <td align="center" >
                            <strong>{{ $contrato->monto_preventivo ? '$'.formatoMiles($contrato->monto_preventivo) : 'NO APLICA' }}</strong>
                        </td>

                        <td align="center" style="background-color: #dddddd;" >
                            <strong>MONTO CORRECTIVO</strong>
                        </td>
                        <td align="center" >
                            <strong>{{ $contrato->monto_correctivo ?  '$ '.formatoMiles($contrato->monto_correctivo) : 'NO APLICA' }}</strong>
                        </td>
                    </tr>

                    <tr>
                        <td align="center" style="background-color: #dddddd;" >
                            <strong>SALDO CONTRATO</strong>
                        </td>
                        <td align="center" >
                            <strong>$ {{ formatoMiles($contrato->saldo_contrato) }}</strong>
                        </td>

                        <td align="center" style="background-color: #dddddd;" >
                            <strong>SALDO PREVENTIVO</strong>
                        </td>
                        <td align="center" >
                            <strong>{{ $contrato->saldo_preventivo ? '$'.formatoMiles($contrato->saldo_preventivo) : 'NO APLICA' }}</strong>
                        </td>

                        <td align="center" style="background-color: #dddddd;" >
                            <strong>SALDO CORRECTIVO</strong>
                        </td>
                        <td align="center" >
                            <strong>{{ $contrato->saldo_correctivo ?  '$ '.formatoMiles($contrato->saldo_correctivo) : 'NO APLICA' }}</strong>
                        </td>
                    </tr>

                </table>
            </div>
        </div>

        <div class="col-xs-12 padding-0" style="margin-top:-6.7px; {!! $contrato->numero_boleta_garantia ? '' : 'display:none;'!!}">
            <div class="table-responsive" style="margin-top: -6.7px">
                <table class="table table-bordered " style="margin-top: -6.7px">

                    <tr>
                        <td align="center" style="background-color: #dddddd;" >
                            <strong>N° BOLETA GARANTÍA</strong>
                        </td>
                        <td align="center" >
                            <strong>{{ $contrato->numero_boleta_garantia }}</strong>
                        </td>

                        <td align="center" style="background-color: #dddddd;" >
                            <strong>MONTO BOLETA GARANTÍA</strong>
                        </td>
                        <td align="center" >
                            <strong>{{ $contrato->monto_boleta_garantia ? '$'.formatoMiles($contrato->monto_boleta_garantia) : 'NO APLICA' }}</strong>
                        </td>

                        <td align="center" style="background-color: #dddddd;" >
                            <strong>VIGENCIA BOLETA GARANTÍA</strong>
                        </td>
                        <td align="center" >
                            <strong>{{ fecha_dmY($contrato->inicio_vigencia_boleta_garantia) }} HASTA {{ fecha_dmY($contrato->fin_vigencia_boleta_garantia) }}</strong>
                        </td>
                    </tr>

                </table>
            </div>
        </div>

        <div class="col-xs-12 padding-0" >
            <div class="table-responsive" >
                <table class="table table-bordered " >
                    <tr>
                        <td align="center" style="background-color: #dddddd;" colspan="2" >
                            <strong>RECEPCIÓN</strong>
                        </td>

                        <td align="center" style="background-color: #dddddd;" {!! $contrato->cuotas == 1 ? 'colspan="6"' : 'colspan="5"' !!}  >
                            <strong>FACTURACIÓN</strong>
                        </td>
                    </tr>
                    <tr>
                        <td align="center" style="background-color: #dddddd;" >
                            <strong>N°</strong>
                        </td>

                        <td align="center" style="background-color: #dddddd;" >
                            <strong>MONTO</strong>
                        </td>

                        <td align="center" style="background-color: #dddddd;" >
                            <strong>ID ORDEN DE COMPRA</strong>
                        </td>

                        <td align="center" style="background-color: #dddddd;" >
                            <strong>N° DOCUMENTO</strong>
                        </td>

                        <td align="center" style="background-color: #dddddd;" >
                            <strong>FECHA DOCUMENTO</strong>
                        </td>

                        @if ( $contrato->cuotas == 1 )
                            <td align="center" style="background-color: #dddddd;" >
                                <strong>N° CUOTA</strong>
                            </td>
                        @endif

                        <td align="center" style="background-color: #dddddd;" >
                            <strong>PERIODO (MES)</strong>
                        </td>

                        <td align="center" style="background-color: #dddddd;" >
                            <strong>MONTO</strong>
                        </td>

                    </tr>

                    @foreach ($cuotasContrato as $cuota)
                        <tr>
                            <td align="center" {!! $cuota->recepciones == '' ? 'width="10%"' : '' !!}>
                                <strong>{{ $cuota->recepciones }}</strong>
                            </td>

                            <td align="center" {!! $cuota->totalRecepciones == '' ? 'width="12%"' : '' !!}>
                                <strong>{{ $cuota->totalRecepciones }}</strong>
                            </td>

                            <td align="center" >
                                <strong>{{ $cuota->orden_compra }}</strong>
                            </td>

                            <td align="center" >
                                <strong>{{ $cuota->numero_documento }}</strong>
                            </td>

                            <td align="center" >
                                <strong>{{ $cuota->fecha_documento }}</strong>
                            </td>

                            @if ( $contrato->cuotas == 1 )
                                <td align="center" >
                                    <strong>{{ $cuota->num_cuota }}</strong>
                                </td>
                            @endif

                            <td align="center" >
                                <strong>{{ $cuota->periodo_doc }}</strong>
                            </td>

                            <td align="center" >
                                <strong>{{ $cuota->monto_doc }}</strong>
                            </td>
                        </tr>
                    @endforeach

                    <tr>
                        <td align="right" style="background-color: #dddddd;"  >
                            <strong>TOTAL RECEPCIONES</strong>
                        </td>

                        <td align="center" >
                            <strong>$ {{formatoMiles( $contrato->total_monto_recepciones ) }}</strong>
                        </td>

                        <td align="right" style="background-color: #dddddd;" {!! $contrato->cuotas == 1 ? 'colspan="5"' : 'colspan="4"' !!}  >
                            <strong>TOTAL CONSUMIDO</strong>
                        </td>

                        <td align="center" >
                            <strong>$ {{ formatoMiles( $contrato->getDocumentosContrato->sum('getDocumento.total_documento') ) }}</strong>
                        </td>
                    </tr>

                    <tr>
                        <td align="right" style="background-color: #dddddd;"  >
                            <strong>SALDO CONTRATO</strong>
                        </td>

                        <td align="center" >
                            <strong>$ {{formatoMiles( $contrato->monto_contrato - $contrato->total_monto_recepciones ) }}</strong>
                        </td>

                        <td align="right" style="background-color: #dddddd;" {!! $contrato->cuotas == 1 ? 'colspan="5"' : 'colspan="4"' !!}  >
                            <strong>SALDO CONTRATO</strong>
                        </td>

                        <td align="center" >
                            <strong>$ {{ formatoMiles( $contrato->monto_contrato - $contrato->getDocumentosContrato->sum('getDocumento.total_documento') ) }}</strong>
                        </td>
                    </tr>
                </table>
            </div>
        </div>
        
	</body>
</html>