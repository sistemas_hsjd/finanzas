<!DOCTYPE html>
<html>
	<head>
        <link href="{{asset('sistema/css/base.css')}}" rel="stylesheet" type="text/css"/>
		<link href="{{asset('assets/plugins/bootstrap/css/bootstrap.min.css')}}" rel="stylesheet" type="text/css"/>
		<style type="text/css">
			table.table-bordered{ font-size: 11px; }
			table.table-bordered td{ padding: 3px;border-color: #909090!important;color: #000000!important;}
			table.table-bordered th{ padding: 3px;border-color: #909090!important;}
			.table-bordered tr td{
				font-size: 12px;
			}
			.justify{
				text-align : justify;
			}
			.sin-salto{
				page-break-inside: avoid;
			}
		</style>
		<title>MEMORANDUM SOLICITUD N° {{ $vistoBueno->memo }}</title>
	</head>
	<body class="pace-running">
		<div class="col-xs-12 padding-0 table-responsive">
			<table class="table">
				<tr>
					<td>
						<h4 class="bigger text-right" width="50%">
							<img style="padding-bottom: 8px;" src="minsal180.png" style="height: 80px;" >
						</h4>
					</td>
					<td  width="10%">
						<h4 style="padding-bottom: 8px; " >
							<img style="padding-bottom: 8px;margin-right:0px;" src="logohsjd.png" >
						</h4>
					</td>
                    <td style="text-align: right;" width="40%">
                        <p>Servicio Salud Metropolitano Occidente</p>
						<p>Hospital San Juan de Dios - CDT</p>
                    </td>
				</tr>
				<tr>
					<td colspan="2">
						<h6><strong>MINISTERIO DE SALUD</strong></h6>
						<h6><strong>SERVICIO DE SALUD OCCIDENTE</strong></h6>
						<h6><strong>HOSPITAL SAN JUAN DE DIOS</strong></h6>
						<h6><strong>CR DE FINANZAS.</strong></h6>
						<h6><strong>DVC/RBG/{{ $vistoBueno->getDigitador->iniciales }}</strong></h6>
					</td>
				</tr>
			</table>
		</div>
		<div class="col-xs-12 padding-0 table-responsive ">
			<table class="table">
				<tr>
					<td></td>
					<td style="text-align: right;" >
						<h6><strong>MEMORANDUM N° {{ $vistoBueno->memo }}</strong></h6>
						<h6><strong>MAT.</strong> V°B° de servicios</h6>
						<h6><strong>ANT.</strong> Adjunta copia de factura</h6>
						<h6>&nbsp;</h6>
						<h6><strong>Santiago, {{ $vistoBueno->fechaMemo }}</strong></h6>
					</td>
				</tr>
				<tr>
					<td style="text-align: left;" >
						<h6><strong>DE: {{ $referenteTecnicoOrigen->responsable }}</strong></h6>
						<h6><strong>{{ $referenteTecnicoOrigen->nombre }}</strong></h6>
						<h6><strong>HOSPITAL SAN JUAN DE DIOS</h6>
						<h6>&nbsp;</h6>
						<h6><strong>A: {{ $vistoBueno->getReferenteTecnico->responsable }}</strong></h6>
						<h6><strong>{{ $vistoBueno->getReferenteTecnico->nombre }}</strong></h6>
						<h6><strong>HOSPITAL SAN JUAN DE DIOS</h6>
					</td>
					<td></td>
				</tr>
			</table>
		</div>
		<div class="col-xs-12 padding-0">
			<p>Junto con saludar, me permito enviar copias de facturas para su revisión, V°B° y autorización de pago, si corresponde, según procedimiento: &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
            &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; @if( $vistoBueno->fecha_informe != null )Día: <b>{{ fecha_dmY($vistoBueno->fecha_informe) }}</b>@endif @if( $vistoBueno->informe != null ) Informe N° <b>{{ $vistoBueno->informe }}@endif</b></p>
		</div>
		<br>
		<div class="col-xs-12 padding-0">
			<div class="table-responsive" >
				<table class="table table-bordered " >
					<tr>
						<td align="center" style="background-color: #eeeeee;" >
							<strong>TD</strong>
						</td>
						<td align="center" style="background-color: #eeeeee;" >
							<strong>Orden Compra</strong>
						</td>
						<td align="center" style="background-color: #eeeeee;" >
							<strong>Nombre Proveedor</strong>
						</td>
						<td align="center" style="background-color: #eeeeee;" >
							<strong>Rut</strong>
						</td>
						<td align="center" style="background-color: #eeeeee;" >
							<strong>N°</strong>
						</td>
						<td align="center" style="background-color: #eeeeee;" >
							<strong>Fecha Doc.</strong>
						</td>
						<td align="center" style="background-color: #eeeeee;" >
							<strong>Monto</strong>
						</td>
					</tr>
					    @forelse($vistoBueno->getDocumentos as $documento)
                            <tr>
                                <td class="text-center"	width="4%" >
                                    {{ $documento->getTipoDocumento->sigla }}
                                </td>
                                <td class="text-center"	width="10%" >
                                    {{ $documento->documento_compra }}
                                </td>
                                <td width="34%" >
                                    {{ $documento->getProveedor->nombre }}
                                </td>
                                <td class="text-center" width="10%" >
                                    {{ $documento->getProveedor->formatRut() }}
                                </td>
                                <td class="text-center"	width="10%" >
                                    {{ $documento->numero_documento }}
                                </td>
                                <td class="text-center"	width="14%" >
                                    {{ fecha_dmY($documento->fecha_documento) }}
                                </td>
                                <td class="text-center"	width="18%" >
                                    $ {{ formatoMiles($documento->total_documento) }}
                                </td>
                            </tr>
						@empty

                        @endforelse
				</table>
			</div>
		</div>
		<br>
		<div class="col-xs-12 padding-0" style="margin-top:40px;">
			<div class="table-responsive sin-salto" >
				<table class="table" >
					<tr>
						<td>
							<p>Saluda Atentamente.</p>
						</td>
					</tr>
					<tr>
						<td>
							&nbsp;
						</td>
					</tr>
					<tr>
						<td>
							&nbsp;
						</td>
					</tr>
					<tr>
						<td class="text-center">
							{{-- @if ($referenteTecnicoOrigen->url_firma != null)
								<img style="padding-bottom: 1px;margin:0px;height: 80px;" src="firma_rodrigo.png" heigth="220" width="180" >
							@endif --}}
							<p style="margin: 0px;" ><strong>{{ $referenteTecnicoOrigen->responsable }}</strong></p>
							<p style="margin: 0px;" ><strong>{{ $referenteTecnicoOrigen->nombre }}</strong></p>
							<p style="margin: 0px;" ><strong>HOSPITAL SAN JUAN DE DIOS</strong></p>
						</td>
					</tr>
					<tr>
						<td>
							&nbsp;
						</td>
					</tr>
					<tr>
						<td>
							<p style="margin: 0px;text-decoration: underline black;" >Distribución:</p>
							<p style="margin: 0px;" >- Archivo</p>
						</td>
					</tr>
				</table>
			</div>
		</div>
		{{--
		<div class="col-xs-12 padding-0 sin-salto">
			<div class="col-xs-12 padding-0">
				<p>Saluda Atentamente.</p>
			</div>
			<br>
			<div class="col-xs-12 padding-0 text-center">
				<p style="margin: 0px;" ><strong>{$arr_origen.gl_responsable}</strong></p>
				<p style="margin: 0px;" ><strong>{$arr_origen.gl_nombre}</strong></p>
				<p style="margin: 0px;" ><strong>HOSPITAL SAN JUAN DE DIOS</strong></p>
			</div>
			<br>
			<br>
			<div class="col-xs-12 padding-0">
				<p style="margin: 0px;" >DISTRIBUCIÓN</p>
				<p style="margin: 0px;" >- ARCHIVO</p>
			</div>
		</div>
		--}}
	</body>
</html>