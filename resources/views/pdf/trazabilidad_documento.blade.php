<!DOCTYPE html>
<html>
	<head>
        <link href="{{asset('sistema/css/base.css')}}" rel="stylesheet" type="text/css"/>
		<link href="{{asset('assets/plugins/bootstrap/css/bootstrap.min.css')}}" rel="stylesheet" type="text/css"/>
		<style type="text/css">
			table.table-bordered{ font-size: 11px; }
			table.table-bordered td{ padding: 3px;border-color: #909090!important;color: #000000!important;}
			table.table-bordered th{ padding: 3px;border-color: #909090!important;}
			.table-bordered tr td{
				font-size: 12px;
			}
			.justify{
				text-align : justify;
			}
			.sin-salto{
				page-break-inside: avoid;
			}

		</style>
		<title>INFORME TRAZABILIDAD DOCUMENTO N° {{ $documento->numero_documento }} - DEPARTAMENTO FINANZAS - HSJD</title>
	</head>
	<body class="pace-running">
        <div class="col-xs-12 padding-0">
            <div class="table-responsive" >
                <table class="table table-bordered " >
                    <tr>
                        <td align="center" style="background-color: #eeeeee; border-color:black; " >
                            <strong>INFORME TRAZABILIDAD DOCUMENTO - DEPARTAMENTO FINANZAS - HSJD</strong>
                        </td>
                    </tr>
                </table>
            </div>
        </div>

        <div class="col-xs-3 padding-0">
            <div class="table-responsive" >
                <table class="table table-bordered " >
                    <tr>
                        <td align="center" style="background-color: #eeeeee; border-color:black;" >
                            <strong>INFORMACIÓN GENERAL</strong>
                        </td>
                    </tr>
                </table>
            </div>
        </div>

        <div class="col-xs-12 padding-0">
            <div class="table-responsive" >
                <table class="table table-bordered " >
                    <tr>
                        <td align="center" style="background-color: #eeeeee;" >
                            <strong>PROVEEDOR</strong>
                        </td>
                        <td align="center" >
                            <strong>{{ $documento->getProveedor->nombre }}</strong>
                        </td>
                        <td align="center" style="background-color: #eeeeee;" >
                            <strong>RUT</strong>
                        </td>
                        <td align="center" >
                            <strong>{{ $documento->getProveedor->rut }}</strong>
                        </td>
                    </tr>
                </table>
            </div>
        </div>

        <div class="col-xs-12 padding-0">
            <div class="table-responsive" >
                <table class="table table-bordered " >
                    <tr>
                        <td align="center" style="background-color: #eeeeee;" >
                            <strong>TIPO DOCUMENTO</strong>
                        </td>
                        <td align="center" >
                            <strong>{{ $documento->getTipoDocumento->nombre }}</strong>
                        </td>
                        <td align="center" style="background-color: #eeeeee;" >
                            <strong>FECHA DOCUMENTO</strong>
                        </td>
                        <td align="center" >
                            <strong>{{ fecha_dmY($documento->fecha_documento) }}</strong>
                        </td>
                    </tr>

                    <tr>
                        <td align="center" style="background-color: #eeeeee;" >
                            <strong>N° DOCUMENTO</strong>
                        </td>
                        <td align="center" >
                            <strong>{{ $documento->numero_documento }}</strong>
                        </td>
                        <td align="center" style="background-color: #eeeeee;" >
                            <strong>FECHA RECEPCIÓN</strong>
                        </td>
                        <td align="center" >
                            <strong>{{ fecha_dmY($documento->fecha_recepcion) }}</strong>
                        </td>
                    </tr>

                    <tr>
                        <td align="center" style="background-color: #eeeeee;" >
                            <strong>MODALIDAD DE COMPRA</strong>
                        </td>
                        <td align="center" >
                            <strong>{{ $documento->getModalidadCompra->nombre }}</strong>
                        </td>
                        <td align="center" style="background-color: #eeeeee;" >
                            <strong>VALOR TOTAL ACT.</strong>
                        </td>
                        <td align="center" >
                            <strong>$ {{ formatoMiles($documento->total_documento_actualizado) }}</strong>
                        </td>
                    </tr>

                    <tr>
                        <td align="center" style="background-color: #eeeeee;" >
                            <strong>N° DOCUMENTO COMPRA</strong>
                        </td>
                        <td align="center" >
                            <strong>{{ $documento->documento_compra }}</strong>
                        </td>
                        <td align="center" style="background-color: #eeeeee;" >
                            <strong>VALOR TOTAL ORIG.</strong>
                        </td>
                        <td align="center" >
                            <strong>$ {{ formatoMiles($documento->total_documento) }}</strong>
                        </td>
                    </tr>

                    @if ( $documento->id_tipo_documento == 3 )
                        <tr>
                            <td align="center" style="background-color: #eeeeee;" >
                                <strong>10% IMPUESTO</strong>
                            </td>
                            <td align="center" >
                                <strong>$ {{ formatoMiles($documento->impuesto) }}</strong>
                            </td>
                            <td align="center" style="background-color: #eeeeee;" >
                                <strong>LÍQUIDO</strong>
                            </td>
                            <td align="center" >
                                <strong>$ {{ formatoMiles($documento->liquido) }}</strong>
                            </td>
                        </tr>
                    @endif
                </table>
            </div>
        </div>

        <div class="col-xs-3 padding-0">
            <div class="table-responsive" >
                <table class="table table-bordered " >
                    <tr>
                        <td align="center" style="background-color: #eeeeee; border-color:black;" >
                            <strong>ORDEN DE COMPRA</strong>
                        </td>
                    </tr>
                </table>
            </div>
        </div>

        <div class="col-xs-12 padding-0">
            <div class="table-responsive" >
                <table class="table table-bordered " >
                    <tr>
                        <td align="center" style="background-color: #eeeeee;" width="25%">
                            <strong>N° OC</strong>
                        </td>
                        <td align="center" width="25%">
                            @if ($documento->getUnaRelacionRecepcionValidada)
                            <strong>{{ $documento->getUnaRelacionRecepcionValidada->getWsDocumento->getWsOrdenCompra->numero_orden_compra_mercado_publico }}</strong>
                            @endif
                        </td>
                        <td align="center" style="background-color: #eeeeee;" width="25%">
                            <strong>MONTO MÁXIMO</strong>
                        </td>
                        <td align="center" width="25%">
                            @if ($documento->getUnaRelacionRecepcionValidada)
                            <strong>$ {{ formatoMiles($documento->getUnaRelacionRecepcionValidada->getWsDocumento->getWsOrdenCompra->total_orden_compra) }}</strong>
                            @endif
                        </td>
                    </tr>

                    <tr>
                        <td align="center" style="background-color: #eeeeee;" >
                            <strong>FECHA</strong>
                        </td>
                        <td align="center" >
                            @if ($documento->getUnaRelacionRecepcionValidada)
                            <strong>{{ fecha_dmY($documento->getUnaRelacionRecepcionValidada->getWsDocumento->getWsOrdenCompra->fecha_orden_compra_mercado_publico) }}</strong>
                            @endif
                        </td>
                        <td align="center" style="background-color: #eeeeee;" >
                            <strong>SALDO OC</strong>
                        </td>
                        <td align="center" >
                            @if ($documento->getUnaRelacionRecepcionValidada)
                            <strong>$ {{ formatoMiles($documento->getUnaRelacionRecepcionValidada->getWsDocumento->getWsOrdenCompra->saldo_orden_compra) }}</strong>
                            @endif
                        </td>
                    </tr>

                </table>
            </div>
        </div>

        @if ( $documento->getDocumentosRelacionados->count() > 0 || $documento->id_relacionado != null || $documento->getArchivoAceptaRefacturado )
            <div class="col-xs-3 padding-0">
                <div class="table-responsive" >
                    <table class="table table-bordered " >
                        <tr>
                            <td align="center" style="background-color: #eeeeee; border-color:black;" >
                                <strong>DOCUMENTOS ASOCIADOS</strong>
                            </td>
                        </tr>
                    </table>
                </div>
            </div>

            @if ( $documento->getArchivoAceptaRefacturado != null )
                <div class="col-xs-12 padding-0">
                    <div class="table-responsive" >
                        <table class="table table-bordered " >
                            <tr>
                                <td align="center" style="background-color: #eeeeee;" >
                                    <strong>TIPO DOCUMENTO</strong>
                                </td>
                                <td align="center" >
                                    <strong>{{ $documento->getArchivoAceptaRefacturado->getTipoDocumento->nombre }} (Rechazado Acepta)</strong>
                                </td>
                                <td align="center" style="background-color: #eeeeee;" >
                                    <strong>FECHA</strong>
                                </td>
                                <td align="center" >
                                    <strong>{{ fecha_dmY($documento->getArchivoAceptaRefacturado->emision) }}</strong>
                                </td>
                            </tr>

                            <tr>
                                <td align="center" style="background-color: #eeeeee;" >
                                    <strong>N° DOCUMENTO</strong>
                                </td>
                                <td align="center" >
                                    <strong>{{ $documento->getArchivoAceptaRefacturado->folio }}</strong>
                                </td>
                                <td align="center" style="background-color: #eeeeee;" >
                                    <strong>MONTO</strong>
                                </td>
                                <td align="center" >
                                    <strong>$ {{ formatoMiles($documento->getArchivoAceptaRefacturado->monto_total) }}</strong>
                                </td>
                            </tr>

                        </table>
                    </div>
                </div>
            @endif

            @if ( $documento->getDocumentoRelacionado != null )
                <div class="col-xs-12 padding-0">
                    <div class="table-responsive" >
                        <table class="table table-bordered " >
                            <tr>
                                <td align="center" style="background-color: #eeeeee;" >
                                    <strong>TIPO DOCUMENTO</strong>
                                </td>
                                <td align="center" >
                                    <strong>{{ $documento->getDocumentoRelacionado->getTipoDocumento->nombre }}</strong>
                                </td>
                                <td align="center" style="background-color: #eeeeee;" >
                                    <strong>FECHA</strong>
                                </td>
                                <td align="center" >
                                    <strong>{{ fecha_dmY($documento->getDocumentoRelacionado->fecha_documento) }}</strong>
                                </td>
                            </tr>

                            <tr>
                                <td align="center" style="background-color: #eeeeee;" >
                                    <strong>N° DOCUMENTO</strong>
                                </td>
                                <td align="center" >
                                    <strong>{{ $documento->getDocumentoRelacionado->numero_documento }}</strong>
                                </td>
                                <td align="center" style="background-color: #eeeeee;" >
                                    <strong>MONTO</strong>
                                </td>
                                <td align="center" >
                                    <strong>$ {{ formatoMiles($documento->getDocumentoRelacionado->total_documento) }}</strong>
                                </td>
                            </tr>

                        </table>
                    </div>
                </div>
            @endif

            @forelse ($documento->getDocumentosRelacionados as $doc)
                <div class="col-xs-12 padding-0">
                    <div class="table-responsive" >
                        <table class="table table-bordered " >
                            <tr>
                                <td align="center" style="background-color: #eeeeee;" >
                                    <strong>TIPO DOCUMENTO</strong>
                                </td>
                                <td align="center" >
                                    <strong>{{ $doc->getTipoDocumento->nombre }}</strong>
                                </td>
                                <td align="center" style="background-color: #eeeeee;" >
                                    <strong>FECHA</strong>
                                </td>
                                <td align="center" >
                                    <strong>{{ fecha_dmY($doc->fecha_documento) }}</strong>
                                </td>
                            </tr>
    
                            <tr>
                                <td align="center" style="background-color: #eeeeee;" >
                                    <strong>N° DOCUMENTO</strong>
                                </td>
                                <td align="center" >
                                    <strong>{{ $doc->numero_documento }}</strong>
                                </td>
                                <td align="center" style="background-color: #eeeeee;" >
                                    <strong>MONTO</strong>
                                </td>
                                <td align="center" >
                                    <strong>$ {{ formatoMiles($doc->total_documento) }}</strong>
                                </td>
                            </tr>
    
                        </table>
                    </div>
                </div>
            @empty
                
            @endforelse
            
        @endif

        <div class="col-xs-3 padding-0">
            <div class="table-responsive" >
                <table class="table table-bordered " >
                    <tr>
                        <td align="center" style="background-color: #eeeeee; border-color:black;" >
                            <strong>ITEM PRESUPUESTARIO</strong>
                        </td>
                    </tr>
                </table>
            </div>
        </div>

        <div class="col-xs-12 padding-0">
            <div class="table-responsive" >
                <table class="table table-bordered " >
                    <tr>
                        <td align="center" style="background-color: #eeeeee;" >
                            <strong>CÓDIGO</strong>
                        </td>
                        <td align="center" style="background-color: #eeeeee;" >
                            <strong>DESCRIPCIÓN</strong>
                        </td>
                        <td align="center" style="background-color: #eeeeee;" >
                            <strong>MONTO</strong>
                        </td>
                        @if ( $documento->getFoliosSigfe->count() > 0 )
                            <td align="center" style="background-color: #eeeeee;" >
                                <strong>ID SIGFE</strong>
                            </td>
                        @endif
                    </tr>
                    @if ( $documento->getFoliosSigfe->count() > 0 )
                        @foreach ($documento->getFoliosSigfe as $folioSigfe)
                            @forelse ( $folioSigfe->getDevengos as $devengo )
                                <tr>
                                    <td align="center" >
                                        <strong>{{ $devengo->getItemPresupuestario->codigo() }}</strong>
                                    </td>
                                    <td align="center" >
                                        <strong>{{ $devengo->getItemPresupuestario->clasificador_presupuestario }}</strong>
                                    </td>
                                    <td align="center" >
                                        <strong>$ {{ formatoMiles($devengo->monto) }}</strong>
                                    </td>
                                    <td align="center" >
                                        <strong>{{ $folioSigfe->id_sigfe }}</strong>
                                    </td>
                                </tr>
                            @empty

                            @endforelse
                        @endforeach
                    @else
                        @forelse($documento->getDevengos as $devengo)
                            <tr>
                                <td align="center" >
                                    <strong>{{ $devengo->getItemPresupuestario->codigo() }}</strong>
                                </td>
                                <td align="center" >
                                    <strong>{{ $devengo->getItemPresupuestario->clasificador_presupuestario }}</strong>
                                </td>
                                <td align="center" >
                                    <strong>$ {{ formatoMiles($devengo->monto) }}</strong>
                                </td>
                            </tr>
                        @empty

                        @endforelse
                    @endif
                </table>
            </div>
        </div>

        @if ( $documento->getFoliosSigfe->count() > 0 )
            <div class="col-xs-3 padding-0">
                <div class="table-responsive" >
                    <table class="table table-bordered " >
                        <tr>
                            <td align="center" style="background-color: #eeeeee; border-color:black;" >
                                <strong>DEVENGO</strong>
                            </td>
                        </tr>
                    </table>
                </div>
            </div>

            <div class="col-xs-12 padding-0">
                <div class="table-responsive" >
                    <table class="table table-bordered " >
                        @foreach ($documento->getFoliosSigfe as $folioSigfe)
                            <tr>
                                <td align="center" style="background-color: #eeeeee;" >
                                    <strong>ID SIGFE</strong>
                                </td>
                                <td align="center" >
                                    <strong>{{ $folioSigfe->id_sigfe }}</strong>
                                </td>
                                <td align="center" style="background-color: #eeeeee;" >
                                    <strong>FECHA SIGFE</strong>
                                </td>
                                <td align="center" >
                                    <strong>{{ fecha_dmY($folioSigfe->fecha_sigfe) }}</strong>
                                </td>
                            </tr>

                            <tr>
                                <td align="center" style="background-color: #eeeeee;" >
                                    <strong>DETALLES</strong>
                                </td>
                                <td align="center" colspan="3">
                                    <strong>{{ $folioSigfe->detalle_sigfe }}</strong>
                                </td>
                                {{-- <td align="center" style="background-color: #eeeeee;" >
                                    <strong>OBSERVACIONES</strong>
                                </td>
                                <td align="center" >
                                    <strong>{{ $documento->observacion }}</strong>
                                </td> --}}
                            </tr>
                            @if ( ! $loop->last )
                                <tr>
                                    <td colspan="4" style="border-right: solid 1px white;border-left:solid 1px white;"></td>
                                </tr>
                            @endif

                        @endforeach
                    </table>
                </div>
            </div>
        @endif

        <div class="col-xs-3 padding-0">
            <div class="table-responsive" >
                <table class="table table-bordered " >
                    <tr>
                        <td align="center" style="background-color: #eeeeee; border-color:black;" >
                            <strong>EGRESO</strong>
                        </td>
                    </tr>
                </table>
            </div>
        </div>

        <div class="col-xs-12 padding-0">
            <div class="table-responsive" >
                <table class="table table-bordered " >
                    <tr>
                        <td align="center" style="background-color: #eeeeee;" width="25%">
                            <strong>N° EGRESO</strong>
                        </td>
                        <td align="center" width="25%">
                            
                        </td>
                        <td align="center" style="background-color: #eeeeee;" width="25%">
                            <strong>CTA. BANCARIA</strong>
                        </td>
                        <td align="center" width="25%">
                        </td>
                    </tr>

                    <tr>
                        <td align="center" style="background-color: #eeeeee;" >
                            <strong>FECHA EGRESO</strong>
                        </td>
                        <td align="center" >
                        </td>
                        <td align="center" style="background-color: #eeeeee;" >
                            <strong>MONTO</strong>
                        </td>
                        <td align="center" >
                        </td>
                    </tr>

                    <tr>
                        <td align="center" style="background-color: #eeeeee;" >
                            <strong>MEDIO PAGO</strong>
                        </td>
                        <td align="center" colspan="3">
                        </td>
                    </tr>

                </table>
            </div>
        </div>

        <div class="col-xs-3 padding-0">
            <div class="table-responsive" >
                <table class="table table-bordered " >
                    <tr>
                        <td align="center" style="background-color: #eeeeee;border-color:black;" >
                            <strong>OBSERVACIONES</strong>
                        </td>
                    </tr>
                </table>
            </div>
        </div>
        
        <div class="col-xs-12 padding-0">
            <div class="table-responsive" >
                <table class="table table-bordered " >
                    <tr>
                        <td align="center" height="100">{{ $documento->observacion }}</td>
                    </tr>
                </table>
            </div>
        </div>

	</body>
</html>