<!DOCTYPE html>
<html>
	<head>
        <link href="{{asset('sistema/css/base.css')}}" rel="stylesheet" type="text/css"/>
		<link href="{{asset('assets/plugins/bootstrap/css/bootstrap.min.css')}}" rel="stylesheet" type="text/css"/>
		<style type="text/css">
			table.table-bordered{ font-size: 11px; }
			table.table-bordered td{ padding: 3px;border-color: #909090!important;color: #000000!important;}
			table.table-bordered th{ padding: 3px;border-color: #909090!important;}
			.table-bordered tr td{
				font-size: 12px;
			}
			.justify{
				text-align : justify;
			}
			.sin-salto{
				page-break-inside: avoid;
                font-size: 10px;
			}
		</style>
		<title>Comprobante de {{ $comprobante->getTipoComprobante->nombre }} - Departamento de Finanzas - HSJD </title>
	</head>
	<body class="pace-running">
		<div class="col-xs-12 padding-0">
			<table class="table">
                <tr>
                    <td style="text-align: right;" width="40%"></td>
                    <td style="text-align: right;" width="10%"></td>
                    <td style="text-align: right;" width="40%">
                        <h3>N° {{ $comprobante->getNumeroComprobante(6) }}</h3>
                    </td>
                </tr>
				<tr>
					<td>
						<h4 class="bigger text-right" width="50%">
							<img style="padding-bottom: 8px;" src="minsal180.png" style="height: 80px;" >
						</h4>
					</td>
					<td style="text-align: right; " width="10%">
						<h4 style="padding-bottom: 8px; " >
							<img style="padding-bottom: 8px;margin-right:-35px;" src="logohsjd.png" >
						</h4>
					</td>
                    <td style="text-align: right;" width="40%">
                        <p>Servicio Salud Metropolitano Occidente</p>
						<p>Hospital San Juan de Dios - CDT</p>
                    </td>
				</tr>
				<tr>
					<td colspan="2">
						<h6><strong>Ministerio de salud</strong></h6>
						{{-- <h6><strong>SERVICIO DE SALUD OCCIDENTE</strong></h6>
						<h6><strong>HOSPITAL SAN JUAN DE DIOS</strong></h6> --}}
					</td>
				</tr>
			</table>
		</div>
		<div class="col-xs-12 padding-0">
			<table class="table" >
				<tr>
					<td style="text-align: left;" >
                        <h4><strong>Comprobante de {{ $comprobante->getTipoComprobante->nombre }} </strong></h4>
                        <hr style="margin-top:0px;margin-bottom:-5px;">
                    </td>
				</tr>
			</table>
        </div>
        
		{{-- <div class="col-xs-12 padding-0">
			<p>Junto con saludar, me permito dar conformidad a las siguientes Facturas y se solicita dar curso al pago.</p>
		</div> --}}

        <div class="col-xs-12 padding-0" style="margin-top:0px;">
			<div class="table-responsive" >
				<table class="table table-bordered " >
					<tr>
						<td align="center" style="background-color: #eeeeee;" >
							<strong>Institución</strong>
                        </td>
                        <td>Servicio de Salud Metropolitano Occidente / Hospital San Juan de Dios</td>
                    </tr>

                    <tr>
						<td align="center" style="background-color: #eeeeee;" >
							<strong>Título</strong>
                        </td>
                        <td>{{ $comprobante->titulo }}</td>
                    </tr>
                    <tr>
						<td align="center" style="background-color: #eeeeee;" >
							<strong>Descripción</strong>
                        </td>
                        <td>{{ $comprobante->descripcion }}</td>
                    </tr>
                    
				</table>
			</div>
        </div>
        
        
		<div class="col-xs-12 padding-0">
			<div class="table-responsive" >
				<table class="table table-bordered " >
					<tr>
                        @if ( $comprobante->folio )
                            <td align="center" style="background-color: #eeeeee;" >
                                <strong>Folio</strong>
                            </td>
                            <td class="text-center">{{ $comprobante->folio }}</td>
                        @endif

						<td align="center" style="background-color: #eeeeee;" >
							<strong>Periodo de Operación</strong>
                        </td>
                        <td class="text-center">{{ getMes($comprobante->mes_proceso) }}</td>

						<td align="center" style="background-color: #eeeeee;" >
							<strong>Ejercicio Fiscal</strong>
                        </td>
                        <td class="text-center">{{ $comprobante->año_proceso }}</td>
					
                    </tr>
                    
				</table>
			</div>
        </div>
        
        <div class="col-xs-12 padding-0">
            <h5>Principal : {{ $comprobante->getComprobantesContablesDocumentos->last()->getProveedor->rut.' '.$comprobante->getComprobantesContablesDocumentos->last()->getProveedor->nombre }}</h5>
        </div>
        
        <div class="col-xs-12 padding-0">
            <div class="table-responsive" >
                <table class="table table-bordered " >
                    <tr>
                        <td align="center" style="background-color: #eeeeee;" >
                            <strong>Tipo Documento</strong>
                        </td>
                        <td align="center" style="background-color: #eeeeee;" >
                            <strong>N° Doc.</strong>
                        </td>
                        @if ( ! $comprobante->folio )
                        <td align="center" style="background-color: #eeeeee;" >
                            <strong>Folio</strong>
                        </td>
                        @endif
                        <td align="center" style="background-color: #eeeeee;" >
                            <strong>Cuenta Contable</strong>
                        </td>
                        <td align="center" style="background-color: #eeeeee;" >
                            <strong>Cuenta Bancaria</strong>
                        </td>
                        <td align="center" style="background-color: #eeeeee;" >
                            <strong>Medio de Pago</strong>
                        </td>
                        <td align="center" style="background-color: #eeeeee;" >
                            <strong>N° Documento de Pago</strong>
                        </td>
                        <td align="center" style="background-color: #eeeeee;" >
                            <strong>Monto</strong>
                        </td>
                    </tr>

                    @forelse($comprobante->getComprobantesContablesDocumentos as $comprobanteDoc)
                        <tr>
                            <td align="center">{{ $comprobanteDoc->getDocumento->getTipoDocumento->nombre }}</td>
                            <td align="center">{{ $comprobanteDoc->getDocumento->numero_documento }}</td>
                            @if ( ! $comprobante->folio )
                            <td align="center">{{ $comprobanteDoc->folio }}</td>
                            @endif
                            <td align="center">
                                {{ $comprobanteDoc->getCuentaContable ? $comprobanteDoc->getCuentaContable->codigo.' '.$comprobanteDoc->getCuentaContable->glosa : '' }}
                            </td>
                            <td align="center">
                                {{ $comprobanteDoc->getCuentaBancaria ? $comprobanteDoc->getCuentaBancaria->codigo : '' }}<br>
                                {{ $comprobanteDoc->getCuentaBancaria ? $comprobanteDoc->getCuentaBancaria->getBanco->nombre : '' }}
                            </td>
                            <td align="center">{{ $comprobanteDoc->getMedioPago ? $comprobanteDoc->getMedioPago->nombre : '' }}</td>
                            <td align="center">{{ $comprobanteDoc->numero_documento_pago }}</td>
                            <td align="center">{{ formatoMiles($comprobanteDoc->monto_comprobante) }}</td>
                        </tr>
                    @empty

                    @endforelse
                    <tr>
                        <td align="center" style="background-color: #eeeeee;" 
                            {!! $comprobante->folio ? 'colspan="6"' : 'colspan="7"' !!}  >
                            <strong>Total (CLP)</strong>
                        </td>
                        <td>
                            {{ formatoMiles($comprobante->monto_total) }}
                        </td>
                    </tr>
                        
                    
                    
                </table>
            </div>
        </div>

        
        @if( $usuarioRevision )
            <br><br>
            <div class="col-xs-12 padding-0" style="margin-top:40px;">
                <div class="table-responsive sin-salto" >
                    <table class="table" >
                        <tr>
                            <td width="20%">
                                <h5>Usuario Revisión</h5>
                            </td>
                            <td  align="left">
                                <h5>{{ $usuarioRevision->name }}<h5>
                            </td>
                        </tr>
                    </table>
                </div>
            </div>
        @endif


        <br><br>
        <div class="col-xs-12 padding-0" style="margin-top:40px;">
            <div class="table-responsive sin-salto" >
                <table class="table" >
                    <tr>
                        <td width="20%">
                            <h5>Autorizado Por</h5>
                        </td>
                        <td width="40%" align="right">
                            <h5>{{ $primerUsuario->name }}<h5>
                        </td>
                        <td width="40%" align="right">
                            <h5>{{ $segundoUsuario->name }}<h5>
                        </td>
                    </tr>
                </table>
            </div>
        </div>
        
	</body>
</html>