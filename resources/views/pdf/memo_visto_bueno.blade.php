<!DOCTYPE html>
<html>
	<head>
        <link href="{{asset('sistema/css/base.css')}}" rel="stylesheet" type="text/css"/>
		<link href="{{asset('assets/plugins/bootstrap/css/bootstrap.min.css')}}" rel="stylesheet" type="text/css"/>
		<style type="text/css">
			table.table-bordered{ font-size: 11px; }
			table.table-bordered td{ padding: 3px;border-color: #909090!important;color: #000000!important;}
			table.table-bordered th{ padding: 3px;border-color: #909090!important;}
			.table-bordered tr td{
				font-size: 12px;
			}
			.justify{
				text-align : justify;
			}
			.sin-salto{
				page-break-inside: avoid;
			}
		</style>
		<title>MEMORANDUM VISTO BUENO N° {{ $vistoBueno->memo_respuesta }}</title>
	</head>
	<body class="pace-running">
		<div class="col-xs-12 padding-0 table-responsive ">
			<table class="table">
				<tr>
					<td>
						<h4 class="bigger text-right" width="50%">
							<img style="padding-bottom: 8px;" src="minsal180.png" style="height: 80px;" >
						</h4>
					</td>
					<td style="text-align: right; " width="10%">
						<h4 style="padding-bottom: 8px; " >
							<img style="padding-bottom: 8px;margin-right:-35px;" src="logohsjd.png" >
						</h4>
					</td>
                    <td style="text-align: right;" width="40%">
                        <p>Servicio Salud Metropolitano Occidente</p>
						<p>Hospital San Juan de Dios - CDT</p>
                    </td>
				</tr>
				<tr>
					<td colspan="2">
						<h6><strong>MINISTERIO DE SALUD</strong></h6>
						<h6><strong>SERVICIO DE SALUD OCCIDENTE</strong></h6>
						<h6><strong>HOSPITAL SAN JUAN DE DIOS</strong></h6>
                        {{-- 
						<h6><strong>CR DE FINANZAS.</strong></h6>
						<h6><strong>DVC/RBG/{{ $vistoBueno->getDigitador->iniciales }}</strong></h6>
                        --}}
					</td>
				</tr>
			</table>
		</div>
		<div class="col-xs-12 padding-0 table-responsive">
			<table class="table">
				<tr>
					<td></td>
					<td style="text-align: right;" >
						<h6><strong>MEMORANDUM N° {{ $vistoBueno->memo_respuesta }}</strong></h6>
						<h6><strong>MAT.</strong> V°B° de servicios</h6>
                        {{-- 
						<h6><strong>ANT.</strong> Adjunta copia de factura</h6>
                        --}}
						<h6>&nbsp;</h6>
						<h6><strong>Santiago, {{ $vistoBueno->fechaMemo }}</strong></h6>
					</td>
				</tr>
				<tr>
					<td style="text-align: left;" >
						<h6><strong>DE: {{ $vistoBueno->getReferenteTecnico->responsable }}</strong></h6>
						<h6><strong>{{ $vistoBueno->getReferenteTecnico->nombre }}</strong></h6>
						<h6><strong>HOSPITAL SAN JUAN DE DIOS</h6>
						<h6>&nbsp;</h6>
						<h6><strong>A: {{ $referenteTecnicoOrigen->responsable }}</strong></h6>
						<h6><strong>{{ $referenteTecnicoOrigen->nombre }}</strong></h6>
						<h6><strong>HOSPITAL SAN JUAN DE DIOS</h6>
					</td>
					<td></td>
				</tr>
			</table>
		</div>
		<div class="col-xs-12 padding-0">
			<p>Junto con saludar, me permito dar conformidad a las siguientes Facturas y se solicita dar curso al pago.</p>
		</div>
		<br>
		<div class="col-xs-12 padding-0">
			<div class="table-responsive" >
				<table class="table table-bordered " >
					<tr>
						<td align="center" style="background-color: #eeeeee;" >
							<strong>N°</strong>
						</td>
						<td align="center" style="background-color: #eeeeee;" >
							<strong>Fecha Doc.</strong>
						</td>
						<td align="center" style="background-color: #eeeeee;" >
							<strong>Monto</strong>
						</td>
						<td align="center" style="background-color: #eeeeee;" >
							<strong>Orden Compra</strong>
						</td>
						<td align="center" style="background-color: #eeeeee;" >
							<strong>Nombre Proveedor</strong>
						</td>
						<td align="center" style="background-color: #eeeeee;" >
							<strong>Rut</strong>
						</td>
                        <td align="center" style="background-color: #eeeeee;" >
							<strong>Nota</strong>
						</td>
						
					</tr>
                    @forelse($vistoBueno->getDocumentos as $documento)
                        <tr>
                            <td class="text-center"	width="7%" >
                                {{ $documento->numero_documento }}
                            </td>
                            <td class="text-center"	width="7%" >
                                {{ fecha_dmY($documento->fecha_documento) }}
                            </td>
                            <td class="text-center"	width="10%" >
                                $ {{ formatoMiles($documento->total_documento) }}
                            </td>
                            <td class="text-center"	width="15%" >
                                {{ $documento->documento_compra }}
                            </td>
                            <td width="28%" >
                                {{ $documento->getProveedor->nombre }}
                            </td>
                            <td class="text-center" width="10%" >
                                {{ $documento->getProveedor->formatRut() }}
                            </td>
                            <td width="23%" >
                                {!! str_replace(PHP_EOL,"<br>", $documento->nota_visto_bueno) !!}
                            </td>
                        </tr>
                    @empty

                    @endforelse
				</table>
			</div>
		</div>
		<br>
        @if ( $vistoBueno->observacion_memo_respuesta != null && $vistoBueno->observacion_memo_respuesta != '' )
            <div class="col-xs-12 padding-0">
                <p>Obs: {!! str_replace(PHP_EOL,"<br>", $vistoBueno->observacion_memo_respuesta) !!}</p>
            </div>
        @endif
		<div class="col-xs-12 padding-0" style="margin-top:40px;">
			<div class="table-responsive sin-salto" >
				<table class="table" >
					<tr>
						<td>
							<p>Saluda Atentamente.</p>
						</td>
					</tr>
					<tr>
						<td>
							&nbsp;
						</td>
					</tr>
					<tr>
						<td>
							&nbsp;
						</td>
					</tr>
					<tr>
						<td class="text-center">
							@if ($vistoBueno->getReferenteTecnico->url_firma != null)
								<img style="padding-bottom: 1px;margin:0px;height: 80px;" src="{{ asset($vistoBueno->getReferenteTecnico->url_firma) }}"  >
							@endif
							<p style="margin: 0px;" ><strong>{{ $vistoBueno->getReferenteTecnico->responsable }}</strong></p>
							<p style="margin: 0px;" ><strong>{{ $vistoBueno->getReferenteTecnico->nombre }}</strong></p>
							<p style="margin: 0px;" ><strong>HOSPITAL SAN JUAN DE DIOS</strong></p>
						</td>
					</tr>
					<tr>
						<td>
							&nbsp;
						</td>
					</tr>
					<tr>
						<td>
							<p style="margin: 0px;text-decoration: underline black;" >Distribución:</p>
                            <p style="margin: 0px;" >- CR Administración y Finanzas</p>
							<p style="margin: 0px;" >- Archivo</p>
						</td>
					</tr>
				</table>
			</div>
		</div>
		{{--
		<div class="col-xs-12 padding-0 sin-salto">
			<div class="col-xs-12 padding-0">
				<p>Saluda Atentamente.</p>
			</div>
			<br>
			<div class="col-xs-12 padding-0 text-center">
				<p style="margin: 0px;" ><strong>{$arr_origen.gl_responsable}</strong></p>
				<p style="margin: 0px;" ><strong>{$arr_origen.gl_nombre}</strong></p>
				<p style="margin: 0px;" ><strong>HOSPITAL SAN JUAN DE DIOS</strong></p>
			</div>
			<br>
			<br>
			<div class="col-xs-12 padding-0">
				<p style="margin: 0px;" >DISTRIBUCIÓN</p>
				<p style="margin: 0px;" >- ARCHIVO</p>
			</div>
		</div>
		--}}
	</body>
</html>