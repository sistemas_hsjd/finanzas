@extends('master')

@section('title', 'Buscador Documentos')

@push('css-head')
<!-- BEGIN PAGE LEVEL STYLES -->
<link rel="stylesheet" type="text/css" href="{{asset('assets/plugins/select2/select2_conquer.css')}}"/>
<link rel="stylesheet" href="{{asset('assets/plugins/data-tables/DT_bootstrap.css')}}"/>
<link rel="stylesheet" href="{{asset('datatables/fixedColumns.dataTables.min.css')}}"/>

<link rel="stylesheet" type="text/css" href="{{asset('assets/plugins/bootstrap-datepicker/css/datepicker.css')}}"/>
<!-- END PAGE LEVEL STYLES -->

<style>
    .label-form {
        font-weight: bold;
        color: black;
        text-align: right;
    }

    .td-bold {
        font-weight: bold;
    }

</style>

@endpush

@section('content')
<!-- BEGIN CONTENT -->

<div class="page-content-wrapper">
	<div class="page-content-wrapper">
		<div class="page-content">

			<!-- BEGIN PAGE CONTENT-->
			<div class="row">
				<div class="col-md-12">
					<!-- BEGIN EXAMPLE TABLE PORTLET-->
					<div class="portlet">
						<div class="portlet-title">
							<div class="caption">
                                <i class="fas fa-search"></i> <i class="fa fa-files-o" style="color: black;"></i> Buscador Documentos
							</div>
						</div>
						<div class="portlet-body">
                            <form action="{{ url('documentos/filtrar') }}" method="post" enctype="multipart/form-data" id="filtros-docs" class="form-horizontal" autocomplete="off">
                                {{-- Permisos para no repetir la consulta al buscar datos --}}
                                <input type="hidden" class="form-control" name="permiso_ver_documento" value="{{ $verDocumento }}" >
                                <input type="hidden" class="form-control" name="permiso_ver_factura_documento_acepta" value="{{ $verFacturaDocumentoAcepta }}" >

                                <h4 class="form-section" style="color: #69aa46;"><strong>Filtros</strong>
                                <div class="btn-group pull-right">

                                    <button class="btn btn-success btn-xs" title="Generar Excel" formaction="{{ url('documentos/buscador/excel') }}">
                                        <i class="fas fa-file-excel"></i> Exportar a Excel
                                    </button>

                                    <button class="btn btn-info btn-xs" title="Filtrar Listado" id="filtroListado" type="button">
                                        <i class="fas fa-search" style="color:black;" ></i> Filtrar
                                    </button>
                                </div>
                                </h4>
                                <div class="form-group row">
                                
                                    <label for="filtro_fecha_a_utilizar" class="col-sm-1 control-label label-form">Fecha </label>
                                    <div class="col-sm-3">
                                        <select class="form-control select2_filtro " id="filtro_fecha_a_utilizar" name="filtro_fecha_a_utilizar" >
                                            @foreach ( $fechasUtilizar as $fechaUtilizar )
                                                <option value="{{ $fechaUtilizar->opcion }}" >{{ $fechaUtilizar->opcion }}</option>
                                            @endforeach
                                        </select>
                                    </div>

                                    <label for="filtro_fecha_inicio" class="col-sm-1 control-label label-form">Fecha Inicio</label>
                                    <div class="col-sm-3">
                                        <div class="input-group">
                                            <input type="text" class="form-control fechas" id="filtro_fecha_inicio" name="filtro_fecha_inicio" value="{{ date("d/m/Y") }}" >
                                            <span class="input-group-addon" onclick="fechaDocumento('filtro_fecha_inicio');" style="cursor:pointer;"><i class="fa fa-calendar" aria-hidden="true" style="color: black;"></i></span>
                                        </div>
                                    </div>

                                    <label for="filtro_fecha_termino" class="col-sm-1 control-label label-form">Fecha Termino</label>
                                    <div class="col-sm-3">
                                        <div class="input-group">
                                            <input type="text" class="form-control fechas" id="filtro_fecha_termino" name="filtro_fecha_termino" value="{{ date("d/m/Y") }}" >
                                            <span class="input-group-addon" onclick="fechaDocumento('filtro_fecha_termino');" style="cursor:pointer;"><i class="fa fa-calendar" aria-hidden="true" style="color: black;"></i></span>
                                        </div>
                                    </div>

                                </div>

                                <div class="form-group row">
                                    <label for="filtro_proveedor" class="col-sm-1 control-label label-form">Proveedor</label>
                                    <div class="col-sm-3">
                                        <select name="filtro_proveedor[]" id="filtro_proveedor" class="form-control select2_filtro" multiple style="width: 100%">
                                            @foreach ( $proveedores as $proveedor )
                                                <option value="{{ $proveedor->id }}" >{{ $proveedor->rut }} {{ $proveedor->nombre }}</option>
                                            @endforeach
                                        </select>
                                    </div>

                                    <label for="filtro_tipo_documento" class="col-sm-1 control-label label-form">Tipo de Documento</label>
                                    <div class="col-sm-3">
                                        <select name="filtro_tipo_documento[]" id="filtro_tipo_documento" class="form-control select2_filtro" multiple style="width: 100%">
                                            @foreach ( $tiposDocumento as $tipoDocumento )
                                                <option value="{{ $tipoDocumento->id }}" >{{ $tipoDocumento->nombre }}</option>
                                            @endforeach
                                        </select>
                                    </div>

                                    <label for="filtro_modalidad_compra" class="col-sm-1 control-label label-form">Modalidad de Compra</label>
                                    <div class="col-sm-3">
                                        <select name="filtro_modalidad_compra[]" id="filtro_modalidad_compra" class="form-control select2_filtro" multiple="multiple">
                                            @foreach ( $modalidadesCompra as $modalidadCompra )
                                                <option value="{{ $modalidadCompra->id }}" >{{ $modalidadCompra->nombre }}</option>
                                            @endforeach
                                        </select>
                                    </div>

                                </div>

                                <div class="form-group row">

                                    <label for="filtro_tipo_adjudicacion" class="col-sm-1 control-label label-form">Tipo de Adjudicación</label>
                                    <div class="col-sm-3">
                                        <select name="filtro_tipo_adjudicacion[]" id="filtro_tipo_adjudicacion" class="form-control select2_filtro" multiple="multiple">
                                            @foreach ( $tiposAdjudicacion as $tipoAdjudicacion )
                                                <option value="{{ $tipoAdjudicacion->id }}" >{{ $tipoAdjudicacion->nombre }}</option>
                                            @endforeach
                                        </select>
                                    </div>

                                    <label for="filtro_tipo_informe" class="col-sm-1 control-label label-form">Tipo de Informe</label>
                                    <div class="col-sm-3">
                                        <select name="filtro_tipo_informe[]" id="filtro_tipo_informe" class="form-control select2_filtro" multiple="multiple">
                                            @foreach ( $tiposInforme as $tipoInforme )
                                                <option value="{{ $tipoInforme->id }}" >{{ $tipoInforme->nombre }}</option>
                                            @endforeach
                                        </select>
                                    </div>

                                    <label for="filtro_responsable" class="col-sm-1 control-label label-form">Responsable</label>
                                    <div class="col-sm-3">
                                        <select name="filtro_responsable[]" id="filtro_responsable" class="form-control select2_filtro" multiple="multiple">
                                            @foreach ( $usuariosResponsables as $responsable )
                                                <option value="{{ $responsable->id }}" >{{ $responsable->name }}</option>
                                            @endforeach
                                        </select>
                                    </div>

                                </div>

                                <div class="form-group row">

                                    <label for="filtro_referente_tecnico" class="col-sm-1 control-label label-form">Referente Técnico</label>
                                    <div class="col-sm-3">
                                        <select name="filtro_referente_tecnico[]" id="filtro_referente_tecnico" class="form-control select2_filtro" multiple="multiple">
                                            @foreach ( $referentesTecnicos as $referenteTecnico )
                                                <option value="{{ $referenteTecnico->id }}" >{{ $referenteTecnico->nombre }} - {{ $referenteTecnico->responsable }}</option>
                                            @endforeach
                                        </select>
                                    </div>

                                    <label for="filtro_digitador" class="col-sm-1 control-label label-form">Digitador</label>
                                    <div class="col-sm-3">
                                        <select name="filtro_digitador[]" id="filtro_digitador" class="form-control select2_filtro" multiple="multiple">
                                            @foreach ( $usuariosDigitadores as $digitador )
                                                <option value="{{ $digitador->id }}" >{{ $digitador->name }}</option>
                                            @endforeach
                                        </select>
                                    </div>

                                    <label for="filtro_item_presupuestario" class="col-sm-1 control-label label-form">Ítem </label>
                                    <div class="col-sm-3">
                                        <select class="form-control select2_filtro " id="filtro_item_presupuestario" name="filtro_item_presupuestario[]" multiple="multiple" >
                                            @foreach ( $itemsPresupuestarios as $item )
                                                <option value="{{ $item->id }}" >{{ $item->codigo() }} {{ $item->clasificador_presupuestario }}</option>
                                            @endforeach
                                        </select>
                                    </div>

                                </div>

                                <div class="form-group row">

                                    <label for="filtro_numero_documento" class="col-sm-1 control-label label-form">N° Documento</label>
                                    <div class="col-sm-3">
                                        <input type="text" class="form-control " id="filtro_numero_documento" name="filtro_numero_documento" >
                                    </div>

                                    <label for="filtro_numero_documento_compra" class="col-sm-1 control-label label-form">N° Documento Compra</label>
                                    <div class="col-sm-3">
                                        <input type="text" class="form-control " id="filtro_numero_documento_compra" name="filtro_numero_documento_compra" >
                                    </div>

                                    <label for="filtro_visto_bueno" class="col-sm-1 control-label label-form">Visto Bueno </label>
                                    <div class="col-sm-3">
                                        <select class="form-control select2_filtro " id="filtro_visto_bueno" name="filtro_visto_bueno">
                                            <option value="" >Puede Seleccionar una Opción</option>
                                            @foreach ( $vistosBuenos as $vistoBueno )
                                                <option value="{{ $vistoBueno->opcion }}" >{{ $vistoBueno->opcion }}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                </div>
                                
                            </form>
                            {{-- <div class="table-toolbar">
                                    
                            </div> --}}
                            <div class="row">
                                <div class="col-md-12">
                                    <div class="panel">
                                        <div class="panel-body">
                                            <div class="table-responsive">
                                                <table class="table table-striped table-bordered table-hover table-full-width " id="tabla_documentos">                                                
                                                </table>
                                            </div>
                                            
                                        </div>
                                    </div>
                                </div>
                            </div>
						</div>
					</div>
					<!-- END EXAMPLE TABLE PORTLET-->
				</div>
			</div>
			<!-- END PAGE CONTENT-->
			
			
			
			
		</div> <!--/div class="page-content" -->
	</div> <!--/div class="page-content-wrapper" -->
</div><!--/div class="page-content-wrapper" -->
<!-- END CONTENT -->
<div id="modal"></div>
<div id="modal2"></div>
@endsection

@push('script-footer')
<!-- BEGIN PAGE LEVEL PLUGINS -->
<script type="text/javascript" src="{{ asset('assets/plugins/select2/select2.min.js') }} "></script>

<script type="text/javascript" src="{{ asset('datatables/jquery.dataTables.min.js') }}"></script>
<script type="text/javascript" src="{{ asset('datatables/fixedColumns.min.js') }}"></script>
<script type="text/javascript" src="{{ asset('assets/plugins/data-tables/DT_bootstrap.js') }}"></script>

<script type="text/javascript" src="{{ asset('js/moment/moment.js') }}"></script>
<script type="text/javascript" src="{{ asset('js/moment/moment-with-locales.js') }}"></script>

<script type="text/javascript" src="{{asset('assets/plugins/bootstrap-datepicker/js/bootstrap-datepicker.js')}}"></script>
<script type="text/javascript" src="{{asset('assets/plugins/bootstrap-datetimepicker/js/bootstrap-datetimepicker.js')}}"></script>
<script type="text/javascript" src="{{ asset('assets/scripts/app.js') }} "></script>
<!-- END PAGE LEVEL PLUGINS -->

<script type="text/javascript">
        
    var tablaPrincipal = $('#tabla_documentos').DataTable({
        "ajax": {
            "url" : "{{ url('documentos/data_table_buscador') }}",
            "type" : "POST",
            "data": function(d){
                d.form = JSON.stringify( $("#filtros-docs").serializeArray() );
            },
        },
        // Set rows IDs
        rowId: function(data) {
            return 'tr_' + data.DT_RowID;
        },
        columns: [
            { title: "<div class='text-center'><i class='fa fa-cog'></i></div>" },
            { title: "Estado", className: "td-bold" },
            { title: "Proveedor" },
            { title: "Tipo Doc." },
            { title: "N° Doc." },
            { title: "N° Documento Compra" },
            { title: "Fecha RD",
                "render": function(data, type) {
                    
                    if ( data != '' ) {
                        moment.locale('es');
                        return type === 'sort' ? data : moment(data).format('L');
                    } else {
                        return data;
                    }
                    
                }
            },
            { title: "Fecha E",
                "render": function(data, type) {
                    moment.locale('es');
                    return type === 'sort' ? data : moment(data).format('L');
                }
            },
            { title: "Ítem" },
            { title: "Tot. Orig.", className: "text-right",
                "render": function(data, type, row, meta) {
                    return type === 'sort' ? data : '$'+formatMoney(data) ;
                }
            },
            { title: "Tot. Act.", className: "text-right",
                "render": function(data, type, row, meta) {
                    return type === 'sort' ? data : '$'+formatMoney(data) ;
                }
            }
        ],
        "deferRender": true,
        "language": {
            "url": "{{ asset('datatables/language.json') }}"
        },
        "lengthMenu": [
            [10, 15, 20, -1],
            [10, 15, 20, "Todos"] // change per page values here
        ],
        // set the initial value
        "displayLength": 10,
        "paging":   true,
        "ordering": true,
        "info":     true,
        "searching": true,
        "orderClasses": true,
        "cache": true,
        // scrollX:        true,
        // fixedColumns: {
        //     leftColumns: 0,
        //     rightColumns: 1,
            
        // }
    });


    $.fn.dataTable.ext.errMode = 'none';//quita los mensajes de error de la datatable ajax

    $('#tabla_documentos').on( 'error.dt', function ( e, settings, techNote, message ) {
		console.log( 'An error has been reported by DataTables: ', message );//para mostrar los mensajes de error en la consola del navegador
	} );

    jQuery('#tabla_documentos_filter input').addClass("form-control input-large"); // modify table search input
    jQuery('#tabla_documentos_wrapper .dataTables_length select').addClass("form-control input-small"); // modify table per page dropdown
    jQuery('#tabla_documentos_wrapper .dataTables_length select').select2(); // initialize select2 dropdown

    $('#tabla_documentos_column_toggler input[type="checkbox"]').change(function() {
        /* Get the DataTables object again - this is not a recreation, just a get of the object */
        var iCol = parseInt($(this).attr("data-column"));
        var bVis = oTable.fnSettings().aoColumns[iCol].bVisible;
        oTable.fnSetColumnVis(iCol, (bVis ? false : true));
    });

    $('#filtroListado').click(function(){
        $("#modalCarga").modal({backdrop: 'static', keyboard: false});
        $('#modalCargaImg').addClass('fa-pulso');
            
        $('#tabla_documentos').DataTable().ajax.reload( function ( json ) {
            $("#modalCarga").modal('toggle');
        });
    });

	$(document).ready(function() {
        
		App.init(); // initlayout and core plugins

        $("#buscador").addClass( "active" );
		$("#buscador-docs-a").append( '<span class="selected"></span>' );

        $('#filtro_fecha_inicio').datepicker({
            format: 'dd/mm/yyyy',
            endDate: new Date(),
            autoclose: true,
            language: 'es'
        });

        $('#filtro_fecha_termino').datepicker({
            format: 'dd/mm/yyyy',          
            autoclose: true,
            language: 'es'
        });

        $(".select2_filtro").select2({
            // allowClear: true,
        });
        
	});

    function fechaDocumento(id_fecha) {
        $('#' + id_fecha).datepicker("show");
    }

    function ver(id)
    {
        $.get( '{{ url("documentos/modal/ver") }}/' + id, function( data ) {
            $( "#modal" ).html( data );
            $( "#modalVer" ).modal();
        });
    }

    function traza(id, eliminado = false)
    {
        if ( eliminado == false ) {
            $.get( '{{ url("documentos/modal/traza") }}/' + id, function( data ) {
                $( "#modal" ).html( data );
                $( "#modalTraza" ).modal();
            });
        } else {
            $.get( '{{ url("documentos/modal/traza") }}/' + id + '/' + eliminado, function( data ) {
                $( "#modal" ).html( data );
                $( "#modalTraza" ).modal();
            });
        }
        
    }

    function imprimirArchivos(id, eliminado = false)
    {

        $.get( '{{ url("documentos/modal/imprimir_documento") }}/' + id , function( data ) {
            $( "#modal" ).html( data );
            $( "#modalImprimirDocumento" ).modal();
        });
    
    }

    function trazaAcepta(id) {
        $.get( '{{ url("archivos_acepta/modal/traza") }}/' + id , function( data ) {
            $( "#modal" ).html( data );
            $( "#modalTraza" ).modal();
        });
    }

    // function editar(id)
    // {

    //     // $("#modalCarga").modal({keyboard: false});
    //     // $('#modalCargaImg').addClass('fa-pulso');
    //     $('.page-header-fixed *').css('cursor', 'wait');
    //     $.ajax({
    //         type: "GET",
    //         url: '{{ url("documentos/modal/editar") }}/' + id +'/ingresados',
    //         success: function(respuesta) {

    //             $( "#modal" ).html( respuesta );
    //             $('.page-header-fixed *').css('cursor', '');
    //             $( "#modalEditar" ).modal();

    //             // $("#modalCarga").modal('hide');

    //             // $('#modalCarga').on('hidden.bs.modal', function () {
    //             //     // Load up a new modal...
    //             //     // $('#myModalNew').modal('show')
    //             //     $( "#modal" ).html( respuesta );
    //             //     $( "#modalEditar" ).modal();
    //             // });
               
    //         }
    //     });
    // }

</script>

@endpush