<link href="{{ asset('sistema/css/modales.css') }}?v={{rand()}}" rel="stylesheet" type="text/css"/>

<div class="modal fade" id="modalEditar" role="dialog" aria-labelledby="myModalLabel3" aria-hidden="true">
	<div class="modal-dialog" style="width:95%;">
		<div class="modal-content">
            <div class="modal-header">
				<button type="button" title="Cerrar" class="close" data-dismiss="modal" aria-hidden="true"></button>
                @if ($datatable == 'pendientes_validacion')
				    <h4 class="modal-title" ><strong><i class="fas fa-thumbs-up"></i>&nbsp;&nbsp;Validar Documento</strong></h4>
                @elseif ($datatable == 'validados')
                    <h4 class="modal-title" ><strong><i class="fas fa-thumbs-down"></i>&nbsp;&nbsp;Invalidar Documento</strong></h4>
                @else
                    <h4 class="modal-title" ><strong><i class="fa fa-files-o fa-lg"></i>&nbsp;&nbsp;Editar Documento</strong></h4>
                @endif
            </div>

            <form action="{{ asset('documentos/editar') }}" method="post" class="horizontal-form" id="form-documentos" autocomplete="off">
                <div class="modal-body">
                
                    <div class="form-body">
                        <div class="note note-danger" id="divErrores" style="display:none;">
                            <h4 class="block" style="margin-bottom:5 px;">Debe Completar los siguientes campos: </h4>
                            <ul id="ulErrores"></ul>
                        </div>

                        @if ( $documento->reclamado == 1 )
                        <div class="form-group row">
                            <span style="font-size: 14px;margin-left: 10px;font-weight:bold;background-color: #d42700;" class="label label-danger" >Documento Reclamado</span>
                        </div>
                        @endif

                        <h4 class="form-section" style="color: #69aa46;margin-top: 0px;">Proveedor</h4>
                        <div class="form-group row">
                            <label for="nombre_proveedor" class="col-sm-3 control-label label-form">Rut y Nombre Proveedor <span class="span-label">*</span></label>
                            <div class="col-sm-8 form-validate">
                                <input type="text" class="form-control" id="nombre_proveedor" name="nombre_proveedor" required placeholder="Buscador por Nombre y RUT" onchange="getProveedor(this);">
                            </div>
                        </div>                        

                        <div class="form-group row" >
                            <div class="col-xs-8 col-sm-8 col-md-8 col-lg-8">
                                <h4 class="form-section" style="color: #69aa46;">Documento</h4>

                                <div class="form-group row">
                                    <div class="col-xs-4 col-sm-4 col-md-4 col-lg-4 form-validate">
                                        <label for="tipo_documento" class="label-form">Tipo Documento <span class="span-label">*</span></label>
                                    
                                        <select name="tipo_documento" id="tipo_documento" class="form-control select2" required onchange="setReemplaza(this);">
                                        <option value=''>Seleccione</option>
                                        @foreach ( $tiposDocumento as $tipoDocumento)
                                            <option value="{{ $tipoDocumento->id }}">{{ $tipoDocumento->nombre }}</option>
                                        @endforeach
                                        </select>
                                    </div>
                                    
                                </div>

                                <div class="form-group row">

                                    <div id="div_relacionado" class="col-xs-6 col-sm-6 col-md-6 col-lg-6 form-validate" >
                                        @if ( $documento->id_tipo_documento == 1 )
                                            <label for="reemplaza" class="label-form"> Reemplaza a </label>

                                            <select name="reemplaza" id="reemplaza" class="form-control select2" onchange="setDocumentoReemplazaFactura(this);">
                                                <option value="">Seleccione</option>
                                                @if ( $documento->getDocumentoRelacionado != null )
                                                    <option id="docProv_{{ $documento->getDocumentoRelacionado->id }}" value="{{ $documento->getDocumentoRelacionado->id }}" selected
                                                            data-datosdocumento="{!! htmlspecialchars( json_encode( $documento->getDocumentoRelacionado->getDatosDocumentosProveedores() ), ENT_QUOTES, 'UTF-8') !!}" >
                                                        {{ $documento->getDocumentoRelacionado->getTipoDocumento->sigla }} N° {{ $documento->getDocumentoRelacionado->numero_documento }} - {{ fecha_dmY($documento->getDocumentoRelacionado->fecha_documento) }} - ${{ formatoMiles($documento->getDocumentoRelacionado->total_documento) }}
                                                    </option>
                                                @endif
                                                @forelse ($documentosProveedor as $documentoProveedor)
                                                    <option id="docProv_{{ $documentoProveedor->id }}" value="{{ $documentoProveedor->id }}"
                                                            data-datosdocumento="{!! htmlspecialchars( json_encode( $documentoProveedor->getDatosDocumentosProveedores() ), ENT_QUOTES, 'UTF-8') !!}" >
                                                        {{ $documentoProveedor->getTipoDocumento->sigla }} N° {{ $documentoProveedor->numero_documento }} - {{ fecha_dmY($documentoProveedor->fecha_documento) }} - ${{ formatoMiles($documentoProveedor->total_documento) }}
                                                    </option>
                                                @empty
                                                @endforelse
                                                </select>
                                            </select>

                                        @elseif ( $documento->id_tipo_documento == 4 || $documento->id_tipo_documento == 5 || $documento->id_tipo_documento == 10 || $documento->id_tipo_documento == 11 )

                                            <label for="reemplaza" class="label-form"> Factura Asociada <span class="span-label">*</span></label>
                                            <select name="reemplaza" id="reemplaza" class="form-control select2" required onchange="setDocumentoNcNd(this);">
                                                <option value="">Seleccione</option>
                                                @if ( $documento->getDocumentoRelacionado != null )
                                                    <option id="docProv_{{ $documento->getDocumentoRelacionado->id }}" value="{{ $documento->getDocumentoRelacionado->id }}" selected 
                                                            data-datosdocumento="{!! htmlspecialchars( json_encode( $documento->getDocumentoRelacionado->getDatosDocumentosProveedores() ), ENT_QUOTES, 'UTF-8') !!}" >
                                                        {{ $documento->getDocumentoRelacionado->getTipoDocumento->sigla }} N° {{ $documento->getDocumentoRelacionado->numero_documento }} - {{ fecha_dmY($documento->getDocumentoRelacionado->fecha_documento) }} - ${{ formatoMiles($documento->getDocumentoRelacionado->total_documento) }}
                                                    </option>
                                                @endif

                                                @forelse( $documentosProveedor as $documentoProveedor )
                                                    <option id="docProv_{{ $documentoProveedor->id }}" value="{{ $documentoProveedor->id }}" 
                                                            data-datosdocumento="{!! htmlspecialchars( json_encode( $documentoProveedor->getDatosDocumentosProveedores() ), ENT_QUOTES, 'UTF-8') !!}" >
                                                        {{ $documentoProveedor->getTipoDocumento->sigla }} N° {{ $documentoProveedor->numero_documento }} - {{ fecha_dmY($documentoProveedor->fecha_documento) }} - ${{ formatoMiles($documentoProveedor->total_documento) }}
                                                    </option>
                                                @empty
                                                @endforelse

                                            </select>

                                        @elseif ( $documento->id_tipo_documento == 8 )

                                            <label for="reemplaza" class="label-form"> Factura Refacturada <span class="span-label">*</span></label>
                                            <select name="reemplaza" id="reemplaza" class="form-control select2" required>
                                                <option value="">Seleccione</option>
                                                @if ( $documento->getDocumentoRelacionado != null )
                                                    <option value="{{ $documento->getDocumentoRelacionado->id }}" selected>
                                                        {{ $documento->getDocumentoRelacionado->getTipoDocumento->sigla }} N° {{ $documento->getDocumentoRelacionado->numero_documento }} - {{ fecha_dmY($documento->getDocumentoRelacionado->fecha_documento) }} - ${{ formatoMiles($documento->getDocumentoRelacionado->total_documento) }}
                                                    </option>
                                                @endif

                                                @forelse( $documentosProveedor as $documentoProveedor )
                                                    <option value="{{ $documentoProveedor->id }}" >
                                                        {{ $documentoProveedor->getTipoDocumento->sigla }} N° {{ $documentoProveedor->numero_documento }} - {{ fecha_dmY($documentoProveedor->fecha_documento) }} - ${{ formatoMiles($documentoProveedor->total_documento) }}
                                                    </option>
                                                @empty
                                                @endforelse
                                                
                                            </select>

                                        @endif
                                    </div>

                                    <div class="col-xs-6 col-sm-6 col-md-6 col-lg-6 form-validate" id="div_rechazados_acepta">

                                        @if ( $documento->id_tipo_documento == 1 )

                                            <label for="reemplaza_archivo_acepta" class="label-form">Reemplaza al Archivo Acepta</label>
                                            <select name="reemplaza_archivo_acepta" id="reemplaza_archivo_acepta" class="form-control select2" onchange="setDocumentoReemplazaRechazado(this);">
                                                <option value="">Seleccione</option>
                                                @if ( $documento->getArchivoAceptaRefacturado )
                                                    <option id="docRechazadoProv_{{ $documento->getArchivoAceptaRefacturado->id }}" value="{{ $documento->getArchivoAceptaRefacturado->id }}" selected
                                                        data-datosdocumento="{!! htmlspecialchars(json_encode($documento->getArchivoAceptaRefacturado->getDatosDocumentosProveedores()), ENT_QUOTES, 'UTF-8') !!}" >
                                                        {{ $documento->getArchivoAceptaRefacturado->getTipoDocumento->sigla }} N° {{ $documento->getArchivoAceptaRefacturado->folio }} - {{ fecha_dmY($documento->getArchivoAceptaRefacturado->emision) }} - ${{ formatoMiles($documento->getArchivoAceptaRefacturado->monto_total) }}
                                                    </option>
                                                @endif
                                                @forelse ($docsRechazadosAceptaProv as $rechazado)
                                                    <option id="docRechazadoProv_{{ $rechazado->id }}" value="{{ $rechazado->id }}" data-datosdocumento="{!! htmlspecialchars(json_encode($rechazado->getDatosDocumentosProveedores()), ENT_QUOTES, 'UTF-8') !!}" >
                                                        {{ $rechazado->getTipoDocumento->sigla }} N° {{ $rechazado->folio }} - {{ fecha_dmY($rechazado->emision) }} - ${{ formatoMiles($rechazado->monto_total) }}
                                                    </option>
                                                @empty
                                                @endforelse
                                            </select>
                                            <span class="help-block">
                                                Estos Archivos estan rechazados
                                            </span>

                                        @endif

                                    </div>
                                    
                                </div>

                                <div class="form-group row">
                                    <div class="col-xs-3 col-sm-3 col-md-3 col-lg-3 form-validate">
                                        <label for="numero_documento" class="label-form">N° Documento <span class="span-label">*</span></label>
                                        <input type="text" class="form-control numero_documento" id="numero_documento" name="numero_documento" required value="{{ $documento->numero_documento }}" 
                                            @if($documento->getUnaRelacionRecepcionValidada && !$documento->getRelacionGuiasDespacho()) readonly @endif
                                            @if($documento->getUnaRelacionRecepcionNoValidada && !$documento->getRelacionGuiasDespachoHermes()) readonly @endif
                                            >
                                    </div>
                                    
                                    <div class="col-xs-3 col-sm-3 col-md-3 col-lg-3 form-validate">
                                    <label for="fecha_documento" class="label-form">Fecha Documento <span class="span-label">*</span></label>
                                        <div class="input-group">
                                            <input type="text" class="form-control fechas" id="fecha_documento" name="fecha_documento" required value="{{ fecha_dmY($documento->fecha_documento) }}" onchange="cambioFechaDocumento(this);" >
                                            <span class="input-group-addon" onclick="fechaDocumento('fecha_documento');" style="cursor:pointer;"><i class="fa fa-calendar" aria-hidden="true" style="color: black;"></i></span>
                                        </div>
                                    </div>

                                    <div class="col-xs-3 col-sm-3 col-md-3 col-lg-3 form-validate">
                                        <label for="fecha_recepcion" class="label-form">Fecha Recepción  <span class="span-label">*</span></label>                                        
                                        <div class="input-group">
                                            <input type="text" class="form-control fechas" id="fecha_recepcion" name="fecha_recepcion" required value="{{ fecha_dmY($documento->fecha_recepcion) }}" onchange="cambioFechaRecepcion(this);">
                                            <span class="input-group-addon " onclick="fechaDocumento('fecha_recepcion');" style="cursor:pointer;"><i class="fa fa-calendar" aria-hidden="true" style="color: black;"></i></span>
                                        </div>
                                    </div>

                                    <div class="col-xs-3 col-sm-3 col-md-3 col-lg-3 form-validate" >
                                        <label for="periodo" class="label-form">Periodo  <span class="span-label">*</span></label>
                                        <div class="input-group" id="div_periodo">
                                            <input type="text" class="form-control fechas" id="periodo" name="periodo" required  @if( $documento->periodo != null ) value="{{ fecha_mY($documento->periodo) }}" @endif >
                                            <span class="input-group-addon " onclick="fechaDocumento('periodo');" style="cursor:pointer;"><i class="fa fa-calendar" aria-hidden="true" style="color: black;"></i></span>

                                        </div>
                                    </div>
                                </div>

                                <div class="form-group row">
                                    <div class="col-xs-4 col-sm-4 col-md-4 col-lg-4 form-validate">
                                        <label for="valor_total_documento" class="label-form">Valor Total <span class="span-label">*</span></label>
                                        <div class="input-group">
                                            <span class="input-group-addon">$</span>
                                            <input type="text" class="form-control numero_documento" id="valor_total_documento" name="valor_total_documento" required onkeyup="ingresoPesos(this);" autocomplete="off" 
                                                    value="{{ formatoMiles($documento->total_documento) }}" 
                                                    @if($documento->getUnaRelacionRecepcionValidada && !$documento->getRelacionGuiasDespacho()) readonly @endif
                                                    @if($documento->getUnaRelacionRecepcionNoValidada && !$documento->getRelacionGuiasDespachoHermes()) readonly @endif
                                                    @if( $documento->getContratoOrdenCompra && $documento->getContratoOrdenCompra->getContrato &&
                                                        ( $documento->getContratoOrdenCompra->getContrato->obtenerValorHoraEdicionDocumento() != 'Por procedimiento' && 
                                                            $documento->getContratoOrdenCompra->getContrato->obtenerValorHoraEdicionDocumento() != '0') ) readonly @endif>
                                        </div>
                                    </div>

                                    <div id="div_valores_boleta" @if( $documento->id_tipo_documento != 3 ) style="display:none" @endif>
                                        <div class="col-xs-4 col-sm-4 col-md-4 col-lg-4 form-validate">
                                            <label for="impuesto" id="label_impuesto" class="label-form">{{ $documento->getLabelImpuesto() }} Impuesto </label>
                                            <div class="input-group">
                                                <span class="input-group-addon">$</span>
                                            <input type="text" class="form-control numero_documento" id="impuesto" name="impuesto" readonly autocomplete="off" value="{{ formatoMiles($documento->impuesto) }}" >
                                            </div>
                                        </div>
                    
                                        <div class="col-xs-4 col-sm-4 col-md-4 col-lg-4 form-validate">
                                            <label for="liquido" class="label-form">Líquido </label>
                                            <div class="input-group">
                                                <span class="input-group-addon">$</span>
                                                <input type="text" class="form-control numero_documento" id="liquido" name="liquido" readonly autocomplete="off" value="{{ formatoMiles($documento->liquido) }}" >
                                            </div>
                                        </div>
                                    </div>
                                </div>

                                <div class="form-group row">
                                    <div class="col-xs-4 col-sm-4 col-md-4 col-lg-4 form-validate">
                                        <label for="modalidad_compra" class="label-form">Modalidad de Compra <span class="span-label">*</span></label>
                                        <select name="modalidad_compra" id="modalidad_compra" class="form-control select2" required onchange="setOrdenCompra(this);">
                                            <option value=''>Seleccione</option>
                                            @foreach ( $modalidadesCompra as $modalidadCompra)
                                                <option value="{{ $modalidadCompra->id }}">{{ $modalidadCompra->nombre }}</option>
                                            @endforeach
                                        </select>
                                    </div>

                                    <div class="col-xs-4 col-sm-4 col-md-4 col-lg-4 form-validate" id="div_orden_compra">
                                        <label for="numero_orden_compra" class="control-label label-form">N° Orden de Compra <span class="span-label">*</span></label>
                                        <input type="text" class="form-control" id="numero_orden_compra" name="numero_orden_compra" required value="{{ $documento->documento_compra }}" @if($documento->getModalidadCompra->predeterminado != null) readonly @endif>
                                    </div>

                                    <div class="col-xs-4 col-sm-4 col-md-4 col-lg-4 form-validate">
                                        <label for="numero_licitacion" class="label-form">N° Licitación </label>
                                        <input type="text" class="form-control" id="numero_licitacion" name="numero_licitacion" value="{{ $documento->licitacion }}" >
                                    </div>
                                </div>

                                <div class="form-group row">

                                    <div class="col-xs-4 col-sm-4 col-md-4 col-lg-4 form-validate">
                                        <label for="tipo_informe" class="label-form">Tipo de Informe <span class="span-label">*</span></label>
                                        <select name="tipo_informe" id="tipo_informe" class="form-control select2" required>
                                            <option value=''>Seleccione</option>
                                            @foreach ( $tiposInforme as $tipoInforme)
                                                <option value="{{ $tipoInforme->id }}">{{ $tipoInforme->nombre }}</option>
                                            @endforeach
                                        </select>
                                    </div>

                                    <div class="col-xs-4 col-sm-4 col-md-4 col-lg-4 form-validate">
                                        <label for="tipo_adjudicacion" class="label-form">Tipo Adjudicación <span class="span-label">*</span></label>
                                        <select name="tipo_adjudicacion" id="tipo_adjudicacion" class="form-control select2" required>
                                        <option value=''>Seleccione</option>
                                        @foreach ( $tiposAdjudicacion as $tipoAdjudicacion)
                                            <option value="{{ $tipoAdjudicacion->id }}">{{ $tipoAdjudicacion->nombre }}</option>
                                        @endforeach
                                        </select>
                                    </div>

                                </div>

                                <div class="form-group row">

                                    <div class="col-xs-8 col-sm-8 col-md-8 col-lg-8 form-validate">
                                        <label for="referente_tecnico" class="label-form">Referente Técnico </label>
                                        <select name="referente_tecnico" id="referente_tecnico" class="form-control select2" >
                                        <option value=''>Seleccione</option>
                                        @foreach ( $referentesTecnicos as $referenteTecnico)
                                            <option value="{{ $referenteTecnico->id }}">{{ $referenteTecnico->nombre }} - {{ $referenteTecnico->responsable }}</option>
                                        @endforeach
                                        </select>
                                    </div>

                                </div>

                                @if ( $documento->getDocumentosRelacionados->count() > 0 || $documento->id_relacionado != null || $documento->getArchivoAceptaRefacturado )
                                    <div class="form-group row">
                                        <h4 class="form-section" style="color: #69aa46;">Documentos Relacionados</h4>
                                        <div class="col-xs-8 col-sm-8 col-md-8 col-lg-8 table-responsive">
                                            <table class="table table-striped table-bordered table-hover " id="tabla_info_archivo">
                                                <thead>
                                                    <tr>
                                                        <th class="text-center">N° Doc.</th>
                                                        <th >Tipo Doc.</th>
                                                        <th class="text-right">Valor Total Doc.</th>
                                                    </tr>
                                                </thead>
                                                <tbody>
                                                    @forelse($documento->getDocumentosRelacionados as $doc)
                                                        <tr>
                                                            <td class="text-center">{{ $doc->numero_documento }}</td>
                                                            <td>
                                                                {{ $doc->getTipoDocumento->nombre }}
                                                                @if ( $doc->getArchivos->count() > 0 )
                                                                    <div class="btn-group pull-right">
                                                                        @foreach($doc->getArchivos as $archivo)
                                                                            <a class="btn btn-info btn-xs " 
                                                                                @if($archivo->cargado == 0) 
                                                                                    href="{{ asset( $archivo->ubicacion ) }}" 
                                                                                @else
                                                                                    href="http://{{ $archivo->ubicacion }}"
                                                                                @endif
                                                                                title="Ver Archivo @if( $archivo->getTipoArchivo ){{ $archivo->getTipoArchivo->nombre }}@endif" 
                                                                                target="_blank" ><i class="fa fa-eye"></i>
                                                                            </a>
                                                                        @endforeach
                                                                    </div>
                                                                @endif
                                                            </td>
                                                            <td class="text-right">$ {{ formatoMiles($doc->total_documento) }}</td>
                                                        </tr>
                                                    @empty
                                    
                                                    @endforelse

                                                    @if ( $documento->id_relacionado != null )
                                                        <tr>
                                                            <td class="text-center">{{ $documento->getDocumentoRelacionado->numero_documento }}</td>
                                                            <td>
                                                                {{ $documento->getDocumentoRelacionado->getTipoDocumento->nombre }}
                                                                @if ( $documento->getDocumentoRelacionado->getArchivos->count() > 0 )
                                                                    <div class="btn-group pull-right">
                                                                        @foreach($documento->getDocumentoRelacionado->getArchivos as $archivo)
                                                                            <a class="btn btn-info btn-xs " 
                                                                                @if($archivo->cargado == 0) 
                                                                                    href="{{ asset( $archivo->ubicacion ) }}" 
                                                                                @else
                                                                                    href="http://{{ $archivo->ubicacion }}"
                                                                                @endif
                                                                                title="Ver Archivo @if( $archivo->getTipoArchivo ){{ $archivo->getTipoArchivo->nombre }}@endif" 
                                                                                target="_blank" ><i class="fa fa-eye"></i>
                                                                            </a>
                                                                        @endforeach
                                                                    </div>
                                                                @endif
                                                            </td>
                                                            <td class="text-right">$ {{ formatoMiles($documento->getDocumentoRelacionado->total_documento) }}</td>
                                                        </tr>
                                                    @endif
    
                                                    @if ( $documento->getArchivoAceptaRefacturado )
                                                        <tr>
                                                            <td class="text-center">{{ $documento->getArchivoAceptaRefacturado->folio }}</td>
                                                            <td>
                                                                {{ $documento->getArchivoAceptaRefacturado->getTipoDocumento->nombre }} (Rechazado Acepta)
                                                                @if ( $documento->getArchivoAceptaRefacturado->uri != null )
                                                                    <div class="btn-group pull-right">
                                                                        <a class="btn btn-info btn-xs" href="http://windte1806.acepta.com/ca4webv3/PdfViewMedia?url={{ $documento->getArchivoAceptaRefacturado->uri }}&menuTitle=Papel%2520Carta"
                                                                                title="Ver Factura" target="_blank" ><i class="fa fa-eye"></i></a>
                                                                    </div>
                                                                @endif
                                                            </td>
                                                            <td class="text-right">$ {{ formatoMiles($documento->getArchivoAceptaRefacturado->monto_total) }}</td>
                                                        </tr>
                                                    @endif
                                                        @if ( $documento->montoAplicadoAlValorTotal() != 0 )
                                                        <tr>
                                                            <td colspan="2" class="text-right"><strong>Sumatoria de NC, ND, CC y CD aplicada al Valor Total</strong></td>
                                                            <td class="text-right">
                                                                <input type="text" class="numero_documento input_oculto" id="monto_aplicado" name="monto_aplicado"
                                                                    readonly value="{{ formatoMiles($documento->montoAplicadoAlValorTotal()) }}">
                                                            </td>
                                                        </tr> 
                                                    @endif                                           
                                                </tbody>
                                            </table>
                                        </div>

                                        <div class="col-xs-4 col-sm-4 col-md-4 col-lg-4 form-validate">
                                            <label for="valor_total_documento_actualizado" class="label-form">Valor Total Actualizado <span class="span-label">*</span></label>
                                            <div class="input-group">
                                                <span class="input-group-addon">$</span>
                                                <input type="text" class="form-control numero_documento" id="valor_total_documento_actualizado" 
                                                    name="valor_total_documento_actualizado" required readonly
                                                    value="{{ formatoMiles($documento->total_documento_actualizado) }}" >
                                            </div>
                                            <span class="help-block">
                                                    Respecto al valor total.
                                            </span>
                                        </div>
                                    </div>
                                @endif


                            </div> <!-- / div class col-xs-6 -->

                            <div class="col-xs-4 col-sm-4 col-md-4 col-lg-4">
                                <h4 class="form-section" style="color: #69aa46;">Información Proveedor</h4>
                                <div id="informacion_proveedor">

                                    <div class="form-group row">
                                        <div class="col-xs-4 col-sm-4 col-md-4 col-lg-4 form-validate" id="setear_select_contrato">
                                            <label for="contrato" class="label-form">Contrato<span class="span-label">*</span></label>
                                            <select name="contrato" id="contrato" class="form-control select2" onchange="getDatosContrato(this);" >
                                                <option value=''>Seleccione</option>
                                                @if( isset($optionsContrato) )
                                                    {!! $optionsContrato !!}
                                                @endif
                                            </select>
                                        </div>

                                        <div class="ocultar_segun_contrato"
                                            @if( $documento->getContratoOrdenCompra ) 
                                                @if( ! $documento->getContratoOrdenCompra->getContrato ) 
                                                    style="display:none;"
                                                @endif 
                                            @else
                                                style="display:none;"
                                            @endif >
                                            <div class="col-xs-3 col-sm-3 col-md-3 col-lg-3 form-validate">
                                                <label for="inicio_vigencia_contrato" class="label-form">Inicio Vigencia</label>
                                                <input type="text" class="form-control" id="inicio_vigencia_contrato" name="inicio_vigencia_contrato" readonly 
                                                        @if( $documento->getContratoOrdenCompra && $documento->getContratoOrdenCompra->getContrato )
                                                            value="{{ fecha_dmY($documento->getContratoOrdenCompra->getContrato->inicio_vigencia) }}" 
                                                        @endif >
                                            </div>

                                            <div class="col-xs-3 col-sm-3 col-md-3 col-lg-3 form-validate">
                                                <label for="fin_vigencia_contrato" class="label-form">Fin Vigencia</label>
                                                <input type="text" class="form-control" id="fin_vigencia_contrato" name="fin_vigencia_contrato" readonly 
                                                        @if( $documento->getContratoOrdenCompra && $documento->getContratoOrdenCompra->getContrato )
                                                            value="{{ fecha_dmY($documento->getContratoOrdenCompra->getContrato->fin_vigencia) }}" 
                                                        @endif >
                                            </div>

                                        </div>
                                    </div>
                
                                    <div class="ocultar_segun_contrato"
                                        @if( $documento->getContratoOrdenCompra ) 
                                            @if( ! $documento->getContratoOrdenCompra->getContrato ) 
                                                style="display:none;"
                                            @endif 
                                        @else
                                            style="display:none;"
                                        @endif >
                
                                        <div class="form-group row">
                                            <div class="col-xs-4 col-sm-4 col-md-4 col-lg-4form-validate">
                                                <label for="monto_contrato" class="label-form">Monto</label>
                                                <div class="input-group">
                                                    <span class="input-group-addon">$</span>
                                                    <input type="text" class="form-control" id="monto_contrato" name="monto_contrato" readonly 
                                                            @if( $documento->getContratoOrdenCompra && $documento->getContratoOrdenCompra->getContrato )
                                                                value="{{ formatoMiles($documento->getContratoOrdenCompra->getContrato->monto_contrato) }}" 
                                                            @endif >
                                                </div>
                                            </div>

                                            <div class="col-xs-4 col-sm-4 col-md-4 col-lg-4 form-validate">
                                                <label for="saldo_contrato" class="label-form">Saldo Contrato</label>
                                                <div class="input-group">
                                                    <span class="input-group-addon">$</span>
                                                    <input type="text" class="form-control" id="saldo_contrato" name="saldo_contrato" readonly 
                                                            @if( $documento->getContratoOrdenCompra && $documento->getContratoOrdenCompra->getContrato )
                                                                value="{{ formatoMiles($documento->getContratoOrdenCompra->getContrato->saldo_contrato) }}" 
                                                            @endif >
                                                </div>
                                            </div>

                                            <div class="col-xs-4 col-sm-4 col-md-4 col-lg-4 form-validate">
                                                <label for="valor_hora_contrato" class="label-form">Valor Hora</label>
                                                <div class="input-group">
                                                    <span class="input-group-addon">$</span>
                                                    <input type="text" class="form-control" id="valor_hora_contrato" name="valor_hora_contrato" readonly 
                                                            @if( $documento->getContratoOrdenCompra && $documento->getContratoOrdenCompra->getContrato )
                                                                value="{{ $documento->getContratoOrdenCompra->getContrato->obtenerValorHoraEdicionDocumento() }}" 
                                                            @endif >
                                                </div>
                                            </div>

                                        </div>
                
                                        <div class="form-group row">
                                            
                                            <div id="ocultar_segun_valor_hora" class="col-xs-4 col-sm-4 col-md-4 col-lg-4 form-validate"
                                                @if( $documento->getContratoOrdenCompra)
                                                    @if( $documento->getContratoOrdenCompra->getContrato )
                                                        @if( $documento->getContratoOrdenCompra->getContrato->obtenerValorHoraEdicionDocumento() == 'Por procedimiento' || $documento->getContratoOrdenCompra->getContrato->obtenerValorHoraEdicionDocumento() == '0' )
                                                            style="display:none;"
                                                        @endif
                                                    @else
                                                        style="display:none;"
                                                    @endif
                                                @else
                                                    style="display:none;"
                                                @endif
                                                        >
                                                    <label for="horas_boleta" class="label-form">Horas <span class="span-label">*</span></label>
                                                    <input type="text" class="form-control numero_documento" id="horas_boleta" name="horas_boleta" required onkeyup="ingresoHoras(this);" autocomplete="off" 
                                                        @if( $documento->getContratoOrdenCompra && $documento->getContratoOrdenCompra->getContrato &&
                                                             $documento->getContratoOrdenCompra->getContrato->obtenerValorHoraEdicionDocumento() != 'Por procedimiento' && 
                                                             $documento->getContratoOrdenCompra->getContrato->obtenerValorHoraEdicionDocumento() != '0' )
                                                            value="{{ formatoMilesDecimal($documento->getContratoOrdenCompra->horas_boleta) }}"
                                                        @endif >
                                            </div>

                                        </div>
                
                                        <div id="ocultar_segun_saldos_preventivos_correctivos"
                                            @if( $documento->getContratoOrdenCompra)
                                                @if( $documento->getContratoOrdenCompra->getContrato )
                                                    @if( ! $documento->getContratoOrdenCompra->getContrato->tieneSaldosPreventivoCorrectivo() )
                                                        style="display:none;"
                                                    @endif
                                                @else
                                                    style="display:none;"
                                                @endif
                                            @else 
                                                style="display:none;"
                                            @endif
                                                >

                                            <div class="form-group row">
                                                <div class="col-xs-6 col-sm-6 col-md-6 col-lg-6 form-validate">
                                                    <label for="monto_preventivo" class="label-form">Monto Preventivo</label>
                                                    <div class="input-group">
                                                        <span class="input-group-addon">$</span>
                                                        <input type="text" class="form-control" id="monto_preventivo" name="monto_preventivo" readonly 
                                                            @if( $documento->getContratoOrdenCompra && $documento->getContratoOrdenCompra->getContrato && $documento->getContratoOrdenCompra->getContrato->tieneSaldosPreventivoCorrectivo() )
                                                                value="{{ formatoMiles($documento->getContratoOrdenCompra->getContrato->monto_preventivo) }}" 
                                                            @endif>
                                                    </div>
                                                </div>
                                                
                                                <div class="col-xs-6 col-sm-6 col-md-6 col-lg-6 form-validate">
                                                    <label for="monto_correctivo" class="label-form">Monto Correctivo</label>
                                                    <div class="input-group">
                                                        <span class="input-group-addon">$</span>
                                                        <input type="text" class="form-control" id="monto_correctivo" name="monto_correctivo" readonly 
                                                            @if( $documento->getContratoOrdenCompra && $documento->getContratoOrdenCompra->getContrato && $documento->getContratoOrdenCompra->getContrato->tieneSaldosPreventivoCorrectivo() )
                                                                value="{{ formatoMiles($documento->getContratoOrdenCompra->getContrato->monto_correctivo) }}" 
                                                            @endif>
                                                    </div>
                                                </div>
                                            </div>
                
                                            <div class="form-group row" >
                                                <div class="col-xs-6 col-sm-6 col-md-6 col-lg-6 form-validate">
                                                    <label for="saldo_preventivo" class="label-form">Saldo Preventivo</label>
                                                    <div class="input-group">
                                                        <span class="input-group-addon">$</span>
                                                        <input type="text" class="form-control" id="saldo_preventivo" name="saldo_preventivo" readonly 
                                                            @if( $documento->getContratoOrdenCompra && $documento->getContratoOrdenCompra->getContrato && $documento->getContratoOrdenCompra->getContrato->tieneSaldosPreventivoCorrectivo() )
                                                                value="{{ formatoMiles($documento->getContratoOrdenCompra->getContrato->saldo_preventivo) }}" 
                                                            @endif>
                                                    </div>
                                                </div>

                                                <div class="col-xs-6 col-sm-6 col-md-6 col-lg-6 form-validate">
                                                    <label for="saldo_correctivo" class="label-form">Saldo Correctivo</label>
                                                    <div class="input-group">
                                                        <span class="input-group-addon">$</span>
                                                        <input type="text" class="form-control" id="saldo_correctivo" name="saldo_correctivo" readonly 
                                                            @if( $documento->getContratoOrdenCompra && $documento->getContratoOrdenCompra->getContrato && $documento->getContratoOrdenCompra->getContrato->tieneSaldosPreventivoCorrectivo() )
                                                                value="{{ formatoMiles($documento->getContratoOrdenCompra->getContrato->saldo_correctivo) }}" 
                                                            @endif>
                                                    </div>
                                                </div>
                                            </div>
                
                                            <div class="form-group row">

                                                <label class="col-md-3 label-form" >Saldo a ocupar</label>
                                                <div class="col-md-9 form-validate" style="margin-top: -8px;">
                                                    <div class="radio-list">
                
                                                        <label class="radio-inline">
                                                            <div class="radio" >
                                                                <span>
                                                                    <input type="radio" name="montoOcupar" id="ocuparPreventivo" value="preventivo" onchange="montoOcuparPreventivoCorrectivo();" 
                                                                    @if( $documento->getContratoOrdenCompra && $documento->getContratoOrdenCompra->ocupa_preventivo == 1 )
                                                                        checked="true"
                                                                    @endif >
                                                                </span>
                                                            </div>Preventivo</label>
                
                                                        <label class="radio-inline">
                                                            <div class="radio" >
                                                                <span>
                                                                    <input type="radio" name="montoOcupar" id="ocuparCorrectivo" value="correctivo" onchange="montoOcuparPreventivoCorrectivo();" 
                                                                    @if( $documento->getContratoOrdenCompra && $documento->getContratoOrdenCompra->ocupa_correctivo == 1 )
                                                                        checked="true"
                                                                    @endif >
                                                                </span>
                                                            </div>Correctivo</label>
                
                                                    </div>
                                                </div>

                                            </div>
                
                                        </div> <!-- /ocultar_segun_saldos_preventivos_correctivos -->
                
                                    </div>
                                </div> <!-- /informacion_proveedor -->
                
                                <div id="informacion_orden_compra">
                
                                    <div class="form-group row">
                                        <div class="col-xs-5 col-sm-5 col-md-5 col-lg-5 form-validate" id="setear_select_oc_boleta">
                                            <label for="orden_compra" class="label-form">Orden de Compra<span class="span-label">*</span></label>            
                                            <select name="orden_compra" id="orden_compra" class="form-control select2" onchange="getDatosOc(this);" >
                                                <option value=''>Seleccione</option>
                                                @if( isset($optionsOc) )
                                                    {!! $optionsOc !!}
                                                @endif
                                            </select>
                                        </div>

                                        <div class="ocultar_segun_oc" 
                                            @if( $documento->getContratoOrdenCompra ) 
                                                @if( ! $documento->getContratoOrdenCompra->getOrdenCompra ) 
                                                    style="display:none;"
                                                @endif 
                                            @else
                                                style="display:none;"
                                            @endif >

                                            <div class="col-xs-3 col-sm-3 col-md-3 col-lg-3 form-validate">
                                                <label for="inicio_vigencia_orden_compra" class="label-form">Inicio Vigencia</label>
                                                <input type="text" class="form-control" id="inicio_vigencia_orden_compra" name="inicio_vigencia_orden_compra" readonly 
                                                        @if( $documento->getContratoOrdenCompra && $documento->getContratoOrdenCompra->getOrdenCompra )
                                                            value="{{ fecha_dmY($documento->getContratoOrdenCompra->getOrdenCompra->inicio_vigencia) }}" 
                                                        @endif >
                                            </div>
                                        
                
                                            <div class="col-xs-3 col-sm-3 col-md-3 col-lg-3 form-validate">
                                                <label for="fin_vigencia_orden_compra" class="label-form">Fin Vigencia</label>
                                                <input type="text" class="form-control" id="fin_vigencia_orden_compra" name="fin_vigencia_orden_compra" readonly 
                                                        @if( $documento->getContratoOrdenCompra && $documento->getContratoOrdenCompra->getOrdenCompra )
                                                            value="{{ fecha_dmY($documento->getContratoOrdenCompra->getOrdenCompra->fin_vigencia) }}" 
                                                        @endif >
                                            </div>

                                        </div>
                                    </div>
                
                                    <div class="ocultar_segun_oc" 
                                        @if( $documento->getContratoOrdenCompra ) 
                                            @if( ! $documento->getContratoOrdenCompra->getOrdenCompra ) 
                                                style="display:none;"
                                            @endif 
                                        @else
                                            style="display:none;"
                                        @endif >
                
                                        <div class="form-group row">

                                            <div class="col-xs-4 col-sm-4 col-md-4 col-lg-4 form-validate">
                                                <label for="monto_orden_compra" class="label-form">Monto</label>
                                                <div class="input-group">
                                                    <span class="input-group-addon">$</span>
                                                    <input type="text" class="form-control" id="monto_orden_compra" name="monto_orden_compra" readonly 
                                                            @if( $documento->getContratoOrdenCompra && $documento->getContratoOrdenCompra->getOrdenCompra )
                                                                value="{{ formatoMiles($documento->getContratoOrdenCompra->getOrdenCompra->monto_oc) }}" 
                                                            @endif >
                                                </div>
                                            </div>

                                            <div class="col-xs-4 col-sm-4 col-md-4 col-lg-4 form-validate">
                                                <label for="saldo_orden_compra" class="label-form">Saldo</label>
                                                <div class="input-group">
                                                    <span class="input-group-addon">$</span>
                                                    <input type="text" class="form-control" id="saldo_orden_compra" name="saldo_orden_compra" readonly 
                                                            @if( $documento->getContratoOrdenCompra && $documento->getContratoOrdenCompra->getOrdenCompra )
                                                                value="{{ formatoMiles($documento->getContratoOrdenCompra->getOrdenCompra->saldo_oc) }}" 
                                                            @endif >
                                                </div>
                                            </div>

                                            <div class="col-xs-3 col-sm-3 col-md-3 col-lg-3 form-validate">
                                                <label for="fecha_orden_compra" class="label-form">Fecha</label>
                                                <input type="text" class="form-control" id="fecha_orden_compra" name="fecha_orden_compra" readonly 
                                                        @if( $documento->getContratoOrdenCompra && $documento->getContratoOrdenCompra->getOrdenCompra )
                                                            value="{{ fecha_dmY($documento->getContratoOrdenCompra->getOrdenCompra->fecha_oc) }}" 
                                                        @endif >
                                            </div>

                                        </div>                                                    
                
                                    </div>
                                </div> <!-- /informacion_orden_compra -->
                                    

                                <div id="divBodega" @if ($opciones == '') style="display:none;" @endif>
                                    <h4 class="form-section" style="color: #69aa46;">Documentos de Recepción</h4>
                                    <div class="form-group row">
                                        <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 form-validate" >
                                            <label for="doc_disponible_bodega" class="label-form">Disponibles</label>
                                            <div id="divDocumentosRecepcion">
                                                @if ($opciones != '')
                                                    <select name="doc_disponible_bodega" id="doc_disponible_bodega" class="form-control select2" onchange="setDocumento(this);" >
                                                        <option value="">Seleccione</option>
                                                        {!! $opciones !!}
                                                    </select>
                                                @endif
                                            </div>
                                        </div>
                                    </div>
                                </div>

                                <div id="informacion_recepcion" 
                                        @if( $documento->getUnaRelacionRecepcionValidada == null ) 
                                            @if( ! $documento->getRelacionGuiasDespacho() ) 
                                                style="display:none;" 
                                            @endif
                                        @endif >

                                    <div class="form-group row">

                                        <div class="col-xs-4 col-sm-4 col-md-4 col-lg-4 form-validate">
                                            <label for="orden_compra_prov" class="label-form">Orden de Compra</label>
                                            <input type="text" class="form-control " id="orden_compra_prov" 
                                                    name="orden_compra_prov" readonly 
                                                    @if ($documento->getUnaRelacionRecepcionValidada && ! $documento->getRelacionGuiasDespacho())
                                                        value="{{ $documento->getUnaRelacionRecepcionValidada->getWsDocumento->getWsOrdenCompra->numero_orden_compra_mercado_publico }}"
                                                    @endif
                                                    >
                                        </div>
                                        
                                        <div class="col-xs-4 col-sm-4 col-md-4 col-lg-4 form-validate">
                                            <label for="monto_maximo_oc" class="label-form">Monto Máximo OC</label>
                                            <div class="input-group">
                                                <span class="input-group-addon">$</span>
                                                <input type="text" class="form-control" id="monto_maximo_oc" 
                                                        name="monto_maximo_oc" readonly 
                                                        @if ($documento->getUnaRelacionRecepcionValidada && ! $documento->getRelacionGuiasDespacho())
                                                            value="{{ formatoMiles($documento->getUnaRelacionRecepcionValidada->getWsDocumento->getWsOrdenCompra->total_orden_compra) }}"
                                                        @endif
                                                        >
                                            </div>
                                        </div>

                                        <div class="col-xs-4 col-sm-4 col-md-4 col-lg-4 form-validate">
                                            <label for="saldo_oc" class="label-form">Saldo OC</label>
                                            <div class="input-group">
                                                <span class="input-group-addon">$</span>
                                                <input type="text" class="form-control" id="saldo_oc" 
                                                        name="saldo_oc" readonly 
                                                        @if ($documento->getUnaRelacionRecepcionValidada && ! $documento->getRelacionGuiasDespacho())
                                                            value="{{ formatoMiles($documento->getUnaRelacionRecepcionValidada->getWsDocumento->getWsOrdenCompra->saldo_orden_compra) }}"
                                                        @endif
                                                        >
                                            </div>
                                        </div>

                                    </div>

                                </div> <!-- /informacion_recepcion -->

                                <div id="divGuiasDespacho" @if ($opcionesGuias == '') style="display:none;" @endif>
                                    <h4 class="form-section" style="color: #69aa46;">Guías Despacho de Recepción</h4>
                                    <div class="form-group row">
                                        <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 form-validate">
                                            <label for="guias_disponible_bodega" class="label-form">Disponibles</label>
                                            <div id="divGuiasRecepcionBodega">
                                                @if ($opcionesGuias != '')
                                                    <select name="guias_disponible_bodega[]" id="guias_disponible_bodega" 
                                                            class="form-control select2" multiple placeholder="Seleccione">
                                                        {!! $opcionesGuias !!}
                                                    </select>
                                                @endif
                                            </div>
                                        </div>
                                    </div>
                                    <div class="form-group row">
                                        <label for="total_monto_guias" class="col-xs-3 col-sm-3 col-md-3 col-lg-3 control-label label-form" style="text-align:left;">Total Guías</label>
                                        <div class="col-xs-7 col-sm-7 col-md-7 col-lg-7 form-validate">
                                            <div class="input-group">
                                                <span class="input-group-addon">$</span>
                                                <input type="text" class="form-control " id="total_monto_guias" name="total_monto_guias" @if($totalGuias != 0) value="{{ formatoMiles($totalGuias) }}" @else value="0"  @endif readonly>
                                            </div>
                                        </div>
                                    </div>
                                </div>

                                <div id="divFacturasHermes" 
                                    @if ( isset($opcionesFacturasHermes) && $opcionesFacturasHermes == '') style="display:none;" @endif
                                    @if ( ! isset($opcionesFacturasHermes) ) style="display:none;" @endif>
                                    <h4 class="form-section" style="color: #69aa46;">Documentos Recepción Hermes</h4>
                                    <div class="form-group row">
                                        
                                        <div class="col-xs-12 form-validate">
                                            <label for="doc_disponible_bodega" class="label-form">Disponibles</label>
                                            @if ( isset($opcionesFacturasHermes) && $opcionesFacturasHermes != '')
                                                <select name="factura_recepcion_hermes" id="factura_recepcion_hermes" class="form-control select2" onchange="setRecepcionHermes(this);" >
                                                    <option value="">Seleccione</option>
                                                    {!! $opcionesFacturasHermes !!}
                                                </select>
                                            @endif
                                        </div>

                                    </div>
                                </div>

                                <div id="divGuiasDespachoHermes" 
                                    @if ( isset($opcionesGuiasHermes) && $opcionesGuiasHermes == '') style="display:none;" @endif
                                    @if ( ! isset($opcionesGuiasHermes) ) style="display:none;" @endif>
                                    <h4 class="form-section" style="color: #69aa46;">Guías Despacho de Hermes</h4>
                                    <div class="form-group row">
                                        <label for="guias_disponible_hermes" class="col-xs-2 col-sm-2 col-md-2 col-lg-2 control-label label-form">Disponibles</label>
                                        <div class="col-xs-10 col-sm-10 col-md-10 col-lg-10 form-validate" id="divGuiasRecepcionHermes">
                                            @if ( isset($opcionesGuiasHermes) && $opcionesGuiasHermes != '')
                                                <select name="guias_disponible_hermes[]" id="guias_disponible_hermes" 
                                                        class="form-control select2" multiple placeholder="Seleccione">
                                                    {!! $opcionesGuiasHermes !!}
                                                </select>
                                            @endif
                                        </div>
                                    </div>
                                    <div class="form-group row">
                                        <label for="total_monto_guias_hermes" class="col-xs-3 col-sm-3 col-md-3 col-lg-3 control-label label-form" style="text-align:left;">Total Guías</label>
                                        <div class="col-xs-7 col-sm-7 col-md-7 col-lg-7 form-validate">
                                            <div class="input-group">
                                                <span class="input-group-addon">$</span>
                                                <input type="text" class="form-control " id="total_monto_guias_hermes" name="total_monto_guias_hermes" 
                                                        value="{{ isset($totalGuiasHermes) && $totalGuiasHermes != 0 ? formatoMiles($totalGuiasHermes) : '0' }}" readonly>
                                            </div>
                                        </div>
                                    </div>
                                </div>

                            </div> <!-- / div class col-xs-4 -->
                        </div> <!-- form-group -->


                        <h4 class="form-section" style="color: #69aa46;">Documento en PDF</h4>
                        <div class="form-group row">
                            <div class="col-xs-12 table-responsive">
                                <table class="table table-striped table-bordered table-hover " id="tabla_info_archivos_existentes">
                                    <thead>
                                        <tr>
                                            <th width="50%">Nombre del Archivo</th>
                                            <th width="20%">Tipo</th>
                                            <th width="10%">Extensión</th>
                                            <th width="10%">Peso</th>
                                            <th width="10%"><i class="fa fa-cog"></i></th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                    @forelse ($documento->getArchivos as $archivo)
                                        <tr>
                                            <td>{{ $archivo->nombre }}</td>
                                            <td>@if( $archivo->getTipoArchivo ){{ $archivo->getTipoArchivo->nombre }}@endif</td>
                                            <td>{{ $archivo->extension }}</td>
                                            <td>{{ pesoArchivoEnMB($archivo->peso) }}</td>
                                            <td>
                                                <a class="btn btn-success btn-xs" @if($archivo->cargado == 0) href="{{ asset( $archivo->ubicacion ) }}" @else href="http://{{ $archivo->ubicacion }}" @endif
                                                title="Ver Archivo" target="_blank" ><i class="fa fa-eye"></i></a>
                                                <label>
                                                    <input type="checkbox" name="delete_list[]" value="{{$archivo->id}}"> Eliminar
                                                </label>
                                            </td>
                                        </tr>
                                    @empty

                                    @endforelse
                                    </tbody>
                                </table>
                            </div>
                        </div>

                        <div class="form-group row">
                            <div class="input-group col-sm-10 col-sm-offset-1">
                                <span class="input-group-addon" id="inputGroupFileAddon01"><strong>Seleccione Documento</strong></span>
                                <input type="file" class="form-control" accept=".pdf" name="archivo" id="archivo" aria-describedby="inputGroupFileAddon01" onchange="validarArchivo();">
                            </div>
                        </div>

                        <div class="form-group row">
                            <div id="div_archivo" class="col-xs-12 table-responsive" style="display: none;">
                                <table class="table table-striped table-bordered table-hover " id="tabla_info_archivo_edit">
                                    <thead>
                                        <tr>
                                            <th width="40%">Nombre</th>
                                            <th width="20%">Peso</th>
                                            <th width="40%">Tipo</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                    </tbody>
                                </table>
                            </div>
                        </div>

                        <h4 class="form-section" style="color: #69aa46;">Ítem Presupuestario</h4>
                        @if( $documento->getFoliosSigfe->count() == 0 )
                            <div class="row btn-group col-xs-12">
                                <button type="button" id="botonAgregarItem" title="Agregar Ítem Presupuestario" 
                                class="btn btn-xs btn-primary col-xs-2" onclick="agregarItem();">
                                Agregar Ítem Presupuestario </button>
                            </div>
                        @else
                            <div class="form-group row">
                                <span style="font-size: 14px;margin-left: 10px;font-weight:bold;" class="label label-info" >
                                     La edición de los items presupuestarios es en "Devengo > Devengado"
                                </span>
                            </div>
                        @endif
                        <div class="form-group row">
                            <div id="div_items" class="col-xs-12 table-responsive" @if(count($documento->getDevengos) == 0) style="display: none;" @endif>
                                <table class="table " id="tabla_item_presupuestario">
                                    
                                    <thead style="display:none" >
                                        <tr>
                                            <th class="col-sm-2">Código</th>
                                            <th class="col-sm-6">Clasificador</th>
                                            <th class="col-sm-3">Monto</th>
                                            @if ( $documento->getFoliosSigfe->count() > 0 )
                                                {{-- <th >Información SIGFE</th> --}}
                                            @else
                                                <th class="col-sm-1"><div class='text-center'><i class='fa fa-cog'></i></div></th>
                                            @endif
                                        </tr>
                                    </thead>
                                    <tbody>
                                        @if ( $documento->getFoliosSigfe->count() > 0 )
                                            @foreach ($documento->getFoliosSigfe as $folioSigfe)
                                                @forelse ( $folioSigfe->getDevengos as $devengo )
                                                    <tr id="{{ $devengo->id_item_presupuestario }}">
                                                        <td class="td_items ">{{ $devengo->getItemPresupuestario->codigo() }}</td>
                                                        <td class="td_items ">{{ $devengo->getItemPresupuestario->clasificador_presupuestario }} <span class="span-label">*</span></td>
                                                        <td class="">
                                                            <div class="input-group form-validate">
                                                                <span class="input-group-addon">$</span>
                                                                <input type="text" class="form-control numero_documento" id="valor_item_{{ $devengo->id_item_presupuestario }}" 
                                                                name="valor_item[{{ $devengo->id_item_presupuestario }}]" required onkeyup="ingresoPesos(this);"
                                                                readonly
                                                                value="{{ formatoMiles($devengo->monto) }}">
                                                            </div>
                                                            &nbsp;&nbsp;ID Sigfe : {{ $folioSigfe->id_sigfe }} Fecha : {{ fecha_dmY($folioSigfe->fecha_sigfe) }}
                                                        </td>
                                                        {{-- <td class="td_items pull-left">ID Sigfe : {{ $folioSigfe->id_sigfe }} Fecha : {{ fecha_dmY($folioSigfe->fecha_sigfe) }}</td> --}}
                                                    </tr>
                                                @empty

                                                @endforelse
                                            @endforeach
                                        @else
                                            @forelse ( $documento->getDevengos as $devengo )
                                                <tr id="{{ $devengo->id_item_presupuestario }}">
                                                    <td class="td_items ">{{ $devengo->getItemPresupuestario->codigo() }}</td>
                                                    <td class="td_items ">{{ $devengo->getItemPresupuestario->clasificador_presupuestario }} <span class="span-label">*</span></td>
                                                    <td class="">
                                                        <div class="input-group form-validate">
                                                            <span class="input-group-addon">$</span>
                                                            <input type="text" class="form-control numero_documento" id="valor_item_{{ $devengo->id_item_presupuestario }}" 
                                                            name="valor_item[{{ $devengo->id_item_presupuestario }}]" required onkeyup="ingresoPesos(this);" 
                                                            value="{{ formatoMiles($devengo->monto) }}">
                                                        </div>
                                                    </td>
                                                    <td class="td_items_dos text-center">
                                                        <div class="btn-group">
                                                            <button type="button" class="btn btn-danger btn-xs" title="Eliminar Item" 
                                                            onclick="eliminarItem({{ $devengo->id_item_presupuestario }});"><i class="fa fa-trash"></i> Eliminar
                                                            </button>
                                                        </div>
                                                    </td>
                                                </tr>
                                            @empty

                                            @endforelse
                                        @endif
                                    </tbody>
                                </table>
                            </div>
                        </div>


                        <h4 class="form-section" style="color: #69aa46;">Observaciones</h4>
                        <div class="form-group row">
                            <div class="col-sm-12">
                                <textarea class="form-control noresize" id="observacion_documento" name="observacion_documento">{{ $documento->observacion }}</textarea>
                            </div>
                        </div>


                        <input type="hidden" name="_token" value="{{ csrf_token() }}">
                        <input type="hidden" name="id_documento" value="{{ $documento->id }}">
                        <input type="hidden" name="datatable" value="{{ $datatable }}">

                    </div>
                </div> {{-- /modal-body --}}

                <div class="modal-footer form-actions right">
                    <button type="button" title="Cancelar" class="btn btn-default" data-dismiss="modal">Cancelar</button>
                    @if ($datatable == 'pendientes_validacion')
                        <button type="submit" title="Validar Documento" class="btn btn-success" id="botonGuardar"><i class="far fa-save" style="color: black;"></i> <i class="fas fa-thumbs-up" style="color:black;"></i> Validar Documento</button>
                    @elseif ($datatable == 'validados')
                        <button type="submit" title="Invalidar Documento" class="btn btn-danger" id="botonGuardar"><i class="far fa-save" style="color: black;"></i> <i class="fas fa-thumbs-down" style="color:black;"></i> Invalidar Documento</button>
                    @else
                        <button type="submit" title="Editar Documento" class="btn btn-success" id="botonGuardar"><i class="far fa-save" style="color: black;"></i> Editar Documento</button>
                    @endif
                </div>
            </form>
        </div>
    </div>
</div>
<script type="text/javascript" src="{{asset('sistema/js/documentos_finanzas.js')}}?v={{rand()}}"></script>
<script type="text/javascript">

    var urlGetContratosProveedor = '{{ url('contratos/get_contratos_para_documentos/') }}';

    var urlGetOrdenesCompraProveedor = '{{ url('ordenes_compra/get_oc_para_boleta/') }}';

    var urlGetFacturasRechazadasAcepta = '{{ url('documentos/get_facturas_rechazadas_acepta/') }}';

    var urlGetItemsAutomaticos = '{{ url('documentos/get_items_presupuestarios_recepcion/') }}';
    // nuevas variables globales
    var urlGetGuiasDespachoBodega = '{{ url('documentos/get_guias_despacho_bodega_proveedor/') }}';

    // var urlGetUltimaOcProveedor = '{{ url('documentos/get_oc_proveedor/') }}';
    var urlGetDocumentosBodega = '{{ url('documentos/get_documentos_bodega_proveedor/') }}';
    
    var urlGetProveedor = '{{ url('documentos/get_proveedor/') }}';
    var selectTipoArchivo = '<select name="tipo_archivo" id="tipo_archivo" class="col-xs-12" required>';
    selectTipoArchivo += '<option value="">Seleccione</option>';
    @foreach ( $tiposArchivo as $tipoArchivo)
        selectTipoArchivo += '<option value="{{ $tipoArchivo->id }}">{{ $tipoArchivo->nombre }}</option>';
    @endforeach
    selectTipoArchivo += '</select>';
    var urlAgregarItem = '{{ url('documentos/modal/agregar_item') }}';
    var urlSetReemplaza = '{{ url('documentos/get_documentos_proveedor/') }}';
    var urlSetOrdenCompra = '{{ url('documentos/get_predeterminado_modalidad_compra/') }}';

    /*Nuevo para permisos*/
    var verDocumento = '{{ \Entrust::can('ver-documento') }}';
    var editarDocumento = '{{ \Entrust::can('editar-documento') }}';
    var eliminarDocumento = '{{ \Entrust::can('eliminar-documento') }}';
    var cuadrarDocumento = '{{ \Entrust::can('cuadrar-documento') }}';

    var problemasDocumento = '{{ \Entrust::can('problemas-documento') }}';
    var quitarProblemasDocumento = '{{ \Entrust::can('quitar-problemas-documento') }}';
    
    var arregloItems = [
        @forelse($documento->getDevengos as $devengo)
            {!! $devengo->getItemPresupuestario->id !!},
        @empty

        @endforelse
    ];
    
    var tablaArchivo = $('#tabla_info_archivo_edit').DataTable({
        "paging":   false,
        "ordering": false,
        "info":     false,
        "searching": false
    });

    var tablaItemsPresupuestarios = $('#tabla_item_presupuestario').DataTable({
        "paging":   false,
        "ordering": false,
        "info":     false,
        "searching": false
    });

    var tablaArchivosExistentes = $('#tabla_info_archivos_existentes').DataTable({
        "language": {
            "emptyTable": "No hay datos disponibles en la tabla",
        },
        "paging":   false,
        "ordering": false,
        "info":     false,
        "searching": false
    });

    $(document).ready(function() {

        $('#fecha_recepcion').datepicker({
            format: 'dd/mm/yyyy',
            //endDate: new Date(),
            //"setDate": new Date(),
            //startDate: new Date(),
            autoclose: true,
            language: 'es'
        });

        $('#fecha_documento').datepicker({
            format: 'dd/mm/yyyy',
            endDate: new Date(),
            autoclose: true,
            language: 'es'
        });

        @if( $documento->getContratoOrdenCompra )
            @if( $documento->getContratoOrdenCompra->getOrdenCompra )

                $('#periodo').datepicker({
                    format: 'mm/yyyy',
                    // endDate: new Date(),
                    startDate: '{{ fecha_mY($documento->getContratoOrdenCompra->getOrdenCompra->inicio_vigencia) }}',
                    endDate: '{{ fecha_mY($documento->getContratoOrdenCompra->getOrdenCompra->fin_vigencia) }}',
                    autoclose: true,
                    language: 'es',
                    viewMode: 'months',
                    minViewMode: 'months'
                });
            
            @elseif( $documento->getContratoOrdenCompra->getContrato )

                $('#periodo').datepicker({
                    format: 'mm/yyyy',
                    // endDate: new Date(),
                    startDate: '{{ fecha_mY($documento->getContratoOrdenCompra->getContrato->inicio_vigencia) }}',
                    endDate: '{{ fecha_mY($documento->getContratoOrdenCompra->getContrato->fin_vigencia) }}',
                    autoclose: true,
                    language: 'es',
                    viewMode: 'months',
                    minViewMode: 'months'
                });

            @else

                $('#periodo').datepicker({
                    format: 'mm/yyyy',
                    // endDate: new Date(),
                    autoclose: true,
                    language: 'es',
                    viewMode: 'months',
                    minViewMode: 'months'
                });

            @endif
        @else

            $('#periodo').datepicker({
                format: 'mm/yyyy',
                // endDate: new Date(),
                autoclose: true,
                language: 'es',
                viewMode: 'months',
                minViewMode: 'months'
            });
            
        @endif

        $(".numero_documento").keypress(function (key) {
            // window.console.log(key.charCode); //Descomentar para ver codigo
            if (
                (key.charCode < 48 || key.charCode > 57)//números
                && (key.charCode != 0) //borrar y enter
                && (key.charCode != 44) //coma
                && (key.charCode != 46) //punto
                && (key.charCode != 45) //guion
                && (key.charCode != 43) //suma
                )
                //console.log(key.charCode);
                return false;
        });

        $("#nombre_proveedor").select2({
            ajax: {
                cache: true,
                allowClear: true,
                //hidden : true,
                url : '{{ url('buscador_proveedor/') }}',
                dataType: 'json',
                delay: 250,
                data: function (params,page) {
                    //console.log("soy params : "+params);
                    //console.log("soy page : "+page);
                    var query = {
                        term: params,
                        page: params.page || 1
                    }
                    return query;
                },
                results: function (data) {
                    return {
                    results: data
                    };
                },
            },
            minimumInputLength: 2,
        
        });

        $(".select2").select2();

        @if ($opcionesGuias != '')
            $('#guias_disponible_bodega').on('select2-selecting', function (e) {

                // No se permite añadir el elemento si excede el monto del documento
                if ( ! setTotalMontoGuias(e.val, 'Agregar') ) {
                    e.preventDefault();
                }

            });

            $('#guias_disponible_bodega').on('select2-removing', function (e) {

                // No se permite añadir el elemento si excede el monto del documento
                if ( ! setTotalMontoGuias(e.val, 'Quitar') ) {
                    e.preventDefault();
                }

            });

            if ( parseInt($('#guias_disponible_bodega').find('option:selected').length) > 0 ) {
                // Hay elementos seleccionados
                // Dejar readonly doc_disponible_bodega
                if ($('#doc_disponible_bodega').length > 0) {
                    console.log('existe doc_disponible_bodega, readonly true');
                    $('#doc_disponible_bodega').select2("readonly", true);
                }
            }
            console.log('largo selected guias_disponible_bodega :' + parseInt($('#guias_disponible_bodega').find('option:selected').length) );
        @endif

        @if ($opciones != '')
            console.log('value selected doc_disponible_bodega :' + $('#doc_disponible_bodega').find('option:selected').val() );

            if ( $('#doc_disponible_bodega').find('option:selected').val() != '' ) {
                // Hay elementos seleccionados
                // Dejar readonly guias_disponible_bodega
                if ($('#guias_disponible_bodega').length > 0) {
                    console.log('existe guias_disponible_bodega, readonly true');
                    $('#guias_disponible_bodega').select2("readonly", true);
                }
            }

        @endif

        @if ( $documento->id_relacionado != null )
            if ( $('#reemplaza_archivo_acepta').length > 0 ) {
                $('#reemplaza_archivo_acepta').select2("readonly", true);
            }

            $('#reemplaza').trigger("change");
        @endif

        @if ( $documento->id_archivo_acepta_refacturado != null )
            if ( $('#reemplaza').length > 0 ) {
                $('#reemplaza').select2("readonly", true);
            }
        @endif

        @if ( $documento->id_tipo_documento == 4 || $documento->id_tipo_documento == 5 || $documento->id_tipo_documento == 10 || $documento->id_tipo_documento == 11 )
            // $('#reemplaza').trigger("change");
            var montoMaximoNc = {{ $documento->getDocumentoRelacionado->total_documento_actualizado }};
        @endif       

        // Se pasan valores a los select AJAX
        $('#nombre_proveedor').select2('data', { id: '{!! $documento->id_proveedor !!}', text: '{!!$documento->getProveedor->formatRut() !!} {!!$documento->getProveedor->nombre !!}' });
        // Se pasan valores a select normales
        $('#tipo_documento').select2('val',{{ $documento->id_tipo_documento }});
        $('#modalidad_compra').select2('val',{{ $documento->id_modalidad_compra }});
        $('#tipo_adjudicacion').select2('val',{{ $documento->id_tipo_adjudicacion }});
        $('#tipo_informe').select2('val',{{ $documento->id_tipo_informe }});
        // $('#responsable').select2('val',{{ $documento->id_responsable }});
        $('#referente_tecnico').select2('val',{{ $documento->id_referente_tecnico }});
        

        $("#form-documentos").validate({
            highlight: function(element) {
                $(element).closest('.form-validate').removeClass('has-success');
                $(element).closest('.form-validate').addClass('has-error');
            },
            unhighlight: function(element) {
                $(element).closest('.form-validate').removeClass('has-error');
                $(element).closest('.form-validate').addClass('has-success');
            },
            errorElement: 'span',
            errorClass: 'help-block',
            errorPlacement: function (error, element) {
                console.log(element);
                if (element.parent('.input-group').length) {
                    error.insertAfter(element.parent());
                } else {
                    error.insertAfter(element);
                }
            },
            rules: {
            },
            messages: {
            },

            //para enviar el formulario por ajax
            submitHandler: function(form) {
                esperandoIngresoDocumento();
                if (validaTotalDocumentoConItemsPresupuestario() && validaNumDocumentoAndLicitacion() && validaTotalGuiasConTotalDocumento() && validaTotalGuiasHermesConTotalDocumento() ) {

                    let formData = new FormData(form);
                    $.ajax({
                        url: form.action,
                        type: form.method,
                        //data: $(form).serialize(),
                        data: formData,
                        processData: false,
                        contentType: false,
                        success: function(respuesta) {
                            console.log(respuesta);
                            
                            if ( respuesta.estado_documento == 'error' ) {
                                toastr.error('No es posible realizar la acción'+'<br><strong>Error: '+ respuesta.mensaje_documento +'</strong>', 'Atención', optionsToastr);
                            } else if ( respuesta.estado_documento == 'success') {
                                
                                toastr.success(respuesta.mensaje_documento, 'Atención', optionsToastr);
                                // Mostrar mensaje de acuerdo al match
                                if ( respuesta.mensajeMatch != '' ) {
                                    toastr.error(respuesta.mensajeMatch, 'Atención', optionsToastr);
                                }
                                // Funcion que utiliza la respuesta para actualizar la tabla principal
                                actualizaTabla(respuesta);
                                $("#modalEditar").modal("hide");

                                if ( respuesta.estado_archivo == 'error' ) {
                                    toastr.error('Archivo: No es posible realizar la acción'+'<br><strong>Error: '+ respuesta.mensaje_archivo +'</strong>', 'Atención', optionsToastr);
                                } else if ( respuesta.estado_archivo == 'succes') {
                                    toastr.success(respuesta.mensaje_archivo, 'Atención', optionsToastr);
                                }
                                
                            }
                            
                        }            
                    }).fail( function(respuesta) {//fail ajax
                        if ( respuesta.status == 400 ) {
                            mostrarErroresValidator(respuesta);
                        } else if ( respuesta.status == 500 ) {
                            toastr.error('No es posible realizar la acción, error en el servidor', 'Atención', optionsToastr);
                        } else {
                            toastr.error('No es posible realizar la acción', 'Atención', optionsToastr);
                        }
                        
                    })
                    .always(function() {
                        listoIngresoDocumento();
                    });//ajax

                } else {
                    listoIngresoDocumento();
                }
                
            }

        });

    });
            
</script>