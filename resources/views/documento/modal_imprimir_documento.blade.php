<link href="{{ asset('sistema/css/modales.css') }}?v={{rand()}}" rel="stylesheet" type="text/css"/>
<div class="modal fade" id="modalImprimirDocumento" role="dialog" aria-labelledby="myModalLabel3" aria-hidden="true">
	<div class="modal-dialog" style="width:1000px;">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" title="Cerrar" class="close" data-dismiss="modal" aria-hidden="true"></button>
				<h4 class="modal-title" ><strong><i class="fas fa-print fa-lg"></i>&nbsp;&nbsp;Imprimir Archivos del Documento</strong></h4>
            </div>
          
            <div class="modal-body">
                <div class="form-group row">
                    <div class="col-xs-12">	
                        <h4 class="headers-traceability">
                            Proveedor
                        </h4>
                        <hr>
                        <div class="form-group">
                            <label class="col-sm-2 control-label font-bold">Nombre</label>
                            <label class="col-sm-4 control-label">@if($documento->getProveedor){{ $documento->getProveedor->nombre }}@endif</label>
                            <label class="col-sm-2 control-label font-bold">Rut </label>
                            <label class="col-sm-4 control-label">@if($documento->getProveedor){{ $documento->getProveedor->rut }}@endif</label>
                        </div>
                    </div>
                </div>

                @if ( $documento->getProveedor->getProveedorMaestro )
                    <div class="form-group row">
                        <div class="col-xs-12">
                            <div class="form-group">
                                <label class="col-sm-3 control-label font-bold">Proveedor "Maestro"</label>
                                <label class="col-sm-9 control-label">{{ $documento->getProveedor->getProveedorMaestro->rut }} {{ $documento->getProveedor->getProveedorMaestro->nombre }}</label>
                            </div>
                        </div>
                    </div>
                @endif

                @if ( $documento->getProveedor->getProveedoresFusion->count() > 0 )
                    <div class="form-group row">
                        <div class="col-xs-12">
                            <div class="form-group">
                                <label class="col-sm-3 control-label font-bold">Proveedores Fusionados</label>
                                <label class="col-sm-9 control-label">
                                    @foreach ($documento->getProveedor->getProveedoresFusion as $provFusion)
                                    {{ $provFusion->rut }} {{ $provFusion->nombre }}<br>
                                    @endforeach
                                </label>
                            </div>
                        </div>
                    </div>
                @endif

                <div class="form-group row">
                    <div class="col-xs-12">	
                        <h4 class="headers-traceability">
                            Documento
                        </h4>
                        <hr>
                        <div class="form-group">
                            <label class="col-sm-2 control-label font-bold">Tipo Documento</label>
                            <label class="col-sm-4 control-label ">{{ $documento->getTipoDocumento->nombre }}</label>
                            {{-- Inicio Refacturación --}}
                            @if ( $documento->id_relacionado != null )
                                @if ( $documento->id_tipo_documento == 4 || $documento->id_tipo_documento == 5 || $documento->id_tipo_documento == 10 || $documento->id_tipo_documento == 11 )
                                    <label class="col-sm-2 control-label font-bold">Documento Relacionado</label>
                                    <label class="col-sm-4 control-label ">{{ $documento->getDocumentoRelacionado->numero_documento }}</label>
                                @elseif ( $documento->id_tipo_documento > 0 )
                                    <label class="col-sm-2 control-label font-bold">Reemplaza a</label>
                                    <label class="col-sm-4 control-label ">{{ $documento->getDocumentoRelacionado->numero_documento }}</label>
                                @endif
                            @endif
                            {{-- Fin Refacturación --}}
                        </div>
                    </div>
                </div>

                @if ( $documento->getArchivoAceptaRefacturado )
                    <div class="form-group row">
                        <div class="col-xs-12">
                            <label class="col-sm-5 control-label font-bold">Reemplaza al Archivo Acepta Rechazado</label>
                            <label class="col-sm-4 control-label ">{{ $documento->getArchivoAceptaRefacturado->folio }}</label>
                        </div>
                    </div>
                @endif

                <div class="form-group row">
                    <div class="col-xs-12">
                        <label class="col-sm-2 control-label font-bold">N° Documento</label>
                        <label class="col-sm-4 control-label ">{{ $documento->numero_documento }}</label>
                    </div>
                </div>

                <div class="form-group row">
                    <div class="col-xs-12">
                        <label class="col-sm-2 control-label font-bold">Valor Tot. Orig.</label>
                        <label class="col-sm-4 control-label ">$ {{ formatoMiles($documento->total_documento) }}</label>
                        <label class="col-sm-2 control-label font-bold">Valor Tot. Act.</label>
                        <label class="col-sm-4 control-label ">$ {{ formatoMiles($documento->total_documento_actualizado) }}</label>
                    </div>
                </div>

                @if ( $documento->id_tipo_documento == 3 )
                    <div class="form-group row">
                        <div class="col-xs-12">
                            <label class="col-sm-2 control-label font-bold">{{ $documento->getLabelImpuesto() }} Impuesto</label>
                            <label class="col-sm-4 control-label ">$ {{ formatoMiles($documento->impuesto) }}</label>
                        </div>
                    </div>

                    <div class="form-group row">
                        <div class="col-xs-12">
                            <label class="col-sm-2 control-label font-bold">Líquido</label>
                            <label class="col-sm-4 control-label ">$ {{ formatoMiles($documento->liquido) }}</label>
                        </div>
                    </div>
                @endif


                <div class="form-group row">
                    <div class="col-xs-12">
                        <label class="col-sm-3 control-label font-bold">N° Documento Compra</label>
                        <label class="col-sm-4 control-label ">{{ $documento->documento_compra }}</label>
                        
                    </div>
                </div>

                <div class="form-group row">
                    <div class="col-xs-12">
                        <label class="col-sm-2 control-label font-bold">Fecha Documento</label>
                        <label class="col-sm-4 control-label ">{{ fecha_dmY($documento->fecha_documento) }}</label>
                        <label class="col-sm-2 control-label font-bold">Fecha Recepción</label>
                        <label class="col-sm-4 control-label ">{{ fecha_dmY($documento->fecha_recepcion) }}</label>
                    </div>
                </div>
                @if ($documento->getArchivoAcepta)
                    <div class="form-group row">
                        <div class="col-xs-12">
                            <label class="col-sm-2 control-label font-bold">Fecha Publicación</label>
                            <label class="col-sm-4 control-label ">{{ fecha_dmY($documento->getArchivoAcepta->publicacion) }}</label>
                        </div>
                    </div>
                @endif

                @if ( $documento->observacion != '' && $documento->observacion != null )
                    <div class="form-group row">
                        <div class="col-xs-12">
                            <label class="col-sm-3 control-label font-bold">Observaciones </label>
                            <label class="col-sm-9 control-label ">{!! str_replace(PHP_EOL,'<br>',$documento->observacion) !!}</label>
                        </div>
                    </div>

                    <div class="col-xs-12" style="margin-top:-30px;">
                        <hr style="border-top-color: #46c737;border-bottom-color: #d9dad9;">
                    </div>
                @endif

                <div class="form-group row">
                    <div class="col-xs-12">	
                        <h4 class="headers-traceability">
                            Archivos Para Imprimir <a class="btn btn-success btn-xs" href="{{ asset('documentos/imprimir/todos_los_archivos/').'/'.$documento->id }}"
                                title="Imprimir Todos" target="_blank" ><i class="fa fa-eye"></i></a>
                        </h4>
                        <hr>
                        <div class="form-group">
                            <div class="col-xs-12 table-responsive">
                                <table class="table table-striped table-bordered table-hover " id="tabla_info_archivo">
                                    <thead>
                                        <tr>
                                            <th width="40%">Nombre del Archivo</th>
                                            <th width="30%">Tipo</th>
                                            
                                            <th width="10%"><i class="fa fa-cog" ></i></th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        @forelse($archivosImprimir as $archivo)
                                            <tr>
                                                <td>{{ $archivo->nombre }} {{ $archivo->nombre_archivo }}</td>
                                                <td>@if( $archivo->getTipoArchivo ){{ $archivo->getTipoArchivo->nombre }}@endif</td>

                                                <td>
                                                    <a class="btn btn-success btn-xs" @if( $archivo->cargado == 0 ) href="{{ asset( $archivo->ubicacion ) }}" @else href="http://{{ $archivo->ubicacion }}" @endif
                                                    title="Ver Archivo" target="_blank" ><i class="fa fa-eye"></i></a>
                                                </td>
                                            </tr>
                                            
                                        @empty

                                        @endforelse
                                            <tr>
                                                <td>Trazabilidad del Documento</td>
                                                <td>Generado por Sistema</td>
                                                <td>
                                                    <a class="btn btn-danger btn-xs" title="PDF Trazabilidad Dcto. Finanzas" target="_blank" 
                                                        href="{{ asset('documentos/generar/pdf_trazabilidad_documento/').'/'.$documento->id }}">
                                                        <i class="fas fa-file-pdf fa-lg"></i></a>
                                                </td>
                                            </tr>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>

            </div>{{-- /modal-body --}}

            {{--
            <div class="modal-footer">
                <div class="btn-group">
                <button type="button" class="btn btn-xs btn-default" title="Cancelar" data-dismiss="modal" aria-hidden="true">Cancelar</button>
                </div>
            </div> --}} {{-- /modal-footer --}}
          	
		</div>
	</div>
</div>

 <script type="text/javascript">
    var tablaItemsView = $('#tabla_item_presupuestario').DataTable({
        "language": {
            "emptyTable": "No hay datos disponibles en la tabla",
        },
        "paging":   false,
        "ordering": false,
        "info":     false,
        "searching": false
    });
 
    var tablaArchivoView = $('#tabla_info_archivo').DataTable({
         "language": {
            "emptyTable": "No hay datos disponibles en la tabla",
        },
        "paging":   false,
        "ordering": false,
        "info":     false,
        "searching": false
    });

 	$(document).ready(function(){
         
  
 	});

    
 </script>