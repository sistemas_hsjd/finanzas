<link href="{{ asset('sistema/css/modales.css') }} " rel="stylesheet" type="text/css"/>
<div class="modal fade" id="modalConectar" role="dialog" aria-labelledby="myModalLabel3" aria-hidden="true">
	<div class="modal-dialog" style="width:95%;">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" title="Cerrar" class="close" data-dismiss="modal" aria-hidden="true"></button>
                @if ($documento->id_relacion == null)
				    <h4 class="modal-title" ><strong><i class="fa fa-file-text"></i> <i class="fas fa-link"></i> <i class="fas fa-clipboard-list"></i>&nbsp;&nbsp;Conectar Documentos</strong></h4>
                @else
                    <h4 class="modal-title" ><strong><i class="fa fa-file-text"></i> <i class="fas fa-link"></i> <i class="fas fa-clipboard-list"></i>&nbsp;&nbsp;Documentos Conectados</strong></h4>
                @endif
            </div>

            <form action="{{ asset('documentos/conectar') }}" method="post" class="horizontal-form" id="form" autocomplete="off">
                <div class="modal-body">
                    <div class="note note-danger" id="divErrores" style="display:none;">
                        <h4 class="block" style="margin-bottom:5 px;">Debe Completar los siguientes campos: </h4>
                        <ul id="ulErrores"></ul>
                    </div>

                    <div class="form-group row" style="margin-bottom: 0px;">
                        <div class="col-xs-12 col-md-12">
                            <div class="form-group row">
                                <div class="col-xs-6 col-md-6">	
                                    <h4 class="headers-view text-center">
                                        <i class="fa fa-file-text"></i> Documento Ingresado
                                    </h4>
                                    <hr style="background-color: black;height: 2px;">
                                    <div class="form-group row">
                                        <div class="col-xs-12">	
                                            <h4 class="headers-view">
                                                Proveedor
                                            </h4>
                                            <hr>
                                            <div class="form-group">
                                                <label class="col-sm-3 control-label font-bold">Rut Proveedor</label>
                                                <label class="col-sm-9 control-label atencion">@if($documento->getProveedor){{ \Rut::parse($documento->getProveedor->rut)->format() }}@endif</label>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="form-group row">
                                        <div class="col-xs-12">
                                            <label class="col-xs-3 control-label font-bold">Nombre Proveedor</label>
                                            <label class="col-xs-9 control-label">@if($documento->getProveedor){{ $documento->getProveedor->nombre }}@endif</label>
                                        </div>
                                    </div>

                                    <div class="form-group row">
                                        <div class="col-xs-12">	
                                            <h4 class="headers-view">
                                                Documento
                                            </h4>
                                            <hr>
                                            <div class="form-group">
                                                
                                                {{-- Inicio Refacturación --}}
                                                @if ( $documento->id_relacionado != null )

                                                    <label class="col-sm-2 control-label font-bold">Tipo Documento</label>
                                                    <label class="col-sm-4 control-label atencion">{{ $documento->getTipoDocumento->nombre }}</label>

                                                    @if ( $documento->id_tipo_documento == 4 || $documento->id_tipo_documento == 5 || $documento->id_tipo_documento == 10 || $documento->id_tipo_documento == 11 )
                                                        <label class="col-sm-2 control-label font-bold">Documento Relacionado</label>
                                                        <label class="col-sm-4 control-label ">{{ $documento->id_relacionado }}</label>
                                                    @elseif ( $documento->id_tipo_documento > 0 )
                                                        <label class="col-sm-2 control-label font-bold">Reemplaza a</label>
                                                        <label class="col-sm-4 control-label ">{{ $documento->id_relacionado }}</label>
                                                    @endif
                                                @else
                                                    <label class="col-sm-3 control-label font-bold">Tipo Documento</label>
                                                    <label class="col-sm-9 control-label atencion">{{ $documento->getTipoDocumento->nombre }}</label>
                                                @endif
                                                {{-- Fin Refacturación --}}
                                            </div>
                                        </div>
                                    </div>

                                    <div class="form-group row">
                                        <div class="col-xs-12">
                                            <label class="col-sm-2 control-label font-bold">N° Documento</label>
                                            <label class="col-sm-4 control-label atencion">{{ $documento->numero_documento }}</label>
                                            <label class="col-sm-3 control-label font-bold">Modalidad de Compra</label>
                                            <label class="col-sm-3 control-label ">{{ $documento->getModalidadCompra->nombre }}</label>
                                        </div>
                                    </div>

                                    <div class="form-group row">
                                        <div class="col-xs-12">
                                            <label class="col-sm-3 control-label font-bold">Tipo de adjudicación</label>
                                            <label class="col-sm-9 control-label ">{{ $documento->getTipoAdjudicacion->nombre }}</label>
                                        </div>
                                    </div>

                                    <div class="form-group row">
                                        <div class="col-xs-12">
                                            <label class="col-sm-2 control-label font-bold">N° Documento Compra</label>
                                            <label class="col-sm-4 control-label ">{{ $documento->documento_compra }}</label>
                                            <label class="col-sm-2 control-label font-bold">Fecha de Documento</label>
                                            <label class="col-sm-4 control-label ">{{ fecha_dmY($documento->fecha_documento) }}</label>
                                        </div>
                                    </div>

                                    <div class="form-group row">
                                        <div class="col-xs-12">
                                            <label class="col-sm-2 control-label font-bold">Tipo de Informe</label>
                                            <label class="col-sm-4 control-label ">{{ $documento->getTipoInforme->nombre }}</label>
                                            <label class="col-sm-2 control-label font-bold">Responsable</label>
                                            <label class="col-sm-4 control-label ">{{ $documento->getResponsable->name }}</label>
                                        </div>
                                    </div>

                                    <div class="form-group row">
                                        <div class="col-xs-12">
                                            <label class="col-sm-2 control-label font-bold">Fecha de Recepción</label>
                                            <label class="col-sm-4 control-label ">{{ fecha_dmY($documento->fecha_recepcion) }}</label>
                                            
                                        </div>
                                    </div>

                                    <div class="form-group row">
                                        <div class="col-xs-12">
                                            <label class="col-sm-2 control-label font-bold">Valor Tot. Orig.</label>
                                            <label class="col-sm-4 control-label ">$ {{ formatoMiles($documento->total_documento) }}</label>
                                            <label class="col-sm-2 control-label font-bold">Valor Tot. Act.</label>
                                            <label class="col-sm-4 control-label atencion">$ {{ formatoMiles($documento->total_documento_actualizado) }}</label>
                                        </div>
                                    </div>

                                    <div class="form-group row">
                                        <div class="col-xs-12">
                                            @if ( $documento->getReferenteTecnico != null )
                                                <label class="col-sm-2 control-label font-bold">Referente Técnico</label>
                                                <label class="col-sm-4 control-label "><strong>{{ $documento->getReferenteTecnico->nombre }}</strong> {{ $documento->getReferenteTecnico->responsable }}</label>
                                            @endif
                                            <label class="col-sm-2 control-label font-bold">Fecha de Ingreso</label>
                                            <label class="col-sm-4 control-label ">{{ fecha_dmY($documento->fecha_ingreso)." ".fechaHoraMinutoSegundo($documento->fecha_ingreso) }}</label>
                                        </div>
                                    </div>

                                    {{-- Falta "Factoring" --}}
                                    @isset ($factoring)
                                        <div class="form-group row">
                                            <div class="col-xs-12">
                                                <label class="col-sm-2 control-label font-bold">Nombre Factoring</label>
                                                <label class="col-sm-4 control-label "></label>
                                                <label class="col-sm-2 control-label font-bold">Rut Factoring</label>
                                                <label class="col-sm-4 control-label "></label>
                                            </div>
                                        </div>
                                    @endisset

                                    @if ($documento->fecha_ingreso >= '2019-06-22 00:00:00' && $documento->validado == null)
                                        <div class="form-group row">
                                            <div class="col-xs-12">
                                                <div>
                                                    <label class="col-sm-3 control-label font-bold">Días Para Cuadratura</label>
                                                    <label class="col-sm-4 control-label "><i class="fas fa-clipboard-check fa-lg fa-bounce" style="color:{{ $documento->getColorPorDiasCuadratura() }} !important"></i> {{ diferencia_dias_cuadratura($documento->fecha_ingreso,date('Y-m-d')) }}</label>
                                                </div>
                                            </div>
                                        </div>
                                    @endif

                                    @if ($documento->id_relacion != null)
                                        <div class="form-group row">
                                            <div class="col-xs-12">
                                                <div>
                                                    <label class="col-sm-2 control-label font-bold">Fecha Conexión</label>
                                                    <label class="col-sm-4 control-label ">{{ fecha_dmY($documento->getUnaRelacionRecepcionValidada->created_at)." ".fechaHoraMinutoSegundo($documento->getUnaRelacionRecepcionValidada->created_at) }}</label>
                                                    <label class="col-sm-2 control-label font-bold">Responsable Conexión</label>
                                                    <label class="col-sm-4 control-label ">@if( $documento->getUnaRelacionRecepcionValidada->getUsuarioResponsable ){{ $documento->getUnaRelacionRecepcionValidada->getUsuarioResponsable->name }}@endif</label>
                                                </div>
                                            </div>
                                        </div>
                                    @endif

                                    <div class="form-group row">
                                        <div class="col-xs-12">
                                            <label class="col-sm-3 control-label font-bold">Observaciones Factura:</label>
                                            <label class="col-sm-9 control-label ">{{ $documento->observacion }}</label>
                                        </div>
                                    </div>

                                    <div class="form-group row">
                                        <div class="col-xs-12">	
                                            <h4 class="headers-view">
                                                Responsables
                                            </h4>
                                            <hr>
                                            <div class="form-group">
                                                <label class="col-sm-2 control-label font-bold">Ingresado por</label>
                                                <label class="col-sm-4 control-label">{{ $documento->getDigitador->name }}</label>
                                                @if ( $documento->getDevengador )
                                                    <label class="col-sm-2 control-label font-bold">Devengado por</label>
                                                    <label class="col-sm-4 control-label">{{ $documento->getDevengador->name }}</label>
                                                @endif
                                            </div>
                                        </div>
                                    </div>

                                    <div class="form-group row">
                                        <div class="col-xs-12">	
                                            <h4 class="headers-view">
                                                Documentos adjuntos
                                            </h4>
                                            <hr>
                                            <div class="form-group">
                                                <div class="col-xs-12 table-responsive">
                                                    <table class="table table-striped table-bordered table-hover " id="tabla_info_archivo">
                                                        <thead>
                                                            <tr>
                                                                <th width="40%">Nombre del Archivo</th>
                                                                <th width="30%">Tipo</th>
                                                                <th width="10%">Extensión</th>
                                                                <th width="10%">Peso</th>
                                                                <th width="10%"><i class="fa fa-cog" ></i></th>
                                                            </tr>
                                                        </thead>
                                                        <tbody>
                                                            @forelse($documento->getArchivos as $archivo)
                                                                <tr>
                                                                    <td>{{ $archivo->nombre }}</td>
                                                                    <td>@if( $archivo->getTipoArchivo ){{ $archivo->getTipoArchivo->nombre }}@endif</td>
                                                                    <td>{{ $archivo->extension }}</td>
                                                                    <td>{{ pesoArchivoEnMB($archivo->peso) }}</td>
                                                                    <td>
                                                                        <a class="btn btn-success btn-xs" @if($archivo->cargado == 0) href="{{ asset( $archivo->ubicacion ) }}" @else href="http://{{ $archivo->ubicacion }}" @endif
                                                                        title="Ver Archivo" target="_blank" ><i class="fa fa-eye"></i></a>
                                                                    </td>
                                                                </tr>
                                                            @empty

                                                            @endforelse
                                                        </tbody>
                                                    </table>
                                                </div>
                                            </div>
                                        </div>
                                    </div>

                                    @if ( $documento->getDocumentosRelacionados->count() > 0 )
                                        <div class="form-group row">
                                            <div class="col-xs-12">	
                                                <h4 class="headers-view">
                                                    Documentos Relacionados
                                                </h4>
                                                <hr>
                                                <div class="form-group">
                                                    <div class="col-xs-12 table-responsive">
                                                        <table class="table table-striped table-bordered table-hover " id="tabla_info_archivo">
                                                            <thead>
                                                                <tr>
                                                                    <th class="text-center">N° Doc.</th>
                                                                    <th >Tipo Doc.</th>
                                                                    <th class="text-right">Valor Total Doc.</th>
                                                                </tr>
                                                            </thead>
                                                            <tbody>
                                                                @foreach($documento->getDocumentosRelacionados as $doc)
                                                                    <tr>
                                                                        <td class="text-center">{{ $doc->numero_documento }}</td>
                                                                        <td>
                                                                            {{ $doc->getTipoDocumento->nombre }}
                                                                            @if ( $doc->getArchivos->count() > 0 )
                                                                                <div class="btn-group pull-right">
                                                                                    @foreach($doc->getArchivos as $archivo)
                                                                                        <a class="btn btn-info btn-xs " 
                                                                                            @if($archivo->cargado == 0) 
                                                                                                href="{{ asset( $archivo->ubicacion ) }}" 
                                                                                            @else
                                                                                                href="http://{{ $archivo->ubicacion }}"
                                                                                            @endif
                                                                                            title="Ver Archivo @if( $archivo->getTipoArchivo ){{ $archivo->getTipoArchivo->nombre }}@endif" 
                                                                                            target="_blank" ><i class="fa fa-eye"></i>
                                                                                        </a>
                                                                                    @endforeach
                                                                                </div>
                                                                            @endif
                                                                        </td>
                                                                        <td class="text-right">$ {{ formatoMiles($doc->total_documento) }}</td>
                                                                    </tr>
                                                                @endforeach
                                                                <tr>
                                                                    <td colspan="2" class="text-right"><strong>Sumatoria de NC y ND aplicada al Valor Tot. Orig.</strong></td>
                                                                    <td class="text-right">$ {{ formatoMiles($documento->montoAplicadoAlValorTotal()) }}</td>
                                                                </tr>
                                                            </tbody>
                                                        </table>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    @endif

                                    @if  ($documento->id_relacion == null && $documento->tieneLosArchivosNecesarios() )
                                        <div class="form-group row">
                                            <div class="input-group col-sm-10 col-sm-offset-1">
                                                <span class="input-group-addon" id="inputGroupFileAddon01"><strong>Seleccione Documento</strong></span>
                                                <input type="file" class="form-control" accept=".pdf" name="archivo" id="archivo" aria-describedby="inputGroupFileAddon01" onchange="validarArchivo();">
                                            </div>
                                        </div>

                                        <div class="form-group row">
                                            <div id="div_archivo" class="col-xs-12 table-responsive" style="display: none;">
                                                <table class="table table-striped table-bordered table-hover " id="tabla_info_archivo_edit">
                                                    <thead>
                                                        <tr>
                                                            <th width="40%">Nombre</th>
                                                            <th width="20%">Peso</th>
                                                            <th width="40%">Tipo</th>
                                                        </tr>
                                                    </thead>
                                                    <tbody>
                                                    </tbody>
                                                </table>
                                            </div>
                                        </div>
                                    @endif

                                    <div class="form-group row">
                                        <div class="col-xs-12">	
                                            <h4 class="headers-view">
                                                Ítem Presupuestario
                                            </h4>
                                            <hr>
                                            <div class="form-group">
                                                <div class="col-xs-12 table-responsive">
                                                    <table class="table table-striped table-bordered table-hover " id="tabla_item_presupuestario">
                                                        <thead>
                                                            <tr>
                                                                <th width="30%">Código</th>
                                                                <th width="50%">Descripción</th>
                                                                <th width="20%">Monto</th>
                                                            </tr>
                                                        </thead>
                                                        <tbody>
                                                            @forelse($documento->getDevengos as $devengo)
                                                                <tr>
                                                                    <td>{{ $devengo->getItemPresupuestario->codigo() }}</td>
                                                                    <td>{{ $devengo->getItemPresupuestario->clasificador_presupuestario }}</td>
                                                                    <td class="text-right">$ {{ formatoMiles($devengo->monto) }}</td>
                                                                </tr>
                                                            @empty

                                                            @endforelse
                                                        </tbody>
                                                    </table>
                                                </div>
                                            </div>
                                        </div>
                                    </div>

                                    @if ($documento->id_devengador != null)
                                        <div class="form-group row">
                                            <div class="col-xs-12">	
                                                <h4 class="headers-view">
                                                    Devengado
                                                </h4>
                                                <hr>
                                                <div class="form-group">
                                                    <label class="col-sm-2 control-label font-bold">ID SIGFE</label>
                                                    <label class="col-sm-4 control-label">{{ $documento->id_sigfe }}</label>
                                                    <label class="col-sm-2 control-label font-bold">Fecha ID SIGFE</label>
                                                    <label class="col-sm-4 control-label">{{ fecha_dmY($documento->fecha_sigfe) }}</label>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="form-group row">
                                            <div class="col-xs-12">
                                                <div>
                                                    <label class="col-sm-2 control-label font-bold">Detalles</label>
                                                    <label class="col-sm-4 control-label ">{{ $documento->detalle }}</label>
                                                    <label class="col-sm-2 control-label font-bold">Observaciones</label>
                                                    <label class="col-sm-4 control-label ">{{ $documento->observacion }}</label>
                                                </div>
                                            </div>
                                        </div>
                                    @endif

                                    @if ( is_object($archivoAcepta) )
                                        <div class="form-group row">
                                            <div class="col-xs-12">	
                                                <h4 class="headers-view">
                                                    Información Acepta
                                                </h4>
                                                <hr>
                                                <div class="form-group">
                                                    <label class="col-sm-2 control-label font-bold">Estado SII</label>
                                                    <label class="col-sm-6 control-label">{{ str_replace('_',' ',$archivoAcepta->estado_sii) }}</label>
                                                </div>
                                            </div>
                                        </div>
                                    @endif

                                </div> <!-- / div class col-xs-6 -->

                                
                                <div class="col-xs-6 col-md-6">	
                                    <h4 class="headers-view text-center">
                                        <i class="fas fa-clipboard-list"></i> Documento Recepción
                                    </h4>
                                    <hr style="background-color: black;height: 2px;">
                                    <div class="form-group row">
                                        <div class="col-xs-12">
                                            <h4 class="headers-view">
                                                Contrato
                                            </h4>
                                            <hr>
                                            <div class="form-group">
                                                <label class="col-sm-3 control-label font-bold">Rut Proveedor</label>
                                                <label class="col-sm-9 control-label atencion">@if ($wsDocumento->getWsOrdenCompra->getWsContrato){{ \Rut::parse($wsDocumento->getWsOrdenCompra->getWsContrato->rut_proveedor)->format() }}@endif</label>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="form-group row">
                                        <div class="col-xs-12">
                                            <label class="col-sm-3 control-label font-bold">Nombre Proveedor</label>
                                            <label class="col-sm-9 control-label">@if ($wsDocumento->getWsOrdenCompra->getWsContrato){{ $wsDocumento->getWsOrdenCompra->getWsContrato->nombre_proveedor }}@endif</label>
                                        </div>
                                    </div>

                                    @if ( $wsDocumento->getWsOrdenCompra->getWsContrato )

                                        @if ( $wsDocumento->getWsOrdenCompra->getWsContrato->fecha_ingreso_contrato != null )
                                            <div class="form-group row">
                                                <div class="col-xs-12">
                                                    <label class="col-sm-3 control-label font-bold">Fecha de Ingreso Contrato</label>
                                                    <label class="col-sm-2 control-label">
                                                            {{ fecha_dmY($wsDocumento->getWsOrdenCompra->getWsContrato->fecha_ingreso_contrato) }}
                                                    </label>
                                                </div>
                                            </div>
                                        @endif

                                        @if ( $wsDocumento->getWsOrdenCompra->getWsContrato->fecha_inicio_contrato != null || $wsDocumento->getWsOrdenCompra->getWsContrato->fecha_termino_contrato != null )
                                            <div class="form-group row">
                                                <div class="col-xs-12">
                                                    @if ( $wsDocumento->getWsOrdenCompra->getWsContrato->fecha_inicio_contrato != null )
                                                        <label class="col-sm-3 control-label font-bold">Fecha Inicio Contrato</label>
                                                        <label class="col-sm-2 control-label">
                                                            {{ fecha_dmY($wsDocumento->getWsOrdenCompra->getWsContrato->fecha_inicio_contrato) }}
                                                        </label>
                                                    @endif
                                                    @if ( $wsDocumento->getWsOrdenCompra->getWsContrato->fecha_termino_contrato != null )
                                                        <label class="col-sm-3 control-label font-bold">Fecha Termino Contrato</label>
                                                        <label class="col-sm-2 control-label">
                                                            {{ fecha_dmY($wsDocumento->getWsOrdenCompra->getWsContrato->fecha_termino_contrato) }}
                                                        </label>
                                                    @endif
                                                </div>
                                            </div>
                                        @endif

                                        @if ( $wsDocumento->getWsOrdenCompra->getWsContrato->resolucion != null || $wsDocumento->getWsOrdenCompra->getWsContrato->fecha_resolucion != null )
                                            <div class="form-group row">
                                                <div class="col-xs-12">
                                                    @if ( $wsDocumento->getWsOrdenCompra->getWsContrato->resolucion != null )
                                                        <label class="col-sm-3 control-label font-bold">N° Resolución</label>
                                                        <label class="col-sm-2 control-label">{{ $wsDocumento->getWsOrdenCompra->getWsContrato->resolucion }}</label>
                                                    @endif
                                                    
                                                    @if ( $wsDocumento->getWsOrdenCompra->getWsContrato->fecha_resolucion != null )
                                                        <label class="col-sm-3 control-label font-bold">Fecha Resolución</label>
                                                        <label class="col-sm-4 control-label">
                                                            {{ fecha_dmY($wsDocumento->getWsOrdenCompra->getWsContrato->fecha_resolucion) }}
                                                        </label>
                                                    @endif
                                                </div>
                                            </div>
                                        @endif

                                        @if ( $wsDocumento->getWsOrdenCompra->getWsContrato->monto_maximo != null )
                                            <div class="form-group row">
                                                <div class="col-xs-12">
                                                    <label class="col-sm-3 control-label font-bold">Monto Máximo Contratado</label>
                                                    <label class="col-sm-4 control-label">
                                                        $ {{ formatoMiles($wsDocumento->getWsOrdenCompra->getWsContrato->monto_maximo) }}
                                                    </label>
                                                </div>
                                            </div>
                                        @endif
                                    @endif

                                    <div class="form-group row">
                                        <div class="col-xs-12">
                                            <h4 class="headers-view">
                                                Documento
                                            </h4>
                                            <hr>
                                            <div class="form-group">
                                                <label class="col-sm-3 control-label font-bold">Tipo Documento</label>
                                                <label class="col-sm-9 control-label atencion">{{ $wsDocumento->tipo_documento }}</label>
                                            </div>
                                        </div>
                                    </div>


                                    <div class="form-group row">
                                        <div class="col-xs-12">
                                            <label class="col-sm-2 control-label font-bold">N° Documento</label>
                                            <label class="col-sm-4 control-label atencion">{{ $wsDocumento->documento }}</label>
                                        </div>
                                    </div>

                                    <div class="form-group row">
                                        <div class="col-xs-12">
                                            <label class="col-sm-2 control-label font-bold">Responsable</label>
                                            <label class="col-sm-9 control-label">{{ $wsDocumento->nombre_usuario_responsable }}</label>
                                        </div>
                                    </div>

                                    <div class="form-group row">
                                        <div class="col-xs-12">
                                            <label class="col-sm-2 control-label font-bold">Monto Neto</label>
                                            <label class="col-sm-4 control-label">$ {{ formatoMiles($wsDocumento->documento_neto) }}</label>
                                            <label class="col-sm-2 control-label font-bold">Monto IVA</label>
                                            <label class="col-sm-4 control-label">$ {{ formatoMiles($wsDocumento->documento_iva) }}</label>
                                        </div>
                                    </div>

                                    <div class="form-group row">
                                        <div class="col-xs-12">
                                            <label class="col-sm-2 control-label font-bold">Monto Descuento</label>
                                            <label class="col-sm-4 control-label">@if( $wsDocumento->documento_descuento_total != '' )$ {{ formatoMiles($wsDocumento->documento_descuento_total) }}@endif</label>
                                            <label class="col-sm-2 control-label font-bold">Monto Total</label>
                                            <label class="col-sm-4 control-label atencion">$ {{ formatoMiles($wsDocumento->documento_total) }}</label>
                                        </div>
                                    </div>

                                    <div class="form-group row">
                                        <div class="col-xs-12">
                                            <h4 class="headers-view">
                                                Orden de Compra
                                            </h4>
                                            <hr>
                                            <div class="form-group">
                                                <label class="col-sm-3 control-label font-bold">Fecha Recepción de O/C</label>
                                                <label class="col-sm-2 control-label">{{ fecha_dmY($wsDocumento->getWsOrdenCompra->fecha_recepcion_orden_compra) }}</label>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="form-group row">
                                        <div class="col-xs-12">
                                            <label class="col-sm-3 control-label font-bold">N° O/C</label>
                                            <label class="col-sm-2 control-label">{{ $wsDocumento->getWsOrdenCompra->numero_orden_compra_mercado_publico }}</label>
                                            <label class="col-sm-3 control-label font-bold">Fecha de O/C</label>
                                            <label class="col-sm-4 control-label">{{ fecha_dmY($wsDocumento->getWsOrdenCompra->fecha_orden_compra_mercado_publico) }}</label>
                                        </div>
                                    </div>

                                    <div class="form-group row">
                                        <div class="col-xs-12">
                                            <label class="col-sm-3 control-label font-bold">Monto Máximo O/C</label>
                                            <label class="col-sm-3 control-label">$ {{ formatoMiles($wsDocumento->getWsOrdenCompra->total_orden_compra) }}</label>
                                            @if ( $wsDocumento->getWsOrdenCompra->saldo_orden_compra != null )
                                                <label class="col-sm-2 control-label font-bold">Saldo O/C</label>
                                                <label class="col-sm-4 control-label">$ {{ formatoMiles($wsDocumento->getWsOrdenCompra->saldo_orden_compra) }}</label>
                                            @endif
                                        </div>
                                    </div>

                                    <div class="form-group row">
                                        <div class="col-xs-12">
                                            <h4 class="headers-view">
                                                Documentos adjuntos
                                            </h4>
                                            <hr>
                                            <div class="form-group">
                                                <div class="col-xs-12 table-responsive">
                                                    <table class="table table-striped table-bordered table-hover " id="tabla_info_archivo">
                                                        <thead>
                                                            <tr>
                                                                <th width="90%">Nombre del Archivo</th>
                                                                <th width="10%" class="text-center"><i class="fa fa-cog" ></i></th>
                                                            </tr>
                                                        </thead>
                                                        <tbody>
                                                            @forelse($wsDocumento->getWsArchivos as $archivo)
                                                                <tr>
                                                                    <td>{{ $archivo->nombre_archivo }}</td>
                                                                    <td>
                                                                        <a class="btn btn-success btn-xs" @if($archivo->cargado == 0) href="{{ asset( $archivo->ubicacion ) }}" @else href="http://{{ $archivo->ubicacion }}" @endif
                                                                        title="Ver Archivo" target="_blank" ><i class="fa fa-eye"></i></a>
                                                                    </td>
                                                                </tr>
                                                            @empty

                                                            @endforelse
                                                        </tbody>
                                                    </table>
                                                </div>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="form-group row">
                                        <div class="col-xs-12">	
                                            <h4 class="headers-view">
                                                Ítem Presupuestario
                                            </h4>
                                            <hr>
                                            <div class="form-group">
                                                <div class="col-xs-12 table-responsive">
                                                    <table class="table table-striped table-bordered table-hover " id="tabla_wsitem_presupuestario">
                                                        <thead>
                                                            <tr>
                                                                <th width="30%">Código</th>
                                                                <th width="40%">Descripción Producto</th>
                                                                <th width="30%">Monto</th>
                                                            </tr>
                                                        </thead>
                                                        <tbody>
                                                            @forelse($wsDocumento->getWsItemsPresupuestarios as $item)
                                                                <tr>
                                                                    <td>{{ $item->item_presupuestario }}</td>
                                                                    <td>{{ $item->descripcion_articulo }}</td>
                                                                    <td class="text-right">$ {{ formatoMiles( $item->cantidad_recepcionada * $item->valor_item_recepcionado ) }}</td>
                                                                </tr>
                                                            @empty

                                                            @endforelse
                                                            <tr>
                                                                <td class="text-right" colspan="2"><strong>Descuento</strong></td>
                                                                <td class="text-right">@if( $wsDocumento->documento_descuento_total != '' )$ {{ formatoMiles( $wsDocumento->documento_descuento_total ) }}@endif</td>
                                                            </tr>
                                                            <tr>
                                                                <td class="text-right" colspan="2"><strong>Neto</strong></td>
                                                                <td class="text-right">$ {{ formatoMiles( $wsDocumento->documento_neto ) }}</td>
                                                            </tr>
                                                            <tr>
                                                                <td class="text-right" colspan="2"><strong>IVA</strong></td>
                                                                <td class="text-right">$ {{ formatoMiles( $wsDocumento->documento_iva ) }}</td>
                                                            </tr>
                                                            <tr>
                                                                <td class="text-right" colspan="2"><strong>Valor Total</strong></td>
                                                                <td class="text-right">$ {{ formatoMiles( $wsDocumento->documento_total ) }}</td>
                                                            </tr>
                                                        </tbody>
                                                    </table>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div> <!-- / div class col-xs-6 -->
                            </div><!--formgroup-->
                        </div> <!--col-xs-12-->
                    </div>

                @if ( !$documento->tieneLosArchivosNecesarios() )
                    <h4 class="headers-view">Para realizar la conexión: Es necesario que el documento tenga el archivo adjuntos de Factura.</h4>
                @endif
                </div> {{-- /modal-body --}}

            
                <input type="hidden" name="_token" value="{{ csrf_token() }}">
                <input type="hidden" name="idDocumento" value="{{ $documento->id }}">
                <input type="hidden" name="idWsDocumento" value="{{ $wsDocumento->id }}">
                <div class="modal-footer form-actions right">
                    @if ($documento->id_relacion == null)
                        <button type="button" class="btn btn-default" title="Cancelar" data-dismiss="modal" aria-hidden="true">Cancelar</button>
                        @if ( $documento->tieneLosArchivosNecesarios() )
                            <button type="submit" title="Conectar Documentos" class="btn btn-success" id="botonAccion"><i class="far fa-save fa-lg" style="color: black;"></i> <i class="fas fa-link fa-lg" style="color:black;"></i> Conectar Documentos</button>
                        @endif
                    @else
                        <button type="button" class="btn btn-default" title="Cerrar" data-dismiss="modal" aria-hidden="true">Cerrar</button>
                    @endif
                </div> {{-- /modal-footer --}}
            </form>
          	
		</div>
	</div>
</div>

 <script type="text/javascript">

    var selectTipoArchivo = '<select name="tipo_archivo" id="tipo_archivo" class="col-xs-12" required>';
    selectTipoArchivo += '<option value="">Seleccione</option>';
    @foreach ( $tiposArchivo as $tipoArchivo)
        selectTipoArchivo += '<option value="{{ $tipoArchivo->id }}">{{ $tipoArchivo->nombre }}</option>';
    @endforeach
    selectTipoArchivo += '</select>';
    var tablaItems = $('#tabla_item_presupuestario').DataTable({
        "language": {
            "emptyTable": "No hay datos disponibles en la tabla",
        },
        "paging":   false,
        "ordering": false,
        "info":     false,
        "searching": false
    });

    var tablaArchivo = $('#tabla_info_archivo_edit').DataTable({
        "paging":   false,
        "ordering": false,
        "info":     false,
        "searching": false
    });
 
    var tablaArchivoExistentes = $('#tabla_info_archivo').DataTable({
         "language": {
            "emptyTable": "No hay datos disponibles en la tabla",
        },
        "paging":   false,
        "ordering": false,
        "info":     false,
        "searching": false
    });

 	$(document).ready(function(){
         $("#form").validate({
            highlight: function(element) {
                $(element).closest('.form-validate').removeClass('has-success');
                $(element).closest('.form-validate').addClass('has-error');
            },
            unhighlight: function(element) {
                $(element).closest('.form-validate').removeClass('has-error');
                $(element).closest('.form-validate').addClass('has-success');
            },
            errorElement: 'span',
            errorClass: 'help-block',
            errorPlacement: function (error, element) {
                console.log(element);
                if (element.parent('.input-group').length) {
                    error.insertAfter(element.parent());
                } else {
                    error.insertAfter(element);
                }
            },
            rules: {
            },
            messages: {
            },

            //para enviar el formulario por ajax
            submitHandler: function(form) {
                esperandoAccion();
                
                let formData = new FormData(form);
                $.ajax({
                    url: form.action,
                    type: form.method,
                    //data: $(form).serialize(),
                    data: formData,
                    processData: false,
                    contentType: false,
                    success: function(respuesta) {
                        console.log(respuesta);
                        if ( respuesta.estadoDocumento == 'error' ) {
                            toastr.error('No es posible realizar la acción'+'<br><strong>Error: '+ respuesta.mensaje_documento +'</strong>', 'Atención', optionsToastr);
                        } else if ( respuesta.estadoDocumento == 'success') {
                            toastr.success(respuesta.mensajeDocumento, 'Atención', optionsToastr);
                            // Quitar Documento de la lista
                            quitarElementoTabla(respuesta);
                            $("#modalConectar").modal("hide");

                            if ( respuesta.estado_archivo == 'error' ) {
                                toastr.error('Archivo: No es posible realizar la acción'+'<br><strong>Error: '+ respuesta.mensaje_archivo +'</strong>', 'Atención', optionsToastr);
                            } else if ( respuesta.estado_archivo == 'succes') {
                                toastr.success(respuesta.mensaje_archivo, 'Atención', optionsToastr);
                            }
                        }
                    }            
                }).fail( function(respuesta) {//fail ajax
                    if ( respuesta.status == 400 ) {
                        mostrarErroresValidator(respuesta);
                    } else if ( respuesta.status == 500 ) {
                        toastr.error('No es posible realizar la acción, error en el servidor', 'Atención', optionsToastr);
                    } else {
                        toastr.error('No es posible realizar la acción', 'Atención', optionsToastr);
                    }
                })
                .always(function() {
                    listoAccion();
                });//ajax

            }

        });
  
 	});

     function esperandoAccion()
    {   
        $('.page-header-fixed *').css('cursor', 'wait');
        $("#botonAccion").attr("disabled",true);
    }

    function listoAccion()
    {
        $('.page-header-fixed *').css('cursor', '');
        $("#botonAccion").attr("disabled",false);
    }

    function mostrarErroresValidator(respuesta) {
        if (respuesta.responseJSON) {
            //console.log(respuesta.responseJSON);
            let htmlErrores = '';
            for (let k in respuesta.responseJSON) {
                //console.log(k, respuesta.responseJSON[k]);
                htmlErrores += '<li>' + respuesta.responseJSON[k] + '</li>';
            }

            $('#ulErrores').html(htmlErrores);
            $('#divErrores').css('display', '');
            toastr.error('No es posible realizar la acción' + '<br>' + 'Errores:<br> <ul>' + htmlErrores + '</ul>', 'Atención', optionsToastr);
        }
    }

    function quitarElementoTabla(respuesta)
    {
        $('#tr_' + respuesta.id).fadeOut(400, function(){
            tablaPrincipal.row('#tr_' + respuesta.id ).remove().draw();
        });
    }

    /**
    * Valida el archivo seleccionado para subir
    */
    function validarArchivo() {
        // console.log("validando archivo");
        let archivo = $('#archivo').val().replace(/.*(\/|\\)/, ''); //val();
        let extension = archivo.substring(archivo.lastIndexOf('.'));
        if (archivo == '') {
            $('#div_archivo').css('display', 'none');
            tablaArchivo.clear().draw(); // se remueven todas las filas de la tabla
            return false;
        } else {
            if (extension != '.pdf') {
                toastr.error('El archivo no tiene la extension correcta: ' + '<strong>PDF</strong>', 'Atención', optionsToastr);
                $('#archivo').val('');
                $('#div_archivo').css('display', 'none');
                tablaArchivo.clear().draw(); // se remueven todas las filas de la tabla
                return false;
            } else {
                mostrarInfoArchivo(archivo);
                return true;
            }
        }
    }

    /**
    * Muestra la informacion del archivo seleccionado para subir
    */
    function mostrarInfoArchivo(archivo) {
        tablaArchivo.clear().draw(); // se remueven todas las filas de la tabla
        let nombreArchivo = archivo.substring(0, archivo.lastIndexOf('.'));
        let tamañoArchivo = $('#archivo')[0].files[0].size;
        /*
        let selectTipoArchivo = '<select name="tipo_archivo" id="tipo_archivo" class="col-xs-12" required>';
        selectTipoArchivo += '<option value="">Seleccione</option>';
        @foreach ( $tiposArchivo as $tipoArchivo)
            selectTipoArchivo += '<option value="{{ $tipoArchivo->id }}">{{ $tipoArchivo->nombre }}</option>';
        @endforeach
        selectTipoArchivo += '</select>';
        */

        tablaArchivo.row.add([
            nombreArchivo,
            tamañoArchivo + " bits",
            selectTipoArchivo
        ]).draw(false);

        $('#div_archivo').css('display', '');
    }

    
 </script>