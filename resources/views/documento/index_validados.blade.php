@extends('master')

@section('title', 'Documentos Validados')

@push('css-head')
<!-- BEGIN PAGE LEVEL STYLES -->
<link rel="stylesheet" type="text/css" href="{{asset('assets/plugins/select2/select2_conquer.css')}}"/>
<link rel="stylesheet" href="{{asset('assets/plugins/data-tables/DT_bootstrap.css')}}"/>
<!-- END PAGE LEVEL STYLES -->

<style>
    .label-form {
        font-weight: bold;
        color: black;
        text-align: right;
    }
</style>

@endpush

@section('content')

<!-- BEGIN CONTENT -->

<div class="page-content-wrapper">
	<div class="page-content-wrapper">
		<div class="page-content">

			<!-- BEGIN PAGE CONTENT-->
			<div class="row">
				<div class="col-md-12">
					<!-- BEGIN EXAMPLE TABLE PORTLET-->
					<div class="portlet">
						<div class="portlet-title">
							<div class="caption">
								<i class="fa fa-files-o" style="color: black;"></i> <i class="fas fa-thumbs-up"></i> Documentos Validados
							</div>
						</div>
						<div class="portlet-body">
                            <form action="{{url('documentos/listado/validados/filtrar')}}" method="post" enctype="multipart/form-data" id="filtros-docs" class="form-horizontal" autocomplete="off">
                                <h4 class="form-section" style="color: #69aa46;"><strong>Filtros</strong>
                                <div class="btn-group pull-right">
                                    
                                    <button class="btn btn-success btn-xs" title="Generar Excel" formaction="{{ url('documentos/listado/validados/excel') }}">
                                        <i class="fas fa-file-excel"></i> Exportar a Excel
                                    </button>

                                    <button class="btn btn-info btn-xs" title="Filtrar Listado"  type="sumbit">
                                        <i class="fas fa-list-alt"></i> Filtrar
                                    </button>
                                </div>
                                </h4>
                                <div class="form-group row">
                                    <label for="filtro_fecha_inicio" class="col-sm-1 col-sm-offset-4 control-label label-form">Fecha Inicio</label>
                                    <div class="col-sm-3">
                                        <div class="input-group">
                                            <input type="text" class="form-control fechas" id="filtro_fecha_inicio" name="filtro_fecha_inicio" @if(isset($filtroFechaInicio)) value="{{ $filtroFechaInicio }}" @else value="{{ date('d/m/Y') }}" @endif >
                                            <span class="input-group-addon" onclick="fechaDocumento('filtro_fecha_inicio');" style="cursor:pointer;"><i class="fa fa-calendar" aria-hidden="true" style="color: black;"></i></span>
                                        </div>
                                    </div>

                                    <label for="filtro_fecha_termino" class="col-sm-1 control-label label-form">Fecha Termino</label>
                                    <div class="col-sm-3">
                                        <div class="input-group">
                                            <input type="text" class="form-control fechas" id="filtro_fecha_termino" name="filtro_fecha_termino" @if(isset($filtroFechaTermino)) value="{{ $filtroFechaTermino }}"  @else value="{{ date("d/m/Y") }}" @endif>
                                            <span class="input-group-addon" onclick="fechaDocumento('filtro_fecha_termino');" style="cursor:pointer;"><i class="fa fa-calendar" aria-hidden="true" style="color: black;"></i></span>
                                        </div>
                                    </div>

                                </div>

                                <div class="form-group row">

                                    <label for="filtro_proveedor" class="col-sm-1 control-label label-form">Proveedor</label>
                                    <div class="col-sm-3">
                                        <select name="filtro_proveedor[]" id="filtro_proveedor" class="form-control select2_filtro" multiple style="width: 100%">
                                            @foreach ( $proveedores as $proveedor )
                                                <option value="{{ $proveedor->id }}" >{{ $proveedor->rut }} {{ $proveedor->nombre }}</option>
                                            @endforeach
                                        </select>
                                    </div>

                                    <label for="filtro_tipo_documento" class="col-sm-1 control-label label-form">Tipo de Documento</label>
                                    <div class="col-sm-3">
                                        <select name="filtro_tipo_documento[]" id="filtro_tipo_documento" class="form-control select2_filtro" multiple style="width: 100%">
                                            @foreach ( $tiposDocumento as $tipoDocumento )
                                                <option value="{{ $tipoDocumento->id }}" >{{ $tipoDocumento->nombre }}</option>
                                            @endforeach
                                        </select>
                                    </div>

                                    <label for="filtro_modalidad_compra" class="col-sm-1 control-label label-form">Modalidad de Compra</label>
                                    <div class="col-sm-3">
                                        <select name="filtro_modalidad_compra[]" id="filtro_modalidad_compra" class="form-control select2_filtro" multiple="multiple">
                                            @foreach ( $modalidadesCompra as $modalidadCompra )
                                                <option value="{{ $modalidadCompra->id }}" >{{ $modalidadCompra->nombre }}</option>
                                            @endforeach
                                        </select>
                                    </div>

                                </div>

                                <div class="form-group row">

                                    <label for="filtro_tipo_adjudicacion" class="col-sm-1 control-label label-form">Tipo de Adjudicación</label>
                                    <div class="col-sm-3">
                                        <select name="filtro_tipo_adjudicacion[]" id="filtro_tipo_adjudicacion" class="form-control select2_filtro" multiple="multiple">
                                            @foreach ( $tiposAdjudicacion as $tipoAdjudicacion )
                                                <option value="{{ $tipoAdjudicacion->id }}" >{{ $tipoAdjudicacion->nombre }}</option>
                                            @endforeach
                                        </select>
                                    </div>

                                    <label for="filtro_tipo_informe" class="col-sm-1 control-label label-form">Tipo de Informe</label>
                                    <div class="col-sm-3">
                                        <select name="filtro_tipo_informe[]" id="filtro_tipo_informe" class="form-control select2_filtro" multiple="multiple">
                                            @foreach ( $tiposInforme as $tipoInforme )
                                                <option value="{{ $tipoInforme->id }}" >{{ $tipoInforme->nombre }}</option>
                                            @endforeach
                                        </select>
                                    </div>

                                    <label for="filtro_responsable" class="col-sm-1 control-label label-form">Responsable</label>
                                    <div class="col-sm-3">
                                        <select name="filtro_responsable[]" id="filtro_responsable" class="form-control select2_filtro" multiple="multiple">
                                            @foreach ( $usuariosResponsables as $responsable )
                                                <option value="{{ $responsable->id }}" >{{ $responsable->name }}</option>
                                            @endforeach
                                        </select>
                                    </div>

                                </div>

                                <div class="form-group row">
                                    <label for="filtro_referente_tecnico" class="col-sm-1 control-label label-form">Referente Técnico</label>
                                    <div class="col-sm-3">
                                        <select name="filtro_referente_tecnico[]" id="filtro_referente_tecnico" class="form-control select2_filtro" multiple="multiple">
                                            @foreach ( $referentesTecnicos as $referenteTecnico )
                                                <option value="{{ $referenteTecnico->id }}" >{{ $referenteTecnico->nombre }} - {{ $referenteTecnico->responsable }}</option>
                                            @endforeach
                                        </select>
                                    </div>

                                    <label for="filtro_digitador" class="col-sm-1 control-label label-form">Digitador</label>
                                    <div class="col-sm-3">
                                        <select name="filtro_digitador[]" id="filtro_digitador" class="form-control select2_filtro" multiple="multiple">
                                            @foreach ( $usuariosDigitadores as $digitador )
                                                <option value="{{ $digitador->id }}" >{{ $digitador->name }}</option>
                                            @endforeach
                                        </select>
                                    </div>

                                    <label for="filtro_item_presupuestario" class="col-sm-1 control-label label-form">Ítem {{--Presupuestario--}}</label>
                                    <div class="col-sm-3">
                                        <select class="form-control select2_filtro " id="filtro_item_presupuestario" name="filtro_item_presupuestario[]" multiple="multiple" >
                                            @foreach ( $itemsPresupuestarios as $item )
                                                <option value="{{ $item->id }}" >{{ $item->codigo() }} {{ $item->clasificador_presupuestario }}</option>
                                            @endforeach
                                        </select>
                                    </div>

                                    

                                    {{-- 
                                    <label for="num_informe" class="col-sm-1 control-label label-form">Número de Informe</label>
                                    <div class="col-sm-3">
                                        <input type="text" class="form-control " id="num_informe" name="num_informe"  placeholder="Solo para exportar Informe" >
                                    </div>

                                    <label for="nombre_informe" class="col-sm-1 control-label label-form">Nombre de Informe</label>
                                    <div class="col-sm-3">
                                        <input type="text" class="form-control " id="nombre_informe" name="nombre_informe"  >
                                    </div>
                                    --}}
                                </div>

                                <div class="form-group row">
                                    <label for="filtro_numero_documento" class="col-sm-1 control-label label-form">N° Documento</label>
                                    <div class="col-sm-3">
                                        <input type="text" class="form-control " id="filtro_numero_documento" name="filtro_numero_documento"
                                        @if(isset($filtroNumeroDocumento)) value="{{ $filtroNumeroDocumento }}"  @endif >
                                    </div>

                                    <label for="filtro_numero_documento_compra" class="col-sm-1 control-label label-form">N° Documento Compra</label>
                                    <div class="col-sm-3">
                                        <input type="text" class="form-control " id="filtro_numero_documento_compra" name="filtro_numero_documento_compra"
                                        @if(isset($filtroNumeroDocumentoCompra)) value="{{ $filtroNumeroDocumentoCompra }}"  @endif>
                                    </div>

                                    <label for="filtro_cuadratura" class="col-sm-1 control-label label-form">Responsonsable Cuadratura</label>
                                    <div class="col-sm-3">
                                        <select name="filtro_cuadratura[]" id="filtro_cuadratura" class="form-control select2_filtro" multiple="multiple">
                                            @foreach ( $usuariosCuadratura as $cuadratura )
                                                <option value="{{ $cuadratura->id }}" >{{ $cuadratura->name }}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                </div>
                                
                            </form>
                            <div class="table-toolbar">
                                    
                            </div>
                            <div class="row">
                                <div class="col-md-12">
                                    <div class="panel">
                                    {{-- 
                                        <div class="panel-heading">
                                            <div class="caption">
                                                <strong style="color: #69aa46;">Últimos Documentos Ingresados</strong>
                                            </div>
                                        </div>
                                        --}}
                                        <div class="panel-body">
                                            <div class="table-responsive">
                                                <table class="table table-striped table-bordered table-hover table-full-width" id="tabla_documentos">
                                                    <thead>
                                                    <tr>
                                                        <th>Rut Proveedor</th>
                                                        <th>Nombre Proveedor</th>
                                                        <th >Tipo Documento</th>
                                                        <th >N° Documento</th>
                                                        <th >N° Documento Compra</th>
                                                        <th >Fecha RD</th>
                                                        <th >Fecha E</th>
                                                        <th>Ítem</th>
                                                        <th >Tot. Orig.</th>
                                                        <th >Tot. Act.</th>
                                                        <th class="text-center">
                                                            <i class="fa fa-cog"></i>
                                                        </th>
                                                    </tr>
                                                    </thead>
                                                    <tbody>
                                                    @forelse($documentos as $documento)
                                                        <tr id="tr_{{ $documento->id }}">
                                                            <td>@if($documento->getProveedor){{ $documento->getProveedor->rut }}@endif</td>
                                                            <td>@if($documento->getProveedor){{ $documento->getProveedor->nombre }}@endif</td>
                                                            <td>{{ $documento->getTipoDocumento->nombre }}</td>
                                                            <td>{{ $documento->numero_documento }}</td>
                                                            <td>{{ $documento->documento_compra }}</td>
                                                            <td class="text-center">{{ fecha_dmY($documento->fecha_recepcion) }}</td>
                                                            <td class="text-center">{{ fecha_dmY($documento->fecha_documento) }}</td>
                                                            <td>
                                                                @forelse ( $documento->getDevengos as $devengo)
                                                                    {{ $devengo->getItemPresupuestario->codigo() }}
                                                                @empty

                                                                @endforelse
                                                            </td>
                                                            <td class="text-right">$ {{ formatoMiles($documento->total_documento) }}</td>
                                                            <td class="text-right">$ {{ formatoMiles($documento->total_documento_actualizado) }}</td>
                                                            <td>
                                                                <div class="btn-group">
                                                                    {{-- 
                                                                    <button class="btn btn-success btn-xs" title="Ver Documento" onclick="verDocumento({{ $documento->id }});">
                                                                        <i class="fa fa-eye"></i>
                                                                    </button>
                                                                    --}}
                                                                    @if ($documento->validado == 1)
                                                                        @permission(['invalidar-documento'])
                                                                        <button class="btn btn-danger btn-xs" title="Invalidar Documento" onclick="invalidar({{ $documento->id }});">
                                                                            <i class="fas fa-thumbs-down "></i>
                                                                        </button>
                                                                        @endpermission
                                                                    @endif

                                                                    {{-- @if ( count($documento->getRelacionRecepcionesValidadas) > 0)
                                                                        @permission(['desvincular-documentos-con-recepcion'])
                                                                        @if ( $documento->getRelacionGuiasDespacho() )
                                                                            <button type="button" class="btn btn-danger btn-xs" title="Desvincular Documentos" 
                                                                                onclick="desconectarDocumentos({{ $documento->id }});">
                                                                                <i class="fas fa-unlink"></i>
                                                                            </button>
                                                                        @else
                                                                            <button type="button" class="btn btn-danger btn-xs" title="Desvincular Documentos" 
                                                                                onclick="desconectarDocumentos({{ $documento->id }},{{ $documento->getUnaRelacionRecepcionValidada->getWsDocumento->id }});">
                                                                                <i class="fas fa-unlink"></i>
                                                                            </button>
                                                                        @endif
                                                                        @endpermission
                                                                    @endif --}}

                                                                </div>
                                                            </td>
                                                        </tr>
                                                    @empty

                                                    @endforelse
                                                    </tbody>
                                                </table>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
						</div>
					</div>
					<!-- END EXAMPLE TABLE PORTLET-->
				</div>
			</div>
			<!-- END PAGE CONTENT-->
			
			
			
			
		</div> <!--/div class="page-content" -->
	</div> <!--/div class="page-content-wrapper" -->
</div><!--/div class="page-content-wrapper" -->
<!-- END CONTENT -->
<div id="modal"></div>
<div id="modal2"></div>
@endsection

@push('script-footer')
<!-- BEGIN PAGE LEVEL PLUGINS -->
<script type="text/javascript" src="{{ asset('assets/plugins/select2/select2.min.js') }} "></script>
{{--<script type="text/javascript" src="{{ asset('assets/plugins/data-tables/jquery.dataTables.min.js') }}"></script>--}}
<script type="text/javascript" src="{{ asset('datatables/jquery.dataTables.min.js') }}"></script>
<script type="text/javascript" src="{{ asset('assets/plugins/data-tables/DT_bootstrap.js') }}"></script>

<link rel="stylesheet" type="text/css" href="{{asset('assets/plugins/bootstrap-datepicker/css/datepicker.css')}}"/>

<script type="text/javascript" src="{{asset('assets/plugins/bootstrap-datepicker/js/bootstrap-datepicker.js')}}"></script>
<script type="text/javascript" src="{{asset('assets/plugins/bootstrap-datetimepicker/js/bootstrap-datetimepicker.js')}}"></script>


<!-- END PAGE LEVEL PLUGINS -->
<!-- BEGIN PAGE LEVEL SCRIPTS -->
<script src="{{ asset('assets/scripts/app.js') }} "></script>

<script type="text/javascript">
    let tablaPrincipal = $('#tabla_documentos').DataTable({
        "language": {
            "emptyTable":     "No hay datos disponibles en la tabla",
            "info":           "Mostrando registros del _START_ al _END_ de un total de _TOTAL_ registros",
            "infoEmpty":      "Showing 0 to 0 of 0 entries",
            "infoFiltered":   "(filtrado de un total de _MAX_ registros)",
            "infoPostFix":    "",
            "thousands":      ",",
            "lengthMenu":     "Mostrar _MENU_ registros",
            "loadingRecords": "Cargando...",
            "processing":     "Procesando...",
            "search":         "Buscar: ",
            "zeroRecords":    "No matching records found",
            "paginate": {
                "first":      "Primero",
                "last":       "Último",
                "next":       "Siguiente",
                "previous":   "Anterior"
            },
            "aria": {
                "sortAscending":  ": Activar para ordenar la columna de manera ascendente",
                "sortDescending": ": Activar para ordenar la columna de manera descendente"
            }
        },
        "lengthMenu": [
            [10, 15, 20, -1],
            [10, 15, 20, "Todos"] // change per page values here
        ],
        // set the initial value
        "displayLength": 10,
        "paging":   true,
        "ordering": true,
        "info":     true,
        "searching": true
    });

    jQuery('#tabla_documentos_filter input').addClass("form-control input-large"); // modify table search input
    jQuery('#tabla_documentos_wrapper .dataTables_length select').addClass("form-control input-small"); // modify table per page dropdown
    jQuery('#tabla_documentos_wrapper .dataTables_length select').select2(); // initialize select2 dropdown

    $('#tabla_documentos_column_toggler input[type="checkbox"]').change(function() {
        /* Get the DataTables object again - this is not a recreation, just a get of the object */
        var iCol = parseInt($(this).attr("data-column"));
        var bVis = oTable.fnSettings().aoColumns[iCol].bVisible;
        oTable.fnSetColumnVis(iCol, (bVis ? false : true));
    });

	$(document).ready(function() {
		App.init(); // initlayout and core plugins

   		$("#documentos").addClass( "active" );
		// $("#listar-li").addClass( "active" );
		$("#documentos-a").append( '<span class="selected"></span>' );
        $("#listado").addClass( "active" );
        $("#documentos-validados-li").addClass( "active" );
        $("#listado-a").append( '<span class="selected"></span>' );

        $('#filtro_fecha_inicio').datepicker({
            format: 'dd/mm/yyyy',
            endDate: new Date(),
            startDate: '22/06/2019',
            autoclose: true,
            language: 'es'
        });

        $('#filtro_fecha_termino').datepicker({
            format: 'dd/mm/yyyy',
            //endDate: new Date(),
            //"setDate": new Date(),
            //startDate: new Date(),            
            autoclose: true,
            language: 'es'
        });

        $(".select2_filtro").select2({
            allowClear: true,
        });

        /*
        $("#filtro_proveedor").select2({
            multiple: true,
            ajax: {
                cache: true,
                allowClear: true,
                //hidden : true,
                url : '{{ url('buscador_proveedor/') }}',
                dataType: 'json',
                delay: 250,
                data: function (params,page) {
                    //console.log("soy params : "+params);
                    //console.log("soy page : "+page);
                    var query = {
                        term: params,
                        page: params.page || 1
                    }
                    return query;
                },
                results: function (data) {
                    return {
                    results: data
                    };
                },
            },
            minimumInputLength: 2,
        
        });
        */
        @isset($filtroProveedor)
            let Aux1 = new Array();
            @foreach ($filtroProveedor as $proveedor)
                Aux1.push('{{ $proveedor }}');
            @endforeach
            $('#filtro_proveedor').select2('val',Aux1);
        @endisset

        @isset($filtroTipoDocumento)
            let Aux2 = new Array();
            @foreach ($filtroTipoDocumento as $tipoDoc)
                Aux2.push('{{ $tipoDoc }}');
            @endforeach
            $('#filtro_tipo_documento').select2('val',Aux2);
        @endisset

        @isset($filtroModalidadCompra)
            let Aux3 = new Array();
            @foreach ($filtroModalidadCompra as $modalidad)
                Aux3.push('{{ $modalidad }}');
            @endforeach
            $('#filtro_modalidad_compra').select2('val',Aux3);
        @endisset

        @isset($filtroTipoAdjudicacion)
            let Aux4 = new Array();
            @foreach ($filtroTipoAdjudicacion as $adjudicacion)
                Aux4.push('{{ $adjudicacion }}');
            @endforeach
            $('#filtro_tipo_adjudicacion').select2('val',Aux4);
        @endisset

         @isset($filtroTipoInforme)
            let Aux5 = new Array();
            @foreach ($filtroTipoInforme as $informe)
                Aux5.push('{{ $informe }}');
            @endforeach
            $('#filtro_tipo_informe').select2('val',Aux5);
        @endisset

        @isset($filtroResponsable)
            let Aux6 = new Array();
            @foreach ($filtroResponsable as $responsable)
                Aux6.push('{{ $responsable }}');
            @endforeach
            $('#filtro_responsable').select2('val',Aux6);
        @endisset

        @isset($filtroReferenteTecnico)
            let Aux7 = new Array();
            @foreach ($filtroReferenteTecnico as $referente)
                Aux7.push('{{ $referente }}');
            @endforeach
            $('#filtro_referente_tecnico').select2('val',Aux7);
        @endisset

        @isset($filtroDigitador)
            let Aux8 = new Array();
            @foreach ($filtroDigitador as $digitador)
                Aux8.push('{{ $digitador }}');
            @endforeach
            $('#filtro_digitador').select2('val',Aux8);
        @endisset

        @isset($filtroItems)
            let Aux9 = new Array();
            @foreach ($filtroItems as $item)
                Aux9.push('{{ $item }}');
            @endforeach
            $('#filtro_item_presupuestario').select2('val',Aux9);
        @endisset

        @isset($filtroCuadratura)
            let Aux10 = new Array();
            @foreach ($filtroCuadratura as $cuadratura)
                Aux10.push('{{ $cuadratura }}');
            @endforeach
            $('#filtro_cuadratura').select2('val',Aux10);
        @endisset
	});

    function fechaDocumento(id_fecha) {
        $('#' + id_fecha).datepicker("show");
    }

    function verDocumento(id)
    {
        $.get( '{{ url("documentos/modal/ver") }}/' + id, function( data ) {
            $( "#modal" ).html( data );
            $( "#modalVer" ).modal();
        });
    }

    function invalidar(id)
    {

        $('.page-header-fixed *').css('cursor', 'wait');
        $.ajax({
            type: "GET",
            url: '{{ url("documentos/modal/editar") }}/' + id +'/validados',
            success: function(respuesta) {

                $( "#modal" ).html( respuesta );
                $('.page-header-fixed *').css('cursor', '');
                $( "#modalEditar" ).modal();
               
            }
        });
        
    }

    function eliminarDocumento(id)
    {
        $.get( '{{ url("documentos/modal/eliminar") }}/' + id, function( data ) {
            $( "#modal" ).html( data );
            $( "#modalEliminar" ).modal();
        });
    }

    function desconectarDocumentos(idDocumento,idWsDocumento = null)
    {
        if ( idWsDocumento != null ) {
            $.get( '{{ url("documentos/modal/desconectar") }}/' + idDocumento + '/' +idWsDocumento, function( data ) {
                $( "#modal" ).html( data );
                $( "#modalDesconectar" ).modal();
            });
        } else {
            $.get( '{{ url("documentos/modal/desconectar") }}/' + idDocumento, function( data ) {
                $( "#modal" ).html( data );
                $( "#modalDesconectar" ).modal();
            });
        }
        
    }

</script>

@endpush