<link href="{{ asset('sistema/css/modales.css') }} " rel="stylesheet" type="text/css"/>
<style>
    .custom-checkbox {}
    
    /* oculto el input */
    .custom-checkbox input[type=checkbox] {
        display: none;
    }
    
    /* oculto el texto */
    .custom-checkbox span {
        display: none;
    }
    
    /* si está activo el input */
    .custom-checkbox input[type=checkbox]:checked + span {
        display: inline-block;
    }
    
    /* si está inactivo el input */
    .custom-checkbox input[type=checkbox]:not(:checked) + span + span {
        display: inline-block;
    }
</style>
<div class="modal fade" id="modalProblema" role="dialog" aria-labelledby="myModalLabel3" aria-hidden="true">
	<div class="modal-dialog" style="width:95%;">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" title="Cerrar" class="close" data-dismiss="modal" aria-hidden="true"></button>
				<h4 class="modal-title" ><strong><i class="fas fa-exclamation-triangle" style="color: #d9534f;border-color: #d43f3a;"></i>&nbsp;&nbsp;Documento con problema</strong></h4>
            </div>
            <form action="{{ asset('documentos/problema') }}" method="post" class="horizontal-form" id="form" autocomplete="off">
                <div class="modal-body">
                    <div class="note note-danger" id="divErrores" style="display:none;">
                        <h4 class="block" style="margin-bottom:5 px;">Debe Completar los siguientes campos: </h4>
                        <ul id="ulErrores"></ul>
                    </div>

                    <div class="form-group row" style="margin-bottom: 0px;">
                        <div class="col-xs-12 col-md-12">
                            <div class="form-group row">
                                <div class="col-xs-6 col-md-6">	
                                    <h4 class="headers-delete text-center">
                                    <i class="fa fa-file-text"></i> Documento
                                    </h4>
                                    <hr style="background-color: black;height: 2px;">

                                    @if ( $documento->reclamado == 1 )
                                    <div class="form-group row">
                                        <span style="font-size: 14px;margin-left: 10px;font-weight:bold;background-color: #d42700;" class="label label-danger" >Documento Reclamado</span>
                                    </div>
                                    @endif
                                    
                                    <div class="form-group row">
                                        <div class="col-xs-12">	
                                            <h4 class="headers-delete">
                                                Proveedor
                                            </h4>
                                            <hr>
                                            <div class="form-group">
                                                <label class="col-sm-3 control-label font-bold">Rut Proveedor</label>
                                                <label class="col-sm-2 control-label">{{ $documento->getProveedor->rut }}</label>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="form-group row">
                                        <div class="col-xs-12">
                                            <label class="col-xs-3 control-label font-bold">Nombre Proveedor</label>
                                            <label class="col-xs-9 control-label">{{ $documento->getProveedor->nombre }}</label>
                                        </div>
                                    </div>

                                    <div class="form-group row">
                                        <div class="col-xs-12">	
                                            <h4 class="headers-delete">
                                                Documento
                                            </h4>
                                            <hr>
                                            <div class="form-group">
                                                <label class="col-sm-2 control-label font-bold">Tipo Documento</label>
                                                <label class="col-sm-4 control-label ">{{ $documento->getTipoDocumento->nombre }}</label>
                                                {{-- Inicio Refacturación --}}
                                                @if ( $documento->id_relacionado != null )
                                                    @if ( $documento->id_tipo_documento == 4 || $documento->id_tipo_documento == 5 || $documento->id_tipo_documento == 10 || $documento->id_tipo_documento == 11 )
                                                        <label class="col-sm-2 control-label font-bold">Documento Relacionado</label>
                                                        <label class="col-sm-4 control-label ">{{ $documento->getDocumentoRelacionado->numero_documento }}</label>
                                                    @elseif ( $documento->id_tipo_documento > 0 )
                                                        <label class="col-sm-2 control-label font-bold">Reemplaza a</label>
                                                        <label class="col-sm-4 control-label ">{{ $documento->getDocumentoRelacionado->numero_documento }}</label>
                                                    @endif
                                                @endif
                                                {{-- Fin Refacturación --}}
                                            </div>
                                        </div>
                                    </div>

                                    @if ( $documento->getArchivoAceptaRefacturado )
                                        <div class="form-group row">
                                            <div class="col-xs-12">
                                                <label class="col-sm-5 control-label font-bold">Reemplaza al Archivo Acepta Rechazado</label>
                                                <label class="col-sm-4 control-label ">{{ $documento->getArchivoAceptaRefacturado->folio }}</label>
                                            </div>
                                        </div>
                                    @endif

                                    <div class="form-group row">
                                        <div class="col-xs-12">
                                            <label class="col-sm-2 control-label font-bold">N° Documento</label>
                                            <label class="col-sm-4 control-label ">{{ $documento->numero_documento }}</label>
                                            <label class="col-sm-2 control-label font-bold">Modalidad de Compra</label>
                                            <label class="col-sm-4 control-label ">{{ $documento->getModalidadCompra->nombre }}</label>
                                        </div>
                                    </div>

                                    <div class="form-group row">
                                        <div class="col-xs-12">
                                            <label class="col-sm-2 control-label font-bold">Tipo de adjudicación</label>
                                            <label class="col-sm-4 control-label ">{{ $documento->getTipoAdjudicacion->nombre }}</label>
                                        </div>
                                    </div>
                        
                                    <div class="form-group row">
                                        <div class="col-xs-12">
                                            <label class="col-sm-2 control-label font-bold">N° Documento Compra</label>
                                            <label class="col-sm-4 control-label ">{{ $documento->documento_compra }}</label>
                                            <label class="col-sm-2 control-label font-bold">Fecha de Documento</label>
                                            <label class="col-sm-4 control-label ">{{ fecha_dmY($documento->fecha_documento) }}</label>
                                        </div>
                                    </div>
                    
                                    <div class="form-group row">
                                        <div class="col-xs-12">
                                            <label class="col-sm-2 control-label font-bold">Tipo de Informe</label>
                                            <label class="col-sm-4 control-label ">{{ $documento->getTipoInforme->nombre }}</label>
                                            <label class="col-sm-2 control-label font-bold">Responsable</label>
                                            <label class="col-sm-4 control-label ">{{ $documento->getResponsable->name }}</label>
                                        </div>
                                    </div>
                        
                                    <div class="form-group row">
                                        <div class="col-xs-12">
                                            <label class="col-sm-2 control-label font-bold">Fecha de Recepción</label>
                                            <label class="col-sm-4 control-label ">{{ fecha_dmY($documento->fecha_recepcion) }}</label>                            
                                        </div>
                                    </div>
                    
                                    <div class="form-group row">
                                        <div class="col-xs-12">
                                            <label class="col-sm-2 control-label font-bold">Valor Tot. Orig.</label>
                                            <label class="col-sm-4 control-label ">$ {{ formatoMiles($documento->total_documento) }}</label>
                                            <label class="col-sm-2 control-label font-bold">Valor Tot. Act.</label>
                                            <label class="col-sm-4 control-label ">$ {{ formatoMiles($documento->total_documento_actualizado) }}</label>
                                        </div>
                                    </div>

                                    @if ( $documento->id_tipo_documento == 3 )
                                        <div class="form-group row">
                                            <div class="col-xs-12">
                                                <label class="col-sm-2 control-label font-bold">10% Impuesto</label>
                                                <label class="col-sm-4 control-label ">$ {{ formatoMiles($documento->impuesto) }}</label>
                                            </div>
                                        </div>

                                        <div class="form-group row">
                                            <div class="col-xs-12">
                                                <label class="col-sm-2 control-label font-bold">Líquido</label>
                                                <label class="col-sm-4 control-label ">$ {{ formatoMiles($documento->liquido) }}</label>
                                            </div>
                                        </div>
                                    @endif
                        
                                    <div class="form-group row">
                                        <div class="col-xs-12">
                                            @if ( $documento->getReferenteTecnico != null )
                                                <label class="col-sm-2 control-label font-bold">Referente Técnico</label>
                                                <label class="col-sm-4 control-label "><strong>{{ $documento->getReferenteTecnico->nombre }}</strong> {{ $documento->getReferenteTecnico->responsable }}</label>
                                            @endif
                                            <label class="col-sm-2 control-label font-bold">Fecha de Ingreso</label>
                                            <label class="col-sm-4 control-label ">{{ fecha_dmY($documento->fecha_ingreso)." ".fechaHoraMinutoSegundo($documento->fecha_ingreso) }}</label>
                                        </div>
                                    </div>

                                    <div class="form-group row">
                                        <div class="col-xs-12"> 
                                            <label class="col-sm-2 control-label font-bold">N° Licitación</label>
                                            <label class="col-sm-4 control-label ">{{ $documento->licitacion }}</label> 
                                        </div>
                                    </div>

                                    <div class="form-group row">
                                        <div class="col-xs-12">
                                            <label class="col-sm-2 control-label font-bold">Observaciones Factura:</label>
                                            <label class="col-sm-10 control-label ">{{ $documento->observacion }}</label>
                                        </div>
                                    </div>

                                    <div class="form-group row">
                                        <div class="col-xs-12">	
                                            <h4 class="headers-view">
                                                Documentos adjuntos
                                            </h4>
                                            <hr>
                                            <div class="form-group">
                                                <div class="col-xs-12 table-responsive">
                                                    <table class="table table-striped table-bordered table-hover " id="tabla_info_archivo">
                                                        <thead>
                                                            <tr>
                                                                <th width="40%">Nombre del Archivo</th>
                                                                <th width="30%">Tipo</th>
                                                                <th width="10%">Extensión</th>
                                                                <th width="10%">Peso</th>
                                                                <th width="10%"><i class="fa fa-cog" ></i></th>
                                                            </tr>
                                                        </thead>
                                                        <tbody>
                                                            @forelse($documento->getArchivos as $archivo)
                                                                <tr>
                                                                    <td>{{ $archivo->nombre }}</td>
                                                                    <td>@if( $archivo->getTipoArchivo ){{ $archivo->getTipoArchivo->nombre }}@endif</td>
                                                                    <td>{{ $archivo->extension }}</td>
                                                                    <td>{{ pesoArchivoEnMB($archivo->peso) }}</td>
                                                                    <td>
                                                                        <a class="btn btn-success btn-xs" @if($archivo->cargado == 0) href="{{ asset( $archivo->ubicacion ) }}" @else href="http://{{ $archivo->ubicacion }}" @endif
                                                                        title="Ver Archivo" target="_blank" ><i class="fa fa-eye"></i></a>
                                                                    </td>
                                                                </tr>
                                                            @empty
                    
                                                            @endforelse
                                                        </tbody>
                                                    </table>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                            
                                    @if ( $documento->getDocumentosRelacionados->count() > 0 || $documento->id_relacionado != null || $documento->getArchivoAceptaRefacturado )
                                        <div class="form-group row">
                                            <div class="col-xs-12">	
                                                <h4 class="headers-view">
                                                    Documentos Relacionados
                                                </h4>
                                                <hr>
                                                <div class="form-group">
                                                    <div class="col-xs-12 table-responsive">
                                                        <table class="table table-striped table-bordered table-hover " id="tabla_info_archivo">
                                                            <thead>
                                                                <tr>
                                                                    <th class="text-center">N° Doc.</th>
                                                                    <th >Tipo Doc.</th>
                                                                    <th class="text-right">Valor Total Doc.</th>
                                                                </tr>
                                                            </thead>
                                                            <tbody>
                                                                @forelse($documento->getDocumentosRelacionados as $doc)
                                                                    <tr>
                                                                        <td class="text-center">{{ $doc->numero_documento }}</td>
                                                                        <td>
                                                                            {{ $doc->getTipoDocumento->nombre }}
                                                                            @if ( $doc->getArchivos->count() > 0 )
                                                                                <div class="btn-group pull-right">
                                                                                    @foreach($doc->getArchivos as $archivo)
                                                                                        <a class="btn btn-info btn-xs " 
                                                                                            @if($archivo->cargado == 0) 
                                                                                                href="{{ asset( $archivo->ubicacion ) }}" 
                                                                                            @else
                                                                                                href="http://{{ $archivo->ubicacion }}"
                                                                                            @endif
                                                                                            title="Ver Archivo @if( $archivo->getTipoArchivo ){{ $archivo->getTipoArchivo->nombre }}@endif" 
                                                                                            target="_blank" ><i class="fa fa-eye"></i>
                                                                                        </a>
                                                                                    @endforeach
                                                                                </div>
                                                                            @endif
                                                                        </td>
                                                                        <td class="text-right">$ {{ formatoMiles($doc->total_documento) }}</td>
                                                                    </tr>
                                                                @empty
                                            
                                                                @endforelse
                                                                @if ( $documento->id_relacionado != null )
                                                                    <tr>
                                                                        <td class="text-center">{{ $documento->getDocumentoRelacionado->numero_documento }}</td>
                                                                        <td>
                                                                            {{ $documento->getDocumentoRelacionado->getTipoDocumento->nombre }}
                                                                            @if ( $documento->getDocumentoRelacionado->getArchivos->count() > 0 )
                                                                                <div class="btn-group pull-right">
                                                                                    @foreach($documento->getDocumentoRelacionado->getArchivos as $archivo)
                                                                                        <a class="btn btn-info btn-xs " 
                                                                                            @if($archivo->cargado == 0) 
                                                                                                href="{{ asset( $archivo->ubicacion ) }}" 
                                                                                            @else
                                                                                                href="http://{{ $archivo->ubicacion }}"
                                                                                            @endif
                                                                                            title="Ver Archivo @if( $archivo->getTipoArchivo ){{ $archivo->getTipoArchivo->nombre }}@endif" 
                                                                                            target="_blank" ><i class="fa fa-eye"></i>
                                                                                        </a>
                                                                                    @endforeach
                                                                                </div>
                                                                            @endif
                                                                        </td>
                                                                        <td class="text-right">$ {{ formatoMiles($documento->getDocumentoRelacionado->total_documento) }}</td>
                                                                    </tr>
                                                                @endif

                                                                @if ( $documento->getArchivoAceptaRefacturado )
                                                                    <tr>
                                                                        <td class="text-center">{{ $documento->getArchivoAceptaRefacturado->folio }}</td>
                                                                        <td>
                                                                            {{ $documento->getArchivoAceptaRefacturado->getTipoDocumento->nombre }} (Rechazado Acepta)
                                                                            @if ( $documento->getArchivoAceptaRefacturado->uri != null )
                                                                                <div class="btn-group pull-right">
                                                                                    <a class="btn btn-info btn-xs" href="http://windte1806.acepta.com/ca4webv3/PdfViewMedia?url={{ $documento->getArchivoAceptaRefacturado->uri }}&menuTitle=Papel%2520Carta"
                                                                                            title="Ver Factura" target="_blank" ><i class="fa fa-eye"></i></a>
                                                                                </div>
                                                                            @endif
                                                                        </td>
                                                                        <td class="text-right">$ {{ formatoMiles($documento->getArchivoAceptaRefacturado->monto_total) }}</td>
                                                                    </tr>
                                                                @endif
                                                                @if ( $documento->montoAplicadoAlValorTotal() != 0 )
                                                                    <tr>
                                                                        <td colspan="2" class="text-right"><strong>Sumatoria de NC y ND aplicada al Valor Tot. Orig.</strong></td>
                                                                        <td class="text-right">$ {{ formatoMiles($documento->montoAplicadoAlValorTotal()) }}</td>
                                                                    </tr>
                                                                @endif
                                                            </tbody>
                                                        </table>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    @endif

                                </div> <!-- / div class col-xs-6 -->

                                
                                <div class="col-xs-6 col-md-6">	
                                    <h4 class="headers-delete text-center">
                                        <i class="fas fa-comment"></i> Motivo de Problema
                                    </h4>
                                    <hr style="background-color: black;height: 2px;">
                                    <div class="form-group row">
                                        <div class="col-xs-12">
                                            <h4 class="headers-delete">
                                                Motivos
                                            </h4>
                                            <hr>
                                            <div class="form-group">

                                                <label for="motivos" class="col-sm-2 control-label label-form">Seleccione</label>
                                                <div class="col-sm-10 form-validate">
                                                    <select name="motivos[]" id="motivos" class="form-control" multiple style="width: 100%" required>
                                                        @foreach ( $motivos as $motivo )
                                                            <option value="{{ $motivo->id }}" >{{ $motivo->nombre }}</option>
                                                        @endforeach
                                                    </select>
                                                </div>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="form-group row">
                                        <div class="col-xs-12">	
                                            <h4 class="headers-delete">
                                                Observación Problema
                                            </h4>
                                            <hr>
                                            <div class="form-group">
                                                <div class="col-sm-12 form-validate">
                                                    <textarea class="form-control noresize" id="observacion_problema" name="observacion_problema"
                                                                style="height: 150px!important;"></textarea>
                                                </div>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="form-group row">
                                        <div class="col-sm-12 text-left">
                                            <div class="custom-checkbox">
                                                <label >
                                                    <strong>¿ En proceso ?
                                                    <input name="proceso_solucion_problema" id="proceso_solucion_problema"  type="checkbox"  />
                                                    <span class="active btn btn-success" title="Se encuentra en proceso"><i class="fas fa-cog fa-spin fa-lg"></i></span >
                                                    <span class="not-active btn btn-danger" title="No se encuentra en proceso" ><i class="fas fa-cog fa-lg"></i></span >
                                                    </strong>
                                                </label>
                                            </div>
                                        </div>
                                    </div>
                                </div> <!-- / div class col-xs-6 -->
                            </div>
                        </div>
                    </div>

                
                    {{-- <h4 class="headers-delete"><i class="fas fa-exclamation-circle" style="color:black"></i> El documento es rechazado solo en el sistema de Finanzas.</h4> --}}
                </div> {{-- /modal-body --}}

            
                @csrf
                <input type="hidden" name="_id" value="{{ $documento->id }}">
                <div class="modal-footer form-actions right">
                    
                    <button type="button" class="btn btn-default" title="Cerrar" data-dismiss="modal" aria-hidden="true">Cerrar</button>
                    <button type="submit" title="Guardar motivos de problema" class="btn btn-danger" id="botonRechazar"><i class="fas fa-exclamation-triangle" style="color:black;"></i>Guardar problemas</button>
                </div> {{-- /modal-footer --}}
            </form>
          	
		</div>
	</div>
</div>

<script>
    $("#motivos").select2({
        allowClear: true,
    });

    $('#motivos').on('select2-selecting', function (e) {
        setRequiredObservacionRechazo(parseInt(e.val),'Agregar');
    });

    $('#motivos').on('select2-removing', function (e) {
        setRequiredObservacionRechazo(parseInt(e.val),'Quitar');
    });

    function setRequiredObservacionRechazo(motivoSeleccionado, opcion) {
        if (opcion == 'Agregar') {

            if (motivoSeleccionado == 9) {
                $('#observacion_problema').prop('required',true);
            }

        } else if (opcion == 'Quitar') {

            if (motivoSeleccionado == 9) {
                $('#observacion_problema').prop('required',false);
            }

        }
    }

    $(document).ready(function() {

        $("#form").validate({
            highlight: function(element) {
                $(element).closest('.form-validate').removeClass('has-success');
                $(element).closest('.form-validate').addClass('has-error');
            },
            unhighlight: function(element) {
                $(element).closest('.form-validate').removeClass('has-error');
                $(element).closest('.form-validate').addClass('has-success');
            },
            errorElement: 'span',
            errorClass: 'help-block',
            errorPlacement: function (error, element) {
                console.log(element);
                if (element.parent('.input-group').length) {
                    error.insertAfter(element.parent());
                } else {
                    error.insertAfter(element);
                }
            },
            rules: {
            },
            messages: {
            },

            //para enviar el formulario por ajax
            submitHandler: function(form) {
                esperandoEliminar();

                let formData = new FormData(form);

                $.ajax({
                    url: form.action,
                    type: form.method,
                    //data: $(form).serialize(),
                    data: formData,
                    processData: false,
                    contentType: false,
                    success: function(respuesta) {
                        console.log(respuesta);

                        if ( respuesta.estado == 'error' ) {
                            toastr.error('No es posible realizar la acción'+'<br><strong>Error: '+ respuesta.mensaje +'</strong>', 'Atención', optionsToastr);
                        } else if ( respuesta.estado == 'success') {
                            
                            toastr.success(respuesta.mensaje, 'Atención', optionsToastr);
                            // Cambiar la informacion de la fila de la tabla principal
                            
                            if ( document.getElementById('tabla_deuda_flotante') != null ) { 
                                $("#modalCarga").modal('toggle');
                                
                                $('#tabla_deuda_flotante').DataTable().ajax.reload( function ( json ) {
                                    $("#modalCarga").modal('toggle');
                                });
                                
                            } else {
                                quitarElementoTabla(respuesta);
                            }
                            $("#modalProblema").modal("hide");
                            
                        }
                        
                    }            
                }).fail( function(respuesta) {//fail ajax
                    if ( respuesta.status == 400 ) {
                        mostrarErroresValidator(respuesta);
                    } else if ( respuesta.status == 500 ) {
                        toastr.error('No es posible realizar la acción, error en el servidor', 'Atención', optionsToastr);
                    } else {
                        toastr.error('No es posible realizar la acción', 'Atención', optionsToastr);
                    }
                    
                })
                .always(function() {
                    listoEliminar();
                });//ajax
                
            }

        });

    });

    function esperandoEliminar()
    {   
        $('.page-header-fixed *').css('cursor', 'wait');
        $("#botonRechazar").attr("disabled",true);
    }

    function listoEliminar()
    {
        $('.page-header-fixed *').css('cursor', '');
        $("#botonRechazar").attr("disabled",false);
    }

    function mostrarErroresValidator(respuesta)
    {
        if ( respuesta.responseJSON ) {
            //console.log(respuesta.responseJSON);
            let htmlErrores = '';
            for (let k in respuesta.responseJSON) { 
                //console.log(k, respuesta.responseJSON[k]);
                htmlErrores += '<li>' + respuesta.responseJSON[k] + '</li>';
            }

            $('#ulErrores').html(htmlErrores);
            $('#divErrores').css('display','');
            toastr.error('No es posible realizar la acción'+'<br>'+'Errores:<br> <ul>'+htmlErrores+'</ul>', 'Atención', optionsToastr);
        }
    }

    function quitarElementoTabla(respuesta)
    {
        $('#tr_' + respuesta.id).fadeOut(400, function(){
            tablaPrincipal.row('#tr_' + respuesta.id ).remove().draw();
        });
    }
</script>