<link href="{{ asset('sistema/css/modales.css') }} " rel="stylesheet" type="text/css"/>
<div class="modal fade" id="modalVer" role="dialog" aria-labelledby="myModalLabel3" aria-hidden="true">
	<div class="modal-dialog" style="width:1000px;">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" title="Cerrar" class="close" data-dismiss="modal" aria-hidden="true"></button>
				<h4 class="modal-title" ><strong><i class="fa fa-files-o fa-lg" ></i>&nbsp;&nbsp;Ver Documento</strong></h4>
            </div>
          
            <div class="modal-body">

                @if ( $documento->reclamado == 1 )
                <div class="form-group row">
                    <span style="font-size: 14px;margin-left: 10px;font-weight:bold;background-color: #d42700;" class="label label-danger" >Documento Reclamado</span>
                </div>
                @endif

                <div class="form-group row">
                    <div class="col-xs-12">	
                        <h4 class="headers-view">
                            Proveedor
                        </h4>
                        <hr>
                        <div class="form-group">
                            <label class="col-sm-2 control-label font-bold">Nombre</label>
                            <label class="col-sm-4 control-label">@if($documento->getProveedor){{ $documento->getProveedor->nombre }}@endif</label>
                            <label class="col-sm-2 control-label font-bold">Rut</label>
                            <label class="col-sm-4 control-label">@if($documento->getProveedor){{ $documento->getProveedor->rut }}@endif</label>
                        </div>
                    </div>
                </div>

                @if ( $documento->getProveedor->getProveedorMaestro )
                    <div class="form-group row">
                        <div class="col-xs-12">
                            <div class="form-group">
                                <label class="col-sm-3 control-label font-bold">Proveedor "Maestro"</label>
                                <label class="col-sm-9 control-label">{{ $documento->getProveedor->getProveedorMaestro->rut }} {{ $documento->getProveedor->getProveedorMaestro->nombre }}</label>
                            </div>
                        </div>
                    </div>
                @endif

                @if ( $documento->getProveedor->getProveedoresFusion->count() > 0 )
                    <div class="form-group row">
                        <div class="col-xs-12">
                            <div class="form-group">
                                <label class="col-sm-3 control-label font-bold">Proveedores Fusionados</label>
                                <label class="col-sm-9 control-label">
                                    @foreach ($documento->getProveedor->getProveedoresFusion as $provFusion)
                                    {{ $provFusion->rut }} {{ $provFusion->nombre }}<br>
                                    @endforeach
                                </label>
                            </div>
                        </div>
                    </div>
                @endif

                <div class="form-group row">
                    <div class="col-xs-12">	
                        <h4 class="headers-view">
                            Documento
                        </h4>
                        <hr>
                        <div class="form-group">
                            <label class="col-sm-2 control-label font-bold">Tipo Documento</label>
                            <label class="col-sm-4 control-label ">{{ $documento->getTipoDocumento->nombre }}</label>
                            {{-- Inicio Refacturación --}}
                            @if ( $documento->id_relacionado != null )
                                @if ( $documento->id_tipo_documento == 4 || $documento->id_tipo_documento == 5 || $documento->id_tipo_documento == 10 || $documento->id_tipo_documento == 11 )
                                    <label class="col-sm-2 control-label font-bold">Documento Relacionado</label>
                                    <label class="col-sm-4 control-label ">{{ $documento->getDocumentoRelacionado->numero_documento }}</label>
                                @elseif ( $documento->id_tipo_documento > 0 )
                                    <label class="col-sm-2 control-label font-bold">Reemplaza a</label>
                                    <label class="col-sm-4 control-label ">{{ $documento->getDocumentoRelacionado->numero_documento }}</label>
                                @endif
                            @endif
                            {{-- Fin Refacturación --}}
                        </div>
                    </div>
                </div>

                @if ( $documento->getArchivoAceptaRefacturado )
                    <div class="form-group row">
                        <div class="col-xs-12">
                            <label class="col-sm-5 control-label font-bold">Reemplaza al Archivo Acepta Rechazado</label>
                            <label class="col-sm-4 control-label ">{{ $documento->getArchivoAceptaRefacturado->folio }}</label>
                        </div>
                    </div>
                @endif

                <div class="form-group row">
                    <div class="col-xs-12">
                        <label class="col-sm-2 control-label font-bold">N° Documento</label>
                        <label class="col-sm-4 control-label ">{{ $documento->numero_documento }}</label>
                        <label class="col-sm-2 control-label font-bold">Modalidad de Compra</label>
                        <label class="col-sm-4 control-label ">{{ $documento->getModalidadCompra->nombre }}</label>
                    </div>
                </div>

                <div class="form-group row">
                    <div class="col-xs-12">
                        <label class="col-sm-2 control-label font-bold">Tipo de adjudicación</label>
                        <label class="col-sm-4 control-label ">{{ $documento->getTipoAdjudicacion->nombre }}</label>
                    </div>
                </div>

                <div class="form-group row">
                    <div class="col-xs-12">
                        <label class="col-sm-2 control-label font-bold">N° Documento Compra</label>
                        <label class="col-sm-4 control-label ">{{ $documento->documento_compra }}</label>
                        <label class="col-sm-2 control-label font-bold">Fecha de Documento</label>
                        <label class="col-sm-4 control-label ">{{ fecha_dmY($documento->fecha_documento) }}</label>
                    </div>
                </div>

                <div class="form-group row">
                    <div class="col-xs-12">
                        <label class="col-sm-2 control-label font-bold">Tipo de Informe</label>
                        <label class="col-sm-4 control-label ">{{ $documento->getTipoInforme->nombre }}</label>
                        {{-- <label class="col-sm-2 control-label font-bold">Responsable</label>
                        <label class="col-sm-4 control-label ">{{ $documento->getResponsable->name }}</label> --}}
                    </div>
                </div>

                <div class="form-group row">
                    <div class="col-xs-12">
                        <label class="col-sm-2 control-label font-bold">Fecha de Recepción</label>
                        <label class="col-sm-4 control-label ">{{ fecha_dmY($documento->fecha_recepcion) }}</label>                            
                    </div>
                </div>

                <div class="form-group row">
                    <div class="col-xs-12">
                        <label class="col-sm-2 control-label font-bold">Valor Tot. Orig.</label>
                        <label class="col-sm-4 control-label ">$ {{ formatoMiles($documento->total_documento) }}</label>
                        <label class="col-sm-2 control-label font-bold">Valor Tot. Act.</label>
                        <label class="col-sm-4 control-label ">$ {{ formatoMiles($documento->total_documento_actualizado) }}</label>
                    </div>
                </div>

                @if ( $documento->id_tipo_documento == 3 )
                    <div class="form-group row">
                        <div class="col-xs-12">
                            <label class="col-sm-2 control-label font-bold">{{ $documento->getLabelImpuesto() }} Impuesto</label>
                            <label class="col-sm-4 control-label ">$ {{ formatoMiles($documento->impuesto) }}</label>
                        </div>
                    </div>

                    <div class="form-group row">
                        <div class="col-xs-12">
                            <label class="col-sm-2 control-label font-bold">Líquido</label>
                            <label class="col-sm-4 control-label ">$ {{ formatoMiles($documento->liquido) }}</label>
                        </div>
                    </div>
                @endif

                <div class="form-group row">
                    <div class="col-xs-12">
                        @if ( $documento->getReferenteTecnico != null )
                            <label class="col-sm-2 control-label font-bold">Referente Técnico</label>
                            <label class="col-sm-4 control-label "><strong>{{ $documento->getReferenteTecnico->nombre }}</strong> {{ $documento->getReferenteTecnico->responsable }}</label>
                        @endif
                        <label class="col-sm-2 control-label font-bold">Fecha de Ingreso</label>
                        <label class="col-sm-4 control-label ">{{ fecha_dmY($documento->fecha_ingreso)." ".fechaHoraMinutoSegundo($documento->fecha_ingreso) }}</label>
                    </div>
                </div>

                @if ($documento->fecha_ingreso >= '2019-06-22 00:00:00' && $documento->validado == null)
                    <div class="form-group row">
                        <div class="col-xs-12">
                            <label class="col-sm-3 control-label font-bold">Días Para Cuadratura</label>
                            <label class="col-sm-4 control-label "><i class="fas fa-clipboard-check fa-lg fa-bounce" style="color:{{ $documento->getColorPorDiasCuadratura() }} !important"></i> {{ diferencia_dias_cuadratura($documento->fecha_ingreso,date('Y-m-d')) }}</label>
                        </div>
                    </div>
                @endif

                {{-- @if ( $documento->getUnaRelacionRecepcionValidada )
                    <div class="form-group row">
                        <div class="col-xs-12">
                            <div>
                                <label class="col-sm-2 control-label font-bold">Fecha Cuadratura</label>
                                <label class="col-sm-4 control-label ">{{ fecha_dmY($documento->getUnaRelacionRecepcionValidada->created_at)." ".fechaHoraMinutoSegundo($documento->getUnaRelacionRecepcionValidada->created_at) }}</label>
                                <label class="col-sm-2 control-label font-bold">Responsable Cuadratura</label>
                                <label class="col-sm-4 control-label ">@if( $documento->getUnaRelacionRecepcionValidada->getUsuarioResponsable ){{ $documento->getUnaRelacionRecepcionValidada->getUsuarioResponsable->name }}@else Automático del Sistema @endif</label>
                            </div>
                        </div>
                    </div>
                @elseif ( $documento->validado != null ) --}}
                @if ( $documento->validado != null )
                    <div class="form-group row">
                        <div class="col-xs-12">
                            <div>
                                <label class="col-sm-2 control-label font-bold">Fecha Cuadratura</label>
                                <label class="col-sm-4 control-label ">{{ fecha_dmY($documento->fecha_validado)." ".fechaHoraMinutoSegundo($documento->fecha_validado) }}</label>
                                <label class="col-sm-2 control-label font-bold">Responsable Cuadratura</label>
                                <label class="col-sm-4 control-label ">@if( $documento->getUserValidado ){{ $documento->getUserValidado->name }}@endif</label>
                            </div>
                        </div>
                    </div>
                @endif
                <div class="form-group row">
                    <div class="col-xs-12">
                        <label class="col-sm-2 control-label font-bold">N° Licitación</label>
                        <label class="col-sm-4 control-label ">{{ $documento->licitacion }}</label>
                    </div>
                </div>

                {{-- Falta "Factoring" --}}
                @isset ($factoring)
                    <div class="form-group row">
                        <div class="col-xs-12">
                            <label class="col-sm-2 control-label font-bold">Nombre Factoring</label>
                            <label class="col-sm-4 control-label "></label>
                            <label class="col-sm-2 control-label font-bold">Rut Factoring</label>
                            <label class="col-sm-4 control-label "></label>
                        </div>
                    </div>
                @endisset

                <div class="form-group row">
                    <div class="col-xs-12">
                        <label class="col-sm-2 control-label font-bold">Observaciones Factura:</label>
                        <label class="col-sm-10 control-label ">{{ $documento->observacion }}</label>
                    </div>
                </div>

                <div class="form-group row">
                    <div class="col-xs-12">	
                        <h4 class="headers-view">
                            Responsables
                        </h4>
                        <hr>
                        <div class="form-group">
                            <label class="col-sm-2 control-label font-bold">Ingresado por</label>
                            <label class="col-sm-4 control-label">@if( $documento->getDigitador ){{ $documento->getDigitador->name }}@endif</label>
                            @if ( $documento->getDevengador )
                                <label class="col-sm-2 control-label font-bold">Devengado por</label>
                                <label class="col-sm-4 control-label">{{ $documento->getDevengador->name }}</label>
                            @endif
                        </div>
                    </div>
                </div>

                <div class="form-group row">
                    <div class="col-xs-12">	
                        <h4 class="headers-view">
                            Documentos adjuntos
                        </h4>
                        <hr>
                        <div class="form-group">
                            <div class="col-xs-12 table-responsive">
                                <table class="table table-striped table-bordered table-hover " id="tabla_info_archivo">
                                    <thead>
                                        <tr>
                                            <th width="40%">Nombre del Archivo</th>
                                            <th width="30%">Tipo</th>
                                            <th width="10%">Extensión</th>
                                            <th width="10%">Peso</th>
                                            <th width="10%"><i class="fa fa-cog" ></i></th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        @forelse($documento->getArchivos as $archivo)
                                            <tr>
                                                <td>{{ $archivo->nombre }}</td>
                                                <td>@if( $archivo->getTipoArchivo ){{ $archivo->getTipoArchivo->nombre }}@endif</td>
                                                <td>{{ $archivo->extension }}</td>
                                                <td>{{ pesoArchivoEnMB($archivo->peso) }}</td>
                                                <td>
                                                    <a class="btn btn-success btn-xs" @if($archivo->cargado == 0) href="{{ asset( $archivo->ubicacion ) }}" @else href="http://{{ $archivo->ubicacion }}" @endif
                                                    title="Ver Archivo" target="_blank" ><i class="fa fa-eye"></i></a>
                                                </td>
                                            </tr>
                                        @empty

                                        @endforelse
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>

                @if ( $documento->getDocumentosRelacionados->count() > 0 || $documento->id_relacionado != null || $documento->getArchivoAceptaRefacturado )
                    <div class="form-group row">
                        <div class="col-xs-12">	
                            <h4 class="headers-view">
                                Documentos Relacionados
                            </h4>
                            <hr>
                            <div class="form-group">
                                <div class="col-xs-12 table-responsive">
                                    <table class="table table-striped table-bordered table-hover " id="tabla_info_archivo">
                                        <thead>
                                            <tr>
                                                <th class="text-center">N° Doc.</th>
                                                <th >Tipo Doc.</th>
                                                <th class="text-right">Valor Total Doc.</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            @forelse($documento->getDocumentosRelacionados as $doc)
                                                <tr>
                                                    <td class="text-center">{{ $doc->numero_documento }}</td>
                                                    <td>
                                                        {{ $doc->getTipoDocumento->nombre }}
                                                        @if ( $doc->getArchivos->count() > 0 )
                                                            <div class="btn-group pull-right">
                                                                @foreach($doc->getArchivos as $archivo)
                                                                    <a class="btn btn-info btn-xs " 
                                                                        @if($archivo->cargado == 0) 
                                                                            href="{{ asset( $archivo->ubicacion ) }}" 
                                                                        @else
                                                                            href="http://{{ $archivo->ubicacion }}"
                                                                        @endif
                                                                        title="Ver Archivo @if( $archivo->getTipoArchivo ){{ $archivo->getTipoArchivo->nombre }}@endif" 
                                                                        target="_blank" ><i class="fa fa-eye"></i>
                                                                    </a>
                                                                @endforeach
                                                            </div>
                                                        @endif
                                                    </td>
                                                    <td class="text-right">$ {{ formatoMiles($doc->total_documento) }}</td>
                                                </tr>
                                            @empty
                                                
                                            @endforelse

                                            @if ( $documento->id_relacionado != null )
                                                <tr>
                                                    <td class="text-center">{{ $documento->getDocumentoRelacionado->numero_documento }}</td>
                                                    <td>
                                                        {{ $documento->getDocumentoRelacionado->getTipoDocumento->nombre }}
                                                        @if ( $documento->getDocumentoRelacionado->getArchivos->count() > 0 )
                                                            <div class="btn-group pull-right">
                                                                @foreach($documento->getDocumentoRelacionado->getArchivos as $archivo)
                                                                    <a class="btn btn-info btn-xs " 
                                                                        @if($archivo->cargado == 0) 
                                                                            href="{{ asset( $archivo->ubicacion ) }}" 
                                                                        @else
                                                                            href="http://{{ $archivo->ubicacion }}"
                                                                        @endif
                                                                        title="Ver Archivo @if( $archivo->getTipoArchivo ){{ $archivo->getTipoArchivo->nombre }}@endif" 
                                                                        target="_blank" ><i class="fa fa-eye"></i>
                                                                    </a>
                                                                @endforeach
                                                            </div>
                                                        @endif
                                                    </td>
                                                    <td class="text-right">$ {{ formatoMiles($documento->getDocumentoRelacionado->total_documento) }}</td>
                                                </tr>
                                            @endif

                                            @if ( $documento->getArchivoAceptaRefacturado )
                                                <tr>
                                                    <td class="text-center">{{ $documento->getArchivoAceptaRefacturado->folio }}</td>
                                                    <td>
                                                        {{ $documento->getArchivoAceptaRefacturado->getTipoDocumento->nombre }} (Rechazado Acepta)
                                                        @if ( $documento->getArchivoAceptaRefacturado->uri != null )
                                                            <div class="btn-group pull-right">
                                                                <a class="btn btn-info btn-xs" href="http://windte1806.acepta.com/ca4webv3/PdfViewMedia?url={{ $documento->getArchivoAceptaRefacturado->uri }}&menuTitle=Papel%2520Carta"
                                                                        title="Ver Factura" target="_blank" ><i class="fa fa-eye"></i></a>
                                                            </div>
                                                        @endif
                                                    </td>
                                                    <td class="text-right">$ {{ formatoMiles($documento->getArchivoAceptaRefacturado->monto_total) }}</td>
                                                </tr>
                                            @endif
                                            @if ( $documento->montoAplicadoAlValorTotal() != 0 )
                                                <tr>
                                                    <td colspan="2" class="text-right"><strong>Sumatoria de NC y ND aplicada al Valor Tot. Orig.</strong></td>
                                                    <td class="text-right">$ {{ formatoMiles($documento->montoAplicadoAlValorTotal()) }}</td>
                                                </tr>
                                            @endif
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                @endif

                @if ( $documento->getRelacionRecepcionesValidadas->count() > 0 )
                    <div class="form-group row">
                        <div class="col-xs-12">	
                            <h4 class="headers-view">
                                Documentos Recepción, archivos adjuntos
                            </h4>
                            <hr>
                            <div class="form-group">
                                <div class="col-xs-12 table-responsive">
                                    <table class="table table-striped table-bordered table-hover " id="tabla_info_archivos_relacion">
                                        <thead>
                                            <tr>
                                                <th width="15%">N° Doc Recepción</th>
                                                <th width="80%">Nombre del Archivo</th>
                                                <th width="5%" class="text-center"><i class="fa fa-cog"></i></th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            @forelse($documento->getRelacionRecepcionesValidadas as $relacion)
                                                @forelse ($relacion->getWsDocumento->getWsArchivos as $archivo)
                                                    <tr>
                                                        <td>{{ $relacion->getWsDocumento->documento }}</td>
                                                        <td>{{ $archivo->nombre_archivo }}</td>
                                                        <td class="text-center">
                                                            <a class="btn btn-success btn-xs" @if($archivo->cargado == 0) href="{{ asset( $archivo->ubicacion ) }}" @else href="http://{{ $archivo->ubicacion }}" @endif
                                                            title="Ver Archivo" target="_blank" ><i class="fa fa-eye"></i></a>
                                                        </td>
                                                    </tr>
                                                @empty
                                                    
                                                @endforelse
                                            @empty

                                            @endforelse
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                @endif

                <div class="form-group row">
                    <div class="col-xs-12">	
                        <h4 class="headers-view">
                            Ítem Presupuestario
                        </h4>
                        <hr>
                        <div class="form-group">
                            <div class="col-xs-12 table-responsive">
                                @include('documento.parcial.items_presupuestarios')
                            </div>
                        </div>
                    </div>
                </div>

                @if ( $documento->getFoliosSigfe->count() > 0 )
                    <div class="form-group row">
                        <div class="col-xs-12">	
                            <h4 class="headers-view">
                                Devengado
                            </h4>
                            <hr>
                        </div>
                    </div>

                    <div class="form-group row">
                        <div class="col-xs-12 table-responsive">
                            @include('documento.parcial.info_devengo')
                        </div>
                    </div>
                @endif

            </div>{{-- /modal-body --}}

            {{--
            <div class="modal-footer">
                <div class="btn-group">
                <button type="button" class="btn btn-xs btn-default" title="Cancelar" data-dismiss="modal" aria-hidden="true">Cancelar</button>
                </div>
            </div> --}} {{-- /modal-footer --}}
          	
		</div>
	</div>
</div>

 <script type="text/javascript">
    var tablaItemsView = $('#tabla_item_presupuestario').DataTable({
        "language": {
            "emptyTable": "No hay datos disponibles en la tabla",
        },
        "paging":   false,
        "ordering": false,
        "info":     false,
        "searching": false
    });
 
    var tablaArchivoView = $('#tabla_info_archivo').DataTable({
         "language": {
            "emptyTable": "No hay datos disponibles en la tabla",
        },
        "paging":   false,
        "ordering": false,
        "info":     false,
        "searching": false
    });

 	$(document).ready(function(){
         
  
 	});

    
 </script>