<link href="{{ asset('sistema/css/modales.css') }}?v={{rand()}}" rel="stylesheet" type="text/css"/>
<div class="modal fade" id="modalTraza" role="dialog" aria-labelledby="myModalLabel3" aria-hidden="true">
	<div class="modal-dialog" style="width:95%;">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" title="Cerrar" class="close" data-dismiss="modal" aria-hidden="true"></button>
				<h4 class="modal-title" ><strong><i class="fas fa-map-signs fa-lg"></i>&nbsp;&nbsp;Trazabilidad Documento</strong></h4>
            </div>
          
            <div class="modal-body">

                <div class="form-group row">
                    <div class="col-xs-12">

                        <div class="col-xs-6">
                            <div class="form-group row">
                                <div class="col-xs-12">	
                                    <h5 class="headers-traceability">
                                        Proveedor
                                    </h5>
                                    <hr style="margin-top: 0px; ">

                                    <table class="table table-bordered " id="tabla_info_proveedor">
                                        <tr>
                                            <th class="th_sigfe" >Nombre</th>
                                            <td class="text-center">@if($documento->getProveedor){{ $documento->getProveedor->nombre }}@endif</td>
                                            <th class="th_sigfe">Rut</th>
                                            <td class="text-center" >@if($documento->getProveedor){{ $documento->getProveedor->rut }}@endif</td>
                                        </tr>

                                        @if ( $documento->getProveedor->getProveedorMaestro )
                                            <tr>
                                                <th class="th_sigfe" colspan="2" >Proveedor "Maestro"</th>
                                                <td class="text-center" colspan="2">{{ $documento->getProveedor->getProveedorMaestro->rut }} {{ $documento->getProveedor->getProveedorMaestro->nombre }}</td>
                                            </tr>
                                        @endif

                                        @if ( $documento->getProveedor->getProveedoresFusion->count() > 0 )
                                            <tr>
                                                <th class="th_sigfe" colspan="4" >Proveedores Fusionados</th>
                                            </tr>

                                            @forelse ($documento->getProveedor->getProveedoresFusion as $provFusion)
                                                <tr>
                                                    <td class="text-center" colspan="4">{{ $provFusion->rut }} {{ $provFusion->nombre }}</td>
                                                </tr>
                                            @empty

                                            @endforelse

                                        @endif

                                    </table>
                                </div>
                            </div>

                            <div class="form-group row">
                                <div class="col-xs-12">	
                                    <h5 class="headers-traceability">
                                        Documento  @role(['propietario']) ID : {{ $documento->id }} @endrole
                                    </h5>
                                    <hr style="margin-top: 0px; ">

                                    <table class="table table-bordered " id="tabla_info_documento">

                                        <tr>
                                            <th class="th_sigfe align_middle" >Proveedor</th>
                                            <td class="align_middle">{{ ($documento->getProveedor->nombre) }}</td>
                                            <th class="th_sigfe align_middle">RUT</th>
                                            <td class="align_middle" >{{ $documento->getProveedor->rut }}</td>
                                        </tr>
                                        
                                        <tr>
                                            <th class="th_sigfe align_middle" >Emisión</th>
                                            <td class="align_middle">{{ fecha_dmY($documento->fecha_documento) }}</td>
                                            <th class="th_sigfe align_middle">Recepción</th>
                                            <td class="align_middle" >{{ fecha_dmY($documento->fecha_recepcion) }}</td>
                                        </tr>
                                            
                                        <tr>
                                            <th class="th_sigfe align_middle" >Tipo</th>
                                            <td class="align_middle">{{ $documento->getTipoDocumento->nombre }}</td>
                                            <th class="th_sigfe align_middle">Número</th>
                                            <td class="align_middle" >{{ $documento->numero_documento }}</td>
                                        </tr>

                                        @if ( $documento->id_relacionado != null )
                                            <tr>
                                                @if ( $documento->id_tipo_documento == 4 || $documento->id_tipo_documento == 5 || $documento->id_tipo_documento == 10 || $documento->id_tipo_documento == 11 )

                                                    <th class="th_sigfe align_middle" colspan="2" >Documento Relacionado</th>
                                                    <td class="align_middle" colspan="2" >{{ $documento->getDocumentoRelacionado->numero_documento }}</td>

                                                @elseif ( $documento->id_tipo_documento > 0 )

                                                    <th class="th_sigfe align_middle" colspan="2" >Reemplaza a</th>
                                                    <td class="align_middle" colspan="2" >{{ $documento->getDocumentoRelacionado->numero_documento }}</td>

                                                @endif
                                            </tr>
                                        @endif

                                        @if ( $documento->getArchivoAceptaRefacturado )
                                            <tr>
                                                <th class="th_sigfe align_middle" colspan="2" >Reemplaza al Archivo Acepta Rechazado</th>
                                                <td class="align_middle" colspan="2" >{{ $documento->getArchivoAceptaRefacturado->folio }}</td>
                                            </tr>
                                        @endif

                                        <tr>
                                            <th class="th_sigfe align_middle" >Valor Tot. Orig.</th>
                                            <td class="text-center align_middle">$ {{ formatoMiles($documento->total_documento) }}</td>
                                            <th class="th_sigfe align_middle">Valor Tot. Act.</th>
                                            <td class="text-center align_middle" >$ {{ formatoMiles($documento->total_documento_actualizado) }}</td>
                                        </tr>

                                        @if ( $documento->id_tipo_documento == 3 )
                                            <tr>
                                                <th class="th_sigfe align_middle" >{{ $documento->getLabelImpuesto() }} Impuesto</th>
                                                <td class="text-center align_middle">$ {{ formatoMiles($documento->impuesto) }}</td>
                                                <th class="th_sigfe align_middle">Líquido</th>
                                                <td class="text-center align_middle" >$ {{ formatoMiles($documento->liquido) }} </td>
                                            </tr>
                                        @endif

                                        <tr>
                                            <th class="th_sigfe align_middle" >N° Documento Compra</th>
                                            <td class="text-center align_middle">{{ $documento->documento_compra }}</td>
                                            <th class="th_sigfe text-center" style="color: #557ef9;vertical-align:middle;">Estado</th>
                                            <td class="text-center" style="font-weight: bold;vertical-align:middle;" >{!! $documento->getEstadoDocumento() !!}</td>
                                        </tr>

                                        @if ( $documento->observacion != '' && $documento->observacion != null )
                                            <tr>
                                                <th class="th_sigfe align_middle" >Observaciones</th>
                                                <td class="align_middle" colspan="3">{{ $documento->observacion }}</td>
                                            </tr>
                                        @endif

                                        @if ($documento->getProveedorFactoring->id != null)
                                            <tr>
                                                <th class="th_sigfe align_middle" >Nombre Factoring</th>
                                                <td class="align_middle">{{ $documento->getProveedorFactoring->nombre }}</td>
                                                <th class="th_sigfe align_middle" >Rut Factoring</th>
                                                <td class="align_middle" >{{ $documento->getProveedorFactoring->rut }}</td>
                                            </tr>
                                        @endif

                                    </table>
                                    @if ( $documento->id_user_problema != null )

                                        <h5 class="headers-traceability">
                                            Problemas
                                        </h5>
                                        <hr style="margin-top: 0px; ">

                                        <table class="table table-bordered " id="tabla_info_problemas">

                                            <tr>
                                                <th class="th_sigfe text-center" colspan="2" >Motivos</th>
                                            </tr>

                                            @foreach ($documento->getDocumentoMotivoProblema as $documentoMotivoProblema)
                                                <tr>
                                                    <td class="text-center" colspan="2" >{{ $documentoMotivoProblema->getMotivoProblema->nombre }}</td>
                                                </tr>
                                            @endforeach

                                            @if ( $documento->getDigitadorProblema )
                                                <tr>
                                                    <th class="th_sigfe" >Responsable</th>
                                                    <td class="text-center">{{ $documento->getDigitadorProblema->name }}</td>
                                                </tr>
                                            @endif

                                            @if ( $documento->observacion_problema != null )
                                                <tr>
                                                    <th class="th_sigfe" >Observaciones</th>
                                                    <td class="text-center">{!! str_replace(PHP_EOL,'<br>',$documento->observacion_problema) !!}</td>
                                                </tr>
                                            @endif

                                        </table>

                                    @endif
                                </div>
                            </div>

                            <div class="form-group row">
                                <div class="col-xs-12">	
                                    <h5 class="headers-traceability">
                                        Responsables
                                    </h5>
                                    <hr style="margin-top: 0px; ">

                                    <table class="table table-bordered " id="tabla_info_responsables">
                                        <tr>
                                            <th class="th_sigfe text-center" >Ingresado por</th>
                                            <td class="text-center">@if( $documento->getDigitador ){{ $documento->getDigitador->name }}@endif</td>
                                        </tr>
                                        <tr>
                                            <th class="th_sigfe text-center">Responsable</th>
                                            <td class="text-center" >@if( $documento->getResponsable ){{ $documento->getResponsable->name }}@endif</td>
                                        </tr>
                                            
                                        {{-- @if ( $documento->getUnaRelacionRecepcionValidada || $documento->validado != null) --}}
                                        @if ( $documento->validado != null )
                                            <tr>
                                                <th class="th_sigfe text-center" >Cuadrado por</th>
                                                {{-- @if( $documento->getUnaRelacionRecepcionValidada )
                                                    @if ( $documento->getUnaRelacionRecepcionValidada->getUsuarioResponsable )
                                                        <td class="text-center">{{ $documento->getUnaRelacionRecepcionValidada->getUsuarioResponsable->name }}</td>
                                                    @else
                                                        <td class="text-center">Automático del Sistema</td>
                                                    @endif
                                                @elseif ( $documento->validado != null ) --}}
                                                    <td class="text-center">{{ $documento->getUserValidado->name }}</td>
                                                {{-- @endif --}}
                                            </tr>
                                        @endif
                                        {{-- @endif --}}

                                        @if ( $documento->id_devengador != null )
                                            <tr>
                                                <th class="th_sigfe text-center">Devengado por</th>
                                                <td class="text-center" >{{ $documento->getDevengador->name }}</td>
                                            </tr>
                                        @endif

                                        @if ( $documento->getComprobanteContableDocumento->count() > 0 && $documento->getComprobanteContableDocumento->last()->getComprobanteContable && 
                                              $documento->getComprobanteContableDocumento->last()->getComprobanteContable->getUsuario 
                                            )
                                            <tr>
                                                <th class="th_sigfe text-center">Pagado por</th>
                                                <td class="text-center" >{{ $documento->getComprobanteContableDocumento->last()->getComprobanteContable->getUsuario->name }}</td>
                                            </tr>
                                        @endif

                                        @if ( $documento->trashed() && $documento->id_user_deleted != null )
                                            <tr>
                                                <th class="th_sigfe text-center">Eliminado por</th>
                                                <td class="text-center" >{{ $documento->getUserQueElimino->name }}</td>
                                            </tr>
                                        @endif

                                    </table>
                                </div>
                            </div>

                            <div class="form-group row">
                                <div class="col-xs-12">	
                                    <h5 class="headers-traceability">
                                        Archivos Adjuntos
                                    </h5>
                                    <hr style="margin-top: 0px; ">

                                    <table class="table table-striped table-bordered table-hover " id="tabla_info_archivo">
                                        <thead>
                                            <tr>
                                                <th width="40%">Nombre del Archivo</th>
                                                <th width="30%">Tipo</th>
                                                <th width="10%">Extensión</th>
                                                <th width="10%">Peso</th>
                                                <th width="10%"><i class="fa fa-cog" ></i></th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            @forelse($documento->getArchivos as $archivo)
                                                <tr>
                                                    <td>{{ $archivo->nombre }}</td>
                                                    <td>@if( $archivo->getTipoArchivo ){{ $archivo->getTipoArchivo->nombre }}@endif</td>
                                                    <td>{{ $archivo->extension }}</td>
                                                    <td>{{ pesoArchivoEnMB($archivo->peso) }}</td>
                                                    <td>
                                                        <a class="btn btn-success btn-xs" @if($archivo->cargado == 0) href="{{ asset( $archivo->ubicacion ) }}" @else href="http://{{ $archivo->ubicacion }}" @endif
                                                        title="Ver Archivo" target="_blank" ><i class="fa fa-eye"></i></a>
                                                    </td>
                                                </tr>
                                            @empty
    
                                            @endforelse
                                            <tr>
                                                <td>Trazabilidad del Documento</td>
                                                <td>Generado por Sistema</td>
                                                <td>pdf</td>
                                                <td></td>
                                                <td>
                                                    <a class="btn btn-danger btn-xs" title="PDF Trazabilidad Dcto. Finanzas" target="_blank" 
                                                        href="{{ asset('documentos/generar/pdf_trazabilidad_documento/').'/'.$documento->id }}">
                                                        <i class="fas fa-file-pdf fa-lg"></i></a>
                                                </td>
                                            </tr>
                                        </tbody>
                                    </table>
                                </div>
                            </div>

                            @if ( $documento->getDocumentosRelacionados->count() > 0 || $documento->id_relacionado != null || $documento->getArchivoAceptaRefacturado )
                                <div class="form-group row">
                                    <div class="col-xs-12">	
                                        <h5 class="headers-traceability">
                                            Documentos Relacionados
                                        </h5>
                                        <hr style="margin-top: 0px; ">

                                        <table class="table table-striped table-bordered table-hover " id="tabla_info_archivo">
                                            <thead>
                                                <tr>
                                                    <th class="text-center">N° Doc.</th>
                                                    <th >Tipo Doc.</th>
                                                    <th class="text-right">Valor Total Doc.</th>
                                                </tr>
                                            </thead>
                                            <tbody>
                                                @forelse($documento->getDocumentosRelacionados as $doc)
                                                    <tr>
                                                        <td class="text-center">{{ $doc->numero_documento }}</td>
                                                        <td>
                                                            {{ $doc->getTipoDocumento->nombre }}
                                                            @if ( $doc->getArchivos->count() > 0 )
                                                                <div class="btn-group pull-right">
                                                                    @foreach($doc->getArchivos as $archivo)
                                                                        <a class="btn btn-info btn-xs " 
                                                                            @if($archivo->cargado == 0) 
                                                                                href="{{ asset( $archivo->ubicacion ) }}" 
                                                                            @else
                                                                                href="http://{{ $archivo->ubicacion }}"
                                                                            @endif
                                                                            title="Ver Archivo @if( $archivo->getTipoArchivo ){{ $archivo->getTipoArchivo->nombre }}@endif" 
                                                                            target="_blank" ><i class="fa fa-eye"></i>
                                                                        </a>
                                                                    @endforeach
                                                                </div>
                                                            @endif
                                                        </td>
                                                        <td class="text-right">$ {{ formatoMiles($doc->total_documento) }}</td>
                                                    </tr>
                                                @empty
                                                
                                                @endforelse
        
                                                @if ( $documento->id_relacionado != null )
                                                    <tr>
                                                        <td class="text-center">{{ $documento->getDocumentoRelacionado->numero_documento }}</td>
                                                        <td>
                                                            {{ $documento->getDocumentoRelacionado->getTipoDocumento->nombre }}
                                                            @if ( $documento->getDocumentoRelacionado->getArchivos->count() > 0 )
                                                                <div class="btn-group pull-right">
                                                                    @foreach($documento->getDocumentoRelacionado->getArchivos as $archivo)
                                                                        <a class="btn btn-info btn-xs " 
                                                                            @if($archivo->cargado == 0) 
                                                                                href="{{ asset( $archivo->ubicacion ) }}" 
                                                                            @else
                                                                                href="http://{{ $archivo->ubicacion }}"
                                                                            @endif
                                                                            title="Ver Archivo @if( $archivo->getTipoArchivo ){{ $archivo->getTipoArchivo->nombre }}@endif" 
                                                                            target="_blank" ><i class="fa fa-eye"></i>
                                                                        </a>
                                                                    @endforeach
                                                                </div>
                                                            @endif
                                                        </td>
                                                        <td class="text-right">$ {{ formatoMiles($documento->getDocumentoRelacionado->total_documento_actualizado) }}</td>
                                                    </tr>

                                                    @forelse( $documento->getDocumentoRelacionado->getDocumentosRelacionados as $doc )
                                                        @if ( $doc->id != $documento->id )
                                                            <tr>
                                                                <td class="text-center">{{ $doc->numero_documento }}</td>
                                                                <td>
                                                                    {{ $doc->getTipoDocumento->nombre }}
                                                                    @if ( $doc->getArchivos->count() > 0 )
                                                                        <div class="btn-group pull-right">
                                                                            @foreach($doc->getArchivos as $archivo)
                                                                                <a class="btn btn-info btn-xs " 
                                                                                    @if($archivo->cargado == 0) 
                                                                                        href="{{ asset( $archivo->ubicacion ) }}" 
                                                                                    @else
                                                                                        href="http://{{ $archivo->ubicacion }}"
                                                                                    @endif
                                                                                    title="Ver Archivo @if( $archivo->getTipoArchivo ){{ $archivo->getTipoArchivo->nombre }}@endif" 
                                                                                    target="_blank" ><i class="fa fa-eye"></i>
                                                                                </a>
                                                                            @endforeach
                                                                        </div>
                                                                    @endif
                                                                </td>
                                                                <td class="text-right">$ {{ formatoMiles($doc->total_documento_actualizado) }}</td>
                                                            </tr>
                                                        @endif
                                                    @empty
                                                    
                                                    @endforelse
                                                @endif
    
                                                @if ( $documento->getArchivoAceptaRefacturado )
                                                    <tr>
                                                        <td class="text-center">{{ $documento->getArchivoAceptaRefacturado->folio }}</td>
                                                        <td>
                                                            {{ $documento->getArchivoAceptaRefacturado->getTipoDocumento->nombre }} (Rechazado Acepta)
                                                            @if ( $documento->getArchivoAceptaRefacturado->uri != null )
                                                                <div class="btn-group pull-right">
                                                                    <a class="btn btn-info btn-xs" href="http://windte1806.acepta.com/ca4webv3/PdfViewMedia?url={{ $documento->getArchivoAceptaRefacturado->uri }}&menuTitle=Papel%2520Carta"
                                                                            title="Ver Factura" target="_blank" ><i class="fa fa-eye"></i></a>
                                                                </div>
                                                            @endif
                                                        </td>
                                                        <td class="text-right">$ {{ formatoMiles($documento->getArchivoAceptaRefacturado->monto_total) }}</td>
                                                    </tr>
                                                @endif
                                                @if ( $documento->montoAplicadoAlValorTotal() != 0 )
                                                    <tr>
                                                        <td colspan="2" class="text-right"><strong>Sumatoria de NC y ND aplicada al Valor Tot. Orig.</strong></td>
                                                        <td class="text-right">$ {{ formatoMiles($documento->montoAplicadoAlValorTotal()) }}</td>
                                                    </tr>
                                                @endif
                                            </tbody>
                                        </table>

                                    </div>
                                </div>
                            @endif
                            
                            @if ( $documento->getRelacionRecepcionesValidadas->count() > 0 )
                                <div class="form-group row">
                                    <div class="col-xs-12">	
                                        <h5 class="headers-traceability">
                                            Documentos Recepción, archivos adjuntos
                                        </h5>
                                        <hr style="margin-top: 0px; ">

                                        <table class="table table-striped table-bordered table-hover " id="tabla_info_archivos_relacion">
                                            <thead>
                                                <tr>
                                                    <th width="15%">N° Doc Recepción</th>
                                                    <th width="80%">Nombre del Archivo</th>
                                                    <th width="5%" class="text-center"><i class="fa fa-cog"></i></th>
                                                </tr>
                                            </thead>
                                            <tbody>
                                                @forelse($documento->getRelacionRecepcionesValidadas as $relacion)
                                                    @forelse ($relacion->getWsDocumento->getWsArchivos as $archivo)
                                                        <tr>
                                                            <td>{{ $relacion->getWsDocumento->documento }}</td>
                                                            <td>{{ $archivo->nombre_archivo }}</td>
                                                            <td class="text-center">
                                                                <a class="btn btn-success btn-xs" @if($archivo->cargado == 0) href="{{ asset( $archivo->ubicacion ) }}" @else href="http://{{ $archivo->ubicacion }}" @endif
                                                                title="Ver Archivo" target="_blank" ><i class="fa fa-eye"></i></a>
                                                            </td>
                                                        </tr>
                                                    @empty
                                                        
                                                    @endforelse
                                                @empty
    
                                                @endforelse
                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                            @endif

                        </div> <!-- / div class col-xs-6 -->

                        <div class="col-xs-6">

                            <div class="form-group row">
                                <div class="col-xs-12">	
                                    <h5 class="headers-traceability">
                                            Ítems Presupuestario
                                    </h5>
                                    <hr style="margin-top: 0px; ">

                                    @include('documento.parcial.items_presupuestarios')                                        
                                </div>
                            </div>

                            @if ( $documento->getFoliosSigfe->count() > 0 )
                                <div class="form-group row">
                                    <div class="col-xs-12">	
                                        <h5 class="headers-traceability">
                                            Devengo
                                        </h5>
                                        <hr style="margin-top: 0px; ">

                                        @include('documento.parcial.info_devengo')
                                    </div>
                                </div>
                            @endif

                            @if ( $documento->getComprobanteContableDocumento->count() > 0 )
                                <div class="form-group row">
                                    <div class="col-xs-12">	
                                        <h5 class="headers-traceability">
                                            Comprobantes
                                        </h5>
                                        <hr style="margin-top: 0px; ">

                                        @include('documento.parcial.info_comprobante')
                                    </div>
                                </div>
                            @endif

                            @if ( $documento->getReversaComprobante->count() > 0 )
                                <div class="form-group row">
                                    <div class="col-xs-12">	
                                        <h5 class="headers-traceability">
                                            Reversas
                                        </h5>
                                        <hr style="margin-top: 0px; ">

                                        @include('documento.parcial.info_reversa')
                                    </div>
                                </div>
                            @endif

                            @role(['propietario','administrador'])
                            <div class="form-group row">
                                <div class="col-xs-12">	
                                    <h5 class="headers-traceability">
                                        Fechas
                                    </h5>
                                    <hr style="margin-top: 0px; ">
                                    <table class="table table-bordered " id="tabla_info_fechas">
                                        <tr>
                                            <th class="th_sigfe text-center" >Ingreso</th>
                                            <td class="text-center">{{ diaSemana( $documento->fecha_ingreso )." ".fecha_dmY( $documento->fecha_ingreso )." ".fechaHoraMinutoSegundo( $documento->fecha_ingreso ) }}</td>
                                        </tr>

                                        {{-- Diferencia Emisión e Ingreso --}}
                                        <tr>
                                            <th class="th_sigfe text-center" >Diferencia Emisión e Ingreso</th>
                                            <td class="text-center"><i class="fas fa-calendar-alt" style="color: {{ $documento->getColorPorDiasDiferenciaDocumentoIngreso() }} !important"></i> {{ diferencia_dias( $documento->fecha_documento,$documento->fecha_ingreso ) }} Días</td>
                                        </tr>

                                        {{-- Días para realizar cuadratura (son maximo 8 dias) --}}
                                        @if ($documento->fecha_ingreso >= '2019-06-22 00:00:00' && $documento->validado == null )
                                            <tr>
                                                <th class="th_sigfe text-center" >Días Restantes para Cuadratura</th>
                                                <td class="text-center"><i class="fas fa-clipboard-check fa-lg fa-bounce" 
                                                    style="color:{{ $documento->getColorPorDiasCuadratura() }} !important"></i> {{ diferencia_dias_cuadratura( $documento->fecha_ingreso,date('Y-m-d') ) }}</td>
                                            </tr>
                                        @endif

                                        {{-- Información Cuadratura Lista --}}
                                        {{-- @if ( $documento->getUnaRelacionRecepcionValidada || $documento->validado != null || ( $documento->id_visto_bueno != null && $documento->getVistoBueno->id_registrador_memo_respuesta != null ) ) --}}
                                        @if ( $documento->validado != null )
                                            <tr>
                                                <th class="th_sigfe text-center" >Cuadratura</th>
                                                <td class="text-center">
                                                    {{-- @if ( $documento->getUnaRelacionRecepcionValidada )

                                                        {{ diaSemana( $documento->getUnaRelacionRecepcionValidada->created_at )." ".fecha_dmY( $documento->getUnaRelacionRecepcionValidada->created_at )." ".fechaHoraMinutoSegundo( $documento->getUnaRelacionRecepcionValidada->created_at ) }}

                                                    @elseif ( $documento->validado != null ) --}}

                                                        {{ diaSemana( $documento->fecha_validado )." ".fecha_dmY( $documento->fecha_validado )." ".fechaHoraMinutoSegundo( $documento->fecha_validado ) }}

                                                    {{-- @elseif ( $documento->id_visto_bueno != null )

                                                        {{ diaSemana( $documento->getVistoBueno->fecha_memo_respuesta )." ".fecha_dmY( $documento->getVistoBueno->fecha_memo_respuesta ) }}

                                                    @endif --}}
                                                </td>
                                            </tr>
                                        @endif
                                        {{-- @endif --}}

                                        {{-- @if ( $documento->getUnaRelacionRecepcionValidada || $documento->validado != null || ( $documento->id_visto_bueno != null && $documento->getVistoBueno->id_registrador_memo_respuesta != null ) ) --}}
                                        @if ( $documento->validado != null )
                                            {{-- Diferencia Ingreso y Cuadratura --}}
                                            <tr>
                                                <th class="th_sigfe text-center" >Diferencia Ingreso y Cuadratura</th>
                                                <td class="text-center"><i class="fas fa-calendar-alt" 
                                                    style="color: {{ $documento->getColorPorDiasDiferenciaIngresoCuadratura() }} !important"></i>
                                            {{-- @if ( $documento->getUnaRelacionRecepcionValidada )

                                                {{ diferencia_dias( $documento->fecha_ingreso, $documento->getUnaRelacionRecepcionValidada->created_at ) }} Días

                                            @elseif ( $documento->validado != null ) --}}

                                                {{ diferencia_dias( $documento->fecha_ingreso, $documento->fecha_validado ) }} Días

                                            {{-- @elseif ( $documento->id_visto_bueno != null )

                                                {{ diferencia_dias( $documento->fecha_ingreso, $documento->getVistoBueno->fecha_memo_respuesta ) }} Días

                                            @endif --}}
                                                </td>
                                            </tr>
                                                    
                                        @endif

                                        {{-- Días Restantes para Devengar, a partir de cuadratura o recepcio del documento (para el caso de los documentos antiguos ,antes del 22 de junio 2019) --}}
                                        {{-- @if ( ($documento->getUnaRelacionRecepcionValidada || $documento->validado != null  || ( $documento->id_visto_bueno != null && $documento->getVistoBueno->id_registrador_memo_respuesta != null ) ) --}}
                                        @if ( $documento->validado != null && ($documento->id_estado == 1 && $documento->id_devengador == null) )

                                            <tr>
                                                <th class="th_sigfe text-center" >Días Restantes para Devengar</th>
                                                <td class="text-center"><i class="fas fa-clipboard-check fa-lg fa-bounce" 
                                                                            style="color:{{ $documento->getColorPorDias() }} !important"></i> 
                                            {{-- @if ( $documento->getUnaRelacionRecepcionValidada )

                                                {{ diferencia_dias_cuadratura( $documento->getUnaRelacionRecepcionValidada->created_at ,date('Y-m-d') ) }}

                                            @elseif ( $documento->validado != null ) --}}

                                                {{ diferencia_dias_cuadratura( $documento->fecha_validado ,date('Y-m-d') ) }}

                                            {{-- @elseif ( $documento->id_visto_bueno != null )

                                                {{ diferencia_dias_cuadratura( $documento->getVistoBueno->fecha_memo_respuesta ,date('Y-m-d') ) }}

                                            @else

                                                {{ diferencia_dias_cuadratura( $documento->fecha_recepcion,date('Y-m-d') ) }}

                                            @endif --}}
                                            
                                                </td>
                                            </tr>

                                        @endif

                                        {{-- Información Devengado --}}
                                        @if ( $documento->id_estado != 1 && $documento->id_devengador != null)
                                            
                                            <tr>
                                                <th class="th_sigfe text-center" >Devengado</th>
                                                <td class="text-center">
                                                @if ( $documento->fecha_devengado != null )
                                                    {{ diaSemana($documento->fecha_devengado)." ".fecha_dmY($documento->fecha_devengado)." ".fechaHoraMinutoSegundo($documento->fecha_devengado) }}
                                                @endif
                                                </td>
                                            </tr>

                                            {{-- Diferencia Cuadratura y Devengo --}}
                                            {{-- @if ( $documento->getUnaRelacionRecepcionValidada || $documento->validado != null || ( $documento->id_visto_bueno != null && $documento->getVistoBueno->id_registrador_memo_respuesta != null ) ) --}}
                                            @if ( $documento->validado != null )
                                                    
                                                <tr>
                                                    <th class="th_sigfe text-center" >Diferencia Cuadratura y Devengado</th>
                                                    <td class="text-center"><i class="fas fa-calendar-alt" style="color: {{ $documento->getColorPorDiasDiferenciaCuadraturaDevengo() }} !important"></i>
                                                    {{-- @if ( $documento->getUnaRelacionRecepcionValidada )

                                                        {{ diferencia_dias( $documento->getUnaRelacionRecepcionValidada->created_at, $documento->fecha_devengado ) }} Días

                                                    @elseif ( $documento->validado != null ) --}}

                                                        {{ diferencia_dias( $documento->fecha_validado, $documento->fecha_devengado ) }} Días
                                                    
                                                    {{-- @elseif ( $documento->id_visto_bueno != null )

                                                        {{ diferencia_dias( $documento->getVistoBueno->fecha_memo_respuesta, $documento->fecha_devengado ) }} Días

                                                    @endif --}}
                                                    </td>
                                                </tr>

                                            @endif

                                        @endif

                                        @if ( $documento->id_estado == 3 && $documento->getComprobanteContableDocumento->count() > 0 )

                                            <tr>
                                                <th class="th_sigfe text-center" >Fecha Pago</th>
                                                <td class="text-center">
                                                @if ( $documento->getComprobanteContableDocumento->last()->getComprobanteContable )
                                                    {{ diaSemana($documento->getComprobanteContableDocumento->last()->getComprobanteContable->fecha_proceso)." ".
                                                        fecha_dmY($documento->getComprobanteContableDocumento->last()->getComprobanteContable->fecha_proceso) }}
                                                @endif
                                                </td>
                                            </tr>

                                        @endif



                                    </table>
                                </div>
                            </div>
                            @endrole

                        </div> <!-- / div class col-xs-6 -->

                    </div> <!-- / div class col-xs-12 -->


                </div>
                
                @role(['propietario','administrador'])
                @if ( count($documento->audits) > 0)
                    <div class="form-group row">
                        <div class="col-xs-12">	
                            {{-- <div class="form-group row">
                                <div class="col-xs-12">	 --}}
                                    <h4 class="headers-traceability">
                                        Auditoría Documento
                                    </h4>
                                    <hr style="margin-top:20px;">
                                {{-- </div>
                            </div> --}}

                            {{-- <div class="form-group">
                                <div class="col-xs-12 table-responsive"> --}}
                                    <table class="table table-striped table-bordered table-hover " id="tabla_auditoria">
                                        <thead>
                                            <tr>
                                                <th width="5%">ID</th>
                                                <th width="15%">Usuario</th>
                                                <th width="35%">Datos Antiguos</th>
                                                <th width="35%">Datos Nuevos</th>
                                                <th width="10%">Fecha</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            @forelse($documento->audits as $audit)
                                                <tr>
                                                    <td class="text-center">{{ $audit->id }}</td>
                                                    <td class="text-info" >{{ $audit->user ? $audit->user->name : '' }}</td>
                                                    <td class="text-left">
                                                        @foreach ($audit->old_values as $i => $value)
                                                            {{ $i }} : {{ $value }}<br>
                                                        @endforeach 
                                                    </td>
                                                    <td class="text-left">
                                                        @foreach ($audit->new_values as $i => $value)
                                                            {{ $i }} : {{ $value }}<br>
                                                        @endforeach   
                                                    </td>
                                                    <td class="text-light">
                                                        {{ diaSemana($audit->created_at)." ".fecha_dmY($audit->created_at)." ".fechaHoraMinutoSegundo($audit->created_at) }}
                                                    </td>
                                                </tr>
                                            @empty

                                            @endforelse
                                        </tbody>
                                    </table>
                                {{-- </div>
                            </div> --}}
                        </div>
                    </div>
                @endif
                @endrole

            </div>{{-- /modal-body --}}

            {{--
            <div class="modal-footer">
                <div class="btn-group">
                <button type="button" class="btn btn-xs btn-default" title="Cancelar" data-dismiss="modal" aria-hidden="true">Cancelar</button>
                </div>
            </div> --}} {{-- /modal-footer --}}
          	
		</div>
	</div>
</div>

 <script type="text/javascript">
    var tablaItemsView = $('#tabla_item_presupuestario').DataTable({
        "language": {
            "emptyTable": "No hay datos disponibles en la tabla",
        },
        "paging":   false,
        "ordering": false,
        "info":     false,
        "searching": false
    });
 
    var tablaArchivoView = $('#tabla_info_archivo').DataTable({
         "language": {
            "emptyTable": "No hay datos disponibles en la tabla",
        },
        "paging":   false,
        "ordering": false,
        "info":     false,
        "searching": false
    });

 	$(document).ready(function(){
         
  
 	});

    
 </script>