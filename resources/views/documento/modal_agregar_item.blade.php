<style>
#tabla_items_presupuestarios_wrapper > .row {
    padding-top: 6px;
    padding-bottom: 0px;
    /*
    background-color: #EFF3F8;
    */
    background-color: #e3f2fd;
    margin-left: 0.3px;
    margin-right: 0.5px;
    margin-bottom: -10px;
    margin-top: -10px;
}
</style>
<link href="{{ asset('sistema/css/modales.css') }} " rel="stylesheet" type="text/css"/>
<div class="modal fade" id="modalAgregarItemPresupuestario" role="dialog" aria-labelledby="myModalLabel3" aria-hidden="true">
	<div class="modal-dialog" style="width:1000px;">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" title="Cerrar" class="close" data-dismiss="modal" aria-hidden="true"></button>
				<h4 class="modal-title"><strong><i class="fa fa-list-ol"></i>&nbsp;&nbsp;Buscar Ítem Presupuestario</strong></h4>
            </div>
          
            <div class="modal-body">
                <div class="form-group row">
                    <div class="col-xs-12 table-responsive">
                        <table class="table table-striped table-bordered table-hover" id="tabla_items_presupuestarios" >
                            <thead>
                                <tr>
                                    <th class="col-sm-3">Código</th>
                                    <th class="col-sm-7">Clasificador Presupuestario</th>
                                    <th class="col-sm-2">Seleccionar</th>
                                </tr>
                            </thead>
                            <tbody>
                                @foreach ($itemsPresupuestarios as $item)
                                    <tr>
                                        <td class="col-sm-3">{{ $item->codigoSinPuntos() }}</td>
                                        <td class="col-sm-7">{{ $item->clasificador_presupuestario }}</td>
                                        <td class="col-sm-2">
                                            <div class="btn-group">
                                                <button type="button" title="Agregar " 
                                                class="btn btn-xs btn-success" onclick="agregarItemTabla({{ $item->id }});">
                                                <i class="fa fa-check"></i>&nbsp; Agregar</button>
                                            </div>
                                        </td>
                                    </tr>
                                @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>

                @if ( $docBodega != null )
                <div class="form-group row">
                    <div class="col-xs-12">	
                        <h4 class="headers-view">
                            Ítem Presupuestario Recepción @if( $docBodega->tieneArchivos )<a class="btn btn-info btn-xs fa-pulso pull-right" @if($docBodega->archivoRecepcionCargado == 0) href="{{ asset( $docBodega->ubicacionArchivoRecepcion ) }}" @else href="http://{{ $docBodega->ubicacionArchivoRecepcion }}" @endif
                                                                        title="Ver Archivo Recepción" target="_blank" ><i class="fas fa-file-invoice fa-lg "></i></a>@endif
                        </h4>
                        <hr>
                        <div class="form-group">
                            <div class="col-xs-12 table-responsive">
                                <table class="table table-striped table-bordered table-hover " id="tabla_wsitem_presupuestario">
                                    <thead>
                                        <tr>
                                            <th width="30%">Código</th>
                                            <th width="70%">Descripción Producto</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        @forelse($docBodega->getWsItemsPresupuestarios as $item)
                                            <tr>
                                                <td>{{ str_replace( '.', '', $item->item_presupuestario ) }}</td>
                                                <td>{{ $item->descripcion_articulo }}</td>
                                            </tr>
                                        @empty

                                        @endforelse
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
                @endif


            </div>{{-- /modal-body --}}
          
            <div class="modal-footer">
                <div class="btn-group">
                <button type="button" class="btn btn-default" title="Cancelar" data-dismiss="modal" aria-hidden="true">Cancelar</button>
                </div>
            </div> {{-- /modal-footer --}}
          	
		</div>
	</div>
</div>

 <script type="text/javascript">
 	$(document).ready(function(){
        let tablaItems = $('#tabla_items_presupuestarios').DataTable({
            "language": {
                "emptyTable":     "No hay datos disponibles en la tabla",
                "info":           "Mostrando registros del _START_ al _END_ de un total de _TOTAL_ registros",
                "infoEmpty":      "Showing 0 to 0 of 0 entries",
                "infoFiltered":   "(filtrado de un total de _MAX_ registros)",
                "infoPostFix":    "",
                "thousands":      ",",
                "lengthMenu":     "Mostrar _MENU_ registros",
                "loadingRecords": "Cargando...",
                "processing":     "Procesando...",
                "search":         "Buscar: ",
                "zeroRecords":    "No matching records found",
                "paginate": {
                    "first":      "Primero",
                    "last":       "Último",
                    "next":       "Siguiente",
                    "previous":   "Anterior"
                },
                "aria": {
                    "sortAscending":  ": Activar para ordenar la columna de manera ascendente",
                    "sortDescending": ": Activar para ordenar la columna de manera descendente"
                }
            },
            "lengthMenu": [
                [10, 15, 20, -1],
                [10, 15, 20, "Todos"] // change per page values here
            ],
            // set the initial value
            "displayLength": 10,
            "paging":   true,
            "ordering": true,
            "info":     true,
            "searching": true
        });

        jQuery('#tabla_items_presupuestarios_filter input').addClass("form-control input-small"); // modify table search input
        jQuery('#tabla_items_presupuestarios_wrapper .dataTables_length select').addClass("form-control input-small"); // modify table per page dropdown
        jQuery('#tabla_items_presupuestarios_wrapper .dataTables_length select').select2(); // initialize select2 dropdown

        $('#tabla_items_presupuestarios_column_toggler input[type="checkbox"]').change(function() {
            /* Get the DataTables object again - this is not a recreation, just a get of the object */
            var iCol = parseInt($(this).attr("data-column"));
            var bVis = oTable.fnSettings().aoColumns[iCol].bVisible;
            oTable.fnSetColumnVis(iCol, (bVis ? false : true));
        });
        
        console.dir({!! json_encode($docBodega) !!});
  
 	});

    function agregarItemTabla(idItem)
    {
        console.log('Modal : idItem : '+idItem);

        $.ajaxSetup({
            headers: {  'X-CSRF-TOKEN': '{{ csrf_token() }}'}
        });

        $.ajax({
            url:   '{{url('documentos/obtener_item/')}}/'+idItem,
            type:  'get',

            success: function(respuesta) {
                console.log('soy respuesta : '+ respuesta);
                console.log(respuesta);
                
                if ( respuesta.estado == 'error' ) {
                    toastr.error('No es posible realizar la acción'+'<br><strong>Error: '+ respuesta.mensaje +'</strong>', 'Atención', optionsToastr);
                } else if ( respuesta.estado == 'success') {
                    nuevaFilaItemsPresupuestarios(respuesta);
                    $("#modalAgregarItemPresupuestario").modal("hide");
                }
                
            }//succes
        }).fail( function(data) {
            toastr.error('No es posible realizar la acción', 'Atención', optionsToastr);
        })
        .always(function() {
            //listaCargaArchivo();
        });//ajax
    }
 </script>