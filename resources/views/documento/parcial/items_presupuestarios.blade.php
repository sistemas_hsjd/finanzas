<table class="table table-striped table-bordered table-hover " id="tabla_item_presupuestario">
    <thead>
        <tr>
            @if ( $documento->getFoliosSigfe->count() > 0 )
                <th width="20%">Código</th>
                <th width="40%">Descripción</th>
                <th width="20%">Monto</th>
                <th width="20%">ID SIGFE</th>
            @else
                <th width="30%">Código</th>
                <th width="50%">Descripción</th>
                <th width="20%">Monto</th>
            @endif
        </tr>
    </thead>
    <tbody>
        @if ( $documento->getFoliosSigfe->count() > 0 )
            @foreach ($documento->getFoliosSigfe as $folioSigfe)
                @forelse ( $folioSigfe->getDevengos as $devengo )
                    <tr>
                        <td>{{ $devengo->getItemPresupuestario->codigo() }}</td>
                        <td>{{ $devengo->getItemPresupuestario->clasificador_presupuestario }}</td>
                        <td class="text-right">$ {{ formatoMiles($devengo->monto) }}</td>
                        <td class="text-center">{{ $folioSigfe->id_sigfe }}</td>
                    </tr>
                @empty

                @endforelse
            @endforeach
        @else
            @forelse($documento->getDevengos as $devengo)
                <tr>
                    <td>{{ $devengo->getItemPresupuestario->codigo() }}</td>
                    <td>{{ $devengo->getItemPresupuestario->clasificador_presupuestario }}</td>
                    <td class="text-right">$ {{ formatoMiles($devengo->monto) }}</td>
                </tr>
            @empty

            @endforelse
        @endif
    </tbody>
</table>