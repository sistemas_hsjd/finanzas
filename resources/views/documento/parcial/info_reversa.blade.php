<table class="table table-bordered " id="tabla_info_pago">
    @foreach ($documento->getReversaComprobante as $reversa)
        <tr>
            <th class="th_sigfe" >Folio Sigfe</th>
            <td class="text-center">{{ $reversa->folio_sigfe }}</td>
            <th class="th_sigfe">Fecha Sigfe</th>
            <td class="text-center" >{{ fecha_dmY($reversa->fecha_sigfe) }}</td>
            <th class="th_sigfe">C. Contable</th>
            <td class="text-center" >{{ ($reversa->getCuentaContable) ? $reversa->getCuentaContable->codigo : '' }}</td>
            <th class="th_sigfe" >Comprobante</th>
            <td >{{ ($reversa->getComprobanteContable) ? $reversa->getComprobanteContable->getNumeroComprobante() : '' }}</td>
        </tr>

        <tr>
            <th class="th_sigfe" >Detalle</th>
            <td colspan="5">{{ $reversa->detalle_sigfe }}</td>
            <th class="th_sigfe" >Monto</th>
            <td >{{ formatoMiles($reversa->monto) }}</td>
        </tr>
        @if ( ! $loop->last )
            <tr>
                <td colspan="6" style="border-right: solid 1px white;border-left:solid 1px white;"></td>
            </tr>
        @endif
    @endforeach
</table>