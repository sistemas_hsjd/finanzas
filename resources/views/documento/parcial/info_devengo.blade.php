<table class="table table-bordered " id="tabla_info_devengo">
    @foreach ($documento->getFoliosSigfe as $folioSigfe)
        <tr>
            <th width="25%" class="th_sigfe" >ID SIGFE</th>
            <td width="25%" class="text-center">{{ $folioSigfe->id_sigfe }}</td>
            <th width="25%" class="th_sigfe">Fecha SIGFE</th>
            <td width="25%" class="text-center" >{{ fecha_dmY($folioSigfe->fecha_sigfe) }}</td>
        </tr>

        <tr>
            <th class="th_sigfe">Detalle</th>
            <td colspan="3">{{ $folioSigfe->detalle_sigfe }}</td>
        </tr>
        @if ( ! $loop->last )
            <tr>
                <td colspan="4" style="border-right: solid 1px white;border-left:solid 1px white;"></td>
            </tr>
        @endif
    @endforeach
</table>