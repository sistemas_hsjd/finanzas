<h4 class="form-section" style="color: #69aa46;margin-top: 0px;">Proveedor</h4>
<div class="form-group row">
    <label for="nombre_proveedor" class="col-sm-2 control-label label-form">Rut y Nombre Proveedor <span class="span-label">*</span></label>
    <div class="col-sm-8 form-validate">
        <input type="text" class="form-control" id="nombre_proveedor" name="nombre_proveedor" required placeholder="Buscador por Nombre y RUT" onchange="getProveedor(this);" >
    </div>
</div>

<div class="form-group row" >
    
    <div class="col-xs-8 col-sm-8 col-md-8 col-lg-8">
        <h4 class="form-section" style="color: #69aa46;">Documento</h4>

        <div class="form-group row">

            <div class="col-xs-4 col-sm-4 col-md-4 col-lg-4  form-validate">
                <label for="tipo_documento" class="label-form">Tipo Documento <span class="span-label">*</span></label>
                <select name="tipo_documento" id="tipo_documento" class="form-control select2" required onchange="setReemplaza(this);">
                    <option value=''>Seleccione</option>
                    @foreach ( $tiposDocumento as $tipoDocumento)
                        <option value="{{ $tipoDocumento->id }}">{{ $tipoDocumento->nombre }}</option>
                    @endforeach
                </select>
            </div>

        </div>
        
        <div class="form-group row">

            <div id="div_relacionado" class="col-xs-6 col-sm-6 col-md-6 col-lg-6 form-validate">
                @isset($tipoDoc)
                    @if ( $tipoDoc->id == 1 && $documentosProveedor->count() > 0 )

                        <label for="reemplaza" class="label-form"> Reemplaza a </label>                        
                        <select name="reemplaza" id="reemplaza" class="form-control select2" onchange="setDocumentoReemplazaFactura(this);">
                            <option value="">Seleccione</option>
                            @forelse ($documentosProveedor as $documentoProveedor)
                                <option id="docProv_{{ $documentoProveedor->id }}" value="{{ $documentoProveedor->id }}"
                                        data-datosdocumento="{!! htmlspecialchars( json_encode( $documentoProveedor->getDatosDocumentosProveedores() ), ENT_QUOTES, 'UTF-8') !!}" >
                                    {{ $documentoProveedor->getTipoDocumento->sigla }} N° {{ $documentoProveedor->numero_documento }} - {{ fecha_dmY($documentoProveedor->fecha_documento) }} - ${{ formatoMiles($documentoProveedor->total_documento) }}
                                </option>
                            @empty
                            @endforelse
                            </select>
                        </select>                        

                    @elseif ( ($tipoDoc->id == 4 || $tipoDoc->id == 5 || $tipoDoc->id == 10 || $tipoDoc->id == 11) && $documentosProveedor->count() > 0 )
                    
                        <label for="reemplaza" class="label-form"> Factura Asociada <span class="span-label">*</span></label>
                        <select name="reemplaza" id="reemplaza" class="form-control select2" required="required" onchange="setDocumentoNcNd(this);">
                            <option value="">Seleccione</option>
                            @forelse( $documentosProveedor as $documentoProveedor )
                                <option id="docProv_{{ $documentoProveedor->id }}" value="{{ $documentoProveedor->id }}"
                                        data-datosdocumento="{!! htmlspecialchars( json_encode( $documentoProveedor->getDatosDocumentosProveedores() ), ENT_QUOTES, 'UTF-8') !!}" >
                                    {{ $documentoProveedor->getTipoDocumento->sigla }} N° {{ $documentoProveedor->numero_documento }} - {{ fecha_dmY($documentoProveedor->fecha_documento) }} - ${{ formatoMiles($documentoProveedor->total_documento) }}
                                </option>
                            @empty
                            @endforelse

                        </select>

                    @elseif ( $tipoDoc->id == 8 && $documentosProveedor->count() > 0 )

                        <label for="reemplaza" class="label-form"> Factura Refacturada <span class="span-label">*</span></label>
                        <select name="reemplaza" id="reemplaza" class="form-control select2" required="required" >
                            <option value="">Seleccione</option>
                            @forelse( $documentosProveedor as $documentoProveedor )
                                <option value="{{ $documentoProveedor->id }}" >
                                    {{ $documentoProveedor->getTipoDocumento->sigla }} N° {{ $documentoProveedor->numero_documento }} - {{ fecha_dmY($documentoProveedor->fecha_documento) }} - ${{ formatoMiles($documentoProveedor->total_documento) }}
                                </option>
                            @empty
                            @endforelse
                            
                        </select>

                    @endif
                @endisset
            </div>

            <div class="col-xs-6 col-sm-6 col-md-6 col-lg-6 form-validate" id="div_rechazados_acepta" 
                @if ( isset($tipoDoc) && $tipoDoc->id != 1 && $docsRechazadosAceptaProv != null  && $docsRechazadosAceptaProv->count() == 0 ) 
                    style="display:none;" 
                @endif >

                @if ( isset($tipoDoc) &&  $tipoDoc->id == 1 && $docsRechazadosAceptaProv->count() > 0 )

                    <label for="reemplaza_archivo_acepta" class="label-form"> Reemplaza al Archivo Acepta Rechazado </label>
                    <select name="reemplaza_archivo_acepta" id="reemplaza_archivo_acepta" class="form-control select2" onchange="setDocumentoReemplazaRechazado(this);">
                        <option value="">Seleccione</option>
                        @forelse ($docsRechazadosAceptaProv as $rechazado)
                            <option id="docRechazadoProv_{{ $rechazado->id }}" value="{{ $rechazado->id }}" data-datosdocumento="{!! htmlspecialchars(json_encode($rechazado->getDatosDocumentosProveedores()), ENT_QUOTES, 'UTF-8') !!}" >
                                {{ $rechazado->getTipoDocumento->sigla }} N° {{ $rechazado->folio }} - {{ fecha_dmY($rechazado->emision) }} - ${{ formatoMiles($rechazado->monto_total) }}
                            </option>
                        @empty
                            
                        @endforelse
                    </select>
                    <span class="help-block">
                        Estos Archivos estan rechazados
                    </span>

                @endif
            </div>
        
        </div>

        <div class="form-group row">

            <div class="col-xs-3 col-sm-3 col-md-3 col-lg-3 form-validate">
                <label for="numero_documento" class="label-form">N° Documento <span class="span-label">*</span></label>
                <input type="text" class="form-control numero_documento" id="numero_documento" name="numero_documento" required >
            </div>

            <div class="col-xs-3 col-sm-3 col-md-3 col-lg-3 form-validate">
                <label for="fecha_documento" class="label-form">Fecha Documento <span class="span-label">*</span></label>
                <div class="input-group">
                    <input type="text" class="form-control fechas" id="fecha_documento" name="fecha_documento" 
                            required onchange="cambioFechaDocumento(this);" @if( isset($archivoAcepta) && fecha_dmY($archivoAcepta->emision) != '31/12/1969' ) readonly @endif >
                    <span class="input-group-addon" 
                            @if( ! isset($archivoAcepta) || ( isset($archivoAcepta) && fecha_dmY($archivoAcepta->emision) == '31/12/1969' ) ) onclick="fechaDocumento('fecha_documento');" @endif
                            style="cursor:pointer;"><i class="fa fa-calendar" aria-hidden="true" style="color: black;"></i></span>
                </div>
            </div>

            <div class="col-xs-3 col-sm-3 col-md-3 col-lg-3 form-validate">
                <label for="fecha_recepcion" class="label-form">Fecha Recepción  <span class="span-label">*</span></label>
                <div class="input-group">
                    <input type="text" class="form-control fechas" id="fecha_recepcion" name="fecha_recepcion" required onchange="cambioFechaRecepcion(this);">
                    <span class="input-group-addon " onclick="fechaDocumento('fecha_recepcion');" style="cursor:pointer;"><i class="fa fa-calendar" aria-hidden="true" style="color: black;"></i></span>
                </div>
            </div>

            <div class="col-xs-3 col-sm-3 col-md-3 col-lg-3 form-validate" >
                <label for="periodo" class="label-form">Periodo  <span class="span-label">*</span></label>
                <div class="input-group" id="div_periodo">
                    <input type="text" class="form-control fechas" id="periodo" name="periodo" required >
                    <span class="input-group-addon " onclick="fechaDocumento('periodo');" style="cursor:pointer;"><i class="fa fa-calendar" aria-hidden="true" style="color: black;"></i></span>
                </div>
            </div>

        </div>

        <div class="form-group row">       
            
            @if (  isset($archivoAcepta) )
                <div class="col-xs-3 col-sm-3 col-md-3 col-lg-3 form-validate">
                    <label for="fecha_publicacion" class="label-form">Publicación</label>
                    <div class="input-group">
                    <input type="text" class="form-control fechas" id="fecha_publicacion" name="fecha_publicacion" value="{{ fecha_dmY($archivoAcepta->publicacion) }}"  readonly >
                        <span class="input-group-addon"><i class="fa fa-calendar" aria-hidden="true" style="color: black;"></i></span>
                    </div>
                </div>
            @endif

        </div>


        <div class="form-group row">

            <div class="col-xs-4 col-sm-4 col-md-4 col-lg-4 form-validate">
                <label for="valor_total_documento" class="label-form">Valor Total <span class="span-label">*</span></label>
                <div class="input-group">
                    <span class="input-group-addon">$</span>
                    <input type="text" class="form-control numero_documento" id="valor_total_documento" name="valor_total_documento" required onkeyup="ingresoPesos(this);" autocomplete="off" >
                </div>
            </div>  

            <div id="div_valores_boleta" @if ( isset($tipoDoc) && $tipoDoc->id != 3 ) style="display:none" @endif>

                <div class="col-xs-4 col-sm-4 col-md-4 col-lg-4 form-validate">
                    <label for="impuesto" id="label_impuesto" class="label-form">10% Impuesto </label>
                    <div class="input-group">
                        <span class="input-group-addon">$</span>
                        <input type="text" class="form-control numero_documento" id="impuesto" name="impuesto" readonly autocomplete="off" >
                    </div>
                </div>

                <div class="col-xs-4 col-sm-4 col-md-4 col-lg-4 form-validate">
                    <label for="liquido" class="label-form">Líquido </label>
                    <div class="input-group">
                        <span class="input-group-addon">$</span>
                        <input type="text" class="form-control numero_documento" id="liquido" name="liquido" readonly autocomplete="off" >
                    </div>
                </div>

            </div>

        </div>
        

        <div class="form-group row">

            <div class="col-xs-4 col-sm-4 col-md-4 col-lg-4 form-validate">
                <label for="modalidad_compra" class="label-form">Modalidad de Compra <span class="span-label">*</span></label>
                <select name="modalidad_compra" id="modalidad_compra" class="form-control select2" required onchange="setOrdenCompra(this);">
                    <option value=''>Seleccione</option>
                    @foreach ( $modalidadesCompra as $modalidadCompra)
                        <option value="{{ $modalidadCompra->id }}">{{ $modalidadCompra->nombre }}</option>
                    @endforeach
                </select>
            </div>

            <div class="col-xs-4 col-sm-4 col-md-4 col-lg-4 form-validate" id="div_orden_compra">
                <label for="numero_orden_compra" class="label-form">N° Orden de Compra <span class="span-label">*</span></label>
                <input type="text" class="form-control" id="numero_orden_compra" name="numero_orden_compra" required >
            </div>

            <div class="col-xs-4 col-sm-4 col-md-4 col-lg-4 form-validate">
                <label for="numero_licitacion" class="label-form">N° Licitación </label>
                <input type="text" class="form-control" id="numero_licitacion" name="numero_licitacion" value="" >
            </div>

        </div>

        <div class="form-group row">

            <div class="col-xs-4 col-sm-4 col-md-4 col-lg-4 form-validate">
                <label for="tipo_informe" class="label-form">Tipo de Informe <span class="span-label">*</span></label>
                <select name="tipo_informe" id="tipo_informe" class="form-control select2" required onchange="setTipoInforme(this);">
                    <option value=''>Seleccione</option>
                    @foreach ( $tiposInforme as $tipoInforme)
                        <option value="{{ $tipoInforme->id }}">{{ $tipoInforme->nombre }}</option>
                    @endforeach
                </select>
            </div>

            <div class="col-xs-4 col-sm-4 col-md-4 col-lg-4 form-validate">
                <label for="tipo_adjudicacion" class="label-form">Tipo Adjudicación <span class="span-label">*</span></label>
                <select name="tipo_adjudicacion" id="tipo_adjudicacion" class="form-control select2" required>
                    <option value=''>Seleccione</option>
                    @foreach ( $tiposAdjudicacion as $tipoAdjudicacion)
                        <option value="{{ $tipoAdjudicacion->id }}">{{ $tipoAdjudicacion->nombre }}</option>
                    @endforeach
                </select>
            </div>  
                                      
        </div>

        <div class="form-group row">

            <div class="col-xs-8 col-sm-8 col-md-8 col-lg-8 form-validate">
                <label for="referente_tecnico" class="label-form">Referente Técnico </label>
                <select name="referente_tecnico" id="referente_tecnico" class="form-control select2" >
                    <option value=''>Seleccione</option>
                    @foreach ( $referentesTecnicos as $referenteTecnico)
                        <option value="{{ $referenteTecnico->id }}">{{ $referenteTecnico->nombre }} - {{ $referenteTecnico->responsable }}</option>
                    @endforeach
                </select>
            </div>

        </div>

        @if ( isset($archivoAcepta) && $archivoAcepta->getNotaCreditoMenorAlCienPorciento )
        <h5 style="color: black;"><strong>Nota de Crédito Asociada</strong> <a class="btn btn-warning btn-xs" href="http://windte1806.acepta.com/ca4webv3/PdfViewMedia?url={{ $archivoAcepta->getNotaCreditoMenorAlCienPorciento->uri }}&menuTitle=Papel%2520Carta"
            title="Ver Documento" target="_blank" ><i class="fa fa-eye"></i></a></h5>
        @endif

        @if ( isset($recepcionHermes) && $recepcionHermes != null )
        <h5 style="color: #3277cc;"><strong>Recepción disponible en Hermes</strong></h5>
        <textarea readonly class="form-control noresize">Monto : $ {{ formatoMiles($recepcionHermes->documento_total).PHP_EOL }}Fecha : {{ fecha_dmY($recepcionHermes->fecha_ingreso_recepcion).PHP_EOL }}N° Doc. : {{ $recepcionHermes->folio_documento.PHP_EOL }}Proveedor : {{ $recepcionHermes->rut_proveedor }} </textarea>
        @endif

        @if ( isset($archivoAcepta) )
        <div class="form-group row">
            <iframe class="col-lg-12 col-md-12 col-sm-12" src="http://windte1806.acepta.com/ca4webv3/PdfViewMedia?url={{ $archivoAcepta->uri }}">
                </iframe>
        </div>
        @endif
    </div> <!-- / div class col-xs-8 -->

    <div class="col-xs-4 col-sm-4 col-md-4 col-lg-4">
        <h4 class="form-section" style="color: #69aa46;">Información Proveedor</h4>
        <div id="informacion_proveedor">

            <div class="form-group row">

                <div class="col-xs-4 col-sm-4 col-md-4 col-lg-4 form-validate" id="setear_select_contrato">
                    <label for="contrato" class="label-form">Contrato<span class="span-label">*</span></label>
                    <select name="contrato" id="contrato" class="form-control select2" onchange="getDatosContrato(this);" >
                        <option value=''>Seleccione</option>
                        @if( isset($optionsContrato) )
                            {!! $optionsContrato !!}
                        @endif
                    </select>
                </div>

                <div class="ocultar_segun_contrato" style="display:none;">

                    <div class="col-xs-3 col-sm-3 col-md-3 col-lg-3 form-validate">
                        <label for="inicio_vigencia_contrato" class="label-form">Inicio Vigencia</label>
                        <input type="text" class="form-control" id="inicio_vigencia_contrato" name="inicio_vigencia_contrato" readonly >
                    </div>
                
                    <div class="col-xs-3 col-sm-3 col-md-3 col-lg-3 form-validate">
                        <label for="fin_vigencia_contrato" class="label-form">Fin Vigencia</label>
                        <input type="text" class="form-control" id="fin_vigencia_contrato" name="fin_vigencia_contrato" readonly >
                    </div>
                
                </div>

            </div>

            <div class="ocultar_segun_contrato" style="display:none;">

                <div class="form-group row">

                    <div class="col-xs-4 col-sm-4 col-md-4 col-lg-4 form-validate">
                        <label for="monto_contrato" class="label-form">Monto</label>
                        <div class="input-group">
                            <span class="input-group-addon">$</span>
                            <input type="text" class="form-control" id="monto_contrato" name="monto_contrato" readonly >
                        </div>
                    </div>
                
                    <div class="col-xs-4 col-sm-4 col-md-4 col-lg-4 form-validate">
                        <label for="saldo_contrato" class="label-form">Saldo Contrato</label>
                        <div class="input-group">
                            <span class="input-group-addon">$</span>
                            <input type="text" class="form-control" id="saldo_contrato" name="saldo_contrato" readonly >
                        </div>
                    </div>
                
                    <div class="col-xs-4 col-sm-4 col-md-4 col-lg-4 form-validate">
                        <label for="valor_hora_contrato" class="label-form">Valor Hora</label>
                        <div class="input-group">
                            <span class="input-group-addon">$</span>
                            <input type="text" class="form-control" id="valor_hora_contrato" name="valor_hora_contrato" readonly >
                        </div>
                    </div>

                </div>

                <div class="form-group row" >
                    <div id="ocultar_segun_valor_hora" class="col-xs-4 col-sm-4 col-md-4 col-lg-4 form-validate">
                        <label for="horas_boleta" class="label-form">Horas <span class="span-label">*</span></label>                            
                        <input type="text" class="form-control numero_documento" id="horas_boleta" name="horas_boleta" required onkeyup="ingresoHoras(this);" autocomplete="off" >
                    </div>
                </div>

                <div id="ocultar_segun_saldos_preventivos_correctivos">

                    <div class="form-group row" >

                        <div class="col-xs-6 col-sm-6 col-md-6 col-lg-6 form-validate">
                            <label for="monto_preventivo" class="label-form">Monto Preventivo</label>
                            <div class="input-group">
                                <span class="input-group-addon">$</span>
                                <input type="text" class="form-control" id="monto_preventivo" name="monto_preventivo" readonly >
                            </div>
                        </div>
                        
                        <div class="col-xs-6 col-sm-6 col-md-6 col-lg-6 form-validate">
                            <label for="monto_correctivo" class="label-form">Monto Correctivo</label>
                            <div class="input-group">
                                <span class="input-group-addon">$</span>
                                <input type="text" class="form-control" id="monto_correctivo" name="monto_correctivo" readonly >
                            </div>
                        </div>

                    </div>

                    <div class="form-group row" >

                        <div class="col-xs-6 col-sm-6 col-md-6 col-lg-6 form-validate">
                            <label for="saldo_preventivo" class="label-form">Saldo Preventivo</label>
                            <div class="input-group">
                                <span class="input-group-addon">$</span>
                                <input type="text" class="form-control" id="saldo_preventivo" name="saldo_preventivo" readonly >
                            </div>
                        </div>
                        
                        <div class="col-xs-6 col-sm-6 col-md-6 col-lg-6 form-validate">
                            <label for="saldo_correctivo" class="label-form">Saldo Correctivo</label>
                            <div class="input-group">
                                <span class="input-group-addon">$</span>
                                <input type="text" class="form-control" id="saldo_correctivo" name="saldo_correctivo" readonly >
                            </div>
                        </div>
                    </div>

                    <div class="form-group row">
                        <label class="col-md-3 label-form" >Saldo a ocupar</label>
                        <div class="col-md-9 form-validate" style="margin-top: -8px;">
                            <div class="radio-list">

                                <label class="radio-inline">
                                    <div class="radio" >
                                        <span class=""><input type="radio" name="montoOcupar" id="ocuparPreventivo" value="preventivo" checked="" onchange="montoOcuparPreventivoCorrectivo();" ></span>
                                    </div>Preventivo</label>

                                <label class="radio-inline">
                                    <div class="radio" >
                                        <span class=""><input type="radio" name="montoOcupar" id="ocuparCorrectivo" value="correctivo" checked="" onchange="montoOcuparPreventivoCorrectivo();" ></span>
                                    </div>Correctivo</label>

                            </div>
                        </div>
                    </div>

                </div> <!-- /ocultar_segun_saldos_preventivos_correctivos -->

            </div>
        </div> <!-- /informacion_proveedor -->

        <div id="informacion_orden_compra">

            <div class="form-group row">

                <div class="col-xs-5 col-sm-5 col-md-5 col-lg-5 form-validate" id="setear_select_oc_boleta">
                    <label for="orden_compra" class="label-form">Orden de Compra<span class="span-label">*</span></label>
                    <select name="orden_compra" id="orden_compra" class="form-control select2" onchange="getDatosOc(this);" >
                        <option value=''>Seleccione</option>
                        @if( isset($optionsOc) )
                            {!! $optionsOc !!}
                        @endif
                    </select>
                </div>

                <div class="ocultar_segun_oc" style="display:none;">

                    <div class="col-xs-3 col-sm-3 col-md-3 col-lg-3 form-validate">
                        <label for="inicio_vigencia_orden_compra" class="label-form">Inicio Vigencia</label>
                        <input type="text" class="form-control" id="inicio_vigencia_orden_compra" name="inicio_vigencia_orden_compra" readonly >
                    </div>
                    
                    <div class="col-xs-3 col-sm-3 col-md-3 col-lg-3 form-validate">
                        <label for="fin_vigencia_orden_compra" class="label-form">Fin Vigencia</label>
                        <input type="text" class="form-control" id="fin_vigencia_orden_compra" name="fin_vigencia_orden_compra" readonly >
                    </div>

                </div>


            </div>

            <div class="ocultar_segun_oc" style="display:none;">

                <div class="form-group row">
                    
                    <div class="col-xs-4 col-sm-4 col-md-4 col-lg-4 form-validate">
                        <label for="monto_orden_compra" class="label-form">Monto</label>
                        <div class="input-group">
                            <span class="input-group-addon">$</span>
                            <input type="text" class="form-control" id="monto_orden_compra" name="monto_orden_compra" readonly >
                        </div>
                    </div>

                    <div class="col-xs-4 col-sm-4 col-md-4 col-lg-4 form-validate">
                        <label for="saldo_orden_compra" class="label-form">Saldo</label>
                        <div class="input-group">
                            <span class="input-group-addon">$</span>
                            <input type="text" class="form-control" id="saldo_orden_compra" name="saldo_orden_compra" readonly >
                        </div>
                    </div>

                    <div class="col-xs-3 col-sm-3 col-md-3 col-lg-3 form-validate">
                        <label for="fecha_orden_compra" class="label-form">Fecha</label>
                        <input type="text" class="form-control" id="fecha_orden_compra" name="fecha_orden_compra" readonly >
                    </div>

                </div>

            </div>

        </div> <!-- /informacion_orden_compra -->

        <div id="divBodega" 
            @if ( isset($opciones) && $opciones == '') style="display:none;" @endif 
            @if ( ! isset($opciones) ) style="display:none;" @endif >

            <h4 class="form-section" style="color: #69aa46;">Documentos de Recepción</h4>
            <div class="form-group row">
                <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 form-validate">
                    <label for="doc_disponible_bodega" class="label-form">Disponibles</label>
                    <div id="divDocumentosRecepcion">
                        @if ( isset($opciones) && $opciones != '')
                            <select name="doc_disponible_bodega" id="doc_disponible_bodega" class="form-control select2" onchange="setDocumento(this);" >
                                <option value="">Seleccione</option>
                                {!! $opciones !!}
                            </select>
                        @endif
                    </div>
                </div>
            </div>
        </div>

        <div id="informacion_recepcion" style="display:none;">

            <div class="form-group row">
                <div class="col-xs-4 col-sm-4 col-md-4 col-lg-4 form-validate">
                    <label for="orden_compra_prov" class="label-form">Orden de Compra</label>
                    <input type="text" class="form-control " id="orden_compra_prov" 
                            name="orden_compra_prov" readonly >
                </div>
            
                {{-- <div class="col-xs-3 col-sm-3 col-md-3 col-lg-3 form-validate">
                    <label for="fecha_oc" class="label-form">Fecha Orden de Compra</label>
                    <input type="text" class="form-control" id="fecha_oc" name="fecha_oc" readonly >
                </div> --}}
            
                <div class="col-xs-4 col-sm-4 col-md-4 col-lg-4 form-validate">
                    <label for="monto_maximo_oc" class="label-form">Monto Máximo OC</label>
                    <div class="input-group">
                        <span class="input-group-addon">$</span>
                        <input type="text" class="form-control" id="monto_maximo_oc" name="monto_maximo_oc" readonly >
                    </div>
                </div>

                <div class="col-xs-4 col-sm-64col-md-4 col-lg-4 form-validate">
                    <label for="saldo_oc" class="label-form">Saldo OC</label>
                    <div class="input-group">
                        <span class="input-group-addon">$</span>
                        <input type="text" class="form-control" id="saldo_oc" name="saldo_oc" readonly >
                    </div>
                </div>

            </div>

        </div> <!-- /informacion_recepcion -->

        <div id="divGuiasDespacho" 
            @if ( isset($opcionesGuias) && $opcionesGuias == '') style="display:none;" @endif 
            @if ( ! isset($opcionesGuias) ) style="display:none;" @endif>

            <h4 class="form-section" style="color: #69aa46;">Guías Despacho de Recepción</h4>
            <div class="form-group row">
                <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 form-validate" id="divGuiasRecepcionBodega">
                    <label for="guias_disponible_bodega" class="label-form">Disponibles</label>
                    <div id="divGuiasRecepcionBodega">
                        @if ( isset($opcionesGuias) && $opcionesGuias != '')
                            <select name="guias_disponible_bodega[]" id="guias_disponible_bodega" 
                                    class="form-control select2" multiple placeholder="Seleccione">
                                {!! $opcionesGuias !!}
                            </select>
                        @endif
                    </div>
                </div>
            </div>
            <div class="form-group row">
                <label for="total_monto_guias" class="col-xs-3 col-sm-3 col-md-3 col-lg-3 control-label label-form" style="text-align:left;">Total Guías</label>
                <div class="col-xs-7 col-sm-7 col-md-7 col-lg-7 form-validate">
                    <div class="input-group">
                        <span class="input-group-addon">$</span>
                        <input type="text" class="form-control " id="total_monto_guias" name="total_monto_guias" value="0" readonly>
                    </div>
                </div>
            </div>
        </div>


        <div id="divFacturasHermes" 
            @if ( isset($opcionesFacturasHermes) && $opcionesFacturasHermes == '') style="display:none;" @endif
            @if ( ! isset($opcionesFacturasHermes) ) style="display:none;" @endif>
            <h4 class="form-section" style="color: #69aa46;">Documentos Recepción Hermes</h4>
            <div class="form-group row">
                
                <div class="col-xs-12 form-validate">
                    <label for="doc_disponible_bodega" class="label-form">Disponibles</label>
                    @if ( isset($opcionesFacturasHermes) && $opcionesFacturasHermes != '')
                        <select name="factura_recepcion_hermes" id="factura_recepcion_hermes" class="form-control select2" onchange="setRecepcionHermes(this);" >
                            <option value="">Seleccione</option>
                            {!! $opcionesFacturasHermes !!}
                        </select>
                    @endif
                </div>

            </div>
        </div>

        <div id="divGuiasDespachoHermes" 
            @if ( isset($opcionesGuiasHermes) && $opcionesGuiasHermes == '') style="display:none;" @endif
            @if ( ! isset($opcionesGuiasHermes) ) style="display:none;" @endif>
            <h4 class="form-section" style="color: #69aa46;">Guías Despacho de Hermes</h4>
            <div class="form-group row">
                <label for="guias_disponible_hermes" class="col-xs-2 col-sm-2 col-md-2 col-lg-2 control-label label-form">Disponibles</label>
                <div class="col-xs-10 col-sm-10 col-md-10 col-lg-10 form-validate" id="divGuiasRecepcionHermes">
                    @if ( isset($opcionesGuiasHermes) && $opcionesGuiasHermes != '')
                        <select name="guias_disponible_hermes[]" id="guias_disponible_hermes" 
                                class="form-control select2" multiple placeholder="Seleccione">
                            {!! $opcionesGuiasHermes !!}
                        </select>
                    @endif
                </div>
            </div>
            <div class="form-group row">
                <label for="total_monto_guias_hermes" class="col-xs-3 col-sm-3 col-md-3 col-lg-3 control-label label-form" style="text-align:left;">Total Guías</label>
                <div class="col-xs-7 col-sm-7 col-md-7 col-lg-7 form-validate">
                    <div class="input-group">
                        <span class="input-group-addon">$</span>
                        <input type="text" class="form-control " id="total_monto_guias_hermes" name="total_monto_guias_hermes" value="0" readonly>
                    </div>
                </div>
            </div>
        </div>

    </div> <!-- / div class col-xs-4 -->
    
</div> <!-- form-group -->

<h4 class="form-section" style="color: #69aa46;">Documento en PDF</h4>
@if ( isset($archivoAcepta) && $archivoAcepta->uri != null)
<div class="form-group">
    <span style="font-size: 14px;margin-left: 10px;font-weight:bold;" class="label label-success">Considerar que la Factura digital en formato PDF ya se encuentra ingresada en el sistema.</span>
    <a class="btn btn-info btn-xs" href="http://windte1806.acepta.com/ca4webv3/PdfViewMedia?url={{ $archivoAcepta->uri }}&menuTitle=Papel%2520Carta"
                        title="Ver Factura" target="_blank" ><i class="fa fa-eye"></i></a>
</div>
@endif
<div class="form-group row">
    <div class="input-group col-sm-10 col-sm-offset-1">
        <span class="input-group-addon" id="inputGroupFileAddon01"><strong>Seleccione Documento</strong></span>
        <input type="file" class="form-control" accept=".pdf" name="archivo" id="archivo" aria-describedby="inputGroupFileAddon01" onchange="validarArchivo();">
    </div>
</div>

<div class="form-group row">
    <div id="div_archivo" class="col-xs-12 table-responsive" style="display: none;">
        <table class="table table-striped table-bordered table-hover " id="tabla_info_archivo">
            <thead>
                <tr>
                    <th width="40%">Nombre</th>
                    <th width="20%">Peso</th>
                    <th width="40%">Tipo</th>
                </tr>
            </thead>
            <tbody>
            </tbody>
        </table>
    </div>
</div>

<h4 class="form-section" style="color: #69aa46;">Ítem Presupuestario</h4>
<div class="row btn-group col-xs-12">
    <button type="button" id="botonAgregarItem" title="Agregar Ítem Presupuestario" 
    class="btn btn-xs btn-primary col-xs-2" onclick="agregarItem();">
    Agregar Ítem Presupuestario </button>
</div>
<div class="form-group row">
    <div id="div_items" class="col-xs-12 table-responsive" style="display: none;" >
        <table class="table " id="tabla_item_presupuestario">
            
            <thead style="display: none;">
                <tr>
                    <th class="col-sm-2">&nbsp;</th>
                    <th class="col-sm-6">&nbsp;</th>
                    <th class="col-sm-3">&nbsp;</th>
                    <th class="col-sm-1">&nbsp;</th>
                </tr>
            </thead>
            <tbody>

            </tbody>
        </table>
    </div>
</div>


<h4 class="form-section" style="color: #69aa46;">Observaciones</h4>
<div class="form-group row">
    <div class="col-sm-12">
    <textarea class="form-control noresize" id="observacion_documento" name="observacion_documento">@if( isset($recepcionHermes) && $recepcionHermes != null )Recepción disponible en Hermes. {{ $recepcionHermes->extra }}@endif</textarea>
    </div>
</div>

{{-- directo_por_devengar --}}
@if ( ! isset($keyTr) )
<div class="form-group row">
    <div class="col-sm-12 text-right">
        <div class="custom-checkbox">
            <label >
                <strong>¿ Pasar directo a Por Devengar ?
                <input name="directo_por_devengar" id="directo_por_devengar"  type="checkbox"  />
                <span class="active btn btn-success" title="Pasar directo a Por Devengar"><i class="far fa-thumbs-up fa-lg"></i></span >
                <span class="not-active btn btn-danger" title="No pasar directo a Por Devengar" ><i class="far fa-thumbs-down fa-lg"></i></span >
                </strong>
            </label>
        </div>
    </div>
</div>
@endif

<input type="hidden" name="_token" value="{{ csrf_token() }}">