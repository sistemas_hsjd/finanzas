<table class="table table-bordered " id="tabla_info_pago">
    @foreach ($documento->getComprobanteContableDocumento as $comprobanteContableDocumento)
        @if ( $comprobanteContableDocumento->getComprobanteContable->id_tipo_comprobante_contable == 1 )
        <tr>
            <th  class="th_sigfe align_middle" >Tipo - N°</th>
            <td class="align_middle">{{ $comprobanteContableDocumento->getComprobanteContable->getTipoComprobante->nombre }} - {{ $comprobanteContableDocumento->getComprobanteContable->getNumeroComprobante() }}</td>

            <th  class="th_sigfe align_middle" >Monto</th>
            <td  class="align_middle">{{ formatoMiles($comprobanteContableDocumento->monto_comprobante) }}</td>

            <th  class="th_sigfe align_middle" >Folio SIGFE</th>
            <td  class="align_middle">{{ $comprobanteContableDocumento->getComprobanteContable->folio }}</td>

            <th  class="th_sigfe align_middle">F. Proceso</th>
            <td  class="align_middle" >{{ fecha_dmY($comprobanteContableDocumento->getComprobanteContable->fecha_proceso) }}</td>
        </tr>

        <tr>
            <th  class="th_sigfe align_middle">Medio Pago</th>
            <td  class="align_middle" >
                @if ( $comprobanteContableDocumento->getMedioPago )
                    {{ $comprobanteContableDocumento->getMedioPago->nombre }}
                @endif
            </td>
            <th  class="th_sigfe align_middle">N° Pago</th>
            <td  class="align_middle" >
                @if (  $comprobanteContableDocumento->numero_documento_pago )
                    {{ $comprobanteContableDocumento->numero_documento_pago }}
                @endif
            </td>
            <th  class="th_sigfe align_middle">F. Pago</th>
            <td  class="align_middle" >
                @if (  $comprobanteContableDocumento->fecha_emision_pago )
                    {{ fecha_dmY_hm( $comprobanteContableDocumento->fecha_emision_pago ) }}
                @endif
            </td>
            <th class="th_sigfe align_middle" >Cuenta Contable</th>
            <td class="align_middle"  >{{ ($comprobanteContableDocumento->getCuentaContable) ? $comprobanteContableDocumento->getCuentaContable->codigo : '' }}</td>
        </tr>

        <tr>
            <th class="th_sigfe align_middle" >Título</th>
            <td colspan="5" class="text-center" style="vertical-align:middle;">{{ $comprobanteContableDocumento->getComprobanteContable->titulo }}</td>
            <th class="th_sigfe align_middle">Cuenta Bancaria</th>
            <td class="align_middle" >
                {{ ($comprobanteContableDocumento->getCuentaBancaria) ? $comprobanteContableDocumento->getCuentaBancaria->getBanco->nombre : '' }}<br>
                {{ ($comprobanteContableDocumento->getCuentaBancaria) ? $comprobanteContableDocumento->getCuentaBancaria->codigo : '' }}
            </td>
        </tr>
        @endif

        @if ( $comprobanteContableDocumento->getComprobanteContable->id_tipo_comprobante_contable != 1 )
        <tr>
            <th class="th_sigfe" >Tipo</th>
            <td class="text-center">{{ $comprobanteContableDocumento->getComprobanteContable->getTipoComprobante->nombre }} {{ $comprobanteContableDocumento->getComprobanteContable->getNumeroComprobante() }}</td>
            <th class="th_sigfe">Monto</th>
            <td class="text-center" >{{ formatoMiles($comprobanteContableDocumento->monto_comprobante) }}</td>
            <th class="th_sigfe" >Folio SIGFE</th>
            @if( $comprobanteContableDocumento->getComprobanteContable->folio )
                <td class="text-center">{{ $comprobanteContableDocumento->getComprobanteContable->folio }}</td>
            @endif
            @if( $comprobanteContableDocumento->folio )
                <td class="text-center">{{ $comprobanteContableDocumento->folio }}</td>
            @endif
            <th class="th_sigfe">F. Proceso</th>
            <td class="text-center" >{{ fecha_dmY($comprobanteContableDocumento->getComprobanteContable->fecha_proceso) }}</td>
        </tr>

            @if ( ! $comprobanteContableDocumento->getComprobanteContable->folio )
                <tr>
                    <th  class="th_sigfe">Medio Pago</th>
                    <td  class="text-center" >
                        @if ( $comprobanteContableDocumento->getMedioPago )
                            {{ $comprobanteContableDocumento->getMedioPago->nombre }}
                        @endif
                    </td>
                    <th  class="th_sigfe">N° Pago</th>
                    <td  class="text-center" >
                        @if (  $comprobanteContableDocumento->numero_documento_pago )
                            {{ $comprobanteContableDocumento->numero_documento_pago }}
                        @endif
                    </td>
                    <th  class="th_sigfe">F. Pago</th>
                    <td  class="text-center" >
                        @if (  $comprobanteContableDocumento->fecha_emision_pago )
                            {{ fecha_dmY_hm( $comprobanteContableDocumento->fecha_emision_pago ) }}
                        @endif
                    </td>
                    <th class="th_sigfe">C. Contable</th>
                    <td class="text-center" >
                        {{ ($comprobanteContableDocumento->getCuentaContable) ? $comprobanteContableDocumento->getCuentaContable->codigo : '' }}
                    </td>
                </tr>

                <tr>
                    <th class="th_sigfe" >Título</th>
                    <td colspan="5" class="text-center">{{ $comprobanteContableDocumento->getComprobanteContable->titulo }}</td>
                    <th class="th_sigfe">C. Bancaria</th>
                    <td class="text-center" >
                        {{ ($comprobanteContableDocumento->getCuentaBancaria) ? $comprobanteContableDocumento->getCuentaBancaria->getBanco->nombre : '' }}<br>
                        {{ ($comprobanteContableDocumento->getCuentaBancaria) ? $comprobanteContableDocumento->getCuentaBancaria->codigo : '' }}
                    </td>
                </tr>
            @else

                <tr>
                    <th class="th_sigfe" >Título</th>
                    <td colspan="7" class="text-center">{{ $comprobanteContableDocumento->getComprobanteContable->titulo }}</td>
                </tr>
            @endif
        @endif


        @if ( ! $loop->last )
            <tr>
                <td colspan="7" style="border-right: solid 1px white;border-left:solid 1px white;"></td>
            </tr>
            {{-- <hr> --}}
        @endif
    @endforeach
</table>