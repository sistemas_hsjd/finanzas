<link href="{{ asset('sistema/css/modales.css') }} " rel="stylesheet" type="text/css"/>
<div class="modal fade" id="modalEliminar" role="dialog" aria-labelledby="myModalLabel3" aria-hidden="true">
	<div class="modal-dialog" style="width:1000px;">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" title="Cerrar" class="close" data-dismiss="modal" aria-hidden="true"></button>
				<h4 class="modal-title" ><strong><i class="fa fa-files-o fa-lg"></i>&nbsp;&nbsp;Eliminar Documento</strong></h4>
            </div>
            <form action="{{ asset('documentos/eliminar') }}" method="post" class="horizontal-form" id="form-documentos">
                <div class="modal-body">
                    <div class="form-body">
                        <div class="form-group row">
                            <div class="col-xs-12">	
                                <h4 class="headers-delete">
                                    Proveedor
                                </h4>
                                <hr>
                                <div class="form-group">
                                    <label class="col-sm-2 control-label font-bold">Nombre Proveedor</label>
                                    <label class="col-sm-4 control-label">{{ $documento->getProveedor->nombre }}</label>
                                    <label class="col-sm-2 control-label font-bold">Rut Proveedor</label>
                                    <label class="col-sm-4 control-label">{{ $documento->getProveedor->rut }}</label>
                                </div>
                            </div>
                        </div>

                        <div class="form-group row">
                            <div class="col-xs-12">	
                                <h4 class="headers-delete">
                                    Documento
                                </h4>
                                <hr>
                                <div class="form-group">
                                    <label class="col-sm-2 control-label font-bold">Tipo Documento</label>
                                    <label class="col-sm-4 control-label ">{{ $documento->getTipoDocumento->nombre }}</label>
                                    {{-- Inicio Refacturación --}}
                                    @if ( $documento->id_relacionado != null )
                                        @if ( $documento->id_tipo_documento == 4 || $documento->id_tipo_documento == 5 || $documento->id_tipo_documento == 10 || $documento->id_tipo_documento == 11 )
                                            <label class="col-sm-2 control-label font-bold">Documento Relacionado</label>
                                            <label class="col-sm-4 control-label ">{{ $documento->getDocumentoRelacionado->numero_documento }}</label>
                                        @elseif ( $documento->id_tipo_documento > 0 )
                                            <label class="col-sm-2 control-label font-bold">Reemplaza a</label>
                                            <label class="col-sm-4 control-label ">{{ $documento->getDocumentoRelacionado->numero_documento }}</label>
                                        @endif
                                    @endif
                                    {{-- Fin Refacturación --}}
                                </div>
                            </div>
                        </div>

                        @if ( $documento->getArchivoAceptaRefacturado )
                            <div class="form-group row">
                                <div class="col-xs-12">
                                    <label class="col-sm-5 control-label font-bold">Reemplaza al Archivo Acepta Rechazado</label>
                                    <label class="col-sm-4 control-label ">{{ $documento->getArchivoAceptaRefacturado->folio }}</label>
                                </div>
                            </div>
                        @endif

                        <div class="form-group row">
                            <div class="col-xs-12">
                                <label class="col-sm-2 control-label font-bold">N° Documento</label>
                                <label class="col-sm-4 control-label ">{{ $documento->numero_documento }}</label>
                                <label class="col-sm-2 control-label font-bold">Modalidad de Compra</label>
                                <label class="col-sm-4 control-label ">{{ $documento->getModalidadCompra->nombre }}</label>
                            </div>
                        </div>

                        <div class="form-group row">
                            <div class="col-xs-12">
                                <label class="col-sm-2 control-label font-bold">Tipo de adjudicación</label>
                                <label class="col-sm-4 control-label ">{{ $documento->getTipoAdjudicacion->nombre }}</label>
                            </div>
                        </div>

                        <div class="form-group row">
                            <div class="col-xs-12">
                                <label class="col-sm-2 control-label font-bold">N° Documento Compra</label>
                                <label class="col-sm-4 control-label ">{{ $documento->documento_compra }}</label>
                                <label class="col-sm-2 control-label font-bold">Fecha de Documento</label>
                                <label class="col-sm-4 control-label ">{{ fecha_dmY($documento->fecha_documento) }}</label>
                            </div>
                        </div>

                        <div class="form-group row">
                            <div class="col-xs-12">
                                <label class="col-sm-2 control-label font-bold">Tipo de Informe</label>
                                <label class="col-sm-4 control-label ">{{ $documento->getTipoInforme->nombre }}</label>
                                <label class="col-sm-2 control-label font-bold">Responsable</label>
                                <label class="col-sm-4 control-label ">{{ $documento->getResponsable->name }}</label>
                            </div>
                        </div>

                        <div class="form-group row">
                            <div class="col-xs-12">
                                <label class="col-sm-2 control-label font-bold">Fecha de Recepción</label>
                                <label class="col-sm-4 control-label ">{{ fecha_dmY($documento->fecha_recepcion) }}</label>
                            </div>
                        </div>

                        <div class="form-group row">
                            <div class="col-xs-12">
                                <label class="col-sm-2 control-label font-bold">Valor Tot. Orig.</label>
                                <label class="col-sm-4 control-label ">$ {{ formatoMiles($documento->total_documento) }}</label>
                                <label class="col-sm-2 control-label font-bold">Valor Tot. Act.</label>
                                <label class="col-sm-4 control-label ">$ {{ formatoMiles($documento->total_documento_actualizado) }}</label>
                            </div>
                        </div>

                        <div class="form-group row">
                            <div class="col-xs-12">
                                <div>
                                    @if ( $documento->getServicio != null )
                                        <label class="col-sm-2 control-label font-bold">Referente Técnico</label>
                                        <label class="col-sm-4 control-label "><strong>{{ $documento->getServicio->nombre }}</strong> {{ $documento->getServicio->responsable }}</label>
                                    @endif
                                    <label class="col-sm-2 control-label font-bold">Fecha de Ingreso</label>
                                    <label class="col-sm-4 control-label ">{{ fecha_dmY($documento->fecha_ingreso)." ".fechaHoraMinutoSegundo($documento->fecha_ingreso) }}</label>
                                </div>
                            </div>
                        </div>

                        {{-- Falta "Factoring" --}}
                        @isset ($factoring)
                            <div class="form-group row">
                                <div class="col-xs-12">
                                    <div>
                                        <label class="col-sm-2 control-label font-bold">Nombre Factoring</label>
                                        <label class="col-sm-4 control-label "></label>
                                        <label class="col-sm-2 control-label font-bold">Rut Factoring</label>
                                        <label class="col-sm-4 control-label "></label>
                                    </div>
                                </div>
                            </div>
                        @endisset

                        <div class="form-group row">
                            <div class="col-xs-12">
                                <div>
                                    <label class="col-sm-2 control-label font-bold">Observaciones Factura:</label>
                                    <label class="col-sm-10 control-label ">{{ $documento->observacion }}</label>
                                </div>
                            </div>
                        </div>

                        <div class="form-group row">
                            <div class="col-xs-12">	
                                <h4 class="headers-delete">
                                    Responsables
                                </h4>
                                <hr>
                                <div class="form-group">
                                    <label class="col-sm-2 control-label font-bold">Ingresado por</label>
                                    <label class="col-sm-4 control-label">{{ $documento->getDigitador->name }}</label>
                                    @if ( $documento->getDevengador )
                                        <label class="col-sm-2 control-label font-bold">Devengado por</label>
                                        <label class="col-sm-4 control-label">{{ $documento->getDevengador->name }}</label>
                                    @endif
                                </div>
                            </div>
                        </div>

                        <div class="form-group row">
                            <div class="col-xs-12">	
                                <h4 class="headers-delete">
                                    Documentos adjuntos
                                </h4>
                                <hr>
                                <div class="form-group">
                                    <div class="col-xs-12 table-responsive">
                                        <table class="table table-striped table-bordered table-hover " id="tabla_info_archivo">
                                            <thead>
                                                <tr>
                                                    <th width="40%">Nombre del Archivo</th>
                                                    <th width="30%">Tipo</th>
                                                    <th width="10%">Extensión</th>
                                                    <th width="10%">Peso</th>
                                                    <th width="10%"><i class="fa fa-cog" ></i></th>
                                                </tr>
                                            </thead>
                                            <tbody>
                                                @forelse($documento->getArchivos as $archivo)
                                                    <tr>
                                                        <td>{{ $archivo->nombre }}</td>
                                                        <td>@if( $archivo->getTipoArchivo ){{ $archivo->getTipoArchivo->nombre }}@endif</td>
                                                        <td>{{ $archivo->extension }}</td>
                                                        <td>{{ pesoArchivoEnMB($archivo->peso) }}</td>
                                                        <td>
                                                            <a class="btn btn-success btn-xs" @if($archivo->cargado == 0) href="{{ asset( $archivo->ubicacion ) }}" @else href="http://{{ $archivo->ubicacion }}" @endif
                                                            title="Ver Archivo" target="_blank" ><i class="fa fa-eye"></i></a>
                                                        </td>
                                                    </tr>
                                                @empty

                                                @endforelse
                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                            </div>
                        </div>

                        @if ( $documento->getDocumentosRelacionados->count() > 0 || $documento->id_relacionado != null || $documento->getArchivoAceptaRefacturado )
                            <div class="form-group row">
                                <div class="col-xs-12">	
                                    <h4 class="headers-delete">
                                        Documentos Relacionados
                                    </h4>
                                    <hr>
                                    <div class="form-group">
                                        <div class="col-xs-12 table-responsive">
                                            <table class="table table-striped table-bordered table-hover " id="tabla_info_archivo">
                                                <thead>
                                                    <tr>
                                                        <th class="text-center">N° Doc.</th>
                                                        <th >Tipo Doc.</th>
                                                        <th class="text-right">Valor Total Doc.</th>
                                                    </tr>
                                                </thead>
                                                <tbody>
                                                    @forelse($documento->getDocumentosRelacionados as $doc)
                                                        <tr>
                                                            <td class="text-center">{{ $doc->numero_documento }}</td>
                                                            <td>
                                                                {{ $doc->getTipoDocumento->nombre }}
                                                                @if ( $doc->getArchivos->count() > 0 )
                                                                    <div class="btn-group pull-right">
                                                                        @foreach($doc->getArchivos as $archivo)
                                                                            <a class="btn btn-info btn-xs " 
                                                                                @if($archivo->cargado == 0) 
                                                                                    href="{{ asset( $archivo->ubicacion ) }}" 
                                                                                @else
                                                                                    href="http://{{ $archivo->ubicacion }}"
                                                                                @endif
                                                                                title="Ver Archivo @if( $archivo->getTipoArchivo ){{ $archivo->getTipoArchivo->nombre }}@endif" 
                                                                                target="_blank" ><i class="fa fa-eye"></i>
                                                                            </a>
                                                                        @endforeach
                                                                    </div>
                                                                @endif
                                                            </td>
                                                            <td class="text-right">$ {{ formatoMiles($doc->total_documento) }}</td>
                                                        </tr>
                                                    @empty
                                            
                                                    @endforelse

                                                    @if ( $documento->id_relacionado != null )
                                                        <tr>
                                                            <td class="text-center">{{ $documento->getDocumentoRelacionado->numero_documento }}</td>
                                                            <td>
                                                                {{ $documento->getDocumentoRelacionado->getTipoDocumento->nombre }}
                                                                @if ( $documento->getDocumentoRelacionado->getArchivos->count() > 0 )
                                                                    <div class="btn-group pull-right">
                                                                        @foreach($documento->getDocumentoRelacionado->getArchivos as $archivo)
                                                                            <a class="btn btn-info btn-xs " 
                                                                                @if($archivo->cargado == 0) 
                                                                                    href="{{ asset( $archivo->ubicacion ) }}" 
                                                                                @else
                                                                                    href="http://{{ $archivo->ubicacion }}"
                                                                                @endif
                                                                                title="Ver Archivo @if( $archivo->getTipoArchivo ){{ $archivo->getTipoArchivo->nombre }}@endif" 
                                                                                target="_blank" ><i class="fa fa-eye"></i>
                                                                            </a>
                                                                        @endforeach
                                                                    </div>
                                                                @endif
                                                            </td>
                                                            <td class="text-right">$ {{ formatoMiles($documento->getDocumentoRelacionado->total_documento) }}</td>
                                                        </tr>
                                                    @endif

                                                    @if ( $documento->getArchivoAceptaRefacturado )
                                                        <tr>
                                                            <td class="text-center">{{ $documento->getArchivoAceptaRefacturado->folio }}</td>
                                                            <td>
                                                                {{ $documento->getArchivoAceptaRefacturado->getTipoDocumento->nombre }} (Rechazado Acepta)
                                                                @if ( $documento->getArchivoAceptaRefacturado->uri != null )
                                                                    <div class="btn-group pull-right">
                                                                        <a class="btn btn-info btn-xs" href="http://windte1806.acepta.com/ca4webv3/PdfViewMedia?url={{ $documento->getArchivoAceptaRefacturado->uri }}&menuTitle=Papel%2520Carta"
                                                                                title="Ver Factura" target="_blank" ><i class="fa fa-eye"></i></a>
                                                                    </div>
                                                                @endif
                                                            </td>
                                                            <td class="text-right">$ {{ formatoMiles($documento->getArchivoAceptaRefacturado->monto_total) }}</td>
                                                        </tr>
                                                    @endif
                                                    @if ( $documento->montoAplicadoAlValorTotal() != 0 )
                                                        <tr>
                                                            <td colspan="2" class="text-right"><strong>Sumatoria de NC y ND aplicada al Valor Tot. Orig.</strong></td>
                                                            <td class="text-right">$ {{ formatoMiles($documento->montoAplicadoAlValorTotal()) }}</td>
                                                        </tr>
                                                    @endif
                                                </tbody>
                                            </table>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        @endif

                        <div class="form-group row">
                            <div class="col-xs-12">	
                                <h4 class="headers-delete">
                                    Ítem Presupuestario
                                </h4>
                                <hr>
                                <div class="form-group">
                                    <div class="col-xs-12 table-responsive">
                                        <table class="table table-striped table-bordered table-hover " id="tabla_item_presupuestario">
                                            <thead>
                                                <tr>
                                                    <th width="30%">Código</th>
                                                    <th width="50%">Descripción</th>
                                                    <th width="20%">Monto</th>
                                                </tr>
                                            </thead>
                                            <tbody>
                                                @forelse($documento->getDevengos as $devengo)
                                                    <tr>
                                                        <td>{{ $devengo->getItemPresupuestario->codigo() }}</td>
                                                        <td>{{ $devengo->getItemPresupuestario->clasificador_presupuestario }}</td>
                                                        <td class="text-right">$ {{ formatoMiles($devengo->monto) }}</td>
                                                    </tr>
                                                @empty

                                                @endforelse
                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="modal-footer form-actions right">
                    <input type="hidden" name="_token" value="{{ csrf_token() }}">
                    <input type="hidden" name="id_documento" value="{{ $documento->id }}">
                    <button type="button" title="Cancelar" class="btn btn-default" data-dismiss="modal">Cancelar</button>
                    <button type="submit" title="Eliminar Documento" class="btn btn-danger" id="botonEliminar"><i class="fa fa-trash"></i> Eliminar Documento</button>
                </div>
            </form>
        </div>
    </div>
</div>

<script>
    $(document).ready(function() {
        $("#form-documentos").validate({
            highlight: function(element) {
                $(element).closest('.form-validate').removeClass('has-success');
                $(element).closest('.form-validate').addClass('has-error');
            },
            unhighlight: function(element) {
                $(element).closest('.form-validate').removeClass('has-error');
                $(element).closest('.form-validate').addClass('has-success');
            },
            errorElement: 'span',
            errorClass: 'help-block',
            errorPlacement: function (error, element) {
                console.log(element);
                if (element.parent('.input-group').length) {
                    error.insertAfter(element.parent());
                } else {
                    error.insertAfter(element);
                }
            },
            rules: {
            },
            messages: {
            },

            //para enviar el formulario por ajax
            submitHandler: function(form) {
                esperandoEliminarDocumento();
                
                let formData = new FormData(form);
                $.ajax({
                    url: form.action,
                    type: form.method,
                    //data: $(form).serialize(),
                    data: formData,
                    processData: false,
                    contentType: false,
                    success: function(respuesta) {
                        console.log(respuesta);
                        if ( respuesta.estado_documento == 'error' ) {
                            toastr.error('No es posible realizar la acción'+'<br><strong>Error: '+ respuesta.mensaje_documento +'</strong>', 'Atención', optionsToastr);
                        } else if ( respuesta.estado_documento == 'success') {
                            toastr.success(respuesta.mensaje_documento, 'Atención', optionsToastr);
                            // Actualizar la vista
                            location.reload();
                        }
                    }            
                }).fail( function(respuesta) {//fail ajax
                    toastr.error('No es posible realizar la acción', 'Atención', optionsToastr);
                })
                .always(function() {
                    listoEliminarDocumento();
                });//ajax

            }

        });
    });

    function esperandoEliminarDocumento()
    {   
        $('.page-header-fixed *').css('cursor', 'wait');
        $("#botonEliminar").attr("disabled",true);
    }

    function listoEliminarDocumento()
    {
        $('.page-header-fixed *').css('cursor', '');
        $("#botonEliminar").attr("disabled",false);
    }
</script>