@extends('master')

@section('title', 'Ingreso de Documentos')

@push('css-head')
<!-- BEGIN PAGE LEVEL STYLES -->
<link rel="stylesheet" type="text/css" href="{{asset('assets/plugins/select2/select2_conquer.css')}}"/>
<link rel="stylesheet" href="{{asset('assets/plugins/data-tables/DT_bootstrap.css')}}"/>
<!-- END PAGE LEVEL STYLES -->
<style type="text/css">
    .label-form {
        font-weight: bold;
        color: black;
        text-align: right;
    }

    .span-label {
        color: red;
    }

    .fechas {
        cursor: pointer;
    }

    .td_items {
        text-align: right;
        font-weight: bold;
        padding-top: 15px !important;
    }

    .td_items_dos {
        padding-top: 15px !important;
    }

    #tabla_item_presupuestario {
        border-color: white;
    }

    .noresize {
        resize: none;
        height: 100px!important;
    }

    .form-section {
        font-weight: bold !important;
        color: #69aa46;
        border-bottom: 1px solid #cac1c1 !important;
    }

    .custom-checkbox {}

    /* oculto el input */
    .custom-checkbox input[type=checkbox] {
    display: none;
    }

    /* oculto el texto */
    .custom-checkbox span {
    display: none;
    }

    /* si está activo el input */
    .custom-checkbox input[type=checkbox]:checked + span {
    display: inline-block;
    }

    /* si está inactivo el input */
    .custom-checkbox input[type=checkbox]:not(:checked) + span + span {
    display: inline-block;
    }
</style>
@endpush

@section('content')
<!-- BEGIN CONTENT -->
<div class="page-content-wrapper">
	<div class="page-content-wrapper">
		<div class="page-content">
			<!-- BEGIN STYLE CUSTOMIZER -->
			
			<!-- END BEGIN STYLE CUSTOMIZER -->
			<!-- BEGIN PAGE HEADER-->
			<!--<div class="row">
				<div class="col-md-12">
					<h3 class="page-title">
					Usuarios <small>Agregar usuarios</small>
					</h3>
				</div>
			</div>-->
			<!-- END PAGE HEADER-->
			<!-- BEGIN PAGE CONTENT-->
			<div class="row">
				<div class="col-md-12">
					<!-- BEGIN EXAMPLE TABLE PORTLET-->
					<div class="portlet">
						<div class="portlet-title">
							<div class="caption">
								<i class="fa fa-files-o" style="color: black;"></i> Ingreso de Documentos
							</div>
						</div>
						<div class="portlet-body form">

							<!-- BEGIN FORM-->
							<form action="{{ asset('documentos') }}" method="post" class="horizontal-form" id="form-documentos" autocomplete="off">
								<div class="form-body">
                                    <div class="note note-danger" id="divErrores" style="display:none;">
                                        <h4 class="block" style="margin-bottom:5 px;">Debe Completar los siguientes campos: </h4>
                                        <ul id="ulErrores"></ul>
                                    </div>                                    

                                    @include('documento.parcial.crea_documento')

									<input type="hidden" name="_token" value="{{ csrf_token() }}">
								</div>
								<div class="form-actions right">
									<button  type="submit" title="Guardar" class="btn btn-info" id="botonGuardar"><i class="fa fa-check"></i> Guardar</button>
								</div>
							</form>
							<!-- END FORM-->
						</div>
					</div>
					<!-- END EXAMPLE TABLE PORTLET-->
				</div>
			</div>
			<!-- END PAGE CONTENT-->
		</div>
	</div>
</div>
<!-- END CONTENT -->

<div id="modal"></div>
<div id="modal2"></div>
@endsection

@push('script-footer')
	<!-- BEGIN PAGE LEVEL PLUGINS -->
	<script type="text/javascript" src="{{asset('assets/plugins/select2/select2.min.js')}}"></script>
	{{--<script type="text/javascript" src="{{ asset('assets/plugins/data-tables/jquery.dataTables.min.js') }}"></script>--}}
    <script type="text/javascript" src="{{ asset('datatables/jquery.dataTables.min.js') }}"></script>
	<script type="text/javascript" src="{{asset('assets/plugins/data-tables/DT_bootstrap.js')}}"></script>
	<!-- END PAGE LEVEL PLUGINS -->
	<!-- BEGIN PAGE LEVEL SCRIPTS -->
    <link rel="stylesheet" type="text/css" href="{{asset('assets/plugins/bootstrap-datepicker/css/datepicker.css')}}"/>

    <script type="text/javascript" src="{{asset('assets/plugins/bootstrap-datepicker/js/bootstrap-datepicker.js')}}"></script>
    <script type="text/javascript" src="{{asset('assets/plugins/bootstrap-datetimepicker/js/bootstrap-datetimepicker.js')}}"></script>

	<script src="{{asset('assets/scripts/table-advanced.js')}}"></script>
    <script src="{{asset('sistema/js/documentos_finanzas.js')}}?v={{rand()}}"></script>
	<script>

        var urlGetContratosProveedor = '{{ url('contratos/get_contratos_para_documentos/') }}';

        var urlGetOrdenesCompraProveedor = '{{ url('ordenes_compra/get_oc_para_boleta/') }}';

        var urlGetFacturasRechazadasAcepta = '{{ url('documentos/get_facturas_rechazadas_acepta/') }}';

        var urlGetItemsAutomaticos = '{{ url('documentos/get_items_presupuestarios_recepcion/') }}';

        // nuevas variables globales
        var urlGetGuiasDespachoBodega = '{{ url('documentos/get_guias_despacho_bodega_proveedor/') }}';
        
        // var urlGetUltimaOcProveedor = '{{ url('documentos/get_oc_proveedor/') }}';
        var urlGetDocumentosBodega = '{{ url('documentos/get_documentos_bodega_proveedor/') }}';
        
        var urlGetProveedor = '{{ url('documentos/get_proveedor/') }}';
        var selectTipoArchivo = '<select name="tipo_archivo" id="tipo_archivo" class="col-xs-12" required>';
        selectTipoArchivo += '<option value="">Seleccione</option>';
        @foreach ( $tiposArchivo as $tipoArchivo)
            selectTipoArchivo += '<option value="{{ $tipoArchivo->id }}">{{ $tipoArchivo->nombre }}</option>';
        @endforeach
        selectTipoArchivo += '</select>';
        var urlAgregarItem = '{{ url("documentos/modal/agregar_item") }}';
        var urlSetReemplaza = '{{ url('documentos/get_documentos_proveedor/') }}';
        var urlSetOrdenCompra = '{{ url('documentos/get_predeterminado_modalidad_compra/') }}';

        var arregloItems = [];
        var tablaArchivo = $('#tabla_info_archivo').DataTable({
            "paging":   false,
            "ordering": false,
            "info":     false,
            "searching": false
        });
        var tablaItemsPresupuestarios = $('#tabla_item_presupuestario').DataTable({
            "paging":   false,
            "ordering": false,
            "info":     false,
            "searching": false
        });

        $(document).ready(function() {
            App.init();
            //TableAdvanced.init();

            $('#fecha_recepcion').datepicker({
                format: 'dd/mm/yyyy',
                //endDate: new Date(),
                //"setDate": new Date(),
                //startDate: new Date(),
                autoclose: true,
                language: 'es'
            });

            $('#fecha_documento').datepicker({
                format: 'dd/mm/yyyy',
                // endDate: new Date(),
                endDate: '{{ date( "d/m/Y", strtotime( date('d-m-Y')."+ 30 days")) }}',
                autoclose: true,
                language: 'es'
            });

            $('#periodo').datepicker({
                format: 'mm/yyyy',
                // endDate: new Date(),
                autoclose: true,
                language: 'es',
                viewMode: 'months',
                minViewMode: 'months'
            });

            $(".numero_documento").keypress(function (key) {
                // window.console.log(key.charCode); //Descomentar para ver codigo
                if (
                    (key.charCode < 48 || key.charCode > 57)//números
                    && (key.charCode != 0) //borrar y enter
                    && (key.charCode != 44) //coma
                    && (key.charCode != 46) //punto
                    && (key.charCode != 45) //guion
                    && (key.charCode != 43) //suma
                    )
                    //console.log(key.charCode);
                    return false;
            });

            $("#nombre_proveedor").select2({
                ajax: {
                    cache: true,
                    allowClear: true,
                    //hidden : true,
                    url : '{{ url('buscador_proveedor/') }}',
                    dataType: 'json',
                    delay: 250,
                    data: function (params,page) {
                        //console.log("soy params : "+params);
                        //console.log("soy page : "+page);
                        var query = {
                            term: params,
                            page: params.page || 1
                        }
                        return query;
                    },
                    results: function (data) {
                        return {
                        results: data
                        };
                    },
                },
                minimumInputLength: 2,
            
            });

            $(".select2").select2();

            $("#documentos").addClass( "active" );
            $("#ingreso").addClass( "active" );
            $("#ingresar-li").addClass( "active" );
            $("#documentos-a").append( '<span class="selected"></span>' );
            // $("#ingreso-a").append( '<span class="selected"></span>' );


            $("#form-documentos").validate({
                highlight: function(element) {
                    $(element).closest('.form-validate').removeClass('has-success');
                    $(element).closest('.form-validate').addClass('has-error');
                },
                unhighlight: function(element) {
                    $(element).closest('.form-validate').removeClass('has-error');
                    $(element).closest('.form-validate').addClass('has-success');
                },
                errorElement: 'span',
                errorClass: 'help-block',
                errorPlacement: function (error, element) {
                    console.log(element);
                    if (element.parent('.input-group').length) {
                        error.insertAfter(element.parent());
                    } else {
                        error.insertAfter(element);
                    }
                },
                rules: {
                },
                messages: {
                },

                //para enviar el formulario por ajax
                submitHandler: function(form) {
                    esperandoIngresoDocumento();
                    if ( validaTotalDocumentoConItemsPresupuestario() && validaNumDocumentoAndLicitacion() && validaTotalGuiasConTotalDocumento() ) {
                        let formData = new FormData(form);
                        $.ajax({
                            url: form.action,
                            type: form.method,
                            //data: $(form).serialize(),
                            data: formData,
                            processData: false,
                            contentType: false,
                            success: function(respuesta) {
                                console.log(respuesta);
                                // return false;
                                

                                if ( respuesta.estado_documento == 'error' ) {
                                    toastr.error('No es posible realizar la acción'+'<br><strong>Error: '+ respuesta.mensaje_documento +'</strong>', 'Atención', optionsToastr);
                                } else if ( respuesta.estado_documento == 'success') {
                                    
                                    toastr.success(respuesta.mensaje_documento, 'Atención', optionsToastr);
                                    // Setear los elementos, dejar todo en blanco, para poder ingresar otro documento
                                    setDOM();
                                    if ( respuesta.estado_archivo == 'error' ) {
                                        toastr.error('No es posible realizar la acción'+'<br><strong>Error: '+ respuesta.mensaje_archivo +'</strong>', 'Atención', optionsToastr);
                                    } else if ( respuesta.estado_archivo == 'succes') {
                                        toastr.success(respuesta.mensaje_archivo, 'Atención', optionsToastr);
                                    }
                                    
                                }
                                
                            }            
                        }).fail( function(respuesta) {//fail ajax
                            if ( respuesta.status == 400 ) {
                                mostrarErroresValidator(respuesta);
                            } else if ( respuesta.status == 500 ) {
                                toastr.error('No es posible realizar la acción, error en el servidor', 'Atención', optionsToastr);
                            } else {
                                toastr.error('No es posible realizar la acción', 'Atención', optionsToastr);
                            }
                            
                        })
                        .always(function() {
                            listoIngresoDocumento();
                        });//ajax
                    } else {
                        listoIngresoDocumento();
                    }
                    
                }

            });

        });
        
</script>
@endpush
