<link href="{{ asset('sistema/css/modales.css') }} " rel="stylesheet" type="text/css"/>
<div class="modal fade" id="modalEliminar" role="dialog" aria-labelledby="myModalLabel3" aria-hidden="true">
	<div class="modal-dialog" style="width:1000px;">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" title="Cerrar" class="close" data-dismiss="modal" aria-hidden="true"></button>
				<h4 class="modal-title" ><strong><i class="fas fa-address-book"></i>&nbsp;&nbsp;Eliminar Unidad</strong></h4>
            </div>
            <form action="{{ asset('unidad/eliminar') }}" method="post" class="horizontal-form" id="form">
                <div class="modal-body">
                    <div class="form-group row">
                        <div class="col-xs-12">
                            <div class="form-group">
                                <label class="col-sm-2 control-label font-bold">Nombre</label>
                                <label class="col-sm-8 control-label">{{ $unidad->nombre }}</label>
                            </div>
                        </div>
                    </div>
                
                </div>{{-- /modal-body --}}

                <div class="modal-footer form-actions right">
                    <input type="hidden" name="_token" value="{{ csrf_token() }}">
                    <input type="hidden" name="_id" value="{{ $unidad->id }}">
                    <button type="button" title="Cancelar" class="btn btn-default" data-dismiss="modal">Cancelar</button>
                    <button type="submit" title="Eliminar Unidad" class="btn btn-danger" id="botonEliminar"><i class="fa fa-trash"></i> Eliminar Unidad</button>
                </div>
            </form>
          	
		</div>
	</div>
</div>

<script type="text/javascript">

 	$(document).ready(function(){
         
        $("#form").validate({
            highlight: function(element) {
                $(element).closest('.form-validate').removeClass('has-success');
                $(element).closest('.form-validate').addClass('has-error');
            },
            unhighlight: function(element) {
                $(element).closest('.form-validate').removeClass('has-error');
                $(element).closest('.form-validate').addClass('has-success');
            },
            errorElement: 'span',
            errorClass: 'help-block',
            errorPlacement: function (error, element) {
                console.log(element);
                if (element.parent('.input-group').length) {
                    error.insertAfter(element.parent());
                } else {
                    error.insertAfter(element);
                }
            },
            rules: {
            },
            messages: {
            },

            //para enviar el formulario por ajax
            submitHandler: function(form) {
                esperandoEliminar();
                
                let formData = new FormData(form);
                $.ajax({
                    url: form.action,
                    type: form.method,
                    //data: $(form).serialize(),
                    data: formData,
                    processData: false,
                    contentType: false,
                    success: function(respuesta) {
                        console.log(respuesta);
                        if ( respuesta.estado == 'error' ) {
                            toastr.error('No es posible realizar la acción'+'<br><strong>Error: '+ respuesta.mensaje +'</strong>', 'Atención', optionsToastr);
                        } else if ( respuesta.estado == 'success') {
                            toastr.success(respuesta.mensaje, 'Atención', optionsToastr);
                            // Quitar el item de la tabla principal
                            quitarElementoTabla(respuesta);
                            $("#modalEliminar").modal("hide");
                        }
                    }            
                }).fail( function(respuesta) {//fail ajax
                    toastr.error('No es posible realizar la acción', 'Atención', optionsToastr);
                })
                .always(function() {
                    listoEliminar();
                });//ajax

            }

        });
 	});

    function esperandoEliminar()
    {   
        $('.page-header-fixed *').css('cursor', 'wait');
        $("#botonEliminar").attr("disabled",true);
    }

    function listoEliminar()
    {
        $('.page-header-fixed *').css('cursor', '');
        $("#botonEliminar").attr("disabled",false);
    }

    function quitarElementoTabla(respuesta)
    {
        $('#tr_' + respuesta.id).fadeOut(400, function(){
            tablaPrincipal.row('#tr_' + respuesta.id ).remove().draw();
        });
    }
    
</script>