@extends('master')

@section('title', 'Carga Items SIGFE')

@push('css-head')
<!-- BEGIN PAGE LEVEL STYLES -->
<link rel="stylesheet" type="text/css" href="{{asset('assets/plugins/select2/select2_conquer.css')}}"/>
<link rel="stylesheet" href="{{asset('assets/plugins/data-tables/DT_bootstrap.css')}}"/>
<!-- END PAGE LEVEL STYLES -->

@endpush

@section('content')

<!-- BEGIN CONTENT -->

<div class="page-content-wrapper">
	<div class="page-content-wrapper">
		<div class="page-content">

			<!-- BEGIN PAGE CONTENT-->
			<div class="row">
				<div class="col-md-12">
					<!-- BEGIN EXAMPLE TABLE PORTLET-->
					<div class="portlet">
						<div class="portlet-title">
							<div class="caption">
								<i class="fas fa-money-bill"></i> <i class="fas fa fa-list-ol"></i> Carga Items SIGFE
							</div>
						</div>
						<div class="portlet-body">
                            <form action="{{url('devengar/upload/items_sigfe')}}" method="post" enctype="multipart/form-data" id="form_carga_archivo" class="form-horizontal">
                                <h4 class="form-section" style="color: #69aa46;"><strong>Documento en XLSX</strong> (Excel obtenido de SIGFE: Mayor Presupuestario)</h4>
                                <div class="row">
                                    <div class="input-group col-sm-6 col-sm-offset-1">
                                        <span class="input-group-addon" id="inputGroupFileAddon01"><strong>Seleccione Documento</strong></span>
                                        <input type="file" class="form-control" accept=".xlsx,.xls" name="archivo" id="archivo" aria-describedby="inputGroupFileAddon01">
                                    </div>
                                    
                                </div>
                                <div class="row" style="margin-top:5pt;">
                                    <div class="btn-group col-md-offset-6 ">
                                        <button type="button" id="botonCargaArchivo" title="Cargar Archivo" class="btn btn-success" onclick="cargarArchivo();">
                                        Cargar Archivo <i class="fa fa-plus"></i></button>
                                    </div>
                                </div>
                                
                            </form>
                            <div class="table-toolbar row">
                                <div id="resumen" class="col-md-12">

                                </div>
                            </div>

                            <div class="row" id="divTablaDocErrorItem" style="display:none;">
                                <div class="col-md-12">
                                    <div class="panel">
                                        <div class="panel-heading">
                                            <div class="caption">
                                                <strong style="color: #ea0000">Documentos con error de monto en item.</strong>
                                            </div>
                                        </div>
                                        <div class="panel-body">
                                            <div class="table-responsive">
                                                <table class="table table-striped table-bordered table-hover table-full-width" id="tabla_documentos_error_item">
                                                </table>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <div class="row" id="divTablaDocCambioItem" style="display:none;">
                                <div class="col-md-12">
                                    <div class="panel">
                                        <div class="panel-heading">
                                            <div class="caption">
                                                <strong style="color: #0089ea">Documentos con cambio de item.</strong>
                                            </div>
                                        </div>
                                        <div class="panel-body">
                                            <div class="table-responsive">
                                                <table class="table table-striped table-bordered table-hover table-full-width" id="tabla_documentos_cambio_item">
                                                </table>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <div class="row" id="divTablaDocNuevoItem" style="display:none;">
                                <div class="col-md-12">
                                    <div class="panel">
                                        <div class="panel-heading">
                                            <div class="caption">
                                                <strong style="color: #0089ea">Documentos con nuevo item.</strong>
                                            </div>
                                        </div>
                                        <div class="panel-body">
                                            <div class="table-responsive">
                                                <table class="table table-striped table-bordered table-hover table-full-width" id="tabla_documentos_nuevo_item">
                                                </table>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <div class="row" id="divTablaDocItemCorrecto" style="display:none;">
                                <div class="col-md-12">
                                    <div class="panel">
                                        <div class="panel-heading">
                                            <div class="caption">
                                                <strong style="color: #0089ea">Documentos con item correcto.</strong>
                                            </div>
                                        </div>
                                        <div class="panel-body">
                                            <div class="table-responsive">
                                                <table class="table table-striped table-bordered table-hover table-full-width" id="tabla_documentos_item_correcto">
                                                </table>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>


                            <div class="row" id="divTablaFolioNoEncontrado" style="display:none;">
                                <div class="col-md-12">
                                    <div class="panel">
                                        <div class="panel-heading">
                                            <div class="caption">
                                                <strong style="color: #4caf50">Folios SIGFE no encontrados.</strong>
                                            </div>
                                        </div>
                                        <div class="panel-body">
                                            <div class="table-responsive">
                                                <table class="table table-striped table-bordered table-hover table-full-width" id="tabla_folios_no_encontrados">
                                                </table>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <div class="row" id="divTablaDevengoNoEncontrado" style="display:none;">
                                <div class="col-md-12">
                                    <div class="panel">
                                        <div class="panel-heading">
                                            <div class="caption">
                                                <strong style="color: #4caf50">Folios SIGFE no encontrados o montos devengados superan el total del documento.</strong>
                                            </div>
                                        </div>
                                        <div class="panel-body">
                                            <div class="table-responsive">
                                                <table class="table table-striped table-bordered table-hover table-full-width" id="tabla_devengo_no_encontrado">
                                                </table>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>

						</div>
					</div>
					<!-- END EXAMPLE TABLE PORTLET-->
				</div>
			</div>
			<!-- END PAGE CONTENT-->
			
			
			
			
		</div> <!--/div class="page-content" -->
	</div> <!--/div class="page-content-wrapper" -->
</div><!--/div class="page-content-wrapper" -->
<!-- END CONTENT -->
<div id="modal"></div>
<div id="modal2"></div>
@endsection

@push('script-footer')
<!-- BEGIN PAGE LEVEL PLUGINS -->
<script type="text/javascript" src="{{ asset('assets/plugins/select2/select2.min.js') }} "></script>
{{--<script type="text/javascript" src="{{ asset('assets/plugins/data-tables/jquery.dataTables.min.js') }}"></script>--}}
<script type="text/javascript" src="{{ asset('datatables/jquery.dataTables.min.js') }}"></script>
<script type="text/javascript" src="{{ asset('assets/plugins/data-tables/DT_bootstrap.js') }}"></script>

<link rel="stylesheet" type="text/css" href="{{asset('assets/plugins/bootstrap-datepicker/css/datepicker.css')}}"/>

<script type="text/javascript" src="{{asset('assets/plugins/bootstrap-datepicker/js/bootstrap-datepicker.js')}}"></script>
<script type="text/javascript" src="{{asset('assets/plugins/bootstrap-datetimepicker/js/bootstrap-datetimepicker.js')}}"></script>


<!-- END PAGE LEVEL PLUGINS -->
<!-- BEGIN PAGE LEVEL SCRIPTS -->
<script src="{{ asset('assets/scripts/app.js') }} "></script>
<script src="{{ asset('assets/scripts/table-advanced.js') }} "></script>


<script type="text/javascript">
	$(document).ready(function() {
		App.init(); // initlayout and core plugins

   		$("#devengo").addClass( "active" );
		$("#devengo-upload-items-sigfe-li").addClass( "active" );
		$("#devengo-a").append( '<span class="selected"></span>' );

	});

    function validarArchivo()
    {
        let archivo = $('#archivo').val().replace(/.*(\/|\\)/, ''); //val();
        let extension = archivo.substring(archivo.lastIndexOf('.'));
        if ( archivo == '' ) {
            toastr.warning('No se ha seleccionado documento', 'Atención', optionsToastr);
            listaCargaArchivo();
            return false;
        } else {
            if ( extension != '.xlsx' && extension != '.xls' ) {
                toastr.warning('El archivo no tiene la extension correcta: '+'<strong>XLSX</strong>', 'Atención', optionsToastr);
                $('#archivo').val('');
                listaCargaArchivo();
                return false;
            } else {
                return true;
            }
        }
    }

    function cargarArchivo() 
    {
        esperandoCargaArchivo();

        if ( validarArchivo() ) {
            let formData = new FormData($('#form_carga_archivo')[0]);

            $.ajaxSetup({
                headers: {  'X-CSRF-TOKEN': '{{ csrf_token() }}'}
            });

            $.ajax({
                data: formData, 
                url:   '{{url('devengar/upload/items_sigfe')}}',
                type:  'post',
                processData: false,
                contentType: false,

                success: function(respuesta) {
                    console.log(respuesta);
                    // return false;

                    if ( respuesta.estado == 'error' ) {
                        toastr.error('No es posible realizar la acción'+'<br><strong>Error: '+ respuesta.mensaje +'</strong>', 'Atención', optionsToastr);
                    } else if ( respuesta.estado == 'success') {
                        $('#archivo').val('');
                        $('#form_carga_archivo').css('display','none');
                        toastr.success(respuesta.mensaje, 'Atención', optionsToastr);
                        $('#resumen').html(respuesta.resumen);
                        crearTablas(respuesta);
                    }
                }//succes
            }).fail( function(data) {
                toastr.error('No es posible realizar la acción', 'Atención', optionsToastr);
            })
            .always(function() {
                listaCargaArchivo();
            });//ajax
        }
        
    }

    function esperandoCargaArchivo()
    {   
        $('.page-header-fixed *').css('cursor', 'wait');
        $("#botonCargaArchivo").attr("disabled",true);
    }

    function listaCargaArchivo()
    {
        $('.page-header-fixed *').css('cursor', '');
        $("#botonCargaArchivo").attr("disabled",false);
    }

    function crearTablas(respuesta)
    {


        var tablaErrorSigfe = $('#tabla_documentos_error_item').DataTable({
            data: respuesta.datosTablaErrorItem,
            // Set rows IDs
            rowId: function(data) {
                // console.log('a :'+ JSON.stringify(a));
                // console.log(a);
                // console.dir(a);
                // console.log('a.id : '+a.DT_RowID);
                return 'tr_' + data.DT_RowID;
            },
            columns: [
                { title: "Proveedor" },
                { title: "Tipo Documento" },
                { title: "N° Documento" },
                // { title: "Tot. Orig.", className: "text-right" },
                // { title: "Tot. Act.", className: "text-right" },
                { title: "ID SIGFE" },
                { title: "Fecha SIGFE"},
                { title: "Item con error" },
                { title: "Monto SIGFE" },
                { title: "Monto SIFCON" },
                { title: "<div class='text-center'><i class='fa fa-cog'></i></div>" }
                // { title: "Doc.", className: "text-center" }
            ],
            "deferRender": true,
            "language": {
                "emptyTable":     "No hay datos disponibles en la tabla",
                "info":           "Mostrando registros del _START_ al _END_ de un total de _TOTAL_ registros",
                "infoEmpty":      "No hay datos disponibles para mostrar en la tabla",
                "infoFiltered":   "(filtrado de un total de _MAX_ registros)",
                "infoPostFix":    "",
                "thousands":      ",",
                "lengthMenu":     "Mostrar _MENU_ registros",
                "loadingRecords": "Cargando...",
                "processing":     "Procesando...",
                "search":         "Buscar: ",
                "zeroRecords":    "<i class='far fa-frown fa-lg'></i> No hay datos que coincidan con la búsqueda <i class='far fa-frown fa-lg'></i>",
                "paginate": {
                    "first":      "Primero",
                    "last":       "Último",
                    "next":       "Siguiente",
                    "previous":   "Anterior"
                },
                "aria": {
                    "sortAscending":  ": Activar para ordenar la columna de manera ascendente",
                    "sortDescending": ": Activar para ordenar la columna de manera descendente"
                }
            },
            "lengthMenu": [
                [10, 15, 20, -1],
                [10, 15, 20, "Todos"] // change per page values here
            ],
            // set the initial value
            "displayLength": 10,
            "paging":   true,
            "ordering": true,
            "info":     true,
            "searching": true,
            "orderClasses": false,
        });
        jQuery('#tabla_documentos_error_item_filter input').addClass("form-control input-large"); // modify table search input
        jQuery('#tabla_documentos_error_item_wrapper .dataTables_length select').addClass("form-control input-small"); // modify table per page dropdown
        jQuery('#tabla_documentos_error_item_wrapper .dataTables_length select').select2(); // initialize select2 dropdown

        $('#tabla_documentos_error_item_column_toggler input[type="checkbox"]').change(function() {
            /* Get the DataTables object again - this is not a recreation, just a get of the object */
            var iCol = parseInt($(this).attr("data-column"));
            var bVis = oTable.fnSettings().aoColumns[iCol].bVisible;
            oTable.fnSetColumnVis(iCol, (bVis ? false : true));
        });

        $('#divTablaDocErrorItem').css('display','block');


        var tablaCambioItem = $('#tabla_documentos_cambio_item').DataTable({
            data: respuesta.datosTablaCambioItem,
            // Set rows IDs
            rowId: function(data) {
                // console.log('a :'+ JSON.stringify(a));
                // console.log(a);
                // console.dir(a);
                // console.log('a.id : '+a.DT_RowID);
                return 'tr_' + data.DT_RowID;
            },
            columns: [
                { title: "Proveedor" },
                { title: "Tipo Documento" },
                { title: "N° Documento" },
                { title: "ID SIGFE" },
                { title: "Fecha SIGFE"},
                { title: "Item nuevo" },
                { title: "Item antiguo" },
                { title: "<div class='text-center'><i class='fa fa-cog'></i></div>" }
                // { title: "Doc.", className: "text-center" }
            ],
            "deferRender": true,
            "language": {
                "emptyTable":     "No hay datos disponibles en la tabla",
                "info":           "Mostrando registros del _START_ al _END_ de un total de _TOTAL_ registros",
                "infoEmpty":      "No hay datos disponibles para mostrar en la tabla",
                "infoFiltered":   "(filtrado de un total de _MAX_ registros)",
                "infoPostFix":    "",
                "thousands":      ",",
                "lengthMenu":     "Mostrar _MENU_ registros",
                "loadingRecords": "Cargando...",
                "processing":     "Procesando...",
                "search":         "Buscar: ",
                "zeroRecords":    "<i class='far fa-frown fa-lg'></i> No hay datos que coincidan con la búsqueda <i class='far fa-frown fa-lg'></i>",
                "paginate": {
                    "first":      "Primero",
                    "last":       "Último",
                    "next":       "Siguiente",
                    "previous":   "Anterior"
                },
                "aria": {
                    "sortAscending":  ": Activar para ordenar la columna de manera ascendente",
                    "sortDescending": ": Activar para ordenar la columna de manera descendente"
                }
            },
            "lengthMenu": [
                [10, 15, 20, -1],
                [10, 15, 20, "Todos"] // change per page values here
            ],
            // set the initial value
            "displayLength": 10,
            "paging":   true,
            "ordering": true,
            "info":     true,
            "searching": true,
            "orderClasses": false,
        });
        jQuery('#tabla_documentos_cambio_item_filter input').addClass("form-control input-large"); // modify table search input
        jQuery('#tabla_documentos_cambio_item_wrapper .dataTables_length select').addClass("form-control input-small"); // modify table per page dropdown
        jQuery('#tabla_documentos_cambio_item_wrapper .dataTables_length select').select2(); // initialize select2 dropdown

        $('#tabla_documentos_cambio_item_column_toggler input[type="checkbox"]').change(function() {
            /* Get the DataTables object again - this is not a recreation, just a get of the object */
            var iCol = parseInt($(this).attr("data-column"));
            var bVis = oTable.fnSettings().aoColumns[iCol].bVisible;
            oTable.fnSetColumnVis(iCol, (bVis ? false : true));
        });

        $('#divTablaDocCambioItem').css('display','block');


        var tablaNuevoItem = $('#tabla_documentos_nuevo_item').DataTable({
            data: respuesta.datosTablaNuevoItem,
            // Set rows IDs
            rowId: function(data) {
                return 'tr_' + data.DT_RowID;
            },
            columns: [
                { title: "Proveedor" },
                { title: "Tipo Documento" },
                { title: "N° Documento" },
                { title: "ID SIGFE" },
                { title: "Fecha SIGFE"},
                { title: "Item nuevo" },
                { title: "<div class='text-center'><i class='fa fa-cog'></i></div>" }
            ],
            "deferRender": true,
            "language": {
                "emptyTable":     "No hay datos disponibles en la tabla",
                "info":           "Mostrando registros del _START_ al _END_ de un total de _TOTAL_ registros",
                "infoEmpty":      "No hay datos disponibles para mostrar en la tabla",
                "infoFiltered":   "(filtrado de un total de _MAX_ registros)",
                "infoPostFix":    "",
                "thousands":      ",",
                "lengthMenu":     "Mostrar _MENU_ registros",
                "loadingRecords": "Cargando...",
                "processing":     "Procesando...",
                "search":         "Buscar: ",
                "zeroRecords":    "<i class='far fa-frown fa-lg'></i> No hay datos que coincidan con la búsqueda <i class='far fa-frown fa-lg'></i>",
                "paginate": {
                    "first":      "Primero",
                    "last":       "Último",
                    "next":       "Siguiente",
                    "previous":   "Anterior"
                },
                "aria": {
                    "sortAscending":  ": Activar para ordenar la columna de manera ascendente",
                    "sortDescending": ": Activar para ordenar la columna de manera descendente"
                }
            },
            "lengthMenu": [
                [10, 15, 20, -1],
                [10, 15, 20, "Todos"] // change per page values here
            ],
            // set the initial value
            "displayLength": 10,
            "paging":   true,
            "ordering": true,
            "info":     true,
            "searching": true,
            "orderClasses": false,
        });
        jQuery('#tabla_documentos_nuevo_item_filter input').addClass("form-control input-large"); // modify table search input
        jQuery('#tabla_documentos_nuevo_item_wrapper .dataTables_length select').addClass("form-control input-small"); // modify table per page dropdown
        jQuery('#tabla_documentos_nuevo_item_wrapper .dataTables_length select').select2(); // initialize select2 dropdown

        $('#tabla_documentos_nuevo_item_column_toggler input[type="checkbox"]').change(function() {
            /* Get the DataTables object again - this is not a recreation, just a get of the object */
            var iCol = parseInt($(this).attr("data-column"));
            var bVis = oTable.fnSettings().aoColumns[iCol].bVisible;
            oTable.fnSetColumnVis(iCol, (bVis ? false : true));
        });

        $('#divTablaDocNuevoItem').css('display','block');


        var tablaNuevoItem = $('#tabla_documentos_item_correcto').DataTable({
            data: respuesta.datosTablaItemCorrecto,
            // Set rows IDs
            rowId: function(data) {
                return 'tr_' + data.DT_RowID;
            },
            columns: [
                { title: "Proveedor" },
                { title: "Tipo Documento" },
                { title: "N° Documento" },
                { title: "ID SIGFE" },
                { title: "Fecha SIGFE"},
                { title: "Item" },
                { title: "<div class='text-center'><i class='fa fa-cog'></i></div>" }
            ],
            "deferRender": true,
            "language": {
                "emptyTable":     "No hay datos disponibles en la tabla",
                "info":           "Mostrando registros del _START_ al _END_ de un total de _TOTAL_ registros",
                "infoEmpty":      "No hay datos disponibles para mostrar en la tabla",
                "infoFiltered":   "(filtrado de un total de _MAX_ registros)",
                "infoPostFix":    "",
                "thousands":      ",",
                "lengthMenu":     "Mostrar _MENU_ registros",
                "loadingRecords": "Cargando...",
                "processing":     "Procesando...",
                "search":         "Buscar: ",
                "zeroRecords":    "<i class='far fa-frown fa-lg'></i> No hay datos que coincidan con la búsqueda <i class='far fa-frown fa-lg'></i>",
                "paginate": {
                    "first":      "Primero",
                    "last":       "Último",
                    "next":       "Siguiente",
                    "previous":   "Anterior"
                },
                "aria": {
                    "sortAscending":  ": Activar para ordenar la columna de manera ascendente",
                    "sortDescending": ": Activar para ordenar la columna de manera descendente"
                }
            },
            "lengthMenu": [
                [10, 15, 20, -1],
                [10, 15, 20, "Todos"] // change per page values here
            ],
            // set the initial value
            "displayLength": 10,
            "paging":   true,
            "ordering": true,
            "info":     true,
            "searching": true,
            "orderClasses": false,
        });
        jQuery('#tabla_documentos_item_correcto_filter input').addClass("form-control input-large"); // modify table search input
        jQuery('#tabla_documentos_item_correcto_wrapper .dataTables_length select').addClass("form-control input-small"); // modify table per page dropdown
        jQuery('#tabla_documentos_item_correcto_wrapper .dataTables_length select').select2(); // initialize select2 dropdown

        $('#tabla_documentos_item_correcto_column_toggler input[type="checkbox"]').change(function() {
            /* Get the DataTables object again - this is not a recreation, just a get of the object */
            var iCol = parseInt($(this).attr("data-column"));
            var bVis = oTable.fnSettings().aoColumns[iCol].bVisible;
            oTable.fnSetColumnVis(iCol, (bVis ? false : true));
        });

        $('#divTablaDocItemCorrecto').css('display','block');


        var tablaFolioNoEncontrado = $('#tabla_folios_no_encontrados').DataTable({
            data: respuesta.datosTablaFolioNoEncontrado,
            // Set rows IDs
            // rowId: function(data) {
            //     return 'tr_' + data.DT_RowID;
            // },
            columns: [
                { title: "ID SIGFE" },
                { title: "Fecha SIGFE"},
                { title: "Item" },
                { title: "Monto" },
            ],
            "deferRender": true,
            "language": {
                "emptyTable":     "No hay datos disponibles en la tabla",
                "info":           "Mostrando registros del _START_ al _END_ de un total de _TOTAL_ registros",
                "infoEmpty":      "No hay datos disponibles para mostrar en la tabla",
                "infoFiltered":   "(filtrado de un total de _MAX_ registros)",
                "infoPostFix":    "",
                "thousands":      ",",
                "lengthMenu":     "Mostrar _MENU_ registros",
                "loadingRecords": "Cargando...",
                "processing":     "Procesando...",
                "search":         "Buscar: ",
                "zeroRecords":    "<i class='far fa-frown fa-lg'></i> No hay datos que coincidan con la búsqueda <i class='far fa-frown fa-lg'></i>",
                "paginate": {
                    "first":      "Primero",
                    "last":       "Último",
                    "next":       "Siguiente",
                    "previous":   "Anterior"
                },
                "aria": {
                    "sortAscending":  ": Activar para ordenar la columna de manera ascendente",
                    "sortDescending": ": Activar para ordenar la columna de manera descendente"
                }
            },
            "lengthMenu": [
                [10, 15, 20, -1],
                [10, 15, 20, "Todos"] // change per page values here
            ],
            // set the initial value
            "displayLength": 10,
            "paging":   true,
            "ordering": true,
            "info":     true,
            "searching": true,
            "orderClasses": false,
        });
        jQuery('#tabla_folios_no_encontrados_filter input').addClass("form-control input-large"); // modify table search input
        jQuery('#tabla_folios_no_encontrados_wrapper .dataTables_length select').addClass("form-control input-small"); // modify table per page dropdown
        jQuery('#tabla_folios_no_encontrados_wrapper .dataTables_length select').select2(); // initialize select2 dropdown

        $('#tabla_folios_no_encontrados_column_toggler input[type="checkbox"]').change(function() {
            /* Get the DataTables object again - this is not a recreation, just a get of the object */
            var iCol = parseInt($(this).attr("data-column"));
            var bVis = oTable.fnSettings().aoColumns[iCol].bVisible;
            oTable.fnSetColumnVis(iCol, (bVis ? false : true));
        });

        $('#divTablaFolioNoEncontrado').css('display','block');


        var tablaDevengoNoEncontrado = $('#tabla_devengo_no_encontrado').DataTable({
            data: respuesta.datosTablaDevengoNoEncontrado,
            // Set rows IDs
            // rowId: function(data) {
            //     return 'tr_' + data.DT_RowID;
            // },
            columns: [
                { title: "ID SIGFE" },
                { title: "Fecha SIGFE"},
                { title: "Item" },
                { title: "Monto" },
            ],
            "deferRender": true,
            "language": {
                "emptyTable":     "No hay datos disponibles en la tabla",
                "info":           "Mostrando registros del _START_ al _END_ de un total de _TOTAL_ registros",
                "infoEmpty":      "No hay datos disponibles para mostrar en la tabla",
                "infoFiltered":   "(filtrado de un total de _MAX_ registros)",
                "infoPostFix":    "",
                "thousands":      ",",
                "lengthMenu":     "Mostrar _MENU_ registros",
                "loadingRecords": "Cargando...",
                "processing":     "Procesando...",
                "search":         "Buscar: ",
                "zeroRecords":    "<i class='far fa-frown fa-lg'></i> No hay datos que coincidan con la búsqueda <i class='far fa-frown fa-lg'></i>",
                "paginate": {
                    "first":      "Primero",
                    "last":       "Último",
                    "next":       "Siguiente",
                    "previous":   "Anterior"
                },
                "aria": {
                    "sortAscending":  ": Activar para ordenar la columna de manera ascendente",
                    "sortDescending": ": Activar para ordenar la columna de manera descendente"
                }
            },
            "lengthMenu": [
                [10, 15, 20, -1],
                [10, 15, 20, "Todos"] // change per page values here
            ],
            // set the initial value
            "displayLength": 10,
            "paging":   true,
            "ordering": true,
            "info":     true,
            "searching": true,
            "orderClasses": false,
        });
        jQuery('#tabla_devengo_no_encontrado_filter input').addClass("form-control input-large"); // modify table search input
        jQuery('#tabla_devengo_no_encontrado_wrapper .dataTables_length select').addClass("form-control input-small"); // modify table per page dropdown
        jQuery('#tabla_devengo_no_encontrado_wrapper .dataTables_length select').select2(); // initialize select2 dropdown

        $('#tabla_devengo_no_encontrado_column_toggler input[type="checkbox"]').change(function() {
            /* Get the DataTables object again - this is not a recreation, just a get of the object */
            var iCol = parseInt($(this).attr("data-column"));
            var bVis = oTable.fnSettings().aoColumns[iCol].bVisible;
            oTable.fnSetColumnVis(iCol, (bVis ? false : true));
        });

        $('#divTablaDevengoNoEncontrado').css('display','block');

    }

    function verDocumento(id)
    {
        $.get( '{{ url("documentos/modal/ver") }}/' + id, function( data ) {
            $( "#modal" ).html( data );
            $( "#modalVer" ).modal();
        });
    }

    function carga(idProveedor, idTipoDocumento, numeroDocumento, totalDocumento, idSigfe, fechaSigfe, keyTr)
    {
        console.log('Carga');
        console.log(idProveedor, idTipoDocumento, numeroDocumento, totalDocumento, idSigfe, fechaSigfe, keyTr);

        $.get( '{{ url("devengar/modal/carga_sigfe") }}/' + idProveedor + '/' + idTipoDocumento + '/' + 
                numeroDocumento + '/' + totalDocumento + '/' + idSigfe + '/' + fechaSigfe + '/' + keyTr  , function( data ) {
            $( "#modal" ).html( data );
            $( "#modalCargaSigfe" ).modal();
        });
    }

    function devengarDocumento(idDocumento, idFolioSigfe)
    {
        $('.page-header-fixed *').css('cursor', 'wait');
        $.ajax({
            type: "GET",
            url: '{{ url("devengar/modal/") }}/' + idDocumento +'/'+ idFolioSigfe,
            success: function(respuesta) {

                $( "#modal" ).html( respuesta );
                $('.page-header-fixed *').css('cursor', '');
                $( "#modalDevengar" ).modal();
               
            }
        });
    }
</script>

@endpush