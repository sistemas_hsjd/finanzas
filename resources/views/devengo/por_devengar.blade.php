@extends('master')

@section('title', 'Por Devengar')

@push('css-head')
<!-- BEGIN PAGE LEVEL STYLES -->
<link rel="stylesheet" type="text/css" href="{{asset('assets/plugins/select2/select2_conquer.css')}}"/>
<link rel="stylesheet" href="{{asset('assets/plugins/data-tables/DT_bootstrap.css')}}"/>
<!-- END PAGE LEVEL STYLES -->
<style>
    .label-form {
        font-weight: bold;
        color: black;
        text-align: right;
    }
</style>
@endpush

@section('content')

<!-- BEGIN CONTENT -->

<div class="page-content-wrapper">
	<div class="page-content-wrapper">
		<div class="page-content">

			<!-- BEGIN PAGE CONTENT-->
			<div class="row">
				<div class="col-md-12">
					<!-- BEGIN EXAMPLE TABLE PORTLET-->
					<div class="portlet">
						<div class="portlet-title">
							<div class="caption">
								<i class="fas fa-gavel fa-flip-horizontal"></i> Documentos por Devengar
							</div>
						</div>
						<div class="portlet-body">
                            <form action="{{url('devengar/version_2/filtrar')}}" method="post" enctype="multipart/form-data" id="filtros-docs" class="form-horizontal">
                                <h4 class="form-section" style="color: #69aa46;"><strong>Filtros</strong>
                                <div class="btn-group pull-right">
                                    
                                    <button class="btn btn-success btn-xs" title="Generar Excel" formaction="{{ url('devengar/version_2/excel') }}">
                                        <i class="fas fa-file-excel"></i> Exportar a Excel
                                    </button>

                                    <button class="btn btn-info btn-xs" title="Filtrar Listado" id="filtroListado" type="button">
                                            <i class="fas fa-search" style="color:black;" ></i> Filtrar
                                    </button>
                                </div>
                                </h4>
                                <div class="form-group row">

                                    <label for="filtro_fecha_a_utilizar" class="col-sm-1 control-label label-form">Fecha </label>
                                    <div class="col-sm-3">
                                        <select class="form-control select2_filtro " id="filtro_fecha_a_utilizar" name="filtro_fecha_a_utilizar" >
                                            @foreach ( $fechasUtilizar as $fechaUtilizar )
                                                <option value="{{ $fechaUtilizar->opcion }}" >{{ $fechaUtilizar->opcion }}</option>
                                            @endforeach
                                        </select>
                                    </div>

                                    <label for="filtro_fecha_inicio" class="col-sm-1 control-label label-form">Fecha Inicio</label>
                                    <div class="col-sm-3">
                                        <div class="input-group">
                                            <input type="text" class="form-control fechas" id="filtro_fecha_inicio" name="filtro_fecha_inicio" value="01/{{ date("m/Y") }}" >
                                            <span class="input-group-addon" onclick="fechaDocumento('filtro_fecha_inicio');" style="cursor:pointer;"><i class="fa fa-calendar" aria-hidden="true" style="color: black;"></i></span>
                                        </div>
                                    </div>

                                    <label for="filtro_fecha_termino" class="col-sm-1 control-label label-form">Fecha Termino</label>
                                    <div class="col-sm-3">
                                        <div class="input-group">
                                            <input type="text" class="form-control fechas" id="filtro_fecha_termino" name="filtro_fecha_termino" value="{{ date("d/m/Y") }}" >
                                            <span class="input-group-addon" onclick="fechaDocumento('filtro_fecha_termino');" style="cursor:pointer;"><i class="fa fa-calendar" aria-hidden="true" style="color: black;"></i></span>
                                        </div>
                                    </div>

                                </div>

                                <div class="form-group row">

                                    <label for="filtro_proveedor" class="col-sm-1 control-label label-form">Proveedor</label>
                                    <div class="col-sm-3">
                                        <select name="filtro_proveedor[]" id="filtro_proveedor" class="form-control select2_filtro" multiple style="width: 100%">
                                            @foreach ( $proveedores as $proveedor )
                                                <option value="{{ $proveedor->id }}" >{{ $proveedor->rut }} {{ $proveedor->nombre }}</option>
                                            @endforeach
                                        </select>
                                    </div>

                                    <label for="filtro_tipo_documento" class="col-sm-1 control-label label-form">Tipo de Documento</label>
                                    <div class="col-sm-3">
                                        <select name="filtro_tipo_documento[]" id="filtro_tipo_documento" class="form-control select2_filtro" multiple style="width: 100%">
                                            @foreach ( $tiposDocumento as $tipoDocumento )
                                                <option value="{{ $tipoDocumento->id }}" >{{ $tipoDocumento->nombre }}</option>
                                            @endforeach
                                        </select>
                                    </div>

                                    <label for="filtro_modalidad_compra" class="col-sm-1 control-label label-form">Modalidad de Compra</label>
                                    <div class="col-sm-3">
                                        <select name="filtro_modalidad_compra[]" id="filtro_modalidad_compra" class="form-control select2_filtro" multiple="multiple">
                                            @foreach ( $modalidadesCompra as $modalidadCompra )
                                                <option value="{{ $modalidadCompra->id }}" >{{ $modalidadCompra->nombre }}</option>
                                            @endforeach
                                        </select>
                                    </div>

                                </div>

                                <div class="form-group row">

                                    <label for="filtro_tipo_adjudicacion" class="col-sm-1 control-label label-form">Tipo de Adjudicación</label>
                                    <div class="col-sm-3">
                                        <select name="filtro_tipo_adjudicacion[]" id="filtro_tipo_adjudicacion" class="form-control select2_filtro" multiple="multiple">
                                            @foreach ( $tiposAdjudicacion as $tipoAdjudicacion )
                                                <option value="{{ $tipoAdjudicacion->id }}" >{{ $tipoAdjudicacion->nombre }}</option>
                                            @endforeach
                                        </select>
                                    </div>

                                    <label for="filtro_tipo_informe" class="col-sm-1 control-label label-form">Tipo de Informe</label>
                                    <div class="col-sm-3">
                                        <select name="filtro_tipo_informe[]" id="filtro_tipo_informe" class="form-control select2_filtro" multiple="multiple">
                                            @foreach ( $tiposInforme as $tipoInforme )
                                                <option value="{{ $tipoInforme->id }}" >{{ $tipoInforme->nombre }}</option>
                                            @endforeach
                                        </select>
                                    </div>

                                    <label for="filtro_responsable" class="col-sm-1 control-label label-form">Responsable</label>
                                    <div class="col-sm-3">
                                        <select name="filtro_responsable[]" id="filtro_responsable" class="form-control select2_filtro" multiple="multiple">
                                            @foreach ( $usuariosResponsables as $responsable )
                                                <option value="{{ $responsable->id }}" >{{ $responsable->name }}</option>
                                            @endforeach
                                        </select>
                                    </div>

                                </div>

                                <div class="form-group row">

                                    <label for="filtro_referente_tecnico" class="col-sm-1 control-label label-form">Referente Técnico</label>
                                    <div class="col-sm-3">
                                        <select name="filtro_referente_tecnico[]" id="filtro_referente_tecnico" class="form-control select2_filtro" multiple="multiple">
                                            @foreach ( $referentesTecnicos as $referenteTecnico )
                                                <option value="{{ $referenteTecnico->id }}" >{{ $referenteTecnico->nombre }} - {{ $referenteTecnico->responsable }}</option>
                                            @endforeach
                                        </select>
                                    </div>

                                    <label for="filtro_digitador" class="col-sm-1 control-label label-form">Digitador</label>
                                    <div class="col-sm-3">
                                        <select name="filtro_digitador[]" id="filtro_digitador" class="form-control select2_filtro" multiple="multiple">
                                            @foreach ( $usuariosDigitadores as $digitador )
                                                <option value="{{ $digitador->id }}" >{{ $digitador->name }}</option>
                                            @endforeach
                                        </select>
                                    </div>

                                    <label for="filtro_item_presupuestario" class="col-sm-1 control-label label-form">Ítem </label>
                                    <div class="col-sm-3">
                                        <select class="form-control select2_filtro " id="filtro_item_presupuestario" name="filtro_item_presupuestario[]" multiple="multiple" >
                                            @foreach ( $itemsPresupuestarios as $item )
                                                <option value="{{ $item->id }}" >{{ $item->codigo() }} {{ $item->clasificador_presupuestario }}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                    
                                </div>

                                <div class="form-group row">
                                    <label for="filtro_numero_documento" class="col-sm-1 control-label label-form">N° Documento</label>
                                    <div class="col-sm-3">
                                        <input type="text" class="form-control " id="filtro_numero_documento" name="filtro_numero_documento" >
                                    </div>
                                    
                                    <label for="total_por_devengar" class="col-sm-2 col-sm-offset-4 control-label label-form">Total Por Devengar</label>
                                    <div class="col-sm-2">
                                        <input type="text" class="form-control text-right" id="total_por_devengar" name="total_por_devengar" value="" readonly>
                                    </div>
                                </div>
                                
                            </form>
                            <div class="table-toolbar">
                                    
                            </div>
                            <div class="row">
                                <div class="col-md-12">
                                    <div class="panel">
                                        <div class="panel-body">
                                            <div class="table-responsive">
                                                <table class="table table-striped table-bordered table-hover table-full-width" id="tabla_documentos">
                                                </table>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
						</div>
					</div>
					<!-- END EXAMPLE TABLE PORTLET-->
				</div>
			</div>
			<!-- END PAGE CONTENT-->
			
			
			
			
		</div> <!--/div class="page-content" -->
	</div> <!--/div class="page-content-wrapper" -->
</div><!--/div class="page-content-wrapper" -->
<!-- END CONTENT -->
<div id="modal"></div>
<div id="modal2"></div>
@endsection

@push('script-footer')
<!-- BEGIN PAGE LEVEL PLUGINS -->
<script type="text/javascript" src="{{ asset('assets/plugins/select2/select2.min.js') }} "></script>
{{--<script type="text/javascript" src="{{ asset('assets/plugins/data-tables/jquery.dataTables.min.js') }}"></script>--}}
<script type="text/javascript" src="{{ asset('datatables/jquery.dataTables.min.js') }}"></script>
<script type="text/javascript" src="{{ asset('assets/plugins/data-tables/DT_bootstrap.js') }}"></script>

<link rel="stylesheet" type="text/css" href="{{asset('assets/plugins/bootstrap-datepicker/css/datepicker.css')}}"/>

<script type="text/javascript" src="{{asset('assets/plugins/bootstrap-datepicker/js/bootstrap-datepicker.js')}}"></script>
<script type="text/javascript" src="{{asset('assets/plugins/bootstrap-datetimepicker/js/bootstrap-datetimepicker.js')}}"></script>


<!-- END PAGE LEVEL PLUGINS -->
<!-- BEGIN PAGE LEVEL SCRIPTS -->
<script src="{{ asset('assets/scripts/app.js') }} "></script>

<script type="text/javascript">

    let tablaPrincipal = $('#tabla_documentos').DataTable({
        "processing": true,
        "serverSide": true,
        "ajax": {
            "url" : "{{ url('devengar/data_table_por_devengar_2') }}",
            "type" : "POST",
            "data": function(d){
                d.form = JSON.stringify( $("#filtros-docs").serializeArray() );
                // console.log('se envia:');
                // console.log(d);
            },
            "dataSrc": function ( json ) {
                // console.log(json);
                $('#total_por_devengar').val(json.totalPorDevengar);
                
                return json.data;
            },
        },
        // Set rows IDs
        rowId: function(data) {
            // console.log('a :'+ JSON.stringify(a));
            // console.log(a);
            // console.dir(a);
            // console.log('a.id : '+a.DT_RowID);
            return 'tr_' + data.DT_RowID;
        },
        columns: [
            { title: "Días", className: "text-center"  },
            { title: "Fecha Conformidad Documento", className: "text-center" },
            { title: "Responsable" },
            { title: "Tipo Informe" },
            { title: "Rut Proveedor" },
            { title: "Nombre Proveedor" },
            { title: "Tipo Documento" },
            { title: "N° Documento" },
            { title: "Modalidad Compra" },
            { title: "N° Documento Compra" },
            { title: "Ítem" },
            { title: "Tot. Orig.", className: "text-right" },
            { title: "Tot. Act.", className: "text-right" },
            { title: "<div class='text-center'><i class='fa fa-cog'></i></div>" }
        ],
        "deferRender": true,
        "language": {
            "emptyTable":     "No hay datos disponibles en la tabla",
            "info":           "Mostrando registros del _START_ al _END_ de un total de _TOTAL_ registros",
            "infoEmpty":      "No hay datos disponibles para mostrar en la tabla",
            "infoFiltered":   "(filtrado de un total de _MAX_ registros)",
            "infoPostFix":    "",
            "thousands":      ",",
            "lengthMenu":     "Mostrar _MENU_ registros",
            "loadingRecords": "Cargando...",
            "processing":     "Procesando...",
            "search":         "Buscar: ",
            "zeroRecords":    "<i class='far fa-frown fa-lg'></i> No hay datos que coincidan con la búsqueda <i class='far fa-frown fa-lg'></i>",
            "paginate": {
                "first":      "Primero",
                "last":       "Último",
                "next":       "Siguiente",
                "previous":   "Anterior"
            },
            "aria": {
                "sortAscending":  ": Activar para ordenar la columna de manera ascendente",
                "sortDescending": ": Activar para ordenar la columna de manera descendente"
            }
        },
        "lengthMenu": [
            [10, 15, 20, -1],
            [10, 15, 20, "Todos"] // change per page values here
        ],
        // set the initial value
        "displayLength": 10,
        "paging":   true,
        "ordering": true,
        "info":     true,
        "searching": true,
        "orderClasses": true,
        "cache": true,
    });

    $.fn.dataTable.ext.errMode = 'none';//quita los mensajes de error de la datatable ajax

    $('#tabla_documentos').on( 'error.dt', function ( e, settings, techNote, message ) {
		console.log( 'An error has been reported by DataTables: ', message );//para mostrar los mensajes de error en la consola del navegador
	} );

    jQuery('#tabla_documentos_filter input').addClass("form-control input-large"); // modify table search input
    jQuery('#tabla_documentos_wrapper .dataTables_length select').addClass("form-control input-small"); // modify table per page dropdown
    jQuery('#tabla_documentos_wrapper .dataTables_length select').select2(); // initialize select2 dropdown

    $('#tabla_documentos_column_toggler input[type="checkbox"]').change(function() {
        /* Get the DataTables object again - this is not a recreation, just a get of the object */
        var iCol = parseInt($(this).attr("data-column"));
        var bVis = oTable.fnSettings().aoColumns[iCol].bVisible;
        oTable.fnSetColumnVis(iCol, (bVis ? false : true));
    });

    $('#filtroListado').click(function(){
        $("#modalCarga").modal({backdrop: 'static', keyboard: false});
        $('#modalCargaImg').addClass('fa-pulso');
            
        $('#tabla_documentos').DataTable().ajax.reload( function ( json ) {
            $("#modalCarga").modal('toggle');
        });
    });

	$(document).ready(function() {
		App.init(); // initlayout and core plugins

   		$("#devengo").addClass( "active" );
		$("#devengar-dos-li").addClass( "active" );
		$("#devengo-a").append( '<span class="selected"></span>' );

        $('#filtro_fecha_inicio').datepicker({
            format: 'dd/mm/yyyy',
            endDate: new Date(),
            // startDate: '22/06/2019',
            autoclose: true,
            language: 'es'
        });

        $('#filtro_fecha_termino').datepicker({
            format: 'dd/mm/yyyy',
            //endDate: new Date(),
            //"setDate": new Date(),
            //startDate: new Date(),            
            autoclose: true,
            language: 'es'
        });

        $(".select2_filtro").select2({
            // allowClear: true,
        });

	});

    function fechaDocumento(id_fecha) {
        $('#' + id_fecha).datepicker("show");
    }

    function devengarDocumento(id)
    {
        // $.get( '{{ url("devengar/modal/") }}/' + id, function( data ) {
        //     $( "#modal" ).html( data );
        //     $( "#modalDevengar" ).modal();
        // });

        $('.page-header-fixed *').css('cursor', 'wait');
        $.ajax({
            type: "GET",
            url: '{{ url("devengar/modal/") }}/' + id,
            success: function(respuesta) {

                $( "#modal" ).html( respuesta );
                $('.page-header-fixed *').css('cursor', '');
                $( "#modalDevengar" ).modal();
                               
            }
        });
    }

    function editarDocumento(id)
    {
        $.get( '{{ url("documentos/modal/editar") }}/' + id +'/devengar' , function( data ) {
            $( "#modal" ).html( data );
            $( "#modalEditar" ).modal();
        });
    }

</script>

@endpush