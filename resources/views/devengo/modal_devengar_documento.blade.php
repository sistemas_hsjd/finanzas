<link href="{{ asset('sistema/css/modales.css') }}?v={{rand()}} " rel="stylesheet" type="text/css"/>
<div class="modal fade" id="modalDevengar" role="dialog" aria-labelledby="myModalLabel3" aria-hidden="true">
	<div class="modal-dialog" style="width:1100px;">
		<div class="modal-content">
            <div class="modal-header">
				<button type="button" title="Cerrar" class="close" data-dismiss="modal" aria-hidden="true"></button>
				<h4 class="modal-title" ><strong><i class="fas fa-gavel"></i>&nbsp;&nbsp;Devengar Documento</strong></h4>
            </div>

            <form action="{{ asset('devengar/devengar_documento') }}" method="post" class="horizontal-form" id="form-documentos" autocomplete="off">
                <div class="modal-body">
                
                    <div class="form-body">
                        <div class="note note-danger" id="divErrores" style="display:none;">
                            <h4 class="block" style="margin-bottom:5 px;">Debe Completar los siguientes campos: </h4>
                            <ul id="ulErrores"></ul>
                        </div>

                        @if ( $documento->reclamado == 1 )
                        <div class="form-group row">
                            <span style="font-size: 14px;margin-left: 10px;font-weight:bold;background-color: #d42700;" class="label label-danger" >Documento Reclamado</span>
                        </div>
                        @endif

                        <h4 class="form-section" style="color: #69aa46;margin-top: 0px;">Proveedor</h4>
                        <div class="form-group row">
                                <label for="nombre_proveedor" class="col-sm-3 control-label label-form">Rut y Nombre Proveedor <span class="span-label">*</span></label>
                                <div class="col-sm-8 form-validate">
                                    <input type="text" class="form-control" id="nombre_proveedor" name="nombre_proveedor" required placeholder="Buscador por Nombre y RUT" onchange="getProveedor(this);">
                                </div>
                            </div>

                        <h4 class="form-section" style="color: #69aa46;">Documento</h4>
                        <div class="form-group row">
                            <label for="tipo_documento" class="col-sm-2 control-label label-form">Tipo Documento <span class="span-label">*</span></label>
                            <div class="col-sm-4 form-validate">
                                <select name="tipo_documento" id="tipo_documento" class="form-control select2" required onchange="setReemplaza(this);">
                                <option value=''>Seleccione</option>
                                @foreach ( $tiposDocumento as $tipoDocumento)
                                    <option value="{{ $tipoDocumento->id }}">{{ $tipoDocumento->nombre }}</option>
                                @endforeach
                                </select>
                            </div>

                            <div id="div_relacionado">
                                @if ( $documento->id_tipo_documento == 1 )

                                    <label for="reemplaza" class="col-sm-2 control-label label-form"> Reemplaza a </label>
                                    <div class="col-sm-4">
                                        <select name="reemplaza" id="reemplaza" class="form-control select2" onchange="setDocumentoReemplazaFactura(this);">
                                            <option value="">Seleccione</option>
                                            @if ( $documento->getDocumentoRelacionado != null )
                                                <option id="docProv_{{ $documento->getDocumentoRelacionado->id }}" value="{{ $documento->getDocumentoRelacionado->id }}" selected
                                                        data-datosdocumento="{!! htmlspecialchars( json_encode( $documento->getDocumentoRelacionado->getDatosDocumentosProveedores() ), ENT_QUOTES, 'UTF-8') !!}" >
                                                    {{ $documento->getDocumentoRelacionado->getTipoDocumento->sigla }} N° {{ $documento->getDocumentoRelacionado->numero_documento }} - {{ fecha_dmY($documento->getDocumentoRelacionado->fecha_documento) }} - ${{ formatoMiles($documento->getDocumentoRelacionado->total_documento) }}
                                                </option>
                                            @endif
                                            @forelse ($documentosProveedor as $documentoProveedor)
                                                <option id="docProv_{{ $documentoProveedor->id }}" value="{{ $documentoProveedor->id }}"
                                                        data-datosdocumento="{!! htmlspecialchars( json_encode( $documentoProveedor->getDatosDocumentosProveedores() ), ENT_QUOTES, 'UTF-8') !!}" >
                                                    {{ $documentoProveedor->getTipoDocumento->sigla }} N° {{ $documentoProveedor->numero_documento }} - {{ fecha_dmY($documentoProveedor->fecha_documento) }} - ${{ formatoMiles($documentoProveedor->total_documento) }}
                                                </option>
                                            @empty
                                            @endforelse
                                            </select>
                                        </select>
                                    </div>

                                @elseif ( $documento->id_tipo_documento == 4 || $documento->id_tipo_documento == 5 || $documento->id_tipo_documento == 10 || $documento->id_tipo_documento == 11 )

                                    <label for="reemplaza" class="col-sm-2 control-label label-form"> Factura Asociada <span class="span-label">*</span> </label>
                                    <div class="col-sm-4 form-validate">
                                        <select name="reemplaza" id="reemplaza" class="form-control select2" required onchange="setDocumentoNcNd(this);">
                                        <option value="">Seleccione</option>
                                        @if ( $documento->getDocumentoRelacionado != null )
                                            <option id="docProv_{{ $documento->getDocumentoRelacionado->id }}" value="{{ $documento->getDocumentoRelacionado->id }}" selected 
                                                    data-datosdocumento="{!! htmlspecialchars( json_encode( $documento->getDocumentoRelacionado->getDatosDocumentosProveedores() ), ENT_QUOTES, 'UTF-8') !!}" >
                                                {{ $documento->getDocumentoRelacionado->getTipoDocumento->sigla }} N° {{ $documento->getDocumentoRelacionado->numero_documento }} - {{ fecha_dmY($documento->getDocumentoRelacionado->fecha_documento) }} - ${{ formatoMiles($documento->getDocumentoRelacionado->total_documento) }}
                                            </option>
                                        @endif
                                        
                                        @forelse ($documentosProveedor as $documentoProveedor)
                                            <option id="docProv_{{ $documentoProveedor->id }}" value="{{ $documentoProveedor->id }}" 
                                                    data-datosdocumento="{!! htmlspecialchars( json_encode( $documentoProveedor->getDatosDocumentosProveedores() ), ENT_QUOTES, 'UTF-8') !!}" >
                                                {{ $documentoProveedor->getTipoDocumento->sigla }} N° {{ $documentoProveedor->numero_documento }} - {{ fecha_dmY($documentoProveedor->fecha_documento) }} - ${{ formatoMiles($documentoProveedor->total_documento) }}
                                            </option>
                                        @empty
                                        @endforelse
                                        </select>
                                    </div>

                                @elseif ( $documento->id_tipo_documento == 8 )

                                    <label for="reemplaza" class="col-sm-2 control-label label-form"> Factura Refacturada <span class="span-label">*</span> </label>
                                    <div class="col-sm-4 form-validate">
                                        <select name="reemplaza" id="reemplaza" class="form-control select2" required>
                                        <option value="">Seleccione</option>
                                        @if ( $documento->getDocumentoRelacionado != null )
                                            <option value="{{ $documento->getDocumentoRelacionado->id }}" selected>
                                                {{ $documento->getDocumentoRelacionado->getTipoDocumento->sigla }} N° {{ $documento->getDocumentoRelacionado->numero_documento }} - {{ fecha_dmY($documento->getDocumentoRelacionado->fecha_documento) }} - ${{ formatoMiles($documento->getDocumentoRelacionado->total_documento) }}
                                            </option>
                                        @endif

                                        @forelse( $documentosProveedor as $documentoProveedor )
                                            <option value="{{ $documentoProveedor->id }}" >
                                                {{ $documentoProveedor->getTipoDocumento->sigla }} N° {{ $documentoProveedor->numero_documento }} - {{ fecha_dmY($documentoProveedor->fecha_documento) }} - ${{ formatoMiles($documentoProveedor->total_documento) }}
                                            </option>
                                        @empty
                                        @endforelse
                                        </select>
                                    </div>

                                @endif

                            </div>
                        </div>

                        <div class="form-group row" id="div_rechazados_acepta">
                            @if ( $documento->id_tipo_documento == 1 )
                                <label for="reemplaza_archivo_acepta" class="col-sm-4 col-sm-offset-4 control-label label-form"> Reemplaza al Archivo Acepta Rechazado </label>
                                <div class="col-sm-4">
                                    <select name="reemplaza_archivo_acepta" id="reemplaza_archivo_acepta" class="form-control select2" onchange="setDocumentoReemplazaRechazado(this);">
                                        <option value="">Seleccione</option>
                                        @if ( $documento->getArchivoAceptaRefacturado )
                                            <option id="docRechazadoProv_{{ $documento->getArchivoAceptaRefacturado->id }}" value="{{ $documento->getArchivoAceptaRefacturado->id }}" selected
                                                data-datosdocumento="{!! htmlspecialchars(json_encode($documento->getArchivoAceptaRefacturado->getDatosDocumentosProveedores()), ENT_QUOTES, 'UTF-8') !!}" >
                                                {{ $documento->getArchivoAceptaRefacturado->getTipoDocumento->sigla }} N° {{ $documento->getArchivoAceptaRefacturado->folio }} - {{ fecha_dmY($documento->getArchivoAceptaRefacturado->emision) }} - ${{ formatoMiles($documento->getArchivoAceptaRefacturado->monto_total) }}
                                            </option>
                                        @endif
                                        @forelse ($docsRechazadosAceptaProv as $rechazado)
                                            <option id="docRechazadoProv_{{ $rechazado->id }}" value="{{ $rechazado->id }}" data-datosdocumento="{!! htmlspecialchars(json_encode($rechazado->getDatosDocumentosProveedores()), ENT_QUOTES, 'UTF-8') !!}" >
                                                {{ $rechazado->getTipoDocumento->sigla }} N° {{ $rechazado->folio }} - {{ fecha_dmY($rechazado->emision) }} - ${{ formatoMiles($rechazado->monto_total) }}
                                            </option>
                                        @empty
                                        @endforelse
                                        </select>
                                    </select>
                                </div>
                            @endif
                        </div>

                        <div class="form-group row">
                            <label for="numero_documento" class="col-sm-2 control-label label-form">N° Documento </label>
                            <div class="col-sm-4 form-validate">
                                <input type="text" class="form-control numero_documento" id="numero_documento" name="numero_documento" required value="{{ $documento->numero_documento }}">
                            </div>

                            <label for="modalidad_compra" class="col-sm-2 control-label label-form">Modalidad de Compra <span class="span-label">*</span></label>
                            <div class="col-sm-4 form-validate">
                                <select name="modalidad_compra" id="modalidad_compra" class="form-control select2" required onchange="setOrdenCompra(this);">
                                <option value=''>Seleccione</option>
                                @foreach ( $modalidadesCompra as $modalidadCompra)
                                    <option value="{{ $modalidadCompra->id }}">{{ $modalidadCompra->nombre }}</option>
                                @endforeach
                                </select>
                            </div>
                        </div>

                        <div class="form-group row">
                            <label for="tipo_adjudicacion" class="col-sm-2 control-label label-form">Tipo Adjudicación <span class="span-label">*</span></label>
                            <div class="col-sm-4 form-validate">
                                <select name="tipo_adjudicacion" id="tipo_adjudicacion" class="form-control select2" required>
                                <option value=''>Seleccione</option>
                                @foreach ( $tiposAdjudicacion as $tipoAdjudicacion)
                                    <option value="{{ $tipoAdjudicacion->id }}">{{ $tipoAdjudicacion->nombre }}</option>
                                @endforeach
                                </select>
                            </div>
                        </div>

                        <div class="form-group row">       
                            <label for="numero_orden_compra" class="col-sm-2 control-label label-form">N° Orden de Compra <span class="span-label">*</span></label>
                            <div class="col-sm-4 form-validate" id="div_orden_compra">
                                <input type="text" class="form-control" id="numero_orden_compra" name="numero_orden_compra" required value="{{ $documento->documento_compra }}" @if($documento->getModalidadCompra->predeterminado != null) readonly @endif>
                            </div>
                            
                            <label for="fecha_documento" class="col-sm-2 control-label label-form">Fecha Documento <span class="span-label">*</span></label>
                            <div class="col-sm-2 form-validate">
                                <div class="input-group">
                                    <input type="text" class="form-control fechas" id="fecha_documento" name="fecha_documento" required value="{{ fecha_dmY($documento->fecha_documento) }}" onchange="cambioFechaDocumento(this);" >
                                    <span class="input-group-addon" onclick="fechaDocumento('fecha_documento');" style="cursor:pointer;"><i class="fa fa-calendar" aria-hidden="true" style="color: black;"></i></span>
                                </div>
                            </div>
                        </div>

                        <div class="form-group row">
                            <label for="tipo_informe" class="col-sm-2 control-label label-form">Tipo de Informe <span class="span-label">*</span></label>
                            <div class="col-sm-4 form-validate">
                                <select name="tipo_informe" id="tipo_informe" class="form-control select2" required>
                                <option value=''>Seleccione</option>
                                @foreach ( $tiposInforme as $tipoInforme)
                                    <option value="{{ $tipoInforme->id }}">{{ $tipoInforme->nombre }}</option>
                                @endforeach
                                </select>
                            </div>

                            {{-- <label for="responsable" class="col-sm-2 control-label label-form">Responsable <span class="span-label">*</span></label>
                            <div class="col-sm-4 form-validate">
                                <select name="responsable" id="responsable" class="form-control select2" required>
                                <option value=''>Seleccione</option>
                                @foreach ( $usuarios as $usuario)
                                    <option value="{{ $usuario->id }}">{{ $usuario->name }}</option>
                                @endforeach
                                </select>
                            </div> --}}
                        </div>

                        <div class="form-group row">
                            <label for="fecha_recepcion" class="col-sm-2 control-label label-form">Fecha Recepción </label>
                            <div class="col-sm-2 form-validate">
                                <div class="input-group">
                                    <input type="text" class="form-control fechas" id="fecha_recepcion" name="fecha_recepcion" required value="{{ fecha_dmY($documento->fecha_recepcion) }}" onchange="cambioFechaRecepcion(this);">
                                    <span class="input-group-addon " onclick="fechaDocumento('fecha_recepcion');" style="cursor:pointer;"><i class="fa fa-calendar" aria-hidden="true" style="color: black;"></i></span>
                                </div>
                            </div>

                            <label for="valor_total_documento" class="col-sm-2 col-sm-offset-2 control-label label-form">Valor total Documento </label>
                            <div class="col-sm-4 form-validate">
                                <div class="input-group">
                                    <span class="input-group-addon">$</span>
                                    <input type="text" class="form-control numero_documento" id="valor_total_documento" name="valor_total_documento" required onkeyup="ingresoPesos(this);" autocomplete="off" value="{{ formatoMiles($documento->total_documento) }}">
                                </div>
                            </div>
                        </div>

                        <div id="div_valores_boleta" @if( $documento->id_tipo_documento != 3 ) style="display:none" @endif>
                            <div class="form-group row" >
        
                                <label for="impuesto" id="label_impuesto" class="col-xs-2 col-sm-2 col-md-2 col-lg-2 col-xs-offset-6 col-sm-offset-6 col-md-offset-6 col-lg-offset-6 control-label label-form">{{ $documento->getLabelImpuesto() }} Impuesto</label>
                                <div class="col-xs-4 col-sm-4 col-md-4 col-lg-4 form-validate">
                                    <div class="input-group">
                                        <span class="input-group-addon">$</span>
                                    <input type="text" class="form-control numero_documento" id="impuesto" name="impuesto" readonly autocomplete="off" value="{{ formatoMiles($documento->impuesto) }}" >
                                    </div>
                                </div>
                            </div>
        
                            <div class="form-group row" >
                                <label for="liquido" class="col-xs-2 col-sm-2 col-md-2 col-lg-2 col-xs-offset-6 col-sm-offset-6 col-md-offset-6 col-lg-offset-6 control-label label-form">Líquido </label>
                                <div class="col-xs-4 col-sm-4 col-md-4 col-lg-4 form-validate">
                                    <div class="input-group">
                                        <span class="input-group-addon">$</span>
                                        <input type="text" class="form-control numero_documento" id="liquido" name="liquido" readonly autocomplete="off" value="{{ formatoMiles($documento->liquido) }}" >
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="form-group row">
                            <label for="referente_tecnico" class="col-sm-2 control-label label-form">Referente Técnico </label>
                            <div class="col-sm-4 form-validate">
                                <select name="referente_tecnico" id="referente_tecnico" class="form-control select2" >
                                <option value=''>Seleccione</option>
                                @foreach ( $referentesTecnicos as $referenteTecnico)
                                    <option value="{{ $referenteTecnico->id }}">{{ $referenteTecnico->nombre }} - {{ $referenteTecnico->responsable }}</option>
                                @endforeach
                                </select>
                            </div>

                            <label for="numero_licitacion" class="col-sm-2 control-label label-form">N° Licitación </label>
                            <div class="col-sm-4 form-validate">
                                <input type="text" class="form-control" id="numero_licitacion" name="numero_licitacion" value="{{ $documento->licitacion }}" >
                            </div>
                        </div>

                        <div class="form-group row">
                            <label for="fecha_ingreso" class="col-sm-2 control-label label-form">Fecha de Ingreso </label>
                            <div class="col-sm-4 form-validate">
                                <input type="text" class="form-control" id="fecha_ingreso" name="fecha_ingreso" value="{{ fecha_dmY_hms($documento->fecha_ingreso) }}" readonly>
                            </div>

                            <label for="ingresado" class="col-sm-2 control-label label-form">Ingresado por </label>
                            <div class="col-sm-4 form-validate">
                                <input type="text" class="form-control" id="ingresado" name="ingresado" @if( $documento->getDigitador ) value="{{ $documento->getDigitador->name }}" @endif readonly>
                            </div>
                        </div>

                        <h4 class="form-section" style="color: #69aa46;">Documentos adjuntos</h4>
                        <div class="form-group row">
                            <div class="col-xs-12 table-responsive">
                                <table class="table table-striped table-bordered table-hover " id="tabla_info_archivos_existentes">
                                    <thead>
                                        <tr>
                                            <th width="50%">Nombre del Archivo</th>
                                            <th width="20%">Tipo</th>
                                            <th width="10%">Extensión</th>
                                            <th width="10%">Peso</th>
                                            <th width="10%" class="text-center"><i class="fa fa-cog"></i></th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                    @forelse ($documento->getArchivos as $archivo)
                                        <tr>
                                            <td>{{ $archivo->nombre }}</td>
                                            <td>@if( $archivo->getTipoArchivo ){{ $archivo->getTipoArchivo->nombre }}@endif</td>
                                            <td>{{ $archivo->extension }}</td>
                                            <td>{{ pesoArchivoEnMB($archivo->peso) }}</td>
                                            <td>
                                                <a class="btn btn-success btn-xs" @if($archivo->cargado == 0) href="{{ asset( $archivo->ubicacion ) }}" @else href="http://{{ $archivo->ubicacion }}" @endif
                                                title="Ver Archivo" target="_blank" ><i class="fa fa-eye"></i></a>
                                                <label>
                                                    <input type="checkbox" name="delete_list[]" value="{{$archivo->id}}"> Eliminar
                                                </label>
                                            </td>
                                        </tr>
                                    @empty

                                    @endforelse
                                    </tbody>
                                </table>
                            </div>
                        </div>

                        <div class="form-group row">
                            <div class="input-group col-sm-10 col-sm-offset-1">
                                <span class="input-group-addon" id="inputGroupFileAddon01"><strong>Seleccione Documento</strong></span>
                                <input type="file" class="form-control" accept=".pdf" name="archivo" id="archivo" aria-describedby="inputGroupFileAddon01" onchange="validarArchivo();">
                            </div>
                        </div>

                        <div class="form-group row">
                            <div id="div_archivo" class="col-xs-12 table-responsive" style="display: none;">
                                <table class="table table-striped table-bordered table-hover " id="tabla_info_archivo">
                                    <thead>
                                        <tr>
                                            <th width="40%">Nombre</th>
                                            <th width="20%">Peso</th>
                                            <th width="40%">Tipo</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                    </tbody>
                                </table>
                            </div>
                        </div>

                        @if ( $documento->getDocumentosRelacionados->count() > 0 || $documento->id_relacionado != null || $documento->getArchivoAceptaRefacturado )
                            <div class="form-group row">
                                <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">	
                                    <h4 class="form-section" style="color: #69aa46;">
                                        Documentos Relacionados
                                    </h4>
                                    
                                    <div class="form-group">
                                        <div class="col-xs-6 col-sm-6 col-md-6 col-lg-6 table-responsive">
                                            <table class="table table-striped table-bordered table-hover " id="tabla_info_archivo">
                                                <thead>
                                                    <tr>
                                                        <th class="text-center">N° Doc.</th>
                                                        <th >Tipo Doc.</th>
                                                        <th class="text-right">Valor Total Doc.</th>
                                                    </tr>
                                                </thead>
                                                <tbody>
                                                    @forelse($documento->getDocumentosRelacionados as $doc)
                                                        <tr>
                                                            <td class="text-center">{{ $doc->numero_documento }}</td>
                                                            <td>
                                                                {{ $doc->getTipoDocumento->nombre }}
                                                                @if ( $doc->getArchivos->count() > 0 )
                                                                    <div class="btn-group pull-right">
                                                                        @foreach($doc->getArchivos as $archivo)
                                                                            <a class="btn btn-info btn-xs " 
                                                                                @if($archivo->cargado == 0) 
                                                                                    href="{{ asset( $archivo->ubicacion ) }}" 
                                                                                @else
                                                                                    href="http://{{ $archivo->ubicacion }}"
                                                                                @endif
                                                                                title="Ver Archivo @if( $archivo->getTipoArchivo ){{ $archivo->getTipoArchivo->nombre }}@endif" 
                                                                                target="_blank" ><i class="fa fa-eye"></i>
                                                                            </a>
                                                                        @endforeach
                                                                    </div>
                                                                @endif
                                                            </td>
                                                            <td class="text-right">$ {{ formatoMiles($doc->total_documento) }}</td>
                                                        </tr>
                                                    @empty
                                            
                                                    @endforelse

                                                    @if ( $documento->id_relacionado != null )
                                                        <tr>
                                                            <td class="text-center">{{ $documento->getDocumentoRelacionado->numero_documento }}</td>
                                                            <td>
                                                                {{ $documento->getDocumentoRelacionado->getTipoDocumento->nombre }}
                                                                @if ( $documento->getDocumentoRelacionado->getArchivos->count() > 0 )
                                                                    <div class="btn-group pull-right">
                                                                        @foreach($documento->getDocumentoRelacionado->getArchivos as $archivo)
                                                                            <a class="btn btn-info btn-xs " 
                                                                                @if($archivo->cargado == 0) 
                                                                                    href="{{ asset( $archivo->ubicacion ) }}" 
                                                                                @else
                                                                                    href="http://{{ $archivo->ubicacion }}"
                                                                                @endif
                                                                                title="Ver Archivo @if( $archivo->getTipoArchivo ){{ $archivo->getTipoArchivo->nombre }}@endif" 
                                                                                target="_blank" ><i class="fa fa-eye"></i>
                                                                            </a>
                                                                        @endforeach
                                                                    </div>
                                                                @endif
                                                            </td>
                                                            <td class="text-right">$ {{ formatoMiles($documento->getDocumentoRelacionado->total_documento) }}</td>
                                                        </tr>
                                                    @endif
    
                                                    @if ( $documento->getArchivoAceptaRefacturado )
                                                        <tr>
                                                            <td class="text-center">{{ $documento->getArchivoAceptaRefacturado->folio }}</td>
                                                            <td>
                                                                {{ $documento->getArchivoAceptaRefacturado->getTipoDocumento->nombre }} (Rechazado Acepta)
                                                                @if ( $documento->getArchivoAceptaRefacturado->uri != null )
                                                                    <div class="btn-group pull-right">
                                                                        <a class="btn btn-info btn-xs" href="http://windte1806.acepta.com/ca4webv3/PdfViewMedia?url={{ $documento->getArchivoAceptaRefacturado->uri }}&menuTitle=Papel%2520Carta"
                                                                                title="Ver Factura" target="_blank" ><i class="fa fa-eye"></i></a>
                                                                    </div>
                                                                @endif
                                                            </td>
                                                            <td class="text-right">$ {{ formatoMiles($documento->getArchivoAceptaRefacturado->monto_total) }}</td>
                                                        </tr>
                                                    @endif

                                                    @if ( $documento->montoAplicadoAlValorTotal() != 0 )
                                                        <tr>
                                                            <td colspan="2" class="text-right"><strong>Sumatoria de NC y ND aplicada al Valor Tot. Orig.</strong></td>
                                                            <td class="text-right">$ {{ formatoMiles($documento->montoAplicadoAlValorTotal()) }}</td>
                                                        </tr>
                                                    @endif

                                                </tbody>
                                            </table>
                                        </div>

                                        <label for="valor_total_documento_actualizado" class="col-xs-2 col-sm-2 col-md-2 col-lg-2 control-label label-form">Valor Tot. Act. <span class="span-label">*</span></label>
                                        <div class="col-xs-4 col-sm-4 col-md-4 col-lg-4 form-validate">
                                            <div class="input-group">
                                                <span class="input-group-addon">$</span>
                                                <input type="text" class="form-control numero_documento" id="valor_total_documento_actualizado" 
                                                    name="valor_total_documento_actualizado" required readonly
                                                    value="{{ formatoMiles($documento->total_documento_actualizado) }}" >
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        @endif

                        @if ( $documento->getRelacionRecepcionesValidadas->count() > 0 )
                            <h4 class="form-section" style="color: #69aa46;">Documentos Recepción, archivos adjuntos</h4>
                            <div class="form-group row">
                                <div class="col-xs-12 table-responsive">
                                    <table class="table table-striped table-bordered table-hover " id="tabla_info_archivos_relacion">
                                        <thead>
                                            <tr>
                                                <th width="15%">N° Doc Recepción</th>
                                                <th width="80%">Nombre del Archivo</th>
                                                <th width="5%" class="text-center"><i class="fa fa-cog"></i></th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            @forelse($documento->getRelacionRecepcionesValidadas as $relacion)
                                                @forelse ($relacion->getWsDocumento->getWsArchivos as $archivo)
                                                    <tr>
                                                        <td>{{ $relacion->getWsDocumento->documento }}</td>
                                                        <td>{{ $archivo->nombre_archivo }}</td>
                                                        <td class="text-center">
                                                            <a class="btn btn-success btn-xs" @if($archivo->cargado == 0) href="{{ asset( $archivo->ubicacion ) }}" @else href="http://{{ $archivo->ubicacion }}" @endif
                                                            title="Ver Archivo" target="_blank" ><i class="fa fa-eye"></i></a>
                                                        </td>
                                                    </tr>
                                                @empty
                                                    
                                                @endforelse
                                            @empty

                                            @endforelse
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        @endif

                        <h4 class="form-section" style="color: #69aa46;">Devengado</h4>
                        <div class="form-group row">
                            <label for="id_sigfe" class="col-sm-2 control-label label-form">ID SIGFE <span class="span-label">*</span></label>
                            <div class="col-sm-4 form-validate">
                                <input type="text" class="form-control numero_id_sigfe" id="id_sigfe" name="id_sigfe" required @if( isset($folioSigfe) ) value="{{ $folioSigfe->id_sigfe }}" @endif>
                            </div>

                            <label for="fecha_id" class="col-sm-2 control-label label-form">Fecha ID <span class="span-label">*</span></label>
                            <div class="col-sm-2 form-validate">
                                <div class="input-group">
                                    <input type="text" class="form-control fechas" id="fecha_id" name="fecha_id" required  @if( isset($folioSigfe) ) value="{{ fecha_dmY($folioSigfe->fecha_sigfe) }}" @endif>
                                    <span class="input-group-addon" onclick="fechaDocumento('fecha_id');" style="cursor:pointer;"><i class="fa fa-calendar" aria-hidden="true" style="color: black;"></i></span>
                                </div>
                            </div>

                        </div>

                        <div class="form-group row">
                            <label for="detalle_documento" class="col-sm-2 control-label label-form">Detalles <span class="span-label">*</span></label>
                            <div class="col-sm-4 form-validate">
                                <textarea class="form-control noresize" id="detalle_documento" name="detalle_documento" required>@if( isset($folioSigfe) ){{$folioSigfe->detalle_sigfe}}@else{{ $detalleRecepcion }}@endif</textarea>
                            </div>
                            
                            <label for="observacion_documento" class="col-sm-2 control-label label-form">Observaciones </label>
                            <div class="col-sm-4 form-validate">
                                <textarea class="form-control noresize" id="observacion_documento" name="observacion_documento" >@if( $documento->observacion != null && $documento->observacion != '' ){{ $documento->observacion }}@endif{{ $descripcion }}</textarea>
                            </div>
                        </div>

                        <h4 class="form-section" style="color: #69aa46;">Ítem Presupuestario</h4>
                        <div class="row btn-group col-xs-12">
                            <button type="button" id="botonAgregarItem" title="Agregar Ítem Presupuestario" 
                            class="btn btn-xs btn-primary col-xs-2" onclick="agregarItem();">
                            Agregar Ítem Presupuestario </button>
                        </div>
                        <div class="form-group row">
                            <div id="div_items" class="col-xs-12 table-responsive" >
                                <table class="table " id="tabla_item_presupuestario">
                                    
                                    <thead style="display: none;">
                                        <tr>
                                            <th class="col-sm-2">&nbsp;</th>
                                            <th class="col-sm-6">&nbsp;</th>
                                            <th class="col-sm-3">&nbsp;</th>
                                            <th class="col-sm-1">&nbsp;</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        @if ( isset($folioSigfe) && $folioSigfe->getDevengos->count() > 0 )
                                        {{-- Si existe el folio sigfe y no tiene devengos (items devengados) --}}
                                            @forelse ( $folioSigfe->getDevengos as $devengo )
                                                <tr id="{{ $devengo->id_item_presupuestario }}">
                                                    <td class="td_items col-sm-2">{{ $devengo->getItemPresupuestario->codigo() }}</td>
                                                    <td class="td_items col-sm-6">{{ $devengo->getItemPresupuestario->clasificador_presupuestario }} <span class="span-label">*</span></td>
                                                    <td class="col-sm-3">
                                                        <div class="input-group form-validate">
                                                            <span class="input-group-addon">$</span>
                                                            <input type="text" class="form-control numero_documento" id="valor_item_{{ $devengo->id_item_presupuestario }}" 
                                                            name="valor_item[{{ $devengo->id_item_presupuestario }}]" required onkeyup="ingresoPesos(this);" 
                                                            value="{{ formatoMiles($devengo->monto) }}">
                                                        </div>
                                                    </td>
                                                    <td class="td_items_dos col-sm-1">
                                                        <div class="btn-group">
                                                            <button type="button" class="btn btn-danger btn-xs" title="Eliminar Item" 
                                                            onclick="eliminarItem({{ $devengo->id_item_presupuestario }});"><i class="fa fa-trash"></i> Eliminar
                                                            </button>
                                                        </div>
                                                    </td>
                                                </tr>
                                            @empty

                                            @endforelse
                                        @else 
                                            @forelse ( $documento->getDevengos as $devengo )
                                                <tr id="{{ $devengo->id_item_presupuestario }}">
                                                    <td class="td_items col-sm-2">{{ $devengo->getItemPresupuestario->codigo() }}</td>
                                                    <td class="td_items col-sm-6">{{ $devengo->getItemPresupuestario->clasificador_presupuestario }} <span class="span-label">*</span></td>
                                                    <td class="col-sm-3">
                                                        <div class="input-group form-validate">
                                                            <span class="input-group-addon">$</span>
                                                            <input type="text" class="form-control numero_documento" id="valor_item_{{ $devengo->id_item_presupuestario }}" 
                                                            name="valor_item[{{ $devengo->id_item_presupuestario }}]" required onkeyup="ingresoPesos(this);" 
                                                            value="{{ formatoMiles($devengo->monto) }}">
                                                        </div>
                                                    </td>
                                                    <td class="td_items_dos col-sm-1">
                                                        <div class="btn-group">
                                                            <button type="button" class="btn btn-danger btn-xs" title="Eliminar Item" 
                                                            onclick="eliminarItem({{ $devengo->id_item_presupuestario }});"><i class="fa fa-trash"></i> Eliminar
                                                            </button>
                                                        </div>
                                                    </td>
                                                </tr>
                                            @empty

                                            @endforelse
                                        @endif
                                    </tbody>
                                </table>
                            </div>
                        </div>

                        <input type="hidden" name="_token" value="{{ csrf_token() }}">
                        <input type="hidden" name="id_documento" value="{{ $documento->id }}">

                    </div>
                </div> {{-- /modal-body --}}

                <div class="modal-footer form-actions right">
                    <button type="button" title="Cancelar" class="btn btn-default" data-dismiss="modal">Cancelar</button>
                    <button type="submit" title="Editar Documento" class="btn btn-success" id="botonGuardar"><i class="far fa-save" style="color: black;"></i> Editar Documento y Registrar Devengo <i class="fas fa-gavel"></i></button>
                </div>
            </form>
        </div>
    </div>
</div>
<script src="{{asset('sistema/js/documentos_finanzas.js')}}?v={{rand()}}"></script>
<script>
    var urlGetFacturasRechazadasAcepta = '{{ url('documentos/get_facturas_rechazadas_acepta/') }}';
    // nuevas variables globales
    // var urlGetUltimaOcProveedor = '{{ url('documentos/get_oc_proveedor/') }}';
    var urlGetDocumentosBodega = '{{ url('documentos/get_documentos_bodega_proveedor/') }}';

    var urlGetProveedor = '{{ url('documentos/get_proveedor/') }}';
    var selectTipoArchivo = '<select name="tipo_archivo" id="tipo_archivo" class="col-xs-12" required>';
    selectTipoArchivo += '<option value="">Seleccione</option>';
    @foreach ( $tiposArchivo as $tipoArchivo)
        selectTipoArchivo += '<option value="{{ $tipoArchivo->id }}">{{ $tipoArchivo->nombre }}</option>';
    @endforeach
    selectTipoArchivo += '</select>';
    var urlAgregarItem = '{{ url('documentos/modal/agregar_item') }}';
    var urlSetReemplaza = '{{ url('documentos/get_documentos_proveedor/') }}';
    var urlSetOrdenCompra = '{{ url('documentos/get_predeterminado_modalidad_compra/') }}';

    var arregloItems = [
        @if ( isset($folioSigfe) && $folioSigfe->getDevengos->count() > 0 )
            @forelse($folioSigfe->getDevengos as $devengo)
                {!! $devengo->getItemPresupuestario->id !!},
            @empty

            @endforelse
        @else
            @forelse($documento->getDevengos as $devengo)
                {!! $devengo->getItemPresupuestario->id !!},
            @empty

            @endforelse
        @endif
    ];

    var tablaArchivo = $('#tabla_info_archivo').DataTable({
        "paging":   false,
        "ordering": false,
        "info":     false,
        "searching": false
    });

    var tablaItemsPresupuestarios = $('#tabla_item_presupuestario').DataTable({
        "paging":   false,
        "ordering": false,
        "info":     false,
        "searching": false
    });

    var tablaArchivosExistentes = $('#tabla_info_archivos_existentes').DataTable({
        "language": {
            "emptyTable": "No hay datos disponibles en la tabla",
        },
        "paging":   false,
        "ordering": false,
        "info":     false,
        "searching": false
    });

    var tablaArchivosRelacion = $('#tabla_info_archivos_relacion').DataTable({
        "language": {
            "emptyTable": "No hay datos disponibles en la tabla",
        },
        "paging":   false,
        "ordering": false,
        "info":     false,
        "searching": false
    });

    

    $(document).ready(function() {

        $('#fecha_id').datepicker({
            format: 'dd/mm/yyyy',
            // startDate: '01/12/{!! $anho_limite !!}',
            //endDate: new Date(),
            //"setDate": new Date(),
            //startDate: new Date(),
            autoclose: true,
            language: 'es'
        });

        $('#fecha_recepcion').datepicker({
            format: 'dd/mm/yyyy',
            //endDate: new Date(),
            //"setDate": new Date(),
            //startDate: new Date(),
            autoclose: true,
            language: 'es'
        });

        $('#fecha_documento').datepicker({
            format: 'dd/mm/yyyy',
            endDate: new Date(),
            autoclose: true,
            language: 'es'
        });

        $(".numero_id_sigfe").keypress(function (key) {
            // window.console.log(key.charCode); //Descomentar para ver codigo
            if (
                (key.charCode < 48 || key.charCode > 57)//números
                )
                //console.log(key.charCode);
                return false;
        });


        $(".numero_documento").keypress(function (key) {
            // window.console.log(key.charCode); //Descomentar para ver codigo
            if (
                (key.charCode < 48 || key.charCode > 57)//números
                && (key.charCode != 0) //borrar y enter
                && (key.charCode != 44) //coma
                && (key.charCode != 46) //punto
                && (key.charCode != 45) //guion
                && (key.charCode != 43) //suma
                )
                //console.log(key.charCode);
                return false;
        });

        $("#nombre_proveedor").select2({
            ajax: {
                cache: true,
                allowClear: true,
                //hidden : true,
                url : '{{ url('buscador_proveedor/') }}',
                dataType: 'json',
                delay: 250,
                data: function (params,page) {
                    //console.log("soy params : "+params);
                    //console.log("soy page : "+page);
                    var query = {
                        term: params,
                        page: params.page || 1
                    }
                    return query;
                },
                results: function (data) {
                    return {
                    results: data
                    };
                },
            },
            minimumInputLength: 2,
        
        });

        $(".select2").select2();

        @if ( $documento->id_relacionado != null )
            if ( $('#reemplaza_archivo_acepta').length > 0 ) {
                $('#reemplaza_archivo_acepta').select2("readonly", true);
            }
        @endif

        @if ( $documento->id_archivo_acepta_refacturado != null )
            if ( $('#reemplaza').length > 0 ) {
                $('#reemplaza').select2("readonly", true);
            }
        @endif
        // Se pasan valores a los select AJAX
        $('#nombre_proveedor').select2('data', { id: '{!! $documento->id_proveedor !!}', text: '{!!$documento->getProveedor->formatRut() !!} {!!$documento->getProveedor->nombre !!}' });
        // Se pasan valores a select normales
        $('#tipo_documento').select2('val',{{ $documento->id_tipo_documento }});
        $('#modalidad_compra').select2('val',{{ $documento->id_modalidad_compra }});
        $('#tipo_adjudicacion').select2('val',{{ $documento->id_tipo_adjudicacion }});
        $('#tipo_informe').select2('val',{{ $documento->id_tipo_informe }});
        // $('#responsable').select2('val',{{ $documento->id_responsable }});
        $('#referente_tecnico').select2('val',{{ $documento->id_referente_tecnico }});
        


        $("#form-documentos").validate({
            highlight: function(element) {
                $(element).closest('.form-validate').removeClass('has-success');
                $(element).closest('.form-validate').addClass('has-error');
            },
            unhighlight: function(element) {
                $(element).closest('.form-validate').removeClass('has-error');
                $(element).closest('.form-validate').addClass('has-success');
            },
            errorElement: 'span',
            errorClass: 'help-block',
            errorPlacement: function (error, element) {
                console.log(element);
                if (element.parent('.input-group').length) {
                    error.insertAfter(element.parent());
                } else {
                    error.insertAfter(element);
                }
            },
            rules: {
            },
            messages: {
            },

            //para enviar el formulario por ajax
            submitHandler: function(form) {

                esperandoIngresoDocumento();
                if (validaTotalDocumentoConItemsPresupuestario() && validaNumDocumentoAndLicitacion()) {

                    let formData = new FormData(form);
                    $.ajax({
                        url: form.action,
                        type: form.method,
                        //data: $(form).serialize(),
                        data: formData,
                        processData: false,
                        contentType: false,
                        success: function(respuesta) {
                            console.log(respuesta);
                            if ( respuesta.estado_documento == 'error' ) {
                                toastr.error('No es posible realizar la acción'+'<br><strong>Error: '+ respuesta.mensaje_documento +'</strong>', 'Atención', optionsToastr);
                            } else if ( respuesta.estado_documento == 'success') {
                                
                                toastr.success(respuesta.mensaje_documento, 'Atención', optionsToastr);
                                // Funcion que utiliza la respuesta para actualizar la tabla principal
                                actualizaTabla(respuesta);
                                $("#modalDevengar").modal("hide");

                                if ( respuesta.estado_archivo == 'error' ) {
                                    toastr.error('Archivo: No es posible realizar la acción'+'<br><strong>Error: '+ respuesta.mensaje_archivo +'</strong>', 'Atención', optionsToastr);
                                } else if ( respuesta.estado_archivo == 'succes') {
                                    toastr.success(respuesta.mensaje_archivo, 'Atención', optionsToastr);
                                }
                                
                            }
                            
                        }            
                    }).fail( function(respuesta) {//fail ajax
                        if ( respuesta.status == 400 ) {
                            mostrarErroresValidator(respuesta);
                        } else if ( respuesta.status == 500 ) {
                            toastr.error('No es posible realizar la acción, error en el servidor', 'Atención', optionsToastr);
                        } else {
                            toastr.error('No es posible realizar la acción', 'Atención', optionsToastr);
                        }
                        
                    })
                    .always(function() {
                        listoIngresoDocumento();
                    });//ajax

                } else {
                    listoIngresoDocumento();
                }
                
            }

        });

    });
            
</script>