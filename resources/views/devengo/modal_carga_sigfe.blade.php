<link href="{{ asset('sistema/css/modales.css') }}?v={{rand()}}" rel="stylesheet" type="text/css"/>
<style>
.custom-checkbox {}

/* oculto el input */
.custom-checkbox input[type=checkbox] {
  display: none;
}

/* oculto el texto */
.custom-checkbox span {
  display: none;
}

/* si está activo el input */
.custom-checkbox input[type=checkbox]:checked + span {
  display: inline-block;
}

/* si está inactivo el input */
.custom-checkbox input[type=checkbox]:not(:checked) + span + span {
  display: inline-block;
}
</style>
<div class="modal fade" id="modalCargaSigfe" role="dialog" aria-labelledby="myModalLabel3" aria-hidden="true">
	<div class="modal-dialog" style="width:90%;">
		<div class="modal-content">
            <div class="modal-header">
				<button type="button" title="Cerrar" class="close" data-dismiss="modal" aria-hidden="true" ></button>
				<h4 class="modal-title">
                    <strong><i class="fas fa-file-export fa-lg"></i>&nbsp;<i class="fas fa-file-invoice fa-lg"></i>
                    &nbsp;&nbsp;Cargar Documento de Acepta a Documentos en Sistema</strong>
                </h4>
            </div>

            <form action="{{ asset('devengar/carga_sigfe_a_sistema') }}" method="post" class="horizontal-form" id="form-documentos" autocomplete="off">
                <div class="modal-body">
                
                    <div class="form-body">
                        <div class="note note-danger" id="divErrores" style="display:none;">
                            <h4 class="block" style="margin-bottom:5 px;">Debe Completar los siguientes campos: </h4>
                            <ul id="ulErrores"></ul>
                        </div>

                        @include('documento.parcial.crea_documento')


                        <h4 class="form-section" style="color: #69aa46;">Devengado</h4>
                        <div class="form-group row">
                            <label for="id_sigfe" class="col-sm-2 control-label label-form">ID SIGFE <span class="span-label">*</span></label>
                            <div class="col-sm-2 form-validate">
                                <input type="text" class="form-control numero_id_sigfe" id="id_sigfe" name="id_sigfe" required value="{{ $idSigfe }}" readonly>
                            </div>

                            <label for="fecha_id" class="col-sm-2 col-sm-offset-2 control-label label-form">Fecha ID <span class="span-label">*</span></label>
                            <div class="col-sm-2 form-validate">
                                <div class="input-group">
                                    <input type="text" class="form-control fechas" id="fecha_id" name="fecha_id" required value="{{ fecha_dmY($fechaSigfe) }}" readonly >
                                    <span class="input-group-addon" onclick="fechaDocumento('fecha_id');" style="cursor:pointer;"><i class="fa fa-calendar" aria-hidden="true" style="color: black;"></i></span>
                                </div>
                            </div>

                        </div>

                        <div class="form-group row">
                            <label for="detalle_documento" class="col-sm-2 control-label label-form">Detalles <span class="span-label">*</span></label>
                            <div class="col-sm-8 form-validate">
                                <textarea class="form-control noresize" id="detalle_documento" name="detalle_documento" required></textarea>
                            </div>                            
                        </div>
                        
                        <input type="hidden" name="_keyTr" value="{{ $keyTr }}">

                    </div>
                </div> {{-- /modal-body --}}

                <div class="modal-footer form-actions right">
                    <button type="button" title="Cancelar" class="btn btn-default" data-dismiss="modal">Cancelar</button>
                    <button type="submit" title="Registrar Documento" class="btn btn-success" id="botonGuardar"><i class="fas fa-file-export fa-lg"></i>&nbsp;<i class="fas fa-file-invoice fa-lg"></i> Registrar Documento</button>
                </div>
            </form>
        </div>
    </div>
</div>
<script src="{{asset('sistema/js/documentos_finanzas.js')}}?v={{rand()}}"></script>
<script>

    var urlGetContratosProveedor = '{{ url('contratos/get_contratos_para_documentos/') }}';

    var urlGetOrdenesCompraProveedor = '{{ url('ordenes_compra/get_oc_para_boleta/') }}';

    var urlGetFacturasRechazadasAcepta = '{{ url('documentos/get_facturas_rechazadas_acepta/') }}';

    var urlGetItemsAutomaticos = '{{ url('documentos/get_items_presupuestarios_recepcion/') }}';

    // nuevas variables globales
    var urlGetGuiasDespachoBodega = '{{ url('documentos/get_guias_despacho_bodega_proveedor/') }}';

    // var urlGetUltimaOcProveedor = '{{ url('documentos/get_oc_proveedor/') }}';
    var urlGetDocumentosBodega = '{{ url('documentos/get_documentos_bodega_proveedor/') }}';
    
    var urlGetProveedor = '{{ url('documentos/get_proveedor/') }}';
    var selectTipoArchivo = '<select name="tipo_archivo" id="tipo_archivo" class="col-xs-12" required>';
    selectTipoArchivo += '<option value="">Seleccione</option>';
    @foreach ( $tiposArchivo as $tipoArchivo)
        selectTipoArchivo += '<option value="{{ $tipoArchivo->id }}">{{ $tipoArchivo->nombre }}</option>';
    @endforeach
    selectTipoArchivo += '</select>';
    var urlAgregarItem = '{{ url('documentos/modal/agregar_item') }}';
    var urlSetReemplaza = '{{ url('documentos/get_documentos_proveedor/') }}';
    var urlSetOrdenCompra = '{{ url('documentos/get_predeterminado_modalidad_compra/') }}';



    var arregloItems = [];
    var tablaArchivo = $('#tabla_info_archivo').DataTable({
        "paging":   false,
        "ordering": false,
        "info":     false,
        "searching": false
    });

    var tablaItemsPresupuestarios = $('#tabla_item_presupuestario').DataTable({
        "paging":   false,
        "ordering": false,
        "info":     false,
        "searching": false
    });

    var tablaArchivosExistentes = $('#tabla_info_archivos_existentes').DataTable({
        "language": {
            "emptyTable": "No hay datos disponibles en la tabla",
        },
        "paging":   false,
        "ordering": false,
        "info":     false,
        "searching": false
    });

    $(document).ready(function() {

        $('#fecha_recepcion').datepicker({
            format: 'dd/mm/yyyy',
            //endDate: new Date(),
            //"setDate": new Date(),
            //startDate: new Date(),
            autoclose: true,
            language: 'es'
        });

        $('#fecha_documento').datepicker({
            format: 'dd/mm/yyyy',
            // endDate: new Date(),
            // endDate: '{{ date( "d/m/Y", strtotime( date('d-m-Y')."+ 30 days")) }}',
            autoclose: true,
            language: 'es'
        });

        $('#periodo').datepicker({
            format: 'mm/yyyy',
            // endDate: new Date(),
            autoclose: true,
            language: 'es',
            viewMode: 'months',
            minViewMode: 'months'
        });

        $(".numero_documento").keypress(function (key) {
            // window.console.log(key.charCode); //Descomentar para ver codigo
            if (
                (key.charCode < 48 || key.charCode > 57)//números
                && (key.charCode != 0) //borrar y enter
                && (key.charCode != 44) //coma
                && (key.charCode != 46) //punto
                && (key.charCode != 45) //guion
                && (key.charCode != 43) //suma
                )
                //console.log(key.charCode);
                return false;
        });

        $("#nombre_proveedor").select2({
            ajax: {
                cache: true,
                allowClear: true,
                //hidden : true,
                url : '{{ url('buscador_proveedor/') }}',
                dataType: 'json',
                delay: 250,
                data: function (params,page) {
                    //console.log("soy params : "+params);
                    //console.log("soy page : "+page);
                    var query = {
                        term: params,
                        page: params.page || 1
                    }
                    return query;
                },
                results: function (data) {
                    return {
                    results: data
                    };
                },
            },
            minimumInputLength: 2,
        
        });

        $(".select2").select2();
        
        $('#guias_disponible_bodega').on('select2-selecting', function (e) {

            // No se permite añadir el elemento si excede el monto del documento
            if ( ! setTotalMontoGuias(e.val, 'Agregar') ) {
                e.preventDefault();
            }

        });

        $('#guias_disponible_bodega').on('select2-removing', function (e) {

            // No se permite añadir el elemento si excede el monto del documento
            if ( ! setTotalMontoGuias(e.val, 'Quitar') ) {
                e.preventDefault();
            }

        });

        // Se pasan valores a los select AJAX
        $('#nombre_proveedor').select2('data', { id: '{!! $proveedor->id !!}', text: '{!!$proveedor->formatRut() !!} {!!$proveedor->nombre !!}' });
        $('#nombre_proveedor').select2("readonly", true);

        // Se pasan valores a select normales
        $('#tipo_documento').select2('val',{{ $tipoDoc->id }});
        $('#tipo_documento').select2("readonly", true);

        // Se pasan valores a los inputs
        $('#numero_documento').val('{{ $numeroDoc }}');
        $('#numero_documento').attr('readonly', 'readonly');

        $('#valor_total_documento').val('{{ formatoMiles($totalDoc) }}');
        $('#valor_total_documento').attr('readonly', 'readonly');

        @if ( $tipoDoc->id == 3 )
            $('#impuesto').val('{{ formatoMiles( formato_entero( round( $totalDoc * 0.1) ) ) }}');
            $('#liquido').val('{{ formatoMiles( formato_entero( round( $totalDoc * 0.9) ) ) }}');
        @endif
        
        @if( $optionsContrato != '' )
            $('#contrato').select2();
        @endif

        @if( $optionsOc != '' )
            $('#orden_compra').select2();
        @endif

        $("#form-documentos").validate({
            highlight: function(element) {
                $(element).closest('.form-validate').removeClass('has-success');
                $(element).closest('.form-validate').addClass('has-error');
            },
            unhighlight: function(element) {
                $(element).closest('.form-validate').removeClass('has-error');
                $(element).closest('.form-validate').addClass('has-success');
            },
            errorElement: 'span',
            errorClass: 'help-block',
            errorPlacement: function (error, element) {
                console.log(element);
                if (element.parent('.input-group').length) {
                    error.insertAfter(element.parent());
                } else {
                    error.insertAfter(element);
                }
            },
            rules: {
            },
            messages: {
            },

            //para enviar el formulario por ajax
            submitHandler: function(form) {
                esperandoIngresoDocumento();
                if ( validaTotalDocumentoConItemsPresupuestario() && validaNumDocumentoAndLicitacion() && validaTotalGuiasConTotalDocumento() ) {

                    let formData = new FormData(form);
                    $.ajax({
                        url: form.action,
                        type: form.method,
                        //data: $(form).serialize(),
                        data: formData,
                        processData: false,
                        contentType: false,
                        success: function(respuesta) {
                            console.log(respuesta);

                            if ( respuesta.estado_documento == 'error' ) {
                                toastr.error('No es posible realizar la acción'+'<br><strong>Error: '+ respuesta.mensaje_documento +'</strong>', 'Atención', optionsToastr);
                            } else if ( respuesta.estado_documento == 'success') {
                                
                                toastr.success(respuesta.mensaje_documento, 'Atención', optionsToastr);
                                // Actualizar la tabla. Se quita el documento de la lista
                                $('#tr_' + respuesta.keyTr).fadeOut(400, function() {
                                    tablaNoEncontrados.row('#tr_' + keyTr).remove().draw();
                                });
                                $("#modalCargaSigfe").modal("hide");
                                if ( respuesta.estado_archivo == 'error' ) {
                                    toastr.error('Archivo: No es posible realizar la acción'+'<br><strong>Error: '+ respuesta.mensaje_archivo +'</strong>', 'Atención', optionsToastr);
                                } else if ( respuesta.estado_archivo == 'succes') {
                                    toastr.success(respuesta.mensaje_archivo, 'Atención', optionsToastr);
                                }
                                
                            }
                            
                        }            
                    }).fail( function(respuesta) {//fail ajax
                        if ( respuesta.status == 400 ) {
                            mostrarErroresValidator(respuesta);
                        } else if ( respuesta.status == 500 ) {
                            toastr.error('No es posible realizar la acción, error en el servidor', 'Atención', optionsToastr);
                        } else {
                            toastr.error('No es posible realizar la acción', 'Atención', optionsToastr);
                        }
                        
                    })
                    .always(function() {
                        listoIngresoDocumento();
                    });//ajax

                } else {
                    listoIngresoDocumento();
                }
                
            }

        });

    });


            
</script>