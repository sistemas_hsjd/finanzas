@extends('master')

@section('title', 'Gráficos - Ítems Devengados')

@push('css-head')
<!-- BEGIN PAGE LEVEL STYLES -->
<link rel="stylesheet" type="text/css" href="{{asset('assets/plugins/select2/select2_conquer.css')}}"/>
<link rel="stylesheet" href="{{asset('assets/plugins/data-tables/DT_bootstrap.css')}}"/>
<!-- END PAGE LEVEL STYLES -->
<style>
	.border-primary {
		border: 1px solid #ffffff00;
    	border-radius: 13px;
		cursor:pointer !important;
	}

    .label-form {
        font-weight: bold;
        color: black;
        text-align: right;
    }
</style>
@endpush

@section('content')

<!-- BEGIN CONTENT -->

<div class="page-content-wrapper">
	<div class="page-content-wrapper">
		<div class="page-content">
						
			<!-- BEGIN PAGE CONTENT-->
			<div class="row">
                <div class="col-md-12">
                    <!-- BEGIN EXAMPLE TABLE PORTLET-->
                    <div class="portlet">
                        <div class="portlet-title">
                            <div class="caption">
                                <i class="fa fa-list-ol"></i> Gráficos - Ítems Devengados
                            </div>
                        </div>
                        <div class="portlet-body">
                            <form action="{{url('graficos/items_devengados')}}" method="post" enctype="multipart/form-data" id="filtros-docs" class="form-horizontal">
                                <h4 class="form-section" style="color: #69aa46;"><strong>Filtros</strong>
                                <div class="btn-group pull-right">
                                    
                                    <button class="btn btn-info btn-xs" title="Filtrar" id="filtrarButton">
                                        <i class="fas fa-search" style="color:black"></i> Filtrar
                                    </button>
                                    
                                </div>
                                </h4>
                                <div class="form-group row">

                                    <label for="filtro_item_presupuestario" class="col-sm-1 control-label label-form">Ítem </label>
                                    <div class="col-sm-7">
                                        <select class="form-control select2_filtro " id="filtro_item_presupuestario" name="filtro_item_presupuestario[]" multiple="multiple" >
                                            @foreach ( $itemsPresupuestarios as $item )
                                                <option value="{{ $item->value }}" {{ $item->selected }} >{{ $item->option }}</option>
                                            @endforeach
                                        </select>
                                    </div>

                                    {{-- <label for="filtro_fecha_a_utilizar" class="col-sm-1 control-label label-form">Fecha </label>
                                    <div class="col-sm-3">
                                        <select class="form-control select2_filtro " id="filtro_fecha_a_utilizar" name="filtro_fecha_a_utilizar" >
                                            @foreach ( $fechasUtilizar as $fechaUtilizar )
                                                <option value="{{ $fechaUtilizar->opcion }}" >{{ $fechaUtilizar->opcion }}</option>
                                            @endforeach
                                        </select>
                                    </div> --}}

                                    {{-- <label for="filtro_fecha_inicio" class="col-sm-1 control-label label-form">Fecha Inicio</label>
                                    <div class="col-sm-2">
                                        <div class="input-group">
                                            <input type="text" class="form-control fechas" id="filtro_fecha_inicio" name="filtro_fecha_inicio" 
                                                    @if( ! isset($fechaInicio) )
                                                        value="{{ date("m/Y") }}"
                                                    @else
                                                        value="{{ $fechaInicio }}"
                                                    @endif
                                                        >
                                            <span class="input-group-addon" onclick="fechaDocumento('filtro_fecha_inicio');" style="cursor:pointer;">
                                                <i class="fa fa-calendar" aria-hidden="true" style="color: black;"></i>
                                            </span>
                                        </div>
                                    </div>

                                    <label for="filtro_fecha_termino" class="col-sm-1 control-label label-form">Fecha Termino</label>
                                    <div class="col-sm-2">
                                        <div class="input-group">
                                            <input type="text" class="form-control fechas" id="filtro_fecha_termino" name="filtro_fecha_termino" v
                                                    @if( ! isset($fechaTermino) )
                                                        value="{{ date("m/Y") }}"
                                                    @else
                                                        value="{{ $fechaTermino }}"
                                                    @endif
                                                        >
                                            <span class="input-group-addon" onclick="fechaDocumento('filtro_fecha_termino');" style="cursor:pointer;">
                                                <i class="fa fa-calendar" aria-hidden="true" style="color: black;"></i>
                                            </span>
                                        </div>
                                    </div> --}}

                                </div>

                                {{-- 
                                    <div class="form-group row">

                                    <label for="filtro_proveedor" class="col-sm-1 control-label label-form">Proveedor</label>
                                    <div class="col-sm-3">
                                        <select name="filtro_proveedor[]" id="filtro_proveedor" class="form-control select2_filtro" multiple style="width: 100%">
                                            @foreach ( $proveedores as $proveedor )
                                                <option value="{{ $proveedor->id }}" >{{ $proveedor->rut }} {{ $proveedor->nombre }}</option>
                                            @endforeach
                                        </select>
                                    </div>

                                    <label for="filtro_tipo_documento" class="col-sm-1 control-label label-form">Tipo de Documento</label>
                                    <div class="col-sm-3">
                                        <select name="filtro_tipo_documento[]" id="filtro_tipo_documento" class="form-control select2_filtro" multiple style="width: 100%">
                                            @foreach ( $tiposDocumento as $tipoDocumento )
                                                <option value="{{ $tipoDocumento->id }}" >{{ $tipoDocumento->nombre }}</option>
                                            @endforeach
                                        </select>
                                    </div>

                                    <label for="filtro_modalidad_compra" class="col-sm-1 control-label label-form">Modalidad de Compra</label>
                                    <div class="col-sm-3">
                                        <select name="filtro_modalidad_compra[]" id="filtro_modalidad_compra" class="form-control select2_filtro" multiple="multiple">
                                            @foreach ( $modalidadesCompra as $modalidadCompra )
                                                <option value="{{ $modalidadCompra->id }}" >{{ $modalidadCompra->nombre }}</option>
                                            @endforeach
                                        </select>
                                    </div>

                                    </div>

                                    <div class="form-group row">

                                        <label for="filtro_tipo_adjudicacion" class="col-sm-1 control-label label-form">Tipo de Adjudicación</label>
                                        <div class="col-sm-3">
                                            <select name="filtro_tipo_adjudicacion[]" id="filtro_tipo_adjudicacion" class="form-control select2_filtro" multiple="multiple">
                                                @foreach ( $tiposAdjudicacion as $tipoAdjudicacion )
                                                    <option value="{{ $tipoAdjudicacion->id }}" >{{ $tipoAdjudicacion->nombre }}</option>
                                                @endforeach
                                            </select>
                                        </div>

                                        <label for="filtro_tipo_informe" class="col-sm-1 control-label label-form">Tipo de Informe</label>
                                        <div class="col-sm-3">
                                            <select name="filtro_tipo_informe[]" id="filtro_tipo_informe" class="form-control select2_filtro" multiple="multiple">
                                                @foreach ( $tiposInforme as $tipoInforme )
                                                    <option value="{{ $tipoInforme->id }}" >{{ $tipoInforme->nombre }}</option>
                                                @endforeach
                                            </select>
                                        </div>

                                        <label for="filtro_responsable" class="col-sm-1 control-label label-form">Responsable</label>
                                        <div class="col-sm-3">
                                            <select name="filtro_responsable[]" id="filtro_responsable" class="form-control select2_filtro" multiple="multiple">
                                                @foreach ( $usuariosResponsables as $responsable )
                                                    <option value="{{ $responsable->id }}" >{{ $responsable->name }}</option>
                                                @endforeach
                                            </select>
                                        </div>

                                    </div>

                                    <div class="form-group row">

                                        <label for="filtro_referente_tecnico" class="col-sm-1 control-label label-form">Referente Técnico</label>
                                        <div class="col-sm-3">
                                            <select name="filtro_referente_tecnico[]" id="filtro_referente_tecnico" class="form-control select2_filtro" multiple="multiple">
                                                @foreach ( $referentesTecnicos as $referenteTecnico )
                                                    <option value="{{ $referenteTecnico->id }}" >{{ $referenteTecnico->nombre }} - {{ $referenteTecnico->responsable }}</option>
                                                @endforeach
                                            </select>
                                        </div>

                                        <label for="filtro_digitador" class="col-sm-1 control-label label-form">Digitador</label>
                                        <div class="col-sm-3">
                                            <select name="filtro_digitador[]" id="filtro_digitador" class="form-control select2_filtro" multiple="multiple">
                                                @foreach ( $usuariosDigitadores as $digitador )
                                                    <option value="{{ $digitador->id }}" >{{ $digitador->name }}</option>
                                                @endforeach
                                            </select>
                                        </div>

                                        <label for="filtro_item_presupuestario" class="col-sm-1 control-label label-form">Ítem </label>
                                        <div class="col-sm-3">
                                            <select class="form-control select2_filtro " id="filtro_item_presupuestario" name="filtro_item_presupuestario[]" multiple="multiple" >
                                                @foreach ( $itemsPresupuestarios as $item )
                                                    <option value="{{ $item->id }}" >{{ $item->codigo() }} {{ $item->clasificador_presupuestario }}</option>
                                                @endforeach
                                            </select>
                                        </div>

                                    </div>

                                    <div class="form-group row">

                                        <label for="filtro_id_sigfe" class="col-sm-1 control-label label-form">ID SIGFE </label>
                                        <div class="col-sm-3">
                                            <input type="text" class="form-control " id="filtro_id_sigfe" name="filtro_id_sigfe"
                                            @if(isset($filtroIdSigfe)) value="{{ $filtroIdSigfe }}"  @endif >
                                        </div>

                                        <label for="filtro_numero_documento" class="col-sm-1 control-label label-form">N° Documento </label>
                                        <div class="col-sm-3">
                                            <input type="text" class="form-control " id="filtro_numero_documento" name="filtro_numero_documento"
                                            @if(isset($filtroNumeroDocumento)) value="{{ $filtroNumeroDocumento }}"  @endif >
                                        </div>

                                        <label for="total_devengado" class="col-sm-2 control-label label-form">Total Devengado </label>
                                        <div class="col-sm-2">
                                            <input type="text" class="form-control text-right" id="total_devengado" name="total_devengado" readonly>
                                        </div>
                                    </div> 
                                
                                --}}
                                
                            </form>

                            {{-- <div class="row">
                                <div id="chart_div" style="width: 900px; height: 500px;" class="col-md-12 text-center"></div>
                            </div> --}}

                            <div class="row">
                                <div id="chart_2018_div" style="width: 1600px; height: 800px;" class="col-md-12 text-center"></div>
                            </div>

                            <div class="row">
                                <div id="chart_2019_div" style="width: 1600px; height: 800px;" class="col-md-12 text-center"></div>
                            </div>
                            
                        </div>
                    </div>
                    <!-- END EXAMPLE TABLE PORTLET-->
                </div>
            </div>
            <!-- END PAGE CONTENT-->
			

		</div> <!--/div class="page-content" -->
	</div> <!--/div class="page-content-wrapper" -->
</div><!--/div class="page-content-wrapper" -->
<!-- END CONTENT -->
@endsection

@push('script-footer')
<!-- BEGIN PAGE LEVEL PLUGINS -->
<script type="text/javascript" src="{{ asset('assets/plugins/select2/select2.min.js') }} "></script>
<script type="text/javascript" src="{{ asset('assets/plugins/data-tables/jquery.dataTables.min.js') }}"></script>
<script type="text/javascript" src="{{ asset('assets/plugins/data-tables/DT_bootstrap.js') }}"></script>

<link rel="stylesheet" type="text/css" href="{{asset('assets/plugins/bootstrap-datepicker/css/datepicker.css')}}"/>

<script type="text/javascript" src="{{asset('assets/plugins/bootstrap-datepicker/js/bootstrap-datepicker.js')}}"></script>
<script type="text/javascript" src="{{asset('assets/plugins/bootstrap-datetimepicker/js/bootstrap-datetimepicker.js')}}"></script>
<!-- END PAGE LEVEL PLUGINS -->
<!-- BEGIN PAGE LEVEL SCRIPTS -->
<script src="{{ asset('assets/scripts/app.js') }} "></script>

<!--Load the AJAX API-->
<script type="text/javascript" src="{{ asset('assets/graficos/charts.js') }}"></script>
<script type="text/javascript">

    google.charts.load('current', {'packages':['corechart']});
    google.charts.setOnLoadCallback(drawVisualization);

    function drawVisualization() {
        // Some raw data (not necessarily accurate)
        // var data = google.visualization.arrayToDataTable([
        //     ['Month', 'Bolivia', 'Ecuador', 'Madagascar', 'Papua New Guinea', 'Rwanda', 'Average'],
        //     ['2004/05',  165,      938,         522,             998,           450,      614.6],
        //     ['2005/06',  135,      1120,        599,             1268,          288,      682],
        //     ['2006/07',  157,      1167,        587,             807,           397,      623],
        //     ['2007/08',  139,      1110,        615,             968,           215,      609.4],
        //     ['2008/09',  136,      691,         629,             1026,          366,      569.6]
        // ]);

        // var options = {
        //     title : 'Monthly Coffee Production by Country',
        //     vAxis: {title: 'Cups'},
        //     hAxis: {title: 'Month'},
        //     seriesType: 'bars',
        //     series: {
        //         5: {type: 'line'}
        //     },
        //     animation: {
        //         duration: 6000,
        //         startup: true
        //     }
        // };

        // var chart = new google.visualization.ComboChart(document.getElementById('chart_div'));
        // chart.draw(data, options);

        // Año 2018 y 2019
        @if( isset($año2018) && isset($año2019) )

            ////////////////////////////////////////////////////// 2018

            // Some raw data (not necessarily accurate)
            var data2018 = google.visualization.arrayToDataTable([
                // ['Ítems', 'Enero', 'Febrero', 'Marzo', 'Abril', 'Mayo', 'Junio', 'Julio', 'Agosto', 'Septiembre', 'Octubre', 'Noviembre', 'Diciembre', 'Promedio'],
                // ['7801', 165, 938, 522, 998, 450, 614, 123, 503, 120, 870, 560, 432, 123.4],
                // ['7801', 265, 548, 122, 398, 750, 214, 623, 103, 620, 470, 860, 932, 523.4],

                ['Ítems', 'Enero', 'Febrero', 'Marzo', 'Abril', 'Mayo', 'Junio', 'Julio', 'Agosto', 'Septiembre', 'Octubre', 'Noviembre', 'Diciembre', 'Promedio'],
                // ['7801', 165, 938, 522, 998, 450, 614, 123, 503, 120, 870, 560, 432, 123.4],
                @foreach ( $año2018 as $cadaItem )
                    [ "{{ $cadaItem['item'] }}" , {{ $cadaItem[1] }}, {{ $cadaItem[2] }}, {{ $cadaItem[3] }}, {{ $cadaItem[4] }}, {{ $cadaItem[5] }}, {{ $cadaItem[6] }}, 
                       {{ $cadaItem[7] }}, {{ $cadaItem[8] }}, {{ $cadaItem[9] }}, {{ $cadaItem[10] }}, {{ $cadaItem[11] }}, {{ $cadaItem[12] }}, {{ $cadaItem[13] }}  ],
                @endforeach
            ]);

            var options2018 = {
                title : '2018',
                vAxis: {title: 'Monto'},
                hAxis: {title: 'Ítem'},
                seriesType: 'bars',
                series: {
                    12: {type: 'line'}
                },
                animation: {
                    duration: 6000,
                    startup: true
                }
            };

            var chart2018 = new google.visualization.ComboChart(document.getElementById('chart_2018_div'));
            chart2018.draw(data2018, options2018);




            ////////////////////////////////////////////////////// 2019
            // Some raw data (not necessarily accurate)
            var data2019 = google.visualization.arrayToDataTable([
                // ['Ítems', 'Enero', 'Febrero', 'Marzo', 'Abril', 'Mayo', 'Junio', 'Julio', 'Agosto', 'Septiembre', 'Octubre', 'Noviembre', 'Diciembre', 'Promedio'],
                // ['7801', 165, 938, 522, 998, 450, 614, 123, 503, 120, 870, 560, 432, 123.4],
                // ['7801', 265, 548, 122, 398, 750, 214, 623, 103, 620, 470, 860, 932, 523.4],

                ['Ítems', 'Enero', 'Febrero', 'Marzo', 'Abril', 'Mayo', 'Junio', 'Julio', 'Agosto', 'Septiembre', 'Octubre', 'Noviembre', 'Diciembre', 'Promedio'],
                // ['7801', 165, 938, 522, 998, 450, 614, 123, 503, 120, 870, 560, 432, 123.4],
                @foreach ( $año2019 as $cadaItem )
                    [ "{{ $cadaItem['item'] }}" , {{ $cadaItem[1] }}, {{ $cadaItem[2] }}, {{ $cadaItem[3] }}, {{ $cadaItem[4] }}, {{ $cadaItem[5] }}, {{ $cadaItem[6] }}, 
                       {{ $cadaItem[7] }}, {{ $cadaItem[8] }}, {{ $cadaItem[9] }}, {{ $cadaItem[10] }}, {{ $cadaItem[11] }}, {{ $cadaItem[12] }}, {{ $cadaItem[13] }} ],
                @endforeach
            ]);

            var options2019 = {
                title : '2019',
                vAxis: {title: 'Monto'},
                hAxis: {title: 'Ítem'},
                seriesType: 'bars',
                series: {
                    12: {type: 'line'}
                },
                animation: {
                    duration: 6000,
                    startup: true
                }
            };

            var chart2019 = new google.visualization.ComboChart(document.getElementById('chart_2019_div'));
            chart2019.draw(data2019, options2019);
            
        @endif
    }

	$(document).ready(function(){

        $('#filtrarButton').click(function(){

            $("#modalCarga").modal({backdrop: 'static', keyboard: false});
            $('#modalCargaImg').addClass('fa-pulso');
                
        });

		App.init(); // initlayout and core plugins
		// TableAdvanced.init();

        $("#graficos").addClass( "active" );
		$("#items-devengados-li").addClass( "active" );
		$("#graficos-a").append( '<span class="selected"></span>' );

        // $('#filtro_fecha_inicio').datepicker({
        //     format: 'mm/yyyy',
        //     endDate: new Date(),
        //     autoclose: true,
        //     language: 'es',
        //     viewMode: 'months',
        //     minViewMode: 'months'
        // });

        // $('#filtro_fecha_termino').datepicker({
        //     format: 'mm/yyyy',
        //     endDate: new Date(),
        //     autoclose: true,
        //     language: 'es',
        //     viewMode: 'months',
        //     minViewMode: 'months'
        // });

        $(".select2_filtro").select2({
            // allowClear: true,
        });
	});

    // function fechaDocumento(id_fecha) {
    //     $('#' + id_fecha).datepicker("show");
    // }

</script>

@endpush
