@extends('master')

@section('title', 'Gráficos - Documentos Ingresados')

@push('css-head')
<!-- BEGIN PAGE LEVEL STYLES -->
<link rel="stylesheet" type="text/css" href="{{asset('assets/plugins/select2/select2_conquer.css')}}"/>
<link rel="stylesheet" href="{{asset('assets/plugins/data-tables/DT_bootstrap.css')}}"/>
<!-- END PAGE LEVEL STYLES -->
<style>
    .label-form {
        font-weight: bold;
        color: black;
        text-align: right;
    }
</style>
@endpush

@section('content')

<!-- BEGIN CONTENT -->

<div class="page-content-wrapper">
	<div class="page-content-wrapper">
		<div class="page-content">
						
			<!-- BEGIN PAGE CONTENT-->
			<div class="row">
                <div class="col-md-12">
                    <!-- BEGIN EXAMPLE TABLE PORTLET-->
                    <div class="portlet">
                        <div class="portlet-title">
                            <div class="caption">
                                <i class="fas fa-book"></i> Gráficos - Documentos Ingresados
                            </div>
                        </div>
                        <div class="portlet-body">
                            <form action="{{url('graficos/documentos_ingresados')}}" method="post" enctype="multipart/form-data" id="filtros-docs" class="form-horizontal">
                                <h4 class="form-section" style="color: #69aa46;"><strong>Filtros</strong>
                                <div class="btn-group pull-right">
                                    
                                    <button class="btn btn-info btn-xs" title="Filtrar" >
                                        <i class="fas fa-search" style="color:black"></i> Filtrar
                                    </button>
                                    
                                </div>
                                </h4>
                                <div class="form-group row">

                                    {{-- <label for="filtro_fecha_a_utilizar" class="col-sm-1 control-label label-form">Fecha </label>
                                    <div class="col-sm-3">
                                        <select class="form-control select2_filtro " id="filtro_fecha_a_utilizar" name="filtro_fecha_a_utilizar" >
                                            @foreach ( $fechasUtilizar as $fechaUtilizar )
                                                <option value="{{ $fechaUtilizar->opcion }}" >{{ $fechaUtilizar->opcion }}</option>
                                            @endforeach
                                        </select>
                                    </div> --}}

                                    <div class="col-sm-2">
                                        <label for="filtro_fecha_inicio" class="label-form">Fecha Inicio</label>                                    
                                        <div class="input-group">
                                            <input type="text" class="form-control fechas" id="filtro_fecha_inicio" name="filtro_fecha_inicio" 
                                                    @if( ! isset($fechaInicio) )
                                                        value="{{ date("m/Y") }}"
                                                    @else
                                                        value="{{ $fechaInicio }}"
                                                    @endif
                                                        >
                                            <span class="input-group-addon" onclick="fechaDocumento('filtro_fecha_inicio');" style="cursor:pointer;">
                                                <i class="fa fa-calendar" aria-hidden="true" style="color: black;"></i>
                                            </span>
                                        </div>
                                    </div>

                                    <div class="col-sm-2">
                                        <label for="filtro_fecha_termino" class="label-form">Fecha Termino</label>
                                        <div class="input-group">
                                            <input type="text" class="form-control fechas" id="filtro_fecha_termino" name="filtro_fecha_termino" v
                                                    @if( ! isset($fechaTermino) )
                                                        value="{{ date("m/Y") }}"
                                                    @else
                                                        value="{{ $fechaTermino }}"
                                                    @endif
                                                        >
                                            <span class="input-group-addon" onclick="fechaDocumento('filtro_fecha_termino');" style="cursor:pointer;">
                                                <i class="fa fa-calendar" aria-hidden="true" style="color: black;"></i>
                                            </span>
                                        </div>
                                    </div>

                                </div>
                                
                            </form>

                            <div class="row">
                                <div id="chart_div" class="col-md-6 text-center"></div>
                                <div id="chart2_div" class="col-md-6 text-center"></div>
                            </div>

                            <div class="row">
                                <div id="user_chart_div" class="col-md-10 col-md-offset-1"></div>
                            </div>
                            
                        </div>
                    </div>
                    <!-- END EXAMPLE TABLE PORTLET-->
                </div>
            </div>
            <!-- END PAGE CONTENT-->
			

		</div> <!--/div class="page-content" -->
	</div> <!--/div class="page-content-wrapper" -->
</div><!--/div class="page-content-wrapper" -->
<!-- END CONTENT -->
@endsection

@push('script-footer')
<!-- BEGIN PAGE LEVEL PLUGINS -->
<script type="text/javascript" src="{{ asset('assets/plugins/select2/select2.min.js') }} "></script>
<script type="text/javascript" src="{{ asset('assets/plugins/data-tables/jquery.dataTables.min.js') }}"></script>
<script type="text/javascript" src="{{ asset('assets/plugins/data-tables/DT_bootstrap.js') }}"></script>

<link rel="stylesheet" type="text/css" href="{{asset('assets/plugins/bootstrap-datepicker/css/datepicker.css')}}"/>

<script type="text/javascript" src="{{asset('assets/plugins/bootstrap-datepicker/js/bootstrap-datepicker.js')}}"></script>
<script type="text/javascript" src="{{asset('assets/plugins/bootstrap-datetimepicker/js/bootstrap-datetimepicker.js')}}"></script>
<!-- END PAGE LEVEL PLUGINS -->
<!-- BEGIN PAGE LEVEL SCRIPTS -->
<script src="{{ asset('assets/scripts/app.js') }} "></script>

<!--Load the AJAX API-->
<script type="text/javascript" src="{{ asset('assets/graficos/charts.js') }}"></script>
<script type="text/javascript">

  // Load the Visualization API and the corechart package.
  google.charts.load('current', {'packages':['corechart']});

  // Set a callback to run when the Google Visualization API is loaded.
  google.charts.setOnLoadCallback(drawChart);

  // Callback that creates and populates a data table,
  // instantiates the pie chart, passes in the data and
  // draws it.
  function drawChart() {

    // Create the data table.
    var data = new google.visualization.DataTable();

    data.addColumn('string', 'Topping');

    data.addColumn('number', 'Documentos');

    data.addRows([

      ['Programa', {{ $indicadores->docsMesPrograma }}],
      ['Cenabast', {{ $indicadores->docsMesCenabast }}],
      ['Clinicas', {{ $indicadores->docsMesClinicas }}],
      ['Convenio', {{ $indicadores->docsMesConvenio }}],
      ['Consumos básicos', {{ $indicadores->docsMesConsumosBasicos }}],
      ['Honorarios', {{ $indicadores->docsMesHonorarios }}],
      ['Fondo fijo', {{ $indicadores->docsMesFondoFijo }}],
      ['Líquida Anticipo', {{ $indicadores->docsMesLiquidaAnticipo }}],
      ['Gob Transparente', {{ $indicadores->docsMesGobTransparente }}]

    ]);

    // Set chart options
    var options = {

        'legend': 'left',
        title: 'Total : {{ $indicadores->documentosMes }}',
        'is3D': true,
        'width': 600,
        'height': 500

        };

    // Instantiate and draw our chart, passing in some options.
    var chart = new google.visualization.PieChart(document.getElementById('chart_div'));
    chart.draw(data, options);

    var barchart_options = {

        title: 'Total : {{ $indicadores->documentosMes }}',
        width: 600,
        height: 500,
        legend: 'none'

    };

    var barchart = new google.visualization.BarChart(document.getElementById('chart2_div'));
    barchart.draw(data, barchart_options);


    var dataUsers = new google.visualization.arrayToDataTable([
        [ 'Usuario', 'N°', {role: 'style'}, { role: 'annotation' } ],
        @foreach ( $users as $user)
            [ '{{ $user->getNombre() }}', {{ $user->getDocumentos->count() }}, 'color: gray',  '{{ formatoMiles($user->getDocumentos->count()) }}'],
        @endforeach
    ]);

    // dataUsers.addColumn( ;

    // dataUsers.addColumn('number', 'Docs');

    // dataUsers.addRows([

    //     @foreach ( $users as $user)
    //         ['{{ $user->getNombre() }}', {{ $user->getDocumentos->count() }}],
    //     @endforeach

    // ]);

    var users_barchart_options = {

        title: 'Documentos ingresados por usuario',
        width: 1200,
        height: 800,
        legend: 'none',

    };

        var users_barchart = new google.visualization.BarChart(document.getElementById('user_chart_div'));
        users_barchart.draw(dataUsers, users_barchart_options);
  }

</script>

<script type="text/javascript">
	$(document).ready(function(){
		App.init(); // initlayout and core plugins
		// TableAdvanced.init();

        $("#graficos").addClass( "active" );
		$("#documentos-ingresados-li").addClass( "active" );
		$("#graficos-a").append( '<span class="selected"></span>' );

        $('#filtro_fecha_inicio').datepicker({
            format: 'mm/yyyy',
            endDate: new Date(),
            autoclose: true,
            language: 'es',
            viewMode: 'months',
            minViewMode: 'months'
        });

        $('#filtro_fecha_termino').datepicker({
            format: 'mm/yyyy',
            endDate: new Date(),
            autoclose: true,
            language: 'es',
            viewMode: 'months',
            minViewMode: 'months'
        });
	});

    function fechaDocumento(id_fecha) {
        $('#' + id_fecha).datepicker("show");
    }

</script>

@endpush
