@extends('master')

@section('title', 'Gráficos - Usuarios y Archivos Acepta')

@push('css-head')
<!-- BEGIN PAGE LEVEL STYLES -->
<link rel="stylesheet" type="text/css" href="{{asset('assets/plugins/select2/select2_conquer.css')}}"/>
<link rel="stylesheet" href="{{asset('assets/plugins/data-tables/DT_bootstrap.css')}}"/>
<!-- END PAGE LEVEL STYLES -->
<style>
    .label-form {
        font-weight: bold;
        color: black;
        text-align: right;
    }
</style>
@endpush

@section('content')

<!-- BEGIN CONTENT -->

<div class="page-content-wrapper">
	<div class="page-content-wrapper">
		<div class="page-content">
						
			<!-- BEGIN PAGE CONTENT-->
			<div class="row">
                <div class="col-md-12">
                    <!-- BEGIN EXAMPLE TABLE PORTLET-->
                    <div class="portlet">
                        <div class="portlet-title">
                            <div class="caption">
                                <i class="fas fa-chart-area"></i> Gráficos - Usuarios y Archivos Acepta
                            </div>
                        </div>
                        <div class="portlet-body">
                            <form action="{{url('graficos/usuarios_acepta')}}" method="post" enctype="multipart/form-data" id="filtros-docs" class="form-horizontal">
                                <h4 class="form-section" style="color: #69aa46;"><strong>Filtros</strong>
                                <div class="btn-group pull-right">
                                    
                                    <button class="btn btn-info btn-xs" title="Filtrar" >
                                        <i class="fas fa-search" style="color:black"></i> Filtrar
                                    </button>
                                    
                                </div>
                                </h4>
                                <div class="form-group row">

                                    {{-- <label for="filtro_fecha_a_utilizar" class="col-sm-1 control-label label-form">Fecha </label>
                                    <div class="col-sm-3">
                                        <select class="form-control select2_filtro " id="filtro_fecha_a_utilizar" name="filtro_fecha_a_utilizar" >
                                            @foreach ( $fechasUtilizar as $fechaUtilizar )
                                                <option value="{{ $fechaUtilizar->opcion }}" >{{ $fechaUtilizar->opcion }}</option>
                                            @endforeach
                                        </select>
                                    </div> --}}

                                    <div class="col-sm-2">
                                        <label for="filtro_fecha_inicio" class="label-form">Fecha Inicio</label>
                                        <div class="input-group">
                                            <input type="text" class="form-control fechas" id="filtro_fecha_inicio" name="filtro_fecha_inicio" 
                                                    @if( ! isset($fechaInicio) )
                                                        value="{{ date("m/Y") }}"
                                                    @else
                                                        value="{{ $fechaInicio }}"
                                                    @endif
                                                        >
                                            <span class="input-group-addon" onclick="fechaDocumento('filtro_fecha_inicio');" style="cursor:pointer;">
                                                <i class="fa fa-calendar" aria-hidden="true" style="color: black;"></i>
                                            </span>
                                        </div>
                                    </div>

                                    <div class="col-sm-2">
                                        <label for="filtro_fecha_termino" class="label-form">Fecha Termino</label>
                                        <div class="input-group">
                                            <input type="text" class="form-control fechas" id="filtro_fecha_termino" name="filtro_fecha_termino" v
                                                    @if( ! isset($fechaTermino) )
                                                        value="{{ date("m/Y") }}"
                                                    @else
                                                        value="{{ $fechaTermino }}"
                                                    @endif
                                                        >
                                            <span class="input-group-addon" onclick="fechaDocumento('filtro_fecha_termino');" style="cursor:pointer;">
                                                <i class="fa fa-calendar" aria-hidden="true" style="color: black;"></i>
                                            </span>
                                        </div>
                                    </div>

                                </div>

                                {{-- 
                                    <div class="form-group row">

                                    <label for="filtro_proveedor" class="col-sm-1 control-label label-form">Proveedor</label>
                                    <div class="col-sm-3">
                                        <select name="filtro_proveedor[]" id="filtro_proveedor" class="form-control select2_filtro" multiple style="width: 100%">
                                            @foreach ( $proveedores as $proveedor )
                                                <option value="{{ $proveedor->id }}" >{{ $proveedor->rut }} {{ $proveedor->nombre }}</option>
                                            @endforeach
                                        </select>
                                    </div>

                                    <label for="filtro_tipo_documento" class="col-sm-1 control-label label-form">Tipo de Documento</label>
                                    <div class="col-sm-3">
                                        <select name="filtro_tipo_documento[]" id="filtro_tipo_documento" class="form-control select2_filtro" multiple style="width: 100%">
                                            @foreach ( $tiposDocumento as $tipoDocumento )
                                                <option value="{{ $tipoDocumento->id }}" >{{ $tipoDocumento->nombre }}</option>
                                            @endforeach
                                        </select>
                                    </div>

                                    <label for="filtro_modalidad_compra" class="col-sm-1 control-label label-form">Modalidad de Compra</label>
                                    <div class="col-sm-3">
                                        <select name="filtro_modalidad_compra[]" id="filtro_modalidad_compra" class="form-control select2_filtro" multiple="multiple">
                                            @foreach ( $modalidadesCompra as $modalidadCompra )
                                                <option value="{{ $modalidadCompra->id }}" >{{ $modalidadCompra->nombre }}</option>
                                            @endforeach
                                        </select>
                                    </div>

                                    </div>

                                    <div class="form-group row">

                                        <label for="filtro_tipo_adjudicacion" class="col-sm-1 control-label label-form">Tipo de Adjudicación</label>
                                        <div class="col-sm-3">
                                            <select name="filtro_tipo_adjudicacion[]" id="filtro_tipo_adjudicacion" class="form-control select2_filtro" multiple="multiple">
                                                @foreach ( $tiposAdjudicacion as $tipoAdjudicacion )
                                                    <option value="{{ $tipoAdjudicacion->id }}" >{{ $tipoAdjudicacion->nombre }}</option>
                                                @endforeach
                                            </select>
                                        </div>

                                        <label for="filtro_tipo_informe" class="col-sm-1 control-label label-form">Tipo de Informe</label>
                                        <div class="col-sm-3">
                                            <select name="filtro_tipo_informe[]" id="filtro_tipo_informe" class="form-control select2_filtro" multiple="multiple">
                                                @foreach ( $tiposInforme as $tipoInforme )
                                                    <option value="{{ $tipoInforme->id }}" >{{ $tipoInforme->nombre }}</option>
                                                @endforeach
                                            </select>
                                        </div>

                                        <label for="filtro_responsable" class="col-sm-1 control-label label-form">Responsable</label>
                                        <div class="col-sm-3">
                                            <select name="filtro_responsable[]" id="filtro_responsable" class="form-control select2_filtro" multiple="multiple">
                                                @foreach ( $usuariosResponsables as $responsable )
                                                    <option value="{{ $responsable->id }}" >{{ $responsable->name }}</option>
                                                @endforeach
                                            </select>
                                        </div>

                                    </div>

                                    <div class="form-group row">

                                        <label for="filtro_referente_tecnico" class="col-sm-1 control-label label-form">Referente Técnico</label>
                                        <div class="col-sm-3">
                                            <select name="filtro_referente_tecnico[]" id="filtro_referente_tecnico" class="form-control select2_filtro" multiple="multiple">
                                                @foreach ( $referentesTecnicos as $referenteTecnico )
                                                    <option value="{{ $referenteTecnico->id }}" >{{ $referenteTecnico->nombre }} - {{ $referenteTecnico->responsable }}</option>
                                                @endforeach
                                            </select>
                                        </div>

                                        <label for="filtro_digitador" class="col-sm-1 control-label label-form">Digitador</label>
                                        <div class="col-sm-3">
                                            <select name="filtro_digitador[]" id="filtro_digitador" class="form-control select2_filtro" multiple="multiple">
                                                @foreach ( $usuariosDigitadores as $digitador )
                                                    <option value="{{ $digitador->id }}" >{{ $digitador->name }}</option>
                                                @endforeach
                                            </select>
                                        </div>

                                        <label for="filtro_item_presupuestario" class="col-sm-1 control-label label-form">Ítem </label>
                                        <div class="col-sm-3">
                                            <select class="form-control select2_filtro " id="filtro_item_presupuestario" name="filtro_item_presupuestario[]" multiple="multiple" >
                                                @foreach ( $itemsPresupuestarios as $item )
                                                    <option value="{{ $item->id }}" >{{ $item->codigo() }} {{ $item->clasificador_presupuestario }}</option>
                                                @endforeach
                                            </select>
                                        </div>

                                    </div>

                                    <div class="form-group row">

                                        <label for="filtro_id_sigfe" class="col-sm-1 control-label label-form">ID SIGFE </label>
                                        <div class="col-sm-3">
                                            <input type="text" class="form-control " id="filtro_id_sigfe" name="filtro_id_sigfe"
                                            @if(isset($filtroIdSigfe)) value="{{ $filtroIdSigfe }}"  @endif >
                                        </div>

                                        <label for="filtro_numero_documento" class="col-sm-1 control-label label-form">N° Documento </label>
                                        <div class="col-sm-3">
                                            <input type="text" class="form-control " id="filtro_numero_documento" name="filtro_numero_documento"
                                            @if(isset($filtroNumeroDocumento)) value="{{ $filtroNumeroDocumento }}"  @endif >
                                        </div>

                                        <label for="total_devengado" class="col-sm-2 control-label label-form">Total Devengado </label>
                                        <div class="col-sm-2">
                                            <input type="text" class="form-control text-right" id="total_devengado" name="total_devengado" readonly>
                                        </div>
                                    </div> 
                                
                                --}}
                                
                            </form>

                            <div class="row">
                                <div id="user_chart_div" class="col-md-12" style="width: 1600px; height: 800px;"></div>
                            </div>
                            
                        </div>
                    </div>
                    <!-- END EXAMPLE TABLE PORTLET-->
                </div>
            </div>
            <!-- END PAGE CONTENT-->
			

		</div> <!--/div class="page-content" -->
	</div> <!--/div class="page-content-wrapper" -->
</div><!--/div class="page-content-wrapper" -->
<!-- END CONTENT -->
@endsection

@push('script-footer')
<!-- BEGIN PAGE LEVEL PLUGINS -->
<script type="text/javascript" src="{{ asset('assets/plugins/select2/select2.min.js') }} "></script>
<script type="text/javascript" src="{{ asset('assets/plugins/data-tables/jquery.dataTables.min.js') }}"></script>
<script type="text/javascript" src="{{ asset('assets/plugins/data-tables/DT_bootstrap.js') }}"></script>

<link rel="stylesheet" type="text/css" href="{{asset('assets/plugins/bootstrap-datepicker/css/datepicker.css')}}"/>

<script type="text/javascript" src="{{asset('assets/plugins/bootstrap-datepicker/js/bootstrap-datepicker.js')}}"></script>
<script type="text/javascript" src="{{asset('assets/plugins/bootstrap-datetimepicker/js/bootstrap-datetimepicker.js')}}"></script>
<!-- END PAGE LEVEL PLUGINS -->
<!-- BEGIN PAGE LEVEL SCRIPTS -->
<script src="{{ asset('assets/scripts/app.js') }} "></script>

<!--Load the AJAX API-->
<script type="text/javascript" src="{{ asset('assets/graficos/charts.js') }}"></script>
<script type="text/javascript">

  // Load the Visualization API and the corechart package.
  google.charts.load('current', {'packages':['corechart']});
  google.charts.setOnLoadCallback(drawVisualization);

  // Callback that creates and populates a data table,
  // instantiates the pie chart, passes in the data and
  // draws it.
  function drawVisualization() {

    var data = google.visualization.arrayToDataTable([
            // ['Ítems', 'Enero', 'Febrero', 'Marzo', 'Abril', 'Mayo', 'Junio', 'Julio', 'Agosto', 'Septiembre', 'Octubre', 'Noviembre', 'Diciembre', 'Promedio'],
            // ['7801', 165, 938, 522, 998, 450, 614, 123, 503, 120, 870, 560, 432, 123.4],
            // ['7801', 265, 548, 122, 398, 750, 214, 623, 103, 620, 470, 860, 932, 523.4],

            [ 'Usuario', 'Cargados', 'Rezagados', 'Rechazados','A Bodega','Sin Procesar' ],
            
            @foreach ( $users as $user )
                [ "{{ $user->getNombre().'  Total: '.$user->archivosTotales }}" , {{ $user->archivosCargados }}, 
                   {{ $user->archivosRezagados }}, {{ $user->archivosRechazados }}, {{ $user->archivosToBodega }}, 
                   {{ $user->archivosSinProcesar }} ],
            @endforeach
        ]);

        var options = {
            title : 'Información de los usuarios responsables de los archivos acepta',
            vAxis: {title: 'Cantidad'},
            hAxis: {title: 'Usuario'},
            seriesType: 'bars',
            // series: {
            //     12: {type: 'line'}
            // },
            animation: {
                duration: 6000,
                startup: true
            }
        };

        var chart = new google.visualization.ComboChart(document.getElementById('user_chart_div'));
        chart.draw(data, options);
  }

</script>

<script type="text/javascript">
	$(document).ready(function(){
		App.init(); // initlayout and core plugins
		// TableAdvanced.init();

        $("#graficos").addClass( "active" );
		$("#usuarios-acepta-li").addClass( "active" );
		$("#graficos-a").append( '<span class="selected"></span>' );

        $('#filtro_fecha_inicio').datepicker({
            format: 'mm/yyyy',
            endDate: new Date(),
            autoclose: true,
            language: 'es',
            viewMode: 'months',
            minViewMode: 'months'
        });

        $('#filtro_fecha_termino').datepicker({
            format: 'mm/yyyy',
            endDate: new Date(),
            autoclose: true,
            language: 'es',
            viewMode: 'months',
            minViewMode: 'months'
        });
	});

    function fechaDocumento(id_fecha) {
        $('#' + id_fecha).datepicker("show");
    }

</script>

@endpush
