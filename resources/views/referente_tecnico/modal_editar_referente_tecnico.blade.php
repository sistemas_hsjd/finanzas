<link href="{{ asset('sistema/css/modales.css') }} " rel="stylesheet" type="text/css"/>
<div class="modal fade" id="modalEditar" role="dialog" aria-labelledby="myModalLabel3" aria-hidden="true">
	<div class="modal-dialog" style="width:1100px;">
		<div class="modal-content">
            <div class="modal-header">
				<button type="button" title="Cerrar" class="close" data-dismiss="modal" aria-hidden="true"></button>
				<h4 class="modal-title" ><strong><i class="fas fa-id-card-alt"></i>&nbsp;&nbsp;Editar Referente Técnico</strong></h4>
            </div>

            <form action="{{ asset('referente_tecnico/editar') }}" method="post" class="horizontal-form" id="form" autocomplete="off">
                <div class="modal-body">

                    <div class="form-body">
                        <div class="note note-danger" id="divErrores" style="display:none;">
                            <h4 class="block" style="margin-bottom:5 px;">Debe Completar los siguientes campos: </h4>
                            <ul id="ulErrores"></ul>
                        </div>
                        <div class="form-group row">
                            <label for="usuario" class="col-sm-2 control-label label-form">Usuario <span class="span-label">*</span></label>
                            <div class="col-sm-6 form-validate">
                                <select name="usuario" id="usuario" class="form-control select2" required onchange="setCamposReferente(this);" >
                                    <option value="" data-name="" data-rut="">Seleccione</option>
                                    @if ($referenteTecnico->getUser)
                                        <option value="{{ $referenteTecnico->getUser->id }}" selected data-name="{{ $referenteTecnico->getUser->name }}" data-rut="{{ $referenteTecnico->getUser->formatRut() }}">{{ $referenteTecnico->getUser->name }}&nbsp;&nbsp;&nbsp;{{ $referenteTecnico->getUser->formatRut() }}</option>
                                    @endif
                                    @foreach ( $usuarios as $usuario)
                                        <option value="{{ $usuario->id }}" data-name="{{ $usuario->name }}" data-rut="{{ $usuario->formatRut() }}" >{{ $usuario->name }}&nbsp;&nbsp;&nbsp;{{ $usuario->formatRut() }}</option>
                                    @endforeach
                                </select>
                            </div>
                        </div>

                        <div class="form-group row">
                            <label for="rut" class="col-sm-2 control-label label-form">Rut </label>
                            <div class="col-sm-4 form-validate">
                                <input type="text" class="form-control" id="rut" name="rut" value="{{ $referenteTecnico->formatRut() }}">
                            </div>

                            <label for="responsable_finanzas" class="col-sm-3 control-label label-form">Responsable Finanzas</label>
                            <div class="col-sm-3 form-validate">
                            <div class="checkbox check" >
                                <label class="label_checkbox">
                                    <input type="checkbox" class="check_input" id="responsable_finanzas" name="responsable_finanzas" value="1" @if($referenteTecnico->origen == 1) checked @endif >
                                </label>
                                </div>
                            </div>
                        </div>

                        <div class="form-group row">
                            <label for="cargo" class="col-sm-2 control-label label-form">Cargo <span class="span-label">*</span></label>
                            <div class="col-sm-8 form-validate">
                                <input type="text" class="form-control" id="cargo" name="cargo" required value="{{ $referenteTecnico->nombre }}">
                            </div>
                        </div>

                        <div class="form-group row">
                            <label for="responsable" class="col-sm-2 control-label label-form">Responsable <span class="span-label">*</span></label>
                            <div class="col-sm-8 form-validate">
                                <input type="text" class="form-control" id="responsable" name="responsable" required value="{{ $referenteTecnico->responsable }}">
                            </div>
                        </div>

                        
                        <div class="form-group row">
                            <label for="archivo" class="col-sm-2 control-label label-form">Imagen Firma {{--@if($referenteTecnico->url_firma == null)<span class="span-label">*</span> @endif--}}</label>
                            @if ($referenteTecnico->url_firma != null)
                                <div class="col-sm-8">
                                    <img src="{{ asset($referenteTecnico->url_firma) }}" heigth="220" width="180" >
                                </div>
                            @endif
                        </div>
                        

                        <div class="form-group row">
                            <div class="input-group col-sm-10 col-sm-offset-1">
                                <span class="input-group-addon" id="inputGroupFileAddon01"><strong>Seleccione Imagen (.jpeg - .png - .jpg)</strong></span>
                                <input type="file" class="form-control" accept="image/*" name="archivo" id="archivo" aria-describedby="inputGroupFileAddon01" {{--@if($referenteTecnico->url_firma == null) required @endif--}} onchange="validarArchivo();">
                            </div>
                        </div>

                    </div>
                </div>

                <div class="modal-footer form-actions right">
                    <input type="hidden" name="_token" value="{{ csrf_token() }}">
                    <input type="hidden" name="_id" value="{{ $referenteTecnico->id }}">

                    <button type="button" title="Cancelar" class="btn btn-default" data-dismiss="modal">Cancelar</button>
                    <button type="submit" title="Editar Referente Técnico" class="btn btn-warning" id="botonGuardar"><i class="far fa-save" style="color:black;"></i> Editar Referente Técnico</button>
                </div>
            </form>

        </div>
    </div>
</div>

<script src="{{asset('assets/plugins/jquery-rut/jquery.Rut.min.js')}}" type="text/javascript"></script>
<script>
    $(document).ready(function() {
        $(".select2").select2();

        $("#rut").Rut({
  			on_error: function(){
                toastr.error("El Rut ingresado no es valido", optionsToastr);
             },
		   format_on: 'keyup'
		});

        $("#form").validate({
            highlight: function(element) {
                $(element).closest('.form-validate').removeClass('has-success');
                $(element).closest('.form-validate').addClass('has-error');
            },
            unhighlight: function(element) {
                $(element).closest('.form-validate').removeClass('has-error');
                $(element).closest('.form-validate').addClass('has-success');
            },
            errorElement: 'span',
            errorClass: 'help-block',
            errorPlacement: function (error, element) {
                console.log(element);
                if (element.parent('.input-group').length) {
                    error.insertAfter(element.parent());
                } else {
                    error.insertAfter(element);
                }
            },
            rules: {
            },
            messages: {
            },

            //para enviar el formulario por ajax
            submitHandler: function(form) {
                esperandoEdicion();

                let formData = new FormData(form);

                $.ajax({
                    url: form.action,
                    type: form.method,
                    //data: $(form).serialize(),
                    data: formData,
                    processData: false,
                    contentType: false,
                    success: function(respuesta) {
                        console.log(respuesta);
                        if ( respuesta.estado == 'error' ) {
                            toastr.error('No es posible realizar la acción'+'<br><strong>Error: '+ respuesta.mensaje +'</strong>', 'Atención', optionsToastr);
                        } else if ( respuesta.estado == 'success') {
                            
                            toastr.success(respuesta.mensaje, 'Atención', optionsToastr);
                            // Cambiar la informacion de la fila de la tabla principal
                            editarFilaTabla(respuesta);
                            $("#modalEditar").modal("hide");
                            
                        }
                        
                    }            
                }).fail( function(respuesta) {//fail ajax
                    if ( respuesta.status == 400 ) {
                        mostrarErroresValidator(respuesta);
                    } else if ( respuesta.status == 500 ) {
                        toastr.error('No es posible realizar la acción, error en el servidor', 'Atención', optionsToastr);
                    } else {
                        toastr.error('No es posible realizar la acción', 'Atención', optionsToastr);
                    }
                    
                })
                .always(function() {
                    listaEdicion();
                });//ajax
                
            }

        });

    });

    function esperandoEdicion()
    {   
        $('.page-header-fixed *').css('cursor', 'wait');
        $("#botonGuardar").attr("disabled",true);
    }

    function listaEdicion()
    {
        $('.page-header-fixed *').css('cursor', '');
        $("#botonGuardar").attr("disabled",false);
    }

    function mostrarErroresValidator(respuesta)
    {
        if ( respuesta.responseJSON ) {
            //console.log(respuesta.responseJSON);
            let htmlErrores = '';
            for (let k in respuesta.responseJSON) { 
                //console.log(k, respuesta.responseJSON[k]);
                htmlErrores += '<li>' + respuesta.responseJSON[k] + '</li>';
            }

            $('#ulErrores').html(htmlErrores);
            $('#divErrores').css('display','');
            toastr.error('No es posible realizar la acción'+'<br>'+'Errores:<br> <ul>'+htmlErrores+'</ul>', 'Atención', optionsToastr);
        }
    }

    function editarFilaTabla(respuesta)
    {

        let botones = '<td width="10%"><div class="btn-group">';
        @permission(['ver-referente-tecnico'])
        botones += '<button class="btn btn-success btn-xs" title="Ver Referente Técnico" onclick="ver('+ respuesta.id +');">';
        botones += '<i class="fas fa-eye"></i></button>';
        @endpermission
        @permission(['editar-referente-tecnico'])
        botones += '<button class="btn btn-warning btn-xs" title="Editar Referente Técnico" onclick="editar('+ respuesta.id +');">';
        botones += '<i class="fas fa-edit"></i></button>';
        @endpermission
        @permission(['eliminar-referente-tecnico'])
        botones += '<button class="btn btn-danger btn-xs" title="Eliminar Referente Técnico" onclick="eliminar('+ respuesta.id +');">';
        botones += '<i class="fas fa-trash"></i></button>';
        @endpermission
        botones += '</div></td>';

        tablaPrincipal.row('#tr_'+ respuesta.id).data([
            respuesta.id,
            respuesta.nombre,
            respuesta.rut,
            respuesta.responsable,
            botones
        ]).draw();
    }

    function setCamposReferente(usuario)
    {
        $('#rut').val($('#usuario').find(':selected').data('rut'));
        $('#responsable').val($('#usuario').find(':selected').data('name'));
    }

    function validarArchivo() {
    // console.log("validando archivo");
    let archivo = $('#archivo').val().replace(/.*(\/|\\)/, ''); //val();
    let extension = archivo.substring(archivo.lastIndexOf('.'));
    if (archivo == '') {
        return false;
    } else {
        if (extension != '.png' && extension != '.jpeg' && extension != '.jpg') {
            toastr.error('El archivo no tiene una extension correcta', 'Atención', optionsToastr);
            $('#archivo').val('');
            return false;
        } else {
            return true;
        }
    }
}
</script>