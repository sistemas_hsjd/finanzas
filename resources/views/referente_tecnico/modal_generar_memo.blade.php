<link href="{{ asset('sistema/css/modales.css') }} " rel="stylesheet" type="text/css"/>
<div class="modal fade" id="modalGenerarMemo" role="dialog" aria-labelledby="myModalLabel3" aria-hidden="true">
	<div class="modal-dialog" style="width:1000px;">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" title="Cerrar" class="close" data-dismiss="modal" aria-hidden="true"></button>
				<h4 class="modal-title" ><strong><i class="fas fa-file-signature fa-lg"></i>&nbsp;&nbsp;Generar Memo Para Visto Bueno</strong></h4>
            </div>

            <form action="{{ asset('referente_tecnico/generar_memo') }}" method="post" class="horizontal-form" id="form" autocomplete="off">
                <div class="modal-body">
                    <div class="form-body" >
                        <div class="form-group row">
                            <div class="col-xs-12">	
                                <h4 class="headers-view">
                                    Proveedor
                                </h4>
                                <hr>
                                <div class="form-group">
                                    <label class="col-sm-2 control-label font-bold">Nombre Proveedor</label>
                                    <label class="col-sm-4 control-label">{{ $documento->getProveedor->nombre }}</label>
                                    <label class="col-sm-2 control-label font-bold">Rut Proveedor</label>
                                    <label class="col-sm-4 control-label">{{ $documento->getProveedor->rut }}</label>
                                </div>
                            </div>
                        </div>

                        <div class="form-group row">
                            <div class="col-xs-12">	
                                <h4 class="headers-view">
                                    Documento
                                </h4>
                                <hr>
                                <div class="form-group">
                                    <label class="col-sm-2 control-label font-bold">Tipo Documento</label>
                                    <label class="col-sm-4 control-label ">{{ $documento->getTipoDocumento->nombre }}</label>
                                    {{-- Inicio Refacturación --}}
                                    @if ( $documento->id_relacionado != null )
                                        @if ( $documento->id_tipo_documento == 4 || $documento->id_tipo_documento == 5 || $documento->id_tipo_documento == 10 || $documento->id_tipo_documento == 11 )
                                            <label class="col-sm-2 control-label font-bold">Documento Relacionado</label>
                                            <label class="col-sm-4 control-label ">{{ $documento->id_relacionado }}</label>
                                        @elseif ( $documento->id_tipo_documento > 0 )
                                            <label class="col-sm-2 control-label font-bold">Reemplaza a</label>
                                            <label class="col-sm-4 control-label ">{{ $documento->id_relacionado }}</label>
                                        @endif
                                    @endif
                                    {{-- Fin Refacturación --}}
                                </div>
                            </div>
                        </div>

                        <div class="form-group row">
                            <div class="col-xs-12">
                                <div>
                                    <label class="col-sm-2 control-label font-bold">N° Documento</label>
                                    <label class="col-sm-4 control-label ">{{ $documento->numero_documento }}</label>
                                    <label class="col-sm-2 control-label font-bold">Modalidad de Compra</label>
                                    <label class="col-sm-4 control-label ">{{ $documento->getModalidadCompra->nombre }}</label>
                                </div>
                            </div>
                        </div>

                        <div class="form-group row">
                            <div class="col-xs-12">
                                <div>
                                    <label class="col-sm-2 control-label font-bold">Tipo de adjudicación</label>
                                    <label class="col-sm-4 control-label ">{{ $documento->getTipoAdjudicacion->nombre }}</label>
                                    
                                </div>
                            </div>
                        </div>

                        <div class="form-group row">
                            <div class="col-xs-12">
                                <div>
                                    <label class="col-sm-2 control-label font-bold">N° Documento Compra</label>
                                    <label class="col-sm-4 control-label ">{{ $documento->documento_compra }}</label>
                                    <label class="col-sm-2 control-label font-bold">Fecha de Documento</label>
                                    <label class="col-sm-4 control-label ">{{ fecha_dmY($documento->fecha_documento) }}</label>
                                </div>
                            </div>
                        </div>

                        <div class="form-group row">
                            <div class="col-xs-12">
                                <div>
                                    <label class="col-sm-2 control-label font-bold">Tipo de Informe</label>
                                    <label class="col-sm-4 control-label ">{{ $documento->getTipoInforme->nombre }}</label>
                                    <label class="col-sm-2 control-label font-bold">Responsable</label>
                                    <label class="col-sm-4 control-label ">{{ $documento->getResponsable->name }}</label>
                                </div>
                            </div>
                        </div>

                        <div class="form-group row">
                            <div class="col-xs-12">
                                <div>
                                    <label class="col-sm-2 control-label font-bold">Fecha de Recepción</label>
                                    <label class="col-sm-4 control-label ">{{ fecha_dmY($documento->fecha_recepcion) }}</label>
                                    <label class="col-sm-2 control-label font-bold">Valor Total</label>
                                    <label class="col-sm-4 control-label ">$ {{ formatoMiles($documento->total_documento) }}</label>
                                </div>
                            </div>
                        </div>

                        <div class="form-group row">
                            <div class="col-xs-12">
                                <div>
                                    @if ( $documento->getServicio != null )
                                        <label class="col-sm-2 control-label font-bold">Referente Técnico</label>
                                        <label class="col-sm-4 control-label "><strong>{{ $documento->getServicio->nombre }}</strong> {{ $documento->getServicio->responsable }}</label>
                                    @endif
                                    <label class="col-sm-2 control-label font-bold">Fecha de Ingreso</label>
                                    <label class="col-sm-4 control-label ">{{ fecha_dmY($documento->fecha_ingreso)." ".fechaHoraMinutoSegundo($documento->fecha_ingreso) }}</label>
                                </div>
                            </div>
                        </div>

                        {{-- Falta "Factoring" --}}
                        @isset ($factoring)
                            <div class="form-group row">
                                <div class="col-xs-12">
                                    <div>
                                        <label class="col-sm-2 control-label font-bold">Nombre Factoring</label>
                                        <label class="col-sm-4 control-label "></label>
                                        <label class="col-sm-2 control-label font-bold">Rut Factoring</label>
                                        <label class="col-sm-4 control-label "></label>
                                    </div>
                                </div>
                            </div>
                        @endisset

                        <div class="form-group row">
                            <div class="col-xs-12">
                                <div>
                                    <label class="col-sm-2 control-label font-bold">Observaciones Factura:</label>
                                    <label class="col-sm-10 control-label ">{{ $documento->observacion }}</label>
                                </div>
                            </div>
                        </div>

                        <div class="form-group row">
                            <div class="col-xs-12">	
                                <h4 class="headers-view">
                                    Responsables
                                </h4>
                                <hr>
                                <div class="form-group">
                                    <label class="col-sm-2 control-label font-bold">Ingresado por</label>
                                    <label class="col-sm-4 control-label">{{ $documento->getDigitador->name }}</label>
                                    @if ( $documento->getDevengador )
                                        <label class="col-sm-2 control-label font-bold">Devengado por</label>
                                        <label class="col-sm-4 control-label">{{ $documento->getDevengador->name }}</label>
                                    @endif
                                </div>
                            </div>
                        </div>

                        <div class="form-group row">
                            <div class="col-xs-12">	
                                <h4 class="headers-view">
                                    Documentos adjuntos
                                </h4>
                                <hr>
                                <div class="form-group">
                                    <div class="col-xs-12 table-responsive">
                                        <table class="table table-striped table-bordered table-hover " id="tabla_info_archivo">
                                            <thead>
                                                <tr>
                                                    <th width="40%">Nombre del Archivo</th>
                                                    <th width="30%">Tipo</th>
                                                    <th width="10%">Extensión</th>
                                                    <th width="10%">Peso</th>
                                                    <th width="10%"><i class="fa fa-cog" ></i></th>
                                                </tr>
                                            </thead>
                                            <tbody>
                                                @forelse($documento->getArchivos as $archivo)
                                                    <tr>
                                                        <td>{{ $archivo->nombre }}</td>
                                                        <td>@if( $archivo->getTipoArchivo ){{ $archivo->getTipoArchivo->nombre }}@endif</td>
                                                        <td>{{ $archivo->extension }}</td>
                                                        <td>{{ pesoArchivoEnMB($archivo->peso) }}</td>
                                                        <td>
                                                            <a class="btn btn-success btn-xs" @if($archivo->cargado == 0) href="{{ asset( $archivo->ubicacion ) }}" @else href="http://{{ $archivo->ubicacion }}" @endif
                                                            title="Ver Archivo" target="_blank" ><i class="fa fa-eye"></i></a>
                                                        </td>
                                                    </tr>
                                                @empty

                                                @endforelse
                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="form-group row">
                            <div class="col-xs-12">	
                                <h4 class="headers-view">
                                    Ítem Presupuestario
                                </h4>
                                <hr>
                                <div class="form-group">
                                    <div class="col-xs-12 table-responsive">
                                        <table class="table table-striped table-bordered table-hover " id="tabla_item_presupuestario">
                                            <thead>
                                                <tr>
                                                    <th width="30%">Código</th>
                                                    <th width="50%">Descripción</th>
                                                    <th width="20%">Monto</th>
                                                </tr>
                                            </thead>
                                            <tbody>
                                                @forelse($documento->getDevengos as $devengo)
                                                    <tr>
                                                        <td>{{ $devengo->getItemPresupuestario->codigo() }}</td>
                                                        <td>{{ $devengo->getItemPresupuestario->clasificador_presupuestario }}</td>
                                                        <td class="text-right">$ {{ formatoMiles($devengo->monto) }}</td>
                                                    </tr>
                                                @empty

                                                @endforelse
                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                            </div>
                        </div>

                        @if ($documento->id_devengador != null)
                            <div class="form-group row">
                                <div class="col-xs-12">	
                                    <h4 class="headers-view">
                                        Devengado
                                    </h4>
                                    <hr>
                                    <div class="form-group">
                                        <label class="col-sm-2 control-label font-bold">ID SIGFE</label>
                                        <label class="col-sm-4 control-label">{{ $documento->id_sigfe }}</label>
                                        <label class="col-sm-2 control-label font-bold">Fecha ID SIGFE</label>
                                        <label class="col-sm-4 control-label">{{ fecha_dmY($documento->fecha_sigfe) }}</label>
                                    </div>
                                </div>
                            </div>

                            <div class="form-group row">
                                <div class="col-xs-12">
                                    <div>
                                        <label class="col-sm-2 control-label font-bold">Detalles</label>
                                        <label class="col-sm-4 control-label ">{{ $documento->detalle }}</label>
                                        <label class="col-sm-2 control-label font-bold">Observaciones</label>
                                        <label class="col-sm-4 control-label ">{{ $documento->observacion }}</label>
                                    </div>
                                </div>
                            </div>
                        @endif
                        
                        <h4 class="form-section headers-view" >Visto Bueno</h4>
                        <div class="note note-danger" id="divErrores" style="display:none;">
                            <h4 class="block" style="margin-bottom:5 px;">Debe Completar los siguientes campos: </h4>
                            <ul id="ulErrores"></ul>
                        </div>

                        <div class="form-group row">
                            <label for="numero_memo" class="col-sm-2 control-label label-form">N° Memo <span class="span-label">*</span></label>
                            <div class="col-sm-4 form-validate">
                                <input type="text" class="form-control" id="numero_memo" name="numero_memo" value="{{ $lastVistoBueno }}" required readonly>
                            </div>

                            <label for="fecha_solicitud" class="col-sm-2 control-label label-form">Fecha Solicitud <span class="span-label">*</span></label>
                            <div class="col-sm-3 form-validate">
                                <div class="input-group">
                                    <input type="text" class="form-control fechas" id="fecha_solicitud" name="fecha_solicitud" required value="{{ date("d/m/Y") }}"  readonly>
                                    <span class="input-group-addon" {{-- onclick="fechaDocumento('fecha_solicitud');" --}} style="cursor:pointer;"><i class="fa fa-calendar" aria-hidden="true" style="color: black;"></i></span>
                                </div>
                            </div>
                        </div>

                        <div class="form-group row" style="display:none;">       
                            <label for="numero_informe" class="col-sm-2 control-label label-form">N° Informe <span class="span-label">*</span></label>
                            <div class="col-sm-4 form-validate" >
                                <input type="text" class="form-control" id="numero_informe" name="numero_informe" >
                            </div>
                            
                            <label for="fecha_informe" class="col-sm-2 control-label label-form">Fecha Informe <span class="span-label">*</span></label>
                            <div class="col-sm-3 form-validate">
                                <div class="input-group">
                                    <input type="text" class="form-control fechas" id="fecha_informe" name="fecha_informe" value="{{ date("d/m/Y") }}" >
                                    <span class="input-group-addon" onclick="fechaDocumento('fecha_informe');" style="cursor:pointer;"><i class="fa fa-calendar" aria-hidden="true" style="color: black;"></i></span>
                                </div>
                            </div>
                        </div>

                        <input type="hidden" name="_token" value="{{ csrf_token() }}">
                        <input type="hidden" name="_id" value="{{ $documento->id }}">
                    </div>  
                </div>{{-- /modal-body --}}

                
                <div class="modal-footer form-actions right ">
                    <button type="button" title="Cancelar" class="btn btn-default" data-dismiss="modal">Cancelar</button>
                    <button type="submit" title="Generar Memo Visto Bueno" class="btn btn-success" id="botonGuardar"><i class="far fa-save" style="color: black;"></i> Generar Memo</button>
                </div> {{-- /modal-footer --}}
            </form>
          	
		</div>
	</div>
</div>

 <script type="text/javascript">
    let tablaItems = $('#tabla_item_presupuestario').DataTable({
        "language": {
            "emptyTable": "No hay datos disponibles en la tabla",
        },
        "paging":   false,
        "ordering": false,
        "info":     false,
        "searching": false
    });
 
    let tablaArchivo = $('#tabla_info_archivo').DataTable({
         "language": {
            "emptyTable": "No hay datos disponibles en la tabla",
        },
        "paging":   false,
        "ordering": false,
        "info":     false,
        "searching": false
    });

 	$(document).ready(function(){
         
        {{-- $('#fecha_solicitud').datepicker({
            format: 'dd/mm/yyyy',
            //endDate: new Date(),
            autoclose: true,
            language: 'es'
        }); --}}

        $('#fecha_informe').datepicker({
            format: 'dd/mm/yyyy',
            //endDate: new Date(),
            autoclose: true,
            language: 'es'
        });

        $("#form").validate({
            highlight: function(element) {
                $(element).closest('.form-validate').removeClass('has-success');
                $(element).closest('.form-validate').addClass('has-error');
            },
            unhighlight: function(element) {
                $(element).closest('.form-validate').removeClass('has-error');
                $(element).closest('.form-validate').addClass('has-success');
            },
            errorElement: 'span',
            errorClass: 'help-block',
            errorPlacement: function (error, element) {
                console.log(element);
                if (element.parent('.input-group').length) {
                    error.insertAfter(element.parent());
                } else {
                    error.insertAfter(element);
                }
            },
            rules: {
            },
            messages: {
            },

            //para enviar el formulario por ajax
            submitHandler: function(form) {
                esperandoGuardar();

                let formData = new FormData(form);
                $.ajax({
                    url: form.action,
                    type: form.method,
                    //data: $(form).serialize(),
                    data: formData,
                    processData: false,
                    contentType: false,
                    success: function(respuesta) {
                        console.log(respuesta);
                        if ( respuesta.estado == 'error' ) {
                            toastr.error('No es posible realizar la acción'+'<br><strong>Error: '+ respuesta.mensaje +'</strong>', 'Atención', optionsToastr);
                        } else if ( respuesta.estado == 'success') {
                            
                            toastr.success(respuesta.mensaje, 'Atención', optionsToastr);
                            // Funcion que utiliza la respuesta para actualizar la tabla principal
                            quitarElementoTabla(respuesta);
                            $("#modalGenerarMemo").modal("hide");
                            
                        }
                        
                    }            
                }).fail( function(respuesta) {//fail ajax
                    if ( respuesta.status == 400 ) {
                        mostrarErroresValidator(respuesta);
                    } else if ( respuesta.status == 500 ) {
                        toastr.error('No es posible realizar la acción, error en el servidor', 'Atención', optionsToastr);
                    } else {
                        toastr.error('No es posible realizar la acción', 'Atención', optionsToastr);
                    }
                    
                })
                .always(function() {
                    listoGuardar();
                });//ajax
                
            }

        });
  
 	});

    function fechaDocumento(id_fecha) {
        $('#' + id_fecha).datepicker("show");
    }

    function esperandoGuardar() {
        $('.page-header-fixed *').css('cursor', 'wait');
        $("#botonGuardar").attr("disabled", true);
    }

    function listoGuardar() {
        $('.page-header-fixed *').css('cursor', '');
        $("#botonGuardar").attr("disabled", false);
    }

    function mostrarErroresValidator(respuesta) {
        if (respuesta.responseJSON) {
            //console.log(respuesta.responseJSON);
            let htmlErrores = '';
            for (let k in respuesta.responseJSON) {
                //console.log(k, respuesta.responseJSON[k]);
                htmlErrores += '<li>' + respuesta.responseJSON[k] + '</li>';
            }

            $('#ulErrores').html(htmlErrores);
            $('#divErrores').css('display', '');
            toastr.error('No es posible realizar la acción' + '<br>' + 'Errores:<br> <ul>' + htmlErrores + '</ul>', 'Atención', optionsToastr);
        }
    }

    function quitarElementoTabla(respuesta) {

        $('#tr_' + respuesta.id).fadeOut(400, function() {
            tablaPrincipal.row('#tr_' + respuesta.id).remove().draw();
        });

    }

 </script>