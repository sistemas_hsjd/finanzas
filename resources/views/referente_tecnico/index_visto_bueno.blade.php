@extends('master')

@section('title', 'Visto Bueno Referente Técnico')

@push('css-head')
<!-- BEGIN PAGE LEVEL STYLES -->
<link rel="stylesheet" type="text/css" href="{{asset('assets/plugins/select2/select2_conquer.css')}}"/>
<link rel="stylesheet" href="{{asset('assets/plugins/data-tables/DT_bootstrap.css')}}"/>
<link rel="stylesheet" type="text/css" href="{{asset('assets/plugins/bootstrap-datepicker/css/datepicker.css')}}"/>
<!-- END PAGE LEVEL STYLES -->

<style>
    .label-form {
        font-weight: bold;
        color: black;
        text-align: right;
    }

    .td-bold {
        font-weight: bold;
    }
</style>

@endpush

@section('content')

<!-- BEGIN CONTENT -->

<div class="page-content-wrapper">
	<div class="page-content-wrapper">
		<div class="page-content">

			<!-- BEGIN PAGE CONTENT-->
			<div class="row">
				<div class="col-md-12">
					<!-- BEGIN EXAMPLE TABLE PORTLET-->
					<div class="portlet">
						<div class="portlet-title">
							<div class="caption">
								<i class="fas fa-id-card-alt"></i> <i class="fas fa-tasks"></i> Generar Memo Para Visto Bueno
							</div>
						</div>
						<div class="portlet-body">

                            <form action="#" method="post" enctype="multipart/form-data" id="filtros-docs" class="form-horizontal" autocomplete="off">
                                <h4 class="form-section" style="color: #69aa46;"><strong>Filtros</strong>
                                <div class="btn-group pull-right">

                                    {{-- <button class="btn btn-success btn-xs" title="Generar Excel" formaction="{{ url('documentos/general/excel') }}">
                                        <i class="fas fa-file-excel"></i> Exportar a Excel
                                    </button> --}}

                                    <button class="btn btn-info btn-xs" title="Filtrar Listado" id="filtroListado" type="button">
                                        <i class="fas fa-search" style="color:black;" ></i> Filtrar
                                    </button>
                                </div>
                                </h4>
                                <div class="form-group row">
                                
                                    <label for="filtro_fecha_a_utilizar" class="col-sm-1 control-label label-form">Fecha </label>
                                    <div class="col-sm-3">
                                        <select class="form-control select2_filtro " id="filtro_fecha_a_utilizar" name="filtro_fecha_a_utilizar" >
                                            @foreach ( $fechasUtilizar as $fechaUtilizar )
                                                <option value="{{ $fechaUtilizar->opcion }}" >{{ $fechaUtilizar->opcion }}</option>
                                            @endforeach
                                        </select>
                                    </div>

                                    <label for="filtro_fecha_inicio" class="col-sm-1 control-label label-form">Fecha Inicio</label>
                                    <div class="col-sm-3">
                                        <div class="input-group">
                                            <input type="text" class="form-control fechas" id="filtro_fecha_inicio" name="filtro_fecha_inicio" value="{{ date("d/m/Y") }}" >
                                            <span class="input-group-addon" onclick="fechaDocumento('filtro_fecha_inicio');" style="cursor:pointer;"><i class="fa fa-calendar" aria-hidden="true" style="color: black;"></i></span>
                                        </div>
                                    </div>

                                    <label for="filtro_fecha_termino" class="col-sm-1 control-label label-form">Fecha Termino</label>
                                    <div class="col-sm-3">
                                        <div class="input-group">
                                            <input type="text" class="form-control fechas" id="filtro_fecha_termino" name="filtro_fecha_termino" value="{{ date("d/m/Y") }}" >
                                            <span class="input-group-addon" onclick="fechaDocumento('filtro_fecha_termino');" style="cursor:pointer;"><i class="fa fa-calendar" aria-hidden="true" style="color: black;"></i></span>
                                        </div>
                                    </div>

                                </div>

                                <div class="form-group row">
                                    <label for="filtro_proveedor" class="col-sm-1 control-label label-form">Proveedor</label>
                                    <div class="col-sm-3">
                                        <select name="filtro_proveedor[]" id="filtro_proveedor" class="form-control select2_filtro" multiple style="width: 100%">
                                            @foreach ( $proveedores as $proveedor )
                                                <option value="{{ $proveedor->id }}" >{{ $proveedor->rut }} {{ $proveedor->nombre }}</option>
                                            @endforeach
                                        </select>
                                    </div>

                                    <label for="filtro_tipo_documento" class="col-sm-1 control-label label-form">Tipo de Documento</label>
                                    <div class="col-sm-3">
                                        <select name="filtro_tipo_documento[]" id="filtro_tipo_documento" class="form-control select2_filtro" multiple style="width: 100%">
                                            @foreach ( $tiposDocumento as $tipoDocumento )
                                                <option value="{{ $tipoDocumento->id }}" >{{ $tipoDocumento->nombre }}</option>
                                            @endforeach
                                        </select>
                                    </div>

                                    <label for="filtro_modalidad_compra" class="col-sm-1 control-label label-form">Modalidad de Compra</label>
                                    <div class="col-sm-3">
                                        <select name="filtro_modalidad_compra[]" id="filtro_modalidad_compra" class="form-control select2_filtro" multiple="multiple">
                                            @foreach ( $modalidadesCompra as $modalidadCompra )
                                                <option value="{{ $modalidadCompra->id }}" >{{ $modalidadCompra->nombre }}</option>
                                            @endforeach
                                        </select>
                                    </div>

                                </div>

                                <div class="form-group row">

                                    <label for="filtro_tipo_adjudicacion" class="col-sm-1 control-label label-form">Tipo de Adjudicación</label>
                                    <div class="col-sm-3">
                                        <select name="filtro_tipo_adjudicacion[]" id="filtro_tipo_adjudicacion" class="form-control select2_filtro" multiple="multiple">
                                            @foreach ( $tiposAdjudicacion as $tipoAdjudicacion )
                                                <option value="{{ $tipoAdjudicacion->id }}" >{{ $tipoAdjudicacion->nombre }}</option>
                                            @endforeach
                                        </select>
                                    </div>

                                    <label for="filtro_tipo_informe" class="col-sm-1 control-label label-form">Tipo de Informe</label>
                                    <div class="col-sm-3">
                                        <select name="filtro_tipo_informe[]" id="filtro_tipo_informe" class="form-control select2_filtro" multiple="multiple">
                                            @foreach ( $tiposInforme as $tipoInforme )
                                                <option value="{{ $tipoInforme->id }}" >{{ $tipoInforme->nombre }}</option>
                                            @endforeach
                                        </select>
                                    </div>

                                    <label for="filtro_responsable" class="col-sm-1 control-label label-form">Responsable</label>
                                    <div class="col-sm-3">
                                        <select name="filtro_responsable[]" id="filtro_responsable" class="form-control select2_filtro" multiple="multiple">
                                            @foreach ( $usuariosResponsables as $responsable )
                                                <option value="{{ $responsable->id }}" >{{ $responsable->name }}</option>
                                            @endforeach
                                        </select>
                                    </div>

                                </div>

                                <div class="form-group row">

                                    <label for="filtro_referente_tecnico" class="col-sm-1 control-label label-form">Referente Técnico</label>
                                    <div class="col-sm-3">
                                        <select name="filtro_referente_tecnico[]" id="filtro_referente_tecnico" class="form-control select2_filtro" multiple="multiple">
                                            @foreach ( $referentesTecnicos as $referenteTecnico )
                                                <option value="{{ $referenteTecnico->id }}" >{{ $referenteTecnico->nombre }} - {{ $referenteTecnico->responsable }}</option>
                                            @endforeach
                                        </select>
                                    </div>

                                    <label for="filtro_digitador" class="col-sm-1 control-label label-form">Digitador</label>
                                    <div class="col-sm-3">
                                        <select name="filtro_digitador[]" id="filtro_digitador" class="form-control select2_filtro" multiple="multiple">
                                            @foreach ( $usuariosDigitadores as $digitador )
                                                <option value="{{ $digitador->id }}" >{{ $digitador->name }}</option>
                                            @endforeach
                                        </select>
                                    </div>

                                    <label for="filtro_item_presupuestario" class="col-sm-1 control-label label-form">Ítem </label>
                                    <div class="col-sm-3">
                                        <select class="form-control select2_filtro " id="filtro_item_presupuestario" name="filtro_item_presupuestario[]" multiple="multiple" >
                                            @foreach ( $itemsPresupuestarios as $item )
                                                <option value="{{ $item->id }}" >{{ $item->codigo() }} {{ $item->clasificador_presupuestario }}</option>
                                            @endforeach
                                        </select>
                                    </div>

                                </div>

                                <div class="form-group row">

                                    <label for="filtro_numero_documento" class="col-sm-1 control-label label-form">N° Documento</label>
                                    <div class="col-sm-3">
                                        <input type="text" class="form-control " id="filtro_numero_documento" name="filtro_numero_documento" >
                                    </div>

                                    <label for="filtro_numero_documento_compra" class="col-sm-1 control-label label-form">N° Documento Compra</label>
                                    <div class="col-sm-3">
                                        <input type="text" class="form-control " id="filtro_numero_documento_compra" name="filtro_numero_documento_compra" >
                                    </div>

                                </div>
                                
                            </form>
                            @permission(['solicitar-visto-bueno'])
                                <div class="table-toolbar">
                                    <div class="btn-group">
                                        <button title="Generar Memo Masivo" class="btn btn-info" onclick="memoMasivo();">
                                            <i class="far fa-file-alt"></i> Memo Masivo
                                        </button>
                                    </div>
                                </div>
                            @endpermission
                            <div class="row">
                                <div class="col-md-12">
                                    <div class="panel">
                                        <div class="panel-body">
                                            <div class="table-responsive">
                                                <table class="table table-striped table-bordered table-hover table-full-width" id="tabla_documentos">
                                                    <thead>
                                                        <tr>
                                                            <th width="2%" >
                                                                <div class="checkbox">
                                                                    <input class="select_all" type="checkbox" id="todos">
                                                                </div> 
                                                            </th>
                                                            <th >Proveedor</th>
                                                            <th >Responsable</th>
                                                            <th >Digitador</th>
                                                            <th>Tipo Doc.</th>
                                                            <th >N° Documento</th>
                                                            <th >N° Documento Compra</th>
                                                            <th >Fecha</th>
                                                            <th >Tipo Informe</th>
                                                            <th>Tot. Orig.</th>
                                                            <th>Tot. Act.</th>
                                                            <th >Referente Técnico</th>
                                                            <th class="hidden-xs text-center" width="8%">
                                                                <i class="fa fa-cog"></i>
                                                            </th>
                                                        </tr>
                                                    </thead>
                                                    {{-- <tbody>
                                                    @forelse($documentos as $documento)
                                                        <tr id="tr_{{ $documento->id }}">
                                                            <td>
                                                                <input class="select_item" name="id[]" id="id[]" value="{{ $documento->id }}" type="checkbox" 
                                                                @if($documento->id_referente_tecnico != null) data-referente="{{ $documento->id_referente_tecnico }}" @else data-referente="0" @endif  />
                                                            </td>
                                                            <td>{{ $documento->getProveedor->formatRut().' '.$documento->getProveedor->nombre }}</td>
                                                            <td>{{ $documento->getResponsable->name }}</td>
                                                            <td>{{ $documento->getDigitador->name }}</td>
                                                            <td>{{ $documento->numero_documento }}</td>
                                                            <td>{{ $documento->documento_compra }}</td>
                                                            <td class="text-center">{{ fecha_dmY($documento->fecha_recepcion) }}</td>
                                                            <td class="text-center">{{ $documento->getTipoInforme->nombre }}</td>
                                                            <td class="text-right">$ {{ formatoMiles($documento->total_documento) }}</td>
                                                            <td class="text-right">$ {{ formatoMiles($documento->total_documento_actualizado) }}</td>
                                                            <td class="text-center">
                                                                @if ($documento->getReferenteTecnico)
                                                                    {{ $documento->getReferenteTecnico->nombre.' '.$documento->getReferenteTecnico->responsable }}
                                                                @endif
                                                            </td>
                                                            <td>
                                                                <div class="btn-group">
                                                                    <button class="btn btn-success btn-xs " title="Ver Documento" onclick="verDocumento({{ $documento->id }});">
                                                                        <i class="fa fa-eye fa-lg"></i>
                                                                    </button>

                                                                    <button class="btn btn-info btn-xs " title="Generar Memo" onclick="generarMemo({{ $documento->id }});">
                                                                        <i class="fas fa-file-signature fa-lg"></i>
                                                                    </button>
                                                                </div>
                                                            </td>
                                                        </tr>
                                                    @empty

                                                    @endforelse
                                                    </tbody> --}}
                                                </table>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
						</div>
					</div>
					<!-- END EXAMPLE TABLE PORTLET-->
				</div>
			</div>
			<!-- END PAGE CONTENT-->
			
			
			
			
		</div> <!--/div class="page-content" -->
	</div> <!--/div class="page-content-wrapper" -->
</div><!--/div class="page-content-wrapper" -->
<!-- END CONTENT -->
<div id="modal"></div>
<div id="modal2"></div>
@endsection

@push('script-footer')
<!-- BEGIN PAGE LEVEL PLUGINS -->
<script type="text/javascript" src="{{ asset('assets/plugins/select2/select2.min.js') }} "></script>
<script type="text/javascript" src="{{ asset('datatables/jquery.dataTables.min.js') }}"></script>
<script type="text/javascript" src="{{ asset('assets/plugins/data-tables/DT_bootstrap.js') }}"></script>
<script type="text/javascript" src="{{asset('assets/plugins/bootstrap-datepicker/js/bootstrap-datepicker.js')}}"></script>
<script type="text/javascript" src="{{asset('assets/plugins/bootstrap-datetimepicker/js/bootstrap-datetimepicker.js')}}"></script>
<!-- END PAGE LEVEL PLUGINS -->

<!-- BEGIN PAGE LEVEL SCRIPTS -->
<script src="{{ asset('assets/scripts/app.js') }} "></script>

<script type="text/javascript">
    var tablaPrincipal = $('#tabla_documentos').DataTable({
        "processing": true,
        "serverSide": true,
        "ajax": {
            "url" : "{{ url('referente_tecnico/data_table_index_solicitud_visto_bueno') }}",
            "type" : "POST",
            "data": function(d){
                d.form = JSON.stringify( $("#filtros-docs").serializeArray() );
                // console.log('se envia:');
                // console.log(d);
            },
        },
        // Set rows IDs
        rowId: function(data) {
            return 'tr_' + data.DT_RowID;
        },
        "deferRender": true,
        "language": {
            "emptyTable":     "No hay datos disponibles en la tabla",
            "info":           "Mostrando registros del _START_ al _END_ de un total de _TOTAL_ registros",
            "infoEmpty":      "No hay datos disponibles para mostrar en la tabla",
            "infoFiltered":   "(filtrado de un total de _MAX_ registros)",
            "infoPostFix":    "",
            "thousands":      ",",
            "lengthMenu":     "Mostrar _MENU_ registros",
            "loadingRecords": "Cargando...",
            "processing":     "Procesando...",
            "search":         "Buscar: ",
            "zeroRecords":    "<i class='far fa-frown fa-lg'></i> No hay datos que coincidan con la búsqueda <i class='far fa-frown fa-lg'></i>",
            "paginate": {
                "first":      "Primero",
                "last":       "Último",
                "next":       "Siguiente",
                "previous":   "Anterior"
            },
            "aria": {
                "sortAscending":  ": Activar para ordenar la columna de manera ascendente",
                "sortDescending": ": Activar para ordenar la columna de manera descendente"
            }
        },
        "lengthMenu": [
            [10, 15, 20, -1],
            [10, 15, 20, "Todos"] // change per page values here
        ],
        // set the initial value
        "displayLength": 10,
        "paging":   true,
        "ordering": true,
        "info":     true,
        "searching": true,
        "order": [],
        "columnDefs": [
            { "orderable": false, "targets": 0 }
        ]
    });

    $.fn.dataTable.ext.errMode = 'none';//quita los mensajes de error de la datatable ajax

    $('#tabla_documentos').on( 'error.dt', function ( e, settings, techNote, message ) {
		console.log( 'An error has been reported by DataTables: ', message );//para mostrar los mensajes de error en la consola del navegador
	} );

    jQuery('#tabla_documentos_filter input').addClass("form-control input-large"); // modify table search input
    jQuery('#tabla_documentos_wrapper .dataTables_length select').addClass("form-control input-small"); // modify table per page dropdown
    jQuery('#tabla_documentos_wrapper .dataTables_length select').select2(); // initialize select2 dropdown

    $('#tabla_documentos_column_toggler input[type="checkbox"]').change(function() {
        /* Get the DataTables object again - this is not a recreation, just a get of the object */
        var iCol = parseInt($(this).attr("data-column"));
        var bVis = oTable.fnSettings().aoColumns[iCol].bVisible;
        oTable.fnSetColumnVis(iCol, (bVis ? false : true));
    });

	$(document).ready(function() {
		App.init(); // initlayout and core plugins

   		$("#referente-tecnico-opciones").addClass( "active" );
		$("#solicitar-visto-bueno-li").addClass( "active" );
		$("#referente-tecnico-opciones-a").append( '<span class="selected"></span>' );

        $('#todos').on('change', function() {
             if ($(this).is(':checked') ) {
                $( ".select_item" ).prop( "checked", true );
             } else {
                $( ".select_item" ).prop( "checked", false );
            }
        });

        $('#filtroListado').click(function(){
            $("#modalCarga").modal({backdrop: 'static', keyboard: false});
            $('#modalCargaImg').addClass('fa-pulso');
                
            $('#tabla_documentos').DataTable().ajax.reload( function ( json ) {
                $("#modalCarga").modal('toggle');
            });
        });

        $('#filtro_fecha_inicio').datepicker({
            format: 'dd/mm/yyyy',
            endDate: new Date(),
            autoclose: true,
            language: 'es'
        });

        $('#filtro_fecha_termino').datepicker({
            format: 'dd/mm/yyyy',          
            autoclose: true,
            language: 'es'
        });

        $(".select2_filtro").select2({
            // allowClear: true,
        });

	});

    function fechaDocumento(id_fecha) {
        $('#' + id_fecha).datepicker("show");
    }

    function verDocumento(id)
    {
        $.get( '{{ url("documentos/modal/ver") }}/' + id, function( data ) {
            $( "#modal" ).html( data );
            $( "#modalVer" ).modal();
        });
    }

    function generarMemo(id)
    {
        $.get( '{{ url("referente_tecnico/modal/generar_memo") }}/' + id, function( data ) {
            $( "#modal" ).html( data );
            $( "#modalGenerarMemo" ).modal();
        });
    }

    function memoMasivo()
    {
        var arregloDocs = [];
        var idReferenteAux = 0;
        var todoBien = true;

        $(".select_item:checked").each(function(){

            if (parseInt(idReferenteAux) == 0) {
                idReferenteAux = parseInt(this.dataset.referente);
                arregloDocs.push(this.value); // se añade el id del documento al arreglo
            } else {
                if ( parseInt(this.dataset.referente) != parseInt(idReferenteAux) ) {
                    toastr.error('Todos los documentos deben tener el mismo Referente Técnico', 'Atención', optionsToastr);
                    todoBien = false;
                    return false;
                } else {
                    arregloDocs.push(this.value);
                }
            }
        });

        if ( arregloDocs.length != 0 && todoBien) {
            let arregloDocsJSON = JSON.stringify(arregloDocs);
            console.log('son todos los mismos referentes, abrir modal');

            $.get( '{{ url("referente_tecnico/modal/generar_memo_masivo") }}/' + arregloDocsJSON, function( data ) {
                $( "#modal" ).html( data );
                $( "#modalGenerarMemoMasivo" ).modal();
            });
        }
        
    }
</script>

@endpush