<link href="{{ asset('sistema/css/modales.css') }} " rel="stylesheet" type="text/css"/>
<style>
#tabla_documentos thead tr th {
    font-size: 11px;
    font-weight: 600;
}
</style>
<div class="modal fade" id="modalEliminarSolicitudVistoBueno" role="dialog" aria-labelledby="myModalLabel3" aria-hidden="true">
	<div class="modal-dialog" style="width:1000px;">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" title="Cerrar" class="close" data-dismiss="modal" aria-hidden="true"></button>
				<h4 class="modal-title" ><strong><i class="fa fa-trash fa-lg"></i>&nbsp;&nbsp;Eliminar Memo Solicitud Visto Bueno</strong></h4>
            </div>

            <form action="{{ asset('referente_tecnico/eliminar_solicitud_visto_bueno') }}" method="post" class="horizontal-form" id="form" autocomplete="off">
                <div class="modal-body">

                    <div class="form-body" >
                        <div class="note note-danger" id="divErrores" style="display:none;">
                            <h4 class="block" style="margin-bottom:5px;">Debe Completar los siguientes campos: </h4>
                            <ul id="ulErrores"></ul>
                        </div>

                        <h4 class="form-section" style="color: #69aa46;margin-top: 0px;">Documentos</h4>
                        <div class="form-group row">
                            <div class="col-xs-12 table-responsive">
                                <table class="table table-striped table-bordered table-hover " id="tabla_documentos">
                                    <thead>
                                        <tr>
                                            <th width="10%">Rut Proveedor</th>
                                            <th width="30%">Nombre Proveedor</th>
                                            <th width="10%">N° Documento</th>
                                            <th width="10%">N° Documento Compra</th>
                                            <th width="10%">Fecha</th>
                                            <th width="10%">Licitación</th>
                                            <th width="20%">Total</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                    @forelse($vistoBueno->getDocumentos as $documento)
                                        <tr>
                                            <td>{{ $documento->getProveedor->formatRut() }}</td>
                                            <td>{{ $documento->getProveedor->nombre }}</td>
                                            <td>{{ $documento->numero_documento }}</td>
                                            <td>{{ $documento->documento_compra }}</td>
                                            <td>{{ fecha_dmY($documento->fecha_documento) }}</td>
                                            <td>{{ $documento->licitacion }}</td>
                                            <td class="text-right">$ {{ formatoMiles($documento->total_documento) }}</td>
                                        </tr>
                                    @empty

                                    @endforelse
                                    </tbody>
                                </table>
                            </div>
                        </div>

                       
                        
                        <h4 class="form-section" style="color: #69aa46;">Visto Bueno</h4>

                        <div class="form-group row">
                            <label for="numero_memo" class="col-sm-2 control-label label-form">N° Memo <span class="span-label">*</span></label>
                            <div class="col-sm-4 form-validate">
                                <input type="text" class="form-control" id="numero_memo" name="numero_memo" required  value="{{ $vistoBueno->memo }}" readonly>
                            </div>

                            <label for="fecha_solicitud" class="col-sm-2 control-label label-form">Fecha Solicitud <span class="span-label">*</span></label>
                            <div class="col-sm-3 form-validate">
                                <div class="input-group">
                                    <input type="text" class="form-control fechas" id="fecha_solicitud" name="fecha_solicitud" required value="{{ fecha_dmY($vistoBueno->fecha_solicitud) }}" readonly>
                                    <span class="input-group-addon" {{--onclick="fechaDocumento('fecha_solicitud');"--}} style="cursor:pointer;"><i class="fa fa-calendar" aria-hidden="true" style="color: black;"></i></span>
                                </div>
                            </div>
                        </div>

                        <div class="form-group row" style="display:none;">       
                            <label for="numero_informe" class="col-sm-2 control-label label-form">N° Informe <span class="span-label">*</span></label>
                            <div class="col-sm-4 form-validate" >
                                <input type="text" class="form-control" id="numero_informe" name="numero_informe" value="{{ $vistoBueno->informe }}">
                            </div>
                            
                            <label for="fecha_informe" class="col-sm-2 control-label label-form">Fecha Informe <span class="span-label">*</span></label>
                            <div class="col-sm-3 form-validate">
                                <div class="input-group">
                                    <input type="text" class="form-control fechas" id="fecha_informe" name="fecha_informe" value="{{ fecha_dmY($vistoBueno->fecha_informe) }}" >
                                    <span class="input-group-addon" onclick="fechaDocumento('fecha_informe');" style="cursor:pointer;"><i class="fa fa-calendar" aria-hidden="true" style="color: black;"></i></span>
                                </div>
                            </div>
                        </div>
                        

                        <input type="hidden" name="_token" value="{{ csrf_token() }}">
                        <input type="hidden" name="_id" value="{{ $vistoBueno->id }}">
                    </div>  
                </div>{{-- /modal-body --}}

                
                <div class="modal-footer form-actions right ">
                    <button type="button" title="Cancelar" class="btn btn-default" data-dismiss="modal">Cancelar</button>
                    <button type="submit" title="Eliminar Memo Solicitud Visto Bueno" class="btn btn-danger" id="botonEliminar"><i class="fa fa-trash" style="color: black;"></i> Eliminar Memo</button>
                </div> {{-- /modal-footer --}}
            </form>
          	
		</div>
	</div>
</div>

 <script type="text/javascript">


 	$(document).ready(function(){

        $("#form").validate({
            highlight: function(element) {
                $(element).closest('.form-validate').removeClass('has-success');
                $(element).closest('.form-validate').addClass('has-error');
            },
            unhighlight: function(element) {
                $(element).closest('.form-validate').removeClass('has-error');
                $(element).closest('.form-validate').addClass('has-success');
            },
            errorElement: 'span',
            errorClass: 'help-block',
            errorPlacement: function (error, element) {
                console.log(element);
                if (element.parent('.input-group').length) {
                    error.insertAfter(element.parent());
                } else {
                    error.insertAfter(element);
                }
            },
            rules: {
            },
            messages: {
            },

            //para enviar el formulario por ajax
            submitHandler: function(form) {
                esperandoGuardar();

                let formData = new FormData(form);
                $.ajax({
                    url: form.action,
                    type: form.method,
                    //data: $(form).serialize(),
                    data: formData,
                    processData: false,
                    contentType: false,
                    success: function(respuesta) {
                        console.log(respuesta);
                        if ( respuesta.estadoMemo == 'error' ) {
                            toastr.error('No es posible realizar la acción'+'<br><strong>Error: '+ respuesta.mensajeMemo +'</strong>', 'Atención', optionsToastr);
                        } else if ( respuesta.estadoMemo == 'success') {
                            
                            toastr.success(respuesta.mensajeMemo, 'Atención', optionsToastr);
                            // Funcion que utiliza la respuesta para actualizar la tabla principal
                            actualizaElementoTabla(respuesta);

                            $("#modalEliminarSolicitudVistoBueno").modal("hide");
                            
                        }
                        
                    }            
                }).fail( function(respuesta) {//fail ajax
                    if ( respuesta.status == 400 ) {
                        mostrarErroresValidator(respuesta);
                    } else if ( respuesta.status == 500 ) {
                        toastr.error('No es posible realizar la acción, error en el servidor', 'Atención', optionsToastr);
                    } else {
                        toastr.error('No es posible realizar la acción', 'Atención', optionsToastr);
                    }
                    
                })
                .always(function() {
                    listoGuardar();
                });//ajax
                
            }

        });
  
 	});

    function fechaDocumento(id_fecha) {
        $('#' + id_fecha).datepicker("show");
    }

    function esperandoGuardar() {
        $('.page-header-fixed *').css('cursor', 'wait');
        $("#botonEliminar").attr("disabled", true);
    }

    function listoGuardar() {
        $('.page-header-fixed *').css('cursor', '');
        $("#botonEliminar").attr("disabled", false);
    }

    function mostrarErroresValidator(respuesta) {
        if (respuesta.responseJSON) {
            //console.log(respuesta.responseJSON);
            let htmlErrores = '';
            for (let k in respuesta.responseJSON) {
                //console.log(k, respuesta.responseJSON[k]);
                htmlErrores += '<li>' + respuesta.responseJSON[k] + '</li>';
            }

            $('#ulErrores').html(htmlErrores);
            $('#divErrores').css('display', '');
            toastr.error('No es posible realizar la acción' + '<br>' + 'Errores:<br> <ul>' + htmlErrores + '</ul>', 'Atención', optionsToastr);
        }
    }

    function actualizaElementoTabla(respuesta) {

        $('#tr_' + respuesta.id).fadeOut(400, function() {
            tablaPrincipal.row('#tr_' + respuesta.id).remove().draw();
        });

    }
    
 </script>