<link href="{{ asset('sistema/css/modales.css') }} " rel="stylesheet" type="text/css"/>
<style>
#tabla_documentos thead tr th {
    font-size: 12px;
    font-weight: 600;
}

.custom-checkbox {}

/* oculto el input */
.custom-checkbox input[type=checkbox] {
  display: none;
}

/* oculto el texto */
.custom-checkbox span {
  display: none;
}

/* si está activo el input */
.custom-checkbox input[type=checkbox]:checked + span {
  display: inline-block;
}

/* si está inactivo el input */
.custom-checkbox input[type=checkbox]:not(:checked) + span + span {
  display: inline-block;
}

</style>
<div class="modal fade" id="modalRegistrarMemoVistoBueno" role="dialog" aria-labelledby="myModalLabel3" aria-hidden="true">
	<div class="modal-dialog" style="width:90%;">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" title="Cerrar" class="close" data-dismiss="modal" aria-hidden="true"></button>
				<h4 class="modal-title" ><strong><i class="fas fa-file-signature fa-lg"></i> <i class="fas fa-tasks"></i>&nbsp;&nbsp;Registrar Visto Bueno</strong></h4>
            </div>

            <form action="{{ asset('referente_tecnico/registrar_memo_visto_bueno') }}" method="post" class="horizontal-form" id="form" autocomplete="off">
                <div class="modal-body">

                    <div class="form-body" >
                        <div class="note note-danger" id="divErrores" style="display:none;">
                            <h4 class="block" style="margin-bottom:5px;">Debe Completar los siguientes campos: </h4>
                            <ul id="ulErrores"></ul>
                        </div>

                        <h4 class="form-section" style="color: #69aa46;margin-top: 0px;">Documentos</h4>
                        <div class="form-group row">
                            <div class="col-xs-12 table-responsive">
                                <table class="table table-striped table-bordered table-hover " id="tabla_documentos">
                                    <thead>
                                        <tr>
                                            <th width="9%">Rut Proveedor</th>
                                            <th width="25%">Nombre Proveedor</th>
                                            <th width="7%" class="text-center">N° Documento</th>
                                            <th width="10%">N° Documento Compra</th>
                                            <th width="7%" class="text-center">Fecha</th>
                                            <th width="9%">Licitación</th>
                                            <th width="9%">Total</th>
                                            <th width="2%">Doc.</th>
                                            <th width="2" class="text-center">Visto Bueno</th>
                                            <th width="20%" class="text-center">Nota</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                    @forelse($vistoBueno->getDocumentos as $documento)
                                        <tr>
                                            <td>{{ $documento->getProveedor->formatRut() }}</td>
                                            <td>{{ $documento->getProveedor->nombre }}</td>
                                            <td class="text-center">{{ $documento->numero_documento }}</td>
                                            <td>{{ $documento->documento_compra }}</td>
                                            <td class="text-center">{{ fecha_dmY($documento->fecha_documento) }}</td>
                                            <td>{{ $documento->licitacion }}</td>
                                            <td class="text-right">$ {{ formatoMiles($documento->total_documento) }}</td>
                                            <td class="text-center">
                                                @forelse ($documento->getArchivos as $archivo)
                                                    @if ($archivo->id_tipo_archivo == 1)
                                                        <a class="btn btn-info btn-xs" 
                                                        @if($archivo->cargado == 0) href="{{ asset( $archivo->ubicacion ) }}" @else href="http://{{ $archivo->ubicacion }}" @endif 
                                                        title="Ver Archivo" target="_blank" ><i class="fa fa-eye"></i></a>
                                                    @endif
                                                @empty

                                                @endforelse
                                            </td>

                                            <td class="text-center">
                                                <div class="custom-checkbox">
                                                    <label >
                                                        <input class="vistos_buenos" name="visto_bueno[]" id="visto_bueno[]" value="{{ $documento->id }}" type="checkbox" checked />
                                                        <span class="active btn btn-success" title="Confirmado"><i class="far fa-thumbs-up"></i></span >
                                                        <span class="not-active btn btn-danger" title="Rechazado" ><i class="far fa-thumbs-down"></i></span >
                                                    </label>
                                                </div>
                                            </td>

                                            <td>
                                                <textarea class="form-control noresize-small" id="nota_documento_{{ $documento->id }}" 
                                                        name="nota_documento[{{ $documento->id }}]" maxlength="255"></textarea>
                                            </td>
                                        </tr>
                                    @empty

                                    @endforelse
                                    </tbody>
                                </table>
                            </div>
                        </div>
                        
                        <h4 class="form-section" style="color: #69aa46;">Visto Bueno</h4>

                        <div class="form-group row">
                            <label for="fecha_respuesta" class="col-sm-2 control-label label-form">Fecha Respuesta <span class="span-label">*</span></label>
                            <div class="col-sm-3 form-validate">
                                <div class="input-group">
                                    <input type="text" class="form-control fechas" id="fecha_respuesta" name="fecha_respuesta" required value="{{ date('d/m/Y') }}" readonly >
                                    <span class="input-group-addon" {{-- onclick="fechaDocumento('fecha_respuesta');" --}} style="cursor:pointer;"><i class="fa fa-calendar" aria-hidden="true" style="color: black;"></i></span>
                                </div>
                            </div>

                            <label for="numero_memo_respuesta" class="col-sm-3 control-label label-form">N° Memo Respuesta <span class="span-label">*</span></label>
                            <div class="col-sm-4 form-validate">
                                <input type="text" class="form-control" id="numero_memo_respuesta" name="numero_memo_respuesta" required >
                            </div>
                        </div>

                        <div class="form-group row">
                            <label for="numero_memo_respuesta" class="col-sm-2 control-label label-form">Observación</label>
                            <div class="col-sm-10">
                                <textarea class="form-control noresize" id="observacion" name="observacion"></textarea>
                            </div>
                        </div>
                        
                        <h4 class="form-section" style="color: #69aa46;">Archivos</h4>
                        <div class="form-group row">
                            <div class="col-xs-12 table-responsive">
                                <table class="table table-striped table-bordered table-hover " id="tabla_info_archivos_existentes">
                                    <thead>
                                        <tr>
                                            <th width="50%">Nombre del Archivo</th>
                                            <th width="20%">Tipo</th>
                                            <th width="10%">Extensión</th>
                                            <th width="10%">Peso</th>
                                            <th width="10%"><i class="fa fa-cog"></i></th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                    @forelse ($vistoBueno->getArchivos as $archivo)
                                        <tr>
                                            <td>{{ $archivo->nombre }}</td>
                                            <td>@if( $archivo->getTipoArchivo ){{ $archivo->getTipoArchivo->nombre }}@endif</td>
                                            <td>{{ $archivo->extension }}</td>
                                            <td>{{ pesoArchivoEnMB($archivo->peso) }}</td>
                                            <td>
                                                <a class="btn btn-success btn-xs" @if($archivo->cargado == 0) href="{{ asset( $archivo->ubicacion ) }}" @else href="http://{{ $archivo->ubicacion }}" @endif
                                                title="Ver Archivo" target="_blank" ><i class="fa fa-eye"></i></a>
                                                {{-- 
                                                <label>
                                                    <input type="checkbox" name="delete_list[]" value="{{$archivo->id}}"> Eliminar
                                                </label>
                                                --}}
                                            </td>
                                        </tr>
                                    @empty

                                    @endforelse
                                    </tbody>
                                </table>
                            </div>
                        </div>
                        <div class="form-group">
                            <span style="font-size: 14px;margin-left: 10px;font-weight:bold;" class="label label-success">Considerar que el Memo de Visto Bueno será generado por el sistema.</span>
                        </div>
                        <div class="form-group row">
                            <div class="input-group col-sm-10 col-sm-offset-1">
                                <span class="input-group-addon" id="inputGroupFileAddon01"><strong>Seleccione Documento</strong></span>
                                <input type="file" class="form-control" accept=".pdf" name="archivo" id="archivo" aria-describedby="inputGroupFileAddon01" onchange="validarArchivo();">
                            </div>
                        </div>

                        <div class="form-group row">
                            <div id="div_archivo" class="col-xs-12 table-responsive" style="display: none;">
                                <table class="table table-striped table-bordered table-hover " id="tabla_info_archivo">
                                    <thead>
                                        <tr>
                                            <th width="40%">Nombre</th>
                                            <th width="20%">Peso</th>
                                            <th width="40%">Tipo</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                    </tbody>
                                </table>
                            </div>
                        </div>

                        <input type="hidden" name="_token" value="{{ csrf_token() }}">
                        <input type="hidden" name="_id" value="{{ $vistoBueno->id }}">
                    </div>  
                </div>{{-- /modal-body --}}

                
                <div class="modal-footer form-actions right ">
                    <button type="button" title="Cancelar" class="btn btn-default" data-dismiss="modal">Cancelar</button>
                    <button type="submit" title="Registrar Visto Bueno" class="btn btn-success" id="botonGuardar"><i class="far fa-save" style="color: black;"></i> Registrar Visto Bueno</button>
                </div> {{-- /modal-footer --}}
            </form>
          	
		</div>
	</div>
</div>

 <script type="text/javascript">
    var selectTipoArchivo = '<select name="tipo_archivo" id="tipo_archivo" class="col-xs-12" required>';
        selectTipoArchivo += '<option value="">Seleccione</option>';
        @foreach ( $tiposArchivo as $tipoArchivo)
            selectTipoArchivo += '<option value="{{ $tipoArchivo->id }}">{{ $tipoArchivo->nombre }}</option>';
        @endforeach
        selectTipoArchivo += '</select>';
 
    var tablaArchivo = $('#tabla_info_archivo').DataTable({
         "language": {
            "emptyTable": "No hay datos disponibles en la tabla",
        },
        "paging":   false,
        "ordering": false,
        "info":     false,
        "searching": false
    });

 	$(document).ready(function(){
         
        {{-- $('#fecha_respuesta').datepicker({
            format: 'dd/mm/yyyy',
            //endDate: new Date(),
            autoclose: true,
            language: 'es'
        }); --}}

        $("#form").validate({
            highlight: function(element) {
                $(element).closest('.form-validate').removeClass('has-success');
                $(element).closest('.form-validate').addClass('has-error');
            },
            unhighlight: function(element) {
                $(element).closest('.form-validate').removeClass('has-error');
                $(element).closest('.form-validate').addClass('has-success');
            },
            errorElement: 'span',
            errorClass: 'help-block',
            errorPlacement: function (error, element) {
                console.log(element);
                if (element.parent('.input-group').length) {
                    error.insertAfter(element.parent());
                } else {
                    error.insertAfter(element);
                }
            },
            rules: {
            },
            messages: {
            },

            //para enviar el formulario por ajax
            submitHandler: function(form) {
                esperandoGuardar();

                let formData = new FormData(form);
                $.ajax({
                    url: form.action,
                    type: form.method,
                    //data: $(form).serialize(),
                    data: formData,
                    processData: false,
                    contentType: false,
                    success: function(respuesta) {
                        console.log(respuesta);
                        
                        if ( respuesta.estadoMemo == 'error' ) {
                            toastr.error('No es posible realizar la acción'+'<br><strong>Error: '+ respuesta.mensajeMemo +'</strong>', 'Atención', optionsToastr);
                        } else if ( respuesta.estadoMemo == 'success') {
                            
                            toastr.success(respuesta.mensajeMemo, 'Atención', optionsToastr);
                            // Funcion que utiliza la respuesta para actualizar la tabla principal
                            quitarElementoTabla(respuesta);
                            if ( respuesta.estadoArchivo == 'error' ) {
                                toastr.error('Archivo: No es posible realizar la acción'+'<br><strong>Error: '+ respuesta.mensajeArchivo +'</strong>', 'Atención', optionsToastr);
                            } else if ( respuesta.estadoArchivo == 'succes') {
                                toastr.success(respuesta.mensajeArchivo, 'Atención', optionsToastr);
                            }

                            $("#modalRegistrarMemoVistoBueno").modal("hide");
                            
                        }
                        
                    }            
                }).fail( function(respuesta) {//fail ajax
                    if ( respuesta.status == 400 ) {
                        mostrarErroresValidator(respuesta);
                    } else if ( respuesta.status == 500 ) {
                        toastr.error('No es posible realizar la acción, error en el servidor', 'Atención', optionsToastr);
                    } else {
                        toastr.error('No es posible realizar la acción', 'Atención', optionsToastr);
                    }
                    
                })
                .always(function() {
                    listoGuardar();
                });//ajax
                
            }

        });
  
 	});

    function fechaDocumento(id_fecha) {
        $('#' + id_fecha).datepicker("show");
    }

    function esperandoGuardar() {
        $('.page-header-fixed *').css('cursor', 'wait');
        $("#botonGuardar").attr("disabled", true);
    }

    function listoGuardar() {
        $('.page-header-fixed *').css('cursor', '');
        $("#botonGuardar").attr("disabled", false);
    }

    function mostrarErroresValidator(respuesta) {
        if (respuesta.responseJSON) {
            //console.log(respuesta.responseJSON);
            let htmlErrores = '';
            for (let k in respuesta.responseJSON) {
                //console.log(k, respuesta.responseJSON[k]);
                htmlErrores += '<li>' + respuesta.responseJSON[k] + '</li>';
            }

            $('#ulErrores').html(htmlErrores);
            $('#divErrores').css('display', '');
            toastr.error('No es posible realizar la acción' + '<br>' + 'Errores:<br> <ul>' + htmlErrores + '</ul>', 'Atención', optionsToastr);
        }
    }

    function quitarElementoTabla(respuesta) {

        $('#tr_' + respuesta.id).fadeOut(400, function() {
            tablaPrincipal.row('#tr_' + respuesta.id).remove().draw();
        });

    }

    function validarArchivo() {
        // console.log("validando archivo");
        let archivo = $('#archivo').val().replace(/.*(\/|\\)/, ''); //val();
        let extension = archivo.substring(archivo.lastIndexOf('.'));
        if (archivo == '') {
            $('#div_archivo').css('display', 'none');
            tablaArchivo.clear().draw(); // se remueven todas las filas de la tabla
            return false;
        } else {
            if (extension != '.pdf') {
                toastr.error('El archivo no tiene la extension correcta: ' + '<strong>PDF</strong>', 'Atención', optionsToastr);
                $('#archivo').val('');
                $('#div_archivo').css('display', 'none');
                tablaArchivo.clear().draw(); // se remueven todas las filas de la tabla
                return false;
            } else {
                mostrarInfoArchivo(archivo);
                return true;
            }
        }
    }

    function mostrarInfoArchivo(archivo) {
        let nombreArchivo = archivo.substring(0, archivo.lastIndexOf('.'));
        let tamañoArchivo = $('#archivo')[0].files[0].size;

        tablaArchivo.row.add([
            nombreArchivo,
            tamañoArchivo + " bits",
            selectTipoArchivo
        ]).draw(false);

        $('#div_archivo').css('display', '');
    }

    
 </script>