@extends('master')

@section('title', 'Registro Recepción Memo Visto Bueno')

@push('css-head')
<!-- BEGIN PAGE LEVEL STYLES -->
<link rel="stylesheet" type="text/css" href="{{asset('assets/plugins/select2/select2_conquer.css')}}"/>
<link rel="stylesheet" href="{{asset('assets/plugins/data-tables/DT_bootstrap.css')}}"/>
<link rel="stylesheet" type="text/css" href="{{asset('assets/plugins/bootstrap-datepicker/css/datepicker.css')}}"/>
<!-- END PAGE LEVEL STYLES -->
<style>
    .label-form {
        font-weight: bold;
        color: black;
        text-align: right;
    }

    .td-bold {
        font-weight: bold;
    }
</style>

@endpush

@section('content')

<!-- BEGIN CONTENT -->

<div class="page-content-wrapper">
	<div class="page-content-wrapper">
		<div class="page-content">

			<!-- BEGIN PAGE CONTENT-->
			<div class="row">
				<div class="col-md-12">
					<!-- BEGIN EXAMPLE TABLE PORTLET-->
					<div class="portlet">
						<div class="portlet-title">
							<div class="caption">
								<i class="fas fa-id-card-alt"></i> <i class="fas fa-tasks"></i> Registrar Visto Bueno
							</div>
						</div>
						<div class="portlet-body">
                            
                            <form action="#" method="post" enctype="multipart/form-data" id="filtros-docs" class="form-horizontal" autocomplete="off">
                                <h4 class="form-section" style="color: #69aa46;"><strong>Filtros</strong>
                                <div class="btn-group pull-right">

                                    {{-- <button class="btn btn-success btn-xs" title="Generar Excel" formaction="{{ url('documentos/general/excel') }}">
                                        <i class="fas fa-file-excel"></i> Exportar a Excel
                                    </button> --}}

                                    <button class="btn btn-info btn-xs" title="Filtrar Listado" id="filtroListado" type="button">
                                        <i class="fas fa-search" style="color:black;" ></i> Filtrar
                                    </button>
                                </div>
                                </h4>

                                <div class="form-group row">
                                    <label for="filtro_proveedor" class="col-sm-1 control-label label-form">Proveedor</label>
                                    <div class="col-sm-3">
                                        <select name="filtro_proveedor[]" id="filtro_proveedor" class="form-control select2_filtro" multiple style="width: 100%">
                                            @foreach ( $proveedores as $proveedor )
                                                <option value="{{ $proveedor->id }}" >{{ $proveedor->rut }} {{ $proveedor->nombre }}</option>
                                            @endforeach
                                        </select>
                                    </div>

                                    <label for="filtro_tipo_documento" class="col-sm-1 control-label label-form">Tipo de Documento</label>
                                    <div class="col-sm-3">
                                        <select name="filtro_tipo_documento[]" id="filtro_tipo_documento" class="form-control select2_filtro" multiple style="width: 100%">
                                            @foreach ( $tiposDocumento as $tipoDocumento )
                                                <option value="{{ $tipoDocumento->id }}" >{{ $tipoDocumento->nombre }}</option>
                                            @endforeach
                                        </select>
                                    </div>

                                    <label for="filtro_numero_documento" class="col-sm-1 control-label label-form">N° Documento</label>
                                    <div class="col-sm-3">
                                        <input type="text" class="form-control " id="filtro_numero_documento" name="filtro_numero_documento" >
                                    </div>

                                </div>

                                <div class="form-group row">
    
                                    <label for="filtro_numero_memo" class="col-sm-1 control-label label-form">N° Memo</label>
                                    <div class="col-sm-3">
                                        <input type="text" class="form-control " id="filtro_numero_memo" name="filtro_numero_memo" >
                                    </div>

                                </div>
                                
                            </form>
                            <div class="row">
                                <div class="col-md-12">
                                    <div class="panel">
                                        <div class="panel-body">
                                            <div class="table-responsive">
                                                <table class="table table-striped table-bordered table-hover table-full-width" id="tabla_principal">
                                                    <thead>
                                                    <tr>
                                                        <th >Fecha Solicitud</th>
                                                        <th >N° Memo</th>
                                                        <th >Responsable</th>
                                                        <th >Referente Técnico</th>
                                                        <th class="text-center">
                                                            <i class="fa fa-cog"></i>
                                                        </th>
                                                    </tr>
                                                    </thead>
                                                    <tbody>
                                                    {{-- @forelse($vistosBuenos as $vistoBueno)
                                                        <tr id="tr_{{ $vistoBueno->id }}">
                                                            <td>{{ fecha_dmY($vistoBueno->fecha_solicitud) }}</td>
                                                            <td>{{ $vistoBueno->memo }}</td>
                                                            <td>{{ $vistoBueno->getDigitador->name }}</td>
                                                            <td>
                                                                @if ($vistoBueno->getReferenteTecnico)
                                                                    {{ $vistoBueno->getReferenteTecnico->nombre.' '.$vistoBueno->getReferenteTecnico->responsable }}
                                                                @endif
                                                            </td>
                                                            <td>
                                                                <div class="btn-group">

                                                                    @permission(['registro-visto-bueno'])
                                                                    <button class="btn btn-info btn-xs " title="Registrar" onclick="registrarVistoBueno({{ $vistoBueno->id }});">
                                                                        <i class="fas fa-file-signature fa-lg"></i>
                                                                    </button>
                                                                    @endpermission
                                                                    @permission(['ver-memo-solicitud-visto-bueno'])
                                                                    <a class="btn btn-danger btn-xs " title="Ver Memo Solicitud Visto Bueno" target="_blank" href="{{ asset('referente_tecnico/generar/pdf_solicitud_visto_bueno/'.$vistoBueno->id) }}">
                                                                        <i class="fas fa-file-pdf fa-lg"></i>
                                                                    </a>
                                                                    @endpermission
                                                                    @permission(['editar-solicitud-visto-bueno'])
                                                                    <button class="btn btn-warning btn-xs " title="Editar Memo" onclick="editarMemo({{ $vistoBueno->id }});">
                                                                        <i class="fas fa-edit fa-lg"></i>
                                                                    </button>
                                                                    @endpermission
                                                                    
                                                                </div>
                                                            </td>
                                                        </tr>
                                                    @empty

                                                    @endforelse --}}
                                                    </tbody>
                                                </table>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
						</div>
					</div>
					<!-- END EXAMPLE TABLE PORTLET-->
				</div>
			</div>
			<!-- END PAGE CONTENT-->
			
			
			
			
		</div> <!--/div class="page-content" -->
	</div> <!--/div class="page-content-wrapper" -->
</div><!--/div class="page-content-wrapper" -->
<!-- END CONTENT -->
<div id="modal"></div>
<div id="modal2"></div>
@endsection

@push('script-footer')
<!-- BEGIN PAGE LEVEL PLUGINS -->
<script type="text/javascript" src="{{ asset('assets/plugins/select2/select2.min.js') }} "></script>
<script type="text/javascript" src="{{ asset('datatables/jquery.dataTables.min.js') }}"></script>
<script type="text/javascript" src="{{ asset('assets/plugins/data-tables/DT_bootstrap.js') }}"></script>
<script type="text/javascript" src="{{asset('assets/plugins/bootstrap-datepicker/js/bootstrap-datepicker.js')}}"></script>
<script type="text/javascript" src="{{asset('assets/plugins/bootstrap-datetimepicker/js/bootstrap-datetimepicker.js')}}"></script>="text/javascript" src="{{asset('assets/plugins/bootstrap-datetimepicker/js/bootstrap-datetimepicker.js')}}"></script>

<!-- END PAGE LEVEL PLUGINS -->

<!-- BEGIN PAGE LEVEL SCRIPTS -->
<script src="{{ asset('assets/scripts/app.js') }} "></script>

<script type="text/javascript">
    let tablaPrincipal = $('#tabla_principal').DataTable({
        "processing": true,
        "serverSide": true,
        "ajax": {
            "url" : "{{ url('referente_tecnico/data_table_index_memo_visto_bueno') }}",
            "type" : "POST",
            "data": function(d){
                d.form = JSON.stringify( $("#filtros-docs").serializeArray() );
                // console.log('se envia:');
                // console.log(d);
            },
        },
        // Set rows IDs
        rowId: function(data) {
            return 'tr_' + data.DT_RowID;
        },
        "deferRender": true,
        "language": {
            "emptyTable":     "No hay datos disponibles en la tabla",
            "info":           "Mostrando registros del _START_ al _END_ de un total de _TOTAL_ registros",
            "infoEmpty":      "No hay datos disponibles para mostrar en la tabla",
            "infoFiltered":   "(filtrado de un total de _MAX_ registros)",
            "infoPostFix":    "",
            "thousands":      ",",
            "lengthMenu":     "Mostrar _MENU_ registros",
            "loadingRecords": "Cargando...",
            "processing":     "Procesando...",
            "search":         "Buscar: ",
            "zeroRecords":    "<i class='far fa-frown fa-lg'></i> No hay datos que coincidan con la búsqueda <i class='far fa-frown fa-lg'></i>",
            "paginate": {
                "first":      "Primero",
                "last":       "Último",
                "next":       "Siguiente",
                "previous":   "Anterior"
            },
            "aria": {
                "sortAscending":  ": Activar para ordenar la columna de manera ascendente",
                "sortDescending": ": Activar para ordenar la columna de manera descendente"
            }
        },
        "lengthMenu": [
            [10, 15, 20, -1],
            [10, 15, 20, "Todos"] // change per page values here
        ],
        // set the initial value
        "displayLength": 10,
        "paging":   true,
        "ordering": true,
        "info":     true,
        "searching": true,
        "order" : [[1,'desc']]
    });

    $.fn.dataTable.ext.errMode = 'none';//quita los mensajes de error de la datatable ajax

    $('#tabla_principal').on( 'error.dt', function ( e, settings, techNote, message ) {
		console.log( 'An error has been reported by DataTables: ', message );//para mostrar los mensajes de error en la consola del navegador
    } );
    
    jQuery('#tabla_principal_filter input').addClass("form-control input-large"); // modify table search input
    jQuery('#tabla_principal_wrapper .dataTables_length select').addClass("form-control input-small"); // modify table per page dropdown
    jQuery('#tabla_principal_wrapper .dataTables_length select').select2(); // initialize select2 dropdown

    $('#tabla_principal_column_toggler input[type="checkbox"]').change(function() {
        /* Get the DataTables object again - this is not a recreation, just a get of the object */
        var iCol = parseInt($(this).attr("data-column"));
        var bVis = oTable.fnSettings().aoColumns[iCol].bVisible;
        oTable.fnSetColumnVis(iCol, (bVis ? false : true));
    });

	$(document).ready(function() {
		App.init(); // initlayout and core plugins

   		$("#referente-tecnico-opciones").addClass( "active" );
		$("#registro-visto-bueno-li").addClass( "active" );
        $("#referente-tecnico-opciones-a").append( '<span class="selected"></span>' );
        
        $('#filtroListado').click(function(){
            $("#modalCarga").modal({backdrop: 'static', keyboard: false});
            $('#modalCargaImg').addClass('fa-pulso');
                
            $('#tabla_principal').DataTable().ajax.reload( function ( json ) {
                $("#modalCarga").modal('toggle');
            });
        });

        $(".select2_filtro").select2({
            // allowClear: true,
        });

	});

    function registrarVistoBueno(id)
    {
        $.get( '{{ url("referente_tecnico/modal/registrar_memo_visto_bueno") }}/' + id, function( data ) {
            $( "#modal" ).html( data );
            $( "#modalRegistrarMemoVistoBueno" ).modal();
        });
    }

    function editarMemo(id)
    {
        $.get( '{{ url("referente_tecnico/modal/editar_memo") }}/' + id, function( data ) {
            $( "#modal" ).html( data );
            $( "#modalEditarMemo" ).modal();
        });
    }

    function eliminarMemo(id)
    {
        $.get( '{{ url("referente_tecnico/modal/eliminar_solicitud_visto_bueno") }}/' + id, function( data ) {
            $( "#modal" ).html( data );
            $( "#modalEliminarSolicitudVistoBueno" ).modal();
        });
    }
</script>

@endpush