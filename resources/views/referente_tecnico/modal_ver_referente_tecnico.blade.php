<link href="{{ asset('sistema/css/modales.css') }} " rel="stylesheet" type="text/css"/>
<div class="modal fade" id="modalVer" role="dialog" aria-labelledby="myModalLabel3" aria-hidden="true">
	<div class="modal-dialog" style="width:1000px;">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" title="Cerrar" class="close" data-dismiss="modal" aria-hidden="true"></button>
				<h4 class="modal-title" ><strong><i class="fas fa-id-card-alt"></i>&nbsp;&nbsp;Ver Referente Técnico</strong></h4>
            </div>
          
            <div class="modal-body">
                <div class="form-group row">
                    <div class="col-xs-12">
                        <div class="form-group">
                            <label class="col-sm-2 control-label font-bold">Rut</label>
                            <label class="col-sm-4 control-label">{{ $referenteTecnico->formatRut() }}</label>

                            <label class="col-sm-3 control-label font-bold">Responsable Finanzas</label>
                            <label class="col-sm-3 control-label">@if( $referenteTecnico->origen == 1 ) Si @else No @endif </label>
                        </div>
                    </div>
                </div>
                <div class="form-group row">
                    <div class="col-xs-12">
                        <div class="form-group">
                            <label class="col-sm-2 control-label font-bold">Cargo</label>
                            <label class="col-sm-8 control-label">{{ $referenteTecnico->nombre }}</label>
                        </div>
                    </div>
                </div>

                <div class="form-group row">
                    <div class="col-xs-12">
                        <div class="form-group">
                            <label class="col-sm-2 control-label font-bold">Responsable</label>
                            <label class="col-sm-8 control-label">{{ $referenteTecnico->responsable }}</label>
                        </div>
                    </div>
                </div>

                <div class="form-group row">
                    <div class="col-xs-12">
                        <div class="form-group">
                            <label class="col-sm-2 control-label font-bold">Imagen Firma</label>
                            <label class="col-sm-8 control-label"><img src="{{ asset($referenteTecnico->url_firma) }}"  ></label>
                        </div>
                    </div>
                </div>

                @if ($referenteTecnico->getUser)
                    <div class="form-group row">
                        <div class="col-xs-12">	
                            <h4 class="headers-view">
                                Usuario
                            </h4>
                            <hr>
                            <div class="form-group">
                                <label class="col-sm-2 control-label font-bold">Rut</label>
                                <label class="col-sm-4 control-label ">{{ $referenteTecnico->getUser->formatRut() }}</label>

                                <label class="col-sm-2 control-label font-bold">Nombre</label>
                                <label class="col-sm-4 control-label ">{{ $referenteTecnico->getUser->name }}</label>
                            </div>
                        </div>
                    </div>

                    <div class="form-group row">
                        <div class="col-xs-12">
                            <div class="form-group">
                                <label class="col-sm-2 control-label font-bold">E-mail</label>
                                <label class="col-sm-8 control-label">{{ $referenteTecnico->getUser->email }}</label>
                            </div>
                        </div>
                    </div>
                @endif
            </div>{{-- /modal-body --}}

            <div class="modal-footer form-actions">
                <div class="btn-group">
                <button type="button" class="btn btn-info" title="Cerrar" data-dismiss="modal" aria-hidden="true">Cerrar</button>
                </div>
            </div>
          	
		</div>
	</div>
</div>

<script type="text/javascript">

 	$(document).ready(function(){
         
  
 	});

    
 </script>