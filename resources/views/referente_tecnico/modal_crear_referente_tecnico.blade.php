<link href="{{ asset('sistema/css/modales.css') }} " rel="stylesheet" type="text/css"/>
<div class="modal fade" id="modalCrear" role="dialog" aria-labelledby="myModalLabel3" aria-hidden="true">
	<div class="modal-dialog" style="width:1100px;">
		<div class="modal-content">
            <div class="modal-header">
				<button type="button" title="Cerrar" class="close" data-dismiss="modal" aria-hidden="true"></button>
				<h4 class="modal-title" ><strong><i class="fas fa-id-card-alt"></i>&nbsp;&nbsp;Crear Referente Técnico</strong></h4>
            </div>

            <form action="{{ asset('referente_tecnico') }}" method="post" class="horizontal-form" id="form" autocomplete="off">
                <div class="modal-body">

                    <div class="form-body">
                        <div class="note note-danger" id="divErrores" style="display:none;">
                            <h4 class="block" style="margin-bottom:5 px;">Debe Completar los siguientes campos: </h4>
                            <ul id="ulErrores"></ul>
                        </div>

                        <div class="form-group row">
                            <label for="usuario" class="col-sm-2 control-label label-form">Usuario <span class="span-label">*</span></label>
                            <div class="col-sm-6 form-validate">
                                <select name="usuario" id="usuario" class="form-control select2" required onchange="setCamposReferente(this);" >
                                    <option value="" data-name="" data-rut="">Seleccione</option>
                                    @foreach ( $usuarios as $usuario)
                                        <option value="{{ $usuario->id }}" data-name="{{ $usuario->name }}" data-rut="{{ $usuario->formatRut() }}" >{{ $usuario->name }}&nbsp;&nbsp;&nbsp;{{ $usuario->formatRut() }}</option>
                                    @endforeach
                                </select>
                            </div>
                        </div>

                       <div class="form-group row">
                            <label for="rut" class="col-sm-2 control-label label-form">Rut </label>
                            <div class="col-sm-4 form-validate">
                                <input type="text" class="form-control" id="rut" name="rut" >
                            </div>

                            <label for="responsable_finanzas" class="col-sm-3 control-label label-form">Responsable Finanzas</label>
                            <div class="col-sm-3 form-validate">
                            <div class="checkbox check" >
                                <label class="label_checkbox">
                                    <input type="checkbox" class="check_input" id="responsable_finanzas" name="responsable_finanzas" value="1"  >
                                </label>
                                </div>
                            </div>
                        </div>

                        <div class="form-group row">
                            <label for="cargo" class="col-sm-2 control-label label-form">Cargo <span class="span-label">*</span></label>
                            <div class="col-sm-8 form-validate">
                                <input type="text" class="form-control" id="cargo" name="cargo" required >
                            </div>
                        </div>

                        <div class="form-group row">
                            <label for="responsable" class="col-sm-2 control-label label-form">Responsable <span class="span-label">*</span></label>
                            <div class="col-sm-8 form-validate">
                                <input type="text" class="form-control" id="responsable" name="responsable" required >
                            </div>
                        </div>

                        <div class="form-group row">
                            <label for="archivo" class="col-sm-2 control-label label-form">Imagen Firma {{--<span class="span-label">*</span>--}}</label>
                        </div>

                        <div class="form-group row">
                            <div class="input-group col-sm-10 col-sm-offset-1 form-validate ">
                                <span class="input-group-addon" id="inputGroupFileAddon01"><strong>Seleccione Imagen (.jpeg - .png - .jpg)</strong></span>
                                <input type="file" class="form-control" accept="image/*" name="archivo" id="archivo" aria-describedby="inputGroupFileAddon01" {{--required--}} onchange="validarArchivo();">
                            </div>
                        </div>

                    </div>
                </div>

                <div class="modal-footer form-actions right">
                    <input type="hidden" name="_token" value="{{ csrf_token() }}">

                    <button type="button" title="Cancelar" class="btn btn-default" data-dismiss="modal">Cancelar</button>
                    <button type="submit" title="Guardar Referente Técnico" class="btn btn-success" id="botonGuardar"><i class="far fa-save" style="color:black;"></i> Guardar Referente Técnico</button>
                </div>
            </form>

        </div>
    </div>
</div>

<script src="{{asset('assets/plugins/jquery-rut/jquery.Rut.min.js')}}" type="text/javascript"></script>
<script>
    $(document).ready(function() {
        $(".select2").select2();

        $("#rut").Rut({
  			on_error: function(){
                toastr.error("El Rut ingresado no es valido", optionsToastr);
             },
		   format_on: 'keyup'
		});
        
        
        $("#form").validate({
            highlight: function(element) {
                $(element).closest('.form-validate').removeClass('has-success');
                $(element).closest('.form-validate').addClass('has-error');
            },
            unhighlight: function(element) {
                $(element).closest('.form-validate').removeClass('has-error');
                $(element).closest('.form-validate').addClass('has-success');
            },
            errorElement: 'span',
            errorClass: 'help-block',
            errorPlacement: function (error, element) {
                console.log(element);
                if (element.parent('.input-group').length) {
                    error.insertAfter(element.parent());
                } else {
                    error.insertAfter(element);
                }
            },
            rules: {
            },
            messages: {
            },

            //para enviar el formulario por ajax
            submitHandler: function(form) {
                esperandoGuardar();

                let formData = new FormData(form);

                $.ajax({
                    url: form.action,
                    type: form.method,
                    //data: $(form).serialize(),
                    data: formData,
                    processData: false,
                    contentType: false,
                    success: function(respuesta) {
                        console.log(respuesta);
                        if ( respuesta.estado == 'error' ) {
                            toastr.error('No es posible realizar la acción'+'<br><strong>Error: '+ respuesta.mensaje +'</strong>', 'Atención', optionsToastr);
                        } else if ( respuesta.estado == 'success') {
                            
                            toastr.success(respuesta.mensaje, 'Atención', optionsToastr);
                            // Agregar la informacion de la fila de la tabla principal
                            nuevaFilaTabla(respuesta);
                            $("#modalCrear").modal("hide");
                            
                        }
                        
                    }            
                }).fail( function(respuesta) {//fail ajax
                    if ( respuesta.status == 400 ) {
                        mostrarErroresValidator(respuesta);
                    } else if ( respuesta.status == 500 ) {
                        toastr.error('No es posible realizar la acción, error en el servidor', 'Atención', optionsToastr);
                    } else {
                        toastr.error('No es posible realizar la acción', 'Atención', optionsToastr);
                    }
                    
                })
                .always(function() {
                    listoGuardar();
                });//ajax
                
            }

        });

    });

    function esperandoGuardar()
    {   
        $('.page-header-fixed *').css('cursor', 'wait');
        $("#botonGuardar").attr("disabled",true);
    }

    function listoGuardar()
    {
        $('.page-header-fixed *').css('cursor', '');
        $("#botonGuardar").attr("disabled",false);
    }

    function mostrarErroresValidator(respuesta)
    {
        if ( respuesta.responseJSON ) {
            //console.log(respuesta.responseJSON);
            let htmlErrores = '';
            for (let k in respuesta.responseJSON) { 
                //console.log(k, respuesta.responseJSON[k]);
                htmlErrores += '<li>' + respuesta.responseJSON[k] + '</li>';
            }

            $('#ulErrores').html(htmlErrores);
            $('#divErrores').css('display','');
            toastr.error('No es posible realizar la acción'+'<br>'+'Errores:<br> <ul>'+htmlErrores+'</ul>', 'Atención', optionsToastr);
        }
    }

    function nuevaFilaTabla(respuesta)
    {

        let botones = '<td width="10%"><div class="btn-group">';
        botones += '<button class="btn btn-success btn-xs" title="Ver Referente Técnico" onclick="ver('+ respuesta.id +');">';
        botones += '<i class="fas fa-eye"></i></button>';
        botones += '<button class="btn btn-warning btn-xs" title="Editar Referente Técnico" onclick="editar('+ respuesta.id +');">';
        botones += '<i class="fas fa-edit"></i></button>';
        botones += '<button class="btn btn-danger btn-xs" title="Eliminar Referente Técnico" onclick="eliminar('+ respuesta.id +');">';
        botones += '<i class="fas fa-trash"></i></button>';
        botones += '</div></td>';

        let rowNode = tablaPrincipal.row.add ([
            respuesta.id,
            respuesta.nombre,
            respuesta.rut,
            respuesta.responsable,
            botones
        ]).draw().node();

        // Se entrega el id del elemento a la fila, para poder eliminar o editar posteriormente
        $( rowNode ).attr("id", 'tr_'+ respuesta.id);
    }

    function setCamposReferente(usuario)
    {
        $('#rut').val($('#usuario').find(':selected').data('rut'));
        $('#responsable').val($('#usuario').find(':selected').data('name'));
    }

    function validarArchivo() {
        // console.log("validando archivo");
        let archivo = $('#archivo').val().replace(/.*(\/|\\)/, ''); //val();
        let extension = archivo.substring(archivo.lastIndexOf('.'));
        if (archivo == '') {
            return false;
        } else {
            if (extension != '.png' && extension != '.jpeg' && extension != '.jpg') {
                toastr.error('El archivo no tiene una extension correcta', 'Atención', optionsToastr);
                $('#archivo').val('');
                return false;
            } else {
                return true;
            }
        }
    }

</script>