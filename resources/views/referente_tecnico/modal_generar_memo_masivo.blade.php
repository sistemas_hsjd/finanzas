<link href="{{ asset('sistema/css/modales.css') }} " rel="stylesheet" type="text/css"/>
<div class="modal fade" id="modalGenerarMemoMasivo" role="dialog" aria-labelledby="myModalLabel3" aria-hidden="true">
	<div class="modal-dialog" style="width:1000px;">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" title="Cerrar" class="close" data-dismiss="modal" aria-hidden="true"></button>
				<h4 class="modal-title" ><strong><i class="fas fa-file-signature fa-lg"></i>&nbsp;&nbsp;Generar Memo Para Visto Bueno</strong></h4>
            </div>

            <form action="{{ asset('referente_tecnico/generar_memo_masivo') }}" method="post" class="horizontal-form" id="form" autocomplete="off">
                <div class="modal-body">
                    <div class="form-body" >
                        <h4 class="form-section headers-view" style="margin-top: 0px;">Visto Bueno</h4>
                        <div class="form-group row">
                            <div class="col-xs-12 table-responsive">
                                <table class="table table-striped table-bordered table-hover " id="tabla_documentos">
                                    <thead>
                                        <tr>
                                            <th width="8%">Rut Proveedor</th>
                                            <th width="25%">Nombre Proveedor</th>
                                            <th width="7%" class="text-center">N° Documento</th>
                                            <th width="10%">N° Documento Compra</th>
                                            <th width="7%" class="text-center">Fecha</th>
                                            <th width="10%">Licitación</th>
                                            <th width="10%">Total</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                    @forelse($documentos as $documento)
                                        <tr>
                                            <td>{{ $documento->getProveedor->formatRut() }}</td>
                                            <td>{{ $documento->getProveedor->nombre }}</td>
                                            <td class="text-center">{{ $documento->numero_documento }}</td>
                                            <td>{{ $documento->documento_compra }}</td>
                                            <td class="text-center">{{ fecha_dmY($documento->fecha_documento) }}</td>
                                            <td>{{ $documento->licitacion }}</td>
                                            <td class="text-right">$ {{ formatoMiles($documento->total_documento) }}</td>
                                            <input type="hidden" class="form-control" id="id_doc_{{ $documento->id }}" 
                                                        name="id_doc[{{ $documento->id }}]" required
                                                        value="{{ $documento->id }}">
                                        </tr>
                                    @empty

                                    @endforelse
                                    </tbody>
                                </table>
                            </div>
                        </div>
                        
                        <h4 class="form-section headers-view" >Visto Bueno</h4>
                        <div class="note note-danger" id="divErrores" style="display:none;">
                            <h4 class="block" style="margin-bottom:5 px;">Debe Completar los siguientes campos: </h4>
                            <ul id="ulErrores"></ul>
                        </div>

                        <div class="form-group row">
                            <label for="numero_memo" class="col-sm-2 control-label label-form">N° Memo <span class="span-label">*</span></label>
                            <div class="col-sm-4 form-validate">
                                <input type="text" class="form-control" id="numero_memo" name="numero_memo" value="{{ $lastVistoBueno }}" required readonly >
                            </div>

                            <label for="fecha_solicitud" class="col-sm-2 control-label label-form">Fecha Solicitud <span class="span-label">*</span></label>
                            <div class="col-sm-3 form-validate">
                                <div class="input-group">
                                    <input type="text" class="form-control fechas" id="fecha_solicitud" name="fecha_solicitud" required value="{{ date("d/m/Y") }}" readonly>
                                    <span class="input-group-addon" {{-- onclick="fechaDocumento('fecha_solicitud');" --}} style="cursor:pointer;"><i class="fa fa-calendar" aria-hidden="true" style="color: black;"></i></span>
                                </div>
                            </div>
                        </div>

                        <div class="form-group row" style="display:none;">       
                            <label for="numero_informe" class="col-sm-2 control-label label-form">N° Informe <span class="span-label">*</span></label>
                            <div class="col-sm-4 form-validate" >
                                <input type="text" class="form-control" id="numero_informe" name="numero_informe"  >
                            </div>
                            
                            <label for="fecha_informe" class="col-sm-2 control-label label-form">Fecha Informe <span class="span-label">*</span></label>
                            <div class="col-sm-3 form-validate">
                                <div class="input-group">
                                    <input type="text" class="form-control fechas" id="fecha_informe" name="fecha_informe"  value="{{ date("d/m/Y") }}" >
                                    <span class="input-group-addon" onclick="fechaDocumento('fecha_informe');" style="cursor:pointer;"><i class="fa fa-calendar" aria-hidden="true" style="color: black;"></i></span>
                                </div>
                            </div>
                        </div>

                        <input type="hidden" name="_token" value="{{ csrf_token() }}">
                        <input type="hidden" name="idReferenteTecnico" value="{{ $idReferenteTecnico }}" required>
                    </div>  
                </div>{{-- /modal-body --}}

                
                <div class="modal-footer form-actions right ">
                    <button type="button" title="Cancelar" class="btn btn-default" data-dismiss="modal">Cancelar</button>
                    <button type="submit" title="Generar Memo Visto Bueno" class="btn btn-success" id="botonGuardar"><i class="far fa-save" style="color: black;"></i> Generar Memo</button>
                </div> {{-- /modal-footer --}}
            </form>
          	
		</div>
	</div>
</div>

 <script type="text/javascript">
    let tablaItems = $('#tabla_item_presupuestario').DataTable({
        "language": {
            "emptyTable": "No hay datos disponibles en la tabla",
        },
        "paging":   false,
        "ordering": false,
        "info":     false,
        "searching": false
    });
 
    let tablaArchivo = $('#tabla_info_archivo').DataTable({
         "language": {
            "emptyTable": "No hay datos disponibles en la tabla",
        },
        "paging":   false,
        "ordering": false,
        "info":     false,
        "searching": false
    });

 	$(document).ready(function(){
         
        {{-- $('#fecha_solicitud').datepicker({
            format: 'dd/mm/yyyy',
            //endDate: new Date(),
            autoclose: true,
            language: 'es'
        }); --}}

        $('#fecha_informe').datepicker({
            format: 'dd/mm/yyyy',
            //endDate: new Date(),
            autoclose: true,
            language: 'es'
        });

        $("#form").validate({
            highlight: function(element) {
                $(element).closest('.form-validate').removeClass('has-success');
                $(element).closest('.form-validate').addClass('has-error');
            },
            unhighlight: function(element) {
                $(element).closest('.form-validate').removeClass('has-error');
                $(element).closest('.form-validate').addClass('has-success');
            },
            errorElement: 'span',
            errorClass: 'help-block',
            errorPlacement: function (error, element) {
                console.log(element);
                if (element.parent('.input-group').length) {
                    error.insertAfter(element.parent());
                } else {
                    error.insertAfter(element);
                }
            },
            rules: {
            },
            messages: {
            },

            //para enviar el formulario por ajax
            submitHandler: function(form) {
                esperandoGuardar();

                let formData = new FormData(form);
                $.ajax({
                    url: form.action,
                    type: form.method,
                    //data: $(form).serialize(),
                    data: formData,
                    processData: false,
                    contentType: false,
                    success: function(respuesta) {
                        // console.log(respuesta);
                        
                        if ( respuesta.estado == 'error' ) {
                            toastr.error('No es posible realizar la acción'+'<br><strong>Error: '+ respuesta.mensaje +'</strong>', 'Atención', optionsToastr);
                        } else if ( respuesta.estado == 'success') {
                            
                            toastr.success(respuesta.mensaje, 'Atención', optionsToastr);
                            // Funcion que utiliza la respuesta para actualizar la tabla principal
                            quitarElementoTabla(respuesta);
                            $("#modalGenerarMemoMasivo").modal("hide");
                            
                        }
                        
                    }            
                }).fail( function(respuesta) {//fail ajax
                    if ( respuesta.status == 400 ) {
                        mostrarErroresValidator(respuesta);
                    } else if ( respuesta.status == 500 ) {
                        toastr.error('No es posible realizar la acción, error en el servidor', 'Atención', optionsToastr);
                    } else {
                        toastr.error('No es posible realizar la acción', 'Atención', optionsToastr);
                    }
                    
                })
                .always(function() {
                    listoGuardar();
                });//ajax
                
            }

        });
  
 	});

    function fechaDocumento(id_fecha) {
        $('#' + id_fecha).datepicker("show");
    }

    function esperandoGuardar() {
        $('.page-header-fixed *').css('cursor', 'wait');
        $("#botonGuardar").attr("disabled", true);
    }

    function listoGuardar() {
        $('.page-header-fixed *').css('cursor', '');
        $("#botonGuardar").attr("disabled", false);
    }

    function mostrarErroresValidator(respuesta) {
        if (respuesta.responseJSON) {
            //console.log(respuesta.responseJSON);
            let htmlErrores = '';
            for (let k in respuesta.responseJSON) {
                //console.log(k, respuesta.responseJSON[k]);
                htmlErrores += '<li>' + respuesta.responseJSON[k] + '</li>';
            }

            $('#ulErrores').html(htmlErrores);
            $('#divErrores').css('display', '');
            toastr.error('No es posible realizar la acción' + '<br>' + 'Errores:<br> <ul>' + htmlErrores + '</ul>', 'Atención', optionsToastr);
        }
    }

    function quitarElementoTabla(respuesta) {

        // Se remueve el checked de los checkbox, para que no los considere la funcion memoMasivo()
        $(".select_item:checked").each(function(){
            $(this).prop('checked',false);
        });

        for (var i = 0; i < respuesta.ids.length; i++) {
            console.log('id del documento ' + respuesta.ids[i]);

             $('#tr_' + respuesta.ids[i]).fadeOut(400, function() {
                tablaPrincipal.row('#tr_' + respuesta.ids[i]).remove().draw();
            });
        }
       

    }

 </script>