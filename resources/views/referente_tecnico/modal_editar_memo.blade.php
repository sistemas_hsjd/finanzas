<link href="{{ asset('sistema/css/modales.css') }} " rel="stylesheet" type="text/css"/>
<style>
#tabla_documentos thead tr th {
    font-size: 11px;
    font-weight: 600;
}
</style>
<div class="modal fade" id="modalEditarMemo" role="dialog" aria-labelledby="myModalLabel3" aria-hidden="true">
	<div class="modal-dialog" style="width:1000px;">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" title="Cerrar" class="close" data-dismiss="modal" aria-hidden="true"></button>
				<h4 class="modal-title" ><strong><i class="fas fa-file-signature fa-lg"></i>&nbsp;&nbsp;Editar Memo Solicitud Visto Bueno</strong></h4>
            </div>

            <form action="{{ asset('referente_tecnico/editar_memo') }}" method="post" class="horizontal-form" id="form" autocomplete="off">
                <div class="modal-body">

                    <div class="form-body" >
                        <div class="note note-danger" id="divErrores" style="display:none;">
                            <h4 class="block" style="margin-bottom:5px;">Debe Completar los siguientes campos: </h4>
                            <ul id="ulErrores"></ul>
                        </div>

                        <h4 class="form-section" style="color: #69aa46;margin-top: 0px;">Documentos</h4>
                        <div class="form-group row">
                            <div class="col-xs-12 table-responsive">
                                <table class="table table-striped table-bordered table-hover " id="tabla_documentos">
                                    <thead>
                                        <tr>
                                            <th width="10%">Rut Proveedor</th>
                                            <th width="30%">Nombre Proveedor</th>
                                            <th width="10%">N° Documento</th>
                                            <th width="10%">N° Documento Compra</th>
                                            <th width="10%">Fecha</th>
                                            <th width="10%">Licitación</th>
                                            <th width="20%">Total</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                    @forelse($vistoBueno->getDocumentos as $documento)
                                        <tr>
                                            <td>{{ $documento->getProveedor->formatRut() }}</td>
                                            <td>{{ $documento->getProveedor->nombre }}</td>
                                            <td>{{ $documento->numero_documento }}</td>
                                            <td>{{ $documento->documento_compra }}</td>
                                            <td>{{ fecha_dmY($documento->fecha_documento) }}</td>
                                            <td>{{ $documento->licitacion }}</td>
                                            <td class="text-right">$ {{ formatoMiles($documento->total_documento) }}</td>
                                        </tr>
                                    @empty

                                    @endforelse
                                    </tbody>
                                </table>
                            </div>
                        </div>

                        <div class="form-group row">
                            <label for="documentos[]" class="col-sm-2 control-label label-form">Documentos <span class="span-label">*</span></label>
                            <div class="col-sm-9 form-validate">
                                <select name="documentos[]" id="documentos" class="form-control select2" required multiple="multiple">
                                @forelse($vistoBueno->getDocumentos as $documento)
                                    <option value="{{ $documento->id }}" selected>{{ $documento->getProveedor->rut.' '.$documento->getProveedor->nombre.' N° '.$documento->numero_documento }}</option>
                                @empty
                                @endforelse

                                @foreach ( $documentosReferenteTecnico as $documento)
                                    <option value="{{ $documento->id }}">{{ $documento->getProveedor->rut.' '.$documento->getProveedor->nombre.' N° '.$documento->numero_documento }}</option>
                                @endforeach
                                </select>
                            </div>
                        </div>
                        
                        <h4 class="form-section" style="color: #69aa46;">Visto Bueno</h4>

                        <div class="form-group row">
                            <label for="numero_memo" class="col-sm-2 control-label label-form">N° Memo <span class="span-label">*</span></label>
                            <div class="col-sm-4 form-validate">
                                <input type="text" class="form-control" id="numero_memo" name="numero_memo" required  value="{{ $vistoBueno->memo }}" readonly>
                            </div>

                            <label for="fecha_solicitud" class="col-sm-2 control-label label-form">Fecha Solicitud <span class="span-label">*</span></label>
                            <div class="col-sm-3 form-validate">
                                <div class="input-group">
                                    <input type="text" class="form-control fechas" id="fecha_solicitud" name="fecha_solicitud" required value="{{ fecha_dmY($vistoBueno->fecha_solicitud) }}" readonly>
                                    <span class="input-group-addon" onclick="fechaDocumento('fecha_solicitud');" style="cursor:pointer;"><i class="fa fa-calendar" aria-hidden="true" style="color: black;"></i></span>
                                </div>
                            </div>
                            {{-- 
                            <label for="fecha_recepcion" class="col-sm-2 col-sm-offset-1 control-label label-form">Fecha Recepción </label>
                            <div class="col-sm-3 form-validate">
                                <div class="input-group">
                                    <input type="text" class="form-control fechas" id="fecha_recepcion" name="fecha_recepcion" @if($vistoBueno->fecha_recepcion != null) value="{{ fecha_dmY($vistoBueno->fecha_recepcion) }}" @endif>
                                    <span class="input-group-addon" onclick="fechaDocumento('fecha_recepcion');" style="cursor:pointer;"><i class="fa fa-calendar" aria-hidden="true" style="color: black;"></i></span>
                                </div>
                            </div>
                            --}}
                        </div>
                        
                        {{-- 
                        <div class="form-group row">
                            <label for="numero_memo" class="col-sm-2 control-label label-form">N° Memo <span class="span-label">*</span></label>
                            <div class="col-sm-4 form-validate">
                                <input type="text" class="form-control" id="numero_memo" name="numero_memo" required  value="{{ $vistoBueno->memo }}">
                            </div>

                            <label for="numero_memo_respuesta" class="col-sm-2 control-label label-form">N° Memo Respuesta </label>
                            <div class="col-sm-4 form-validate">
                                <input type="text" class="form-control" id="numero_memo_respuesta" name="numero_memo_respuesta" value="{{ $vistoBueno->memo_respuesta }}">
                            </div>
                        </div>
                        --}}

                        <div class="form-group row" style="display:none;">       
                            <label for="numero_informe" class="col-sm-2 control-label label-form">N° Informe <span class="span-label">*</span></label>
                            <div class="col-sm-4 form-validate" >
                                <input type="text" class="form-control" id="numero_informe" name="numero_informe" value="{{ $vistoBueno->informe }}">
                            </div>
                            
                            <label for="fecha_informe" class="col-sm-2 control-label label-form">Fecha Informe <span class="span-label">*</span></label>
                            <div class="col-sm-3 form-validate">
                                <div class="input-group">
                                    <input type="text" class="form-control fechas" id="fecha_informe" name="fecha_informe" value="{{ fecha_dmY($vistoBueno->fecha_informe) }}" >
                                    <span class="input-group-addon" onclick="fechaDocumento('fecha_informe');" style="cursor:pointer;"><i class="fa fa-calendar" aria-hidden="true" style="color: black;"></i></span>
                                </div>
                            </div>
                        </div>
                        {{-- 
                        <div class="form-group row">
                            <label for="fecha_recepcion_fisica" class="col-sm-2 control-label label-form">Fecha Recepción Física</label>
                            <div class="col-sm-3 form-validate">
                                <div class="input-group">
                                    <input type="text" class="form-control fechas" id="fecha_recepcion_fisica" name="fecha_recepcion_fisica" @if($vistoBueno->fecha_recepcion_fisica != null) value="{{ fecha_dmY($vistoBueno->fecha_recepcion_fisica) }}" @endif>
                                    <span class="input-group-addon" onclick="fechaDocumento('fecha_recepcion_fisica');" style="cursor:pointer;"><i class="fa fa-calendar" aria-hidden="true" style="color: black;"></i></span>
                                </div>
                            </div>
                        </div>
                        --}}
                        <h4 class="form-section" style="color: #69aa46;">Archivos</h4>
                        <div class="form-group row">
                            <div class="col-xs-12 table-responsive">
                                <table class="table table-striped table-bordered table-hover " id="tabla_info_archivos_existentes">
                                    <thead>
                                        <tr>
                                            <th width="50%">Nombre del Archivo</th>
                                            <th width="20%">Tipo</th>
                                            <th width="10%">Extensión</th>
                                            <th width="10%">Peso</th>
                                            <th width="10%"><i class="fa fa-cog"></i></th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                    @forelse ($vistoBueno->getArchivos as $archivo)
                                        <tr>
                                            <td>{{ $archivo->nombre }}</td>
                                            <td>@if( $archivo->getTipoArchivo ){{ $archivo->getTipoArchivo->nombre }}@endif</td>
                                            <td>{{ $archivo->extension }}</td>
                                            <td>{{ pesoArchivoEnMB($archivo->peso) }}</td>
                                            <td>
                                                <a class="btn btn-success btn-xs" @if($archivo->cargado == 0) href="{{ asset( $archivo->ubicacion ) }}" @else href="http://{{ $archivo->ubicacion }}" @endif
                                                title="Ver Archivo" target="_blank" ><i class="fa fa-eye"></i></a>
                                                <label>
                                                    <input type="checkbox" name="delete_list[]" value="{{$archivo->id}}"> Eliminar
                                                </label>
                                            </td>
                                        </tr>
                                    @empty

                                    @endforelse
                                    </tbody>
                                </table>
                            </div>
                        </div>

                        <div class="form-group row">
                            <div class="input-group col-sm-10 col-sm-offset-1">
                                <span class="input-group-addon" id="inputGroupFileAddon01"><strong>Seleccione Documento</strong></span>
                                <input type="file" class="form-control" accept=".pdf" name="archivo" id="archivo" aria-describedby="inputGroupFileAddon01" onchange="validarArchivo();">
                            </div>
                        </div>

                        <div class="form-group row">
                            <div id="div_archivo" class="col-xs-12 table-responsive" style="display: none;">
                                <table class="table table-striped table-bordered table-hover " id="tabla_info_archivo">
                                    <thead>
                                        <tr>
                                            <th width="40%">Nombre</th>
                                            <th width="20%">Peso</th>
                                            <th width="40%">Tipo</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                    </tbody>
                                </table>
                            </div>
                        </div>

                        <input type="hidden" name="_token" value="{{ csrf_token() }}">
                        <input type="hidden" name="_id" value="{{ $vistoBueno->id }}">
                    </div>  
                </div>{{-- /modal-body --}}

                
                <div class="modal-footer form-actions right ">
                    <button type="button" title="Cancelar" class="btn btn-default" data-dismiss="modal">Cancelar</button>
                    <button type="submit" title="Editar Memo Para Visto Bueno" class="btn btn-success" id="botonGuardar"><i class="far fa-save" style="color: black;"></i> Editar Memo</button>
                </div> {{-- /modal-footer --}}
            </form>
          	
		</div>
	</div>
</div>

 <script type="text/javascript">
    var selectTipoArchivo = '<select name="tipo_archivo" id="tipo_archivo" class="col-xs-12" required>';
        selectTipoArchivo += '<option value="">Seleccione</option>';
        @foreach ( $tiposArchivo as $tipoArchivo)
            selectTipoArchivo += '<option value="{{ $tipoArchivo->id }}">{{ $tipoArchivo->nombre }}</option>';
        @endforeach
        selectTipoArchivo += '</select>';
    var tablaItems = $('#tabla_item_presupuestario').DataTable({
        "language": {
            "emptyTable": "No hay datos disponibles en la tabla",
        },
        "paging":   false,
        "ordering": false,
        "info":     false,
        "searching": false
    });
 
    var tablaArchivo = $('#tabla_info_archivo').DataTable({
         "language": {
            "emptyTable": "No hay datos disponibles en la tabla",
        },
        "paging":   false,
        "ordering": false,
        "info":     false,
        "searching": false
    });

 	$(document).ready(function(){
         
        $('#fecha_solicitud').datepicker({
            format: 'dd/mm/yyyy',
            //endDate: new Date(),
            autoclose: true,
            language: 'es'
        });

        $('#fecha_informe').datepicker({
            format: 'dd/mm/yyyy',
            //endDate: new Date(),
            autoclose: true,
            language: 'es'
        });

        $(".select2").select2();

        $("#form").validate({
            highlight: function(element) {
                $(element).closest('.form-validate').removeClass('has-success');
                $(element).closest('.form-validate').addClass('has-error');
            },
            unhighlight: function(element) {
                $(element).closest('.form-validate').removeClass('has-error');
                $(element).closest('.form-validate').addClass('has-success');
            },
            errorElement: 'span',
            errorClass: 'help-block',
            errorPlacement: function (error, element) {
                console.log(element);
                if (element.parent('.input-group').length) {
                    error.insertAfter(element.parent());
                } else {
                    error.insertAfter(element);
                }
            },
            rules: {
            },
            messages: {
            },

            //para enviar el formulario por ajax
            submitHandler: function(form) {
                esperandoGuardar();

                let formData = new FormData(form);
                $.ajax({
                    url: form.action,
                    type: form.method,
                    //data: $(form).serialize(),
                    data: formData,
                    processData: false,
                    contentType: false,
                    success: function(respuesta) {
                        console.log(respuesta);
                        if ( respuesta.estadoMemo == 'error' ) {
                            toastr.error('No es posible realizar la acción'+'<br><strong>Error: '+ respuesta.mensajeMemo +'</strong>', 'Atención', optionsToastr);
                        } else if ( respuesta.estadoMemo == 'success') {
                            
                            toastr.success(respuesta.mensajeMemo, 'Atención', optionsToastr);
                            // Funcion que utiliza la respuesta para actualizar la tabla principal
                            actualizaElementoTabla(respuesta);
                            if ( respuesta.estadoArchivo == 'error' ) {
                                toastr.error('Archivo: No es posible realizar la acción'+'<br><strong>Error: '+ respuesta.mensajeArchivo +'</strong>', 'Atención', optionsToastr);
                            } else if ( respuesta.estadoArchivo == 'succes') {
                                toastr.success(respuesta.mensajeArchivo, 'Atención', optionsToastr);
                            }

                            $("#modalEditarMemo").modal("hide");
                            
                        }
                        
                    }            
                }).fail( function(respuesta) {//fail ajax
                    if ( respuesta.status == 400 ) {
                        mostrarErroresValidator(respuesta);
                    } else if ( respuesta.status == 500 ) {
                        toastr.error('No es posible realizar la acción, error en el servidor', 'Atención', optionsToastr);
                    } else {
                        toastr.error('No es posible realizar la acción', 'Atención', optionsToastr);
                    }
                    
                })
                .always(function() {
                    listoGuardar();
                });//ajax
                
            }

        });
  
 	});

    function fechaDocumento(id_fecha) {
        $('#' + id_fecha).datepicker("show");
    }

    function esperandoGuardar() {
        $('.page-header-fixed *').css('cursor', 'wait');
        $("#botonGuardar").attr("disabled", true);
    }

    function listoGuardar() {
        $('.page-header-fixed *').css('cursor', '');
        $("#botonGuardar").attr("disabled", false);
    }

    function mostrarErroresValidator(respuesta) {
        if (respuesta.responseJSON) {
            //console.log(respuesta.responseJSON);
            let htmlErrores = '';
            for (let k in respuesta.responseJSON) {
                //console.log(k, respuesta.responseJSON[k]);
                htmlErrores += '<li>' + respuesta.responseJSON[k] + '</li>';
            }

            $('#ulErrores').html(htmlErrores);
            $('#divErrores').css('display', '');
            toastr.error('No es posible realizar la acción' + '<br>' + 'Errores:<br> <ul>' + htmlErrores + '</ul>', 'Atención', optionsToastr);
        }
    }

    function actualizaElementoTabla(respuesta) {

        let botones = '<td ><div class="btn-group">';
        botones += '<button class="btn btn-info btn-xs " title="Registrar" onclick="registrarVistoBueno(' + respuesta.id + ');">';
        botones += '<i class="fas fa-file-signature fa-lg"></i></button>';
        botones += '<a class="btn btn-danger btn-xs " title="Ver Memo Solicitud Visto Bueno" target="_blank" href="{{ asset('referente_tecnico/generar/pdf_solicitud_visto_bueno/') }}/'+ respuesta.id + '" >';
        botones += '<i class="fas fa-file-pdf fa-lg"></i></a>';
        botones += '<button class="btn btn-warning btn-xs " title="Editar Memo" onclick="editarMemo(' + respuesta.id + ');">';
        botones += '<i class="fas fa-edit fa-lg"></i></button>';
        botones += '</div></td>';

        tablaPrincipal.row('#tr_' + respuesta.id).data([
            respuesta.fechaSolicitud,
            respuesta.memo,
            respuesta.responsable,
            respuesta.referenteTecnico,
            botones
        ]).draw();

    }

    function validarArchivo() {
        // console.log("validando archivo");
        let archivo = $('#archivo').val().replace(/.*(\/|\\)/, ''); //val();
        let extension = archivo.substring(archivo.lastIndexOf('.'));
        if (archivo == '') {
            $('#div_archivo').css('display', 'none');
            tablaArchivo.clear().draw(); // se remueven todas las filas de la tabla
            return false;
        } else {
            if (extension != '.pdf') {
                toastr.error('El archivo no tiene la extension correcta: ' + '<strong>PDF</strong>', 'Atención', optionsToastr);
                $('#archivo').val('');
                $('#div_archivo').css('display', 'none');
                tablaArchivo.clear().draw(); // se remueven todas las filas de la tabla
                return false;
            } else {
                mostrarInfoArchivo(archivo);
                return true;
            }
        }
    }

    function mostrarInfoArchivo(archivo) {
        let nombreArchivo = archivo.substring(0, archivo.lastIndexOf('.'));
        let tamañoArchivo = $('#archivo')[0].files[0].size;

        tablaArchivo.row.add([
            nombreArchivo,
            tamañoArchivo + " bits",
            selectTipoArchivo
        ]).draw(false);

        $('#div_archivo').css('display', '');
    }

    
 </script>