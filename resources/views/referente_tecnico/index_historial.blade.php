@extends('master')

@section('title', 'Registro Recepción Memo Visto Bueno')

@push('css-head')
<!-- BEGIN PAGE LEVEL STYLES -->
<link rel="stylesheet" type="text/css" href="{{asset('assets/plugins/select2/select2_conquer.css')}}"/>
<link rel="stylesheet" href="{{asset('assets/plugins/data-tables/DT_bootstrap.css')}}"/>
<!-- END PAGE LEVEL STYLES -->

@endpush

@section('content')

<!-- BEGIN CONTENT -->

<div class="page-content-wrapper">
	<div class="page-content-wrapper">
		<div class="page-content">

			<!-- BEGIN PAGE CONTENT-->
			<div class="row">
				<div class="col-md-12">
					<!-- BEGIN EXAMPLE TABLE PORTLET-->
					<div class="portlet">
						<div class="portlet-title">
							<div class="caption">
								<i class="fas fa-id-card-alt"></i> <i class="fas fa-tasks"></i><i class="fas fa-archive"></i> Historial Vistos Buenos
							</div>
						</div>
						<div class="portlet-body">
                            {{-- 
                            <form action="{{url('archivos_acepta/carga_archivo')}}" method="post" enctype="multipart/form-data" id="form_carga_archivo" class="form-horizontal">
                                <h4 class="form-section" style="color: #69aa46;">Documento en XLS</h4>
                                <div class="row">
                                    <div class="input-group col-sm-6 col-sm-offset-1">
                                        <span class="input-group-addon" id="inputGroupFileAddon01"><strong>Seleccione Documento</strong></span>
                                        <input type="file" class="form-control" accept=".xls" name="archivo" id="archivo" aria-describedby="inputGroupFileAddon01">
                                    </div>
                                    
                                </div>
                                <div class="row" style="margin-top:5pt;">
                                    <div class="btn-group col-md-offset-6 ">
                                        <button type="button" id="botonCargaArchivo" title="Cargar Archivo" class="btn btn-success" onclick="cargarArchivo();">
                                        Cargar Archivo <i class="fa fa-plus"></i></button>
                                    </div>
                                </div>
                                
                            </form>
                            --}}
                            <div class="table-toolbar">
                                    
                            </div>
                            <div class="row">
                                <div class="col-md-12">
                                    <div class="panel">
                                    {{-- 
                                        <div class="panel-heading">
                                            <div class="caption">
                                                <strong style="color: #69aa46;">Últimos Documentos Ingresados</strong>
                                            </div>
                                        </div>
                                        --}}
                                        <div class="panel-body">
                                            <div class="table-responsive">
                                                <table class="table table-striped table-bordered table-hover table-full-width" id="tabla_principal">
                                                    <thead>
                                                    <tr>
                                                        <th >Fecha Solicitud</th>
                                                        <th >N° Memo</th>
                                                        <th >Responsable</th>
                                                        <th>Fecha Respuesta</th>
                                                        <th>N° Memo Respuesta</th>
                                                        <th >Referente Técnico</th>
                                                        <th class="text-center">
                                                            <i class="fa fa-cog"></i>
                                                        </th>
                                                    </tr>
                                                    </thead>
                                                    <tbody>
                                                    @forelse($vistosBuenos as $vistoBueno)
                                                        <tr id="tr_{{ $vistoBueno->id }}">
                                                            <td>{{ fecha_dmY($vistoBueno->fecha_solicitud) }}</td>
                                                            <td>{{ $vistoBueno->memo }}</td>
                                                            <td>{{ $vistoBueno->getDigitador->name }}</td>
                                                            <td>{{ fecha_dmY($vistoBueno->fecha_memo_respuesta) }}</td>
                                                            <td>{{ $vistoBueno->memo_respuesta }}</td>
                                                            <td>
                                                                @if ($vistoBueno->getReferenteTecnico)
                                                                    {{ $vistoBueno->getReferenteTecnico->nombre.' '.$vistoBueno->getReferenteTecnico->responsable }}
                                                                @endif
                                                            </td>
                                                            <td>
                                                                <div class="btn-group">
                                                                    @permission(['ver-memo-solicitud-visto-bueno'])
                                                                    <a class="btn btn-danger btn-xs " title="Ver Memo Solicitud Visto Bueno" target="_blank" href="{{ asset('referente_tecnico/generar/pdf_solicitud_visto_bueno/'.$vistoBueno->id) }}">
                                                                        <i class="fas fa-file-pdf fa-lg"></i>
                                                                    </a>
                                                                    @endpermission
                                                                    
                                                                    <button class="btn btn-success btn-xs " title="Ver Memo" onclick="verVistoBueno({{ $vistoBueno->id }});">
                                                                        <i class="fa fa-eye fa-lg"></i>
                                                                    </button>

                                                                    @permission(['editar-registro-visto-bueno'])
                                                                    <button class="btn btn-warning btn-xs " title="Editar Memo" onclick="editarVistoBueno({{ $vistoBueno->id }});">
                                                                        <i class="fas fa-edit fa-lg"></i>
                                                                    </button>
                                                                    @endpermission

                                                                    @permission(['ver-memo-solicitud-visto-bueno'])
                                                                    <a class="btn btn-danger btn-xs " title="Ver Memo Visto Bueno" target="_blank" href="{{ asset('referente_tecnico/generar/pdf_visto_bueno/'.$vistoBueno->id) }}">
                                                                        <i class="fas fa-file-pdf fa-lg"></i>
                                                                    </a>
                                                                    @endpermission
                                                                </div>
                                                            </td>
                                                        </tr>
                                                    @empty

                                                    @endforelse
                                                    </tbody>
                                                </table>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
						</div>
					</div>
					<!-- END EXAMPLE TABLE PORTLET-->
				</div>
			</div>
			<!-- END PAGE CONTENT-->
			
			
			
			
		</div> <!--/div class="page-content" -->
	</div> <!--/div class="page-content-wrapper" -->
</div><!--/div class="page-content-wrapper" -->
<!-- END CONTENT -->
<div id="modal"></div>
<div id="modal2"></div>
@endsection

@push('script-footer')
<!-- BEGIN PAGE LEVEL PLUGINS -->
<script type="text/javascript" src="{{ asset('assets/plugins/select2/select2.min.js') }} "></script>
{{--<script type="text/javascript" src="{{ asset('assets/plugins/data-tables/jquery.dataTables.min.js') }}"></script>--}}
<script type="text/javascript" src="{{ asset('datatables/jquery.dataTables.min.js') }}"></script>
<script type="text/javascript" src="{{ asset('assets/plugins/data-tables/DT_bootstrap.js') }}"></script>

<link rel="stylesheet" type="text/css" href="{{asset('assets/plugins/bootstrap-datepicker/css/datepicker.css')}}"/>

<script type="text/javascript" src="{{asset('assets/plugins/bootstrap-datepicker/js/bootstrap-datepicker.js')}}"></script>
<script type="text/javascript" src="{{asset('assets/plugins/bootstrap-datetimepicker/js/bootstrap-datetimepicker.js')}}"></script>


<!-- END PAGE LEVEL PLUGINS -->
<!-- BEGIN PAGE LEVEL SCRIPTS -->
<script src="{{ asset('assets/scripts/app.js') }} "></script>

<script type="text/javascript">
    let tablaPrincipal = $('#tabla_principal').DataTable({
        "language": {
            "emptyTable":     "No hay datos disponibles en la tabla",
            "info":           "Mostrando registros del _START_ al _END_ de un total de _TOTAL_ registros",
            "infoEmpty":      "Showing 0 to 0 of 0 entries",
            "infoFiltered":   "(filtrado de un total de _MAX_ registros)",
            "infoPostFix":    "",
            "thousands":      ",",
            "lengthMenu":     "Mostrar _MENU_ registros",
            "loadingRecords": "Cargando...",
            "processing":     "Procesando...",
            "search":         "Buscar: ",
            "zeroRecords":    "No matching records found",
            "paginate": {
                "first":      "Primero",
                "last":       "Último",
                "next":       "Siguiente",
                "previous":   "Anterior"
            },
            "aria": {
                "sortAscending":  ": Activar para ordenar la columna de manera ascendente",
                "sortDescending": ": Activar para ordenar la columna de manera descendente"
            }
        },
        "lengthMenu": [
            [10, 15, 20, -1],
            [10, 15, 20, "Todos"] // change per page values here
        ],
        // set the initial value
        "displayLength": 10,
        "paging":   true,
        "ordering": true,
        "info":     true,
        "searching": true
    });

    jQuery('#tabla_principal_filter input').addClass("form-control input-large"); // modify table search input
    jQuery('#tabla_principal_wrapper .dataTables_length select').addClass("form-control input-small"); // modify table per page dropdown
    jQuery('#tabla_principal_wrapper .dataTables_length select').select2(); // initialize select2 dropdown

    $('#tabla_principal_column_toggler input[type="checkbox"]').change(function() {
        /* Get the DataTables object again - this is not a recreation, just a get of the object */
        var iCol = parseInt($(this).attr("data-column"));
        var bVis = oTable.fnSettings().aoColumns[iCol].bVisible;
        oTable.fnSetColumnVis(iCol, (bVis ? false : true));
    });

	$(document).ready(function() {
		App.init(); // initlayout and core plugins

   		$("#referente-tecnico-opciones").addClass( "active" );
		$("#historial-memos-li").addClass( "active" );
		$("#referente-tecnico-opciones-a").append( '<span class="selected"></span>' );

	});

    function verVistoBueno(id)
    {
        $.get( '{{ url("referente_tecnico/modal/ver/memo") }}/' + id, function( data ) {
            $( "#modal" ).html( data );
            $( "#modalVer" ).modal();
        });
    }

    function editarVistoBueno(id)
    {
        $.get( '{{ url("referente_tecnico/modal/editar_visto_bueno") }}/' + id, function( data ) {
            $( "#modal" ).html( data );
            $( "#modalEditarVistoBueno" ).modal();
        });
    } 
</script>

@endpush