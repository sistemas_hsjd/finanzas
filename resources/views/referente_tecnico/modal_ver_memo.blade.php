<link href="{{ asset('sistema/css/modales.css') }} " rel="stylesheet" type="text/css"/>
<style>
#tabla_documentos thead tr th {
    font-size: 11px;
    font-weight: 600;
}
</style>
<div class="modal fade" id="modalVer" role="dialog" aria-labelledby="myModalLabel3" aria-hidden="true">
	<div class="modal-dialog" style="width:1300px;">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" title="Cerrar" class="close" data-dismiss="modal" aria-hidden="true"></button>
				<h4 class="modal-title" ><strong><i class="fas fa-file-signature fa-lg"></i> <i class="fas fa-tasks"></i>&nbsp;&nbsp;Ver Memo Visto Bueno</strong></h4>
            </div>
          
            <div class="modal-body">
                <div class="form-group row" style="margin-bottom: 0px;">
                    <div class="col-xs-12">	
                        <h4 class="headers-view">
                            Documentos
                        </h4>
                        <hr>
                    </div>
                </div>

                <div class="form-group row">
                    <div class="col-xs-12 table-responsive">
                        <table class="table table-striped table-bordered table-hover " id="tabla_documentos">
                            <thead>
                                <tr>
                                    <th width="8%">Rut Proveedor</th>
                                    <th width="25%">Nombre Proveedor</th>
                                    <th width="7%" class="text-center">N° Documento</th>
                                    <th width="10%">N° Documento Compra</th>
                                    <th width="7%" class="text-center">Fecha</th>
                                    <th width="10%">Licitación</th>
                                    <th width="10%">Total</th>
                                    <th width="23%" class="text-center">Nota</th>
                                </tr>
                            </thead>
                            <tbody>
                                @forelse($vistoBueno->getDocumentos as $documento)
                                    <tr>
                                        <td>{{ $documento->getProveedor->formatRut() }}</td>
                                        <td>{{ $documento->getProveedor->nombre }}</td>
                                        <td class="text-center">{{ $documento->numero_documento }}</td>
                                        <td>{{ $documento->documento_compra }}</td>
                                        <td class="text-center">{{ fecha_dmY($documento->fecha_documento) }}</td>
                                        <td>{{ $documento->licitacion }}</td>
                                        <td class="text-right">$ {{ formatoMiles($documento->total_documento) }}</td>
                                        <td>{!! str_replace(PHP_EOL,"<br>", $documento->nota_visto_bueno) !!}</td>
                                    </tr>
                                @empty

                                @endforelse
                            </tbody>
                        </table>
                    </div>
                </div>

                <div class="form-group row">
                    <div class="col-xs-12">	
                        <h4 class="headers-view">
                            Visto Bueno
                        </h4>
                        <hr>
                        <div class="form-group">
                            <label class="col-sm-2 control-label font-bold">Fecha Solicitud</label>
                            <label class="col-sm-4 control-label ">{{ fecha_dmY($vistoBueno->fecha_solicitud) }}</label>
                            <label class="col-sm-2 control-label font-bold">Fecha Respuesta</label>
                            <label class="col-sm-4 control-label ">{{ fecha_dmY($vistoBueno->fecha_memo_respuesta) }}</label>
                        </div>
                    </div>
                </div>

                <div class="form-group row">
                    <div class="col-xs-12">
                        <label class="col-sm-2 control-label font-bold">N° Memo</label>
                        <label class="col-sm-4 control-label ">{{ $vistoBueno->memo }}</label>
                        <label class="col-sm-3 control-label font-bold">N° Memo Respuesta</label>
                        <label class="col-sm-3 control-label ">{{ $vistoBueno->memo_respuesta }}</label>
                    </div>
                </div>

                <div class="form-group row">
                    <div class="col-xs-12">
                        <label class="col-sm-3 control-label font-bold">Responsable Solicitud</label>
                        <label class="col-sm-3 control-label ">{{ $vistoBueno->getDigitador->name }}</label>
                        <label class="col-sm-3 control-label font-bold">Responsable Respuesta</label>
                        <label class="col-sm-3 control-label ">{{ $vistoBueno->getRegistrador->name }}</label>
                    </div>
                </div>

                <div class="form-group row">
                    <div class="col-xs-12">
                        <label class="col-sm-3 control-label font-bold">Referente Técnico</label>
                        <label class="col-sm-9 control-label ">{{ $vistoBueno->getReferenteTecnico->nombre.' '.$vistoBueno->getReferenteTecnico->responsable }}</label>
                    </div>
                </div>

                <div class="form-group row">
                    <div class="col-xs-12">
                        <label class="col-sm-3 control-label font-bold">N° Informe</label>
                        <label class="col-sm-3 control-label ">{{ $vistoBueno->informe }}</label>
                        <label class="col-sm-3 control-label font-bold">Fecha Informe</label>
                        <label class="col-sm-3 control-label ">{{ fecha_dmY($vistoBueno->fecha_informe) }}</label>
                    </div>
                </div>
                <div class="form-group row">
                    <div class="col-xs-12">
                        <label class="col-sm-1 control-label font-bold">Observación</label>
                        <label class="col-sm-10 control-label ">{!! str_replace(PHP_EOL,"<br>", $vistoBueno->observacion_memo_respuesta) !!}</label>
                    </div>
                </div>

                <div class="form-group row">
                    <div class="col-xs-12">	
                        <h4 class="headers-view">
                            Archivos
                        </h4>
                        <hr>
                        <div class="form-group">
                            <div class="col-xs-12 table-responsive">
                                <table class="table table-striped table-bordered table-hover " id="tabla_info_archivo">
                                    <thead>
                                        <tr>
                                            <th width="40%">Nombre del Archivo</th>
                                            <th width="30%">Tipo</th>
                                            <th width="10%">Extensión</th>
                                            <th width="10%">Peso</th>
                                            <th width="10%"><i class="fa fa-cog" ></i></th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        @forelse($vistoBueno->getArchivos as $archivo)
                                            <tr>
                                                <td>{{ $archivo->nombre }}</td>
                                                <td>@if( $archivo->getTipoArchivo ){{ $archivo->getTipoArchivo->nombre }}@endif</td>
                                                <td>{{ $archivo->extension }}</td>
                                                <td>{{ pesoArchivoEnMB($archivo->peso) }}</td>
                                                <td>
                                                    <a class="btn btn-success btn-xs" @if($archivo->cargado == 0) href="{{ asset( $archivo->ubicacion ) }}" @else href="http://{{ $archivo->ubicacion }}" @endif
                                                    title="Ver Archivo" target="_blank" ><i class="fa fa-eye"></i></a>
                                                </td>
                                            </tr>
                                        @empty

                                        @endforelse
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>

            </div>{{-- /modal-body --}}

            {{--  
            <div class="modal-footer form-actions">
                <div class="btn-group">
                <button type="button" class="btn btn-info" title="Cerrar" data-dismiss="modal" aria-hidden="true">Cerrar</button>
                </div>
            </div>
            --}}
          	
		</div>
	</div>
</div>

<script type="text/javascript">
    let tablaArchivo = $('#tabla_info_archivo').DataTable({
        "language": {
            "emptyTable": "No hay datos disponibles en la tabla",
        },
        "paging":   false,
        "ordering": false,
        "info":     false,
        "searching": false
    });

 	$(document).ready(function(){
         
  
 	});

    
 </script>