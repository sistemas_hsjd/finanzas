@extends('master')

@section('title', 'Nube Libro de Compras')

@push('css-head')
<!-- BEGIN PAGE LEVEL STYLES -->
<link rel="stylesheet" type="text/css" href="{{asset('assets/plugins/select2/select2_conquer.css')}}"/>
<link rel="stylesheet" href="{{asset('assets/plugins/data-tables/DT_bootstrap.css')}}"/>
<!-- END PAGE LEVEL STYLES -->

@endpush

@section('content')

<!-- BEGIN CONTENT -->

<div class="page-content-wrapper">
	<div class="page-content-wrapper">
		<div class="page-content">

			<!-- BEGIN PAGE CONTENT-->
			<div class="row">
				<div class="col-md-12">
					<!-- BEGIN EXAMPLE TABLE PORTLET-->
					<div class="portlet">
						<div class="portlet-title">
							<div class="caption">
                                <i class="fas fa-cloud"></i> <i class="fas fa-book"></i> Nube Libro de Compras
							</div>
						</div>
						<div class="portlet-body">

                            <form action="{{url('nube/libro_de_compras')}}" method="POST" enctype="multipart/form-data" id="form" class="form-horizontal">
                                <h4 class="form-section" style="color: #69aa46;">Documento en XLSX</h4>
                                <div class="row">
                                    <div class="input-group col-sm-6 col-sm-offset-1">
                                        <span class="input-group-addon" id="inputGroupFileAddon01"><strong>Seleccione Documento</strong></span>
                                        <input type="file" class="form-control" accept=".xlsx" name="archivo" id="archivo" aria-describedby="inputGroupFileAddon01" required>
                                    </div>
                                    
                                </div>
                                <div class="row" style="margin-top:5pt;">
                                    <div class="btn-group col-md-offset-6 ">
                                        <button type="button" id="botonCargaArchivo" title="Cargar Archivo" class="btn btn-success" onclick="cargarArchivo();">
                                        Subir Archivo <i class="fas fa-cloud-upload-alt"></i></button>
                                    </div>
                                </div>
                                
                            </form>
                            <div class="table-toolbar">
                                    
                            </div>
                            <div class="row">
                                <div class="col-md-12">
                                    <div class="panel panel-default">
                                        <div class="panel-heading">
                                            <div class="caption">
                                                <strong style="color: #69aa46;">Libros de Compras cargados</strong>
                                            </div>
                                        </div>
                                        <div class="panel-body">
                                            <div class="table-responsive">
                                                <table class="table table-striped table-bordered table-hover table-full-width" id="sample_2">
                                                    <thead>
                                                    <tr>
                                                        <th>Fecha Ingreso</th>
                                                        <th>Ingresado por</th>
                                                        <th class="hidden-xs">Nombre del archivo</th>
                                                        <th class="hidden-xs">Ubicación</th>
                                                        <th class="hidden-xs">Extensión</th>
                                                        <th class="hidden-xs">Peso</th>
                                                        <th class="hidden-xs">
                                                            <i class="fa fa-cog"></i>
                                                        </th>
                                                    </tr>
                                                    </thead>
                                                    <tbody>
                                                    @forelse($nubeLibrosCompras as $libro)
                                                    <tr>
                                                        <td>{{ fecha_dmY($libro->created_at) }}</td>
                                                        <td>{{ $libro->getUser->name }}</td>
                                                        <td>{{ $libro->nombre_original }}</td>
                                                        <td>{{ $libro->ubicacion }}</td>
                                                        <td>{{ $libro->extension }}</td>
                                                        <td>{{ pesoArchivoEnMB($libro->peso) }}</td>
                                                        <td>
                                                            <div class="btn-group">
                                                                {{-- @permission(['ver-nube-libro-compra']) --}}
                                                                <a class="btn btn-success btn-xs" href="{{ asset( $libro->ubicacion ) }}" target="_blank" title="Descargar Archivo">
                                                                    <i class="fas fa-cloud-download-alt"></i>
                                                                </a>
                                                                {{-- @endpermission --}}

                                                                {{-- @permission(['eliminar-nube-libro-compra']) --}}
                                                                <button class="btn btn-danger btn-xs" title="Eliminar" onclick="eliminar({{ $libro->id }});">
                                                                    <i class="fas fa-trash"></i>
                                                                </button>
                                                                {{-- @endpermission --}}
                                                            </div>
                                                        </td>
                                                    </tr>
                                                    @empty

                                                    @endforelse
                                                    </tbody>
                                                </table>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
						</div>
					</div>
					<!-- END EXAMPLE TABLE PORTLET-->
				</div>
			</div>
			<!-- END PAGE CONTENT-->
			
			
			
			
		</div> <!--/div class="page-content" -->
	</div> <!--/div class="page-content-wrapper" -->
</div><!--/div class="page-content-wrapper" -->
<!-- END CONTENT -->
<div id="modal"></div>
@endsection

@push('script-footer')
<!-- BEGIN PAGE LEVEL PLUGINS -->
<script type="text/javascript" src="{{ asset('assets/plugins/select2/select2.min.js') }} "></script>
{{--<script type="text/javascript" src="{{ asset('assets/plugins/data-tables/jquery.dataTables.min.js') }}"></script>--}}
<script type="text/javascript" src="{{ asset('datatables/jquery.dataTables.min.js') }}"></script>
<script type="text/javascript" src="{{ asset('assets/plugins/data-tables/DT_bootstrap.js') }}"></script>


<!-- END PAGE LEVEL PLUGINS -->
<!-- BEGIN PAGE LEVEL SCRIPTS -->
<script src="{{ asset('assets/scripts/app.js') }} "></script>
<script src="{{ asset('assets/scripts/table-advanced.js') }} "></script>


<script type="text/javascript">
	$(document).ready(function() {
		App.init(); // initlayout and core plugins
		TableAdvanced.init();

   		$("#nube").addClass( "active" );
        $("#nube-a").append( '<span class="selected"></span>' );

        $("#nube-libro-de-compras-li").addClass( "active" );

		

	});

    function validarArchivo()
    {
        let archivo = $('#archivo').val().replace(/.*(\/|\\)/, ''); //val();
        let extension = archivo.substring(archivo.lastIndexOf('.'));
        if ( archivo == '' ) {
            toastr.warning('No se ha seleccionado documento', 'Atención', optionsToastr);
            listaCargaArchivo();
            return false;
        } else {
            if ( extension != '.xlsx' ) {
                toastr.warning('El archivo no tiene la extension correcta: '+'<strong>XLSX</strong>', 'Atención', optionsToastr);
                $('#archivo').val('');
                listaCargaArchivo();
                return false;
            } else {
                return true;
            }
        }
    }

    function cargarArchivo() 
    {
        esperandoCargaArchivo();

        if ( validarArchivo() ) {
            let formData = new FormData($('#form')[0]);

            $.ajaxSetup({
                headers: {  'X-CSRF-TOKEN': '{{ csrf_token() }}'}
            });

            $.ajax({
                data: formData, 
                url:   '{{url('nube/libro_de_compras')}}',
                type:  'post',
                processData: false,
                contentType: false,

                success: function(respuesta) {
                    if ( respuesta.estado == 'error' ) {
                        toastr.error('No es posible realizar la acción'+'<br><strong>Error: '+ respuesta.mensaje +'</strong>', 'Atención', optionsToastr);
                    } else if ( respuesta.estado == 'success') {
                        $('#archivo').val('');
                        toastr.success(respuesta.mensaje, 'Atención', optionsToastr);
                        nuevaFila(respuesta);
                    }
                }//succes
            }).fail( function(data) {
                toastr.error('No es posible realizar la acción', 'Atención', optionsToastr);
            })
            .always(function() {
                listaCargaArchivo();
            });//ajax
        }
        
    }

    function nuevaFila(respuesta)
    {
        let tabla = $('#sample_2').DataTable();

        tabla.row.add ([
            respuesta.fechaIngreso,
            respuesta.ingresadoPor,
            respuesta.nombreDelArchivo,
            respuesta.ubicacion,
            respuesta.extension,
            respuesta.peso,
            respuesta.boton
        ]).draw( false );
    }

    function esperandoCargaArchivo()
    {   
        $('.page-header-fixed *').css('cursor', 'wait');
        $("#botonCargaArchivo").attr("disabled",true);
    }

    function listaCargaArchivo()
    {
        $('.page-header-fixed *').css('cursor', '');
        $("#botonCargaArchivo").attr("disabled",false);
    }

    function eliminar(id)
    {
        $.get( '{{ url("nube/libro_de_compras/modal_eliminar") }}/' + id, function( data ) {
            $( "#modal" ).html( data );
            $( "#modalEliminar" ).modal();
        });
    }

</script>

@endpush