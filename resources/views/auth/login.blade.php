<!DOCTYPE html>
<!--
Template Name: Conquer - Responsive Admin Dashboard Template build with Twitter Bootstrap 3.0.3
Version: 1.5.2
Author: KeenThemes
Website: http://www.keenthemes.com/
Purchase: http://themeforest.net/item/conquer-responsive-admin-dashboard-template/3716838?ref=keenthemes
-->
<!--[if IE 8]> <html lang="en" class="ie8 no-js"> <![endif]-->
<!--[if IE 9]> <html lang="en" class="ie9 no-js"> <![endif]-->
<!--[if !IE]><!-->
<html lang="en" class="no-js">
<!--<![endif]-->
<!-- BEGIN HEAD -->

<!-- Mirrored from www.keenthemes.com/preview/conquer/login.html by HTTrack Website Copier/3.x [XR&CO'2013], Fri, 23 May 2014 03:10:02 GMT -->
<!-- Added by HTTrack --><meta http-equiv="content-type" content="text/html;charset=UTF-8" /><!-- /Added by HTTrack -->
<head>
<meta charset="utf-8"/>
<title>Finanzas | Login</title>
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta content="width=device-width, initial-scale=1.0" name="viewport"/>
<meta content="" name="description"/>
<meta content="" name="author"/>
<meta name="MobileOptimized" content="320">
<!-- BEGIN GLOBAL MANDATORY STYLES -->
<link href="{{asset('assets/plugins/font-awesome/css/font-awesome.min.css')}}" rel="stylesheet" type="text/css"/>
<link href="{{asset('assets/plugins/font-awesome/css/all.css')}}" rel="stylesheet" type="text/css"/>
<link href="{{asset('assets/plugins/bootstrap/css/bootstrap.min.css')}}" rel="stylesheet" type="text/css"/>
<!-- END PAGE LEVEL SCRIPTS -->
<!-- BEGIN THEME STYLES -->
<link href="{{asset('assets/css/style-conquer.css')}}" rel="stylesheet" type="text/css"/>
<link href="{{asset('assets/css/style.css')}}" rel="stylesheet" type="text/css"/>
<link href="{{asset('assets/css/pages/login.css')}}" rel="stylesheet" type="text/css"/>
<!-- END THEME STYLES -->
<link rel="shortcut icon" href="{{ asset('faviconA.ico') }}"/>
</head>
<!-- BEGIN BODY -->
<body class="login">
<!-- BEGIN LOGO -->
<div class="logo">
    {{-- 
        <h1 style="color: red;font-weight: bold !important;"><i class="fa fa-hospital-o" style="color: #69aa46;"></i> Hospital</h1>
        <h4 style="color: #478fca;font-weight: bold !important;">San Juan de Dios</h4>
        <h4 style="color: #69aa46;font-weight: bold !important;">Sistema de Finanzas Digital</h4>
    --}}
    <img src="{{asset('logo-finanzasA.png')}}" alt="" style="width:23%;"/>
    
</div>
<!-- END LOGO -->
<!-- BEGIN LOGIN -->
<div class="content">
    <form method="POST" action="{{ route('login') }}" aria-label="{{ __('Login') }}" class="login-form" autocomplete="off">
        <h4 class="form-title"> <i class="fa fa-pencil-square-o" style="color: #69aa46;" ></i> Ingrese su información</h4>
        <hr>
        @csrf

        <div class="form-group row">
        {{-- 
            <label for="rut" class="control-label">{{ __('Rut') }}</label>
            --}}

            <div class="col-md-12">
            {{--
                <input id="rut" type="rut" class="form-control{{ $errors->has('rut') ? ' is-invalid' : '' }}" name="rut" value="{{ old('rut') }}" required autofocus>
            --}}
                <div class="input-icon">
                    <i class="fa fa-user"></i>
                    <input class="form-control{{ $errors->has('rut') ? ' is-invalid' : '' }}" type="text" placeholder="Rut" name="rut" id="rut" value="{{ old('rut') }}" required autofocus/>
                </div>
                @if ($errors->has('rut'))
                    <span class="invalid-feedback" role="alert">
                        <strong>{{ $errors->first('rut') }}</strong>
                    </span>
                @endif
            </div>

        {{--
            
            <label for="email" class="col-sm-4 col-form-label text-md-right">{{ __('E-Mail Address') }}</label>

            <div class="col-md-6">
                <input id="email" type="email" class="form-control{{ $errors->has('email') ? ' is-invalid' : '' }}" name="email" value="{{ old('email') }}" required autofocus>

                @if ($errors->has('email'))
                    <span class="invalid-feedback" role="alert">
                        <strong>{{ $errors->first('email') }}</strong>
                    </span>
                @endif
            </div>
        --}}
        </div>

        <div class="form-group row">
        {{-- 
            <label for="password" class="col-md-4 col-form-label text-md-right">{{ __('Password') }}</label>
        --}}

            <div class="col-md-12">
            {{-- 
                <input id="password" type="password" class="form-control{{ $errors->has('password') ? ' is-invalid' : '' }}" name="password" required>
            --}}

                <div class="input-icon">
                    <i class="fa fa-lock"></i>
                    <input class="form-control{{ $errors->has('password') ? ' is-invalid' : '' }}" type="password" placeholder="Clave" name="password" id="password" required/>
                </div>
                @if ($errors->has('password'))
                    <span class="invalid-feedback" role="alert">
                        <strong>{{ $errors->first('password') }}</strong>
                    </span>
                @endif
            </div>
        </div>

        {{-- 
        <div class="form-group row">
            <div class="col-md-6 offset-md-4">
                <div class="form-check">
                    <input class="form-check-input" type="checkbox" name="remember" id="remember" {{ old('remember') ? 'checked' : '' }}>

                    <label class="form-check-label" for="remember">
                        {{ __('Remember Me') }}
                    </label>
                </div>
            </div>
        </div>
        --}}
        <div class="form-group row">
            <div class="col-md-12">
                <button type="submit" class="btn btn-primary col-md-4 col-md-offset-8">
                    <i class="fas fa-key"></i>
                    {{ __('Login') }}
                </button>

                {{-- 
                <a class="btn btn-link" href="{{ route('password.request') }}">
                    {{ __('Forgot Your Password?') }}
                </a>
                --}}
            </div>
        </div>
    </form>       
</div>

<div class="logo">
    <img src="{{asset('logo300x80.png')}}" alt="" style="width:15%;"/>
</div>

<div class="copyright">
     {{ date("Y") }} &copy; Hospital San Juan de Dios - Departamento de Informática.
</div>
<script src="{{asset('assets/plugins/jquery-1.10.2.min.js')}}" type="text/javascript"></script>
<script src="{{asset('assets/plugins/bootstrap/js/bootstrap.min.js')}}" type="text/javascript"></script>
<script src="{{asset('assets/plugins/jquery-slimscroll/jquery.slimscroll.min.js')}}" type="text/javascript"></script>
<script src="{{asset('assets/scripts/app.js')}}" type="text/javascript"></script>
<script>
jQuery(document).ready(function() {
  App.init();
});
</script>
</body>
</html>


