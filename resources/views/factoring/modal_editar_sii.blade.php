<link href="{{ asset('sistema/css/modales.css') }} " rel="stylesheet" type="text/css"/>

<div class="modal fade" id="modalEditar" role="dialog" aria-labelledby="myModalLabel3" aria-hidden="true">
	<div class="modal-dialog" style="width:1100px;">
		<div class="modal-content">
            <div class="modal-header">
				<button type="button" title="Cerrar" class="close" data-dismiss="modal" aria-hidden="true"></button>
				<h4 class="modal-title" ><strong><i class="fas fa-file-export"></i> <i class="fa fa-files-o" ></i>&nbsp;&nbsp;Editar Documento</strong></h4>
            </div>

            <form action="{{ asset('factoring/modal/editar_sii') }}" method="post" class="horizontal-form" id="form" autocomplete="off">
                <div class="modal-body">
                    <div class="form-body">
                        <div class="note note-danger" id="divErrores" style="display:none;">
                            <h4 class="block" style="margin-bottom:5 px;">Debe Completar los siguientes campos: </h4>
                            <ul id="ulErrores"></ul>
                        </div>

                        <div class="form-group row">
                            <label for="vendedor" class="col-sm-2 control-label label-form">Vendedor </label>
                            <div class="col-sm-4 form-validate">
                                <input type="text" class="form-control" id="vendedor" name="vendedor" value="{{ $documento->vendedor }}">
                            </div>

                            <label for="estado_cesion" class="col-sm-2 control-label label-form">Estado Cesión <span class="span-label">*</span></label>
                            <div class="col-sm-4 form-validate">
                                <input type="text" class="form-control" id="estado_cesion" required name="estado_cesion" value="{{ $documento->estado_cesion }}" >
                            </div>
                        </div>

                        <div class="form-group row">
                            <label for="deudor" class="col-sm-2 control-label label-form">Deudor <span class="span-label">*</span></label>
                            <div class="col-sm-4 form-validate">
                                <input type="text" class="form-control" required id="deudor" name="deudor" value="{{ $documento->deudor }}" >
                            </div>

                            <label for="mail_deudor" class="col-sm-2 control-label label-form">Mail Deudor <span class="span-label">*</span></label>
                            <div class="col-sm-4 form-validate">
                                <input type="text" class="form-control" required id="mail_deudor" name="mail_deudor" value="{{ $documento->mail_deudor }}" >
                            </div>
                        </div>

                        <div class="form-group row">
                            <label for="tipo_doc" class="col-sm-2 control-label label-form">Tipo Doc <span class="span-label">*</span></label>
                            <div class="col-sm-4 form-validate">
                                <input type="text" class="form-control" required id="tipo_doc" name="tipo_doc" value="{{ $documento->tipo_doc }}" >
                            </div>

                            <label for="nombre_doc" class="col-sm-2 control-label label-form">Nombre Doc<span class="span-label">*</span></label>
                            <div class="col-sm-4 form-validate">
                                <input type="text" class="form-control" required id="nombre_doc" name="nombre_doc" value="{{ $documento->nombre_doc }}" >
                            </div>
                        </div>

                        <div class="form-group row">
                            <label for="folio_doc" class="col-sm-2 control-label label-form">Folio Doc <span class="span-label">*</span></label>
                            <div class="col-sm-4 form-validate">
                                <input type="text" class="form-control" required id="folio_doc" name="folio_doc" value="{{ $documento->folio_doc }}" >
                            </div>

                            <label for="fecha_emision_dte" class="col-sm-2 control-label label-form">Fecha Emisión DTE <span class="span-label">*</span></label>
                            <div class="col-sm-4 form-validate">
                                <input type="text" class="form-control" required id="fecha_emision_dte" name="fecha_emision_dte" value="{{ fecha_dmY($documento->fecha_emis_dte) }}" >
                            </div>
                        </div>

                        <div class="form-group row">
                            <label for="monto_total" class="col-sm-2 control-label label-form">Monto Total <span class="span-label">*</span></label>
                            <div class="col-sm-4 form-validate">
                                <input type="text" class="form-control" required id="monto_total" name="monto_total" value="{{ $documento->mnt_total }}" >
                            </div>

                            <label for="cedente" class="col-sm-2 control-label label-form">Cedente <span class="span-label">*</span></label>
                            <div class="col-sm-4 form-validate">
                                <input type="text" class="form-control" required id="cedente" name="cedente" value="{{ $documento->cedente }}" >
                            </div>
                        </div>

                        <div class="form-group row">
                            <label for="rz_cedente" class="col-sm-2 control-label label-form">RZ Cedente <span class="span-label">*</span></label>
                            <div class="col-sm-4 form-validate">
                                <input type="text" class="form-control" required id="rz_cedente" name="rz_cedente" value="{{ $documento->rz_cedente }}" >
                            </div>

                            <label for="mail_cedente" class="col-sm-2 control-label label-form">Mail Cedente <span class="span-label">*</span></label>
                            <div class="col-sm-4 form-validate">
                                <input type="text" class="form-control" required id="mail_cedente" name="mail_cedente" value="{{ $documento->mail_cedente }}" >
                            </div>
                        </div>

                        <div class="form-group row">
                            <label for="cesionario" class="col-sm-2 control-label label-form">Cesionario <span class="span-label">*</span></label>
                            <div class="col-sm-4 form-validate">
                                <input type="text" class="form-control" required id="cesionario" name="cesionario" value="{{ $documento->cesionario }}" >
                            </div>

                            <label for="rz_cesionario" class="col-sm-2 control-label label-form">RZ Cesionario <span class="span-label">*</span></label>
                            <div class="col-sm-4 form-validate">
                                <input type="text" class="form-control" required id="rz_cesionario" name="rz_cesionario" value="{{ $documento->razon_cesionario }}" >
                            </div>
                        </div>

                        <div class="form-group row">
                            <label for="mail_cesionario" class="col-sm-2 control-label label-form">Mail Cesionario <span class="span-label">*</span></label>
                            <div class="col-sm-4 form-validate">
                                <input type="text" class="form-control" required id="mail_cesionario" name="mail_cesionario" value="{{ $documento->mail_cesionario }}" >
                            </div>

                            <label for="fecha_cesion" class="col-sm-2 control-label label-form">Fecha Cesión <span class="span-label">*</span></label>
                            <div class="col-sm-4 form-validate">
                                <input type="text" class="form-control" required id="fecha_cesion" name="fecha_cesion" value="{{ fecha_dmY_hm($documento->fecha_cesion) }}" >
                            </div>
                        </div>

                        <div class="form-group row">
                            <label for="monto_cesion" class="col-sm-2 control-label label-form">Monto Cesión <span class="span-label">*</span></label>
                            <div class="col-sm-4 form-validate">
                                <input type="text" class="form-control" required id="monto_cesion" name="monto_cesion" value="{{ $documento->mnt_cesion }}" >
                            </div>

                            <label for="fecha_vencimiento" class="col-sm-2 control-label label-form">Fecha Vencimiento <span class="span-label">*</span></label>
                            <div class="col-sm-4 form-validate">
                                <input type="text" class="form-control" required id="fecha_vencimiento" name="fecha_vencimiento" value="{{ fecha_dmY($documento->fecha_vencimiento) }}" >
                            </div>
                        </div>

                        <div class="form-group row">
                            <label for="pagado" class="col-sm-2 control-label label-form">Pagado </label>
                            <div class="col-sm-4 form-validate">
                            <div class="checkbox check" >
                                <label class="label_checkbox">
                                    <input type="checkbox" class="check_input" id="pagado" name="pagado" value="1" @if($documento->pagado == 1) checked @endif >
                                </label>
                                </div>
                            </div>
                        </div>

                    </div>
                </div>

                <div class="modal-footer form-actions right">
                    <input type="hidden" name="_token" value="{{ csrf_token() }}">
                    <input type="hidden" name="_id" value="{{ $documento->id }}">

                    <button type="button" title="Cancelar" class="btn btn-default" data-dismiss="modal">Cancelar</button>
                    <button type="submit" title="Editar Documento" class="btn btn-warning" id="botonGuardar"><i class="far fa-save" style="color:black;"></i> Editar</button>
                </div>
            </form>

        </div>
    </div>
</div>

<script src="{{asset('assets/plugins/jquery-rut/jquery.Rut.min.js')}}" type="text/javascript"></script>
<script>
var pagadoOriginal = {{ $documento->pagado }};
    $(document).ready(function() {

        $('#fecha_vencimiento').datetimepicker({
            format: 'DD/MM/YYYY',
            //endDate: new Date(),
            //"setDate": new Date(),
            //startDate: new Date(),
            //autoclose: true,
            locale:'es'
        });


        $('#fecha_emision_dte').datetimepicker({
            format: 'DD/MM/YYYY',
            //endDate: new Date(),
            //"setDate": new Date(),
            //startDate: new Date(),
            //autoclose: true,
            locale:'es'
        });

    
        $('#fecha_cesion').datetimepicker({
            format: 'DD/MM/YYYY HH:mm',
            //ignoreReadonly: true,
            //sideBySide: true,
            //defaultDate: null,
            //minDate : moment()
            locale:'es'
        });

        $("#form").validate({
            highlight: function(element) {
                $(element).closest('.form-validate').removeClass('has-success');
                $(element).closest('.form-validate').addClass('has-error');
            },
            unhighlight: function(element) {
                $(element).closest('.form-validate').removeClass('has-error');
                $(element).closest('.form-validate').addClass('has-success');
            },
            errorElement: 'span',
            errorClass: 'help-block',
            errorPlacement: function (error, element) {
                console.log(element);
                if (element.parent('.input-group').length) {
                    error.insertAfter(element.parent());
                } else {
                    error.insertAfter(element);
                }
            },
            rules: {
            },
            messages: {
            },

            //para enviar el formulario por ajax
            submitHandler: function(form) {
                esperandoEdicion();

                let formData = new FormData(form);
                /*
                formData.delete('rut');
                let rut = $('#rut').val().replace(/\./g, '');
                formData.append('rut',rut);
                */

                $.ajax({
                    url: form.action,
                    type: form.method,
                    //data: $(form).serialize(),
                    data: formData,
                    processData: false,
                    contentType: false,
                    success: function(respuesta) {
                        console.log(respuesta);

                        if ( respuesta.estado == 'error' ) {
                            toastr.error('No es posible realizar la acción'+'<br><strong>Error: '+ respuesta.mensaje +'</strong>', 'Atención', optionsToastr);
                        } else if ( respuesta.estado == 'success') {
                            
                            toastr.success(respuesta.mensaje, 'Atención', optionsToastr);
                            // Cambiar la informacion de la fila de la tabla
                            editarFilaTabla(respuesta);
                            $("#modalEditar").modal("hide");
                            
                        }
                        
                    }            
                }).fail( function(respuesta) {//fail ajax
                    if ( respuesta.status == 400 ) {
                        mostrarErroresValidator(respuesta);
                    } else if ( respuesta.status == 500 ) {
                        toastr.error('No es posible realizar la acción, error en el servidor', 'Atención', optionsToastr);
                    } else {
                        toastr.error('No es posible realizar la acción', 'Atención', optionsToastr);
                    }
                    
                })
                .always(function() {
                    listaEdicion();
                });//ajax
                
            }

        });

    });

    function esperandoEdicion()
    {   
        $('.page-header-fixed *').css('cursor', 'wait');
        $("#botonGuardar").attr("disabled",true);
    }

    function listaEdicion()
    {
        $('.page-header-fixed *').css('cursor', '');
        $("#botonGuardar").attr("disabled",false);
    }

    function mostrarErroresValidator(respuesta)
    {
        if ( respuesta.responseJSON ) {
            //console.log(respuesta.responseJSON);
            let htmlErrores = '';
            for (let k in respuesta.responseJSON) { 
                //console.log(k, respuesta.responseJSON[k]);
                htmlErrores += '<li>' + respuesta.responseJSON[k] + '</li>';
            }

            $('#ulErrores').html(htmlErrores);
            $('#divErrores').css('display','');
            toastr.error('No es posible realizar la acción'+'<br>'+'Errores:<br> <ul>'+htmlErrores+'</ul>', 'Atención', optionsToastr);
        }
    }

    function editarFilaTabla(respuesta)
    {
        // alert(tablaProveedores.row('#proveedor_'+ respuesta.idProveedor).data());
        console.log('pagadoOriginal = '+ pagadoOriginal);
        console.log('respuesta pagado = '+ respuesta.pagado);

        if ( pagadoOriginal == respuesta.pagado ) {

            let botones = '<td width="10%"><div class="btn-group">';
            botones += '<button class="btn btn-success btn-xs" title="Ver Documento" onclick="ver('+ respuesta.id +');">';
            botones += '<i class="fas fa-eye"></i></button>';
            botones += '<button class="btn btn-warning btn-xs" title="Editar Documento" onclick="editar('+ respuesta.id +');">';
            botones += '<i class="fas fa-edit"></i></button>';
            botones += '</div></td>';

            tablaPrincipal.row('#tr_'+ respuesta.id).data([
                respuesta.estado_cesion,
                respuesta.nombre_doc,
                respuesta.folio_doc,
                respuesta.fecha_emis_dte,
                respuesta.mnt_total,
                respuesta.cedente,
                respuesta.rz_cedente,
                respuesta.cesionario,
                respuesta.razon_cesionario,
                respuesta.fecha_cesion,
                respuesta.mnt_cesion,
                respuesta.fecha_vencimiento,
                botones
            ]).draw();

        } else {
            // se quita la fila
            $('#tr_' + respuesta.id).fadeOut(500, function(){
                tablaPrincipal.row('#tr_' + respuesta.id ).remove().draw();
            });
        }
        
    }

</script>