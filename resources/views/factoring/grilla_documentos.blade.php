@extends('master')

@section('title', 'Documentos con Factoring')

@push('css-head')
<!-- BEGIN PAGE LEVEL STYLES -->
<link rel="stylesheet" type="text/css" href="{{asset('assets/plugins/select2/select2_conquer.css')}}"/>
<link rel="stylesheet" href="{{asset('assets/plugins/data-tables/DT_bootstrap.css')}}"/>
<!-- END PAGE LEVEL STYLES -->

<style>
    .label-form {
        font-weight: bold;
        color: black;
        text-align: right;
    }
</style>

@endpush

@section('content')

<!-- BEGIN CONTENT -->

<div class="page-content-wrapper">
	<div class="page-content-wrapper">
		<div class="page-content">

			<!-- BEGIN PAGE CONTENT-->
			<div class="row">
				<div class="col-md-12">
					<!-- BEGIN EXAMPLE TABLE PORTLET-->
					<div class="portlet">
						<div class="portlet-title">
							<div class="caption">
								<i class="fas fa-file-export"></i> <i class="fa fa-files-o" ></i> Documentos con Factoring
							</div>
						</div>
						<div class="portlet-body">
                            <form action="{{url('factoring/grilla/filtrar')}}" method="post" enctype="multipart/form-data" id="filtros-docs" class="form-horizontal">
                                <h4 class="form-section" style="color: #69aa46;"><strong>Filtros</strong>
                                <div class="btn-group pull-right">
                                    {{-- 
                                    <button class="btn btn-success btn-xs" title="Generar Excel" onclick="generarExcel();" type="button">
                                        <i class="fas fa-file-excel"></i> Exportar a Excel
                                    </button>
                                    --}}
                                    <button class="btn btn-info btn-xs" title="Filtrar Listado"  type="sumbit">
                                        <i class="fas fa-list-alt"></i> Filtrar
                                    </button>
                                </div>
                                </h4>
                                <div class="form-group row">
                                    <label for="filtro_fecha_inicio" class="col-sm-1 control-label label-form">Fecha Inicio</label>
                                    <div class="col-sm-3">
                                        <div class="input-group">
                                            <input type="text" class="form-control fechas" id="filtro_fecha_inicio" name="filtro_fecha_inicio" @if(isset($filtroFechaInicio)) value="{{ $filtroFechaInicio }}" @else value="01/{{ date("/m/Y") }}" @endif >
                                            <span class="input-group-addon" onclick="fechaDocumento('filtro_fecha_inicio');" style="cursor:pointer;"><i class="fa fa-calendar" aria-hidden="true" style="color: black;"></i></span>
                                        </div>
                                    </div>

                                    <label for="filtro_fecha_termino" class="col-sm-1 control-label label-form">Fecha Termino</label>
                                    <div class="col-sm-3">
                                        <div class="input-group">
                                            <input type="text" class="form-control fechas" id="filtro_fecha_termino" name="filtro_fecha_termino" @if(isset($filtroFechaTermino)) value="{{ $filtroFechaTermino }}"  @else value="{{ date("d/m/Y") }}" @endif>
                                            <span class="input-group-addon" onclick="fechaDocumento('filtro_fecha_termino');" style="cursor:pointer;"><i class="fa fa-calendar" aria-hidden="true" style="color: black;"></i></span>
                                        </div>
                                    </div>

                                    <label for="filtro_proveedor" class="col-sm-1 control-label label-form">Proveedor</label>
                                    <div class="col-sm-3">
                                        <select name="filtro_proveedor[]" id="filtro_proveedor" class="form-control select2_filtro" multiple style="width: 100%">
                                            @foreach ( $proveedores as $proveedor )
                                                <option value="{{ $proveedor->id }}" >{{ $proveedor->rut }} {{ $proveedor->nombre }}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                </div>

                                <div class="form-group row">
                                    <label for="filtro_tipo_documento" class="col-sm-1 control-label label-form">Tipo de Documento</label>
                                    <div class="col-sm-3">
                                        <select name="filtro_tipo_documento[]" id="filtro_tipo_documento" class="form-control select2_filtro" multiple style="width: 100%">
                                            @foreach ( $tiposDocumento as $tipoDocumento )
                                                <option value="{{ $tipoDocumento->id }}" >{{ $tipoDocumento->nombre }}</option>
                                            @endforeach
                                        </select>
                                    </div>

                                    <label for="filtro_factoring" class="col-sm-1 control-label label-form">Factoring</label>
                                    <div class="col-sm-3">
                                        <select name="filtro_factoring[]" id="filtro_factoring" class="form-control select2_filtro" multiple="multiple">
                                            @foreach ( $proveedores as $proveedor )
                                                <option value="{{ $proveedor->id }}" >{{ $proveedor->formatRut() }} {{ $proveedor->nombre }}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                </div>
                                
                            </form>
                            <div class="table-toolbar">
                                    
                            </div>
                            <div class="row">
                                <div class="col-md-12">
                                    <div class="panel">
                                    {{-- 
                                        <div class="panel-heading">
                                            <div class="caption">
                                                <strong style="color: #69aa46;">Últimos Documentos Ingresados</strong>
                                            </div>
                                        </div>
                                        --}}
                                        <div class="panel-body">
                                            <div class="table-responsive">
                                                <table class="table table-striped table-bordered table-hover table-full-width" id="tabla_principal">
                                                    <thead>
                                                    <tr>
                                                        <th>Rut Proveedor</th>
                                                        <th>Nombre Proveedor</th>
                                                        <th >Tipo Documento</th>
                                                        <th >N° Documento</th>
                                                        <th>Rut Factoring</th>
                                                        <th>Nombre Factoring</th>
                                                        <th>Fecha</th>
                                                        <th >Total</th>
                                                        <th class="text-center">
                                                            <i class="fa fa-cog"></i>
                                                        </th>
                                                    </tr>
                                                    </thead>
                                                    <tbody>
                                                    @forelse($documentos as $documento)
                                                        <tr id="tr_{{ $documento->id }}">
                                                            <td>@if($documento->getProveedor){{ $documento->getProveedor->rut }}@endif</td>
                                                            <td>@if($documento->getProveedor){{ $documento->getProveedor->nombre }}@endif</td>
                                                            <td>{{ $documento->getTipoDocumento->nombre }}</td>
                                                            <td>{{ $documento->numero_documento }}</td>
                                                            <td>@if($documento->getProveedorFactoring){{ $documento->getProveedorFactoring->formatRut() }}@endif</td>
                                                            <td>@if($documento->getProveedorFactoring){{ $documento->getProveedorFactoring->nombre }}@endif</td>
                                                            <td class="text-center">{{ fecha_dmY($documento->fecha_documento) }}</td>
                                                            <td class="text-right">$ {{ formatoMiles($documento->total_documento) }}</td>
                                                            <td>
                                                                <div class="btn-group">
                                                                    @permission(['ver-documento'])
                                                                    <button class="btn btn-success btn-xs" title="Ver Documento" onclick="verDocumento({{ $documento->id }});">
                                                                        <i class="fa fa-eye"></i>
                                                                    </button>
                                                                    @endpermission
                                                                    @permission(['eliminar-factoring'])
                                                                    <button class="btn btn-danger btn-xs" title="Eliminar Factoring" onclick="eliminarFactoring({{ $documento->id }});">
                                                                        <i class="fa fa-trash"></i>
                                                                    </button>
                                                                    @endpermission
                                                                </div>
                                                            </td>
                                                        </tr>
                                                    @empty

                                                    @endforelse
                                                    </tbody>
                                                </table>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
						</div>
					</div>
					<!-- END EXAMPLE TABLE PORTLET-->
				</div>
			</div>
			<!-- END PAGE CONTENT-->
			
			
			
			
		</div> <!--/div class="page-content" -->
	</div> <!--/div class="page-content-wrapper" -->
</div><!--/div class="page-content-wrapper" -->
<!-- END CONTENT -->
<div id="modal"></div>
<div id="modal2"></div>
@endsection

@push('script-footer')
<!-- BEGIN PAGE LEVEL PLUGINS -->
<script type="text/javascript" src="{{ asset('assets/plugins/select2/select2.min.js') }} "></script>
{{--<script type="text/javascript" src="{{ asset('assets/plugins/data-tables/jquery.dataTables.min.js') }}"></script>--}}
<script type="text/javascript" src="{{ asset('datatables/jquery.dataTables.min.js') }}"></script>
<script type="text/javascript" src="{{ asset('assets/plugins/data-tables/DT_bootstrap.js') }}"></script>

<link rel="stylesheet" type="text/css" href="{{asset('assets/plugins/bootstrap-datepicker/css/datepicker.css')}}"/>

<script type="text/javascript" src="{{asset('assets/plugins/bootstrap-datepicker/js/bootstrap-datepicker.js')}}"></script>
<script type="text/javascript" src="{{asset('assets/plugins/bootstrap-datetimepicker/js/bootstrap-datetimepicker.js')}}"></script>


<!-- END PAGE LEVEL PLUGINS -->
<!-- BEGIN PAGE LEVEL SCRIPTS -->
<script src="{{ asset('assets/scripts/app.js') }} "></script>

<script type="text/javascript">
    let tablaPrincipal = $('#tabla_principal').DataTable({
        "language": {
            "emptyTable":     "No hay datos disponibles en la tabla",
            "info":           "Mostrando registros del _START_ al _END_ de un total de _TOTAL_ registros",
            "infoEmpty":      "Showing 0 to 0 of 0 entries",
            "infoFiltered":   "(filtrado de un total de _MAX_ registros)",
            "infoPostFix":    "",
            "thousands":      ",",
            "lengthMenu":     "Mostrar _MENU_ registros",
            "loadingRecords": "Cargando...",
            "processing":     "Procesando...",
            "search":         "Buscar: ",
            "zeroRecords":    "No matching records found",
            "paginate": {
                "first":      "Primero",
                "last":       "Último",
                "next":       "Siguiente",
                "previous":   "Anterior"
            },
            "aria": {
                "sortAscending":  ": Activar para ordenar la columna de manera ascendente",
                "sortDescending": ": Activar para ordenar la columna de manera descendente"
            }
        },
        "lengthMenu": [
            [10, 15, 20, -1],
            [10, 15, 20, "Todos"] // change per page values here
        ],
        // set the initial value
        "displayLength": 10,
        "paging":   true,
        "ordering": true,
        "info":     true,
        "searching": true
    });

    jQuery('#tabla_principal_filter input').addClass("form-control input-large"); // modify table search input
    jQuery('#tabla_principal_wrapper .dataTables_length select').addClass("form-control input-small"); // modify table per page dropdown
    jQuery('#tabla_principal_wrapper .dataTables_length select').select2(); // initialize select2 dropdown

    $('#tabla_principal_column_toggler input[type="checkbox"]').change(function() {
        /* Get the DataTables object again - this is not a recreation, just a get of the object */
        var iCol = parseInt($(this).attr("data-column"));
        var bVis = oTable.fnSettings().aoColumns[iCol].bVisible;
        oTable.fnSetColumnVis(iCol, (bVis ? false : true));
    });

	$(document).ready(function() {
		App.init(); // initlayout and core plugins

   		$("#tesoreria").addClass( "active" );
        $("#tesoreria-a").append( '<span class="selected"></span>' );
        $("#factoring").addClass( "active" );
        $("#factoring-grilla-li").addClass( "active" );
        $("#factoring-a").append( '<span class="selected"></span>' );

        $('#filtro_fecha_inicio').datepicker({
            format: 'dd/mm/yyyy',
            endDate: new Date(),
            autoclose: true,
            language: 'es'
        });

        $('#filtro_fecha_termino').datepicker({
            format: 'dd/mm/yyyy',
            //endDate: new Date(),
            //"setDate": new Date(),
            //startDate: new Date(),            
            autoclose: true,
            language: 'es'
        });

        $(".select2_filtro").select2({
            allowClear: true,
        });

        @isset($filtroProveedor)
            let Aux1 = new Array();
            @foreach ($filtroProveedor as $proveedor)
                Aux1.push('{{ $proveedor }}');
            @endforeach
            $('#filtro_proveedor').select2('val',Aux1);
        @endisset

        @isset($filtroTipoDocumento)
            let Aux2 = new Array();
            @foreach ($filtroTipoDocumento as $tipoDoc)
                Aux2.push('{{ $tipoDoc }}');
            @endforeach
            $('#filtro_tipo_documento').select2('val',Aux2);
        @endisset

        @isset($filtroProveedorFactoring)
            let Aux3 = new Array();
            @foreach ($filtroProveedorFactoring as $proveedor)
                Aux3.push('{{ $proveedor }}');
            @endforeach
            $('#filtro_factoring').select2('val',Aux3);
        @endisset
	});

    function fechaDocumento(id_fecha) {
        $('#' + id_fecha).datepicker("show");
    }

    function verDocumento(id)
    {
        $.get( '{{ url("documentos/modal/ver") }}/' + id, function( data ) {
            $( "#modal" ).html( data );
            $( "#modalVer" ).modal();
        });
    }

    function eliminarFactoring(id)
    {
        $.get( '{{ url("factoring/eliminar_factoring") }}/' + id, function( respuesta ) {
            console.log(respuesta);
            if (respuesta.estado == 'success') {
                toastr.success(respuesta.mensaje, 'Atención', optionsToastr);
                $('#tr_' + respuesta.id).fadeOut(400, function(){
                    tablaPrincipal.row('#tr_' + respuesta.id ).remove().draw();
                });
            }
        }).fail( function(respuesta) {
            toastr.error('No es posible realizar la acción', 'Atención', optionsToastr);
        });
    } 
</script>

@endpush