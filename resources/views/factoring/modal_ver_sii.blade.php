<link href="{{ asset('sistema/css/modales.css') }} " rel="stylesheet" type="text/css"/>
<div class="modal fade" id="modalVer" role="dialog" aria-labelledby="myModalLabel3" aria-hidden="true">
	<div class="modal-dialog" style="width:1000px;">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" title="Cerrar" class="close" data-dismiss="modal" aria-hidden="true"></button>
				<h4 class="modal-title" ><strong><i class="fas fa-file-export"></i> <i class="fa fa-files-o" ></i>&nbsp;&nbsp;Ver Documento</strong></h4>
            </div>
          
            <div class="modal-body">
                <div class="form-group row">
                    <div class="col-xs-12">
                        <div class="form-group">
                            <label class="col-sm-2 control-label font-bold">Vendedor</label>
                            <label class="col-sm-4 control-label">{{ $documento->vendedor }}</label>
                            <label class="col-sm-2 control-label font-bold">Estado Cesión</label>
                            <label class="col-sm-4 control-label">{{ $documento->estado_cesion }}</label>
                        </div>
                    </div>
                </div>

                <div class="form-group row">
                    <div class="col-xs-12">
                        <div class="form-group">
                            <label class="col-sm-2 control-label font-bold">Deudor</label>
                            <label class="col-sm-4 control-label">{{ $documento->deudor }}</label>
                            <label class="col-sm-2 control-label font-bold">Mail Deudor</label>
                            <label class="col-sm-4 control-label">{{ $documento->mail_deudor }}</label>
                        </div>
                    </div>
                </div>

                <div class="form-group row">
                    <div class="col-xs-12">
                        <div class="form-group">
                            <label class="col-sm-2 control-label font-bold">Tipo Doc</label>
                            <label class="col-sm-4 control-label">{{ $documento->tipo_doc }}</label>
                            <label class="col-sm-2 control-label font-bold">Nombre Doc</label>
                            <label class="col-sm-4 control-label">{{ $documento->nombre_doc }}</label>
                        </div>
                    </div>
                </div>

                <div class="form-group row">
                    <div class="col-xs-12">
                        <div class="form-group">
                            <label class="col-sm-2 control-label font-bold">Folio Doc</label>
                            <label class="col-sm-4 control-label">{{ $documento->folio_doc }}</label>
                            <label class="col-sm-2 control-label font-bold">Fecha Emisión DTE</label>
                            <label class="col-sm-4 control-label">{{ fecha_dmY($documento->fecha_emis_dte) }}</label>
                        </div>
                    </div>
                </div>

                <div class="form-group row">
                    <div class="col-xs-12">
                        <div class="form-group">
                            <label class="col-sm-2 control-label font-bold">Monto Total</label>
                            <label class="col-sm-4 control-label">$ {{ formatoMiles($documento->mnt_total) }}</label>
                            <label class="col-sm-2 control-label font-bold">Cedente</label>
                            <label class="col-sm-4 control-label">{{ $documento->cedente }}</label>
                        </div>
                    </div>
                </div>

                <div class="form-group row">
                    <div class="col-xs-12">
                        <div class="form-group">
                            <label class="col-sm-2 control-label font-bold">RZ Cedente</label>
                            <label class="col-sm-4 control-label">{{ $documento->rz_cedente }}</label>
                            <label class="col-sm-2 control-label font-bold">Mail Cedente</label>
                            <label class="col-sm-4 control-label">{{ $documento->mail_cedente }}</label>
                        </div>
                    </div>
                </div>

                <div class="form-group row">
                    <div class="col-xs-12">
                        <div class="form-group">
                            <label class="col-sm-2 control-label font-bold">Cesionario</label>
                            <label class="col-sm-4 control-label">{{ $documento->cesionario }}</label>
                            <label class="col-sm-2 control-label font-bold">RZ Cesionario</label>
                            <label class="col-sm-4 control-label">{{ $documento->razon_cesionario }}</label>
                        </div>
                    </div>
                </div>

                <div class="form-group row">
                    <div class="col-xs-12">
                        <div class="form-group">
                            <label class="col-sm-2 control-label font-bold">Mail Cesionario</label>
                            <label class="col-sm-4 control-label">{{ $documento->mail_cesionario }}</label>
                            <label class="col-sm-2 control-label font-bold">Fecha Cesión</label>
                            <label class="col-sm-4 control-label">{{ fecha_dmY($documento->fecha_cesion) }}</label>
                        </div>
                    </div>
                </div>

                <div class="form-group row">
                    <div class="col-xs-12">
                        <div class="form-group">
                            <label class="col-sm-2 control-label font-bold">Monto Cesión</label>
                            <label class="col-sm-4 control-label">$ {{ formatoMiles($documento->mnt_cesion) }}</label>
                            <label class="col-sm-2 control-label font-bold">Fecha Vencimiento</label>
                            <label class="col-sm-4 control-label">{{ fecha_dmY($documento->fecha_vencimiento)  }}</label>
                        </div>
                    </div>
                </div>

                <div class="form-group row">
                    <div class="col-xs-12">
                        <div class="form-group">
                            <label class="col-sm-2 control-label font-bold">Pagado</label>
                            <label class="col-sm-4 control-label">@if( $documento->pagado == 0)No @else Si @endif</label>
                        </div>
                    </div>
                </div>

                

              
            </div>{{-- /modal-body --}}

            <div class="modal-footer form-actions">
                <div class="btn-group">
                <button type="button" class="btn btn-info" title="Cerrar" data-dismiss="modal" aria-hidden="true">Cerrar</button>
                </div>
            </div>
          	
		</div>
	</div>
</div>

<script type="text/javascript">

 	$(document).ready(function(){
         
  
 	});

    
 </script>