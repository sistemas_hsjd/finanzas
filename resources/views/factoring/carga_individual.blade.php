@extends('master')

@section('title', 'Ingreso de Documentos Factoring')

@push('css-head')
<!-- BEGIN PAGE LEVEL STYLES -->
<link rel="stylesheet" type="text/css" href="{{asset('assets/plugins/select2/select2_conquer.css')}}"/>
<link rel="stylesheet" href="{{asset('assets/plugins/data-tables/DT_bootstrap.css')}}"/>
<!-- END PAGE LEVEL STYLES -->
<style type="text/css">
    .label-form {
        font-weight: bold;
        color: black;
        text-align: right;
    }

    .span-label {
        color: red;
    }
</style>
@endpush

@section('content')
<!-- BEGIN CONTENT -->
<div class="page-content-wrapper">
	<div class="page-content-wrapper">
		<div class="page-content">
			<!-- BEGIN STYLE CUSTOMIZER -->
			
			<!-- END BEGIN STYLE CUSTOMIZER -->
			<!-- BEGIN PAGE HEADER-->
			<!--<div class="row">
				<div class="col-md-12">
					<h3 class="page-title">
					Usuarios <small>Agregar usuarios</small>
					</h3>
				</div>
			</div>-->
			<!-- END PAGE HEADER-->
			<!-- BEGIN PAGE CONTENT-->
			<div class="row">
				<div class="col-md-12">
					<!-- BEGIN EXAMPLE TABLE PORTLET-->
					<div class="portlet">
						<div class="portlet-title">
							<div class="caption">
								<i class="fa fa-files-o" style="color: black;"></i> Ingreso de Documentos Factoring
							</div>
						</div>
						<div class="portlet-body form">

							<!-- BEGIN FORM-->
							<form action="{{ asset('factoring/carga/individual') }}" method="post" class="horizontal-form" id="form" autocomplete="off">
								<div class="form-body">
                                    <div class="note note-danger" id="divErrores" style="display:none;">
                                        <h4 class="block" style="margin-bottom:5 px;">Debe Completar los siguientes campos: </h4>
                                        <ul id="ulErrores"></ul>
                                    </div>

                                    <h4 class="form-section" style="color: #69aa46;">Proveedor</h4>
                                    <div class="form-group row">
                                        <label for="nombre_proveedor" class="col-sm-2 control-label label-form">Nombre Proveedor <span class="span-label">*</span></label>
                                        <div class="col-sm-4 form-validate">
                                            <input type="text" class="form-control" id="nombre_proveedor" name="nombre_proveedor" required placeholder="Buscador por Nombre" onchange="getProveedor(this);">
                                        </div>

                                        <label for="rut_proveedor" class="col-sm-2 control-label label-form">Rut Proveedor <span class="span-label">*</span></label>
                                        <div class="col-sm-4 form-validate">
                                            <input type="text" class="form-control" id="rut_proveedor" name="rut_proveedor" required placeholder="Buscador por Rut" onchange="getProveedor(this);">
                                        </div>
                                    </div>

                                    <h4 class="form-section" style="color: #69aa46;">Documento</h4>
                                    <div class="form-group row" >
                                        <label for="factura" class="col-sm-2 control-label label-form">Factura <span class="span-label">*</span></label>
                                        <div class="col-sm-4 form-validate" id="divFactura">
                                            <select name="factura" id="factura" class="form-control select2" required >
                                            <option value=''>Seleccione</option>
                                            </select>
                                        </div>
                                    </div>

                                    <h4 class="form-section" style="color: #69aa46;">Factoring</h4>
                                    <div class="form-group row">
                                        <label for="nombre_factoring" class="col-sm-2 control-label label-form">Nombre Factoring <span class="span-label">*</span></label>
                                        <div class="col-sm-4 form-validate">
                                            <input type="text" class="form-control" id="nombre_factoring" name="nombre_factoring" required placeholder="Buscador por Nombre" onchange="getProveedor(this);">
                                        </div>

                                        <label for="rut_factoring" class="col-sm-2 control-label label-form">Rut Factoring <span class="span-label">*</span></label>
                                        <div class="col-sm-4 form-validate">
                                            <input type="text" class="form-control" id="rut_factoring" name="rut_factoring" required placeholder="Buscador por Rut" onchange="getProveedor(this);">
                                        </div>
                                    </div>


                                    <h4 class="form-section" style="color: #69aa46;">Documento en PDF</h4>
                                    <div class="form-group row">
                                        <div class="input-group col-sm-10 col-sm-offset-1">
                                            <span class="input-group-addon" id="inputGroupFileAddon01"><strong>Seleccione Documento</strong></span>
                                            <input type="file" class="form-control" accept=".pdf" name="archivo" id="archivo" aria-describedby="inputGroupFileAddon01" onchange="validarArchivo();">
                                        </div>
                                    </div>

                                    <div class="form-group row">
                                        <div id="div_archivo" class="col-xs-12 table-responsive" style="display: none;">
                                            <table class="table table-striped table-bordered table-hover " id="tabla_info_archivo">
                                                <thead>
                                                    <tr>
                                                        <th width="40%">Nombre</th>
                                                        <th width="20%">Peso</th>
                                                        <th width="40%">Tipo</th>
                                                    </tr>
                                                </thead>
                                                <tbody>
                                                </tbody>
	                                        </table>
				                        </div>
                                    </div>

									<input type="hidden" name="_token" value="{{ csrf_token() }}">
								</div>
								<div class="form-actions right">
									<button  type="submit" title="Registrar Factoring" class="btn btn-success" id="botonGuardar"><i class="fa fa-check"></i> Registrar Factoring</button>
								</div>
							</form>
							<!-- END FORM-->
						</div>
					</div>
					<!-- END EXAMPLE TABLE PORTLET-->
				</div>
			</div>
			<!-- END PAGE CONTENT-->
		</div>
	</div>
</div>
<!-- END CONTENT -->

<div id="modal"></div>
<div id="modal2"></div>
@endsection

@push('script-footer')
	<!-- BEGIN PAGE LEVEL PLUGINS -->
	<script type="text/javascript" src="{{asset('assets/plugins/select2/select2.min.js')}}"></script>
	{{--<script type="text/javascript" src="{{ asset('assets/plugins/data-tables/jquery.dataTables.min.js') }}"></script>--}}
    <script type="text/javascript" src="{{ asset('datatables/jquery.dataTables.min.js') }}"></script>
	<script type="text/javascript" src="{{asset('assets/plugins/data-tables/DT_bootstrap.js')}}"></script>
	<!-- END PAGE LEVEL PLUGINS -->
	<!-- BEGIN PAGE LEVEL SCRIPTS -->
    <link rel="stylesheet" type="text/css" href="{{asset('assets/plugins/bootstrap-datepicker/css/datepicker.css')}}"/>

    <script type="text/javascript" src="{{asset('assets/plugins/bootstrap-datepicker/js/bootstrap-datepicker.js')}}"></script>
    <script type="text/javascript" src="{{asset('assets/plugins/bootstrap-datetimepicker/js/bootstrap-datetimepicker.js')}}"></script>

	<script src="{{asset('assets/scripts/table-advanced.js')}}"></script>
    
	<script>

        var selectTipoArchivo = '<select name="tipo_archivo" id="tipo_archivo" class="col-xs-12" required>';
        selectTipoArchivo += '<option value="">Seleccione</option>';
        @foreach ( $tiposArchivo as $tipoArchivo)
            selectTipoArchivo += '<option value="{{ $tipoArchivo->id }}">{{ $tipoArchivo->nombre }}</option>';
        @endforeach
        selectTipoArchivo += '</select>';

        var tablaArchivo = $('#tabla_info_archivo').DataTable({
            "paging":   false,
            "ordering": false,
            "info":     false,
            "searching": false
        });

        $(document).ready(function() {
            App.init();
            //TableAdvanced.init();

            $("#nombre_proveedor").select2({
                ajax: {
                    cache: true,
                    allowClear: true,
                    //hidden : true,
                    url : '{{ url('buscador_proveedor_nombre/') }}',
                    dataType: 'json',
                    delay: 250,
                    data: function (params,page) {
                        //console.log("soy params : "+params);
                        //console.log("soy page : "+page);
                        var query = {
                            term: params,
                            page: params.page || 1
                        }
                        return query;
                    },
                    results: function (data) {
                        return {
                        results: data
                        };
                    },
                },
                minimumInputLength: 2,
            
            });

            $("#rut_proveedor").select2({
                ajax: {
                    cache: true,
                    allowClear: true,
                    //hidden : true,
                    url : '{{ url('buscador_proveedor_rut/') }}',
                    dataType: 'json',
                    delay: 250,
                    data: function (params,page) {
                        //console.log("soy params : "+params);
                        //console.log("soy page : "+page);
                        var query = {
                            term: params,
                            page: params.page || 1
                        }
                        return query;
                    },
                    results: function (data) {
                        return {
                        results: data
                        };
                    },
                },
                minimumInputLength: 2,
            
            });

            $("#nombre_factoring").select2({
                ajax: {
                    cache: true,
                    allowClear: true,
                    //hidden : true,
                    url : '{{ url('buscador_proveedor_nombre/') }}',
                    dataType: 'json',
                    delay: 250,
                    data: function (params,page) {
                        //console.log("soy params : "+params);
                        //console.log("soy page : "+page);
                        var query = {
                            term: params,
                            page: params.page || 1
                        }
                        return query;
                    },
                    results: function (data) {
                        return {
                        results: data
                        };
                    },
                },
                minimumInputLength: 2,
            
            });

            $("#rut_factoring").select2({
                ajax: {
                    cache: true,
                    allowClear: true,
                    //hidden : true,
                    url : '{{ url('buscador_proveedor_rut/') }}',
                    dataType: 'json',
                    delay: 250,
                    data: function (params,page) {
                        //console.log("soy params : "+params);
                        //console.log("soy page : "+page);
                        var query = {
                            term: params,
                            page: params.page || 1
                        }
                        return query;
                    },
                    results: function (data) {
                        return {
                        results: data
                        };
                    },
                },
                minimumInputLength: 2,
            
            });

            $(".select2").select2();

            $("#tesoreria").addClass( "active" );
            $("#tesoreria-a").append( '<span class="selected"></span>' );
            $("#factoring").addClass( "active" );
            $("#factoring-carga-individual-li").addClass( "active" );
            $("#factoring-a").append( '<span class="selected"></span>' );


            $("#form").validate({
                highlight: function(element) {
                    $(element).closest('.form-validate').removeClass('has-success');
                    $(element).closest('.form-validate').addClass('has-error');
                },
                unhighlight: function(element) {
                    $(element).closest('.form-validate').removeClass('has-error');
                    $(element).closest('.form-validate').addClass('has-success');
                },
                errorElement: 'span',
                errorClass: 'help-block',
                errorPlacement: function (error, element) {
                    console.log(element);
                    if (element.parent('.input-group').length) {
                        error.insertAfter(element.parent());
                    } else {
                        error.insertAfter(element);
                    }
                },
                rules: {
                },
                messages: {
                },

                //para enviar el formulario por ajax
                submitHandler: function(form) {
                    esperandoIngreso();
                    
                    let formData = new FormData(form);
                    $.ajax({
                        url: form.action,
                        type: form.method,
                        //data: $(form).serialize(),
                        data: formData,
                        processData: false,
                        contentType: false,
                        success: function(respuesta) {
                            console.log(respuesta);

                            if ( respuesta.estadoDocumento == 'error' ) {
                                toastr.error('No es posible realizar la acción'+'<br><strong>Error: '+ respuesta.mensajeDocumento +'</strong>', 'Atención', optionsToastr);
                            } else if ( respuesta.estadoDocumento == 'success') {
                                
                                toastr.success(respuesta.mensajeDocumento, 'Atención', optionsToastr);
                                // Setear los elementos, dejar todo en blanco, para poder ingresar otro factoring
                                setDOM();
                                if ( respuesta.estadoArchivo == 'error' ) {
                                    toastr.error('No es posible realizar la acción'+'<br><strong>Error: '+ respuesta.mensajeArchivo +'</strong>', 'Atención', optionsToastr);
                                } else if ( respuesta.estadoArchivo == 'succes') {
                                    toastr.success(respuesta.mensajeArchivo, 'Atención', optionsToastr);
                                }
                                
                            }
                            
                        }            
                    }).fail( function(respuesta) {//fail ajax
                        if ( respuesta.status == 400 ) {
                            mostrarErroresValidator(respuesta);
                        } else if ( respuesta.status == 500 ) {
                            toastr.error('No es posible realizar la acción, error en el servidor', 'Atención', optionsToastr);
                        } else {
                            toastr.error('No es posible realizar la acción', 'Atención', optionsToastr);
                        }
                        
                    })
                    .always(function() {
                        listoIngreso();
                    });//ajax
                    
                }

            });

        });

        function getProveedor(input) {
            // console.log("soy el input :"+input.id);
            // console.log("soy el value :"+input.value);
            if (input.value != '') {
                $.ajaxSetup({
                    headers: { 'X-CSRF-TOKEN': '{{ csrf_token() }}' }
                });

                $.ajax({
                    url: '{{ url('documentos/get_proveedor/') }}/' + input.value,
                    type: 'get',

                    success: function(proveedor) {
                        //se pasan los datos a los otros select, dependiendo de cual se cambio en primera instancia
                        switch (input.id) {
                            case "rut_proveedor":
                                $("#nombre_proveedor").select2("data", { id: input.value, text: proveedor.nombre });
                                $('#factura').select2('val', '');
                                //Buscar facturas del proveedor.
                                getFacturasProveedor(input.value);
                                break;
                            case "nombre_proveedor":
                                $("#rut_proveedor").select2("data", { id: input.value, text: proveedor.rut });
                                $('#factura').select2('val', '');
                                //Buscar facturas del proveedor.
                                getFacturasProveedor(input.value);
                                break;

                            case "rut_factoring":
                                $("#nombre_factoring").select2("data", { id: input.value, text: proveedor.nombre });
                                break;
                            case "nombre_factoring":
                                $("#rut_factoring").select2("data", { id: input.value, text: proveedor.rut });
                                break;
                        }

                        
                    } // succes

                }).fail(function() {
                    toastr.warning("No es posible buscar datos, intentelo más tarde", "Atención", optionsToastr);
                });

            } else {
                $("#nombre_proveedor").select2("data", { id: '', text: '' });
                $("#rut_proveedor").select2("data", { id: '', text: '' });
                $('#divFactura').html('');
            }

        }

        function getFacturasProveedor(idProveedor) {

            console.log('idProveedor : '+idProveedor);
            $('#divFactura').html('');
            $.ajax({
                url: '{{ url('factoring/get_facturas_proveedor/') }}/' + idProveedor,
                type: 'get',

                success: function(documentos) {
                    console.log(documentos);
                    if (documentos.estado == 'success') {

                        let htmlReemplaza = '<select name="factura" id="factura" class="form-control select2" required >';
                        htmlReemplaza += '<option value="">Seleccione</option>';
                        htmlReemplaza += documentos.opciones; // se añaden las opciones traidas desde el controlador
                        htmlReemplaza += '</select></div>';

                        $('#divFactura').html(htmlReemplaza);
                        $('#factura').select2();
                    } else {
                        toastr.warning(documentos.mensaje, "Atención", optionsToastr);
                    }
                   
                } // succes

            }).fail(function() {
                toastr.warning("No es posible buscar datos, intentelo más tarde", "Atención", optionsToastr);
            });

        }
        
        function validarArchivo() {
            // console.log("validando archivo");
            let archivo = $('#archivo').val().replace(/.*(\/|\\)/, ''); //val();
            let extension = archivo.substring(archivo.lastIndexOf('.'));
            if (archivo == '') {
                $('#div_archivo').css('display', 'none');
                tablaArchivo.clear().draw(); // se remueven todas las filas de la tabla
                return false;
            } else {
                if (extension != '.pdf') {
                    toastr.error('El archivo no tiene la extension correcta: ' + '<strong>PDF</strong>', 'Atención', optionsToastr);
                    $('#archivo').val('');
                    $('#div_archivo').css('display', 'none');
                    tablaArchivo.clear().draw(); // se remueven todas las filas de la tabla
                    return false;
                } else {
                    mostrarInfoArchivo(archivo);
                    return true;
                }
            }
        }

        function mostrarInfoArchivo(archivo) {
            let nombreArchivo = archivo.substring(0, archivo.lastIndexOf('.'));
            let tamañoArchivo = $('#archivo')[0].files[0].size;

            tablaArchivo.row.add([
                nombreArchivo,
                tamañoArchivo + " bits",
                selectTipoArchivo
            ]).draw(false);

            $('#div_archivo').css('display', '');
        }

        function esperandoIngreso() {
            $('.page-header-fixed *').css('cursor', 'wait');
            $("#botonGuardar").attr("disabled", true);
        }

        function listoIngreso() {
            $('.page-header-fixed *').css('cursor', '');
            $("#botonGuardar").attr("disabled", false);
        }

        function mostrarErroresValidator(respuesta) {
            if (respuesta.responseJSON) {
                //console.log(respuesta.responseJSON);
                let htmlErrores = '';
                for (let k in respuesta.responseJSON) {
                    //console.log(k, respuesta.responseJSON[k]);
                    htmlErrores += '<li>' + respuesta.responseJSON[k] + '</li>';
                }

                $('#ulErrores').html(htmlErrores);
                $('#divErrores').css('display', '');
                toastr.error('No es posible realizar la acción' + '<br>' + 'Errores:<br> <ul>' + htmlErrores + '</ul>', 'Atención', optionsToastr);
            }
        }

        function setDOM() {
            $('#nombre_proveedor').select2('val', '');
            $('#rut_proveedor').select2('val', '').trigger('change');

            $('#nombre_factoring').select2('val', '');
            $('#rut_factoring').select2('val', '');

            //Archivo
            $('#archivo').val(null);
            $('#div_archivo').css('display', 'none');
            tablaArchivo.clear().draw(); // se remueven todas las filas de la tabla

            // Errores validator laravel
            $('#ulErrores').html('');
            $('#divErrores').css('display', 'none');

            $('.form-validate').removeClass('has-success');
        }
    </script>
@endpush
