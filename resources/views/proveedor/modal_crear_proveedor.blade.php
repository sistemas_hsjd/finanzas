<link href="{{ asset('sistema/css/modales.css') }} " rel="stylesheet" type="text/css"/>
<div class="modal fade" id="modalCrear" role="dialog" aria-labelledby="myModalLabel3" aria-hidden="true">
	<div class="modal-dialog" style="width:1100px;">
		<div class="modal-content">
            <div class="modal-header">
				<button type="button" title="Cerrar" class="close" data-dismiss="modal" aria-hidden="true"></button>
				<h4 class="modal-title" ><strong><i class="fas fa-truck fa-lg" ></i>&nbsp;&nbsp;Crear Proveedor</strong></h4>
            </div>

            <form action="{{ asset('proveedores') }}" method="post" class="horizontal-form" id="form-proveedor" autocomplete="off">
                <div class="modal-body">
                    <div class="form-body">
                        <div class="note note-danger" id="divErrores" style="display:none;">
                            <h4 class="block" style="margin-bottom:5 px;">Debe Completar los siguientes campos: </h4>
                            <ul id="ulErrores"></ul>
                        </div>

                        <div class="form-group row">
                            <label for="rut" class="col-sm-2 control-label label-form">Rut <span class="span-label">*</span></label>
                            <div class="col-sm-3 form-validate">
                                <input type="text" class="form-control" id="rut" name="rut" required >
                            </div>
                        </div>

                        <div class="form-group row">
                            <label for="nombre_razon_social" class="col-sm-2 control-label label-form">Nombre / Razón Social <span class="span-label">*</span></label>
                            <div class="col-sm-10 form-validate">
                                <input type="text" class="form-control" id="nombre_razon_social" name="nombre_razon_social" required >
                            </div>
                        </div>

                        <div class="form-group row">
                            <label for="direccion" class="col-sm-2 control-label label-form">Dirección </label>
                            <div class="col-sm-4 form-validate">
                                <input type="text" class="form-control" id="direccion" name="direccion" >
                            </div>

                            <label for="telefono" class="col-sm-2 control-label label-form">Teléfono </label>
                            <div class="col-sm-4 form-validate">
                                <input type="text" class="form-control" id="telefono" name="telefono" >
                            </div>
                        </div>

                        <div class="form-group row">
                            <label for="email" class="col-sm-2 control-label label-form">E-mail </label>
                            <div class="col-sm-4 form-validate">
                                <input type="email" class="form-control" id="email" name="email" >
                            </div>

                            <label for="contacto" class="col-sm-2 control-label label-form">Contacto </label>
                            <div class="col-sm-4 form-validate">
                                <input type="text" class="form-control" id="contacto" name="contacto" >
                            </div>
                        </div>

                        <div class="form-group row">
                            <label for="banco" class="col-sm-2 control-label label-form">Banco </label>
                            <div class="col-sm-4 form-validate">
                                <select name="banco" id="banco" class="form-control select2" >
                                <option value=''>Seleccione</option>
                                @foreach ( $bancos as $banco)
                                    <option value="{{ $banco->id }}">{{ $banco->nombre }}</option>
                                @endforeach
                                </select>
                            </div>

                            <label for="tipo_cuenta" class="col-sm-2 control-label label-form">Tipo Cuenta </label>
                            <div class="col-sm-4 form-validate">
                                <select name="tipo_cuenta" id="tipo_cuenta" class="form-control select2" >
                                <option value=''>Seleccione</option>
                                @foreach ( $tiposCuenta as $tipoCuenta)
                                    <option value="{{ $tipoCuenta->id }}">{{ $tipoCuenta->nombre }}</option>
                                @endforeach
                                </select>
                            </div>
                        </div>

                        <div class="form-group row">
                            <label for="numero_cuenta" class="col-sm-2 control-label label-form">N° Cuenta </label>
                            <div class="col-sm-4 form-validate">
                                <input type="text" class="form-control" id="numero_cuenta" name="numero_cuenta" >
                            </div>

                            <label for="transferencia" class="col-sm-2 control-label label-form">Transferencia </label>
                            <div class="col-sm-4 form-validate">
                            <div class="checkbox check" >
                                <label class="label_checkbox">
                                    <input type="checkbox" class="check_input" id="transferencia" name="transferencia" value="1" >
                                </label>
                                </div>
                            </div>
                        </div>

                        <div class="form-group row">
                            <label for="activo" class="col-sm-2 control-label label-form">Activo </label>
                            <div class="col-sm-4 form-validate">
                            <div class="checkbox check" >
                                <label class="label_checkbox">
                                    <input type="checkbox" class="check_input" id="activo" name="activo" value="1" >
                                </label>
                                </div>
                            </div>
                        </div>

                        <div class="form-group row">
                            <label for="proveedores_fusion" class="col-sm-3 control-label label-form">Fusión de los Proveedores </label>
                            <div class="col-sm-9 form-validate">
                                <select name="proveedores_fusion[]" id="proveedores_fusion" class="form-control select2" multiple >
                                <option value=''>Seleccione</option>
                                @foreach ( $proveedoresParaFusion as $provFusion)
                                    <option value="{{ $provFusion->id }}">{{ $provFusion->rut }} {{ $provFusion->nombre }}</option>
                                @endforeach
                                </select>
                            </div>
                        </div>

                    </div>
                </div>

                <div class="modal-footer form-actions right">
                    <input type="hidden" name="_token" value="{{ csrf_token() }}">

                    <button type="button" title="Cancelar" class="btn btn-default" data-dismiss="modal">Cancelar</button>
                    <button type="submit" title="Guardar Proveedor" class="btn btn-success" id="botonGuardar"><i class="far fa-save" style="color:black;"></i> Guardar Proveedor</button>
                </div>
            </form>

        </div>
    </div>
</div>


<script>
    $(document).ready(function() {

        $(".select2").select2();
        
        $("#rut").Rut({
  			on_error: function(){
                toastr.error("El Rut ingresado no es valido", optionsToastr);
             },
		   format_on: 'keyup'
		});

        $("#form-proveedor").validate({
            highlight: function(element) {
                $(element).closest('.form-validate').removeClass('has-success');
                $(element).closest('.form-validate').addClass('has-error');
            },
            unhighlight: function(element) {
                $(element).closest('.form-validate').removeClass('has-error');
                $(element).closest('.form-validate').addClass('has-success');
            },
            errorElement: 'span',
            errorClass: 'help-block',
            errorPlacement: function (error, element) {
                console.log(element);
                if (element.parent('.input-group').length) {
                    error.insertAfter(element.parent());
                } else {
                    error.insertAfter(element);
                }
            },
            rules: {
            },
            messages: {
            },

            //para enviar el formulario por ajax
            submitHandler: function(form) {
                esperandoCrearProveedor();

                let formData = new FormData(form);
                formData.delete('rut');
                let rut = $('#rut').val().replace(/\./g, '');
                formData.append('rut',rut);
                
                $.ajax({
                    url: form.action,
                    type: form.method,
                    //data: $(form).serialize(),
                    data: formData,
                    processData: false,
                    contentType: false,
                    success: function(respuesta) {
                        console.log(respuesta);
                        if ( respuesta.estado == 'error' ) {
                            toastr.error('No es posible realizar la acción'+'<br><strong>Error: '+ respuesta.mensaje +'</strong>', 'Atención', optionsToastr);
                        } else if ( respuesta.estado == 'success') {
                            
                            toastr.success(respuesta.mensaje, 'Atención', optionsToastr);
                            // Agregar informacion a la tabla de proveedores
                            nuevaFilaTabla(respuesta);
                            $("#modalCrear").modal("hide");
                        }
                        
                    }            
                }).fail( function(respuesta) {//fail ajax
                    if ( respuesta.status == 400 ) {
                        mostrarErroresValidator(respuesta);
                    } else if ( respuesta.status == 500 ) {
                        toastr.error('No es posible realizar la acción, error en el servidor', 'Atención', optionsToastr);
                    } else {
                        toastr.error('No es posible realizar la acción', 'Atención', optionsToastr);
                    }
                    
                })
                .always(function() {
                    listoCrearProveedor();
                });//ajax
                
            }

        });

    });

    function esperandoCrearProveedor()
    {   
        $('.page-header-fixed *').css('cursor', 'wait');
        $("#botonGuardar").attr("disabled",true);
    }

    function listoCrearProveedor()
    {
        $('.page-header-fixed *').css('cursor', '');
        $("#botonGuardar").attr("disabled",false);
    }

    function mostrarErroresValidator(respuesta)
    {
        if ( respuesta.responseJSON ) {
            //console.log(respuesta.responseJSON);
            let htmlErrores = '';
            for (let k in respuesta.responseJSON) { 
                //console.log(k, respuesta.responseJSON[k]);
                htmlErrores += '<li>' + respuesta.responseJSON[k] + '</li>';
            }

            $('#ulErrores').html(htmlErrores);
            $('#divErrores').css('display','');
            toastr.error('No es posible realizar la acción'+'<br>'+'Errores:<br> <ul>'+htmlErrores+'</ul>', 'Atención', optionsToastr);
        }
    }

    function nuevaFilaTabla(respuesta)
    {
        // alert(tablaProveedores.row('#proveedor_'+ respuesta.idProveedor).data());

        let botones = '<td width="10%"><div class="btn-group">';
        @permission(['ver-proveedor'])
        botones += '<button class="btn btn-success btn-xs" title="Ver Proveedor" onclick="ver('+ respuesta.idProveedor +');">';
        botones += '<i class="fas fa-eye"></i></button>';
        @endpermission
        @permission(['editar-proveedor'])
        botones += '<button class="btn btn-warning btn-xs" title="Editar Proveedor" onclick="editar('+ respuesta.idProveedor +');">';
        botones += '<i class="fas fa-edit"></i></button>';
        @endpermission
        @permission(['eliminar-proveedor'])
        botones += '<button class="btn btn-danger btn-xs" title="Eliminar Proveedor" onclick="eliminar('+ respuesta.idProveedor +');">';
        botones += '<i class="fas fa-trash"></i></button>';
        @endpermission
        botones += '</div></td>';

        let rowNode = tablaProveedores.row.add ([
           respuesta.rutProveedor,
            respuesta.nombreProveedor,
            respuesta.emailProveedor,
            respuesta.contactoProveedor,
            respuesta.transferenciaProveedor,
            respuesta.activoProveedor,
            botones
        ]).draw().node();

        // Se entrega el id del item a la fila, para poder eliminar o editar posteriormente
        $( rowNode ).attr("id", 'proveedor_'+ respuesta.idProveedor);
    }

</script>