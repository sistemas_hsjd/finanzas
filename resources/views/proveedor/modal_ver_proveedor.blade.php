<link href="{{ asset('sistema/css/modales.css') }} " rel="stylesheet" type="text/css"/>
<div class="modal fade" id="modalVer" role="dialog" aria-labelledby="myModalLabel3" aria-hidden="true">
	<div class="modal-dialog" style="width:1000px;">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" title="Cerrar" class="close" data-dismiss="modal" aria-hidden="true"></button>
				<h4 class="modal-title" ><strong><i class="fas fa-truck fa-lg" ></i>&nbsp;&nbsp;Ver Proveedor</strong></h4>
            </div>
          
            <div class="modal-body">
                <div class="form-group row">
                    <div class="col-xs-12">
                        <div class="form-group">
                            <label class="col-sm-2 control-label font-bold">Rut</label>
                            <label class="col-sm-4 control-label">{{ $proveedor->rut }}</label>
                            
                        </div>
                    </div>
                </div>

                <div class="form-group row">
                    <div class="col-xs-12">
                        <div class="form-group">
                            <label class="col-sm-2 control-label font-bold">Nombre / Razón Social</label>
                            <label class="col-sm-10 control-label">{{ $proveedor->nombre }}</label>
                        </div>
                    </div>
                </div>

                <div class="form-group row">
                    <div class="col-xs-12">
                        <div class="form-group">
                            <label class="col-sm-2 control-label font-bold">Dirreción</label>
                            <label class="col-sm-4 control-label">{{ $proveedor->direccion }}</label>
                            <label class="col-sm-2 control-label font-bold">Teléfono</label>
                            <label class="col-sm-4 control-label">{{ $proveedor->telefono }}</label>
                        </div>
                    </div>
                </div>

                <div class="form-group row">
                    <div class="col-xs-12">
                        <div class="form-group">
                            <label class="col-sm-2 control-label font-bold">E-Mail</label>
                            <label class="col-sm-4 control-label">{{ $proveedor->email }}</label>
                            <label class="col-sm-2 control-label font-bold">Contacto</label>
                            <label class="col-sm-4 control-label">{{ $proveedor->contacto }}</label>
                        </div>
                    </div>
                </div>

                <div class="form-group row">
                    <div class="col-xs-12">
                        <div class="form-group">
                            <label class="col-sm-2 control-label font-bold">Banco</label>
                            <label class="col-sm-4 control-label">@if( $proveedor->getBanco ){{ $proveedor->getBanco->nombre }} @else Sin Banco  @endif</label>
                            <label class="col-sm-2 control-label font-bold">Tipo Cuenta</label>
                            <label class="col-sm-4 control-label">@if( $proveedor->getTipoCuenta ){{ $proveedor->getTipoCuenta->nombre }} @else Sin Cuenta @endif</label>
                        </div>
                    </div>
                </div>

                <div class="form-group row">
                    <div class="col-xs-12">
                        <div class="form-group">
                            <label class="col-sm-2 control-label font-bold">N° Cuenta</label>
                            <label class="col-sm-4 control-label">{{ $proveedor->numero_cuenta }}</label>
                            <label class="col-sm-2 control-label font-bold">Transferencia</label>
                            <label class="col-sm-4 control-label">@if($proveedor->transferencia == 0 )No @else Si @endif</label>
                        </div>
                    </div>
                </div>

                <div class="form-group row">
                    <div class="col-xs-12">
                        <div class="form-group">
                            <label class="col-sm-2 control-label font-bold">Activo</label>
                            <label class="col-sm-4 control-label">@if( $proveedor->activo == 0)No @else Si @endif</label>
                        </div>
                    </div>
                </div>

                @if ( $proveedor->getProveedorMaestro )
                <div class="form-group row">
                    <div class="col-xs-12">
                        <div class="form-group">
                            <label class="col-sm-3 control-label font-bold">Proveedor "Maestro"</label>
                            <label class="col-sm-9 control-label">{{ $proveedor->getProveedorMaestro->rut }} {{ $proveedor->getProveedorMaestro->nombre }}</label>
                        </div>
                    </div>
                </div>
                @endif

                @if ( $proveedor->getProveedoresFusion->count() > 0 )
                <div class="form-group row">
                    <div class="col-xs-12">
                        <div class="form-group">
                            <label class="col-sm-3 control-label font-bold">Proveedores Fusionados</label>
                            <label class="col-sm-9 control-label">
                                @foreach ($proveedor->getProveedoresFusion as $provFusion)
                                {{ $provFusion->rut }} {{ $provFusion->nombre }}<br>
                                @endforeach
                            </label>
                        </div>
                    </div>
                </div>
                @endif

                

              
            </div>{{-- /modal-body --}}

            <div class="modal-footer form-actions">
                <div class="btn-group">
                <button type="button" class="btn btn-info" title="Cerrar" data-dismiss="modal" aria-hidden="true">Cerrar</button>
                </div>
            </div>
          	
		</div>
	</div>
</div>

<script type="text/javascript">

 	$(document).ready(function(){
         
  
 	});

    
 </script>