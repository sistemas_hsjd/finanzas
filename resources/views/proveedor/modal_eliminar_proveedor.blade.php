<link href="{{ asset('sistema/css/modales.css') }} " rel="stylesheet" type="text/css"/>
<div class="modal fade" id="modalEliminar" role="dialog" aria-labelledby="myModalLabel3" aria-hidden="true">
	<div class="modal-dialog" style="width:1000px;">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" title="Cerrar" class="close" data-dismiss="modal" aria-hidden="true"></button>
				<h4 class="modal-title" ><strong><i class="fas fa-truck fa-lg"></i>&nbsp;&nbsp;Eliminar Proveedor</strong></h4>
            </div>
            <form action="{{ asset('proveedores/eliminar_proveedor') }}" method="post" class="horizontal-form" id="form-proveedor">
                <div class="modal-body">
                    <div class="form-body">
                        <div class="form-group row">
                            <div class="col-xs-12">
                                <div class="form-group">
                                    <label class="col-sm-2 control-label font-bold">Rut</label>
                                    <label class="col-sm-4 control-label">{{ $proveedor->rut }}</label>
                                    
                                </div>
                            </div>
                        </div>

                        <div class="form-group row">
                            <div class="col-xs-12">
                                <div class="form-group">
                                    <label class="col-sm-2 control-label font-bold">Nombre / Razón Social</label>
                                    <label class="col-sm-10 control-label">{{ $proveedor->nombre }}</label>
                                </div>
                            </div>
                        </div>

                        <div class="form-group row">
                            <div class="col-xs-12">
                                <div class="form-group">
                                    <label class="col-sm-2 control-label font-bold">Dirreción</label>
                                    <label class="col-sm-4 control-label">{{ $proveedor->direccion }}</label>
                                    <label class="col-sm-2 control-label font-bold">Teléfono</label>
                                    <label class="col-sm-4 control-label">{{ $proveedor->telefono }}</label>
                                </div>
                            </div>
                        </div>

                        <div class="form-group row">
                            <div class="col-xs-12">
                                <div class="form-group">
                                    <label class="col-sm-2 control-label font-bold">E-Mail</label>
                                    <label class="col-sm-4 control-label">{{ $proveedor->email }}</label>
                                    <label class="col-sm-2 control-label font-bold">Contacto</label>
                                    <label class="col-sm-4 control-label">{{ $proveedor->contacto }}</label>
                                </div>
                            </div>
                        </div>

                        <div class="form-group row">
                            <div class="col-xs-12">
                                <div class="form-group">
                                    <label class="col-sm-2 control-label font-bold">Banco</label>
                                    <label class="col-sm-4 control-label">@if( $proveedor->getBanco ){{ $proveedor->getBanco->nombre }} @else Sin Banco  @endif</label>
                                    <label class="col-sm-2 control-label font-bold">Tipo Cuenta</label>
                                    <label class="col-sm-4 control-label">@if( $proveedor->getTipoCuenta ){{ $proveedor->getTipoCuenta->nombre }} @else Sin Cuenta @endif</label>
                                </div>
                            </div>
                        </div>

                        <div class="form-group row">
                            <div class="col-xs-12">
                                <div class="form-group">
                                    <label class="col-sm-2 control-label font-bold">N° Cuenta</label>
                                    <label class="col-sm-4 control-label">{{ $proveedor->numero_cuenta }}</label>
                                    <label class="col-sm-2 control-label font-bold">Transferencia</label>
                                    <label class="col-sm-4 control-label">@if($proveedor->transferencia == 0 )No @else Si @endif</label>
                                </div>
                            </div>
                        </div>

                        <div class="form-group row">
                            <div class="col-xs-12">
                                <div class="form-group">
                                    <label class="col-sm-2 control-label font-bold">Activo</label>
                                    <label class="col-sm-4 control-label">@if( $proveedor->activo == 0)No @else Si @endif</label>
                                </div>
                            </div>
                        </div>

                        @if ( $proveedor->getProveedoresFusion->count() > 0 )
                        <div class="form-group row">
                            <div class="col-xs-12">
                                <div class="form-group">
                                    <label class="col-sm-3 control-label font-bold">Proveedores Fusionados</label>
                                    <label class="col-sm-9 control-label">
                                        @foreach ($proveedor->getProveedoresFusion as $provFusion)
                                        {{ $provFusion->rut }} {{ $provFusion->nombre }}<br>
                                        @endforeach
                                    </label>
                                </div>
                            </div>
                        </div>
                        @endif

                    </div>
                </div>

                <div class="modal-footer form-actions right">
                    <input type="hidden" name="_token" value="{{ csrf_token() }}">
                    <input type="hidden" name="id_proveedor" value="{{ $proveedor->id }}">
                    <button type="button" title="Cancelar" class="btn btn-default" data-dismiss="modal">Cancelar</button>
                    <button type="submit" title="Eliminar Proveedor" class="btn btn-danger" id="botonEliminar"><i class="fa fa-trash"></i> Eliminar Proveedor</button>
                </div>
            </form>
        </div>
    </div>
</div>

<script>
    $(document).ready(function() {
        $("#form-proveedor").validate({
            highlight: function(element) {
                $(element).closest('.form-validate').removeClass('has-success');
                $(element).closest('.form-validate').addClass('has-error');
            },
            unhighlight: function(element) {
                $(element).closest('.form-validate').removeClass('has-error');
                $(element).closest('.form-validate').addClass('has-success');
            },
            errorElement: 'span',
            errorClass: 'help-block',
            errorPlacement: function (error, element) {
                console.log(element);
                if (element.parent('.input-group').length) {
                    error.insertAfter(element.parent());
                } else {
                    error.insertAfter(element);
                }
            },
            rules: {
            },
            messages: {
            },

            //para enviar el formulario por ajax
            submitHandler: function(form) {
                esperandoEliminar();
                
                let formData = new FormData(form);
                $.ajax({
                    url: form.action,
                    type: form.method,
                    //data: $(form).serialize(),
                    data: formData,
                    processData: false,
                    contentType: false,
                    success: function(respuesta) {
                        console.log(respuesta);
                        if ( respuesta.estado == 'error' ) {
                            toastr.error('No es posible realizar la acción'+'<br><strong>Error: '+ respuesta.mensaje +'</strong>', 'Atención', optionsToastr);
                        } else if ( respuesta.estado == 'success') {
                            toastr.success(respuesta.mensaje, 'Atención', optionsToastr);
                            // Quitar el proveedor de la tabla
                            quitarProveedorTabla(respuesta);
                            $("#modalEliminar").modal("hide");
                        }
                    }            
                }).fail( function(respuesta) {//fail ajax
                    toastr.error('No es posible realizar la acción', 'Atención', optionsToastr);
                })
                .always(function() {
                    listoEliminar();
                });//ajax

            }

        });
    });

    function esperandoEliminar()
    {   
        $('.page-header-fixed *').css('cursor', 'wait');
        $("#botonEliminar").attr("disabled",true);
    }

    function listoEliminar()
    {
        $('.page-header-fixed *').css('cursor', '');
        $("#botonEliminar").attr("disabled",false);
    }

    function quitarProveedorTabla(respuesta)
    {
        $('#proveedor_' + respuesta.idProveedor).fadeOut(400, function(){
            tablaProveedores.row('#proveedor_' + respuesta.idProveedor ).remove().draw();
        });
    }
</script>