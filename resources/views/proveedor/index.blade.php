@extends('master')

@section('title', 'Mantenedor de Proveedores')

@push('css-head')
<!-- BEGIN PAGE LEVEL STYLES -->
<link rel="stylesheet" type="text/css" href="{{asset('assets/plugins/select2/select2_conquer.css')}}"/>
<link rel="stylesheet" href="{{asset('assets/plugins/data-tables/DT_bootstrap.css')}}"/>
<!-- END PAGE LEVEL STYLES -->

@endpush

@section('content')

<!-- BEGIN CONTENT -->

<div class="page-content-wrapper">
	<div class="page-content-wrapper">
		<div class="page-content">

			<!-- BEGIN PAGE CONTENT-->
			<div class="row">
				<div class="col-md-12">
					<!-- BEGIN EXAMPLE TABLE PORTLET-->
					<div class="portlet">
						<div class="portlet-title">
							<div class="caption">
								<i class="fas fa-truck" style="color: black;"></i> Mantenedor de Proveedores
							</div>
						</div>
						<div class="portlet-body">
                            <div class="table-toolbar">
                                <div class="btn-group">
                                    @permission(['crear-proveedor'])
                                    <button title="Agregar Proveedor" class="btn btn-success" onclick="nuevo();">
										<i class="fa fa-plus"></i> Agregar Proveedor 
									</button>
                                    @endpermission
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-12">
                                    <div class="panel">
                                    {{-- 
                                        <div class="panel-heading">
                                            <div class="caption">
                                                <strong style="color: #69aa46;">Últimos Documentos Ingresados</strong>
                                            </div>
                                        </div>
                                        --}}
                                        <div class="panel-body">
                                            <div class="table-responsive">
                                                <table class="table table-striped table-bordered table-hover table-full-width" id="tabla_proveedores">
                                                </table>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
						</div>
					</div>
					<!-- END EXAMPLE TABLE PORTLET-->
				</div>
			</div>
			<!-- END PAGE CONTENT-->
			
			
			
			
		</div> <!--/div class="page-content" -->
	</div> <!--/div class="page-content-wrapper" -->
</div><!--/div class="page-content-wrapper" -->
<!-- END CONTENT -->
<div id="modal"></div>
<div id="modal2"></div>
@endsection

@push('script-footer')
<script src="{{asset('assets/plugins/jquery-rut/jquery.Rut.min.js')}}" type="text/javascript"></script>
<!-- BEGIN PAGE LEVEL PLUGINS -->
<script type="text/javascript" src="{{ asset('assets/plugins/select2/select2.min.js') }} "></script>
{{--<script type="text/javascript" src="{{ asset('assets/plugins/data-tables/jquery.dataTables.min.js') }}"></script>--}}
<script type="text/javascript" src="{{ asset('datatables/jquery.dataTables.min.js') }}"></script>
<script type="text/javascript" src="{{ asset('assets/plugins/data-tables/DT_bootstrap.js') }}"></script>

<link rel="stylesheet" type="text/css" href="{{asset('assets/plugins/bootstrap-datepicker/css/datepicker.css')}}"/>

<script type="text/javascript" src="{{asset('assets/plugins/bootstrap-datepicker/js/bootstrap-datepicker.js')}}"></script>
<script type="text/javascript" src="{{asset('assets/plugins/bootstrap-datetimepicker/js/bootstrap-datetimepicker.js')}}"></script>


<!-- END PAGE LEVEL PLUGINS -->
<!-- BEGIN PAGE LEVEL SCRIPTS -->
<script src="{{ asset('assets/scripts/app.js') }} "></script>
<script src="{{ asset('assets/scripts/table-advanced.js') }} "></script>


<script type="text/javascript">

    let tablaProveedores = $('#tabla_proveedores').DataTable({
        "processing": true,
        "serverSide": true,
        "ajax": {
            "url" : "{{ url('proveedores/data_table/index') }}",
            "type" : "POST",
        },
        // Set rows IDs
        rowId: function(data) {
            return 'proveedor_' + data.DT_RowID;
        },
        columns: [
            { title: "Rut", width: "10%" },
            { title: "Nombre / Razón Social", width: "30%" },
            { title: "E-Mail", width: "20%" },
            { title: "Contacto", width: "10%" },
            { title: "Transferencia", width: "10%" },
            { title: "Activo", width: "10%" },
            { title: "<div class='text-center'><i class='fa fa-cog'></i></div>", width: "10%"}
        ],
        "deferRender": true,
        "language": {
            "emptyTable":     "No hay datos disponibles en la tabla",
            "info":           "Mostrando registros del _START_ al _END_ de un total de _TOTAL_ registros",
            "infoEmpty":      "No hay datos disponibles para mostrar en la tabla",
            "infoFiltered":   "(filtrado de un total de _MAX_ registros)",
            "infoPostFix":    "",
            "thousands":      ",",
            "lengthMenu":     "Mostrar _MENU_ registros",
            "loadingRecords": "Cargando...",
            "processing":     "Procesando...",
            "search":         "Buscar: ",
            "zeroRecords":    "<i class='far fa-frown fa-lg'></i> No hay datos que coincidan con la búsqueda <i class='far fa-frown fa-lg'></i>",
            "paginate": {
                "first":      "Primero",
                "last":       "Último",
                "next":       "Siguiente",
                "previous":   "Anterior"
            },
            "aria": {
                "sortAscending":  ": Activar para ordenar la columna de manera ascendente",
                "sortDescending": ": Activar para ordenar la columna de manera descendente"
            }
        },
        "lengthMenu": [
            [10, 15, 20, -1],
            [10, 15, 20, "Todos"] // change per page values here
        ],
        // set the initial value
        "displayLength": 10,
        "paging":   true,
        "ordering": true,
        "info":     true,
        "searching": true,
        "orderClasses": true,
        "cache": true,
    });


    $.fn.dataTable.ext.errMode = 'none';//quita los mensajes de error de la datatable ajax

    $('#tabla_proveedores').on( 'error.dt', function ( e, settings, techNote, message ) {
		console.log( 'An error has been reported by DataTables: ', message );//para mostrar los mensajes de error en la consola del navegador
    } );
    
    jQuery('#tabla_proveedores_filter input').addClass("form-control input-large"); // modify table search input
    jQuery('#tabla_proveedores_wrapper .dataTables_length select').addClass("form-control input-small"); // modify table per page dropdown
    jQuery('#tabla_proveedores_wrapper .dataTables_length select').select2(); // initialize select2 dropdown

    $('#tabla_proveedores_column_toggler input[type="checkbox"]').change(function() {
        /* Get the DataTables object again - this is not a recreation, just a get of the object */
        var iCol = parseInt($(this).attr("data-column"));
        var bVis = oTable.fnSettings().aoColumns[iCol].bVisible;
        oTable.fnSetColumnVis(iCol, (bVis ? false : true));
    });

	$(document).ready(function() {
		App.init(); // initlayout and core plugins        

   		$("#mantenedores").addClass( "active" );
		$("#proveedores-li").addClass( "active" );
		$("#mantenedores-a").append( '<span class="selected"></span>' );

	});

    function ver(idProveedor)
    {
        $.get( '{{ url("proveedores/modal/ver_proveedor") }}/' + idProveedor, function( data ) {
            $( "#modal" ).html( data );
            $( "#modalVer" ).modal();
        });
    }

    function editar(idProveedor)
    {
        $.get( '{{ url("proveedores/modal/editar_proveedor") }}/' + idProveedor, function( data ) {
            $( "#modal" ).html( data );
            $( "#modalEditar" ).modal();
        });
    }

    function nuevo()
    {
        $.get( '{{ url("proveedores/create") }}', function( data ) {
            $( "#modal" ).html( data );
            $( "#modalCrear" ).modal();
        });
    }

    function eliminar(idProveedor)
    {
        $.get( '{{ url("proveedores/modal/eliminar_proveedor") }}/' + idProveedor, function( data ) {
            $( "#modal" ).html( data );
            $( "#modalEliminar" ).modal();
        });
    }

</script>

@endpush