<link href="{{ asset('sistema/css/modales.css') }} " rel="stylesheet" type="text/css"/>
<div class="modal fade" id="modalConsumir" role="dialog" aria-labelledby="myModalLabel3" aria-hidden="true">
	<div class="modal-dialog" style="width:90%;">
		<div class="modal-content">
            <div class="modal-header">
				<button type="button" title="Cerrar" class="close" data-dismiss="modal" aria-hidden="true"></button>
				<h4 class="modal-title" ><strong><i class="fas fa-donate fa-lg"></i>&nbsp;<i class="fas fa-id-card-alt"></i>&nbsp;&nbsp;Consumir Contrato</strong></h4>
            </div>

            <form action="{{ url('contratos/consumir') }}" method="POST" class="horizontal-form" id="form" autocomplete="off">
                <div class="modal-body">
                    <div class="note note-danger" id="divErrores" style="display:none;">
                        <h4 class="block" style="margin-bottom:5 px;">Errores : </h4>
                        <br>
                        <ul id="ulErrores"></ul>
                    </div>

                    <div class="form-group row">
                        <div class="col-xs-6" style="border-right-style: double;">

                            <div class="form-group row">

                                <div class="col-xs-4 form-validate">
                                    <label for="tipo_contrato" class="label-form">Tipo Contrato </label>
                                    <select name="tipo_contrato" id="tipo_contrato" class="form-control select2" required readonly onchange="setTipoContrato(this);">
                                        <option value=''>Seleccione</option>
                                        <option value='1' @if( $contrato->id_tipo_contrato == 1 ) selected @endif >Honorario</option>
                                        <option value='2' @if( $contrato->id_tipo_contrato == 2 ) selected @endif>Convenio</option>
                                    </select>
                                </div>

                                <div class="col-xs-8 form-validate">
                                    <label for="proveedor" class="label-form" >Proveedor </label>
                                    <input type="text" class="form-control" id="proveedor" name="proveedor" required placeholder="Buscar por Nombre o RUT" readonly >
                                </div>
        
                            </div>

                            <div class="form-group row">

                                <div class="col-xs-3 form-validate">
                                    <label for="resolucion" class="label-form">Resolución </label>
                                    <input type="text" class="form-control solo_numeros" id="resolucion" name="resolucion" required value="{{ $contrato->resolucion }}" readonly >
                                </div>
        
                                <div class="col-xs-3 form-validate">
                                    <label for="fecha_resolucion" class="label-form">Fecha Resolución </label>
                                    <div class="input-group">
                                        <input type="text" class="form-control fechas" id="fecha_resolucion" name="fecha_resolucion" required value="{{ fecha_dmY( $contrato->fecha_resolucion ) }}" readonly >
                                        <span class="input-group-addon" onclick="fecha('fecha_resolucion');" style="cursor:pointer;"><i class="fa fa-calendar" aria-hidden="true" style="color: black;"></i></span>
                                    </div>
                                </div>

                                <div class="col-xs-3 form-validate">
                                    <label for="inicio_vigencia" class="label-form">Inicio Vigencia </label>
                                    <div class="input-group">
                                        <input type="text" class="form-control fechas" id="inicio_vigencia" name="inicio_vigencia" required value="{{ fecha_dmY( $contrato->inicio_vigencia ) }}"  readonly >
                                        <span class="input-group-addon" onclick="fecha('inicio_vigencia');" style="cursor:pointer;"><i class="fa fa-calendar" aria-hidden="true" style="color: black;"></i></span>
                                    </div>
                                </div>

                                <div class="col-xs-3  form-validate">
                                    <label for="fin_vigencia" class="label-form">Fin Vigencia </label>
                                    <div class="input-group">
                                        <input type="text" class="form-control fechas" id="fin_vigencia" name="fin_vigencia" required value="{{ fecha_dmY( $contrato->fin_vigencia ) }}" readonly >
                                        <span class="input-group-addon" onclick="fecha('fin_vigencia');" style="cursor:pointer;"><i class="fa fa-calendar" aria-hidden="true" style="color: black;"></i></span>
                                    </div>
                                </div>

                            </div>

                            <div class="form-group row">
                                
                                <div class="col-xs-3 form-validate">
                                    <label for="tipo_adjudicacion" class="label-form">Tipo Adjudicación </label>
                                    <input type="text" class="form-control" id="tipo_adjudicacion" name="tipo_adjudicacion" required value="{{ $contrato->getTipoAdjudicacion->nombre }}" readonly >
                                </div>
        
                                <div class="col-xs-6 form-validate">
                                    <label for="referente_tecnico" class="label-form">Referente Técnico </label>
                                    <input type="text" class="form-control" id="referente_tecnico" name="referente_tecnico" required 
                                            value="{{ $contrato->getReferenteTecnico ? $contrato->getReferenteTecnico->nombre : '' }}" readonly >
                                </div>
        
                            </div>

                            <div class="form-group row">

                                <div class="col-xs-3 col-xs-offset-3 form-validate">
                                    <label for="monto_contrato" class="label-form">Monto Contrato </label>
                                    <div class="input-group">
                                        <span class="input-group-addon">$</span>
                                        <input type="text" class="form-control numero_documento solo_numeros" id="monto_contrato" readonly
                                                name="monto_contrato" required onkeyup="ingresoPesos(this);" autocomplete="off" value="{{ formatoMiles($contrato->monto_contrato) }}" >
                                    </div>
                                </div>

                                <div class="col-xs-3 form-validate">
                                    <label for="saldo_contrato" class="label-form">Saldo Contrato </label>
                                    <div class="input-group">
                                        <span class="input-group-addon">$</span>
                                        <input type="text" class="form-control numero_documento solo_numeros" id="saldo_contrato" readonly
                                                name="saldo_contrato" required onkeyup="ingresoPesos(this);" autocomplete="off" value="{{ formatoMiles($contrato->saldo_contrato) }}" >
                                    </div>
                                </div>
                                
                            </div>

                            <div class="form-group row">

                                <div class="col-xs-12 form-validate">
                                    <label for="detalle_contrato" class="label-form">Detalle </label>
                                    <textarea class="form-control noresize" id="detalle_contrato" name="detalle_contrato" required readonly>{{ $contrato->detalle_contrato }}</textarea>
                                </div>

                            </div>

                            <div class="form-group row" {!! ( $contrato->termino == 0 ) ? 'style="display:none;"' : '' !!} >

                                <div class="col-xs-3 form-validate">
                                    <label for="fecha_termino" class="label-form">Fecha Término </label>
                                    <div class="input-group">
                                        <input type="text" class="form-control fechas" id="fecha_termino" name="fecha_termino" required value="{{ fecha_dmY( $contrato->fecha_termino ) }}" readonly >
                                        <span class="input-group-addon" onclick="fecha('fecha_termino');" style="cursor:pointer;"><i class="fa fa-calendar" aria-hidden="true" style="color: black;"></i></span>
                                    </div>
                                </div>

                                <div class="col-xs-3 form-validate">
                                    <label for="user_termino" class="label-form">Usuario Término </label>
                                    <input type="text" class="form-control" id="user_termino" name="user_termino" required value="{{ ($contrato->getUserTermino) ? $contrato->getUserTermino->name : '' }}" readonly >
                                </div>
        
                            </div>

                            <div class="form-group row" {!! ( $contrato->termino == 0 ) ? 'style="display:none;"' : '' !!}>

                                <div class="col-xs-12 form-validate">
                                    <label for="detalle_termino" class="label-form">Detalle Término</label>
                                    <textarea class="form-control noresize" id="detalle_termino" name="detalle_termino" required readonly>{{ $contrato->detalle_termino }}</textarea>
                                </div>

                            </div>

                            <div class="form-group row">
                                <div class="col-xs-12 table-responsive">
                                    <h4 class="form-section" style="color: #69aa46;margin: 5px 0px 2px 0px;">Documentos en PDF</h4>
        
                                    <table class="table table-striped table-bordered table-hover " id="tabla_info_archivos_existentes">
                                        <thead>
                                            <tr>
                                                <th width="50%">Nombre del Archivo</th>
                                                <th width="20%">Tipo</th>
                                                <th width="10%">Extensión</th>
                                                <th width="10%">Peso</th>
                                                <th width="10%"><i class="fa fa-cog"></i></th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                        @forelse ($contrato->getArchivos as $archivo)
                                            <tr>
                                                <td>{{ $archivo->nombre }}</td>
                                                <td>@if( $archivo->getTipoArchivo ){{ $archivo->getTipoArchivo->nombre }}@endif</td>
                                                <td>{{ $archivo->extension }}</td>
                                                <td>{{ pesoArchivoEnMB($archivo->peso) }}</td>
                                                <td>
                                                    <a class="btn btn-success btn-xs" @if($archivo->cargado == 0) href="{{ asset( $archivo->ubicacion ) }}" @else href="http://{{ $archivo->ubicacion }}" @endif
                                                    title="Ver Archivo" target="_blank" ><i class="fa fa-eye"></i></a>
                                                </td>
                                            </tr>
                                        @empty
        
                                        @endforelse
                                        </tbody>
                                    </table>
                                </div>
                            </div>

                            <div id="convenios" style="display:none;">

                                <div class="form-group row">
                                    
                                    <div class="col-xs-3 form-validate">
                                        <label for="monto_preventivo" class="label-form">Monto Preventivo </label>
                                        <div class="input-group">
                                            <span class="input-group-addon">$</span>
                                            <input type="text" class="form-control numero_documento solo_numeros" id="monto_preventivo" 
                                                    name="monto_preventivo" onkeyup="ingresoPesos(this);" autocomplete="off" readonly
                                                    @if( $contrato->monto_preventivo )
                                                        value="{{ formatoMiles($contrato->monto_preventivo) }}"
                                                    @endif >
                                        </div>
                                    </div>

                                    <div class="col-xs-3 form-validate">
                                        <label for="monto_correctivo" class="control-label label-form">Monto Correctivo </label>
                                        <div class="input-group">
                                            <span class="input-group-addon">$</span>
                                            <input type="text" class="form-control numero_documento solo_numeros" id="monto_correctivo" 
                                                    name="monto_correctivo" onkeyup="ingresoPesos(this);" autocomplete="off" readonly
                                                    @if( $contrato->monto_correctivo ) 
                                                        value="{{ formatoMiles($contrato->monto_correctivo) }}"
                                                    @endif >
                                        </div>
                                    </div>

                                    <div class="col-xs-3 form-validate">
                                        <label for="licitacion" class="label-form">Licitación </label>
                                        <input type="text" class="form-control" id="licitacion" name="licitacion" value="{{ $contrato->licitacion }}" readonly>
                                    </div>

                                    <div class="col-xs-3 form-validate">
                                        <label for="cuotas" class="label-form">Cuotas </label>
                                        <div class="checkbox check" >
                                            <label class="label_checkbox">
                                                <input type="checkbox" class="check_input" id="cuotas" name="cuotas" value="1" readonly @if( $contrato->cuotas == 1 ) checked @endif >
                                            </label>
                                        </div>
                                    </div> 
                                    
                                </div>

                                <div class="form-group row">
                                    
                                    <div class="col-xs-3 form-validate">
                                        <label for="saldo_preventivo" class="label-form">Saldo Preventivo </label>
                                        <div class="input-group">
                                            <span class="input-group-addon">$</span>
                                            <input type="text" class="form-control numero_documento solo_numeros" id="saldo_preventivo" 
                                                    name="saldo_preventivo" onkeyup="ingresoPesos(this);" autocomplete="off" readonly
                                                    @if( $contrato->saldo_preventivo )
                                                        value="{{ formatoMiles($contrato->saldo_preventivo) }}"
                                                    @endif >
                                        </div>
                                    </div>

                                    <div class="col-xs-3 form-validate">
                                        <label for="saldo_correctivo" class="control-label label-form">Saldo Correctivo </label>
                                        <div class="input-group">
                                            <span class="input-group-addon">$</span>
                                            <input type="text" class="form-control numero_documento solo_numeros" id="saldo_correctivo" 
                                                    name="saldo_correctivo" onkeyup="ingresoPesos(this);" autocomplete="off" readonly
                                                    @if( $contrato->saldo_correctivo ) 
                                                        value="{{ formatoMiles($contrato->saldo_correctivo) }}"
                                                    @endif >
                                        </div>
                                    </div>
                                    
                                </div>

                                <div class="row">
                                    <div class="col-xs-12">
                                        <h5 class="form-section" style="color: #69aa46;font-weight: bold !important; ">Boleta Garantía</h5>
                                    </div>
                                </div>

                                <div class="form-group row">
        
                                    <div class="col-xs-3 form-validate">
                                        <label for="numero_boleta_garantia" class="label-form">Número </label>
                                        <input type="text" class="form-control solo_numeros" id="numero_boleta_garantia" readonly
                                                name="numero_boleta_garantia" value="{{ $contrato->numero_boleta_garantia }}" >
                                    </div>
        
                                    <div class="col-xs-3 form-validate">
                                        <label for="monto_boleta_garantia" class="label-form">Monto </label>
                                        <div class="input-group">
                                            <span class="input-group-addon">$</span>
                                            <input type="text" class="form-control numero_documento solo_numeros" id="monto_boleta_garantia" readonly
                                                    name="monto_boleta_garantia" onkeyup="ingresoPesos(this);" autocomplete="off" value="{{ formatoMiles($contrato->monto_boleta_garantia) }}" >
                                        </div>
                                    </div>

                                    <div class="col-xs-3 form-validate">
                                        <label for="inicio_vigencia_boleta_garantia" class="label-form">Inicio Vigencia </label>
                                        <div class="input-group">
                                            <input type="text" class="form-control fechas" id="inicio_vigencia_boleta_garantia" readonly
                                                    name="inicio_vigencia_boleta_garantia"  value="{{ fecha_dmY( $contrato->inicio_vigencia_boleta_garantia ) }}" >
                                            <span class="input-group-addon" onclick="fecha('inicio_vigencia_boleta_garantia');" style="cursor:pointer;">
                                                <i class="fa fa-calendar" aria-hidden="true" style="color: black;"></i>
                                            </span>
                                        </div>
                                    </div>
        
                                    <div class="col-xs-3 form-validate">
                                        <label for="fin_vigencia_boleta_garantia" class="label-form">Fin Vigencia </label>
                                        <div class="input-group">
                                            <input type="text" class="form-control fechas" id="fin_vigencia_boleta_garantia" readonly
                                                    name="fin_vigencia_boleta_garantia" value="{{ fecha_dmY( $contrato->fin_vigencia_boleta_garantia ) }}">
                                            <span class="input-group-addon" onclick="fecha('fin_vigencia_boleta_garantia');" style="cursor:pointer;">
                                                <i class="fa fa-calendar" aria-hidden="true" style="color: black;"></i>
                                            </span>
                                        </div>
                                    </div>
        
                                </div>
        
                                <div class="form-group row">
                                    
                                </div>
        
                            </div>

                            <div id="honorarios" style="display:none;">

                                <div class="form-group row">
                                    
                                    <div class="col-xs-3 form-validate">
                                        <label for="valor_hora" class="label-form"> Valor Hora </label>
                                        <div class="input-group">
                                            <span class="input-group-addon">$</span>
                                            <input type="text" class="form-control numero_documento solo_numeros" id="valor_hora" name="valor_hora" readonly
                                                    onkeyup="ingresoPesos(this);" autocomplete="off" @if( $contrato->valor_hora ) value="{{ formatoMiles($contrato->valor_hora) }}" @endif >
                                        </div>
                                    </div>
        
                                    <div class="col-xs-3 form-validate">
                                        <label for="por_procedimiento" class="label-form">Por procedimiento </label>
                                        <div class="checkbox check" >
                                            <label class="label_checkbox">
                                                <input type="checkbox" class="check_input" id="por_procedimiento" name="por_procedimiento" readonly
                                                        value="1" @if($contrato->por_procedimiento == 1) checked @endif >
                                            </label>
                                        </div>
                                    </div>


                                    <div class="col-xs-3 form-validate">
                                        <label for="unidad" class="label-form">Unidad </label>
                                        <input type="text" class="form-control" id="unidad" name="unidad" required 
                                            value="{{ ($contrato->getUnidad) ? $contrato->getUnidad->nombre : '' }}" readonly >
                                    </div>

                                    <div class="col-xs-3 form-validate">
                                        <label for="prestacion" class="label-form">Prestación </label>
                                        <input type="text" class="form-control" id="prestacion" name="prestacion" required 
                                                value="{{ ($contrato->getTipoPrestacion) ? $contrato->getTipoPrestacion->nombre : '' }}" readonly >
                                    </div> 
        
                                </div>
        
                                <div class="form-group row">
                                    
                                    <div class="col-xs-4 form-validate">
                                        <label for="profesion" class="label-form">Profesión </label>
                                        <input type="text" class="form-control" id="profesion" name="profesion" required 
                                                value="{{ ($contrato->getProfesion) ? $contrato->getProfesion->nombre : '' }}" readonly >
                                    </div>
                                    
                                    <div class="col-xs-3 form-validate">
                                        <label for="categoria" class="label-form">Categoría </label>
                                        <input type="text" class="form-control" id="categoria" name="categoria" 
                                                value="{{ ($contrato->getCategoria) ? $contrato->getCategoria->nombre : '' }}" readonly >
                                    </div>

                                    <div class="col-xs-2 form-validate">
                                        <label for="sueldo_horas" class="label-form">Suledo / Horas </label>
                                        <input type="text" class="form-control" id="sueldo_horas" name="sueldo_horas" required 
                                                value="{{ ($contrato->sueldo_horas) ? $contrato->sueldo_horas : '' }}" readonly >
                                    </div> 
        
                                </div>
        
                                <div class="form-group row">
                                    
                                    <div class="col-xs-3 form-validate">
                                        <label for="addemdum" class="label-form">Addemdum </label>
                                        <input type="text" class="form-control solo_numeros" id="addemdum" name="addemdum" 
                                                value="{{ ($contrato->addemdum) ? $contrato->addemdum : '' }}" readonly >
                                    </div>
        
                                    <div class="col-xs-3 orm-validate">
                                        <label for="fecha_addemdum" class="label-form">Fecha Addemdum </label>
                                        <div class="input-group">
                                            <input type="text" class="form-control fechas" id="fecha_addemdum" name="fecha_addemdum" readonly
                                                @if ( $contrato->fecha_addemdum != null ) value="{{ fecha_dmY( $contrato->fecha_addemdum ) }}" @endif >
                                            <span class="input-group-addon" onclick="fecha('fecha_addemdum');" style="cursor:pointer;"><i class="fa fa-calendar" aria-hidden="true" style="color: black;"></i></span>
                                        </div>
                                    </div>
                                </div>
        
                            </div>

                        </div> <!-- / col-xs-6 -->

                        <div class="col-xs-6" >

                            <div class="form-group row">

                                <div class="col-xs-10 form-validate">
                                    <label for="orden_compra" class="label-form">Orden de Compra {!! ($contrato->getOrdenesCompra->count() > 0) ? '<span class="span-label">*</span>' : '' !!} </label>
                                    <select name="orden_compra" id="orden_compra" class="form-control select2" 
                                            {!! ($contrato->getOrdenesCompra->count() > 0) ? 'required' : '' !!} onchange="setOc(this);" >
                                        <option value=''>Seleccione</option>
                                        @forelse ($contrato->getOrdenesCompra as $oc)
                                            <option value='{{ $oc->id }}' id="oc_{{ $oc->id }}"
                                                    data-inicio-vigencia-oc-periodo="{{ fecha_mY($oc->inicio_vigencia) }}"
                                                    data-fin-vigencia-oc-periodo="{{ fecha_mY($oc->fin_vigencia) }}"
                                                    data-saldo="{{ $oc->saldo_oc  }}"
                                                    dato-monto="{{ $oc->monto_oc }}"
                                                >{{ $oc->numero_oc }}&nbsp;&nbsp;&nbsp;Monto:{{ formatoMiles($oc->monto_oc) }}&nbsp;&nbsp;&nbsp;Saldo: {{ formatoMiles($oc->saldo_oc) }}</option>
                                        @empty
                                            
                                        @endforelse
                                    </select>
                                </div>
        
                            </div>
                            
                            <div class="form-group row">

                                <div class="col-xs-6 form-validate">
                                    <label for="documento" class="label-form">Documento <span class="span-label">*</span> </label>
                                    <select name="documento" id="documento" class="form-control select2" required onchange="setDocumento(this);">
                                        <option value=''>Seleccione</option>
                                        @forelse ($documentos as $doc)
                                            <option value='{{ $doc->id }}' id="doc_{{ $doc->id }}"
                                                data-tipo-documento="{{ $doc->id_tipo_documento }}"
                                                data-total-documento="{{ $doc->total_documento }}"
                                                >
                                                {{ $doc->numero_documento }}&nbsp;&nbsp;&nbsp;{{ $doc->getTipoDocumento->nombre }}&nbsp;&nbsp;&nbsp;{{ formatoMiles($doc->total_documento) }}
                                            </option>
                                        @empty
                                            
                                        @endforelse
                                    </select>
                                </div>
        
                            </div>

                            <div id="mostrar_segun_doc" style="display:none;">

                                <div class="form-group row">

                                    <div class="col-xs-3 form-validate" >
                                        <label for="periodo" class="label-form">Periodo  <span class="span-label">*</span></label>
                                        <div class="input-group" id="div_periodo">
                                            <input type="text" class="form-control " id="periodo" name="periodo" required >
                                            <span class="input-group-addon " onclick="fecha('periodo');" style="cursor:pointer;">
                                                <i class="fa fa-calendar" aria-hidden="true" style="color: black;"></i>
                                            </span>
                                        </div>
                                    </div>

                                </div>

                                <div class="form-group row" >
                                    <div id="ocultar_segun_valor_hora" class="col-xs-4 form-validate">
                                        <label for="horas_boleta" class="label-form">Horas <span class="span-label">*</span></label>                            
                                        <input type="text" class="form-control numero_documento" id="horas_boleta" name="horas_boleta" required onkeyup="ingresoHoras(this);" autocomplete="off" >
                                    </div>
                                </div>

                                <div id="ocultar_segun_saldos_preventivos_correctivos">
                
                                    <div class="form-group row">
                                        <label class="col-xs-3 label-form" style="text-align:left;">Saldo a ocupar <span class="span-label">*</span></label>
                                        <div class="col-xs-9 form-validate" style="margin-top: -8px;">
                                            <div class="radio-list">
                
                                                <label class="radio-inline">
                                                    <div class="radio" >
                                                        <span class=""><input type="radio" name="montoOcupar" id="ocuparPreventivo" value="preventivo" onchange="montoOcuparPreventivoCorrectivo();" required></span>
                                                    </div>Preventivo</label>
                
                                                <label class="radio-inline">
                                                    <div class="radio" >
                                                        <span class=""><input type="radio" name="montoOcupar" id="ocuparCorrectivo" value="correctivo" onchange="montoOcuparPreventivoCorrectivo();" required></span>
                                                    </div>Correctivo</label>
                
                                            </div>
                                        </div>
                                    </div>
                
                                </div> <!-- /ocultar_segun_saldos_preventivos_correctivos -->

                            </div>

                        </div> <!-- / col-xs-6 -->
                    </div>
                                    
                </div>

                <div class="modal-footer form-actions right">
                    <input type="hidden" name="_token" value="{{ csrf_token() }}">
                    <input type="hidden" name="_id" value="{{ $contrato->id }}">

                    <button type="button" title="Cerrar" class="btn btn-default" data-dismiss="modal">Cerrar</button>
                    <button type="submit" title="Consumir" class="btn btn-danger" id="botonGuardar" >
                        <i class="far fa-save" style="color:black;"></i> Consumir</button>
                </div>
            </form>

        </div>
    </div>
</div>

<script src="{{asset('sistema/js/contratos.js')}}?v={{rand()}}"></script>
<script>
    $(document).ready(function() {

        @if( $contrato->por_procedimiento == 1 )
            setValorHora();
        @endif

        @if( $contrato->valor_hora )
            setPorProcedimiento();
        @endif

        $(".solo_numeros").keypress(function (key) {
            // window.console.log(key.charCode); //Descomentar para ver codigo
            if (
                (key.charCode < 48 || key.charCode > 57)//números
                && (key.charCode != 0) //borrar y enter
                && (key.charCode != 44) //coma
                && (key.charCode != 46) //punto
                && (key.charCode != 45) //guion
                && (key.charCode != 43) //suma
                )
                //console.log(key.charCode);
                return false;
        });

        $('.fechas').datepicker({
            format: 'dd/mm/yyyy',
            // endDate: new Date(),
            autoclose: true,
            language: 'es'
        });

        $('#periodo').datepicker({
            format: 'mm/yyyy',
            // endDate: new Date(),
            startDate: '{{ fecha_mY($contrato->inicio_vigencia) }}',
            endDate: '{{ fecha_mY($contrato->fin_vigencia) }}',
            autoclose: true,
            language: 'es',
            viewMode: 'months',
            minViewMode: 'months'
        });

        $("#proveedor").select2({
            ajax: {
                cache: true,
                allowClear: true,
                //hidden : true,
                url : '{{ url('buscador_proveedor/') }}',
                dataType: 'json',
                delay: 250,
                data: function (params,page) {
                    //console.log("soy params : "+params);
                    //console.log("soy page : "+page);
                    var query = {
                        term: params,
                        page: params.page || 1
                    }
                    return query;
                },
                results: function (data) {
                    return {
                    results: data
                    };
                },
            },
            minimumInputLength: 2,
        
        });

        $(".select2").select2();

        $("#form").validate({
            highlight: function(element) {
                $(element).closest('.form-validate').removeClass('has-success');
                $(element).closest('.form-validate').addClass('has-error');
            },
            unhighlight: function(element) {
                $(element).closest('.form-validate').removeClass('has-error');
                $(element).closest('.form-validate').addClass('has-success');
            },
            errorElement: 'span',
            errorClass: 'help-block',
            errorPlacement: function (error, element) {
                console.log(element);
                if (element.parent('.input-group').length) {
                    error.insertAfter(element.parent());
                } else {
                    error.insertAfter(element);
                }
            },
            rules: {
            },
            messages: {
            },

            //para enviar el formulario por ajax
            submitHandler: function(form) {
                esperandoCrear();

                let formData = new FormData(form);
                
                $.ajax({
                    url: form.action,
                    type: form.method,
                    //data: $(form).serialize(),
                    data: formData,
                    processData: false,
                    contentType: false,
                    success: function(respuesta) {
                        console.log(respuesta);

                        if ( respuesta.estado == 'error' ) {
                            toastr.error('No es posible realizar la acción'+'<br><strong>Error: '+ respuesta.mensaje +'</strong>', 'Atención', optionsToastr);
                        } else if ( respuesta.estado == 'success') {
                            
                            toastr.success(respuesta.mensaje, 'Atención', optionsToastr);

                            if ( respuesta.estado_archivo == 'error' ) {
                                toastr.error('No es posible realizar la acción'+'<br><strong>Error: '+ respuesta.mensaje_archivo +'</strong>', 'Atención', optionsToastr);
                            } else if ( respuesta.estado_archivo == 'succes') {
                                toastr.success(respuesta.mensaje_archivo, 'Atención', optionsToastr);
                            }

                            // Cambiar informacion de la tabla
                            $('#tabla_principal').DataTable().ajax.reload();
                            $("#modalConsumir").modal("hide");

                        }
                        
                    }            
                }).fail( function(respuesta) {//fail ajax
                    if ( respuesta.status == 400 ) {
                        mostrarErroresValidator(respuesta);
                    } else if ( respuesta.status == 500 ) {
                        toastr.error('No es posible realizar la acción, error en el servidor', 'Atención', optionsToastr);
                    } else {
                        toastr.error('No es posible realizar la acción', 'Atención', optionsToastr);
                    }
                    
                })
                .always(function() {
                    listoCrear();
                });//ajax
                
            }

        });

        $('#proveedor').select2('data', { id: '{!! $contrato->id_proveedor !!}', text: '{!!$contrato->getProveedor->formatRut() !!} {!!$contrato->getProveedor->nombre !!}' });

        $('#tipo_contrato').select2('val', '{{ $contrato->id_tipo_contrato }}').trigger('change');
        $('#tipo_contrato').select2('readonly', true);
        
    });

    var tablaArchivosExistentes = $('#tabla_info_archivos_existentes').DataTable({
        "language": {
            "emptyTable": "No hay datos disponibles en la tabla",
        },
        "paging":   false,
        "ordering": false,
        "info":     false,
        "searching": false
    });

    var montoMinimoContrato = '{!! $contrato->getMontoMinimo() !!}';
    var montoMinimoPreventivo = '{!! $contrato->getMontoMinimoPreventivo() !!}';
    var montoMinimoCorrectivo = '{!! $contrato->getMontoMinimoCorrectivo() !!}';

    var inicioVigencia = '{{ fecha_mY($contrato->inicio_vigencia) }}';
    var finVigencia = '{{ fecha_mY($contrato->fin_vigencia) }}';
    var totalDocumento = 0;
    var tipoDocumento = 0;
    var saldoOc = 'Sin saldo';
    var montoOc = 'Sin monto';

    function montoOcuparPreventivoCorrectivo() {

        let montoPreventivo = ! isNaN( parseInt($('#monto_preventivo').val().replace(/\./g, '')) ) ? parseInt($('#monto_preventivo').val().replace(/\./g, '')) : 0;
        let montoCorrectivo = ! isNaN( parseInt($('#monto_correctivo').val().replace(/\./g, '')) ) ? parseInt($('#monto_correctivo').val().replace(/\./g, '')) : 0;
        let saldoPreventivo = ! isNaN( parseInt($('#saldo_preventivo').val().replace(/\./g, '')) ) ? parseInt($('#saldo_preventivo').val().replace(/\./g, '')) : 0;
        let saldoCorrectivo = ! isNaN( parseInt($('#saldo_correctivo').val().replace(/\./g, '')) ) ? parseInt($('#saldo_correctivo').val().replace(/\./g, '')) : 0;
        
        if ( totalDocumento != 0 && $('#saldo_preventivo').val() != '' || $('#saldo_correctivo').val() != '' ) {

            if ( $('input:radio[name=montoOcupar]:checked').val() === 'preventivo' ) {

                if ( tipoDocumento == 4 || tipoDocumento == 10 ) {

                    if ( ( totalDocumento + saldoPreventivo > montoPreventivo ) ) {

                        $('#documento').select2('val', '').trigger('change');           
                        toastr.warning('El total del documento más el saldo preventivo no puede superar el monto preventivo', 'Atención', optionsToastr);                    

                    }

                } else {

                    if ( ( totalDocumento > saldoPreventivo ) ) {

                        $('#documento').select2('val', '').trigger('change');
                        toastr.warning('El total del documento seleccionado supera el Saldo Preventivo', 'Atención', optionsToastr);

                    }

                }

            } else if ( $('input:radio[name=montoOcupar]:checked').val() === 'correctivo' ) {

                if ( tipoDocumento == 4 || tipoDocumento == 10 ) {

                    if ( ( totalDocumento + saldoCorrectivo > montoCorrectivo ) ) {

                        $('#documento').select2('val', '').trigger('change');
                        toastr.warning('El total del documento seleccionado más el saldo correctivo no puede superar el monto correctivo', 'Atención', optionsToastr);

                    }

                } else {

                    if ( ( totalDocumento > saldoCorrectivo ) ) {

                        $('#documento').select2('val', '').trigger('change');
                        toastr.warning('El total del documento seleccionado supera el Saldo Correctivo', 'Atención', optionsToastr);

                    }

                }

            }

        }

    }

    function setDocumento(input = null) {

        if ( input != null && input.value != '') {

            var documento = document.getElementById('doc_' + input.value);

            totalDocumento = parseInt( documento.dataset.totalDocumento );
            tipoDocumento = parseInt( documento.dataset.tipoDocumento );

            if ( saldoOc != 'Sin saldo' && montoOc != 'Sin monto' ) {

                evaluarMontoSaldoOc();

            } 

            $('#mostrar_segun_doc').css('display','');

            if ( $('#saldo_preventivo').val() != '' || $('#saldo_correctivo').val() != '' ) {
                montoOcuparPreventivoCorrectivo();
                $('#ocultar_segun_saldos_preventivos_correctivos').css('display', '');
            } else {
                $('#ocultar_segun_saldos_preventivos_correctivos').css('display', 'none');
            }

            if ( $('#por_procedimiento').prop('checked') ) {
                $('#ocultar_segun_valor_hora').css('display', 'none');
            } else if ( $('#valor_hora').val() != 0 && $('#valor_hora').val() != '' ) {
                $('#ocultar_segun_valor_hora').css('display', '');
            } else {
                $('#ocultar_segun_valor_hora').css('display', 'none');
            }

        } else {

            totalDocumento = 0;
            tipoDocumento = 0;
            $('#mostrar_segun_doc').css('display','none');
            $('#ocultar_segun_saldos_preventivos_correctivos').css('display', 'none');
            $('#ocultar_segun_valor_hora').css('display', 'none');
            $('#ocuparPreventivo').prop('checked', false);
            $('#ocuparCorrectivo').prop('checked', false);

        }

    }

    /*
    * Para cambiar el rango de tiempo seleccionable en el periodo
    */
    function setOc(input = null) {

        let periodo = '<input type="text" class="form-control" id="periodo" name="periodo" required >';
            periodo += '<span class="input-group-addon " onclick="fecha(\'periodo\');" style="cursor:pointer;">';
            periodo += '<i class="fa fa-calendar" aria-hidden="true" style="color: black;"></i></span>';

        $('#div_periodo').html(periodo);

        if ( input != null && input.value != '') {

            var oc = document.getElementById('oc_' + input.value);
            saldoOc = parseInt( oc.dataset.saldo );
            montoOc = parseInt( oc.dataset.monto );

            if ( totalDocumento != 0 ) {

                evaluarMontoSaldoOc();

            }

            $('#periodo').datepicker({
                format: 'mm/yyyy',
                startDate: oc.dataset.inicioVigenciaOcPeriodo,
                endDate: oc.dataset.finVigenciaOcPeriodo,
                autoclose: true,
                language: 'es',
                viewMode: 'months',
                minViewMode: 'months'
            });

        } else {

            $('#periodo').datepicker({
                format: 'mm/yyyy',
                startDate: inicioVigencia,
                endDate: finVigencia,
                autoclose: true,
                language: 'es',
                viewMode: 'months',
                minViewMode: 'months'
            });

            saldoOc = 'Sin saldo';
            montoOc = 'Sin monto';

        }

    }

    function evaluarMontoSaldoOc() {

        if ( tipoDocumento == 4 || tipoDocumento == 10 ) {

            if ( ( totalDocumento + saldoOc > montoOc ) ) {

                $('#documento').select2('val', '').trigger('change');           
                toastr.warning('El total del documento más el saldo oc no puede superar el monto oc', 'Atención', optionsToastr);                    

            }

        } else {

            if ( totalDocumento > saldoOc ) {

                $('#documento').select2('val', '').trigger('change');
                toastr.warning('El total del documento seleccionado supera el saldo de la OC seleccionada', 'Atención', optionsToastr);

            }

        }

    }

</script>