<link href="{{ asset('sistema/css/modales.css') }} " rel="stylesheet" type="text/css"/>
<div class="modal fade" id="modalCrear" role="dialog" aria-labelledby="myModalLabel3" aria-hidden="true">
	<div class="modal-dialog" style="width:1100px;">
		<div class="modal-content">
            <div class="modal-header">
				<button type="button" title="Cerrar" class="close" data-dismiss="modal" aria-hidden="true"></button>
				<h4 class="modal-title" ><strong><i class="fas fa-id-card-alt"></i>&nbsp;&nbsp;Crear Contrato</strong></h4>
            </div>

            <form action="{{ url('contratos') }}" method="POST" class="horizontal-form" id="form" autocomplete="off">
                <div class="modal-body">
                    <div class="form-body">
                        <div class="note note-danger" id="divErrores" style="display:none;">
                            <h4 class="block" style="margin-bottom:5 px;">Errores: </h4>
                            <br>
                            <ul id="ulErrores"></ul>
                        </div>

                        <div class="form-group row">
                                
                            <label for="tipo_contrato" class="col-sm-2 control-label label-form">Tipo Contrato <span class="span-label">*</span></label>
                            <div class="col-sm-3 form-validate">
                                <select name="tipo_contrato" id="tipo_contrato" class="form-control select2" required onchange="setTipoContrato(this);">
                                    <option value=''>Seleccione</option>
                                    <option value='1'>Honorario</option>
                                    <option value='2'>Convenio</option>
                                </select>
                            </div>

                        </div>

                        <div class="form-group row">
                            <label for="proveedor" class="col-sm-2 control-label label-form" >Proveedor <span class="span-label">*</span></label>
                            <div class="col-sm-8 form-validate">
                                <input type="text" class="form-control" id="proveedor" name="proveedor" required placeholder="Buscar por Nombre o RUT" >
                            </div>
                        </div>

                        <div class="form-group row">
                            <label for="resolucion" class="col-sm-2 control-label label-form">Resolución <span class="span-label">*</span></label>
                            <div class="col-sm-3 form-validate">
                                <input type="text" class="form-control solo_numeros" id="resolucion" name="resolucion" required >
                            </div>

                            <label for="fecha_resolucion" class="col-xs-2 col-sm-2 col-md-2 col-lg-2 col-md-offset-1 control-label label-form">Fecha Resolución <span class="span-label">*</span></label>
                            <div class="col-xs-2 col-sm-2 col-md-2 col-lg-2 form-validate">
                                <div class="input-group">
                                    <input type="text" class="form-control fechas" id="fecha_resolucion" name="fecha_resolucion" required value="{{ date('d/m/Y') }}"  >
                                    <span class="input-group-addon" onclick="fecha('fecha_resolucion');" style="cursor:pointer;"><i class="fa fa-calendar" aria-hidden="true" style="color: black;"></i></span>
                                </div>
                            </div>
                        </div>

                        <div class="form-group row">
                            <label for="inicio_vigencia" class="col-xs-2 col-sm-2 col-md-2 col-lg-2 control-label label-form">Inicio Vigencia <span class="span-label">*</span></label>
                            <div class="col-xs-2 col-sm-2 col-md-2 col-lg-2 form-validate">
                                <div class="input-group">
                                    <input type="text" class="form-control fechas" id="inicio_vigencia" name="inicio_vigencia" required value="{{ date('d/m/Y') }}"  >
                                    <span class="input-group-addon" onclick="fecha('inicio_vigencia');" style="cursor:pointer;"><i class="fa fa-calendar" aria-hidden="true" style="color: black;"></i></span>
                                </div>
                            </div>

                            <label for="fin_vigencia" class="col-xs-2 col-sm-2 col-md-2 col-lg-2 col-md-offset-2 control-label label-form">Fin Vigencia <span class="span-label">*</span></label>
                            <div class="col-xs-2 col-sm-2 col-md-2 col-lg-2 form-validate">
                                <div class="input-group">
                                    <input type="text" class="form-control fechas" id="fin_vigencia" name="fin_vigencia" required value="{{ date('d/m/Y') }}"  >
                                    <span class="input-group-addon" onclick="fecha('fin_vigencia');" style="cursor:pointer;"><i class="fa fa-calendar" aria-hidden="true" style="color: black;"></i></span>
                                </div>
                            </div>
                        </div>

                        <div class="form-group row">

                            <label for="monto_contrato" class="col-xs-2 col-sm-2 col-md-2 col-lg-2 control-label label-form">Monto Contrato <span class="span-label">*</span></label>
                            <div class="col-xs-3 col-sm-3 col-md-3 col-lg-3 form-validate">
                                <div class="input-group">
                                    <span class="input-group-addon">$</span>
                                    <input type="text" class="form-control numero_documento solo_numeros" id="monto_contrato" name="monto_contrato" required value="0" onkeyup="ingresoPesos(this);" autocomplete="off" >
                                </div>
                            </div>

                        </div>

                        <div class="form-group row">
                                
                            <label for="tipo_adjudicacion" class="col-sm-2 control-label label-form">Tipo Adjudicación <span class="span-label">*</span></label>
                            <div class="col-sm-3 form-validate">
                                <select name="tipo_adjudicacion" id="tipo_adjudicacion" class="form-control select2" required >
                                    <option value=''>Seleccione</option>
                                    @foreach ( $tiposAdjudicacion as $adjudicacion)
                                        <option value="{{ $adjudicacion->id }}">{{ $adjudicacion->nombre }}</option>
                                    @endforeach
                                </select>
                            </div>

                            <label for="referente_tecnico" class="col-sm-2 control-label label-form">Referente Técnico <!--<span class="span-label">*</span>--></label>
                            <div class="col-sm-3 form-validate">
                                <select name="referente_tecnico" id="referente_tecnico" class="form-control select2"  >
                                    <option value=''>Seleccione</option>
                                    @foreach ( $referentes as $referente)
                                        <option value="{{ $referente->id }}">{{ $referente->nombre }}</option>
                                    @endforeach
                                </select>
                            </div>

                        </div>

                        <label for="detalle_contrato" class="col-xs-2 col-sm-2 col-md-2 col-lg-2 control-label label-form">Detalle <span class="span-label">*</span></label>
                        <div class="form-group row">
                            <div class="col-sm-9 col-md-offset-1 form-validate">
                                <textarea class="form-control noresize" id="detalle_contrato" name="detalle_contrato" required></textarea>
                            </div>
                        </div>

                        
                        <div class="form-group row">
                            <div class="input-group col-sm-9 col-sm-offset-1">
                                <span class="input-group-addon" id="inputGroupFileAddon01"><strong>Seleccione Documento</strong></span>
                                <input type="file" class="form-control" accept=".pdf" name="archivo" id="archivo" aria-describedby="inputGroupFileAddon01" onchange="validarArchivo();">
                            </div>
                        </div>

                        <div class="form-group row">
                            <div id="div_archivo" class="col-xs-9 col-sm-offset-1 table-responsive" style="display: none;">
                                <table class="table table-striped table-bordered table-hover " id="tabla_info_archivo">
                                    <thead>
                                        <tr>
                                            <th width="40%">Nombre</th>
                                            <th width="20%">Peso</th>
                                            <th width="40%">Tipo</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                    </tbody>
                                </table>
                            </div>
                        </div> 
                       

                        <div id="convenios" style="display:none;">

                            <div class="form-group row">
                                
                                <label for="monto_preventivo" class="col-xs-2 col-sm-2 col-md-2 col-lg-2 control-label label-form">Monto Preventivo </label>
                                <div class="col-xs-3 col-sm-3 col-md-3 col-lg-3 form-validate">
                                    <div class="input-group">
                                        <span class="input-group-addon">$</span>
                                        <input type="text" class="form-control numero_documento solo_numeros" id="monto_preventivo" name="monto_preventivo" onkeyup="ingresoPesos(this);" autocomplete="off" >
                                    </div>
                                </div>

                                <label for="monto_correctivo" class="col-xs-2 col-sm-2 col-md-2 col-lg-2 control-label label-form">Monto Correctivo </label>
                                <div class="col-xs-3 col-sm-3 col-md-3 col-lg-3 form-validate">
                                    <div class="input-group">
                                        <span class="input-group-addon">$</span>
                                        <input type="text" class="form-control numero_documento solo_numeros" id="monto_correctivo" name="monto_correctivo" onkeyup="ingresoPesos(this);" autocomplete="off" >
                                    </div>
                                </div>
                                
                            </div>

                            <div class="form-group row">

                                <label for="licitacion" class="col-sm-2 control-label label-form">Licitación </label>
                                <div class="col-sm-3 form-validate">
                                    <input type="text" class="form-control" id="licitacion" name="licitacion" >
                                </div>

                                <label for="cuotas" class="col-xs-2 col-sm-2 col-md-2 col-lg-2 col-md-offset-1  control-label label-form">Cuotas </label>
                                <div class="col-sm-4 form-validate">
                                <div class="checkbox check" >
                                    <label class="label_checkbox">
                                        <input type="checkbox" class="check_input" id="cuotas" name="cuotas" value="1" >
                                    </label>
                                    </div>
                                </div>                            
    
                            </div>

                            <h5 class="form-section" style="color: #69aa46;font-weight: bold !important; ">Boleta Garantía</h5>

                            <div class="form-group row">
                                <label for="numero_boleta_garantia" class="col-sm-2 control-label label-form">Número </label>
                                <div class="col-sm-3 form-validate">
                                    <input type="text" class="form-control solo_numeros" id="numero_boleta_garantia" name="numero_boleta_garantia" >
                                </div>
    
                                <label for="monto_boleta_garantia" class="col-xs-2 col-sm-2 col-md-2 col-lg-2 control-label label-form">Monto </label>
                                <div class="col-xs-3 col-sm-3 col-md-3 col-lg-3 form-validate">
                                    <div class="input-group">
                                        <span class="input-group-addon">$</span>
                                        <input type="text" class="form-control numero_documento solo_numeros" id="monto_boleta_garantia" name="monto_boleta_garantia" onkeyup="ingresoPesos(this);" autocomplete="off" >
                                    </div>
                                </div>

                            </div>

                            <div class="form-group row">
                                <label for="inicio_vigencia_boleta_garantia" class="col-xs-2 col-sm-2 col-md-2 col-lg-2 control-label label-form">Inicio Vigencia </label>
                                <div class="col-xs-2 col-sm-2 col-md-2 col-lg-2 form-validate">
                                    <div class="input-group">
                                        <input type="text" class="form-control fechas" id="inicio_vigencia_boleta_garantia" name="inicio_vigencia_boleta_garantia"  >
                                        <span class="input-group-addon" onclick="fecha('inicio_vigencia_boleta_garantia');" style="cursor:pointer;"><i class="fa fa-calendar" aria-hidden="true" style="color: black;"></i></span>
                                    </div>
                                </div>
    
                                <label for="fin_vigencia_boleta_garantia" class="col-xs-2 col-sm-2 col-md-2 col-lg-2 col-md-offset-2 control-label label-form">Fin Vigencia </label>
                                <div class="col-xs-2 col-sm-2 col-md-2 col-lg-2 form-validate">
                                    <div class="input-group">
                                        <input type="text" class="form-control fechas" id="fin_vigencia_boleta_garantia" name="fin_vigencia_boleta_garantia" >
                                        <span class="input-group-addon" onclick="fecha('fin_vigencia_boleta_garantia');" style="cursor:pointer;"><i class="fa fa-calendar" aria-hidden="true" style="color: black;"></i></span>
                                    </div>
                                </div>
                            </div>

                        </div>
                        

                        <div id="honorarios" style="display:none;">

                            <div class="form-group row">

                                <label for="valor_hora" class="col-xs-2 col-sm-2 col-md-2 col-lg-2 control-label label-form"> Valor Hora </label>
                                <div class="col-xs-3 col-sm-3 col-md-3 col-lg-3 form-validate">
                                    <div class="input-group">
                                        <span class="input-group-addon">$</span>
                                        <input type="text" class="form-control numero_documento solo_numeros" id="valor_hora" name="valor_hora" onkeyup="ingresoPesos(this);" autocomplete="off" >
                                    </div>
                                </div>

                                <label for="por_procedimiento" class="col-xs-2 col-sm-2 col-md-2 col-lg-2 col-md-offset-1  control-label label-form">Por procedimiento </label>
                                <div class="col-sm-4 form-validate">
                                <div class="checkbox check" >
                                    <label class="label_checkbox">
                                        <input type="checkbox" class="check_input" id="por_procedimiento" name="por_procedimiento" value="1" onchange="setValorHora();">
                                    </label>
                                    </div>
                                </div>                            
    
                            </div>


                            <div class="form-group row">
                                
                                <label for="unidad" class="col-sm-2 control-label label-form">Unidad <span class="span-label">*</span></label>
                                <div class="col-sm-3 form-validate">
                                    <select name="unidad" id="unidad" class="form-control select2 requerido" >
                                        <option value=''>Seleccione</option>
                                        @foreach ( $unidades as $unidad)
                                            <option value="{{ $unidad->id }}">{{ $unidad->nombre }}</option>
                                        @endforeach
                                    </select>
                                </div>

                            </div>

                            <div class="form-group row">

                                <label for="profesion" class="col-sm-2 control-label label-form">Profesión <span class="span-label">*</span></label>
                                <div class="col-sm-3 form-validate">
                                    <select name="profesion" id="profesion" class="form-control select2 requerido"  >
                                        <option value=''>Seleccione</option>
                                        @foreach ( $profesiones as $profesion)
                                            <option value="{{ $profesion->id }}">{{ $profesion->nombre }}</option>
                                        @endforeach
                                    </select>
                                </div> 
                                
                                <label for="categoria" class="col-sm-2 control-label label-form">Categoría </label>
                                <div class="col-sm-3 form-validate">
                                    <select name="categoria" id="categoria" class="form-control select2"  >
                                        <option value=''>Seleccione</option>
                                        @foreach ( $categorias as $categoria)
                                            <option value="{{ $categoria->id }}">{{ $categoria->nombre }}</option>
                                        @endforeach
                                    </select>
                                </div> 

                            </div>

                            <div class="form-group row">

                                <label for="prestacion" class="col-sm-2 control-label label-form">Prestación <span class="span-label">*</span></label>
                                <div class="col-sm-3 form-validate">
                                    <select name="prestacion" id="prestacion" class="form-control select2 requerido"  >
                                        <option value=''>Seleccione</option>
                                        @foreach ( $tiposPrestacion as $prestacion)
                                            <option value="{{ $prestacion->id }}">{{ $prestacion->nombre }}</option>
                                        @endforeach
                                    </select>
                                </div> 
                                
                                <label for="sueldo_horas" class="col-sm-2 control-label label-form">Suledo / Horas <span class="span-label">*</span></label>
                                <div class="col-sm-3 form-validate">
                                    <select name="sueldo_horas" id="sueldo_horas" class="form-control select2 requerido"  >
                                        <option value=''>Seleccione</option>
                                        <option value="Sueldo" >Sueldo</option>
                                        <option value="Horas" >Horas</option>

                                    </select>
                                </div> 

                            </div>

                            <div class="form-group row">
                                <label for="addemdum" class="col-sm-2 control-label label-form">Addemdum </label>
                                <div class="col-sm-3 form-validate">
                                    <input type="text" class="form-control solo_numeros" id="addemdum" name="addemdum"  >
                                </div>

                                <label for="fecha_addemdum" class="col-xs-2 col-sm-2 col-md-2 col-lg-2 col-md-offset-1 control-label label-form">Fecha Addemdum </label>
                                <div class="col-xs-2 col-sm-2 col-md-2 col-lg-2 form-validate">
                                    <div class="input-group">
                                        <input type="text" class="form-control fechas" id="fecha_addemdum" name="fecha_addemdum" value=""  >
                                        <span class="input-group-addon" onclick="fecha('fecha_addemdum');" style="cursor:pointer;"><i class="fa fa-calendar" aria-hidden="true" style="color: black;"></i></span>
                                    </div>
                                </div>
                            </div>

                        </div>

                    </div>
                </div>

                <div class="modal-footer form-actions right">
                    <input type="hidden" name="_token" value="{{ csrf_token() }}">

                    <button type="button" title="Cancelar" class="btn btn-default" data-dismiss="modal">Cancelar</button>
                    <button type="submit" title="Crear contrato" class="btn btn-success" id="botonGuardar" style="display:none;"><i class="far fa-save" style="color:black;"></i> Crear contrato</button>
                </div>
            </form>

        </div>
    </div>
</div>

<script src="{{asset('sistema/js/contratos.js')}}?v={{rand()}}"></script>
<script>
    $(document).ready(function() {

        $(".solo_numeros").keypress(function (key) {
            // window.console.log(key.charCode); //Descomentar para ver codigo
            if (
                (key.charCode < 48 || key.charCode > 57)//números
                && (key.charCode != 0) //borrar y enter
                && (key.charCode != 44) //coma
                && (key.charCode != 46) //punto
                && (key.charCode != 45) //guion
                && (key.charCode != 43) //suma
                )
                //console.log(key.charCode);
                return false;
        });

        $('.fechas').datepicker({
            format: 'dd/mm/yyyy',
            // endDate: new Date(),
            autoclose: true,
            language: 'es'
        });

        $("#proveedor").select2({
            ajax: {
                cache: true,
                allowClear: true,
                //hidden : true,
                url : '{{ url('buscador_proveedor/') }}',
                dataType: 'json',
                delay: 250,
                data: function (params,page) {
                    //console.log("soy params : "+params);
                    //console.log("soy page : "+page);
                    var query = {
                        term: params,
                        page: params.page || 1
                    }
                    return query;
                },
                results: function (data) {
                    return {
                    results: data
                    };
                },
            },
            minimumInputLength: 2,
        
        });

        $(".select2").select2();

        $("#form").validate({
            highlight: function(element) {
                $(element).closest('.form-validate').removeClass('has-success');
                $(element).closest('.form-validate').addClass('has-error');
            },
            unhighlight: function(element) {
                $(element).closest('.form-validate').removeClass('has-error');
                $(element).closest('.form-validate').addClass('has-success');
            },
            errorElement: 'span',
            errorClass: 'help-block',
            errorPlacement: function (error, element) {
                console.log(element);
                if (element.parent('.input-group').length) {
                    error.insertAfter(element.parent());
                } else {
                    error.insertAfter(element);
                }
            },
            rules: {
            },
            messages: {
            },

            //para enviar el formulario por ajax
            submitHandler: function(form) {
                esperandoCrear();

                let formData = new FormData(form);
                
                $.ajax({
                    url: form.action,
                    type: form.method,
                    //data: $(form).serialize(),
                    data: formData,
                    processData: false,
                    contentType: false,
                    success: function(respuesta) {
                        console.log(respuesta);

                        if ( respuesta.estado == 'error' ) {
                            toastr.error('No es posible realizar la acción'+'<br><strong>Error: '+ respuesta.mensaje +'</strong>', 'Atención', optionsToastr);
                        } else if ( respuesta.estado == 'success') {
                            
                            toastr.success(respuesta.mensaje, 'Atención', optionsToastr);

                            if ( respuesta.estado_archivo == 'error' ) {
                                toastr.error('No es posible realizar la acción'+'<br><strong>Error: '+ respuesta.mensaje_archivo +'</strong>', 'Atención', optionsToastr);
                            } else if ( respuesta.estado_archivo == 'succes') {
                                toastr.success(respuesta.mensaje_archivo, 'Atención', optionsToastr);
                            }

                            // Agregar informacion a la tabla
                            $('#tabla_principal').DataTable().ajax.reload();
                            $("#modalCrear").modal("hide");

                        }
                        
                    }            
                }).fail( function(respuesta) {//fail ajax
                    if ( respuesta.status == 400 ) {
                        mostrarErroresValidator(respuesta);
                    } else if ( respuesta.status == 500 ) {
                        toastr.error('No es posible realizar la acción, error en el servidor', 'Atención', optionsToastr);
                    } else {
                        toastr.error('No es posible realizar la acción', 'Atención', optionsToastr);
                    }
                    
                })
                .always(function() {
                    listoCrear();
                });//ajax
                
            }

        });

    });

    var selectTipoArchivo = '<select name="tipo_archivo" id="tipo_archivo" class="col-xs-12" required>';
    selectTipoArchivo += '<option value="">Seleccione</option>';
    @foreach ( $tiposArchivo as $tipoArchivo)
        selectTipoArchivo += '<option value="{{ $tipoArchivo->id }}">{{ $tipoArchivo->nombre }}</option>';
    @endforeach
    selectTipoArchivo += '</select>';

    var tablaArchivo = $('#tabla_info_archivo').DataTable({
        "paging":   false,
        "ordering": false,
        "info":     false,
        "searching": false
    });

    var montoMinimoContrato = '0';
    var montoMinimoPreventivo = '0';
    var montoMinimoCorrectivo = '0';

</script>