<link href="{{ asset('sistema/css/modales.css') }} " rel="stylesheet" type="text/css"/>
<div class="modal fade" id="modalVer" role="dialog" aria-labelledby="myModalLabel3" aria-hidden="true">
	<div class="modal-dialog" style="width:90%;">
		<div class="modal-content">
            <div class="modal-header">
				<button type="button" title="Cerrar" class="close" data-dismiss="modal" aria-hidden="true"></button>
				<h4 class="modal-title" ><strong><i class="fas fa-id-card-alt"></i>&nbsp;&nbsp;Contrato</strong></h4>
            </div>

            {{-- <form action="#" method="POST" class="horizontal-form" id="form" autocomplete="off"> --}}
                <div class="modal-body">
                    <div class="note note-danger" id="divErrores" style="display:none;">
                        <h4 class="block" style="margin-bottom:5 px;">Errores : </h4>
                        <br>
                        <ul id="ulErrores"></ul>
                    </div>

                    <div class="form-group row">
                        <div class="col-xs-6" style="border-right-style: double;">

                            <div class="form-group row">

                                <div class="col-xs-4 form-validate">
                                    <label for="tipo_contrato" class="label-form">Tipo Contrato </label>
                                    <select name="tipo_contrato" id="tipo_contrato" class="form-control select2" required readonly onchange="setTipoContrato(this);">
                                        <option value=''>Seleccione</option>
                                        <option value='1' @if( $contrato->id_tipo_contrato == 1 ) selected @endif >Honorario</option>
                                        <option value='2' @if( $contrato->id_tipo_contrato == 2 ) selected @endif>Convenio</option>
                                    </select>
                                </div>

                                <div class="col-xs-8 form-validate">
                                    <label for="proveedor" class="label-form" >Proveedor </label>
                                    <input type="text" class="form-control" id="proveedor" name="proveedor" required placeholder="Buscar por Nombre o RUT" readonly >
                                </div>
        
                            </div>

                            <div class="form-group row">

                                <div class="col-xs-3 form-validate">
                                    <label for="resolucion" class="label-form">Resolución </label>
                                    <input type="text" class="form-control solo_numeros" id="resolucion" name="resolucion" required value="{{ $contrato->resolucion }}" readonly >
                                </div>
        
                                <div class="col-xs-3 form-validate">
                                    <label for="fecha_resolucion" class="label-form">Fecha Resolución </label>
                                    <div class="input-group">
                                        <input type="text" class="form-control fechas" id="fecha_resolucion" name="fecha_resolucion" required value="{{ fecha_dmY( $contrato->fecha_resolucion ) }}" readonly >
                                        <span class="input-group-addon" onclick="fecha('fecha_resolucion');" style="cursor:pointer;"><i class="fa fa-calendar" aria-hidden="true" style="color: black;"></i></span>
                                    </div>
                                </div>

                                <div class="col-xs-3 form-validate">
                                    <label for="inicio_vigencia" class="label-form">Inicio Vigencia </label>
                                    <div class="input-group">
                                        <input type="text" class="form-control fechas" id="inicio_vigencia" name="inicio_vigencia" required value="{{ fecha_dmY( $contrato->inicio_vigencia ) }}"  readonly >
                                        <span class="input-group-addon" onclick="fecha('inicio_vigencia');" style="cursor:pointer;"><i class="fa fa-calendar" aria-hidden="true" style="color: black;"></i></span>
                                    </div>
                                </div>

                                <div class="col-xs-3  form-validate">
                                    <label for="fin_vigencia" class="label-form">Fin Vigencia </label>
                                    <div class="input-group">
                                        <input type="text" class="form-control fechas" id="fin_vigencia" name="fin_vigencia" required value="{{ fecha_dmY( $contrato->fin_vigencia ) }}" readonly >
                                        <span class="input-group-addon" onclick="fecha('fin_vigencia');" style="cursor:pointer;"><i class="fa fa-calendar" aria-hidden="true" style="color: black;"></i></span>
                                    </div>
                                </div>

                            </div>

                            <div class="form-group row">
                                
                                <div class="col-xs-3 form-validate">
                                    <label for="tipo_adjudicacion" class="label-form">Tipo Adjudicación </label>
                                    <input type="text" class="form-control" id="tipo_adjudicacion" name="tipo_adjudicacion" required value="{{ $contrato->getTipoAdjudicacion->nombre }}" readonly >
                                </div>
        
                                <div class="col-xs-6 form-validate">
                                    <label for="referente_tecnico" class="label-form">Referente Técnico </label>
                                    <input type="text" class="form-control" id="referente_tecnico" name="referente_tecnico" required 
                                            value="{{ $contrato->getReferenteTecnico ? $contrato->getReferenteTecnico->nombre : '' }}" readonly >
                                </div>
        
                            </div>

                            <div class="form-group row">

                                <div class="col-xs-3 col-xs-offset-3 form-validate">
                                    <label for="monto_contrato" class="label-form">Monto Contrato </label>
                                    <div class="input-group">
                                        <span class="input-group-addon">$</span>
                                        <input type="text" class="form-control numero_documento solo_numeros" id="monto_contrato" readonly
                                                name="monto_contrato" required onkeyup="ingresoPesos(this);" autocomplete="off" value="{{ formatoMiles($contrato->monto_contrato) }}" >
                                    </div>
                                </div>

                                <div class="col-xs-3 form-validate">
                                    <label for="saldo_contrato" class="label-form">Saldo Contrato </label>
                                    <div class="input-group">
                                        <span class="input-group-addon">$</span>
                                        <input type="text" class="form-control numero_documento solo_numeros" id="saldo_contrato" readonly
                                                name="saldo_contrato" required onkeyup="ingresoPesos(this);" autocomplete="off" value="{{ formatoMiles($contrato->saldo_contrato) }}" >
                                    </div>
                                </div>
                                
                            </div>

                            <div class="form-group row">

                                <div class="col-xs-12 form-validate">
                                    <label for="detalle_contrato" class="label-form">Detalle </label>
                                    <textarea class="form-control noresize" id="detalle_contrato" name="detalle_contrato" required readonly>{{ $contrato->detalle_contrato }}</textarea>
                                </div>

                            </div>

                            <div class="form-group row" {!! ( $contrato->termino == 0 ) ? 'style="display:none;"' : '' !!} >

                                <div class="col-xs-3 form-validate">
                                    <label for="fecha_termino" class="label-form">Fecha Término </label>
                                    <div class="input-group">
                                        <input type="text" class="form-control fechas" id="fecha_termino" name="fecha_termino" required value="{{ fecha_dmY( $contrato->fecha_termino ) }}" readonly >
                                        <span class="input-group-addon" onclick="fecha('fecha_termino');" style="cursor:pointer;"><i class="fa fa-calendar" aria-hidden="true" style="color: black;"></i></span>
                                    </div>
                                </div>

                                <div class="col-xs-3 form-validate">
                                    <label for="user_termino" class="label-form">Usuario Término </label>
                                    <input type="text" class="form-control" id="user_termino" name="user_termino" required value="{{ ($contrato->getUserTermino) ? $contrato->getUserTermino->name : '' }}" readonly >
                                </div>
        
                            </div>

                            <div class="form-group row" {!! ( $contrato->termino == 0 ) ? 'style="display:none;"' : '' !!}>

                                <div class="col-xs-12 form-validate">
                                    <label for="detalle_termino" class="label-form">Detalle Término</label>
                                    <textarea class="form-control noresize" id="detalle_termino" name="detalle_termino" required readonly>{{ $contrato->detalle_termino }}</textarea>
                                </div>

                            </div>

                            <div class="form-group row">
                                <div class="col-xs-12 table-responsive">
                                    <h4 class="form-section" style="color: #69aa46;margin: 5px 0px 2px 0px;">Documentos en PDF</h4>
        
                                    <table class="table table-striped table-bordered table-hover " id="tabla_info_archivos_existentes">
                                        <thead>
                                            <tr>
                                                <th width="50%">Nombre del Archivo</th>
                                                <th width="20%">Tipo</th>
                                                <th width="10%">Extensión</th>
                                                <th width="10%">Peso</th>
                                                <th width="10%"><i class="fa fa-cog"></i></th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                        @forelse ($contrato->getArchivos as $archivo)
                                            <tr>
                                                <td>{{ $archivo->nombre }}</td>
                                                <td>@if( $archivo->getTipoArchivo ){{ $archivo->getTipoArchivo->nombre }}@endif</td>
                                                <td>{{ $archivo->extension }}</td>
                                                <td>{{ pesoArchivoEnMB($archivo->peso) }}</td>
                                                <td>
                                                    <a class="btn btn-success btn-xs" @if($archivo->cargado == 0) href="{{ asset( $archivo->ubicacion ) }}" @else href="http://{{ $archivo->ubicacion }}" @endif
                                                    title="Ver Archivo" target="_blank" ><i class="fa fa-eye"></i></a>
                                                </td>
                                            </tr>
                                        @empty
        
                                        @endforelse
                                        </tbody>
                                    </table>
                                </div>
                            </div>

                            <div id="convenios" style="display:none;">

                                <div class="form-group row">
                                    
                                    <div class="col-xs-3 form-validate">
                                        <label for="monto_preventivo" class="label-form">Monto Preventivo </label>
                                        <div class="input-group">
                                            <span class="input-group-addon">$</span>
                                            <input type="text" class="form-control numero_documento solo_numeros" id="monto_preventivo" 
                                                    name="monto_preventivo" onkeyup="ingresoPesos(this);" autocomplete="off" readonly
                                                    @if( $contrato->monto_preventivo )
                                                        value="{{ formatoMiles($contrato->monto_preventivo) }}"
                                                    @endif >
                                        </div>
                                    </div>

                                    <div class="col-xs-3 form-validate">
                                        <label for="monto_correctivo" class="control-label label-form">Monto Correctivo </label>
                                        <div class="input-group">
                                            <span class="input-group-addon">$</span>
                                            <input type="text" class="form-control numero_documento solo_numeros" id="monto_correctivo" 
                                                    name="monto_correctivo" onkeyup="ingresoPesos(this);" autocomplete="off" readonly
                                                    @if( $contrato->monto_correctivo ) 
                                                        value="{{ formatoMiles($contrato->monto_correctivo) }}"
                                                    @endif >
                                        </div>
                                    </div>

                                    <div class="col-xs-3 form-validate">
                                        <label for="licitacion" class="label-form">Licitación </label>
                                        <input type="text" class="form-control" id="licitacion" name="licitacion" value="{{ $contrato->licitacion }}" readonly>
                                    </div>

                                    <div class="col-xs-3 form-validate">
                                        <label for="cuotas" class="label-form">Cuotas </label>
                                        <div class="checkbox check" >
                                            <label class="label_checkbox">
                                                <input type="checkbox" class="check_input" id="cuotas" name="cuotas" value="1" readonly @if( $contrato->cuotas == 1 ) checked @endif >
                                            </label>
                                        </div>
                                    </div> 
                                    
                                </div>

                                <div class="form-group row">
                                    
                                    <div class="col-xs-3 form-validate">
                                        <label for="saldo_preventivo" class="label-form">Saldo Preventivo </label>
                                        <div class="input-group">
                                            <span class="input-group-addon">$</span>
                                            <input type="text" class="form-control numero_documento solo_numeros" id="saldo_preventivo" 
                                                    name="saldo_preventivo" onkeyup="ingresoPesos(this);" autocomplete="off" readonly
                                                    @if( $contrato->saldo_preventivo )
                                                        value="{{ formatoMiles($contrato->saldo_preventivo) }}"
                                                    @endif >
                                        </div>
                                    </div>

                                    <div class="col-xs-3 form-validate">
                                        <label for="saldo_correctivo" class="control-label label-form">Saldo Correctivo </label>
                                        <div class="input-group">
                                            <span class="input-group-addon">$</span>
                                            <input type="text" class="form-control numero_documento solo_numeros" id="saldo_correctivo" 
                                                    name="saldo_correctivo" onkeyup="ingresoPesos(this);" autocomplete="off" readonly
                                                    @if( $contrato->saldo_correctivo ) 
                                                        value="{{ formatoMiles($contrato->saldo_correctivo) }}"
                                                    @endif >
                                        </div>
                                    </div>
                                    
                                </div>
                                        
                                <div class="row">
                                    <div class="col-xs-12">
                                        <h5 class="form-section" style="color: #69aa46;font-weight: bold !important; ">Boleta Garantía</h5>
                                    </div>
                                </div>

                                <div class="form-group row">
        
                                    <div class="col-xs-3 form-validate">
                                        <label for="numero_boleta_garantia" class="label-form">Número </label>
                                        <input type="text" class="form-control solo_numeros" id="numero_boleta_garantia" readonly
                                                name="numero_boleta_garantia" value="{{ $contrato->numero_boleta_garantia }}" >
                                    </div>
        
                                    <div class="col-xs-3 form-validate">
                                        <label for="monto_boleta_garantia" class="label-form">Monto </label>
                                        <div class="input-group">
                                            <span class="input-group-addon">$</span>
                                            <input type="text" class="form-control numero_documento solo_numeros" id="monto_boleta_garantia" readonly
                                                    name="monto_boleta_garantia" onkeyup="ingresoPesos(this);" autocomplete="off" value="{{ formatoMiles($contrato->monto_boleta_garantia) }}" >
                                        </div>
                                    </div>

                                    <div class="col-xs-3 form-validate">
                                        <label for="inicio_vigencia_boleta_garantia" class="label-form">Inicio Vigencia </label>
                                        <div class="input-group">
                                            <input type="text" class="form-control fechas" id="inicio_vigencia_boleta_garantia" readonly
                                                    name="inicio_vigencia_boleta_garantia"  value="{{ fecha_dmY( $contrato->inicio_vigencia_boleta_garantia ) }}" >
                                            <span class="input-group-addon" onclick="fecha('inicio_vigencia_boleta_garantia');" style="cursor:pointer;">
                                                <i class="fa fa-calendar" aria-hidden="true" style="color: black;"></i>
                                            </span>
                                        </div>
                                    </div>
        
                                    <div class="col-xs-3 form-validate">
                                        <label for="fin_vigencia_boleta_garantia" class="label-form">Fin Vigencia </label>
                                        <div class="input-group">
                                            <input type="text" class="form-control fechas" id="fin_vigencia_boleta_garantia" readonly
                                                    name="fin_vigencia_boleta_garantia" value="{{ fecha_dmY( $contrato->fin_vigencia_boleta_garantia ) }}">
                                            <span class="input-group-addon" onclick="fecha('fin_vigencia_boleta_garantia');" style="cursor:pointer;">
                                                <i class="fa fa-calendar" aria-hidden="true" style="color: black;"></i>
                                            </span>
                                        </div>
                                    </div>
        
                                </div>
        
                                <div class="form-group row">
                                    
                                </div>
        
                            </div>

                            <div id="honorarios" style="display:none;">

                                <div class="form-group row">
                                    
                                    <div class="col-xs-3 form-validate">
                                        <label for="valor_hora" class="label-form"> Valor Hora </label>
                                        <div class="input-group">
                                            <span class="input-group-addon">$</span>
                                            <input type="text" class="form-control numero_documento solo_numeros" id="valor_hora" name="valor_hora" readonly
                                                    onkeyup="ingresoPesos(this);" autocomplete="off" @if( $contrato->valor_hora ) value="{{ formatoMiles($contrato->valor_hora) }}" @endif >
                                        </div>
                                    </div>
        
                                    <div class="col-xs-3 form-validate">
                                        <label for="por_procedimiento" class="label-form">Por procedimiento </label>
                                        <div class="checkbox check" >
                                            <label class="label_checkbox">
                                                <input type="checkbox" class="check_input" id="por_procedimiento" name="por_procedimiento" readonly
                                                        value="1" onchange="setValorHora();" @if($contrato->por_procedimiento == 1) checked @endif >
                                            </label>
                                        </div>
                                    </div>


                                    <div class="col-xs-3 form-validate">
                                        <label for="unidad" class="label-form">Unidad </label>
                                        <input type="text" class="form-control" id="unidad" name="unidad" required 
                                            value="{{ ($contrato->getUnidad) ? $contrato->getUnidad->nombre : '' }}" readonly >
                                    </div>

                                    <div class="col-xs-3 form-validate">
                                        <label for="prestacion" class="label-form">Prestación </label>
                                        <input type="text" class="form-control" id="prestacion" name="prestacion" required 
                                                value="{{ ($contrato->getTipoPrestacion) ? $contrato->getTipoPrestacion->nombre : '' }}" readonly >
                                    </div> 
        
                                </div>
        
                                <div class="form-group row">
                                    
                                    <div class="col-xs-4 form-validate">
                                        <label for="profesion" class="label-form">Profesión </label>
                                        <input type="text" class="form-control" id="profesion" name="profesion" required 
                                                value="{{ ($contrato->getProfesion) ? $contrato->getProfesion->nombre : '' }}" readonly >
                                    </div>
                                    
                                    <div class="col-xs-3 form-validate">
                                        <label for="categoria" class="label-form">Categoría </label>
                                        <input type="text" class="form-control" id="categoria" name="categoria" 
                                                value="{{ ($contrato->getCategoria) ? $contrato->getCategoria->nombre : '' }}" readonly >
                                    </div>

                                    <div class="col-xs-2 form-validate">
                                        <label for="sueldo_horas" class="label-form">Suledo / Horas </label>
                                        <input type="text" class="form-control" id="sueldo_horas" name="sueldo_horas" required 
                                                value="{{ ($contrato->sueldo_horas) ? $contrato->sueldo_horas : '' }}" readonly >
                                    </div> 
        
                                </div>
        
                                <div class="form-group row">
                                    
                                    <div class="col-xs-3 form-validate">
                                        <label for="addemdum" class="label-form">Addemdum </label>
                                        <input type="text" class="form-control solo_numeros" id="addemdum" name="addemdum" 
                                                value="{{ ($contrato->addemdum) ? $contrato->addemdum : '' }}" readonly >
                                    </div>
        
                                    <div class="col-xs-3 orm-validate">
                                        <label for="fecha_addemdum" class="label-form">Fecha Addemdum </label>
                                        <div class="input-group">
                                            <input type="text" class="form-control fechas" id="fecha_addemdum" name="fecha_addemdum" readonly
                                                @if ( $contrato->fecha_addemdum != null ) value="{{ fecha_dmY( $contrato->fecha_addemdum ) }}" @endif >
                                            <span class="input-group-addon" onclick="fecha('fecha_addemdum');" style="cursor:pointer;"><i class="fa fa-calendar" aria-hidden="true" style="color: black;"></i></span>
                                        </div>
                                    </div>
                                </div>
        
                            </div>

                        </div> <!-- / col-xs-6 -->

                        <div class="col-xs-6" >

                            <div class="form-group row">
                                <div class="col-xs-12 table-responsive">
                                    <h4 class="form-section" style="color: #69aa46;margin: 5px 0px 2px 0px;">Ordenes de Compra</h4>
        
                                    <table class="table table-striped table-bordered table-hover " id="tabla_ordenes_compra">
                                        <thead>
                                            <tr>
                                                <th >Número</th>
                                                <th >Monto</th>
                                                <th >Saldo</th>
                                                <th >Periodo</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                        @forelse ($contrato->getOrdenesCompra as $oc)
                                            <tr>
                                                <td>{{ $oc->numero_oc }}</td>
                                                <td>{{ formatoMiles($oc->monto_oc) }}</td>
                                                <td>{{ formatoMiles($oc->saldo_oc) }}</td>
                                                <td>{{ fecha_dmY( $oc->inicio_vigencia ).' - '.fecha_dmY( $oc->fin_vigencia ) }}</td>
                                            </tr>
                                        @empty
        
                                        @endforelse
                                        </tbody>
                                    </table>
                                </div>
                            </div>

                            <div class="form-group row">
                                <div class="col-xs-12 table-responsive">
                                    <h4 class="form-section" style="color: #69aa46;margin: 5px 0px 2px 0px;">Documentos &nbsp;
                                        <span style="color: black;font-size: small;font-weight: normal;">(Ingresados en SIFCON)</span>
                                    </h4>
        
                                    <table class="table table-striped table-bordered table-hover " id="tabla_documentos">
                                        <thead>
                                            <tr>
                                                <th >Número</th>
                                                <th >Tipo</th>
                                                <th >Total</th>
                                                <th >Orden de Compra</th>
                                                <th >Periodo</th>
                                                <th ><i class="fa fa-cog"></i></th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                        @forelse ($contrato->getDocumentosContrato as $documentoOc)
                                            <tr>
                                                <td>{{ $documentoOc->getDocumento->numero_documento }}</td>
                                                <td>{{ $documentoOc->getDocumento->getTipoDocumento->nombre }}</td>
                                                <td>{{ formatoMiles($documentoOc->getDocumento->total_documento) }}</td>
                                                <td>
                                                    {{ ( $documentoOc->getDocumento->getContratoOrdenCompra->getOrdenCompra ) ? $documentoOc->getDocumento->getContratoOrdenCompra->getOrdenCompra->numero_oc : '' }}
                                                </td>
                                                <td>{{ fecha_mY($documentoOc->getDocumento->periodo) }}</td>
                                                <td>
                                                    @if ( $documentoOc->getDocumento->getArchivos->count() > 0 )
                                                        <div class="btn-group ">
                                                            @foreach($documentoOc->getDocumento->getArchivos as $archivo)
                                                                <a class="btn btn-info btn-xs " 
                                                                    @if($archivo->cargado == 0) 
                                                                        href="{{ asset( $archivo->ubicacion ) }}" 
                                                                    @else
                                                                        href="http://{{ $archivo->ubicacion }}"
                                                                    @endif
                                                                    title="Ver Archivo {{ ($archivo->getTipoArchivo ) ? $archivo->getTipoArchivo->nombre : '' }}" 
                                                                    target="_blank" ><i class="fa fa-eye"></i>
                                                                </a>
                                                            @endforeach
                                                        </div>
                                                    @endif
                                                </td>
                                            </tr>
                                        @empty
        
                                        @endforelse
                                        </tbody>
                                    </table>
                                </div>
                            </div>

                            <div class="form-group row" {!! ($archivosAcepta->count() == 0) ? 'style="display:none;"': '' !!}>
                                <div class="col-xs-12 table-responsive">
                                    <h4 class="form-section" style="color: #69aa46;margin: 5px 0px 2px 0px;">Archivos Acepta &nbsp;
                                        <span style="color: black;font-size: small;font-weight: normal;">(Aún no ingresados en SIFCON)</span>
                                    </h4>
        
                                    <table class="table table-striped table-bordered table-hover " id="tabla_archivos_acepta">
                                        <thead>
                                            <tr>
                                                <th >Número</th>
                                                <th >Tipo</th>
                                                <th >Total</th>
                                                <th ><i class="fa fa-cog"></i></th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                        @forelse ($archivosAcepta as $archivo)
                                            <tr>
                                                <td>{{ $archivo->folio }}</td>
                                                <td>{{ $archivo->getTipoDocumento->nombre }}</td>
                                                <td>{{ formatoMiles($archivo->monto_total) }}</td>
                                                <td>
                                                    <div class="btn-group ">
                                                        <a class="btn btn-info btn-xs " title="Ver Archivo" target="_blank"
                                                        href="http://windte1806.acepta.com/ca4webv3/PdfViewMedia?url={{ $archivo->uri }}&menuTitle=Papel%2520Carta" >
                                                            <i class="fa fa-eye"></i>
                                                        </a>
                                                    </div>
                                                </td>
                                            </tr>
                                        @empty
        
                                        @endforelse
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div> <!-- / col-xs-6 -->
                    </div>
                                    
                </div>

                <div class="modal-footer form-actions right">
                    {{-- <input type="hidden" name="_token" value="{{ csrf_token() }}"> --}}
                    <input type="hidden" name="_id" value="{{ $contrato->id }}">

                    <button type="button" title="Cerrar" class="btn btn-default" data-dismiss="modal">Cerrar</button>
                    {{-- <button type="submit" title="Terminar contrato" class="btn btn-danger" id="botonGuardar" style="display:none;">
                        <i class="far fa-save" style="color:black;"></i> Dar Térrmino al contrato</button> --}}
                </div>
            {{-- </form> --}}

        </div>
    </div>
</div>

<script src="{{asset('sistema/js/contratos.js')}}?v={{rand()}}"></script>
<script>
    $(document).ready(function() {

        @if( $contrato->por_procedimiento == 1 )
            setValorHora();
        @endif

        @if( $contrato->valor_hora )
            setPorProcedimiento();
        @endif

        $(".solo_numeros").keypress(function (key) {
            // window.console.log(key.charCode); //Descomentar para ver codigo
            if (
                (key.charCode < 48 || key.charCode > 57)//números
                && (key.charCode != 0) //borrar y enter
                && (key.charCode != 44) //coma
                && (key.charCode != 46) //punto
                && (key.charCode != 45) //guion
                && (key.charCode != 43) //suma
                )
                //console.log(key.charCode);
                return false;
        });

        $('.fechas').datepicker({
            format: 'dd/mm/yyyy',
            // endDate: new Date(),
            autoclose: true,
            language: 'es'
        });

        $("#proveedor").select2({
            ajax: {
                cache: true,
                allowClear: true,
                //hidden : true,
                url : '{{ url('buscador_proveedor/') }}',
                dataType: 'json',
                delay: 250,
                data: function (params,page) {
                    //console.log("soy params : "+params);
                    //console.log("soy page : "+page);
                    var query = {
                        term: params,
                        page: params.page || 1
                    }
                    return query;
                },
                results: function (data) {
                    return {
                    results: data
                    };
                },
            },
            minimumInputLength: 2,
        
        });

        $(".select2").select2();

        // $("#form").validate({
        //     highlight: function(element) {
        //         $(element).closest('.form-validate').removeClass('has-success');
        //         $(element).closest('.form-validate').addClass('has-error');
        //     },
        //     unhighlight: function(element) {
        //         $(element).closest('.form-validate').removeClass('has-error');
        //         $(element).closest('.form-validate').addClass('has-success');
        //     },
        //     errorElement: 'span',
        //     errorClass: 'help-block',
        //     errorPlacement: function (error, element) {
        //         console.log(element);
        //         if (element.parent('.input-group').length) {
        //             error.insertAfter(element.parent());
        //         } else {
        //             error.insertAfter(element);
        //         }
        //     },
        //     rules: {
        //     },
        //     messages: {
        //     },

        //     //para enviar el formulario por ajax
        //     submitHandler: function(form) {
        //         esperandoCrear();

        //         let formData = new FormData(form);
                
        //         $.ajax({
        //             url: form.action,
        //             type: form.method,
        //             //data: $(form).serialize(),
        //             data: formData,
        //             processData: false,
        //             contentType: false,
        //             success: function(respuesta) {
        //                 console.log(respuesta);

        //                 if ( respuesta.estado == 'error' ) {
        //                     toastr.error('No es posible realizar la acción'+'<br><strong>Error: '+ respuesta.mensaje +'</strong>', 'Atención', optionsToastr);
        //                 } else if ( respuesta.estado == 'success') {
                            
        //                     toastr.success(respuesta.mensaje, 'Atención', optionsToastr);

        //                     if ( respuesta.estado_archivo == 'error' ) {
        //                         toastr.error('No es posible realizar la acción'+'<br><strong>Error: '+ respuesta.mensaje_archivo +'</strong>', 'Atención', optionsToastr);
        //                     } else if ( respuesta.estado_archivo == 'succes') {
        //                         toastr.success(respuesta.mensaje_archivo, 'Atención', optionsToastr);
        //                     }

        //                     // Agregar informacion a la tabla
        //                     $('#tabla_principal').DataTable().ajax.reload();
        //                     $("#modalVer").modal("hide");

        //                 }
                        
        //             }            
        //         }).fail( function(respuesta) {//fail ajax
        //             if ( respuesta.status == 400 ) {
        //                 mostrarErroresValidator(respuesta);
        //             } else if ( respuesta.status == 500 ) {
        //                 toastr.error('No es posible realizar la acción, error en el servidor', 'Atención', optionsToastr);
        //             } else {
        //                 toastr.error('No es posible realizar la acción', 'Atención', optionsToastr);
        //             }
                    
        //         })
        //         .always(function() {
        //             listoCrear();
        //         });//ajax
                
        //     }

        // });

        $('#proveedor').select2('data', { id: '{!! $contrato->id_proveedor !!}', text: '{!!$contrato->getProveedor->formatRut() !!} {!!$contrato->getProveedor->nombre !!}' });
        $('#tipo_contrato').trigger( 'onchange' );
        $('#tipo_contrato').select2('readonly', true);
        
    });

    var selectTipoArchivo = '<select name="tipo_archivo" id="tipo_archivo" class="col-xs-12" required>';
    selectTipoArchivo += '<option value="">Seleccione</option>';
   
    selectTipoArchivo += '</select>';

    var tablaArchivo = $('#tabla_info_archivo').DataTable({
        "paging":   false,
        "ordering": false,
        "info":     false,
        "searching": false
    });

    var tablaArchivosExistentes = $('#tabla_info_archivos_existentes').DataTable({
        "language": {
            "emptyTable": "No hay datos disponibles en la tabla",
        },
        "paging":   false,
        "ordering": false,
        "info":     false,
        "searching": false
    });

    var tablaDocumentos = $('#tabla_documentos').DataTable({
        "language": {
            "emptyTable": "No hay datos disponibles en la tabla",
        },
        "paging":   false,
        "ordering": false,
        "info":     false,
        "searching": false
    });

    var tablaOrdenesCompra = $('#tabla_ordenes_compra').DataTable({
        "language": {
            "emptyTable": "No hay datos disponibles en la tabla",
        },
        "paging":   false,
        "ordering": false,
        "info":     false,
        "searching": false
    });

    var tablaArchivosAcepta = $('#tabla_archivos_acepta').DataTable({
        "language": {
            "emptyTable": "No hay datos disponibles en la tabla",
        },
        "paging":   false,
        "ordering": false,
        "info":     false,
        "searching": false
    });

    var montoMinimoContrato = '{!! $contrato->getMontoMinimo() !!}';
    var montoMinimoPreventivo = '{!! $contrato->getMontoMinimoPreventivo() !!}';
    var montoMinimoCorrectivo = '{!! $contrato->getMontoMinimoCorrectivo() !!}';

</script>