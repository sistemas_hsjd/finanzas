{{--
@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">Dashboard</div>

                <div class="card-body">
                    @if (session('status'))
                        <div class="alert alert-success" role="alert">
                            {{ session('status') }}
                        </div>
                    @endif

                    You are logged in!
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
--}}

@extends('master')

@section('title', 'Home')

@push('css-head')
<!-- BEGIN PAGE LEVEL STYLES -->
<link rel="stylesheet" type="text/css" href="{{asset('assets/plugins/select2/select2_conquer.css')}}"/>
<link rel="stylesheet" href="{{asset('assets/plugins/data-tables/DT_bootstrap.css')}}"/>
<!-- END PAGE LEVEL STYLES -->
<style>
	.border-primary {
		border: 1px solid #ffffff00;
    	border-radius: 13px;
		cursor:pointer !important;
	}
</style>
@endpush

@section('content')

<!-- BEGIN CONTENT -->

<div class="page-content-wrapper">
	<div class="page-content-wrapper">
		<div class="page-content">

			<!-- BEGIN PAGE CONTENT-->
			<div class="row" id="indicadores" style="display:none;">
				{{-- @role(['propietario','administrador','administrador-devengo','usuario-general','usuario-ingreso']) --}}
					<div class="col-md-12">
						<div class="row" style="margin-top:100px;">
							<hr>
							<div class="col-md-4" >
								<div class="border-primary" style="background-color:green;" onclick="window.location='{{ asset('documentos') }}'" id="indicador_doc_ingresados_hoy">
									<h3 style="color:white;font-weight:bold !important;" class="text-center"><i class="fa fa-file-text fa-pulso"></i>  Documentos Ingresados Hoy<i class="fa fa-file-text fa-flip-horizontal fa-pulso"></i> </h3>
									
									<h6 style="color:white;" class="text-center"> esta semana |  este mes |  este año</h6>
									<!--
									<div class="card"> 
										<div class="card-body">A computer science portal for geeks 
										</div> 
									</div>
									-->
								</div>
							</div>

							<div class="col-md-4 " >
								<div class="border-primary" style="background-color:#042fa2;" id="indicador_doc_cuadrados_hoy" >
									<h3 style="color:white;font-weight:bold !important;" class="text-center"> <i class="fas fa-thumbs-up fa-pulso"></i> Documentos Cuadrados Hoy <i class="fas fa-thumbs-up fa-flip-horizontal fa-pulso"></i> </h3>
									<h6 style="color:white;" class="text-center"> esta semana |  este mes |  este año</h6>
								</div>
							</div>

							<div class="col-md-4 " >
								<div class="border-primary" style="background-color: #ff6a00;" onclick="window.location='{{ asset('devengar/devengados') }}'" id="indicador_doc_devengados_hoy">
									<h3 style="color:white;font-weight:bold !important;" class="text-center"><i class="fas fa-gavel fa-pulso"></i>  Documentos Devengados Hoy <i class="fas fa-gavel fa-flip-horizontal fa-pulso"></i></h3>
									<h6 style="color:white;" class="text-center"> esta semana |  este mes |  este año</h6>
								</div>
							</div>

						</div>

						<div class="row" style="margin-top:20px;" >
							<div class="col-md-4 col-md-offset-2" >
								<div class="border-primary" style="background-color:#543729;" onclick="window.location='{{ asset('archivos_acepta/home/excel/2019') }}'" id="indicador_acepta_por_ingresar_2019">
									<h3 style="color:white;font-weight:bold !important;" class="text-center"><i class="fas fa-file-import fa-pulso"></i>  Documentos Acepta Por Ingresar <i class="fas fa-file-import fa-flip-horizontal fa-pulso"></i></h3>
									<h5 style="color:white;" class="text-center">Monto Total <strong> $ </strong></h5>
									<h6 style="color:white;" class="text-center">Rango : 2019</h6>
								</div>
							</div>

							<div class="col-md-4" >
								<div class="border-primary" style="background-color:#ed7902;" onclick="window.location='{{ asset('devengar/home/excel/2019') }}'" id="indicadores_doc_por_devengar_2019">
									<h3 style="color:white;font-weight:bold !important;" class="text-center"><i class="fas fa-gavel fa-pulso"></i>  Documentos Por Devengar<i class="fas fa-gavel fa-flip-horizontal fa-pulso"></i></h3>
									<h5 style="color:white;" class="text-center">Monto Total <strong> $ </strong></h5>
									<h6 style="color:white;" class="text-center">Rango : 2019</h6>
								</div>
							</div>
						</div>

						<div class="row" style="margin-top:20px;" >
							<div class="col-md-4 col-md-offset-2" >
								<div class="border-primary" style="background-color:#543729;" onclick="window.location='{{ asset('archivos_acepta/home/excel/2020') }}'" id="indicador_acepta_por_ingresar_2020">
									<h3 style="color:white;font-weight:bold !important;" class="text-center">
										<i class="fas fa-file-import fa-pulso"></i>  Documentos Acepta Por Ingresar <i class="fas fa-file-import fa-flip-horizontal fa-pulso"></i>
									</h3>
									<h5 style="color:white;" class="text-center">Monto Total <strong> $ </strong></h5>
									<h6 style="color:white;" class="text-center">Año : 2020</h6>
								</div>
							</div>

							<div class="col-md-4" >
								<div class="border-primary" style="background-color:#ed7902;" onclick="window.location='{{ asset('devengar/home/excel/2020') }}'" id="indicadores_doc_por_devengar_2020">
									<h3 style="color:white;font-weight:bold !important;" class="text-center">
										<i class="fas fa-gavel fa-pulso"></i>  Documentos Por Devengar <i class="fas fa-gavel fa-flip-horizontal fa-pulso"></i>
									</h3>
									<h5 style="color:white;" class="text-center">Monto Total <strong> $ </strong></h5>
									<h6 style="color:white;" class="text-center">Rango : 2020</h6>
								</div>
							</div>
						</div>

						
						<div class="row" style="margin-top:20px;">
							<div class="col-md-4 " >
								<div class="border-primary" style="background-color:#5654a4;" onclick="window.location='{{ asset('documentos/listado/pendiente_validacion') }}'" id="indicadores_doc_por_validar">
									<h3 style="color:white;font-weight:bold !important;" class="text-center">
										<i class="fas fa-thumbs-down"></i>  Docs. por Validar <i class="fas fa-thumbs-down fa-flip-horizontal fa-pulso"></i>
									</h3>
									<h5 style="color:white;" class="text-center">Monto Total <strong> $ </strong></h5>
								</div>
							</div>

							<div class="col-md-4 "  >
								<div class="border-primary" style="background-color: #005670;" onclick="window.location='{{ asset('documentos/listado/pendiente_validacion/convenio') }}'" id="indicadores_convenios_por_validar">
									<h3 style="color:white;font-weight:bold !important;" class="text-center">
										<i class="fas fa-eye fa-pulso"></i> Convenio por Validar <i class="fas fa-eye fa-flip-horizontal fa-pulso"></i>
									</h3>
									<h5 style="color:white;" class="text-center">Monto Total <strong> $ </strong></h5>
								</div>
							</div>

							<div class="col-md-4 "  >
								<div class="border-primary" style="background-color: #be0027;" onclick="window.location='{{ asset('documentos/listado/pendiente_validacion/programa') }}'" id="indicadores_programas_por_validar">
									<h3 style="color:white;font-weight:bold !important;" class="text-center">
										<i class="fas fa-eye fa-pulso"></i>  Programas por Validar <i class="fas fa-eye fa-flip-horizontal fa-pulso"></i>
									</h3>
									<h5 style="color:white;" class="text-center">Monto Total <strong> $ </strong></h5>
								</div>
							</div>
						</div>

						
						<div class="row" style="margin-top:20px;">
							<div class="col-md-4 " >
								<div class="border-primary" style="background-color:#5654a4;" id="indicadores_docs_deuda_real">
									<h3 style="color:white;font-weight:bold !important;" class="text-center">
										<i class="fas fa-search-dollar fa-flip-horizontal fa-pulso"></i>  Docs. en Deuda Real <i class="fas fa-search-dollar"></i>
									</h3>
									<h5 style="color:white;" class="text-center">Monto Total <strong> $ </strong></h5>
								</div>
							</div>

						 	<div class="col-md-4 "  >
								<div class="border-primary" style="background-color: #005670;" id="indicadores_docs_estimacion_deuda">
									<h3 style="color:white;font-weight:bold !important;" class="text-center">
										<i class="fas fa-hand-holding-usd"></i> Docs. en Estimación Deuda <i class="fas fa-hand-holding-usd fa-flip-horizontal fa-pulso"></i>
									</h3>
									<h5 style="color:white;" class="text-center">Monto Total <strong> $ </strong></h5>
								</div>
							</div>

							
							<div class="col-md-4 "  >
								<div class="border-primary" style="background-color: #be0027;" id="indicadores_docs_pagados">
									<h3 style="color:white;font-weight:bold !important;" class="text-center">
										<i class="fas fa-comment-dollar fa-flip-horizontal fa-pulso"></i>  Docs. Pagados <i class="fas fa-comment-dollar"></i>
									</h3>
									<h5 style="color:white;" class="text-center">Monto Total <strong> $ </strong></h5>									
								</div>
							</div>
						</div>

						<div class="row">
							<hr>
						</div>
					</div>
				{{-- @endrole --}}
			</div>
			
			
			
			<!-- END PAGE CONTENT-->

		</div> <!--/div class="page-content" -->
	</div> <!--/div class="page-content-wrapper" -->
</div><!--/div class="page-content-wrapper" -->
<!-- END CONTENT -->
@endsection

@push('script-footer')
<!-- BEGIN PAGE LEVEL PLUGINS -->
<script type="text/javascript" src="{{ asset('assets/plugins/select2/select2.min.js') }} "></script>
<script type="text/javascript" src="{{ asset('assets/plugins/data-tables/jquery.dataTables.min.js') }}"></script>
<script type="text/javascript" src="{{ asset('assets/plugins/data-tables/DT_bootstrap.js') }}"></script>
<!-- END PAGE LEVEL PLUGINS -->
<!-- BEGIN PAGE LEVEL SCRIPTS -->
<script src="{{ asset('assets/scripts/app.js') }} "></script>


<script type="text/javascript">
	$(document).ready(function(){
		App.init(); // initlayout and core plugins
	

   		$("#home").addClass( "active" );
		$("#home-li").addClass( "active" );
		$("#home-a").append( '<span class="selected"></span>' );

		@role(['propietario','administrador','administrador-devengo','usuario-general','usuario-ingreso'])
			toastr.success('Cargando indicadores', 'Atención', optionsToastr);
			$.ajax({
				url: "{{ url('/home/indicadores') }}",
				type: 'GET',
				processData: false,
				contentType: false,
				success: function(respuesta) {
					console.log('todo bien');
					console.log(respuesta);

					$('#indicador_doc_ingresados_hoy').html(
						`
						<h3 style="color:white;font-weight:bold !important;" class="text-center"><i class="fa fa-file-text fa-pulso"></i> ${respuesta.documentosHoy} Documentos Ingresados Hoy <i class="fa fa-file-text fa-flip-horizontal fa-pulso"></i></h3>
						<h6 style="color:white;" class="text-center">${respuesta.documentosSemana} esta semana | ${respuesta.documentosMes} este mes | ${respuesta.documentosYear} este año</h6>
						`
					);

					$('#indicador_doc_cuadrados_hoy').html(
						`
						<h3 style="color:white;font-weight:bold !important;" class="text-center"><i class="fas fa-thumbs-up fa-pulso"></i> ${respuesta.cuadradosHoy} Documentos Cuadrados Hoy <i class="fas fa-thumbs-up fa-flip-horizontal fa-pulso"></i></h3>
						<h6 style="color:white;" class="text-center">${respuesta.cuadradosSemana} esta semana | ${respuesta.cuadradosMes} este mes | ${respuesta.cuadradosYear} este año</h6>
						`
					);

					$('#indicador_doc_devengados_hoy').html(
						`
						<h3 style="color:white;font-weight:bold !important;" class="text-center"><i class="fas fa-gavel fa-pulso"></i> ${respuesta.devengadosHoy} Documentos Devengados Hoy <i class="fas fa-gavel fa-flip-horizontal fa-pulso"></i></h3>
						<h6 style="color:white;" class="text-center">${respuesta.devengadosSemana} esta semana | ${respuesta.devengadosMes} este mes | ${respuesta.devengadosYear} este año</h6>
						`
					);

					$('#indicador_acepta_por_ingresar_2019').html(
						`
						<h3 style="color:white;font-weight:bold !important;" class="text-center"><i class="fas fa-file-import fa-pulso"></i> ${respuesta.documentosAceptaInputarAnhio2019} Documentos Acepta Por Ingresar <i class="fas fa-file-import fa-flip-horizontal fa-pulso"></i></h3>
						<h5 style="color:white;" class="text-center">Monto Total <strong> $ ${respuesta.documentosAceptaInputarAnhioMonto2019}</strong></h5>
						<h6 style="color:white;" class="text-center">Rango : 2019</h6>
						`
					);

					$('#indicadores_doc_por_devengar_2019').html(
						`
						<h3 style="color:white;font-weight:bold !important;" class="text-center"><i class="fas fa-gavel fa-pulso"></i> ${respuesta.documentosPorDevengar2019} Documentos Por Devengar <i class="fas fa-gavel fa-flip-horizontal fa-pulso"></i></h3>
						<h5 style="color:white;" class="text-center">Monto Total <strong> $ ${respuesta.totalPorDevengar2019}</strong></h5>
						<h6 style="color:white;" class="text-center">Rango : 2019</h6>
						`
					);

					$('#indicador_acepta_por_ingresar_2020').html(
						`
						<h3 style="color:white;font-weight:bold !important;" class="text-center">
							<i class="fas fa-file-import fa-pulso"></i> ${respuesta.documentosAceptaInputarAnhio2020} Documentos Acepta Por Ingresar <i class="fas fa-file-import fa-flip-horizontal fa-pulso"></i>
						</h3>
						<h5 style="color:white;" class="text-center">Monto Total <strong> $ ${respuesta.documentosAceptaInputarAnhioMonto2020}</strong></h5>
						<h6 style="color:white;" class="text-center">Año : 2020</h6>
						`
					);

					$('#indicadores_doc_por_devengar_2020').html(
						`
						<h3 style="color:white;font-weight:bold !important;" class="text-center">
							<i class="fas fa-gavel fa-pulso"></i> ${respuesta.documentosPorDevengar2020} Documentos Por Devengar <i class="fas fa-gavel fa-flip-horizontal fa-pulso"></i>
						</h3>
						<h5 style="color:white;" class="text-center">Monto Total <strong> $ ${respuesta.totalPorDevengar2020}</strong></h5>
						<h6 style="color:white;" class="text-center">Rango : 2020</h6>
						`
					);

					$('#indicadores_doc_por_validar').html(
						`
						<h3 style="color:white;font-weight:bold !important;" class="text-center">
							<i class="fas fa-thumbs-down"></i> ${respuesta.documentosPendientesValidacion } Docs. por Validar <i class="fas fa-thumbs-down fa-flip-horizontal fa-pulso"></i>
						</h3>
						<h5 style="color:white;" class="text-center">Monto Total <strong> $ ${respuesta.totalPendienteValidacion }</strong></h5>
						`
					);

					$('#indicadores_convenios_por_validar').html(
						`
						<h3 style="color:white;font-weight:bold !important;" class="text-center">
							<i class="fas fa-eye fa-pulso"></i> ${respuesta.conveniosPendientesValidacion } Convenio por Validar <i class="fas fa-eye fa-flip-horizontal fa-pulso"></i>
						</h3>
						<h5 style="color:white;" class="text-center">Monto Total <strong> $ ${respuesta.totalConveniosPendientesValidacion }</strong></h5>
						`
					);

					$('#indicadores_programas_por_validar').html(
						`
						<h3 style="color:white;font-weight:bold !important;" class="text-center">
							<i class="fas fa-eye fa-pulso"></i> ${respuesta.programasPendientesValidacion } Programas por Validar <i class="fas fa-eye fa-flip-horizontal fa-pulso"></i>
						</h3>
						<h5 style="color:white;" class="text-center">Monto Total <strong> $ ${respuesta.totalProgramasPendientesValidacion }</strong></h5>
						`
					);

					$('#indicadores_docs_deuda_real').html(
						`
						<h3 style="color:white;font-weight:bold !important;" class="text-center">
							<i class="fas fa-search-dollar fa-flip-horizontal fa-pulso"></i> ${respuesta.deudaReal } Docs. en Deuda Real <i class="fas fa-search-dollar"></i>
						</h3>
						<h5 style="color:white;" class="text-center">Monto Total <strong> $ ${respuesta.montoDeudaReal }</strong></h5>
						`
					);

					$('#indicadores_docs_estimacion_deuda').html(
						`
						<h3 style="color:white;font-weight:bold !important;" class="text-center">
							<i class="fas fa-hand-holding-usd"></i> ${respuesta.estimacionDeuda } Dcos. en Estimación Deuda <i class="fas fa-hand-holding-usd fa-flip-horizontal fa-pulso"></i>
						</h3>
						<h5 style="color:white;" class="text-center">Monto Total <strong> $ ${respuesta.montoEstimacionDeuda }</strong></h5>
						`
					);

					$('#indicadores_docs_pagados').html(
						`
						<h3 style="color:white;font-weight:bold !important;" class="text-center">
							<i class="fas fa-comment-dollar fa-flip-horizontal fa-pulso"></i> ${respuesta.pagados} Docs. Pagados <i class="fas fa-comment-dollar"></i>
						</h3>
						<h5 style="color:white;" class="text-center">Monto Total <strong> $ ${respuesta.montoPagados}</strong></h5>
						`
					);


					$('#indicadores').css('display', '');
					
				}            
			}).fail( function(respuesta) {//fail ajax
				console.log('todo mal');
				console.log(respuesta);
				
				if ( respuesta.status == 500 ) {
					toastr.error('No es posible obtener los indicadores, error en el servidor', 'Atención', optionsToastr);
				} else {
					toastr.error('No es posible realizar la acción', 'Atención', optionsToastr);
				}
				
			})
			.always(function() {
				console.log('esto siempre pasa');
				// listoIngresoDocumento();
			});//ajax
		@endrole

	});
</script>

@endpush
