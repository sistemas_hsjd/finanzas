@extends('master')

@section('title', 'Documentos Rechazados de Acepta')

@push('css-head')
<!-- BEGIN PAGE LEVEL STYLES -->
<link rel="stylesheet" type="text/css" href="{{asset('assets/plugins/select2/select2_conquer.css')}}"/>
<link rel="stylesheet" href="{{asset('assets/plugins/data-tables/DT_bootstrap.css')}}"/>
<!-- END PAGE LEVEL STYLES -->
<style>
    .label-form {
        font-weight: bold;
        color: black;
        text-align: right;
    }

    .truncate {
        max-width: 170px;
        white-space: nowrap;
        overflow: hidden;
        text-overflow: ellipsis;
        cursor: help;
    }
</style>
@endpush

@section('content')

<!-- BEGIN CONTENT -->

<div class="page-content-wrapper">
	<div class="page-content-wrapper">
		<div class="page-content">

			<!-- BEGIN PAGE CONTENT-->
			<div class="row">
				<div class="col-md-12">
					<!-- BEGIN EXAMPLE TABLE PORTLET-->
					<div class="portlet">
						<div class="portlet-title">
							<div class="caption">
								<i class="fas fa-ban" style="color: black;"></i> Documentos Rechazados de Acepta
							</div>
						</div>
						<div class="portlet-body">
                            <form action="#" method="post" enctype="multipart/form-data" id="filtros-docs" class="form-horizontal">
                                <h4 class="form-section" style="color: #69aa46;"><strong>Filtros</strong>
                                <div class="btn-group pull-right">
                                    
                                    <button class="btn btn-success btn-xs" title="Generar Excel" formaction="{{ url('archivos_acepta/rechazados/excel') }}">
                                        <i class="fas fa-file-excel"></i> Exportar a Excel
                                    </button>
                                    
                                    <button class="btn btn-info btn-xs" title="Filtrar Listado" id="filtroListado" type="button">
                                        <i class="fas fa-search" style="color:black;" ></i> Filtrar
                                    </button>
                                </div>
                                </h4>
                                <div class="form-group row">
                                    <label for="filtro_fecha_inicio" class="col-sm-1 control-label label-form">Fecha Inicio</label>
                                    <div class="col-sm-3">
                                        <div class="input-group">
                                            <input type="text" class="form-control fechas" id="filtro_fecha_inicio" name="filtro_fecha_inicio" value="01/{{ date("m/Y") }}" >
                                            <span class="input-group-addon" onclick="fechaDocumento('filtro_fecha_inicio');" style="cursor:pointer;"><i class="fa fa-calendar" aria-hidden="true" style="color: black;"></i></span>
                                        </div>
                                    </div>

                                    <label for="filtro_fecha_termino" class="col-sm-1 control-label label-form">Fecha Termino</label>
                                    <div class="col-sm-3">
                                        <div class="input-group">
                                            <input type="text" class="form-control fechas" id="filtro_fecha_termino" name="filtro_fecha_termino" value="{{ date("d/m/Y") }}" >
                                            <span class="input-group-addon" onclick="fechaDocumento('filtro_fecha_termino');" style="cursor:pointer;"><i class="fa fa-calendar" aria-hidden="true" style="color: black;"></i></span>
                                        </div>
                                    </div>

                                    <label for="filtro_proveedor" class="col-sm-1 control-label label-form">Proveedor</label>
                                    <div class="col-sm-3">
                                        <select name="filtro_proveedor[]" id="filtro_proveedor" class="form-control select2_filtro" multiple style="width: 100%">
                                            @foreach ( $proveedores as $proveedor )
                                                <option value="{{ $proveedor->id }}" >{{ $proveedor->rut }} {{ $proveedor->nombre }}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                </div>

                                <div class="form-group row">
                                    <label for="filtro_tipo_documento" class="col-sm-1 control-label label-form">Tipo de Documento</label>
                                    <div class="col-sm-3">
                                        <select name="filtro_tipo_documento[]" id="filtro_tipo_documento" class="form-control select2_filtro" multiple style="width: 100%">
                                            @foreach ( $tiposDocumento as $tipoDocumento )
                                                <option value="{{ $tipoDocumento->id }}" >{{ $tipoDocumento->nombre }}</option>
                                            @endforeach
                                        </select>
                                    </div>

                                    <label for="filtro_numero_documento" class="col-sm-1 control-label label-form">N° Documento</label>
                                    <div class="col-sm-3">
                                        <input type="text" class="form-control " id="filtro_numero_documento" name="filtro_numero_documento"
                                        @if(isset($filtroNumeroDocumento)) value="{{ $filtroNumeroDocumento }}"  @endif >
                                    </div>

                                </div>
                                
                            </form>
                            <div class="table-toolbar">
                                    
                            </div>
                            <div class="row">
                                <div class="col-md-12">
                                    <div class="panel-body">
                                        <div class="table-responsive">
                                            <table class="table table-striped table-bordered table-hover table-full-width" id="tabla_documentos_acepta"></table>
                                        </div>
                                    </div>
                                </div>
                            </div>
						</div>
					</div>
					<!-- END EXAMPLE TABLE PORTLET-->
				</div>
			</div>
			<!-- END PAGE CONTENT-->
			
			
			
			
		</div> <!--/div class="page-content" -->
	</div> <!--/div class="page-content-wrapper" -->
</div><!--/div class="page-content-wrapper" -->
<!-- END CONTENT -->
<div id="modal"></div>
<div id="modal2"></div>
@endsection

@push('script-footer')
<!-- BEGIN PAGE LEVEL PLUGINS -->
<script type="text/javascript" src="{{ asset('assets/plugins/select2/select2.min.js') }} "></script>
{{--<script type="text/javascript" src="{{ asset('assets/plugins/data-tables/jquery.dataTables.min.js') }}"></script>--}}
<script type="text/javascript" src="{{ asset('datatables/jquery.dataTables.min.js') }}"></script>
<script type="text/javascript" src="{{ asset('assets/plugins/data-tables/DT_bootstrap.js') }}"></script>

<link rel="stylesheet" type="text/css" href="{{asset('assets/plugins/bootstrap-datepicker/css/datepicker.css')}}"/>

<script type="text/javascript" src="{{asset('assets/plugins/bootstrap-datepicker/js/bootstrap-datepicker.js')}}"></script>
<script type="text/javascript" src="{{asset('assets/plugins/bootstrap-datetimepicker/js/bootstrap-datetimepicker.js')}}"></script>

<script type="text/javascript" src="{{ asset('js/moment/moment.js') }}"></script>
<script type="text/javascript" src="{{ asset('js/moment/moment-with-locales.js') }}"></script>
<!-- END PAGE LEVEL PLUGINS -->
<!-- BEGIN PAGE LEVEL SCRIPTS -->
<script src="{{ asset('assets/scripts/app.js') }} "></script>


<script type="text/javascript">
    
    // console.log(dataSet);

    let tablaDocsAcepta = $('#tabla_documentos_acepta').DataTable({
        "ajax": {
            "url" : "{{ url('archivos_acepta/data_table_rechazados') }}",
            "type" : "POST",
            "data": function(d){
                d.form = JSON.stringify( $("#filtros-docs").serializeArray() );
                // d.dataTable = 'Buscador';
            },
        },
        // Set rows IDs
        rowId: function(data) {
            return 'doc_' + data.DT_RowID;
        },
        columns: [
            { title: "Tipo Doc" },
            { title: "Proveedor" },
            { title: "N° Doc", className: "text-right" },
            { title: "Fecha Doc.", className: "text-center",
                "render": function(data, type) {
                    moment.locale('es');
                    return type === 'sort' ? data : moment(data).format('L');
                }
            },
            { title: "Total", className: "text-right",
                "render": function(data, type, row, meta) {
                    return type === 'sort' ? data : '$'+formatMoney(data) ;
                } 
            },
            { title: "Motivo" },
            { title: "Observación", className: "truncate" },
            { title: "<div class='text-center'><i class='fa fa-cog'></i></div>" }
        ],
        createdRow: function(row){
            $(row).find(".truncate").each(function(){
                $(this).attr("title", this.innerText);
            });
        },
        "deferRender": true,
        "language": {
            "url": "{{ asset('datatables/language.json') }}"
        },
        "lengthMenu": [
            [10, 15, 20, -1],
            [10, 15, 20, "Todos"] // change per page values here
        ],
        // set the initial value
        "displayLength": 10,
        "paging":   true,
        "ordering": true,
        "info":     true,
        "searching": true,
        "orderClasses": false,
    });

    $.fn.dataTable.ext.errMode = 'none';//quita los mensajes de error de la datatable ajax

    $('#tabla_documentos_acepta').on( 'error.dt', function ( e, settings, techNote, message ) {
		console.log( 'An error has been reported by DataTables: ', message );//para mostrar los mensajes de error en la consola del navegador
	} );

    jQuery('#tabla_documentos_acepta_filter input').addClass("form-control input-large"); // modify table search input
    jQuery('#tabla_documentos_acepta_wrapper .dataTables_length select').addClass("form-control input-small"); // modify table per page dropdown
    jQuery('#tabla_documentos_acepta_wrapper .dataTables_length select').select2(); // initialize select2 dropdown

    $('#tabla_documentos_acepta_column_toggler input[type="checkbox"]').change(function() {
        /* Get the DataTables object again - this is not a recreation, just a get of the object */
        var iCol = parseInt($(this).attr("data-column"));
        var bVis = oTable.fnSettings().aoColumns[iCol].bVisible;
        oTable.fnSetColumnVis(iCol, (bVis ? false : true));
    });
    
	$(document).ready(function() {
		App.init(); // initlayout and core plugins
        // TableAdvanced.init();

        $("#documentos").addClass( "active" );
        $("#documentos-a").append( '<span class="selected"></span>' );
        $("#ingreso").addClass( "active" );
        $("#acepta").addClass( "active" );
        $("#acepta-rechazados-li").addClass( "active" );
        $("#acepta-a").append( '<span class="selected"></span>' );
        $("#acepta ul").css( "display","block");

        $('#filtro_fecha_inicio').datepicker({
            format: 'dd/mm/yyyy',
            endDate: new Date(),
            autoclose: true,
            language: 'es'
        });

        $('#filtro_fecha_termino').datepicker({
            format: 'dd/mm/yyyy',
            //endDate: new Date(),
            //"setDate": new Date(),
            //startDate: new Date(),            
            autoclose: true,
            language: 'es'
        });

        $(".select2_filtro").select2({
            allowClear: true,
        });

        $('#filtroListado').click(function(){
            $("#modalCarga").modal({backdrop: 'static', keyboard: false});
            $('#modalCargaImg').addClass('fa-pulso');
                
            $('#tabla_documentos_acepta').DataTable().ajax.reload( function ( json ) {
                $("#modalCarga").modal('toggle');
            });
        });

	});

    function fechaDocumento(id_fecha) {
        $('#' + id_fecha).datepicker("show");
    }

    function rechazar(id) {
        $.get( '{{ url("archivos_acepta/modal/rechazar") }}/' + id, function( data ) {
            $( "#modal" ).html( data );
            $( "#modalRechazar" ).modal();
        });
    }
    
    function quitarRechazo(id)
    {
        $.get( '{{ url("archivos_acepta/modal/quitar_rechazo") }}/' + id, function( data ) {
            $( "#modal" ).html( data );
            $( "#modalQuitarRechazo" ).modal();
        });
    }

    

</script>

@endpush