@extends('master')

@section('title', 'Carga Reclamados')

@push('css-head')
<!-- BEGIN PAGE LEVEL STYLES -->
<link rel="stylesheet" type="text/css" href="{{asset('assets/plugins/select2/select2_conquer.css')}}"/>
<link rel="stylesheet" href="{{asset('assets/plugins/data-tables/DT_bootstrap.css')}}"/>
<!-- END PAGE LEVEL STYLES -->

@endpush

@section('content')

<!-- BEGIN CONTENT -->

<div class="page-content-wrapper">
	<div class="page-content-wrapper">
		<div class="page-content">

			<!-- BEGIN PAGE CONTENT-->
			<div class="row">
				<div class="col-md-12">
					<!-- BEGIN EXAMPLE TABLE PORTLET-->
					<div class="portlet">
						<div class="portlet-title">
							<div class="caption">
                                <i class="fas fa-exclamation-triangle"></i> Carga Reclamados
							</div>
						</div>
						<div class="portlet-body">
                            <form action="{{url('archivos_acepta/carga_reclamados')}}" method="post" enctype="multipart/form-data" id="form_carga_archivo" class="form-horizontal">
                                <h4 class="form-section" style="color: #69aa46;"><strong>Documento en XLSX</strong></h4>
                                <div class="row">
                                    <div class="input-group col-sm-6 col-sm-offset-1">
                                        <span class="input-group-addon" id="inputGroupFileAddon01"><strong>Seleccione Documento</strong></span>
                                        <input type="file" class="form-control" accept=".xlsx" name="archivo" id="archivo" aria-describedby="inputGroupFileAddon01">
                                    </div>
                                    
                                </div>
                                <div class="row" style="margin-top:5pt;">
                                    <div class="btn-group col-md-offset-6 ">
                                        <button type="button" id="botonCargaArchivo" title="Cargar Archivo" class="btn btn-success" onclick="cargarArchivo();">
                                        Cargar Archivo <i class="fa fa-plus"></i></button>
                                    </div>
                                </div>
                                
                            </form>
                            <div class="table-toolbar row">
                                <div id="resumen" class="col-md-12">

                                </div>
                            </div>

                            {{-- <div class="row" id="divTablaDocSigfeNoAsignados" style="display:none;">
                                <div class="col-md-12">
                                    <div class="panel">
                                        <div class="panel-heading">
                                            <div class="caption">
                                                <strong style="color: #0a74d0">ID SIGFE no asignado.</strong>
                                            </div>
                                        </div>
                                        <div class="panel-body">
                                            <table class="table table-striped table-bordered table-hover table-full-width" id="tabla_documentos_sigfe_no_asignados">
                                            </table>
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <div class="row" id="divTablaDocAsignadoSigfe" style="display:none;">
                                <div class="col-md-12">
                                    <div class="panel">
                                        <div class="panel-heading">
                                            <div class="caption">
                                                <strong style="color: #1c05e4">Documentos con el ID SIGFE asignado.</strong>
                                            </div>
                                        </div>
                                        <div class="panel-body">
                                            <table class="table table-striped table-bordered table-hover table-full-width" id="tabla_documentos_asignado_sigfe">
                                            </table>
                                        </div>
                                    </div>
                                </div>
                            </div> --}}

						</div>
					</div>
					<!-- END EXAMPLE TABLE PORTLET-->
				</div>
			</div>
			<!-- END PAGE CONTENT-->
			
			
			
			
		</div> <!--/div class="page-content" -->
	</div> <!--/div class="page-content-wrapper" -->
</div><!--/div class="page-content-wrapper" -->
<!-- END CONTENT -->
<div id="modal"></div>
<div id="modal2"></div>
@endsection

@push('script-footer')
<!-- BEGIN PAGE LEVEL PLUGINS -->
<script type="text/javascript" src="{{ asset('assets/plugins/select2/select2.min.js') }} "></script>
{{--<script type="text/javascript" src="{{ asset('assets/plugins/data-tables/jquery.dataTables.min.js') }}"></script>--}}
<script type="text/javascript" src="{{ asset('datatables/jquery.dataTables.min.js') }}"></script>
<script type="text/javascript" src="{{ asset('assets/plugins/data-tables/DT_bootstrap.js') }}"></script>
{{-- Para botones de las datatables --}}
<script type="text/javascript" src="{{ asset('datatables/dataTables.buttons.min.js') }}"></script>
<script type="text/javascript" src="{{ asset('datatables/buttons.flash.min.js') }}"></script>
<script type="text/javascript" src="{{ asset('datatables/jszip.min.js') }}"></script>
<script type="text/javascript" src="{{ asset('datatables/buttons.html5.min.js') }}"></script>


<link rel="stylesheet" type="text/css" href="{{asset('assets/plugins/bootstrap-datepicker/css/datepicker.css')}}"/>

<script type="text/javascript" src="{{asset('assets/plugins/bootstrap-datepicker/js/bootstrap-datepicker.js')}}"></script>
<script type="text/javascript" src="{{asset('assets/plugins/bootstrap-datetimepicker/js/bootstrap-datetimepicker.js')}}"></script>


<!-- END PAGE LEVEL PLUGINS -->
<!-- BEGIN PAGE LEVEL SCRIPTS -->
<script src="{{ asset('assets/scripts/app.js') }} "></script>
<script src="{{ asset('assets/scripts/table-advanced.js') }} "></script>


<script type="text/javascript">
	$(document).ready(function() {
		App.init(); // initlayout and core plugins

        $("#documentos").addClass( "active" );
        $("#ingreso").addClass( "active" );
        $("#carga-reclamados-li").addClass( "active" );
        $("#documentos-a").append( '<span class="selected"></span>' );

	});

    function validarArchivo()
    {
        let archivo = $('#archivo').val().replace(/.*(\/|\\)/, ''); //val();
        let extension = archivo.substring(archivo.lastIndexOf('.'));
        if ( archivo == '' ) {
            toastr.warning('No se ha seleccionado documento', 'Atención', optionsToastr);
            listaCargaArchivo();
            return false;
        } else {
            if ( extension != '.xlsx' ) {
                toastr.warning('El archivo no tiene la extension correcta: '+'<strong>XLSX</strong>', 'Atención', optionsToastr);
                $('#archivo').val('');
                listaCargaArchivo();
                return false;
            } else {
                return true;
            }
        }
    }

    function cargarArchivo() 
    {
        esperandoCargaArchivo();

        if ( validarArchivo() ) {
            let formData = new FormData($('#form_carga_archivo')[0]);

            $.ajaxSetup({
                headers: {  'X-CSRF-TOKEN': '{{ csrf_token() }}'}
            });

            $.ajax({
                data: formData, 
                url:   '{{url('archivos_acepta/carga_reclamados')}}',
                type:  'post',
                processData: false,
                contentType: false,

                success: function(respuesta) {
                    console.log(respuesta);
                    // return false;

                    if ( respuesta.estado == 'error' ) {
                        toastr.error('No es posible realizar la acción'+'<br><strong>Error: '+ respuesta.mensaje +'</strong>', 'Atención', optionsToastr);
                    } else if ( respuesta.estado == 'success') {
                        $('#archivo').val('');
                        // $('#form_carga_archivo').css('display','none');
                        toastr.success(respuesta.mensaje, 'Atención', optionsToastr);
                        $('#resumen').html(respuesta.resumen);
                        // crearTablas(respuesta);
                    }
                }//succes
            }).fail( function(data) {
                toastr.error('No es posible realizar la acción', 'Atención', optionsToastr);
            })
            .always(function() {
                listaCargaArchivo();
            });//ajax
        }
        
    }

    function esperandoCargaArchivo()
    {   
        $('.page-header-fixed *').css('cursor', 'wait');
        $("#botonCargaArchivo").attr("disabled",true);
    }

    function listaCargaArchivo()
    {
        $('.page-header-fixed *').css('cursor', '');
        $("#botonCargaArchivo").attr("disabled",false);
    }

    function crearTablas(respuesta)
    {

        var tablaErrorSigfe = $('#tabla_documentos_sigfe_no_asignados').DataTable({
            data: respuesta.datosTablaSigfeNoAsignados,         
            columns: [
                { title: "Proveedor" },
                { title: "Tipo Documento" },
                { title: "N° Documento" },
                { title: "Monto Doc." },
                { title: "ID SIGFE" },
                { title: "Fecha SIGFE"},
            ],
            dom: 'Blfrtip',
            // buttons: [ 'excel' ],
            // initComplete: function () {
            //     $('.buttons-excel').html('<button class="btn btn-success btn-xs" title="Generar Excel" type="button"><i class="fas fa-file-excel"></i> Exportar a Excel</button>')
            // },
            buttons: [{ 
                extend: 'excelHtml5',
                filename: 'Carga SIGFE _ ID SIGFE no asignado'
            }],
            initComplete: function () {
                $('.buttons-excel').html('<i class="fas fa-file-excel"></i> Excel')
            },
            "deferRender": true,
            "language": {
                "emptyTable":     "No hay datos disponibles en la tabla",
                "info":           "Mostrando registros del _START_ al _END_ de un total de _TOTAL_ registros",
                "infoEmpty":      "No hay datos disponibles para mostrar en la tabla",
                "infoFiltered":   "(filtrado de un total de _MAX_ registros)",
                "infoPostFix":    "",
                "thousands":      ",",
                "lengthMenu":     "Mostrar _MENU_ registros",
                "loadingRecords": "Cargando...",
                "processing":     "Procesando...",
                "search":         "Buscar: ",
                "zeroRecords":    "<i class='far fa-frown fa-lg'></i> No hay datos que coincidan con la búsqueda <i class='far fa-frown fa-lg'></i>",
                "paginate": {
                    "first":      "Primero",
                    "last":       "Último",
                    "next":       "Siguiente",
                    "previous":   "Anterior"
                },
                "aria": {
                    "sortAscending":  ": Activar para ordenar la columna de manera ascendente",
                    "sortDescending": ": Activar para ordenar la columna de manera descendente"
                }
            },
            "lengthMenu": [
                [10, 15, 20, -1],
                [10, 15, 20, "Todos"] // change per page values here
            ],
            // set the initial value
            "displayLength": 10,
            "paging":   true,
            "ordering": true,
            "info":     true,
            "searching": true,
            "orderClasses": false,
        });
        jQuery('#tabla_documentos_sigfe_no_asignados_filter input').addClass("form-control input-large"); // modify table search input
        jQuery('#tabla_documentos_sigfe_no_asignados_wrapper .dataTables_length select').addClass("form-control input-small"); // modify table per page dropdown
        jQuery('#tabla_documentos_sigfe_no_asignados_wrapper .dataTables_length select').select2(); // initialize select2 dropdown

        $('#tabla_documentos_sigfe_no_asignados_column_toggler input[type="checkbox"]').change(function() {
            /* Get the DataTables object again - this is not a recreation, just a get of the object */
            var iCol = parseInt($(this).attr("data-column"));
            var bVis = oTable.fnSettings().aoColumns[iCol].bVisible;
            oTable.fnSetColumnVis(iCol, (bVis ? false : true));
        });

        $('#divTablaDocSigfeNoAsignados').css('display','block');
        /////////////////////////////////////

        var tablaAsignadoSigfe = $('#tabla_documentos_asignado_sigfe').DataTable({
            data: respuesta.datosTablaAsignadoSigfe,
            // Set rows IDs
            rowId: function(data) {
                return 'tr_' + data.DT_RowID;
            },
            columns: [
                { title: "Estado Doc." },
                { title: "Rut Proveedor" },
                { title: "Nombre Proveedor" },
                { title: "Tipo Documento" },
                { title: "N° Documento" },
                { title: "Tot. Orig.", className: "text-right" },
                { title: "Tot. Act.", className: "text-right" },
                { title: "ID SIGFE" },
                { title: "Fecha SIGFE"},
                { title: "<div class='text-center'><i class='fa fa-cog'></i></div>" }
            ],
            dom: 'Blfrtip',
            buttons: [{ 
                extend: 'excelHtml5',
                filename: 'Carga SIGFE _ Documentos con el ID SIGFE asignado'
            }],
            initComplete: function () {
                $('.buttons-excel').html('<i class="fas fa-file-excel"></i> Excel')
            },
            "deferRender": true,
            "language": {
                "emptyTable":     "No hay datos disponibles en la tabla",
                "info":           "Mostrando registros del _START_ al _END_ de un total de _TOTAL_ registros",
                "infoEmpty":      "No hay datos disponibles para mostrar en la tabla",
                "infoFiltered":   "(filtrado de un total de _MAX_ registros)",
                "infoPostFix":    "",
                "thousands":      ",",
                "lengthMenu":     "Mostrar _MENU_ registros",
                "loadingRecords": "Cargando...",
                "processing":     "Procesando...",
                "search":         "Buscar: ",
                "zeroRecords":    "<i class='far fa-frown fa-lg'></i> No hay datos que coincidan con la búsqueda <i class='far fa-frown fa-lg'></i>",
                "paginate": {
                    "first":      "Primero",
                    "last":       "Último",
                    "next":       "Siguiente",
                    "previous":   "Anterior"
                },
                "aria": {
                    "sortAscending":  ": Activar para ordenar la columna de manera ascendente",
                    "sortDescending": ": Activar para ordenar la columna de manera descendente"
                }
            },
            "lengthMenu": [
                [10, 15, 20, -1],
                [10, 15, 20, "Todos"] // change per page values here
            ],
            // set the initial value
            "displayLength": 10,
            "paging":   true,
            "ordering": true,
            "info":     true,
            "searching": true,
            "orderClasses": false,
        });
        jQuery('#tabla_documentos_asignado_sigfe_filter input').addClass("form-control input-large"); // modify table search input
        jQuery('#tabla_documentos_asignado_sigfe_wrapper .dataTables_length select').addClass("form-control input-small"); // modify table per page dropdown
        jQuery('#tabla_documentosasignado_sigfe_wrapper .dataTables_length select').select2(); // initialize select2 dropdown

        $('#tabla_documentos_asignado_sigfe_column_toggler input[type="checkbox"]').change(function() {
            /* Get the DataTables object again - this is not a recreation, just a get of the object */
            var iCol = parseInt($(this).attr("data-column"));
            var bVis = oTable.fnSettings().aoColumns[iCol].bVisible;
            oTable.fnSetColumnVis(iCol, (bVis ? false : true));
        });

        $('#divTablaDocAsignadoSigfe').css('display','block');

    }

    // function verDocumento(id)
    // {
    //     $.get( '{{ url("documentos/modal/ver") }}/' + id, function( data ) {
    //         $( "#modal" ).html( data );
    //         $( "#modalVer" ).modal();
    //     });
    // }

    // function carga(idProveedor, idTipoDocumento, numeroDocumento, totalDocumento, idSigfe, fechaSigfe, keyTr)
    // {
    //     console.log('Carga');
    //     console.log(idProveedor, idTipoDocumento, numeroDocumento, totalDocumento, idSigfe, fechaSigfe, keyTr);

    //     $.get( '{{ url("devengar/modal/carga_sigfe") }}/' + idProveedor + '/' + idTipoDocumento + '/' + 
    //             numeroDocumento + '/' + totalDocumento + '/' + idSigfe + '/' + fechaSigfe + '/' + keyTr  , function( data ) {
    //         $( "#modal" ).html( data );
    //         $( "#modalCargaSigfe" ).modal();
    //     });
    // }
</script>

@endpush