@extends('master')

@section('title', 'Carga de documentos acepta')

@push('css-head')
<!-- BEGIN PAGE LEVEL STYLES -->
<link rel="stylesheet" type="text/css" href="{{asset('assets/plugins/select2/select2_conquer.css')}}"/>
<link rel="stylesheet" href="{{asset('assets/plugins/data-tables/DT_bootstrap.css')}}"/>
<!-- END PAGE LEVEL STYLES -->

@endpush

@section('content')

<!-- BEGIN CONTENT -->

<div class="page-content-wrapper">
	<div class="page-content-wrapper">
		<div class="page-content">

			<!-- BEGIN PAGE CONTENT-->
			<div class="row">
				<div class="col-md-12">
					<!-- BEGIN EXAMPLE TABLE PORTLET-->
					<div class="portlet">
						<div class="portlet-title">
							<div class="caption">
								<i class="fa fa-files-o" style="color: black;"></i> Carga de documentos acepta
							</div>
                            {{--
							<div class="actions">
								<div class="btn-group">
									<a class="btn btn-info dropdown-toggle" href="#" data-toggle="dropdown">
									Columnas <i class="fa fa-angle-down"></i>
									</a>
									<div id="sample_2_column_toggler" class="dropdown-menu hold-on-click dropdown-checkboxes pull-right">
										<label><input type="checkbox" checked data-column="0">Nombre</label>
										<label><input type="checkbox" checked data-column="1">Correo</label>
										<label><input type="checkbox" checked data-column="2">Rut</label>
										<label><input type="checkbox" checked data-column="3">Teléfono Contacto</label>
										<label><input type="checkbox" checked data-column="4">Acción</label>
									</div>
								</div>
							</div>
                            --}}
						</div>
						<div class="portlet-body">
							@if(session("success"))

								@if(Session::get("success")=="carga-archivo")
									<div class="alert alert-success alert-dismissable">
										<button type="button" title="Cerrar" class="close" data-dismiss="alert" aria-hidden="true"></button>
										<i class="fa fa-thumbs-up fa-flip-horizontal"></i> <strong>Ingreso de documento exitoso.</strong> 
                                        Existen {{count($archivosAcepta)}} archivos xls en el sistema.
									</div>
								@endif

							@endif
                            <form action="{{url('archivos_acepta/carga_archivo')}}" method="post" enctype="multipart/form-data" id="form_carga_archivo" class="form-horizontal">
                                <h4 class="form-section" style="color: #69aa46;">Documento en XLS</h4>
                                <div class="row">
                                    <div class="input-group col-sm-6 col-sm-offset-1">
                                        <span class="input-group-addon" id="inputGroupFileAddon01"><strong>Seleccione Documento</strong></span>
                                        <input type="file" class="form-control" accept=".xls" name="archivo" id="archivo" aria-describedby="inputGroupFileAddon01">
                                    </div>
                                    
                                </div>
                                <div class="row" style="margin-top:5pt;">
                                    <div class="btn-group col-md-offset-6 ">
                                        <button type="button" id="botonCargaArchivo" title="Cargar Archivo" class="btn btn-success" onclick="cargarArchivo();">
                                        Cargar Archivo <i class="fa fa-plus"></i></button>
                                    </div>
                                </div>
                                
                            </form>
                            <div class="table-toolbar">
                                    
                            </div>
                            <div class="row">
                                <div class="col-md-12">
                                    <div class="panel panel-default">
                                        <div class="panel-heading">
                                            <div class="caption">
                                                <strong style="color: #69aa46;">Últimos Documentos Cargados</strong>
                                            </div>
                                        </div>
                                        <div class="panel-body">
                                            <div class="table-responsive">
                                                <table class="table table-striped table-bordered table-hover table-full-width" id="sample_2">
                                                    <thead>
                                                    <tr>
                                                        <th>Fecha Ingreso</th>
                                                        <th>Ingresado por</th>
                                                        <th class="hidden-xs">Nombre del archivo</th>
                                                        <th class="hidden-xs">Ubicación</th>
                                                        <th class="hidden-xs">Extensión</th>
                                                        <th class="hidden-xs">Peso</th>
                                                        <th class="hidden-xs">
                                                            <i class="fa fa-cog"></i>
                                                        </th>
                                                    </tr>
                                                    </thead>
                                                    <tbody>
                                                    @forelse($archivosAcepta as $archivo)
                                                    <tr>
                                                        <td>{{ fecha_dmY($archivo->ingreso) }}</td>
                                                        <td>{{ $archivo->getUser->name }}</td>
                                                        <td>{{ $archivo->nombre_original }}</td>
                                                        <td>{{ $archivo->ubicacion }}</td>
                                                        <td>{{ $archivo->extension }}</td>
                                                        <td>{{ pesoArchivoEnMB($archivo->peso) }}</td>
                                                        <td>
                                                            <div class="btn-group">
                                                                @permission(['ver-excel-acepta'])
                                                                <a class="btn btn-success btn-xs" @if($archivo->cargado == 0) href="{{ asset( $archivo->ubicacion ) }}" @else href="http://{{ $archivo->ubicacion }}" @endif target="_blank" title="Descargar Archivo">
                                                                    <i class="fa fa-eye"></i>
                                                                </a>
                                                                @endpermission
                                                            </div>
                                                        </td>
                                                    </tr>
                                                    @empty

                                                    @endforelse
                                                    </tbody>
                                                </table>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
						</div>
					</div>
					<!-- END EXAMPLE TABLE PORTLET-->
				</div>
			</div>
			<!-- END PAGE CONTENT-->
			
			
			
			
		</div> <!--/div class="page-content" -->
	</div> <!--/div class="page-content-wrapper" -->
</div><!--/div class="page-content-wrapper" -->
<!-- END CONTENT -->
<div id="modal"></div>
@endsection

@push('script-footer')
<!-- BEGIN PAGE LEVEL PLUGINS -->
<script type="text/javascript" src="{{ asset('assets/plugins/select2/select2.min.js') }} "></script>
{{--<script type="text/javascript" src="{{ asset('assets/plugins/data-tables/jquery.dataTables.min.js') }}"></script>--}}
<script type="text/javascript" src="{{ asset('datatables/jquery.dataTables.min.js') }}"></script>
<script type="text/javascript" src="{{ asset('assets/plugins/data-tables/DT_bootstrap.js') }}"></script>


<!-- END PAGE LEVEL PLUGINS -->
<!-- BEGIN PAGE LEVEL SCRIPTS -->
<script src="{{ asset('assets/scripts/app.js') }} "></script>
<script src="{{ asset('assets/scripts/table-advanced.js') }} "></script>


<script type="text/javascript">
	$(document).ready(function() {
		App.init(); // initlayout and core plugins
		TableAdvanced.init();

   		$("#documentos").addClass( "active" );
        $("#documentos-a").append( '<span class="selected"></span>' );
        $("#ingreso").addClass( "active" );
        $("#acepta").addClass( "active" );
        $("#carga-acepta-li").addClass( "active" );
        $("#acepta-a").append( '<span class="selected"></span>' );
        $("#acepta ul").css( "display","block");

		$(".delete-data").click(function(){
		  var data = $(this).data("role");
		  $.get( "pacientes/delete/" + data, function( data ) {
			  $( "#modal" ).html( data );
			  $( "#modalEliminar" ).modal();
			});
		});

		$(".view-data").click(function(){
		  var data = $(this).data("role");
		  $.get( "pacientes/" + data, function( data ) {
			  $( "#modal" ).html( data );
			  $( "#modalVer" ).modal();
			});
		});

	});

    function validarArchivo()
    {
        let archivo = $('#archivo').val().replace(/.*(\/|\\)/, ''); //val();
        let extension = archivo.substring(archivo.lastIndexOf('.'));
        if ( archivo == '' ) {
            toastr.warning('No se ha seleccionado documento', 'Atención', optionsToastr);
            listaCargaArchivo();
            return false;
        } else {
            if ( extension != '.xls' ) {
                toastr.warning('El archivo no tiene la extension correcta: '+'<strong>XLS</strong>', 'Atención', optionsToastr);
                $('#archivo').val('');
                listaCargaArchivo();
                return false;
            } else {
                return true;
            }
        }
    }

    function cargarArchivo() 
    {
        esperandoCargaArchivo();

        if ( validarArchivo() ) {
            let formData = new FormData($('#form_carga_archivo')[0]);

            $.ajaxSetup({
                headers: {  'X-CSRF-TOKEN': '{{ csrf_token() }}'}
            });

            $.ajax({
                data: formData, 
                url:   '{{url('archivos_acepta/carga_archivo/')}}',
                type:  'post',
                processData: false,
                contentType: false,

                success: function(respuesta) {
                    if ( respuesta.estado == 'error' ) {
                        toastr.error('No es posible realizar la acción'+'<br><strong>Error: '+ respuesta.mensaje +'</strong>', 'Atención', optionsToastr);
                    } else if ( respuesta.estado == 'success') {
                        $('#archivo').val('');
                        toastr.success(respuesta.mensaje, 'Atención', optionsToastr);
                        nuevaFila(respuesta);
                    }
                }//succes
            }).fail( function(data) {
                toastr.error('No es posible realizar la acción', 'Atención', optionsToastr);
            })
            .always(function() {
                listaCargaArchivo();
            });//ajax
        }
        
    }

    function nuevaFila(respuesta)
    {
        let tabla = $('#sample_2').DataTable();

        tabla.row.add ([
            respuesta.fechaIngreso,
            respuesta.ingresadoPor,
            respuesta.nombreDelArchivo,
            respuesta.ubicacion,
            respuesta.extension,
            respuesta.peso,
            respuesta.boton
        ]).draw( false );
    }

    function esperandoCargaArchivo()
    {   
        $('.page-header-fixed *').css('cursor', 'wait');
        $("#botonCargaArchivo").attr("disabled",true);
    }

    function listaCargaArchivo()
    {
        $('.page-header-fixed *').css('cursor', '');
        $("#botonCargaArchivo").attr("disabled",false);
    }

</script>

@endpush