<link href="{{ asset('sistema/css/modales.css') }}?v={{rand()}}" rel="stylesheet" type="text/css"/>
<div class="modal fade" id="modalTraza" role="dialog" aria-labelledby="myModalLabel3" aria-hidden="true">
	<div class="modal-dialog" style="width:60%;">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" title="Cerrar" class="close" data-dismiss="modal" aria-hidden="true"></button>
				<h4 class="modal-title" ><strong><i class="fas fa-map-signs fa-lg"></i>&nbsp;&nbsp;Trazabilidad Archivo Acepta</strong></h4>
            </div>
          
            <div class="modal-body">

                <div class="form-group row">
                    <div class="col-xs-12">

                        <div class="form-group row">
                            <div class="col-xs-12">	
                                <h5 class="headers-traceability">
                                    Proveedor
                                </h5>
                                <hr style="margin-top: 0px; ">

                                <table class="table table-bordered " id="tabla_info_proveedor">
                                    <tr>
                                        <th class="th_sigfe" >Nombre</th>
                                        <td class="text-center">{{ $archivoAcepta->getProveedor ? $archivoAcepta->getProveedor->nombre : $archivoAcepta->razon_social_emisor }}</td>
                                        <th class="th_sigfe">Rut</th>
                                        <td class="text-center" >{{ $archivoAcepta->getProveedor ? $archivoAcepta->getProveedor->rut : $archivoAcepta->emisor }}</td>
                                    </tr>

                                    @if ( $archivoAcepta->getProveedor && $archivoAcepta->getProveedor->getProveedorMaestro )
                                        <tr>
                                            <th class="th_sigfe" colspan="2" >Proveedor "Maestro"</th>
                                            <td class="text-center" colspan="2">{{ $archivoAcepta->getProveedor->getProveedorMaestro->rut }} {{ $archivoAcepta->getProveedor->getProveedorMaestro->nombre }}</td>
                                        </tr>
                                    @endif

                                    @if ( $archivoAcepta->getProveedor && $archivoAcepta->getProveedor->getProveedoresFusion->count() > 0 )
                                        <tr>
                                            <th class="th_sigfe" colspan="4" >Proveedores Fusionados</th>
                                        </tr>

                                        @forelse ($archivoAcepta->getProveedor->getProveedoresFusion as $provFusion)
                                            <tr>
                                                <td class="text-center" colspan="4">{{ $provFusion->rut }} {{ $provFusion->nombre }}</td>
                                            </tr>
                                        @empty

                                        @endforelse

                                    @endif

                                </table>
                            </div>
                        </div>

                        <div class="form-group row">
                            <div class="col-xs-12">	
                                <h5 class="headers-traceability">
                                    Documento  @role(['propietario']) ID : {{ $archivoAcepta->id }} @endrole
                                    <a class="btn btn-success btn-xs" href="http://windte1806.acepta.com/ca4webv3/PdfViewMedia?url={{$archivoAcepta->uri}}" target="_blank" title="Ver Archivo"><i class="fas fa-eye fa-lg"></i></a>
                                </h5>
                                <hr style="margin-top: 0px; ">

                                <table class="table table-bordered " id="tabla_info_documento">

                                    <tr>
                                        <th class="th_sigfe" >Emisión</th>
                                        <td class="text-center">{{ fecha_dmY_hms($archivoAcepta->emision) }}</td>
                                        <th class="th_sigfe">Fecha Carga</th>
                                        <td class="text-center" >{{ fecha_dmY_hms($archivoAcepta->created_at) }}</td>
                                    </tr>

                                    <tr>
                                        <th class="th_sigfe" >Fecha Publicación</th>
                                        <td class="text-center">{{ fecha_dmY_hms($archivoAcepta->publicacion) }}</td>
                                        <th class="th_sigfe">Fecha Recepción SII</th>
                                        <td class="text-center" >{{ fecha_dmY_hms($archivoAcepta->fecha_recepcion_sii) }}</td>
                                    </tr>
                                        
                                    <tr>
                                        <th class="th_sigfe" >Tipo</th>
                                        <td class="text-center">{{ $archivoAcepta->getTipoDocumento ? $archivoAcepta->getTipoDocumento->nombre : $archivoAcepta->tipo_documento }}</td>
                                        <th class="th_sigfe">Número / Folio</th>
                                        <td class="text-center" >{{ $archivoAcepta->folio }}</td>
                                    </tr>

                                    @if ( $archivoAcepta->getDocumentoFactura )
                                        <tr>
                                            <th class="th_sigfe" colspan="2" >Re-Facturado con</th>
                                            <td class="text-center" colspan="2" >{{ $archivoAcepta->getDocumentoFactura->numero_documento }}</td>
                                        </tr>
                                    @endif

                                    <tr>
                                        <th class="th_sigfe"  >Monto Neto</th>
                                        <td class="text-center" >$ {{ formatoMiles($archivoAcepta->monto_neto) }}</td>
                                        <th class="th_sigfe" >Monto Exento</th>
                                        <td class="text-center" >$ {{ formatoMiles($archivoAcepta->monto_exento) }}</td>
                                    </tr>

                                    <tr>
                                        <th class="th_sigfe"  >Monto IVA</th>
                                        <td class="text-center" >$ {{ formatoMiles($archivoAcepta->monto_iva) }}</td>
                                        <th class="th_sigfe"  >Monto Total</th>
                                        <td class="text-center" >$ {{ formatoMiles($archivoAcepta->monto_total) }}</td>
                                    </tr>

                                    <tr>
                                        <th class="th_sigfe" >Referencias</th>
                                        <td class="text-center">{!! $archivoAcepta->getReferencias() !!}</td>
                                        <th class="th_sigfe text-center" style="color: #557ef9;vertical-align:middle;">Estado</th>
                                        <td class="text-center" style="font-weight: bold;vertical-align:middle;" >{!! $archivoAcepta->getEstadoAcepta() !!}</td>
                                    </tr>

                                    <tr>
                                        <th class="th_sigfe"  >Estado Acepta</th>
                                        <td class="text-center" >{{ $archivoAcepta->estado_acepta }}</td>
                                        <th class="th_sigfe"  >Estado SII</th>
                                        <td class="text-center" >{{ str_replace('_', ' ', $archivoAcepta->estado_sii) }}</td>
                                    </tr>

                                </table>

                                @if ( $archivoAcepta->id_user_rechazo != null )

                                    <h5 class="headers-traceability">
                                        Rechazo
                                    </h5>
                                    <hr style="margin-top: 0px; ">

                                    <table class="table table-bordered " id="tabla_info_problemas">

                                        <tr>
                                            <th class="th_sigfe text-center" colspan="2" >Motivos</th>
                                        </tr>

                                        @foreach ($archivoAcepta->getAceptaMotivoRechazo as $aceptaMotivoRechazar)
                                            <tr>
                                                <td class="text-center" colspan="2" >{{ $aceptaMotivoRechazar->getMotivoRechazo->nombre }}</td>
                                            </tr>
                                        @endforeach

                                        @if ( $archivoAcepta->getUsuarioRechazo )
                                            <tr>
                                                <th class="th_sigfe" >Responsable</th>
                                                <td class="text-center">{{ $archivoAcepta->getUsuarioRechazo->name }}</td>
                                            </tr>
                                        @endif

                                        @if ( $archivoAcepta->observacion_rechazo != null )
                                            <tr>
                                                <th class="th_sigfe" >Observaciones</th>
                                                <td class="text-center">{!! str_replace(PHP_EOL,'<br>',$archivoAcepta->observacion_rechazo) !!}</td>
                                            </tr>
                                        @endif

                                        <tr>
                                            <th class="th_sigfe" >Fecha</th>
                                            <td class="text-center">{{fecha_dmY_hms($archivoAcepta->getAceptaMotivoRechazo->first()->created_at) }}</td>
                                        </tr>

                                    </table>

                                @endif

                                @if ( $archivoAcepta->id_user_rezagar != null )

                                    <h5 class="headers-traceability">
                                        Rezagado
                                    </h5>
                                    <hr style="margin-top: 0px; ">

                                    <table class="table table-bordered " id="tabla_info_problemas">

                                        <tr>
                                            <th class="th_sigfe text-center" colspan="2" >Motivos</th>
                                        </tr>

                                        @foreach ($archivoAcepta->getAceptaMotivoRezagar as $aceptaMotivoRezagar)
                                            <tr>
                                                <td class="text-center" colspan="2" >{{ $aceptaMotivoRezagar->getMotivoRezagar->nombre }}</td>
                                            </tr>
                                        @endforeach

                                        @if ( $archivoAcepta->getUsuarioRezagado )
                                            <tr>
                                                <th class="th_sigfe" >Responsable</th>
                                                <td class="text-center">{{ $archivoAcepta->getUsuarioRezagado->name }}</td>
                                            </tr>
                                        @endif

                                        @if ( $archivoAcepta->observacion_rezagar != null )
                                            <tr>
                                                <th class="th_sigfe" >Observaciones</th>
                                                <td class="text-center">{!! str_replace(PHP_EOL,'<br>',$archivoAcepta->observacion_rezagar) !!}</td>
                                            </tr>
                                        @endif

                                        <tr>
                                            <th class="th_sigfe" >Fecha</th>
                                            <td class="text-center">{{fecha_dmY_hms($archivoAcepta->getAceptaMotivoRezagar->first()->created_at) }}</td>
                                        </tr>

                                    </table>

                                @endif

                                @if ( $archivoAcepta->getArchivoAceptaRevisionBodega )

                                    <h5 class="headers-traceability">
                                        Revisión Bodega
                                    </h5>
                                    <hr style="margin-top: 0px; ">

                                    <table class="table table-bordered " id="tabla_info_problemas">

                                        @if ( $archivoAcepta->getArchivoAceptaRevisionBodega->getUsuarioEnviaToRevision )
                                            <tr>
                                                <th class="th_sigfe" >Responsable</th>
                                                <td class="text-center">{{ $archivoAcepta->getArchivoAceptaRevisionBodega->getUsuarioEnviaToRevision->name }}</td>
                                            </tr>
                                        @endif

                                        <tr>
                                            <th class="th_sigfe" >Fecha</th>
                                            <td class="text-center">{{fecha_dmY_hms($archivoAcepta->getArchivoAceptaRevisionBodega->created_at) }}</td>
                                        </tr>

                                        @if ( $archivoAcepta->getArchivoAceptaRevisionBodega->revisado == 1 )
                                            <tr>
                                                <th class="th_sigfe text-center" colspan="2" >Revisado</th>
                                            </tr>

                                            <tr>
                                                <th class="th_sigfe text-center" colspan="2" >
                                                    @if ( $archivoAcepta->getArchivoAceptaRevisionBodega->recepcion_total == 1 )
                                                        Recepción Total
                                                    @endif

                                                    @if ( $archivoAcepta->getArchivoAceptaRevisionBodega->recepcion_parcial == 1 )
                                                        Recepción Parcial
                                                    @endif

                                                    @if ( $archivoAcepta->getArchivoAceptaRevisionBodega->sin_recepcion == 1 )
                                                        Sin Recepción
                                                    @endif
                                                </th>
                                            </tr>

                                            <tr>
                                                <th class="th_sigfe" >Comentario</th>
                                                <td class="text-center">{!! str_replace(PHP_EOL,'<br>',$archivoAcepta->getArchivoAceptaRevisionBodega->comentario) !!}</td>
                                            </tr>

                                            @if ( $archivoAcepta->getArchivoAceptaRevisionBodega->getUsuarioRespuesta )
                                                <tr>
                                                    <th class="th_sigfe" >Responsable Respuesta</th>
                                                    <td class="text-center">{{ $archivoAcepta->getArchivoAceptaRevisionBodega->getUsuarioRespuesta->name }}</td>
                                                </tr>
                                            @endif
                                        @endif

                                    </table>

                                @endif
                            </div>
                        </div>

                        <div class="form-group row">
                            <div class="col-xs-12">	
                                <h5 class="headers-traceability">
                                    Responsables
                                </h5>
                                <hr style="margin-top: 0px; ">

                                <table class="table table-bordered " id="tabla_info_responsables">
                                    <tr>
                                        <th class="th_sigfe text-center" >Ingreso</th>
                                        <td class="text-center">{{ $archivoAcepta->getUsuarioResponsable ? $archivoAcepta->getUsuarioResponsable->name : '' }}</td>
                                    </tr>

                                    @if( $archivoAcepta->getUsuarioQuitaRechazo )
                                        <tr>
                                            <th class="th_sigfe text-center">Quita Rechazo</th>
                                            <td class="text-center" >{{ $archivoAcepta->getUsuarioQuitaRechazo->name }}</td>
                                        </tr>
                                    @endif

                                    @if( $archivoAcepta->getUsuarioQuitaRezagado )
                                        <tr>
                                            <th class="th_sigfe text-center">Quita Rezagado</th>
                                            <td class="text-center" >{{ $archivoAcepta->getUsuarioQuitaRezagado->name }}</td>
                                        </tr>
                                    @endif

                                </table>
                            </div>
                        </div>


                    </div> <!-- / div class col-xs-12 -->


                </div>

            </div>{{-- /modal-body --}}

            {{--
            <div class="modal-footer">
                <div class="btn-group">
                <button type="button" class="btn btn-xs btn-default" title="Cancelar" data-dismiss="modal" aria-hidden="true">Cancelar</button>
                </div>
            </div> --}} {{-- /modal-footer --}}
          	
		</div>
	</div>
</div>

 <script type="text/javascript">
    var tablaItemsView = $('#tabla_item_presupuestario').DataTable({
        "language": {
            "emptyTable": "No hay datos disponibles en la tabla",
        },
        "paging":   false,
        "ordering": false,
        "info":     false,
        "searching": false
    });
 
    var tablaArchivoView = $('#tabla_info_archivo').DataTable({
         "language": {
            "emptyTable": "No hay datos disponibles en la tabla",
        },
        "paging":   false,
        "ordering": false,
        "info":     false,
        "searching": false
    });

 	$(document).ready(function(){
         
  
 	});

    
 </script>