<link href="{{ asset('sistema/css/modales.css') }} " rel="stylesheet" type="text/css"/>
<div class="modal fade" id="modalRechazar" role="dialog" aria-labelledby="myModalLabel3" aria-hidden="true">
	<div class="modal-dialog" style="width:95%;">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" title="Cerrar" class="close" data-dismiss="modal" aria-hidden="true"></button>
				<h4 class="modal-title" ><strong><i class="fas fa-ban"></i>&nbsp;&nbsp;Rechazar Documento</strong></h4>
            </div>
            <form action="{{ asset('archivos_acepta/rechazar') }}" method="post" class="horizontal-form" id="form" autocomplete="off">
                <div class="modal-body">
                    <div class="note note-danger" id="divErrores" style="display:none;">
                        <h4 class="block" style="margin-bottom:5 px;">Debe Completar los siguientes campos: </h4>
                        <ul id="ulErrores"></ul>
                    </div>

                    <div class="form-group row" style="margin-bottom: 0px;">
                        <div class="col-xs-12 col-md-12">
                            <div class="form-group row">
                                <div class="col-xs-6 col-md-6">	
                                    <h4 class="headers-delete text-center">
                                    <i class="fa fa-file-text"></i> Documento Acepta
                                    </h4>
                                    <hr style="background-color: black;height: 2px;">
                                    <div class="form-group row">
                                        <div class="col-xs-12">	
                                            <h4 class="headers-delete">
                                                Proveedor
                                            </h4>
                                            <hr>
                                            <div class="form-group">
                                                <label class="col-sm-3 control-label font-bold">Rut Proveedor</label>
                                                <label class="col-sm-2 control-label">@if($archivoAcepta->getProveedor){{ \Rut::parse($archivoAcepta->getProveedor->rut)->format() }}@endif</label>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="form-group row">
                                        <div class="col-xs-12">
                                            <label class="col-xs-3 control-label font-bold">Nombre Proveedor</label>
                                            <label class="col-xs-9 control-label">@if($archivoAcepta->getProveedor){{ $archivoAcepta->getProveedor->nombre }}@endif</label>
                                        </div>
                                    </div>

                                    <div class="form-group row">
                                        <div class="col-xs-12">	
                                            <h4 class="headers-delete">
                                                Documento
                                            </h4>
                                            <hr>
                                            <div class="form-group">
                                                <label class="col-sm-2 control-label font-bold">Tipo Documento</label>
                                                <label class="col-sm-4 control-label ">{{ $archivoAcepta->getTipoDocumento->nombre }}</label>
                                                <label class="col-sm-2 control-label font-bold">N° Documento</label>
                                                <label class="col-sm-4 control-label ">{{ $archivoAcepta->folio }}</label>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="form-group row">
                                        <div class="col-xs-12">
                                            <div>
                                                <label class="col-sm-2 control-label font-bold">Fecha de Documento</label>
                                                <label class="col-sm-4 control-label ">{{ fecha_dmY($archivoAcepta->emision) }}</label>
                                                <label class="col-sm-2 control-label font-bold">Valor Total</label>
                                                <label class="col-sm-4 control-label ">$ {{ formatoMiles($archivoAcepta->monto_total) }}</label>
                                            </div>
                                        </div>
                                    </div>
                                </div> <!-- / div class col-xs-6 -->

                                
                                <div class="col-xs-6 col-md-6">	
                                    <h4 class="headers-delete text-center">
                                        <i class="fas fa-comment"></i> Motivo de Rechazo
                                    </h4>
                                    <hr style="background-color: black;height: 2px;">
                                    <div class="form-group row">
                                        <div class="col-xs-12">
                                            <h4 class="headers-delete">
                                                Motivos
                                            </h4>
                                            <hr>
                                            <div class="form-group">

                                                <label for="motivos" class="col-sm-2 control-label label-form">Seleccione</label>
                                                <div class="col-sm-10 form-validate">
                                                    <select name="motivos[]" id="motivos" class="form-control" multiple style="width: 100%" required>
                                                        @foreach ( $motivos as $motivo )
                                                            <option value="{{ $motivo->id }}" >{{ $motivo->nombre }}</option>
                                                        @endforeach
                                                    </select>
                                                </div>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="form-group row">
                                            <div class="col-xs-12">	
                                                <h4 class="headers-delete">
                                                    Observación Rechazo
                                                </h4>
                                                <hr>
                                                <div class="form-group">
                                                    <div class="col-sm-12 form-validate">
                                                        <textarea class="form-control noresize" id="observacion_rechazo" name="observacion_rechazo"
                                                                    style="height: 150px!important;"></textarea>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                </div> <!-- / div class col-xs-6 -->
                            </div>
                        </div>
                    </div>

                
                    <h4 class="headers-delete"><i class="fas fa-exclamation-circle" style="color:black"></i> El documento es rechazado solo en el sistema de Finanzas.</h4>
                </div> {{-- /modal-body --}}

            
                @csrf
                <input type="hidden" name="_id" value="{{ $archivoAcepta->id }}">
                <div class="modal-footer form-actions right">
                    
                    <button type="button" class="btn btn-default" title="Cerrar" data-dismiss="modal" aria-hidden="true">Cerrar</button>
                    <button type="submit" title="Rechazar Documento" class="btn btn-danger" id="botonRechazar"><i class="fas fa-ban" style="color:black;"></i> Rechazar Documento</button>
                </div> {{-- /modal-footer --}}
            </form>
          	
		</div>
	</div>
</div>

<script>
    $("#motivos").select2({
        allowClear: true,
    });

    $('#motivos').on('select2-selecting', function (e) {
        setRequiredObservacionRechazo(parseInt(e.val),'Agregar');
    });

    $('#motivos').on('select2-removing', function (e) {
        setRequiredObservacionRechazo(parseInt(e.val),'Quitar');
    });

    function setRequiredObservacionRechazo(motivoSeleccionado, opcion) {
        if (opcion == 'Agregar') {

            if (motivoSeleccionado == 4) {
                $('#observacion_rechazo').prop('required',true);
            }

        } else if (opcion == 'Quitar') {

            if (motivoSeleccionado == 4) {
                $('#observacion_rechazo').prop('required',false);
            }

        }
    }

    $(document).ready(function() {

        $("#form").validate({
            highlight: function(element) {
                $(element).closest('.form-validate').removeClass('has-success');
                $(element).closest('.form-validate').addClass('has-error');
            },
            unhighlight: function(element) {
                $(element).closest('.form-validate').removeClass('has-error');
                $(element).closest('.form-validate').addClass('has-success');
            },
            errorElement: 'span',
            errorClass: 'help-block',
            errorPlacement: function (error, element) {
                console.log(element);
                if (element.parent('.input-group').length) {
                    error.insertAfter(element.parent());
                } else {
                    error.insertAfter(element);
                }
            },
            rules: {
            },
            messages: {
            },

            //para enviar el formulario por ajax
            submitHandler: function(form) {
                esperandoEliminar();

                let formData = new FormData(form);

                $.ajax({
                    url: form.action,
                    type: form.method,
                    //data: $(form).serialize(),
                    data: formData,
                    processData: false,
                    contentType: false,
                    success: function(respuesta) {
                        console.log(respuesta);

                        if ( respuesta.estado == 'error' ) {
                            toastr.error('No es posible realizar la acción'+'<br><strong>Error: '+ respuesta.mensaje +'</strong>', 'Atención', optionsToastr);
                        } else if ( respuesta.estado == 'success') {
                            
                            toastr.success(respuesta.mensaje, 'Atención', optionsToastr);
                            // Cambiar la informacion de la fila de la tabla principal
                            quitarElementoTabla(respuesta);
                            $("#modalRechazar").modal("hide");
                            
                        }
                        
                    }            
                }).fail( function(respuesta) {//fail ajax
                    if ( respuesta.status == 400 ) {
                        mostrarErroresValidator(respuesta);
                    } else if ( respuesta.status == 500 ) {
                        toastr.error('No es posible realizar la acción, error en el servidor', 'Atención', optionsToastr);
                    } else {
                        toastr.error('No es posible realizar la acción', 'Atención', optionsToastr);
                    }
                    
                })
                .always(function() {
                    listoEliminar();
                });//ajax
                
            }

        });

    });

    function esperandoEliminar()
    {   
        $('.page-header-fixed *').css('cursor', 'wait');
        $("#botonRechazar").attr("disabled",true);
    }

    function listoEliminar()
    {
        $('.page-header-fixed *').css('cursor', '');
        $("#botonRechazar").attr("disabled",false);
    }

    function mostrarErroresValidator(respuesta)
    {
        if ( respuesta.responseJSON ) {
            //console.log(respuesta.responseJSON);
            let htmlErrores = '';
            for (let k in respuesta.responseJSON) { 
                //console.log(k, respuesta.responseJSON[k]);
                htmlErrores += '<li>' + respuesta.responseJSON[k] + '</li>';
            }

            $('#ulErrores').html(htmlErrores);
            $('#divErrores').css('display','');
            toastr.error('No es posible realizar la acción'+'<br>'+'Errores:<br> <ul>'+htmlErrores+'</ul>', 'Atención', optionsToastr);
        }
    }

    function quitarElementoTabla(respuesta)
    {
        $('#doc_' + respuesta.id).fadeOut(400, function(){
            tablaDocsAcepta.row('#doc_' + respuesta.id ).remove().draw();
        });
    }
</script>