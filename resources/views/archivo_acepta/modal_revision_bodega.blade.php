<link href="{{ asset('sistema/css/modales.css') }} " rel="stylesheet" type="text/css"/>
<div class="modal fade" id="modalRevisionBodega" role="dialog" aria-labelledby="myModalLabel3" aria-hidden="true">
	<div class="modal-dialog" style="width:45%;">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" title="Cerrar" class="close" data-dismiss="modal" aria-hidden="true"></button>
				<h4 class="modal-title" ><strong><i class="fas fa-eye"></i>&nbsp;&nbsp;Revisión de Bodega</strong></h4>
            </div>
            <form action="{{ asset('archivos_acepta/revision_bodega') }}" method="post" class="horizontal-form" id="form" autocomplete="off">
                <div class="modal-body">
                    <div class="note note-danger" id="divErrores" style="display:none;">
                        <h4 class="block" style="margin-bottom:5 px;">Debe Completar los siguientes campos: </h4>
                        <ul id="ulErrores"></ul>
                    </div>

                    <div class="form-group row" style="margin-bottom: 0px;">

                        <div class="col-xs-12 col-md-12">	
                            <h4 class="text-center headers-view">
                            <i class="fa fa-file-text"></i> Documento Acepta
                            </h4>
                            <hr style="background-color: black;height: 2px;">
                            <div class="form-group row">
                                <div class="col-xs-12">	
                                    <h4 class="headers-view">
                                        Proveedor
                                    </h4>
                                    <hr>
                                    <div class="form-group">
                                        <label class="col-sm-3 control-label font-bold">Rut Proveedor</label>
                                        <label class="col-sm-4 control-label">@if($archivoAcepta->getProveedor){{ \Rut::parse($archivoAcepta->getProveedor->rut)->format() }}@endif</label>
                                    </div>
                                </div>
                            </div>

                            <div class="form-group row">
                                <div class="col-xs-12">
                                    <label class="col-xs-3 control-label font-bold">Nombre Proveedor</label>
                                    <label class="col-xs-9 control-label">@if($archivoAcepta->getProveedor){{ $archivoAcepta->getProveedor->nombre }}@endif</label>
                                </div>
                            </div>

                            <div class="form-group row">
                                <div class="col-xs-12">	
                                    <h4 class="headers-view">
                                        Documento
                                    </h4>
                                    <hr>
                                    <div class="form-group">
                                        <label class="col-sm-3 control-label font-bold">Tipo Documento</label>
                                        <label class="col-sm-4 control-label ">{{ $archivoAcepta->getTipoDocumento->nombre }}</label>
                                        <label class="col-sm-2 control-label font-bold">N° Documento</label>
                                        <label class="col-sm-3 control-label ">{{ $archivoAcepta->folio }}</label>
                                    </div>
                                </div>
                            </div>

                            <div class="form-group row">
                                <div class="col-xs-12">
                                    <div>
                                        <label class="col-sm-3 control-label font-bold">Fecha Documento</label>
                                        <label class="col-sm-4 control-label ">{{ fecha_dmY($archivoAcepta->emision) }}</label>
                                        <label class="col-sm-2 control-label font-bold">Valor Total</label>
                                        <label class="col-sm-3 control-label ">$ {{ formatoMiles($archivoAcepta->monto_total) }}</label>
                                    </div>
                                </div>
                            </div>

                            <div class="form-group row">
                                <div class="col-xs-12">	
                                    <h4 class="headers-view">
                                        Motivo Rezagar
                                    </h4>
                                    <hr>
                                    <div class="form-group ">
                                        @foreach ($archivoAcepta->getAceptaMotivoRezagar as $aceptaMotivoRezagar)
                                            <label class="col-sm-6 control-label ">{{ $aceptaMotivoRezagar->getMotivoRezagar->nombre }}</label>
                                        @endforeach
                                        
                                    </div>
                                </div>
                            </div>

                            <div class="form-group row">
                                <div class="col-xs-12">	
                                    <h4 class="headers-view">
                                        Respuesta
                                    </h4>
                                    <hr>
                                    <div class="form-group row">
                                        <div class="radio">
                                            <label class="col-sm-4 text-center">
                                                <input type="radio" name="respuesta" id="Radios1" value="Con Recepción Total" checked>
                                                Con Recepción Total
                                            </label>
                                            
                                            <label class="col-sm-4 text-center">
                                                <input type="radio" name="respuesta" id="Radios2" value="Con Recepción Parcial">
                                                Con Recepción Parcial
                                            </label>

                                            <label class="col-sm-4 text-center">
                                                <input type="radio" name="respuesta" id="Radios2" value="Sin Recepción">
                                                Sin Recepción
                                            </label>
                                            
                                        </div>
                                        
                                    </div>

                                    <div class="form-group ">
                                        <label class="col-sm-3 control-label font-bold">Comentario</label>
                                    </div>

                                    <div class="form-group">
                                        <textarea class="form-control noresize" id="comentario" name="comentario"></textarea>
                                    </div>
                                </div>
                            </div>
                        </div> <!-- / div class col-xs-12 -->
                        
                    </div>

                </div> {{-- /modal-body --}}

            
                @csrf
                <input type="hidden" name="_id" value="{{ $archivoAcepta->id }}">
                <div class="modal-footer form-actions right">
                    
                    <button type="button" class="btn btn-default" title="Cerrar" data-dismiss="modal" aria-hidden="true">Cerrar</button>
                    <button type="submit" title="Enviar Documento" class="btn btn-success" id="botonRechazar"><i class="fas fa-eye" style="color:black;"></i> Guardar respuesta</button>
                </div> {{-- /modal-footer --}}
            </form>
          	
		</div>
	</div>
</div>

<script>
    $("#motivos").select2({
        allowClear: true,
    });

    $('#motivos').on('select2-selecting', function (e) {
        setRequiredObservacionRechazo(parseInt(e.val),'Agregar');
    });

    $('#motivos').on('select2-removing', function (e) {
        setRequiredObservacionRechazo(parseInt(e.val),'Quitar');
    });

    function setRequiredObservacionRechazo(motivoSeleccionado, opcion) {
        if (opcion == 'Agregar') {

            if (motivoSeleccionado == 4) {
                $('#observacion_rechazo').prop('required',true);
            }

        } else if (opcion == 'Quitar') {

            if (motivoSeleccionado == 4) {
                $('#observacion_rechazo').prop('required',false);
            }

        }
    }

    $(document).ready(function() {

        $("#form").validate({
            highlight: function(element) {
                $(element).closest('.form-validate').removeClass('has-success');
                $(element).closest('.form-validate').addClass('has-error');
            },
            unhighlight: function(element) {
                $(element).closest('.form-validate').removeClass('has-error');
                $(element).closest('.form-validate').addClass('has-success');
            },
            errorElement: 'span',
            errorClass: 'help-block',
            errorPlacement: function (error, element) {
                console.log(element);
                if (element.parent('.input-group').length) {
                    error.insertAfter(element.parent());
                } else {
                    error.insertAfter(element);
                }
            },
            rules: {
            },
            messages: {
            },

            //para enviar el formulario por ajax
            submitHandler: function(form) {
                esperando();

                let formData = new FormData(form);

                $.ajax({
                    url: form.action,
                    type: form.method,
                    //data: $(form).serialize(),
                    data: formData,
                    processData: false,
                    contentType: false,
                    success: function(respuesta) {
                        console.log(respuesta);

                        if ( respuesta.estado == 'error' ) {
                            toastr.error('No es posible realizar la acción'+'<br><strong>Error: '+ respuesta.mensaje +'</strong>', 'Atención', optionsToastr);
                        } else if ( respuesta.estado == 'success') {
                            
                            toastr.success(respuesta.mensaje, 'Atención', optionsToastr);
                            // Cambiar la informacion de la fila de la tabla principal
                            // quitarElementoTabla(respuesta);
                            $('#tabla_documentos_acepta').DataTable().ajax.reload();
                            $("#modalRevisionBodega").modal("hide");
                            
                        }
                        
                    }            
                }).fail( function(respuesta) {//fail ajax
                    if ( respuesta.status == 400 ) {
                        mostrarErroresValidator(respuesta);
                    } else if ( respuesta.status == 500 ) {
                        toastr.error('No es posible realizar la acción, error en el servidor', 'Atención', optionsToastr);
                    } else {
                        toastr.error('No es posible realizar la acción', 'Atención', optionsToastr);
                    }
                    
                })
                .always(function() {
                    listo();
                });//ajax
                
            }

        });

    });

    function esperando()
    {   
        $('.page-header-fixed *').css('cursor', 'wait');
        $("#botonRechazar").attr("disabled",true);
    }

    function listo()
    {
        $('.page-header-fixed *').css('cursor', '');
        $("#botonRechazar").attr("disabled",false);
    }

    function mostrarErroresValidator(respuesta)
    {
        if ( respuesta.responseJSON ) {
            //console.log(respuesta.responseJSON);
            let htmlErrores = '';
            for (let k in respuesta.responseJSON) { 
                //console.log(k, respuesta.responseJSON[k]);
                htmlErrores += '<li>' + respuesta.responseJSON[k] + '</li>';
            }

            $('#ulErrores').html(htmlErrores);
            $('#divErrores').css('display','');
            toastr.error('No es posible realizar la acción'+'<br>'+'Errores:<br> <ul>'+htmlErrores+'</ul>', 'Atención', optionsToastr);
        }
    }

    function quitarElementoTabla(respuesta)
    {
        $('#doc_' + respuesta.id).fadeOut(400, function(){
            tablaDocsAcepta.row('#doc_' + respuesta.id ).remove().draw();
        });
    }

</script>