<link href="{{ asset('sistema/css/modales.css') }}?v={{rand()}}" rel="stylesheet" type="text/css"/>
<div class="modal fade" id="modalQuitarReclamo" role="dialog" aria-labelledby="myModalLabel3" aria-hidden="true">
	<div class="modal-dialog" style="width:60%;">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" title="Cerrar" class="close" data-dismiss="modal" aria-hidden="true"></button>
				<h4 class="modal-title" ><strong><i class="fas fa-eraser fa-lg"></i>&nbsp;&nbsp;Quitar Reclamo</strong></h4>
            </div>
          
            <form action="{{ asset('archivos_acepta/quitar_reclamo') }}" method="post" class="horizontal-form" id="form" autocomplete="off">
                <div class="modal-body">

                    <div class="form-group row" style="margin-bottom: 0px;">
                        <div class="col-xs-12 col-md-12">
                            <div class="form-group row">
                                <div class="col-xs-12 col-md-12">	
                                    <h4 class="headers-view text-center">
                                    <i class="fa fa-file-text"></i> Documento Acepta
                                    </h4>
                                    <hr style="background-color: black;height: 2px;">
                                    <div class="form-group row">
                                        <div class="col-xs-12">	
                                            <h4 class="headers-view">
                                                Proveedor
                                            </h4>
                                            <hr>
                                            <div class="form-group">
                                                <label class="col-sm-3 control-label font-bold">Rut Proveedor</label>
                                                <label class="col-sm-2 control-label">@if($archivoAcepta->getProveedor){{ \Rut::parse($archivoAcepta->getProveedor->rut)->format() }}@endif</label>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="form-group row">
                                        <div class="col-xs-12">
                                            <label class="col-xs-3 control-label font-bold">Nombre Proveedor</label>
                                            <label class="col-xs-9 control-label">@if($archivoAcepta->getProveedor){{ $archivoAcepta->getProveedor->nombre }}@endif</label>
                                        </div>
                                    </div>

                                    <div class="form-group row">
                                        <div class="col-xs-12">	
                                            <h4 class="headers-view">
                                                Documento
                                            </h4>
                                            <hr>
                                            <div class="form-group">
                                                <label class="col-sm-2 control-label font-bold">Tipo Documento</label>
                                                <label class="col-sm-4 control-label ">{{ $archivoAcepta->getTipoDocumento->nombre }}</label>
                                                <label class="col-sm-2 control-label font-bold">N° Documento</label>
                                                <label class="col-sm-4 control-label ">{{ $archivoAcepta->folio }}</label>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="form-group row">
                                        <div class="col-xs-12">
                                            <div>
                                                <label class="col-sm-2 control-label font-bold">Fecha de Documento</label>
                                                <label class="col-sm-4 control-label ">{{ fecha_dmY($archivoAcepta->emision) }}</label>
                                                <label class="col-sm-2 control-label font-bold">Valor Total</label>
                                                <label class="col-sm-4 control-label ">$ {{ formatoMiles($archivoAcepta->monto_total) }}</label>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="form-group row">
                                        <div class="col-xs-12">
                                            <hr>
                                            <h4 class="headers-view">
                                                <i class="fas fa-exclamation-triangle" style="color: black"></i> Quitar el reclamo permitirá ingresar el documento a SIFCON
                                            </h4>
                                        </div>
                                    </div>
                                </div> <!-- / div class col-xs-6 -->

                            </div>
                        </div>
                    </div>

                </div> {{-- /modal-body --}}

            
                @csrf
                <input type="hidden" name="_id" value="{{ $archivoAcepta->id }}">
                <div class="modal-footer form-actions right">
                    
                    <button type="button" class="btn btn-default" title="Cancelar" data-dismiss="modal" aria-hidden="true">Cancelar</button>
                    <button type="submit" class="btn btn-info" id="botonSubmit"><i class="fas fa-eraser" style="color:black;" ></i> Quitar Reclamo</button>
                </div> {{-- /modal-footer --}}
            </form>
          	
		</div>
	</div>
</div>

<script type="text/javascript">

$(document).ready(function() {

    $("#form").validate({
        highlight: function(element) {
            $(element).closest('.form-validate').removeClass('has-success');
            $(element).closest('.form-validate').addClass('has-error');
        },
        unhighlight: function(element) {
            $(element).closest('.form-validate').removeClass('has-error');
            $(element).closest('.form-validate').addClass('has-success');
        },
        errorElement: 'span',
        errorClass: 'help-block',
        errorPlacement: function (error, element) {
            console.log(element);
            if (element.parent('.input-group').length) {
                error.insertAfter(element.parent());
            } else {
                error.insertAfter(element);
            }
        },
        rules: {
        },
        messages: {
        },

        //para enviar el formulario por ajax
        submitHandler: function(form) {
            esperandoSubmit();

            let formData = new FormData(form);

            $.ajax({
                url: form.action,
                type: form.method,
                //data: $(form).serialize(),
                data: formData,
                processData: false,
                contentType: false,
                success: function(respuesta) {
                    console.log(respuesta);

                    if ( respuesta.estado == 'error' ) {
                        toastr.error('No es posible realizar la acción'+'<br><strong>Error: '+ respuesta.mensaje +'</strong>', 'Atención', optionsToastr);
                    } else if ( respuesta.estado == 'success') {
                        
                        toastr.success(respuesta.mensaje, 'Atención', optionsToastr);
                        // Actualiza la tabla
                        $('#tabla_documentos_acepta').DataTable().ajax.reload();
                        $("#modalQuitarReclamo").modal("hide");
                        
                    }
                    
                }            
            }).fail( function(respuesta) {//fail ajax
                if ( respuesta.status == 400 ) {
                    mostrarErroresValidator(respuesta);
                } else if ( respuesta.status == 500 ) {
                    toastr.error('No es posible realizar la acción, error en el servidor', 'Atención', optionsToastr);
                } else {
                    toastr.error('No es posible realizar la acción', 'Atención', optionsToastr);
                }
                
            })
            .always(function() {
                listoSubmit();
            });//ajax
            
        }

    });

});

function esperandoSubmit()
{   
    $('.page-header-fixed *').css('cursor', 'wait');
    $("#botonSubmit").attr("disabled",true);
}

function listoSubmit()
{
    $('.page-header-fixed *').css('cursor', '');
    $("#botonSubmit").attr("disabled",false);
}

</script>