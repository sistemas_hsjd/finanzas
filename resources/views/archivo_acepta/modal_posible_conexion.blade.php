<link href="{{ asset('sistema/css/modales.css') }} " rel="stylesheet" type="text/css"/>
<div class="modal fade" id="modalPosibleConexion" role="dialog" aria-labelledby="myModalLabel3" aria-hidden="true">
	<div class="modal-dialog" style="width:95%;">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" title="Cerrar" class="close" data-dismiss="modal" aria-hidden="true"></button>
				<h4 class="modal-title" ><strong><i class="fas fa-list"></i> <i class="fas fa-link"></i> <i class="fas fa-clipboard-list"></i>&nbsp;&nbsp;Posible Conexión Documentos</strong></h4>
            </div>
          
            <div class="modal-body">
                <div class="note note-danger" id="divErrores" style="display:none;">
                    <h4 class="block" style="margin-bottom:5 px;">Debe Completar los siguientes campos: </h4>
                    <ul id="ulErrores"></ul>
                </div>

                <div class="form-group row" style="margin-bottom: 0px;">
                    <div class="col-xs-12 col-md-12">
                        <div class="form-group row">
                            <div class="col-xs-6 col-md-6">	
                                <h4 class="headers-view text-center">
                                   <i class="fa fa-file-text"></i> Documento Acepta
                                </h4>
                                <hr style="background-color: black;height: 2px;">
                                <div class="form-group row">
                                    <div class="col-xs-12">	
                                        <h4 class="headers-view">
                                            Proveedor
                                        </h4>
                                        <hr>
                                        <div class="form-group">
                                            <label class="col-sm-3 control-label font-bold">Rut Proveedor</label>
                                            <label class="col-sm-2 control-label">@if($archivoAcepta->getProveedor){{ \Rut::parse($archivoAcepta->getProveedor->rut)->format() }}@endif</label>
                                        </div>
                                    </div>
                                </div>

                                <div class="form-group row">
                                    <div class="col-xs-12">
                                        <label class="col-xs-3 control-label font-bold">Nombre Proveedor</label>
                                        <label class="col-xs-9 control-label">@if($archivoAcepta->getProveedor){{ $archivoAcepta->getProveedor->nombre }}@endif</label>
                                    </div>
                                </div>

                                <div class="form-group row">
                                    <div class="col-xs-12">	
                                        <h4 class="headers-view">
                                            Documento
                                        </h4>
                                        <hr>
                                        <div class="form-group">
                                            <label class="col-sm-2 control-label font-bold">Tipo Documento</label>
                                            <label class="col-sm-4 control-label ">{{ $archivoAcepta->getTipoDocumento->nombre }}</label>
                                        </div>
                                    </div>
                                </div>

                                <div class="form-group row">
                                    <div class="col-xs-12">
                                        <div>
                                            <label class="col-sm-2 control-label font-bold">N° Documento</label>
                                            <label class="col-sm-4 control-label ">{{ $archivoAcepta->folio }}</label>
                                        </div>
                                    </div>
                                </div>

                                <div class="form-group row">
                                    <div class="col-xs-12">
                                        <div>
                                            <label class="col-sm-2 control-label font-bold">N° Documento Compra</label>
                                            <label class="col-sm-4 control-label ">{{ $numOrdenCompra }}</label>
                                            <label class="col-sm-2 control-label font-bold">Fecha de Documento</label>
                                            <label class="col-sm-4 control-label ">{{ fecha_dmY($archivoAcepta->emision) }}</label>
                                        </div>
                                    </div>
                                </div>


                                <div class="form-group row">
                                    <div class="col-xs-12">
                                        <div>
                                            <label class="col-sm-2 control-label font-bold">Valor Total</label>
                                            <label class="col-sm-4 control-label ">$ {{ formatoMiles($archivoAcepta->monto_total) }}</label>
                                        </div>
                                    </div>
                                </div>

                            </div> <!-- / div class col-xs-6 -->

                            
                            <div class="col-xs-6 col-md-6">	
                                <h4 class="headers-view text-center">
                                    <i class="fas fa-clipboard-list"></i> Documento Recepción Bodega
                                </h4>
                                <hr style="background-color: black;height: 2px;">
                                <div class="form-group row">
                                    <div class="col-xs-12">
                                        <h4 class="headers-view">
                                            Contrato
                                        </h4>
                                        <hr>
                                        <div class="form-group">
                                            <label class="col-sm-3 control-label font-bold">Rut Proveedor</label>
                                            <label class="col-sm-2 control-label">@if ($wsDocumento->getWsOrdenCompra->getWsContrato){{ \Rut::parse($wsDocumento->getWsOrdenCompra->getWsContrato->rut_proveedor)->format() }}@endif</label>
                                        </div>
                                    </div>
                                </div>

                                <div class="form-group row">
                                    <div class="col-xs-12">
                                        <div>
                                            <label class="col-sm-3 control-label font-bold">Nombre Proveedor</label>
                                            <label class="col-sm-9 control-label">@if ($wsDocumento->getWsOrdenCompra->getWsContrato){{ $wsDocumento->getWsOrdenCompra->getWsContrato->nombre_proveedor }}@endif</label>
                                        </div>
                                    </div>
                                </div>
                                @if ( $wsDocumento->getWsOrdenCompra->getWsContrato )
                                    @if ( $wsDocumento->getWsOrdenCompra->getWsContrato->fecha_ingreso_contrato != null )
                                        <div class="form-group row">
                                            <div class="col-xs-12">
                                                <label class="col-sm-3 control-label font-bold">Fecha de Ingreso Contrato</label>
                                                <label class="col-sm-2 control-label">
                                                        {{ fecha_dmY($wsDocumento->getWsOrdenCompra->getWsContrato->fecha_ingreso_contrato) }}
                                                </label>
                                            </div>
                                        </div>
                                    @endif

                                    @if ( $wsDocumento->getWsOrdenCompra->getWsContrato->fecha_inicio_contrato != null || $wsDocumento->getWsOrdenCompra->getWsContrato->fecha_termino_contrato != null )
                                        <div class="form-group row">
                                            <div class="col-xs-12">
                                                @if ( $wsDocumento->getWsOrdenCompra->getWsContrato->fecha_inicio_contrato != null )
                                                    <label class="col-sm-3 control-label font-bold">Fecha Inicio Contrato</label>
                                                    <label class="col-sm-2 control-label">
                                                        {{ fecha_dmY($wsDocumento->getWsOrdenCompra->getWsContrato->fecha_inicio_contrato) }}
                                                    </label>
                                                @endif
                                                @if ( $wsDocumento->getWsOrdenCompra->getWsContrato->fecha_termino_contrato != null )
                                                    <label class="col-sm-3 control-label font-bold">Fecha Termino Contrato</label>
                                                    <label class="col-sm-2 control-label">
                                                        {{ fecha_dmY($wsDocumento->getWsOrdenCompra->getWsContrato->fecha_termino_contrato) }}
                                                    </label>
                                                @endif
                                            </div>
                                        </div>
                                    @endif

                                    @if ( $wsDocumento->getWsOrdenCompra->getWsContrato->resolucion != null || $wsDocumento->getWsOrdenCompra->getWsContrato->fecha_resolucion != null)
                                        <div class="form-group row">
                                            <div class="col-xs-12">
                                                @if ( $wsDocumento->getWsOrdenCompra->getWsContrato->resolucion != null )
                                                    <label class="col-sm-3 control-label font-bold">N° Resolución</label>
                                                    <label class="col-sm-2 control-label">{{ $wsDocumento->getWsOrdenCompra->getWsContrato->resolucion }}</label>
                                                @endif
                                                
                                                @if ( $wsDocumento->getWsOrdenCompra->getWsContrato->fecha_resolucion != null )
                                                    <label class="col-sm-3 control-label font-bold">Fecha Resolución</label>
                                                    <label class="col-sm-4 control-label">
                                                        {{ fecha_dmY($wsDocumento->getWsOrdenCompra->getWsContrato->fecha_resolucion) }}
                                                    </label>
                                                @endif
                                            </div>
                                        </div>
                                    @endif

                                    @if ( $wsDocumento->getWsOrdenCompra->getWsContrato->monto_maximo != null )
                                        <div class="form-group row">
                                            <div class="col-xs-12">
                                                <label class="col-sm-3 control-label font-bold">Monto Máximo Contratado</label>
                                                <label class="col-sm-4 control-label">
                                                    $ {{ formatoMiles($wsDocumento->getWsOrdenCompra->getWsContrato->monto_maximo) }}
                                                </label>
                                            </div>
                                        </div>
                                    @endif
                                @endif

                                <div class="form-group row">
                                    <div class="col-xs-12">
                                        <h4 class="headers-view">
                                            Documento
                                        </h4>
                                        <hr>
                                        <div class="form-group">
                                            <label class="col-sm-2 control-label font-bold">Tipo Documento</label>
                                            <label class="col-sm-4 control-label">{{ $wsDocumento->tipo_documento }}</label>
                                        </div>
                                    </div>
                                </div>

                                <div class="form-group row">
                                    <div class="col-xs-12">
                                        <label class="col-sm-2 control-label font-bold">N° Documento</label>
                                        <label class="col-sm-4 control-label">{{ $wsDocumento->documento }}</label>
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <div class="col-xs-12">
                                        <div>
                                            <label class="col-sm-2 control-label font-bold">Responsable</label>
                                            <label class="col-sm-9 control-label">{{ $wsDocumento->nombre_usuario_responsable }}</label>
                                        </div>
                                    </div>
                                </div>

                                <div class="form-group row">
                                    <div class="col-xs-12">
                                        <div>
                                            <label class="col-sm-2 control-label font-bold">Monto Neto</label>
                                            <label class="col-sm-4 control-label">$ {{ formatoMiles($wsDocumento->documento_neto) }}</label>
                                            <label class="col-sm-2 control-label font-bold">Monto IVA</label>
                                            <label class="col-sm-4 control-label">$ {{ formatoMiles($wsDocumento->documento_iva) }}</label>
                                        </div>
                                    </div>
                                </div>

                                <div class="form-group row">
                                    <div class="col-xs-12">
                                        <div>
                                            <label class="col-sm-2 control-label font-bold">Monto Descuento</label>
                                            <label class="col-sm-4 control-label">$ {{ formatoMiles($wsDocumento->documento_descuento_total) }}</label>
                                            <label class="col-sm-2 control-label font-bold">Monto Total</label>
                                            <label class="col-sm-4 control-label">$ {{ formatoMiles($wsDocumento->documento_total) }}</label>
                                        </div>
                                    </div>
                                </div>

                                <div class="form-group row">
                                    <div class="col-xs-12">
                                        <h4 class="headers-view">
                                            Orden de Compra
                                        </h4>
                                        <hr>
                                        <div class="form-group">
                                            <label class="col-sm-3 control-label font-bold">Fecha Recepción de O/C</label>
                                            <label class="col-sm-2 control-label">{{ fecha_dmY($wsDocumento->getWsOrdenCompra->fecha_recepcion_orden_compra) }}</label>
                                        </div>
                                    </div>
                                </div>

                                <div class="form-group row">
                                    <div class="col-xs-12">
                                        <div>
                                            <label class="col-sm-3 control-label font-bold">N° O/C</label>
                                            <label class="col-sm-2 control-label">{{ $wsDocumento->getWsOrdenCompra->numero_orden_compra }}</label>
                                            <label class="col-sm-3 control-label font-bold">Fecha de O/C</label>
                                            <label class="col-sm-4 control-label">{{ fecha_dmY($wsDocumento->getWsOrdenCompra->fecha_orden_compra) }}</label>
                                        </div>
                                    </div>
                                </div>

                                <div class="form-group row">
                                    <div class="col-xs-12">
                                        <div>
                                            <label class="col-sm-3 control-label font-bold">Monto Máximo O/C</label>
                                            <label class="col-sm-3 control-label">$ {{ formatoMiles($wsDocumento->getWsOrdenCompra->total_orden_compra) }}</label>
                                            @if ( $wsDocumento->getWsOrdenCompra->saldo_orden_compra != null )
                                                <label class="col-sm-2 control-label font-bold">Saldo O/C</label>
                                                <label class="col-sm-4 control-label">$ {{ formatoMiles($wsDocumento->getWsOrdenCompra->saldo_orden_compra) }}</label>
                                            @endif
                                        </div>
                                    </div>
                                </div>

                                <div class="form-group row">
                                    <div class="col-xs-12">
                                        <h4 class="headers-view">
                                            Documentos adjuntos
                                        </h4>
                                        <hr>
                                        <div class="form-group">
                                            <div class="col-xs-12 table-responsive">
                                                <table class="table table-striped table-bordered table-hover " id="tabla_info_archivo">
                                                    <thead>
                                                        <tr>
                                                            <th width="90%">Nombre del Archivo</th>
                                                            <th width="10%" class="text-center"><i class="fa fa-cog" ></i></th>
                                                        </tr>
                                                    </thead>
                                                    <tbody>
                                                        @forelse($wsDocumento->getWsArchivos as $archivo)
                                                            <tr>
                                                                <td>{{ $archivo->nombre_archivo }}</td>
                                                                <td>
                                                                    <a class="btn btn-success btn-xs" @if($archivo->cargado == 0) href="{{ asset( $archivo->ubicacion ) }}" @else href="http://{{ $archivo->ubicacion }}" @endif
                                                                    title="Ver Archivo" target="_blank" ><i class="fa fa-eye"></i></a>
                                                                </td>
                                                            </tr>
                                                        @empty

                                                        @endforelse
                                                    </tbody>
                                                </table>
                                            </div>
                                        </div>
                                    </div>
                                </div>

                                <div class="form-group row">
                                    <div class="col-xs-12">	
                                        <h4 class="headers-view">
                                            Ítem Presupuestario
                                        </h4>
                                        <hr>
                                        <div class="form-group">
                                            <div class="col-xs-12 table-responsive">
                                                <table class="table table-striped table-bordered table-hover " id="tabla_wsitem_presupuestario">
                                                    <thead>
                                                        <tr>
                                                            <th width="50%">Código</th>
                                                            <th width="20%">Cantidad</th>
                                                            <th width="30%">Monto</th>
                                                        </tr>
                                                    </thead>
                                                    <tbody>
                                                        @forelse($wsDocumento->getWsItemsPresupuestarios as $item)
                                                            <tr>
                                                                <td>{{ $item->item_presupuestario }}</td>
                                                                <td>{{ $item->cantidad_recepcionada }}</td>
                                                                <td class="text-right">$ {{ formatoMiles($item->valor_item_recepcionado) }}</td>
                                                            </tr>
                                                        @empty

                                                        @endforelse
                                                    </tbody>
                                                </table>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div> <!-- / div class col-xs-6 -->
                        </div>
                    </div>
                </div>

                
            <h4 class="headers-view">Para realizar la conexión: Se debe cargar el documento acepta como documento en el sistema.</h4>
            </div> {{-- /modal-body --}}

            <form action="{{ asset('documentos/conectar') }}" method="post" class="horizontal-form" id="form" autocomplete="off">
                <input type="hidden" name="_token" value="{{ csrf_token() }}">
                <input type="hidden" name="idArchivoAcepta" value="{{ $archivoAcepta->id }}">
                <input type="hidden" name="idWsDocumento" value="{{ $wsDocumento->id }}">
                <div class="modal-footer form-actions right">
                    
                    <button type="button" class="btn btn-default" title="Cerrar" data-dismiss="modal" aria-hidden="true">Cerrar</button>
                </div> {{-- /modal-footer --}}
            </form>
          	
		</div>
	</div>
</div>

 <script type="text/javascript">
    var tablaItems = $('#tabla_item_presupuestario').DataTable({
        "language": {
            "emptyTable": "No hay datos disponibles en la tabla",
        },
        "paging":   false,
        "ordering": false,
        "info":     false,
        "searching": false
    });
 
    var tablaArchivo = $('#tabla_info_archivo').DataTable({
         "language": {
            "emptyTable": "No hay datos disponibles en la tabla",
        },
        "paging":   false,
        "ordering": false,
        "info":     false,
        "searching": false
    });

 	$(document).ready(function(){
         $("#form").validate({
            highlight: function(element) {
                $(element).closest('.form-validate').removeClass('has-success');
                $(element).closest('.form-validate').addClass('has-error');
            },
            unhighlight: function(element) {
                $(element).closest('.form-validate').removeClass('has-error');
                $(element).closest('.form-validate').addClass('has-success');
            },
            errorElement: 'span',
            errorClass: 'help-block',
            errorPlacement: function (error, element) {
                console.log(element);
                if (element.parent('.input-group').length) {
                    error.insertAfter(element.parent());
                } else {
                    error.insertAfter(element);
                }
            },
            rules: {
            },
            messages: {
            },

            //para enviar el formulario por ajax
            submitHandler: function(form) {
                esperandoAccion();
                
                let formData = new FormData(form);
                $.ajax({
                    url: form.action,
                    type: form.method,
                    //data: $(form).serialize(),
                    data: formData,
                    processData: false,
                    contentType: false,
                    success: function(respuesta) {
                        console.log(respuesta);
                        if ( respuesta.estadoDocumento == 'error' ) {
                            toastr.error('No es posible realizar la acción'+'<br><strong>Error: '+ respuesta.mensaje_documento +'</strong>', 'Atención', optionsToastr);
                        } else if ( respuesta.estadoDocumento == 'success') {
                            toastr.success(respuesta.mensajeDocumento, 'Atención', optionsToastr);
                            // Quitar Documento de la lista
                            quitarElementoTabla(respuesta);
                            $("#modalConectar").modal("hide");
                        }
                    }            
                }).fail( function(respuesta) {//fail ajax
                    if ( respuesta.status == 400 ) {
                        mostrarErroresValidator(respuesta);
                    } else if ( respuesta.status == 500 ) {
                        toastr.error('No es posible realizar la acción, error en el servidor', 'Atención', optionsToastr);
                    } else {
                        toastr.error('No es posible realizar la acción', 'Atención', optionsToastr);
                    }
                })
                .always(function() {
                    listoAccion();
                });//ajax

            }

        });
  
 	});

     function esperandoAccion()
    {   
        $('.page-header-fixed *').css('cursor', 'wait');
        $("#botonAccion").attr("disabled",true);
    }

    function listoAccion()
    {
        $('.page-header-fixed *').css('cursor', '');
        $("#botonAccion").attr("disabled",false);
    }

    function mostrarErroresValidator(respuesta) {
        if (respuesta.responseJSON) {
            //console.log(respuesta.responseJSON);
            let htmlErrores = '';
            for (let k in respuesta.responseJSON) {
                //console.log(k, respuesta.responseJSON[k]);
                htmlErrores += '<li>' + respuesta.responseJSON[k] + '</li>';
            }

            $('#ulErrores').html(htmlErrores);
            $('#divErrores').css('display', '');
            toastr.error('No es posible realizar la acción' + '<br>' + 'Errores:<br> <ul>' + htmlErrores + '</ul>', 'Atención', optionsToastr);
        }
    }

    function quitarElementoTabla(respuesta)
    {
        $('#tr_' + respuesta.id).fadeOut(400, function(){
            tablaPrincipal.row('#tr_' + respuesta.id ).remove().draw();
        });
    }

    
 </script>