@extends('master')

@section('title', 'Repartir Carga Acepta')

@push('css-head')
<!-- BEGIN PAGE LEVEL STYLES -->
<link rel="stylesheet" type="text/css" href="{{asset('assets/plugins/select2/select2_conquer.css')}}"/>
<link rel="stylesheet" href="{{asset('assets/plugins/data-tables/DT_bootstrap.css')}}"/>
<!-- END PAGE LEVEL STYLES -->
<style>
    .label-form {
        font-weight: bold;
        color: black;
        text-align: right;
    }
</style>
@endpush

@section('content')

<!-- BEGIN CONTENT -->

<div class="page-content-wrapper">
	<div class="page-content-wrapper">
		<div class="page-content">

			<!-- BEGIN PAGE CONTENT-->
			<div class="row">
				<div class="col-md-12">
					<!-- BEGIN EXAMPLE TABLE PORTLET-->
					<div class="portlet">
						<div class="portlet-title">
							<div class="caption">
                                <i class="fas fa-random"></i> Repartir Carga de Documentos Recibidos de Acepta
							</div>
						</div>
						<div class="portlet-body">
                            <form action="{{url('archivos_acepta/filtrar/repartir_carga')}}" method="post" enctype="multipart/form-data" id="filtros-docs" class="form-horizontal">
                                <h4 class="form-section" style="color: #69aa46;"><strong>Filtros</strong>

                                <div class="btn-group pull-right">                                    
                                    <button class="btn btn-info btn-xs" title="Filtrar Listado" id="filtroListado" type="submit">
                                        <i class="fas fa-search" style="color:black;" ></i> Filtrar
                                    </button>
                                </div>

                                </h4>
                                <div class="form-group row">
                                    <label for="filtro_fecha_inicio" class="col-sm-1 control-label label-form">Fecha Inicio</label>
                                    <div class="col-sm-3">
                                        <div class="input-group">
                                            <input type="text" class="form-control fechas" id="filtro_fecha_inicio" name="filtro_fecha_inicio" @if(isset($filtroFechaInicio)) value="{{ $filtroFechaInicio }}" @else value="01/{{ date("m/Y") }}" @endif >
                                            <span class="input-group-addon" onclick="fechaDocumento('filtro_fecha_inicio');" style="cursor:pointer;"><i class="fa fa-calendar" aria-hidden="true" style="color: black;"></i></span>
                                        </div>
                                    </div>

                                    <label for="filtro_fecha_termino" class="col-sm-1 control-label label-form">Fecha Termino</label>
                                    <div class="col-sm-3">
                                        <div class="input-group">
                                            <input type="text" class="form-control fechas" id="filtro_fecha_termino" name="filtro_fecha_termino" @if(isset($filtroFechaTermino)) value="{{ $filtroFechaTermino }}"  @else value="{{ date("d/m/Y") }}" @endif>
                                            <span class="input-group-addon" onclick="fechaDocumento('filtro_fecha_termino');" style="cursor:pointer;"><i class="fa fa-calendar" aria-hidden="true" style="color: black;"></i></span>
                                        </div>
                                    </div>

                                    <label for="filtro_proveedor" class="col-sm-1 control-label label-form">Proveedor</label>
                                    <div class="col-sm-3">
                                        <select name="filtro_proveedor[]" id="filtro_proveedor" class="form-control select2_filtro" multiple style="width: 100%">
                                            @foreach ( $proveedores as $proveedor )
                                                <option value="{{ $proveedor->id }}" >{{ $proveedor->rut }} {{ $proveedor->nombre }}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                </div>

                                <div class="form-group row">
                                    <label for="filtro_tipo_documento" class="col-sm-1 control-label label-form">Tipo de Documento</label>
                                    <div class="col-sm-3">
                                        <select name="filtro_tipo_documento[]" id="filtro_tipo_documento" class="form-control select2_filtro" multiple style="width: 100%">
                                            @foreach ( $tiposDocumento as $tipoDocumento )
                                                <option value="{{ $tipoDocumento->id }}" >{{ $tipoDocumento->nombre }}</option>
                                            @endforeach
                                        </select>
                                    </div>

                                    <label for="filtro_numero_documento" class="col-sm-1 control-label label-form">N° Documento</label>
                                    <div class="col-sm-3">
                                        <input type="text" class="form-control " id="filtro_numero_documento" name="filtro_numero_documento"
                                        @if(isset($filtroNumeroDocumento)) value="{{ $filtroNumeroDocumento }}"  @endif >
                                    </div>

                                    <label for="filtro_archivos_repartir" class="col-sm-1 control-label label-form">Repartir</label>
                                    <div class="col-sm-3">
                                        <select name="filtro_archivos_repartir" id="filtro_archivos_repartir" class="form-control select2_filtro" style="width: 100%">
                                            <option value="Sin Recepción" >Sin Recepción</option>
                                            <option value="Con Recepción" >Con Recepción</option>
                                        </select>
                                    </div>

                                </div>

                                <div class="form-group row">
                                    <label for="users_ingreso" class="col-sm-1 control-label label-form">Usuarios Ingreso</label>
                                    <div class="col-sm-3">
                                        {{-- <input type="text" class="form-control " id="users_ingreso"
                                             name="users_ingreso" value="{{ $usuariosIngreso }}" readonly > --}}

                                             <select name="filtro_usuarios[]" id="filtro_usuarios" class="form-control select2_filtro" 
                                                     multiple style="width: 100%" required>
                                                @foreach ( $usuariosIngreso as $user )
                                                    <option value="{{ $user->id }}" >{{ $user->name }}</option>
                                                @endforeach
                                            </select>
                                    </div>
                                </div>
                                
                            

                            <div class="table-toolbar">
                                <div id="resumen" class="col-md-4">

                                </div>
                                <button class="btn btn-success " title="Repartir" style="display:none;" type="button"
                                    formaction="{{ url('archivos_acepta/repartir_carga/guardar') }}" id="botonRepartir">
                                    <i class="fas fa-random fa-pulso"></i> Repartir
                                </button>
                            </div>

                        </form>
                            <div class="row">
                                <div class="col-md-12">
                                    <div class="panel-body">
                                        {{-- <table class="table table-striped table-bordered table-hover table-full-width" id="tabla_documentos_acepta">
                                        </table> --}}
                                    </div>
                                </div>
                            </div>
						</div>
					</div>
					<!-- END EXAMPLE TABLE PORTLET-->
				</div>
			</div>
			<!-- END PAGE CONTENT-->
			
			
			
			
		</div> <!--/div class="page-content" -->
	</div> <!--/div class="page-content-wrapper" -->
</div><!--/div class="page-content-wrapper" -->
<!-- END CONTENT -->
<div id="modal"></div>
<div id="modal2"></div>
@endsection

@push('script-footer')
<!-- BEGIN PAGE LEVEL PLUGINS -->
<script type="text/javascript" src="{{ asset('assets/plugins/select2/select2.min.js') }} "></script>
{{--<script type="text/javascript" src="{{ asset('assets/plugins/data-tables/jquery.dataTables.min.js') }}"></script>--}}
<script type="text/javascript" src="{{ asset('datatables/jquery.dataTables.min.js') }}"></script>
<script type="text/javascript" src="{{ asset('assets/plugins/data-tables/DT_bootstrap.js') }}"></script>

<link rel="stylesheet" type="text/css" href="{{asset('assets/plugins/bootstrap-datepicker/css/datepicker.css')}}"/>

<script type="text/javascript" src="{{asset('assets/plugins/bootstrap-datepicker/js/bootstrap-datepicker.js')}}"></script>
<script type="text/javascript" src="{{asset('assets/plugins/bootstrap-datetimepicker/js/bootstrap-datetimepicker.js')}}"></script>
<!-- END PAGE LEVEL PLUGINS -->
<!-- BEGIN PAGE LEVEL SCRIPTS -->
<script src="{{ asset('assets/scripts/app.js') }} "></script>
<script src="{{ asset('assets/scripts/table-advanced.js') }} "></script>


<script type="text/javascript">

    // let tablaDocsAcepta = $('#tabla_documentos_acepta').DataTable({
    //     "processing": true,
    //     "serverSide": true,
    //     "ajax": {
    //         "url" : "{{ url('archivos_acepta/data_table_sin_recepcion') }}",
    //         "type" : "POST",
    //         "data": function(d){
    //             d.form = JSON.stringify( $("#filtros-docs").serializeArray() );
    //             // console.log('se envia:');
    //             // console.log(d);
    //         },
    //     },
    //     // Set rows IDs
    //     rowId: function(data) {
    //         // console.log('a :'+ JSON.stringify(a));
    //         // console.log(a);
    //         // console.dir(a);
    //         // console.log('a.id : '+a.DT_RowID);
    //         return 'doc_' + data.DT_RowID;
    //     },
    //     columns: [
    //         { title: "Días Para Rechazar", className: "text-center" },
    //         { title: "Tipo Documento" },
    //         { title: "Rut Proveedor", className: "text-center" },
    //         { title: "Nombre Proveedor" },
    //         { title: "¿Guías?", className: "text-center"},
    //         { title: "¿NC < 100%?", className: "text-center"},
    //         { title: "N° Documento", className: "text-right" },
    //         { title: "Fecha Documento", className: "text-center" },
    //         { title: "Total", className: "text-right" },
    //         { title: "<div class='text-center'><i class='fa fa-cog'></i></div>" }
    //     ],
    //     'rowCallback': function(row, data, index){
    //         if( data[3] == 'Si') {
    //             // $(row).find('td:eq(3)').css('color', 'teal');
    //             // $('td', row).css('background-color', '#03a9f478');
    //             $('td', row).css('background-color', 'teal');
    //             $('td', row).css('color', 'white');
    //         }
    //     },
    //     "deferRender": true,
    //     "language": {
    //         "emptyTable":     "No hay datos disponibles en la tabla",
    //         "info":           "Mostrando registros del _START_ al _END_ de un total de _TOTAL_ registros",
    //         "infoEmpty":      "No hay datos disponibles para mostrar en la tabla",
    //         "infoFiltered":   "(filtrado de un total de _MAX_ registros)",
    //         "infoPostFix":    "",
    //         "thousands":      ",",
    //         "lengthMenu":     "Mostrar _MENU_ registros",
    //         "loadingRecords": "Cargando...",
    //         "processing":     "Procesando...",
    //         "search":         "Buscar: ",
    //         "zeroRecords":    "<i class='far fa-frown fa-lg'></i> No hay datos que coincidan con la búsqueda <i class='far fa-frown fa-lg'></i>",
    //         "paginate": {
    //             "first":      "Primero",
    //             "last":       "Último",
    //             "next":       "Siguiente",
    //             "previous":   "Anterior"
    //         },
    //         "aria": {
    //             "sortAscending":  ": Activar para ordenar la columna de manera ascendente",
    //             "sortDescending": ": Activar para ordenar la columna de manera descendente"
    //         }
    //     },
    //     "lengthMenu": [
    //         [10, 15, 20, -1],
    //         [10, 15, 20, "Todos"] // change per page values here
    //     ],
    //     // set the initial value
    //     "displayLength": 10,
    //     "paging":   true,
    //     "ordering": true,
    //     "info":     true,
    //     "searching": true,
    //     "orderClasses": true,
    //     "cache": true,
    // });

    // $.fn.dataTable.ext.errMode = 'none';//quita los mensajes de error de la datatable ajax

    // $('#tabla_documentos_acepta').on( 'error.dt', function ( e, settings, techNote, message ) {
	// 	console.log( 'An error has been reported by DataTables: ', message ); //para mostrar los mensajes de error en la consola del navegador
	// } );

    // jQuery('#tabla_documentos_acepta_filter input').addClass("form-control input-large"); // modify table search input
    // jQuery('#tabla_documentos_acepta_wrapper .dataTables_length select').addClass("form-control input-small"); // modify table per page dropdown
    // jQuery('#tabla_documentos_acepta_wrapper .dataTables_length select').select2(); // initialize select2 dropdown

    // $('#tabla_documentos_acepta_column_toggler input[type="checkbox"]').change(function() {
    //     /* Get the DataTables object again - this is not a recreation, just a get of the object */
    //     var iCol = parseInt($(this).attr("data-column"));
    //     var bVis = oTable.fnSettings().aoColumns[iCol].bVisible;
    //     oTable.fnSetColumnVis(iCol, (bVis ? false : true));
    // });

    // $('#filtroListado').click(function(){
    //     $("#modalCarga").modal({backdrop: 'static', keyboard: false});
    //     $('#modalCargaImg').addClass('fa-pulso');
            
    //     $('#tabla_documentos_acepta').DataTable().ajax.reload( function ( json ) {
    //         $("#modalCarga").modal('toggle');
    //     });
    // });
    
	$(document).ready(function() {
		App.init(); // initlayout and core plugins

        $("#documentos").addClass( "active" );
        $("#documentos-a").append( '<span class="selected"></span>' );
        $("#ingreso").addClass( "active" );
        $("#acepta").addClass( "active" );
        $("#repartir-carga-acepta-li").addClass( "active" );
        $("#acepta-a").append( '<span class="selected"></span>' );
        $("#acepta ul").css( "display","block");

        $('#filtro_fecha_inicio').datepicker({
            format: 'dd/mm/yyyy',
            endDate: new Date(),
            autoclose: true,
            language: 'es'
        });

        $('#filtro_fecha_termino').datepicker({
            format: 'dd/mm/yyyy',
            //endDate: new Date(),
            //"setDate": new Date(),
            //startDate: new Date(),            
            autoclose: true,
            language: 'es'
        });

        $(".select2_filtro").select2({
            // allowClear: true,
        });

        $("#filtros-docs").validate({
            highlight: function(element) {
                $(element).closest('.form-validate').removeClass('has-success');
                $(element).closest('.form-validate').addClass('has-error');
            },
            unhighlight: function(element) {
                $(element).closest('.form-validate').removeClass('has-error');
                $(element).closest('.form-validate').addClass('has-success');
            },
            errorElement: 'span',
            errorClass: 'help-block',
            errorPlacement: function (error, element) {
                console.log(element);
                if (element.parent('.input-group').length) {
                    error.insertAfter(element.parent());
                } else {
                    error.insertAfter(element);
                }
            },
            rules: {
            },
            messages: {
            },

            //para enviar el formulario por ajax
            submitHandler: function(form) {
                // esperandoEdicion();
                $("#modalCarga").modal({backdrop: 'static', keyboard: false});
                $('#modalCargaImg').addClass('fa-pulso');

                let formData = new FormData(form);
                console.log('form');
                console.log(form);
                $.ajax({
                    url: form.action,
                    type: form.method,
                    //data: $(form).serialize(),
                    data: formData,
                    processData: false,
                    contentType: false,
                    success: function(respuesta) {
                        console.log(respuesta);
                        // return false;

                        if ( respuesta.estado == 'error' ) {
                            toastr.error('No es posible realizar la acción'+'<br><strong>Error: '+ respuesta.mensaje +'</strong>', 'Atención', optionsToastr);
                        } else if ( respuesta.estado == 'success') {

                            // toastr.success(respuesta.mensaje, 'Atención', optionsToastr);
                            $('#filtro_usuarios').select2("readonly", true);
                            $('#resumen').html(respuesta.resumen);
                            $('#botonRepartir').css('display','');
                            
                        }
                        
                    }            
                }).fail( function(respuesta) {//fail ajax
                    if ( respuesta.status == 400 ) {
                        // mostrarErroresValidator(respuesta);
                    } else if ( respuesta.status == 500 ) {
                        toastr.error('No es posible realizar la acción, error en el servidor', 'Atención', optionsToastr);
                    } else {
                        toastr.error('No es posible realizar la acción', 'Atención', optionsToastr);
                    }
                    
                })
                .always(function() {
                    // listaEdicion();
                    $("#modalCarga").modal('toggle');
                });//ajax
                
            }

        });


        $('#botonRepartir').click(function(){
            console.log('holo');

            $("#modalCarga").modal({backdrop: 'static', keyboard: false});
            $('#modalCargaImg').addClass('fa-pulso');

            let formData = new FormData( document.getElementById("filtros-docs") );
            // console.log('form');
            // console.log(formData);
            $.ajax({
                url: "{{ url('archivos_acepta/repartir_carga/guardar') }}",
                type: 'POST',
                //data: $(form).serialize(),
                data: formData,
                processData: false,
                contentType: false,
                success: function(respuesta) {
                    console.log(respuesta);
                    // return false;

                    if ( respuesta.estado == 'error' ) {
                        toastr.error('No es posible realizar la acción'+'<br><strong>Error: '+ respuesta.mensaje +'</strong>', 'Atención', optionsToastr);
                    } else if ( respuesta.estado == 'success') {

                        // toastr.success(respuesta.mensaje, 'Atención', optionsToastr);
                        $('#filtro_usuarios').select2("readonly", false);
                        $('#resumen').html(respuesta.resumen);
                        $('#botonRepartir').css('display','none');
                        
                    }
                    
                }            
            }).fail( function(respuesta) {//fail ajax
                if ( respuesta.status == 400 ) {
                    // mostrarErroresValidator(respuesta);
                } else if ( respuesta.status == 500 ) {
                    toastr.error('No es posible realizar la acción, error en el servidor', 'Atención', optionsToastr);
                } else {
                    toastr.error('No es posible realizar la acción', 'Atención', optionsToastr);
                }
                
            })
            .always(function() {
                // listaEdicion();
                $("#modalCarga").modal('toggle');
            });//ajax
        });

	});

    function fechaDocumento(id_fecha) {
        $('#' + id_fecha).datepicker("show");
    }

    // function rechazar(id) {
    //     $.get( '{{ url("archivos_acepta/modal/rechazar") }}/' + id, function( data ) {
    //         $( "#modal" ).html( data );
    //         $( "#modalRechazar" ).modal();
    //     });
    // }
    
    // function cargarArchivo(id)
    // {
    //     $.get( '{{ url("archivos_acepta/grilla/modal/carga") }}/' + id, function( data ) {
    //         $( "#modal" ).html( data );
    //         $( "#modalCargar" ).modal();
    //     });
    // }

    

</script>

@endpush