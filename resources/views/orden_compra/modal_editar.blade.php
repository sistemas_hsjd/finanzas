<link href="{{ asset('sistema/css/modales.css') }} " rel="stylesheet" type="text/css"/>
<div class="modal fade" id="modalEditar" role="dialog" aria-labelledby="myModalLabel3" aria-hidden="true">
	<div class="modal-dialog" style="width:1100px;">
		<div class="modal-content">
            <div class="modal-header">
				<button type="button" title="Cerrar" class="close" data-dismiss="modal" aria-hidden="true"></button>
				<h4 class="modal-title" >
                    <strong> <i class="fas fa-file-contract"></i> &nbsp;&nbsp;Editar Orden de Compra</strong>
                </h4>
            </div>

            <form action="{{ url('ordenes_compra/editar') }}" method="POST" class="horizontal-form" id="form" autocomplete="off">
                <div class="modal-body">
                    <div class="form-body">
                        <div class="note note-danger" id="divErrores" style="display:none;">
                            <h4 class="block" style="margin-bottom:5 px;">Errores: </h4>
                            <br>
                            <ul id="ulErrores"></ul>
                        </div>

                        {{-- <h4 class="form-section" style="color: #69aa46;margin-top: 0px;">Proveedor</h4> --}}
                        <div class="form-group row">
                            <label for="proveedor" class="col-sm-2 control-label label-form" >Proveedor <span class="span-label">*</span></label>
                            <div class="col-sm-8 form-validate">
                                <input type="text" class="form-control" id="proveedor" name="proveedor" 
                                        required placeholder="Buscar por Nombre o RUT" onchange="getProveedor(this);">
                            </div>
                        </div>

                        <div class="form-group row" id="div_seleccion_contrato" 
                                @if ( $oc->getProveedor->getContratos->count() == 0 )
                                    style="display:none;"
                                @endif >

                            <label for="contrato" class="col-sm-2 control-label label-form">Contrato </label>
                            <div class="col-sm-3 form-validate" id="setear_select_contrato">
                                <select name="contrato" id="contrato" class="form-control select2" onchange="setContrato(this);" >
                                    <option value=''>Seleccione</option>

                                    @if ( $oc->getContrato )

                                        <option value="{{ $oc->id_contrato }}" selected 
                                            id="contrato_seleccionado_{{ $oc->id_contrato }}"
                                            data-saldocontrato="{{ $oc->getContrato->saldo_contrato }}"
                                            data-montomaximooc="{{ $oc->montoMaximoOC }}"
                                            data-iniciovigenciacontrato="{{ fecha_dmY($oc->getContrato->inicio_vigencia) }}"
                                            data-finvigenciacontrato="{{ fecha_dmY($oc->getContrato->fin_vigencia) }}"
                                            >{{ $oc->getContrato->resolucion }}</option>


                                    @endif

                                    @foreach ($oc->getProveedor->getContratos as $contrato)

                                        @if ( $contrato->id != $oc->id_contrato )

                                            <option value="{{ $contrato->id }}" 
                                                id="contrato_seleccionado_{{ $contrato->id }}"
                                                data-saldocontrato="{{ $contrato->saldo_contrato }}"
                                                data-montomaximooc="{{ $contrato->monto_contrato - $contrato->getOrdenesCompra->sum('monto_oc') }}"
                                                data-iniciovigenciacontrato="{{ fecha_dmY($contrato->inicio_vigencia) }}"
                                                data-finvigenciacontrato="{{ fecha_dmY($contrato->fin_vigencia) }}"
                                            >{{ $contrato->resolucion }}</option>

                                        @endif

                                    @endforeach

                                </select>
                            </div>

                        </div>

                        <div class="form-group row">
                            <label for="numero_oc" class="col-sm-2 control-label label-form">Número OC <span class="span-label">*</span></label>
                            <div class="col-sm-3 form-validate">
                                <input type="text" class="form-control" id="numero_oc" name="numero_oc" required value="{{ $oc->numero_oc }}" >
                            </div>

                            <label for="fecha_oc" class="col-xs-2 col-sm-2 col-md-2 col-lg-2 col-md-offset-1 control-label label-form">Fecha OC <span class="span-label">*</span></label>
                            <div class="col-xs-2 col-sm-2 col-md-2 col-lg-2 form-validate">
                                <div class="input-group">
                                    <input type="text" class="form-control fechas" id="fecha_oc" name="fecha_oc" required value="{{ fecha_dmY($oc->fecha_oc) }}"  >
                                    <span class="input-group-addon" onclick="fecha('fecha_oc');" style="cursor:pointer;"><i class="fa fa-calendar" aria-hidden="true" style="color: black;"></i></span>
                                </div>
                            </div>
                        </div>

                        <div class="form-group row">
                            <label for="inicio_vigencia" class="col-xs-2 col-sm-2 col-md-2 col-lg-2 control-label label-form">Inicio Vigencia <span class="span-label">*</span></label>
                            <div class="col-xs-2 col-sm-2 col-md-2 col-lg-2 form-validate">
                                <div class="input-group" id="div_inicio_vigencia">

                                    <input type="text" class="form-control fechas" id="inicio_vigencia" name="inicio_vigencia" required value="{{ fecha_dmY($oc->inicio_vigencia) }}"  >
                                    <span class="input-group-addon" onclick="fecha('inicio_vigencia');" style="cursor:pointer;"><i class="fa fa-calendar" aria-hidden="true" style="color: black;"></i></span>

                                </div>
                            </div>

                            <label for="fin_vigencia" class="col-xs-2 col-sm-2 col-md-2 col-lg-2 col-md-offset-2 control-label label-form">Fin Vigencia <span class="span-label">*</span></label>
                            <div class="col-xs-2 col-sm-2 col-md-2 col-lg-2 form-validate">
                                <div class="input-group" id="div_fin_vigencia">

                                    <input type="text" class="form-control fechas" id="fin_vigencia" name="fin_vigencia" required value="{{ fecha_dmY($oc->fin_vigencia) }}"  >
                                    <span class="input-group-addon" onclick="fecha('fin_vigencia');" style="cursor:pointer;"><i class="fa fa-calendar" aria-hidden="true" style="color: black;"></i></span>
                                    
                                </div>
                            </div>
                        </div>

                        <div class="form-group row">

                            <label for="monto_oc" class="col-xs-2 col-sm-2 col-md-2 col-lg-2 control-label label-form">Monto OC <span class="span-label">*</span></label>
                            <div class="col-xs-3 col-sm-3 col-md-3 col-lg-3 form-validate">
                                <div class="input-group">
                                    <span class="input-group-addon">$</span>
                                    <input type="text" class="form-control numero_documento solo_numeros" id="monto_oc" name="monto_oc" 
                                            required onkeyup="ingresoPesos(this);" autocomplete="off" value="{{ formatoMiles($oc->monto_oc) }}">
                                </div>
                            </div>

                        </div>

                        <label for="detalle_oc" class="col-xs-2 col-sm-2 col-md-2 col-lg-2 control-label label-form">Detalle <span class="span-label">*</span></label>
                        <div class="form-group row">
                            <div class="col-sm-9 col-md-offset-1 form-validate">
                                <textarea class="form-control noresize" id="detalle_oc" name="detalle_oc" required>{{ $oc->detalle_oc }}</textarea>
                            </div>
                        </div>

                        <div class="form-group row">
                            <div class="col-xs-9 col-md-offset-1 table-responsive">
                                <h4 class="form-section" style="color: #69aa46;margin: 5px 0px 2px 0px;">Archivos en PDF</h4>

                                <table class="table table-striped table-bordered table-hover " id="tabla_info_archivos_existentes">
                                    <thead>
                                        <tr>
                                            <th width="50%">Nombre del Archivo</th>
                                            <th width="20%">Tipo</th>
                                            <th width="10%">Extensión</th>
                                            <th width="10%">Peso</th>
                                            <th width="10%"><i class="fa fa-cog"></i></th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                    @forelse ($oc->getArchivos as $archivo)
                                        <tr>
                                            <td>{{ $archivo->nombre }}</td>
                                            <td>@if( $archivo->getTipoArchivo ){{ $archivo->getTipoArchivo->nombre }}@endif</td>
                                            <td>{{ $archivo->extension }}</td>
                                            <td>{{ pesoArchivoEnMB($archivo->peso) }}</td>
                                            <td>
                                                <a class="btn btn-success btn-xs" @if($archivo->cargado == 0) href="{{ asset( $archivo->ubicacion ) }}" @else href="http://{{ $archivo->ubicacion }}" @endif
                                                title="Ver Archivo" target="_blank" ><i class="fa fa-eye"></i></a>
                                                <label>
                                                    <input type="checkbox" name="delete_list[]" value="{{$archivo->id}}"> Eliminar
                                                </label>
                                            </td>
                                        </tr>
                                    @empty

                                    @endforelse
                                    </tbody>
                                </table>
                            </div>
                        </div>

                        <div class="form-group row">
                            <div class="input-group col-sm-9 col-sm-offset-1">
                                <span class="input-group-addon" id="inputGroupFileAddon01"><strong>Seleccione Documento</strong></span>
                                <input type="file" class="form-control" accept=".pdf" name="archivo" id="archivo" aria-describedby="inputGroupFileAddon01" onchange="validarArchivo();">
                            </div>
                        </div>

                        <div class="form-group row">
                            <div id="div_archivo" class="col-xs-9 col-sm-offset-1 table-responsive" style="display: none;">
                                <table class="table table-striped table-bordered table-hover " id="tabla_info_archivo">
                                    <thead>
                                        <tr>
                                            <th width="40%">Nombre</th>
                                            <th width="20%">Peso</th>
                                            <th width="40%">Tipo</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                    </tbody>
                                </table>
                            </div>
                        </div>

                        <div class="form-group row">
                            <div class="col-xs-9 col-md-offset-1 table-responsive">
                                <h4 class="form-section" style="color: #69aa46;margin: 5px 0px 2px 0px;">Documentos</h4>

                                <table class="table table-striped table-bordered table-hover " id="tabla_documentos">
                                    <thead>
                                        <tr>
                                            <th >Número</th>
                                            <th>Tipo</th>
                                            <th >Total</th>
                                            <th >Periodo</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                    @forelse ($oc->getDocumentosOrdenCompra as $documentoOc)
                                        <tr>
                                            <td>{{ $documentoOc->getDocumento->numero_documento }}</td>
                                            <td>{{ $documentoOc->getDocumento->getTipoDocumento->nombre }}</td>
                                            <td>{{ formatoMiles($documentoOc->getDocumento->total_documento) }}</td>
                                            <td>{{ fecha_mY($documentoOc->getDocumento->periodo) }}</td>
                                        </tr>
                                    @empty

                                    @endforelse
                                    </tbody>
                                </table>
                            </div>
                        </div>

                    </div>
                </div>

                <div class="modal-footer form-actions right">
                    <input type="hidden" name="_token" value="{{ csrf_token() }}">
                    <input type="hidden" name="_id" value="{{ $oc->id }}">

                    <button type="button" title="Cancelar" class="btn btn-default" data-dismiss="modal">Cancelar</button>
                    <button type="submit" title="Editar" class="btn btn-success" id="botonGuardar" >
                        <i class="far fa-save" style="color:black;"></i> Editar Orden de Compra
                    </button>
                </div>
            </form>

        </div>
    </div>
</div>

<script src="{{asset('sistema/js/ordenes_compra.js')}}?v={{rand()}}"></script>
<script>

    $(document).ready(function() {

        $(".solo_numeros").keypress(function (key) {
            // window.console.log(key.charCode); //Descomentar para ver codigo
            if (
                (key.charCode < 48 || key.charCode > 57)//números
                && (key.charCode != 0) //borrar y enter
                && (key.charCode != 44) //coma
                && (key.charCode != 46) //punto
                && (key.charCode != 45) //guion
                && (key.charCode != 43) //suma
                )
                //console.log(key.charCode);
                return false;
        });

        $('#fecha_oc').datepicker({
            format: 'dd/mm/yyyy',
            // endDate: new Date(),
            autoclose: true,
            language: 'es'
        });

        $('#inicio_vigencia').datepicker({
            format: 'dd/mm/yyyy',
            @if ( $oc->getContrato )
                startDate: '{{ fecha_dmY($oc->getContrato->inicio_vigencia) }}',
                endDate: '{{ fecha_dmY($oc->getContrato->fin_vigencia) }}',
            @endif
            autoclose: true,
            language: 'es'
        });

        $('#fin_vigencia').datepicker({
            format: 'dd/mm/yyyy',
            @if ( $oc->getContrato )
                startDate: '{{ fecha_dmY($oc->getContrato->inicio_vigencia) }}',
                endDate: '{{ fecha_dmY($oc->getContrato->fin_vigencia) }}',
            @endif
            autoclose: true,
            language: 'es'
        });

        $("#proveedor").select2({
            ajax: {
                cache: true,
                allowClear: true,
                //hidden : true,
                url : '{{ url('buscador_proveedor/') }}',
                dataType: 'json',
                delay: 250,
                data: function (params,page) {
                    //console.log("soy params : "+params);
                    //console.log("soy page : "+page);
                    var query = {
                        term: params,
                        page: params.page || 1
                    }
                    return query;
                },
                results: function (data) {
                    return {
                    results: data
                    };
                },
            },
            minimumInputLength: 2,
        
        });

        $(".select2").select2();

        $("#form").validate({
            highlight: function(element) {
                $(element).closest('.form-validate').removeClass('has-success');
                $(element).closest('.form-validate').addClass('has-error');
            },
            unhighlight: function(element) {
                $(element).closest('.form-validate').removeClass('has-error');
                $(element).closest('.form-validate').addClass('has-success');
            },
            errorElement: 'span',
            errorClass: 'help-block',
            errorPlacement: function (error, element) {
                console.log(element);
                if (element.parent('.input-group').length) {
                    error.insertAfter(element.parent());
                } else {
                    error.insertAfter(element);
                }
            },
            rules: {
            },
            messages: {
            },

            //para enviar el formulario por ajax
            submitHandler: function(form) {
                esperandoCrear();

                let formData = new FormData(form);
                
                $.ajax({
                    url: form.action,
                    type: form.method,
                    //data: $(form).serialize(),
                    data: formData,
                    processData: false,
                    contentType: false,
                    success: function(respuesta) {
                        console.log(respuesta);

                        if ( respuesta.estado == 'error' ) {
                            toastr.error('No es posible realizar la acción'+'<br><strong>Error: '+ respuesta.mensaje +'</strong>', 'Atención', optionsToastr);
                        } else if ( respuesta.estado == 'success') {
                            
                            toastr.success(respuesta.mensaje, 'Atención', optionsToastr);

                            if ( respuesta.estado_archivo == 'error' ) {
                                toastr.error('No es posible realizar la acción'+'<br><strong>Error: '+ respuesta.mensaje_archivo +'</strong>', 'Atención', optionsToastr);
                            } else if ( respuesta.estado_archivo == 'succes') {
                                toastr.success(respuesta.mensaje_archivo, 'Atención', optionsToastr);
                            }

                            // Agregar informacion a la tabla
                            $('#tabla_principal').DataTable().ajax.reload();
                            $("#modalEditar").modal("hide");

                        }
                        
                    }            
                }).fail( function(respuesta) {//fail ajax
                    if ( respuesta.status == 400 ) {
                        mostrarErroresValidator(respuesta);
                    } else if ( respuesta.status == 500 ) {
                        toastr.error('No es posible realizar la acción, error en el servidor', 'Atención', optionsToastr);
                    } else {
                        toastr.error('No es posible realizar la acción', 'Atención', optionsToastr);
                    }
                    
                })
                .always(function() {
                    listoCrear();
                });//ajax
                
            }

        });

        $('#proveedor').select2('data', { id: '{!! $oc->id_proveedor !!}', text: '{!!$oc->getProveedor->formatRut() !!} {!!$oc->getProveedor->nombre !!}' });
        $('#proveedor').select2("readonly", true);

    });

    var selectTipoArchivo = '<select name="tipo_archivo" id="tipo_archivo" class="col-xs-12" required>';
    selectTipoArchivo += '<option value="">Seleccione</option>';
    @foreach ( $tiposArchivo as $tipoArchivo)
        selectTipoArchivo += '<option value="{{ $tipoArchivo->id }}">{{ $tipoArchivo->nombre }}</option>';
    @endforeach
    selectTipoArchivo += '</select>';

    var tablaArchivo = $('#tabla_info_archivo').DataTable({
        "paging":   false,
        "ordering": false,
        "info":     false,
        "searching": false
    });

    var tablaArchivosExistentes = $('#tabla_info_archivos_existentes').DataTable({
        "language": {
            "emptyTable": "No hay datos disponibles en la tabla",
        },
        "paging":   false,
        "ordering": false,
        "info":     false,
        "searching": false
    });

    var tablaDocumentos = $('#tabla_documentos').DataTable({
        "language": {
            "emptyTable": "No hay datos disponibles en la tabla",
        },
        "paging":   false,
        "ordering": false,
        "info":     false,
        "searching": false
    });

    var montoMaximoOc = '{!! $oc->montoMaximoOC !!}';
    var urlGetContratos = '{{ url('ordenes_compra/get_contratos/') }}';
    var fechaActual = '{{ date('d/m/Y') }}';
    var montoMinimoOc = '{!! $oc->getMontoMinimo() !!}';

</script>