<link href="{{ asset('sistema/css/modales.css') }} " rel="stylesheet" type="text/css"/>
<div class="modal fade" id="modalCrear" role="dialog" aria-labelledby="myModalLabel3" aria-hidden="true">
	<div class="modal-dialog" style="width:1100px;">
		<div class="modal-content">
            <div class="modal-header">
				<button type="button" title="Cerrar" class="close" data-dismiss="modal" aria-hidden="true"></button>
				<h4 class="modal-title" >
                    <strong> <i class="fas fa-file-contract"></i> &nbsp;&nbsp;Crear Orden de Compra</strong>
                </h4>
            </div>

            <form action="{{ url('ordenes_compra') }}" method="POST" class="horizontal-form" id="form" autocomplete="off">
                <div class="modal-body">
                    <div class="form-body">
                        <div class="note note-danger" id="divErrores" style="display:none;">
                            <h4 class="block" style="margin-bottom:5 px;">Errores: </h4>
                            <br>
                            <ul id="ulErrores"></ul>
                        </div>

                        <div class="form-group row">
                            <label for="proveedor" class="col-sm-2 control-label label-form" >Proveedor <span class="span-label">*</span></label>
                            <div class="col-sm-8 form-validate">
                                <input type="text" class="form-control" id="proveedor" name="proveedor" 
                                        required placeholder="Buscar por Nombre o RUT" onchange="getProveedor(this);">
                            </div>
                        </div>

                        <div class="form-group row" id="div_seleccion_contrato" style="display: none;">

                            <label for="contrato" class="col-sm-2 control-label label-form">Contrato </label>
                            <div class="col-sm-3 form-validate" id="setear_select_contrato">
                                <select name="contrato" id="contrato" class="form-control select2" >
                                    <option value=''>Seleccione</option>
                                    
                                </select>
                            </div>

                        </div>

                        <div class="form-group row">
                            <label for="numero_oc" class="col-sm-2 control-label label-form">Número OC <span class="span-label">*</span></label>
                            <div class="col-sm-3 form-validate">
                                <input type="text" class="form-control" id="numero_oc" name="numero_oc" required >
                            </div>

                            <label for="fecha_oc" class="col-xs-2 col-sm-2 col-md-2 col-lg-2 col-md-offset-1 control-label label-form">Fecha OC <span class="span-label">*</span></label>
                            <div class="col-xs-2 col-sm-2 col-md-2 col-lg-2 form-validate">
                                <div class="input-group">
                                    <input type="text" class="form-control fechas" id="fecha_oc" name="fecha_oc" required value="{{ date('d/m/Y') }}"  >
                                    <span class="input-group-addon" onclick="fecha('fecha_oc');" style="cursor:pointer;"><i class="fa fa-calendar" aria-hidden="true" style="color: black;"></i></span>
                                </div>
                            </div>
                        </div>

                        <div class="form-group row">

                            <label for="inicio_vigencia" class="col-xs-2 col-sm-2 col-md-2 col-lg-2 control-label label-form">Inicio Vigencia <span class="span-label">*</span></label>
                            <div class="col-xs-2 col-sm-2 col-md-2 col-lg-2 form-validate">
                                <div class="input-group" id="div_inicio_vigencia">
                                    
                                    <input type="text" class="form-control fechas" id="inicio_vigencia" name="inicio_vigencia" required value="{{ date('d/m/Y') }}"  >
                                    <span class="input-group-addon" onclick="fecha('inicio_vigencia');" style="cursor:pointer;"><i class="fa fa-calendar" aria-hidden="true" style="color: black;"></i></span>

                                </div>
                            </div>

                            <label for="fin_vigencia" class="col-xs-2 col-sm-2 col-md-2 col-lg-2 col-md-offset-2 control-label label-form">Fin Vigencia <span class="span-label">*</span></label>
                            <div class="col-xs-2 col-sm-2 col-md-2 col-lg-2 form-validate">
                                <div class="input-group" id="div_fin_vigencia">

                                    <input type="text" class="form-control fechas" id="fin_vigencia" name="fin_vigencia" required value="{{ date('d/m/Y') }}"  >
                                    <span class="input-group-addon" onclick="fecha('fin_vigencia');" style="cursor:pointer;"><i class="fa fa-calendar" aria-hidden="true" style="color: black;"></i></span>

                                </div>
                            </div>

                        </div>

                        <div class="form-group row">

                            <label for="monto_oc" class="col-xs-2 col-sm-2 col-md-2 col-lg-2 control-label label-form">Monto OC <span class="span-label">*</span></label>
                            <div class="col-xs-3 col-sm-3 col-md-3 col-lg-3 form-validate">
                                <div class="input-group">
                                    <span class="input-group-addon">$</span>
                                    <input type="text" class="form-control numero_documento solo_numeros" id="monto_oc" name="monto_oc" required onkeyup="ingresoPesos(this);" autocomplete="off" >
                                </div>
                            </div>

                        </div>

                        <label for="detalle_oc" class="col-xs-2 col-sm-2 col-md-2 col-lg-2 control-label label-form">Detalle <span class="span-label">*</span></label>
                        <div class="form-group row">
                            <div class="col-sm-9 col-md-offset-1 form-validate">
                                <textarea class="form-control noresize" id="detalle_oc" name="detalle_oc" required></textarea>
                            </div>
                        </div>

                        <div class="form-group row">
                            <span style="font-size: 14px;font-weight:bold;" class="label label-success col-sm-6 col-sm-offset-1">
                                Considerar que la OC en formato PDF será obtenida desde Mercado Público.
                            </span>
                        </div>

                        <div class="form-group row">
                            <div class="input-group col-sm-9 col-sm-offset-1">
                                <span class="input-group-addon" id="inputGroupFileAddon01"><strong>Seleccione Archivo</strong></span>
                                <input type="file" class="form-control" accept=".pdf" name="archivo" id="archivo" aria-describedby="inputGroupFileAddon01" onchange="validarArchivo();">
                            </div>
                        </div>

                        <div class="form-group row">
                            <div id="div_archivo" class="col-xs-9 col-sm-offset-1 table-responsive" style="display: none;">
                                <table class="table table-striped table-bordered table-hover " id="tabla_info_archivo">
                                    <thead>
                                        <tr>
                                            <th width="40%">Nombre</th>
                                            <th width="20%">Peso</th>
                                            <th width="40%">Tipo</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                    </tbody>
                                </table>
                            </div>
                        </div>

                    </div>
                </div>

                <div class="modal-footer form-actions right">
                    <input type="hidden" name="_token" value="{{ csrf_token() }}">

                    <button type="button" title="Cancelar" class="btn btn-default" data-dismiss="modal">Cancelar</button>
                    <button type="submit" title="Crear" class="btn btn-success" id="botonGuardar" >
                        <i class="far fa-save" style="color:black;"></i> Crear Orden de Compra
                    </button>
                </div>
            </form>

        </div>
    </div>
</div>

<script src="{{asset('sistema/js/ordenes_compra.js')}}?v={{rand()}}"></script>
<script>

    $(document).ready(function() {

        $(".solo_numeros").keypress(function (key) {
            // window.console.log(key.charCode); //Descomentar para ver codigo
            if (
                (key.charCode < 48 || key.charCode > 57)//números
                && (key.charCode != 0) //borrar y enter
                && (key.charCode != 44) //coma
                && (key.charCode != 46) //punto
                && (key.charCode != 45) //guion
                && (key.charCode != 43) //suma
                )
                //console.log(key.charCode);
                return false;
        });

        $('#fecha_oc').datepicker({
            format: 'dd/mm/yyyy',
            // endDate: new Date(),
            autoclose: true,
            language: 'es'
        });

        $('#inicio_vigencia').datepicker({
            format: 'dd/mm/yyyy',
            // endDate: new Date(),
            autoclose: true,
            language: 'es'
        });

        $('#fin_vigencia').datepicker({
            format: 'dd/mm/yyyy',
            // endDate: new Date(),
            autoclose: true,
            language: 'es'
        });

        $("#proveedor").select2({
            ajax: {
                cache: true,
                allowClear: true,
                //hidden : true,
                url : '{{ url('buscador_proveedor/') }}',
                dataType: 'json',
                delay: 250,
                data: function (params,page) {
                    //console.log("soy params : "+params);
                    //console.log("soy page : "+page);
                    var query = {
                        term: params,
                        page: params.page || 1
                    }
                    return query;
                },
                results: function (data) {
                    return {
                    results: data
                    };
                },
            },
            minimumInputLength: 2,
        
        });

        $(".select2").select2();

        $("#form").validate({
            highlight: function(element) {
                $(element).closest('.form-validate').removeClass('has-success');
                $(element).closest('.form-validate').addClass('has-error');
            },
            unhighlight: function(element) {
                $(element).closest('.form-validate').removeClass('has-error');
                $(element).closest('.form-validate').addClass('has-success');
            },
            errorElement: 'span',
            errorClass: 'help-block',
            errorPlacement: function (error, element) {
                console.log(element);
                if (element.parent('.input-group').length) {
                    error.insertAfter(element.parent());
                } else {
                    error.insertAfter(element);
                }
            },
            rules: {
            },
            messages: {
            },

            //para enviar el formulario por ajax
            submitHandler: function(form) {
                esperandoCrear();

                let formData = new FormData(form);
                
                $.ajax({
                    url: form.action,
                    type: form.method,
                    //data: $(form).serialize(),
                    data: formData,
                    processData: false,
                    contentType: false,
                    success: function(respuesta) {
                        console.log(respuesta);

                        if ( respuesta.estado == 'error' ) {
                            toastr.error('No es posible realizar la acción'+'<br><strong>Error: '+ respuesta.mensaje +'</strong>', 'Atención', optionsToastr);
                        } else if ( respuesta.estado == 'success') {
                            
                            toastr.success(respuesta.mensaje, 'Atención', optionsToastr);

                            if ( respuesta.estado_archivo == 'error' ) {
                                toastr.error('No es posible realizar la acción'+'<br><strong>Error: '+ respuesta.mensaje_archivo +'</strong>', 'Atención', optionsToastr);
                            } else if ( respuesta.estado_archivo == 'succes') {
                                toastr.success(respuesta.mensaje_archivo, 'Atención', optionsToastr);
                            }

                            // Agregar informacion a la tabla
                            $('#tabla_principal').DataTable().ajax.reload();
                            $("#modalCrear").modal("hide");

                        }
                        
                    }            
                }).fail( function(respuesta) {//fail ajax
                    if ( respuesta.status == 400 ) {
                        mostrarErroresValidator(respuesta);
                    } else if ( respuesta.status == 500 ) {
                        toastr.error('No es posible realizar la acción, error en el servidor', 'Atención', optionsToastr);
                    } else {
                        toastr.error('No es posible realizar la acción', 'Atención', optionsToastr);
                    }
                    
                })
                .always(function() {
                    listoCrear();
                });//ajax
                
            }

        });

    });

    var selectTipoArchivo = '<select name="tipo_archivo" id="tipo_archivo" class="col-xs-12" required>';
    selectTipoArchivo += '<option value="">Seleccione</option>';
    @foreach ( $tiposArchivo as $tipoArchivo)
        selectTipoArchivo += '<option value="{{ $tipoArchivo->id }}">{{ $tipoArchivo->nombre }}</option>';
    @endforeach
    selectTipoArchivo += '</select>';

    var tablaArchivo = $('#tabla_info_archivo').DataTable({
        "paging":   false,
        "ordering": false,
        "info":     false,
        "searching": false
    });

    var montoMaximoOc = 'Sin Contrato';
    var urlGetContratos = '{{ url('ordenes_compra/get_contratos/') }}';
    var fechaActual = '{{ date('d/m/Y') }}';
    var montoMinimoOc = '0';

</script>