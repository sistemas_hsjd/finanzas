<link href="{{ asset('sistema/css/modales.css') }} " rel="stylesheet" type="text/css"/>
<div class="modal fade" id="modalCrear" role="dialog" aria-labelledby="myModalLabel3" aria-hidden="true">
	<div class="modal-dialog" style="width:50%;">
		<div class="modal-content">
            <div class="modal-header">
				<button type="button" title="Cerrar" class="close" data-dismiss="modal" aria-hidden="true"></button>
				<h4 class="modal-title" ><strong><i class="fas fa-cash-register"></i>&nbsp;&nbsp;Crear Nómina</strong></h4>
            </div>

            <form action="{{ url('nomina_pago/store') }}" method="POST" class="horizontal-form" id="form" autocomplete="off">
                <div class="modal-body">
                    <div class="form-body">
                        <div class="note note-danger" id="divErrores" style="display:none;">
                            <h4 class="block" style="margin-bottom:5 px;">Debe Completar los siguientes campos: </h4>
                            <ul id="ulErrores"></ul>
                        </div>

                        <div class="form-group row">

                            <div class="col-xs-3 col-xs-offset-1 form-validate">
                                <label for="fecha_pago" class="label-form">Fecha Pago <span class="span-label">*</span></label>
                                <div class="input-group">
                                    <input type="text" class="form-control fechas" id="fecha_pago" name="fecha_pago" required value="{{ date('m/Y') }}"  >
                                    <span class="input-group-addon" onclick="fecha('fecha_pago');" style="cursor:pointer;"><i class="fa fa-calendar" aria-hidden="true" style="color: black;"></i></span>
                                </div>
                            </div>

                            <div class="col-xs-3 form-validate">
                                <label for="saldo_disponible" class="label-form">Saldo Disponible <span class="span-label">*</span></label>
                                <div class="input-group">
                                    <span class="input-group-addon">$</span>
                                    <input type="text" class="form-control text-right numero_documento" id="saldo_disponible" name="saldo_disponible" required onkeyup="ingresoPesos(this);" autocomplete="off">
                                </div>
                            </div>

                            <div class="col-xs-4 form-validate">
                                <label for="tipo_documentos" class="label-form">Tipo Documentos <span class="span-label">*</span></label>
                                <select name="tipo_documentos" id="tipo_documentos" class="form-control select2" required >
                                    <option value=''>Seleccione</option>
                                    <option value='Honorarios'>Honorarios</option>
                                    <option value='Consumos Basicos'>Consumos Básicos</option>
                                    <option value='Facturas'>Facturas</option>
                                </select>
                                <span class="help-block">
                                    Documentos para llenar automáticamente la nómina
                                </span>
                            </div>

                        </div>

                    </div>
                </div>

                <div class="modal-footer form-actions right">
                    <input type="hidden" name="_token" value="{{ csrf_token() }}">

                    <button type="button" title="Cancelar" class="btn btn-default" data-dismiss="modal">Cancelar</button>
                    <button type="submit" title="Grear nomina" class="btn btn-success" id="botonGuardar"><i class="far fa-save fa-lg" style="color:black;"></i> Crear</button>
                </div>
            </form>

        </div>
    </div>
</div>


<script>
    function ingresoPesos(input) {
        $("#" + input.id).val(formatMoney(($("#" + input.id).val()).replace(/\./g, '')));
    }

    $(document).ready(function() {

        $(".numero_documento").keypress(function (key) {
            // window.console.log(key.charCode); //Descomentar para ver codigo
            if (
                (key.charCode < 48 || key.charCode > 57)//números
                && (key.charCode != 0) //borrar y enter
                && (key.charCode != 44) //coma
                && (key.charCode != 46) //punto
                && (key.charCode != 45) //guion
                && (key.charCode != 43) //suma
                )
                //console.log(key.charCode);
                return false;
        });

        $('#fecha_pago').datepicker({
            format: 'mm/yyyy',
            endDate: new Date(),
            autoclose: true,
            language: 'es',
            viewMode: 'months',
            minViewMode: 'months'
        });

        $(".select2").select2();

        $("#form").validate({
            highlight: function(element) {
                $(element).closest('.form-validate').removeClass('has-success');
                $(element).closest('.form-validate').addClass('has-error');
            },
            unhighlight: function(element) {
                $(element).closest('.form-validate').removeClass('has-error');
                $(element).closest('.form-validate').addClass('has-success');
            },
            errorElement: 'span',
            errorClass: 'help-block',
            errorPlacement: function (error, element) {
                console.log(element);
                if (element.parent('.input-group').length) {
                    error.insertAfter(element.parent());
                } else {
                    error.insertAfter(element);
                }
            },
            rules: {
            },
            messages: {
            },

            //para enviar el formulario por ajax
            submitHandler: function(form) {
                esperandoCrear();

                let formData = new FormData(form);
                
                $.ajax({
                    url: form.action,
                    type: form.method,
                    //data: $(form).serialize(),
                    data: formData,
                    processData: false,
                    contentType: false,
                    success: function(respuesta) {
                        console.log(respuesta);

                        if ( respuesta.estado == 'error' ) {
                            toastr.error('No es posible realizar la acción'+'<br><strong>Error: '+ respuesta.mensaje +'</strong>', 'Atención', optionsToastr);
                        } else if ( respuesta.estado == 'success') {
                            
                            toastr.success(respuesta.mensaje, 'Atención', optionsToastr);

                            // Agregar informacion a la tabla
                            // $('#tabla_nominas').DataTable().ajax.reload();
                            
                            window.location.replace( "{{ url('nomina_pago/edit') }}/" + respuesta.idNomina + "/1");
                            $("#modalCrear").modal("hide");

                        }
                        
                    }            
                }).fail( function(respuesta) {//fail ajax
                    if ( respuesta.status == 400 ) {
                        mostrarErroresValidator(respuesta);
                    } else if ( respuesta.status == 500 ) {
                        toastr.error('No es posible realizar la acción, error en el servidor', 'Atención', optionsToastr);
                    } else {
                        toastr.error('No es posible realizar la acción', 'Atención', optionsToastr);
                    }
                    
                })
                .always(function() {
                    listoCrear();
                });//ajax
                
            }

        });

    });

    function esperandoCrear()
    {
        $("#modalCarga").modal({backdrop: 'static', keyboard: false});
        $('.page-header-fixed *').css('cursor', 'wait');
        $("#botonGuardar").attr("disabled",true);
    }

    function listoCrear()
    {
        $('.page-header-fixed *').css('cursor', '');
        $("#botonGuardar").attr("disabled",false);
    }

    function mostrarErroresValidator(respuesta)
    {
        if ( respuesta.responseJSON ) {
            //console.log(respuesta.responseJSON);
            let htmlErrores = '';
            for (let k in respuesta.responseJSON) { 
                //console.log(k, respuesta.responseJSON[k]);
                htmlErrores += '<li>' + respuesta.responseJSON[k] + '</li>';
            }

            $('#ulErrores').html(htmlErrores);
            $('#divErrores').css('display','');
            toastr.error('No es posible realizar la acción'+'<br>'+'Errores:<br> <ul>'+htmlErrores+'</ul>', 'Atención', optionsToastr);
        }
    }

</script>