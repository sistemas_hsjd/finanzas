@extends('master')

@section('title', 'Edición nómina de pago')

@push('css-head')
<!-- BEGIN PAGE LEVEL STYLES -->
<link rel="stylesheet" type="text/css" href="{{asset('assets/plugins/select2/select2_conquer.css')}}"/>
<link rel="stylesheet" href="{{asset('assets/plugins/data-tables/DT_bootstrap.css')}}"/>
<!-- END PAGE LEVEL STYLES -->
<style type="text/css">
    .label-form {
        font-weight: bold;
        color: black;
        text-align: right;
    }

    .span-label {
        color: red;
    }

    .fechas {
        cursor: pointer;
    }

    .td_items {
        text-align: right;
        font-weight: bold;
        padding-top: 15px !important;
    }

    .td_items_dos {
        padding-top: 15px !important;
    }

    #tabla_item_presupuestario {
        border-color: white;
    }

    .noresize {
        resize: none;
        height: 100px!important;
    }

    .form-section {
        font-weight: bold !important;
        color: #69aa46;
        border-bottom: 1px solid #cac1c1 !important;
    }

    .custom-checkbox {}

    /* oculto el input */
    .custom-checkbox input[type=checkbox] {
    display: none;
    }

    /* oculto el texto */
    .custom-checkbox span {
    display: none;
    }

    /* si está activo el input */
    .custom-checkbox input[type=checkbox]:checked + span {
    display: inline-block;
    }

    /* si está inactivo el input */
    .custom-checkbox input[type=checkbox]:not(:checked) + span + span {
    display: inline-block;
    }
</style>
@endpush

@section('content')
<!-- BEGIN CONTENT -->
<div class="page-content-wrapper">
	<div class="page-content-wrapper">
		<div class="page-content">
			<!-- BEGIN STYLE CUSTOMIZER -->
			
			<!-- END BEGIN STYLE CUSTOMIZER -->
			<!-- BEGIN PAGE HEADER-->
			<!--<div class="row">
				<div class="col-md-12">
					<h3 class="page-title">
					Usuarios <small>Agregar usuarios</small>
					</h3>
				</div>
			</div>-->
			<!-- END PAGE HEADER-->
			<!-- BEGIN PAGE CONTENT-->
			<div class="row">
				<div class="col-md-12">
					<!-- BEGIN EXAMPLE TABLE PORTLET-->
					<div class="portlet">
						<div class="portlet-title">
							<div class="caption">
                                <i class="fas fa-cash-register"></i> Edición nómina de pago
							</div>
						</div>
						<div class="portlet-body form">

							<!-- BEGIN FORM-->
							<form action="{{ asset('nomina_pago/saldo_disponible') }}" method="post" class="horizontal-form" id="form-saldo-disponible" autocomplete="off">
								<div class="form-body">
                                    <div class="note note-danger" id="divErrores" style="display:none;">
                                        <h4 class="block" style="margin-bottom:5 px;">Debe Completar los siguientes campos: </h4>
                                        <ul id="ulErrores"></ul>
                                    </div>                                    

                                    <div class="form-group row" >
                                        <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12" >
                                            <h4 class="form-section" style="color: #69aa46;">
                                                @if ( $nuevo != false )
                                                    Nueva
                                                @endif
                                                Nómina de Pago: {{ $nomina->id }}
                                                <span class="pull-right">Fecha de Pago: {{ fecha_M($nomina->fecha_nomina).'/'.fecha_Y($nomina->fecha_nomina) }}</span>
                                            </h4>
                                        </div> <!-- / col-xs-12-->
                                    </div>

                                    <div class="form-group row" style="margin-bottom: 5px;">

                                        <label for="saldo_disponible" class="col-xs-2 col-sm-2 col-md-2 col-lg-2 control-label label-form">Saldo Disponible</label>
                                        <div class="col-xs-3 col-sm-3 col-md-3 col-lg-3 form-validate">
                                            <div class="input-group">
                                                <span class="input-group-addon">$</span>
                                                <input type="text" class="form-control text-right numero_documento" 
                                                        id="saldo_disponible" name="saldo_disponible" required 
                                                        value="{{ formatoMiles($nomina->saldo_disponible) }}" onkeyup="ingresoPesos(this);" autocomplete="off">
                                            </div>
                                        </div>

                                        <div class="table-toolbar col-xs-7 col-sm-7 col-md-7 col-lg-7">
                                            <div class="btn-group">
                                                <button type="button" onclick="seleccionarDocumentos();" title="Seleccionar documentos" class="btn btn-primary">Seleccionar documentos</button>
                                                <a href="{{ url('nomina_pago') }}" title="Volver" class="btn btn-warning">Volver</a>
                                                <button type="submit" title="Guardar" class="btn btn-success" name="boton" id="botonGuardar">Guardar Saldo Disponible</button>
                                                <a href="{{ asset('nomina_pago/imprimir/'.$nomina->id) }}" title="Imprimir" class="btn btn-danger" target="_blank"><i class="fa fa-print f"></i></a>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="form-group row" style="margin-bottom: 5px;">

                                        <label for="nomina_pago" class="col-xs-2 col-sm-2 col-md-2 col-lg-2 control-label label-form">Nómina Pago</label>
                                        <div class="col-xs-3 col-sm-3 col-md-3 col-lg-3 form-validate">
                                            <div class="input-group">
                                                <span class="input-group-addon">$</span>
                                                <input type="text" class="form-control text-right numero_documento" 
                                                        id="nomina_pago" name="nomina_pago" required 
                                                        value="{{ formatoMiles($nomina->sumatoria_nomina_pago) }}" onkeyup="ingresoPesos(this);" readonly autocomplete="off">
                                            </div>
                                        </div>

                                    </div>

                                    <div class="form-group row" >

                                        <label for="saldo_pago" class="col-xs-2 col-sm-2 col-md-2 col-lg-2 control-label label-form">Saldo Pago</label>
                                        <div class="col-xs-3 col-sm-3 col-md-3 col-lg-3 form-validate">
                                            <div class="input-group">
                                                <span class="input-group-addon">$</span>
                                                <input type="text" class="form-control text-right numero_documento" 
                                                        id="saldo_pago" name="saldo_pago" required 
                                                        value="{{ formatoMiles($nomina->saldo_pago) }}" onkeyup="ingresoPesos(this);" readonly autocomplete="off">
                                            </div>
                                        </div>

                                    </div>


                                    <div class="row">
                                        <div class="col-md-12">
                                            <table class="table table-striped table-bordered table-hover table-full-width" id="tabla_documentos_nomina">                                                
                                                <thead>
                                                    <tr>
                                                        <th>Días</th>
                                                        <th>Estado</th>
                                                        <th >Proveedor</th>
                                                        <th>Factoring</th>
                                                        <th >Tipo Doc.</th>
                                                        <th >N° Doc.</th>
                                                        <th >Informe</th>
                                                        <th >Fecha Doc.</th>
                                                        <th >Tot. Act.</th>
                                                        <th>Deuda</th>
                                                        <th class="text-center" >
                                                            <i class="fa fa-cog"></i>
                                                        </th>
                                                    </tr>
                                                </thead>
                                                

                                                <tfoot>
                                                    <tr>
                                                        <th colspan="9" style="text-align:right">Total:</th>
                                                        <th colspan="2" style="text-align:left"></th>
                                                    </tr>
                                                </tfoot>
                                            </table>
                                        </div>
                                    </div>

                                    <input type="hidden" name="_token" value="{{ csrf_token() }}">
                                    <input type="hidden" name="_id" value="{{ $nomina->id }}">
								</div>
								{{-- <div class="form-actions right">
								</div> --}}
							</form>
							<!-- END FORM-->
						</div>
					</div>
					<!-- END EXAMPLE TABLE PORTLET-->
				</div>
			</div>
			<!-- END PAGE CONTENT-->
		</div>
	</div>
</div>
<!-- END CONTENT -->

<div id="modal"></div>
<div id="modal2"></div>
@endsection

@push('script-footer')
<!-- BEGIN PAGE LEVEL PLUGINS -->
<script type="text/javascript" src="{{asset('assets/plugins/select2/select2.min.js')}}"></script>
{{--<script type="text/javascript" src="{{ asset('assets/plugins/data-tables/jquery.dataTables.min.js') }}"></script>--}}
<script type="text/javascript" src="{{ asset('datatables/jquery.dataTables.min.js') }}"></script>
<script type="text/javascript" src="{{asset('assets/plugins/data-tables/DT_bootstrap.js')}}"></script>
<script type="text/javascript" src="{{ asset('js/moment/moment.js') }}"></script>
<script type="text/javascript" src="{{ asset('js/moment/moment-with-locales.js') }}"></script>

<!-- END PAGE LEVEL PLUGINS -->
<!-- BEGIN PAGE LEVEL SCRIPTS -->
<link rel="stylesheet" type="text/css" href="{{asset('assets/plugins/bootstrap-datepicker/css/datepicker.css')}}"/>

<script type="text/javascript" src="{{asset('assets/plugins/bootstrap-datepicker/js/bootstrap-datepicker.js')}}"></script>
<script type="text/javascript" src="{{asset('assets/plugins/bootstrap-datetimepicker/js/bootstrap-datetimepicker.js')}}"></script>

<script>

    function ingresoPesos(input) {
        // Se ingresa solo el saldo disponible
        
        var saldoDisponible = parseInt($('#saldo_disponible').val().replace(/\./g, ''));
        var nominaPago = parseInt($('#nomina_pago').val().replace(/\./g, ''));
        // var saldoPago = parseInt($('#saldo_pago').val().replace(/\./g, ''));

        if ( saldoDisponible < nominaPago ) {
            toastr.warning('El saldo disponible no puede ser menor a la nómina de pago', 'Atención', optionsToastr);
            $("#" + input.id).val(formatMoney( nominaPago ));
        } else {
            $("#" + input.id).val(formatMoney(($("#" + input.id).val()).replace(/\./g, '')));
        }

        var saldoDisponible = parseInt($('#saldo_disponible').val().replace(/\./g, ''));
        var nominaPago = parseInt($('#nomina_pago').val().replace(/\./g, ''));
        // var saldoPago = parseInt($('#saldo_pago').val().replace(/\./g, ''));

        $("#saldo_pago").val(formatMoney( saldoDisponible - nominaPago ));

        // console.log('saldoDisponible : '+ saldoDisponible);
        // console.log('nominaPago : '+ nominaPago);
        // console.log('saldoPago : '+ saldoPago);
    }

    function seleccionarDocumentos() {

        $.ajax({
            type: "GET",
            url: '{{ url("nomina_pago/modal/seleccionar_documentos") }}/{{ $nomina->id }}',
            success: function(respuesta) {

                $( "#modal" ).html( respuesta );
                $( "#modalSeleccionDocumentos" ).modal();
            
            }
        });

    }

    function traza(id)
    {
        $.get( '{{ url("documentos/modal/traza") }}/' + id, function( data ) {
            $( "#modal2" ).html( data );
            $( "#modalTraza" ).modal();
        });
    }

    function eliminarDoc(id)
    {
        $.get( '{{ url("nomina_pago/modal/quitar_doc") }}/' + id, function( data ) {
            $( "#modal" ).html( data );
            $( "#modalQuitarDoc" ).modal();
        });
    }

       
    var tablaDocsComprobantes = $('#tabla_documentos_nomina').DataTable({ 

        "ajax": {
            "url" : "{{ url('nomina_pago/documentos_nomina') }}/{{ $nomina->id }}",
        },
        // Set rows IDs
        rowId: function(data) {
            return 'tr_' + data.DT_RowID;
        },
        "deferRender": true,
        "language": {
            "url": "{{ asset('datatables/language.json') }}"
        },
        "lengthMenu": [
            [10, 15, 20, -1],
            [10, 15, 20, "Todos"] // change per page values here
        ],
        // set the initial value
        "displayLength": 10,
        "paging":   true,
        "ordering": true,
        "info":     true,
        "searching": true,
        "orderClasses": true,
        "cache": true,
        "order": [],
        "footerCallback": function ( row, data, start, end, display ) {
            var api = this.api(), data;
            var numFormat = $.fn.dataTable.render.number( '.', '' ).display;
 
            // Remove the formatting to get integer data for summation
            var intVal = function ( i ) {
                return typeof i === 'string' ?
                    i.replace(/[\$.]/g, '')*1 :
                    typeof i === 'number' ?
                        i : 0;
            };
 
            // Total over all pages
            total = api
                .column( 9 )
                .data()
                .reduce( function (a, b) {
                    return intVal(a) + intVal(b);
                }, 0 );
 
            // Total over this page
            pageTotal = api
                .column( 9, { page: 'current'} )
                .data()
                .reduce( function (a, b) {
                    return intVal(a) + intVal(b);
                }, 0 );
 
            // Update footer
            $( api.column( 9 ).footer() ).html(
                '$ '+numFormat(pageTotal) +' ( $ '+ numFormat(total) +' )'
            );
        }

    });

    $('#tabla_documentos_nomina').on( 'error.dt', function ( e, settings, techNote, message ) {
		console.log( 'An error has been reported by DataTables: ', message );//para mostrar los mensajes de error en la consola del navegador
	} );

    jQuery('#tabla_documentos_nomina_filter input').addClass("form-control input-large"); // modify table search input
    jQuery('#tabla_documentos_nomina_wrapper .dataTables_length select').addClass("form-control input-small"); // modify table per page dropdown
    jQuery('#tabla_documentos_nomina_wrapper .dataTables_length select').select2(); // initialize select2 dropdown

    $('#tabla_documentos_nomina_column_toggler input[type="checkbox"]').change(function() {
        /* Get the DataTables object again - this is not a recreation, just a get of the object */
        var iCol = parseInt($(this).attr("data-column"));
        var bVis = oTable.fnSettings().aoColumns[iCol].bVisible;
        oTable.fnSetColumnVis(iCol, (bVis ? false : true));
    });

    function esperandoGuardar()
    {   
        $('.page-header-fixed *').css('cursor', 'wait');
        $("#botonGuardar").attr("disabled",true);
    }

    function listoGuardar()
    {
        $('.page-header-fixed *').css('cursor', '');
        $("#botonGuardar").attr("disabled",false);
    }

    function mostrarErroresValidator(respuesta)
    {
        if ( respuesta.responseJSON ) {
            //console.log(respuesta.responseJSON);
            let htmlErrores = '';
            for (let k in respuesta.responseJSON) { 
                //console.log(k, respuesta.responseJSON[k]);
                htmlErrores += '<li>' + respuesta.responseJSON[k] + '</li>';
            }

            $('#ulErrores').html(htmlErrores);
            $('#divErrores').css('display','');
            toastr.error('No es posible realizar la acción'+'<br>'+'Errores:<br> <ul>'+htmlErrores+'</ul>', 'Atención', optionsToastr);
        }
    }

    $(document).ready(function() {
        App.init();

        $(".numero_documento").keypress(function (key) {
            // window.console.log(key.charCode); //Descomentar para ver codigo
            if (
                (key.charCode < 48 || key.charCode > 57)//números
                && (key.charCode != 0) //borrar y enter
                && (key.charCode != 44) //coma
                && (key.charCode != 46) //punto
                && (key.charCode != 45) //guion
                && (key.charCode != 43) //suma
                )
                //console.log(key.charCode);
                return false;
        });

        $("#tesoreria").addClass( "active" );
        $("#tesoreria-a").append( '<span class="selected"></span>' );
        $("#nomina-pago-li").addClass( "active" );


        $("#form-saldo-disponible").validate({
            highlight: function(element) {
                $(element).closest('.form-validate').removeClass('has-success');
                $(element).closest('.form-validate').addClass('has-error');
            },
            unhighlight: function(element) {
                $(element).closest('.form-validate').removeClass('has-error');
                $(element).closest('.form-validate').addClass('has-success');
            },
            errorElement: 'span',
            errorClass: 'help-block',
            errorPlacement: function (error, element) {
                console.log(element);
                if (element.parent('.input-group').length) {
                    error.insertAfter(element.parent());
                } else {
                    error.insertAfter(element);
                }
            },
            rules: {
            },
            messages: {
            },

            //para enviar el formulario por ajax
            submitHandler: function(form) {
                esperandoGuardar();
            
                let formData = new FormData(form);
                $.ajax({
                    url: form.action,
                    type: form.method,
                    //data: $(form).serialize(),
                    data: formData,
                    processData: false,
                    contentType: false,
                    success: function(respuesta) {
                        console.log(respuesta);

                        if ( respuesta.estado == 'error' ) {
                            toastr.error('No es posible realizar la acción'+'<br><strong>Error: '+ respuesta.mensaje +'</strong>', 'Atención', optionsToastr);
                        } else if ( respuesta.estado == 'success') {
                            toastr.success(respuesta.mensaje, 'Atención', optionsToastr);
                        }
                        
                    }            
                }).fail( function(respuesta) {//fail ajax
                    if ( respuesta.status == 400 ) {
                        mostrarErroresValidator(respuesta);
                    } else if ( respuesta.status == 500 ) {
                        toastr.error('No es posible realizar la acción, error en el servidor', 'Atención', optionsToastr);
                    } else {
                        toastr.error('No es posible realizar la acción', 'Atención', optionsToastr);
                    }
                    
                })
                .always(function() {
                    listoGuardar();
                });//ajax
                
                
            }

        });

    });
        
</script>
@endpush
