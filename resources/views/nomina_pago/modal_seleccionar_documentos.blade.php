<link href="{{ asset('sistema/css/modales.css') }} " rel="stylesheet" type="text/css"/>

<div class="modal fade" id="modalSeleccionDocumentos" role="dialog" aria-labelledby="myModalLabel3" aria-hidden="true">
	<div class="modal-dialog" style="width:90%">
		<div class="modal-content">
            <div class="modal-header">
				<button type="button" title="Cerrar" class="close" data-dismiss="modal" aria-hidden="true"></button>
				<h4 class="modal-title" ><strong><i class="fas fa-cash-register"></i>&nbsp;&nbsp;Seleccionar documentos</strong></h4>
            </div>

            <form class="horizontal-form" id="form-buscador-documentos" autocomplete="off">
                <div class="modal-body">
                    <div class="form-body">

                        <div class="form-group row">
                            <label for="filtro_días_antiguedad_seleccion_documentos" 
                                    class="col-sm-2 control-label label-form">Días antigüedad</label>
                            <div class="col-sm-4 form-validate">
                                <select class="form-control select2_filtro_seleccion_documentos" 
                                        id="filtro_días_antiguedad_seleccion_documentos" name="filtro_días_antiguedad_seleccion_documentos" >
                                    @foreach ( $fechasUtilizar as $fechaUtilizar )
                                        <option value="{{ $fechaUtilizar->opcion }}" >{{ $fechaUtilizar->opcion }}</option>
                                    @endforeach
                                </select>
                            </div>
                        </div>

                        <div class="form-group row">

                            <label for="filtro_tipo_informe_seleccion_documentos" class="col-sm-2 control-label label-form">Tipo de Informe </label>
                            <div class="col-sm-4 form-validate">
                                <select name="filtro_tipo_informe_seleccion_documentos[]" id="filtro_tipo_informe_seleccion_documentos" 
                                        class="form-control select2_filtro_seleccion_documentos" multiple="multiple">
                                    @foreach ( $tiposInforme as $tipoInforme )
                                        <option value="{{ $tipoInforme->id }}" >{{ $tipoInforme->nombre }}</option>
                                    @endforeach
                                </select>
                            </div>

                            <button type="button" class="btn btn-primary" id="buscarDocumentos"  >
                                <i class="fas fa-search" ></i> Buscar documentos</button>

                        </div>
                    </div> <!-- /form-body-->
                </div> <!-- /modal-body-->
            </form>

            <form action="{{ asset('nomina_pago/agregar/documentos') }}" method="post" class="horizontal-form" id="form-seleccionar-documentos" autocomplete="off">
                <div class="modal-body">
                    <div class="note note-danger" id="divErrores" style="display:none;">
                        <h4 class="block" style="margin-bottom:5 px;">Debe Completar los siguientes campos: </h4>
                        <ul id="ulErrores"></ul>
                    </div>

                    <div class="form-body">
                        
                        <div class="form-group row" id="div_docs" >
                            <div class="col-xs-12 table-responsive">
                                <table class="table table-striped table-bordered table-hover table-full-width" id="tabla_docs_para_nomina" >
                                    <thead>
                                        <tr>
                                            {{-- <th >
                                                <div class="checkbox">
                                                    <input class="select_all" type="checkbox" id="todos">
                                                </div> 
                                            </th>
                                            <th>Días</th>
                                            <th >Proveedor</th>
                                            <th >Factoring</th>
                                            <th >Tipo Doc.</th>
                                            <th >N° Doc.</th>
                                            <th >N° Documento Compra</th>
                                            <th >Fecha Doc.</th>
                                            <th >Tot. Act.</th>
                                            <th >Deuda</th>
                                            <th class="hidden-xs text-center" >
                                                <i class="fa fa-cog"></i>
                                            </th> --}}
                                        </tr>
                                    </thead>
                                </table>
                            </div>
                        </div>

                    </div><!-- /form-body-->
                </div> <!-- /modal-body-->

                <div class="modal-footer form-actions right">
                    <input type="hidden" name="_token" value="{{ csrf_token() }}">
                    <input type="hidden" name="idNomina" value="{{ $idNomina }}" required>

                    <button type="button" title="Cerrar" class="btn btn-default" data-dismiss="modal">Cerrar</button>
                    <button type="submit" title="Guardar" class="btn btn-success" id="botonGuardar"><i class="fas fa-cash-register fa-lg" style="color:black"></i> Guardar</button>
                </div>
            </form>

        </div>
    </div>
</div>


<script type="text/javascript">

    $('#modalSeleccionDocumentos').on('hidden.bs.modal', function () {
        var tablaDocsParaNomina = null;
    });

    var tablaDocsParaNomina = $('#tabla_docs_para_nomina').DataTable({
        // "processing": true,
        // "serverSide": true,
        "ajax": {
            "url" : "{{ url('nomina_pago/documentos_para_nomina') }}",
            "type" : "POST",
            "data": function(d){
                d.form = JSON.stringify( $("#form-buscador-documentos").serializeArray() );
            },
        },
        rowId: function(data) {
            return 'tr_' + data.DT_RowID;
        },
        columns: [
            { 
              title: '<div class="checkbox"><input class="select_all" type="checkbox" id="todos" onchange="seleccionTodos();"></div> ', 
              className: "text-center",
              "orderable": false
            },
            { title: "Días" },
            { title: "Proveedor"},
            { title: "Factoring"},
            { title: "Tipo Doc." },
            { title: "N° Doc." , className: "text-center" },
            { title: "Informe" },
            { title: "Fecha Doc.", 
                "render": function(data, type) {
                    moment.locale('es');
                    return type === 'sort' ? data : moment(data).format('L');
                } 
            },
            { title: "Tot. Act.", className: "text-right",
                "render": function(data, type, row, meta) {
                    return type === 'sort' ? data : '$'+formatMoney(data) ;
                } 
            },
            { title: "Deuda", className: "text-right",
                "render": function(data, type, row, meta) {
                    return type === 'sort' ? data : '$'+formatMoney(data) ;
                } 
            },
            { title: "<div class='text-center'><i class='fa fa-cog'></i></div>", "orderable": false }
        ],
        "deferRender": true,
        "language": {
            "url": "{{ asset('datatables/language.json') }}"
        },
        "lengthMenu": [
            [10, 15, 20, -1],
            [10, 15, 20, "Todos"] // change per page values here
        ],
        // set the initial value
        "displayLength": 10,
        "paging":   true,
        "ordering": true,
        "info":     true,
        "searching": true,
        "order": []
    });

    $.fn.dataTable.ext.errMode = 'none';//quita los mensajes de error de la datatable ajax

    $('#tabla_docs_para_nomina').on( 'error.dt', function ( e, settings, techNote, message ) {
		console.log( 'An error has been reported by DataTables: ', message );//para mostrar los mensajes de error en la consola del navegador
	} );


    jQuery('#tabla_docs_para_nomina_filter input').addClass("form-control input-large"); // modify table search input
    jQuery('#tabla_docs_para_nomina_wrapper .dataTables_length select').addClass("form-control input-small"); // modify table per page dropdown
    jQuery('#tabla_docs_para_nomina_wrapper .dataTables_length select').select2(); // initialize select2 dropdown

    $('#tabla_docs_para_nomina_column_toggler input[type="checkbox"]').change(function() {
        /* Get the DataTables object again - this is not a recreation, just a get of the object */
        var iCol = parseInt($(this).attr("data-column"));
        var bVis = oTable.fnSettings().aoColumns[iCol].bVisible;
        oTable.fnSetColumnVis(iCol, (bVis ? false : true));
    });

    $('#buscarDocumentos').click(function(){
        $("#modalCarga").modal({backdrop: 'static', keyboard: false});
        $('#modalCargaImg').addClass('fa-pulso');
            
        $('#tabla_docs_para_nomina').DataTable().ajax.reload( function ( json ) {
            $("#modalCarga").modal('toggle');
        });
    });

    // $('#botonGuardar').click(function(){
    //     console.log('botonGuardar');
    //     $('#form-seleccionar-documentos').submit();
    // });

    $(document).ready(function() {

        $(".select2_filtro_seleccion_documentos").select2({
            allowClear: true,
        });

        // $('#todos').on('change', function() {
        //      if ($(this).is(':checked') ) {
        //         $( ".select_item" ).prop( "checked", true );
        //      } else {
        //         $( ".select_item" ).prop( "checked", false );
        //     }
        // });

        $("#form-seleccionar-documentos").validate({
            highlight: function(element) {
                $(element).closest('.form-validate').removeClass('has-success');
                $(element).closest('.form-validate').addClass('has-error');
            },
            unhighlight: function(element) {
                $(element).closest('.form-validate').removeClass('has-error');
                $(element).closest('.form-validate').addClass('has-success');
            },
            errorElement: 'span',
            errorClass: 'help-block',
            errorPlacement: function (error, element) {
                console.log(element);
                if (element.parent('.input-group').length) {
                    error.insertAfter(element.parent());
                } else {
                    error.insertAfter(element);
                }
            },
            rules: {
            },
            messages: {
            },

            //para enviar el formulario por ajax
            submitHandler: function(form) {
                esperandoCrear();

                let formData = new FormData(form);                
                
                $.ajax({
                    url: form.action,
                    type: form.method,
                    //data: $(form).serialize(),
                    data: formData,
                    processData: false,
                    contentType: false,
                    success: function(respuesta) {
                        console.log(respuesta);

                        if ( respuesta.estado == 'error' ) {
                            toastr.error('No es posible realizar la acción'+'<br><strong>Error: '+ respuesta.mensaje +'</strong>', 'Atención', optionsToastr);
                        } else if ( respuesta.estado == 'success') {
                            
                            toastr.success(respuesta.mensaje, 'Atención', optionsToastr);
                            $('#nomina_pago').val(respuesta.sumatoriaNominaPago);
                            $('#saldo_pago').val(respuesta.saldoPago);

                            $("#modalCarga").modal({backdrop: 'static', keyboard: false});
                            $('#modalCargaImg').addClass('fa-pulso');
                            $('#tabla_docs_para_nomina').DataTable().ajax.reload( function ( json ) {
                                $("#modalCarga").modal('toggle');
                            });

                            $('#tabla_documentos_nomina').DataTable().ajax.reload();
                            // $("#modalSeleccionDocumentos").modal("hide");

                        }
                        
                    }            
                }).fail( function(respuesta) {//fail ajax
                    if ( respuesta.status == 400 ) {
                        mostrarErroresValidator(respuesta);
                    } else if ( respuesta.status == 500 ) {
                        toastr.error('No es posible realizar la acción, error en el servidor', 'Atención', optionsToastr);
                    } else {
                        toastr.error('No es posible realizar la acción', 'Atención', optionsToastr);
                    }
                    
                })
                .always(function() {
                    listoCrear();
                });//ajax
                
            }

        });

    });

    function esperandoCrear()
    {   
        $('.page-header-fixed *').css('cursor', 'wait');
        $("#botonGuardar").attr("disabled",true);
    }

    function listoCrear()
    {
        $('.page-header-fixed *').css('cursor', '');
        $("#botonGuardar").attr("disabled",false);
    }

    function mostrarErroresValidator(respuesta)
    {
        if ( respuesta.responseJSON ) {
            //console.log(respuesta.responseJSON);
            let htmlErrores = '';
            for (let k in respuesta.responseJSON) { 
                //console.log(k, respuesta.responseJSON[k]);
                htmlErrores += '<li>' + respuesta.responseJSON[k] + '</li>';
            }

            $('#form-seleccionar-documentos #ulErrores').html(htmlErrores);
            $('#form-seleccionar-documentos #divErrores').css('display','');
            toastr.error('No es posible realizar la acción'+'<br>'+'Errores:<br> <ul>'+htmlErrores+'</ul>', 'Atención', optionsToastr);
        }
    }

    function seleccionTodos()
    {
        console.log('aca');
        if ($('#todos').is(':checked') ) {
            $( ".select_item" ).prop( "checked", true );
            } else {
            $( ".select_item" ).prop( "checked", false );
        }
    }

</script>