<link href="{{ asset('sistema/css/modales.css') }} " rel="stylesheet" type="text/css"/>
<div class="modal fade" id="modalEliminarNomina" role="dialog" aria-labelledby="myModalLabel3" aria-hidden="true">
	<div class="modal-dialog" style="width:1000px;">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" title="Cerrar" class="close" data-dismiss="modal" aria-hidden="true"></button>
				<h4 class="modal-title" ><strong><i class="fas fa-cash-register"></i>&nbsp;&nbsp;Eliminar nómina de pago</strong></h4>
            </div>
            <form action="{{ asset('nomina_pago/eliminar_nomina') }}" method="post" class="horizontal-form" id="form-eliminar-nomina">
                <div class="modal-body">
                    <div class="form-body">

                        <div class="form-group row">
                            <div class="col-xs-12">
                                <label class="col-sm-2 control-label font-bold">Nómina Pago</label>
                                <label class="col-sm-3 control-label">{{ $nomina->id }}</label>

                                <label class="col-sm-3 control-label font-bold">Fecha de Pago</label>
                                <label class="col-sm-3 control-label">{{ fecha_M($nomina->fecha_nomina).'/'.fecha_Y($nomina->fecha_nomina) }}</label>
                            </div>
                        </div>

                        {{-- <div class="form-group row">
                            <div class="col-xs-12">
                                <label class="col-sm-2 control-label font-bold">Proveedor</label>
                                <label class="col-sm-10 control-label">{{ $doc->getProveedor->rut.' '.$doc->getProveedor->nombre }}</label>
                            </div>
                        </div> --}}

                    </div>
                </div>

                <div class="modal-footer form-actions right">
                    <input type="hidden" name="_token" value="{{ csrf_token() }}">
                    <input type="hidden" name="_id" value="{{ $nomina->id }}">
                    <button type="button" title="Cancelar" class="btn btn-default" data-dismiss="modal">Cancelar</button>
                    <button type="submit" title="Eliminar Nómina" class="btn btn-danger" id="botonEliminar"><i class="fa fa-trash"></i> Eliminar</button>
                </div>
            </form>
        </div>
    </div>
</div>

<script>
    $(document).ready(function() {
        
        $("#form-eliminar-nomina").validate({
            highlight: function(element) {
                $(element).closest('.form-validate').removeClass('has-success');
                $(element).closest('.form-validate').addClass('has-error');
            },
            unhighlight: function(element) {
                $(element).closest('.form-validate').removeClass('has-error');
                $(element).closest('.form-validate').addClass('has-success');
            },
            errorElement: 'span',
            errorClass: 'help-block',
            errorPlacement: function (error, element) {
                console.log(element);
                if (element.parent('.input-group').length) {
                    error.insertAfter(element.parent());
                } else {
                    error.insertAfter(element);
                }
            },
            rules: {
            },
            messages: {
            },

            //para enviar el formulario por ajax
            submitHandler: function(form) {
                esperandoEliminar();
                
                let formData = new FormData(form);
                $.ajax({
                    url: form.action,
                    type: form.method,
                    //data: $(form).serialize(),
                    data: formData,
                    processData: false,
                    contentType: false,
                    success: function(respuesta) {
                        console.log(respuesta);
    
                        if ( respuesta.estado == 'error' ) {
                            toastr.error('No es posible realizar la acción'+'<br><strong>Error: '+ respuesta.mensaje +'</strong>', 'Atención', optionsToastr);
                        } else if ( respuesta.estado == 'success') {

                            toastr.success(respuesta.mensaje, 'Atención', optionsToastr);                            
                            $('#tabla_nominas').DataTable().ajax.reload();
                            $("#modalEliminarNomina").modal("hide");

                        }
                    }            
                }).fail( function(respuesta) {//fail ajax
                    toastr.error('No es posible realizar la acción', 'Atención', optionsToastr);
                })
                .always(function() {
                    listoEliminar();
                });//ajax

            }

        });
    });

    function esperandoEliminar()
    {   
        $('.page-header-fixed *').css('cursor', 'wait');
        $("#botonEliminar").attr("disabled",true);
    }

    function listoEliminar()
    {
        $('.page-header-fixed *').css('cursor', '');
        $("#botonEliminar").attr("disabled",false);
    }

</script>