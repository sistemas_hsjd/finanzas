<!DOCTYPE html>
<!-- 
Template Name: Conquer - Responsive Admin Dashboard Template build with Twitter Bootstrap 3.0.3
Version: 1.5.2
Author: KeenThemes
Website: http://www.keenthemes.com/
Purchase: http://themeforest.net/item/conquer-responsive-admin-dashboard-template/3716838?ref=keenthemes
-->
<!--[if IE 8]> <html lang="en" class="ie8 no-js"> <![endif]-->
<!--[if IE 9]> <html lang="en" class="ie9 no-js"> <![endif]-->
<!--[if !IE]><!-->
<html lang="es" class="no-js">
<!--<![endif]-->
<!-- BEGIN HEAD -->

<!-- Mirrored from www.keenthemes.com/preview/conquer/ by HTTrack Website Copier/3.x [XR&CO'2013], Fri, 23 May 2014 03:00:43 GMT -->
<!-- Added by HTTrack --><meta http-equiv="content-type" content="text/html;charset=UTF-8" /><!-- /Added by HTTrack -->
<head>
<meta charset="utf-8"/>
<title>@yield('title') | SIFCON </title>
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta content="width=device-width, initial-scale=1.0" name="viewport"/>
<meta content="" name="description"/>
<meta content="" name="author"/>
<meta name="MobileOptimized" content="320">

{{-- controla cache... <meta http-equiv="Expires" content="0">
<meta http-equiv="Last-Modified" content="0">
<meta http-equiv="Cache-Control" content="no-cache, mustrevalidate">
<meta http-equiv="Pragma" content="no-cache"> --}}
<!-- BEGIN GLOBAL MANDATORY STYLES -->
<link href="{{asset('assets/plugins/font-awesome/css/font-awesome.min.css')}}" rel="stylesheet" type="text/css"/>
<link href="{{asset('assets/plugins/font-awesome/css/all.css')}}" rel="stylesheet" type="text/css"/>
<link href="{{asset('assets/plugins/bootstrap/css/bootstrap.min.css')}}" rel="stylesheet" type="text/css"/>
{{-- 
<link href="{{ asset('assets/plugins/uniform/css/uniform.default.css') }}" rel="stylesheet" type="text/css"/>
--}}
<link rel="stylesheet" type="text/css" href="{{asset('assets/plugins/bootstrap-toastr/toastr.min.css')}}"/>
<!-- END GLOBAL MANDATORY STYLES -->

<!-- BEGIN PAGE LEVEL PLUGIN STYLES -->
@stack('css-head')
<style type="text/css">
 /*
    input, textarea, .mayusculas{
        text-transform:uppercase;
    }
*/
.portlet-title{
    color: white !important;
    background-color: #275360 !important;
    border-bottom: 3px solid #881b1f;
}
/*Colores institucionales
Azul : #275360;
Burdeo: #881b1f;
*/

/*Colores sistema antiguo
Azul: #2c6aa0
Amarillo: #EAAC20
*/

.portlet-title i{
    color: white !important;
}

.portlet-title .caption {
    color: white !important;
}

.navbar-brand {
    float: left;
    padding: 10px 15px;
    font-size: 23px;
    line-height: 20px;
    color: white !important;
	margin-left: 5px;
}

.navbar-brand i{
	/*color: #881b1f !important; */
	color: #37b730 !important
}

ul.page-sidebar-menu>li>ul.sub-menu li>ul.sub-menu>li.active>a i,ul.page-sidebar-menu>li>ul.sub-menu>li.active>a i{
	color: #02b111;
}
</style>
<!-- END PAGE LEVEL PLUGIN STYLES -->

<!-- BEGIN THEME STYLES -->
<link href="{{ asset('assets/css/style-conquer.css') }} " rel="stylesheet" type="text/css"/>
<link href="{{ asset('assets/css/style.css') }} " rel="stylesheet" type="text/css"/>
<link href="{{ asset('assets/css/style-responsive.css') }} " rel="stylesheet" type="text/css"/>
<link href="{{ asset('assets/css/plugins.css') }} " rel="stylesheet" type="text/css"/>
<link href="{{ asset('assets/css/pages/tasks.css') }} " rel="stylesheet" type="text/css"/>
<link href="{{ asset('assets/css/themes/default.css') }} " rel="stylesheet" type="text/css" id="style_color"/>
<link href="{{ asset('assets/css/custom.css') }} " rel="stylesheet" type="text/css"/>
<link href="{{ asset('assets/plugins/jquery-ui/jquery-ui-1.12.1.min.css') }}" rel="stylesheet" type="text/css"/>

<!-- END THEME STYLES -->

<link rel="shortcut icon" href="{{ asset('faviconA.ico') }}"/>
<style type="text/css" media="screen"> </style>

</head>
<!-- END HEAD -->
<!-- BEGIN BODY -->
<body class="page-header-fixed">
<!-- BEGIN HEADER -->
<div class="header navbar navbar-inverse navbar-fixed-top">
	<!-- BEGIN TOP NAVIGATION BAR -->
	<div class="header-inner">
		<!-- BEGIN LOGO -->
		{{-- 
		<a class="navbar-brand" href="{{ asset('/home') }}">
		<small>
			<i class="fas fa-hand-holding-usd fa-lg green"></i>
			Finanzas Digital
		</small>
		</a>
		--}}
		<!-- buscador
		<form class="search-form search-form-header" role="form" action="http://www.keenthemes.com/preview/conquer/index.html">
			<div class="input-icon right">
				<i class="fa fa-search"></i>
				<input type="text" class="form-control input-medium input-sm" name="query" placeholder="Search...">
			</div>
		</form>
		-->
		<!-- END LOGO -->
		<!-- BEGIN RESPONSIVE MENU TOGGLER -->
		<a href="javascript:;" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
		<img src="{{ asset('assets/img/menu-toggler.png') }}" alt=""/>
		</a>
		<!-- END RESPONSIVE MENU TOGGLER -->
		<!-- BEGIN TOP NAVIGATION MENU -->
		<ul class="nav navbar-nav pull-right">
			<!-- BEGIN ALERT DROPDOWN -->
			<li class="dropdown" id="header_alert_bar" style="display: none;">
				<a href="#" class="dropdown-toggle" data-toggle="dropdown" data-hover="dropdown" data-close-others="true">
				<i class="fas fa-check-circle fa-pulso" aria-hidden="true" style="color:#3cc051 !important;"></i>
				<span class="badge badge-success">
					<div id="numeroAlertas">0</div>
				</span>
				</a>
				<ul class="dropdown-menu extended notification">
					<li>
						<ul class="dropdown-menu-list scroller " style="height: 82px;" {{-- style="height: 250px;" --}}>
							<li id="numDocIntermediacion">
								<a href="#">
								<i class="fas fa-link fa-lg fa-bounce" style="color: #3cc051;"></i>
								Se han conectado 0 Documentos (Intermediación) con Recepción de Bodega.
								</a>
							</li>
						</ul>
					</li>
				</ul>
			</li>
			<!-- END ALERT DROPDOWN -->
			<!-- BEGIN NOTIFICATION DROPDOWN -->
			<li class="dropdown" id="header_notification_bar" style="display: none;">
				<a href="#" class="dropdown-toggle" data-toggle="dropdown" data-hover="dropdown" data-close-others="true">
				<i class="fa fa-warning fa-pulso" aria-hidden="true" style="color:#fcb322 !important;"></i>
				<span class="badge badge-warning">
					<div id="numeroNotificaciones">0</div>
				</span>
				</a>
				<ul class="dropdown-menu extended notification">
				{{-- 
					<li>
						<p>
							Tienes 14 nuevas notificaciones
						</p>
					</li>
				--}}
					<li>
						<ul class="dropdown-menu-list scroller " style="height: 120px;" {{-- style="height: 250px;" --}}>
							@role(['propietario','administrador','administrador-devengo','usuario-general'])
								<li id="numDocRecepcion">
									<a href="{{ asset('documentos/listado/pendiente_validacion/') }}">
									<span class="label label-sm label-icon label-warning">
										<i class="fas fa-link"></i>
									</span>
									Hay 0 Documentos con Recepción de Bodega por confirmar
									{{-- 
									<span class="time">
										Ir
									</span>
									--}}
									</a>
								</li>

								<li id="numAceptaRecepcion">
									<a href="{{ asset('archivos_acepta/listado/bodega') }}">
									<span class="label label-sm label-icon label-warning">
										<i class="fas fa-eye"></i>
									</span>
									Hay 0 Documentos Acepta con Recepción de Bodega
									{{-- 
									<span class="time">
										Ir
									</span>
									--}}
									</a>
								</li>

								<li id="numRechazoVistoBueno">
									{{-- <a href="{{ asset('documentos/listado/rechazo_visto_bueno/') }}">
									<i class="fas fa-id-card-alt fa-lg fa-bounce" style="color: #d2d200"></i>
										Hay 0 Documentos con Rechazo &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Visto Bueno
									</a> --}}
								</li>
							@endrole

							@role(['referente-tecnico'])
								<li id="numVistosBuenosPendientes">
									<a href="{{ asset('referente_tecnico/memo/index') }}">
									<span class="label label-sm label-icon label-warning">
										<i class="fas fa-eye"></i>
									</span>
									Hay 0 Vistos Buenos Pendientes
									</a>
								</li>
							@endrole
						</ul>
					</li>
				</ul>
			</li>
			<!-- END NOTIFICATION DROPDOWN -->
			<!-- linea de separacion "|"
			<li class="devider">
				 &nbsp;
			</li>
			-->
			<!-- BEGIN USER LOGIN DROPDOWN -->
			<li class="dropdown user">
				<a href="#" class="dropdown-toggle" data-toggle="dropdown" data-hover="dropdown" data-close-others="true">
				<!--
				<img alt="" src="assets/img/avatar3_small.jpg"/>
				-->
               

				<!--
				<span class="username">
					@if(Auth::check())
						{{ Auth::user()->name }}
					@else
						Sin Sesión
					@endif
				</span>
				-->
                @if (Auth::user())
					<strong style="color:white;">{{ Auth::user()->name }}@if ( Auth::user()->roles && Auth::user()->roles->first()['display_name'] != '' ) | {{ Auth::user()->roles->first()['display_name'] }}@endif</strong>
					
                @endif
				<i class="fa fa-angle-down" ></i>
				</a>
				<ul class="dropdown-menu">
					
					<li>
						<a title="Editar Perfil" onclick="editarPerfil();" style="cursor:pointer;">
							<i class="fa fa-user"></i> Mi Perfil
						</a>
					</li>
					<!--
					<li>
						<a href="page_calendar.html"><i class="fa fa-calendar"></i> My Calendar</a>
					</li>
					
					<li>
						<a href="inbox.html"><i class="fa fa-envelope"></i> My Inbox
						<span class="badge badge-danger">
							 3
						</span>
						</a>
					</li>
					<li>
						<a href="#"><i class="fa fa-tasks"></i> My Tasks
						<span class="badge badge-success">
							 7
						</span>
						</a>
					</li>
					
					<li class="divider">
					</li>
					-->
				<li>
					<!--
					<a href="login.html"><i class="fa fa-key"></i> Log Out</a>
					-->

					<a href="{{ url('/logout') }}" onclick="event.preventDefault(); document.getElementById('logout-form').submit();">
	                    <strong><i class="menu-icon fa fa-power-off" style="color:red;"></i> Salir</strong></a>

	                <form id="logout-form" action="{{ url('/logout') }}" method="POST" style="display: none;">
	                    {{ csrf_field() }}
	                </form>
				</li>
			</ul>
		</li>
		<!-- END USER LOGIN DROPDOWN -->
	</ul>
	<!-- END TOP NAVIGATION MENU -->
</div>
<!-- END TOP NAVIGATION BAR -->
</div>
<!-- END HEADER -->
<div class="clearfix">
</div>
<!-- BEGIN CONTAINER -->
<div class="page-container">
<!-- BEGIN SIDEBAR -->
<div class="page-sidebar-wrapper">
	<div class="page-sidebar-wrapper">
		<div class="page-sidebar navbar-collapse collapse">
			<!-- BEGIN SIDEBAR MENU -->
			<ul class="page-sidebar-menu">
				<li class="sidebar-toggler-wrapper" style="background-color:#fafafa;">
					<img src="{{ asset('logo-finanzas-navA.png') }}" alt="logo" class="img-responsive"/>
					<!-- BEGIN SIDEBAR TOGGLER BUTTON -->
					{{-- 
					<div class="sidebar-toggler">
						
					</div>
					--}}
					<div class="clearfix">
					</div>
					<!-- BEGIN SIDEBAR TOGGLER BUTTON -->
				</li>
				
				<li id="home" class="">
					<a id="home-a" href="{{ asset('/home') }}">
						<i class="fa fa-home"></i>
						<span class="title">
							Inicio
						</span>
						<!--
					<span class="selected">
					</span>
					-->
					</a>
				</li>
				@permission(['ver-documento'])
					<li id="buscador" class="">
						<a id="buscador-docs-a" href="{{ url('documentos/buscador/docs') }}">
							<i class="fas fa-search"></i>
							<span class="title">
								Buscador Docs.
							</span>						
						</a>
					</li>
				@endpermission
					<li id="buscador_recepcion" class="">
						<a id="buscador-recepcion-a" href="{{ url('buscador_recepcion') }}">
							<i class="fas fa-search"></i>
							<span class="title">
								Buscador Recepción
							</span>						
						</a>
					</li>

				@if ( Auth::user() && Auth::user()->getReferenteTecnico && Auth::user()->getReferenteTecnico->id == 13)
					<li id="acepta-revision-bodega-referente-li" >
						<a href="{{ asset('archivos_acepta/listado/revision_bodega') }}">
							<i class="fas fa-eye"></i>
							<span class="title">
							Revisión Bodega
							</span>
						</a>
					</li>
				@endif

				@role(['propietario', 'administrador', 'administrador-devengo', 'usuario-general', 'visita', 'usuario-ingreso'])
				@permission(['crear-documento',
				             'editar-documento','eliminar-documento','ver-documento','ver-general-documento',
							 'ver-documentos-con-recepcion-por-confirmar','match-documentos-con-recepcion',
							 'ver-documentos-con-recepcion-confirmada','desvincular-documentos-con-recepcion',
							 'ver-documentos-sin-recepcion','ver-documentos-pendiente-cuadratura','cuadrar-documento',
							 'ver-documentos-cuadrados','invalidar-documento','ver-documentos-rechazados-visto-bueno',
							 'ver-general-documento-recepcionado','ver-documentos-recepcionados','ver-archivo-documento',
						     'ver-general-recepcion-sin-documento','comentar-error-recepcion',
							 'ver-general-excel-acepta','ver-excel-acepta','ver-sin-recepcion-acepta',
							 'ver-factura-documento-acepta','carga-documento-acepta-al-sistema',
							 'ver-listado-acepta-con-recepcion','ver-posible-conexion-acepta-recepcion',
							 'rechazar-documento-acepta',
							 'problemas-documento','quitar-problemas-documento'
							])
					<li id="documentos" class="" >
						<a id="documentos-a" href="javascript:;" >
							<i class="fa fa-file-text"></i>
							<span class="title" >
								Documento
							</span>
							<span class="arrow">
							</span>
						</a>
						<ul class="sub-menu">

							<li id="ingreso" class="" >
								<a id="ingreso-a" href="javascript:;" >
									<i class="fas fa-file-import"></i>
									<span class="title" >
										Ingreso
									</span>
									<span class="arrow">
									</span>
								</a>
								<ul class="sub-menu">
									@permission(['crear-documento'])
									<li id="ingresar-li" >
										<a href="{{ asset('documentos/create/') }}">
											{{-- <i class="fa fa-files-o" ></i> --}}
											<i class="fas fa-keyboard"></i>
											Ingreso Manual
										</a>
									</li>
									@endpermission
								
									@permission(['ver-general-excel-acepta','ver-excel-acepta','ver-sin-recepcion-acepta',
										'ver-factura-documento-acepta','carga-documento-acepta-al-sistema',
										'ver-listado-acepta-con-recepcion','ver-posible-conexion-acepta-recepcion'])
									<li id="acepta" class="" >
										<a id="acepta-a" href="javascript:;" >
											<i class="fas fa-list"></i>
											<span class="title" >
												Carga Acepta
											</span>
											<span class="arrow">
											</span>
										</a>
										<ul class="sub-menu" style="display:none">
											@permission(['ver-general-excel-acepta','ver-excel-acepta'])
											<li id="carga-acepta-li" >
												<a href="{{ asset('archivos_acepta') }}">
												<i class="fas fa-file-import"></i>
													Cargar Archivo
												</a>
											</li>
											@endpermission

											<li id="repartir-carga-acepta-li" >
												<a href="{{ asset('archivos_acepta/repartir_carga') }}">
													<i class="fas fa-random"></i>
													Repartir Carga
												</a>
											</li>

											<li id="reasignar-carga-acepta-li" >
												<a href="{{ asset('archivos_acepta/reasignar_carga') }}">
													<i class="fas fa-exchange-alt"></i>
													Reasignar Carga
												</a>
											</li>

											@permission(['ver-sin-recepcion-acepta','ver-factura-documento-acepta','carga-documento-acepta-al-sistema','rechazar-documento-acepta'])
											<li id="grilla-acepta-li" >
												<a href="{{ asset('archivos_acepta/sin_recepcion') }}">
												<i class="fas fa-th-list"></i>
													Sin Recepción
												</a>
											</li>
											@endpermission

											@permission(['ver-listado-acepta-con-recepcion','ver-posible-conexion-acepta-recepcion','rechazar-documento-acepta'])
											<li id="acepta-con-recepcion-bodega-li" >
												<a href="{{ asset('archivos_acepta/listado/bodega') }}">
												<i class="fas fa-link"></i>
													Con Recepción
												</a>
											</li>
											@endpermission

											@permission(['ver-factura-documento-acepta','quitar-rechazo-documento-acepta'])
											<li id="acepta-rechazados-li" >
												<a href="{{ asset('archivos_acepta/listado/rechazados') }}">
												<i class="fas fa-ban"></i>
													Rechazados
												</a>
											</li>
											@endpermission

											{{-- @permission(['ver-factura-documento-acepta','quitar-rezago-documento-acepta']) --}}
											<li id="acepta-rezagados-li" >
												<a href="{{ asset('archivos_acepta/listado/rezagados') }}">
													<i class="fas fa-hourglass-half"></i>
													Rezagados
												</a>
											</li>
											{{-- @endpermission --}}

											<li id="acepta-revision-bodega-li" >
												<a href="{{ asset('archivos_acepta/listado/revision_bodega') }}">
													<i class="fas fa-eye"></i>
													Revisión Bodega
												</a>
											</li>
											
										</ul>
									</li>
									@endpermission

									{{-- @permission(['crear-documento']) --}}
									<li id="carga-reclamados-li" >
										<a href="{{ asset('archivos_acepta/carga_reclamados') }}">
											<i class="fas fa-exclamation-triangle"></i>
											Cargar Reclamados
										</a>
									</li>
									{{-- @endpermission --}}
								</ul>
							</li>
							

							@permission(['editar-documento','eliminar-documento','ver-documento','ver-general-documento',
										'ver-documentos-con-recepcion-por-confirmar','match-documentos-con-recepcion',
										'ver-documentos-con-recepcion-confirmada','desvincular-documentos-con-recepcion',
										'ver-documentos-sin-recepcion','ver-documentos-pendiente-cuadratura','cuadrar-documento',
										'ver-documentos-cuadrados','invalidar-documento','ver-documentos-rechazados-visto-bueno',
										'problemas-documento','quitar-problemas-documento'])
							<li id="listado" class="" >
								<a id="listado-a" href="javascript:;" >
									<i class="fas fa-list"></i>
									<span class="title" >
										Listados
									</span>
									<span class="arrow">
									</span>
								</a>
								<ul class="sub-menu" >
									@permission(['editar-documento','ver-documento','ver-general-documento'])
									<li id="general-li" >
										<a href="{{ asset('documentos') }}">
										<i class="fa fa-file-text"></i>
											General
										</a>
									</li>
									@endpermission

									@role(['propietario','administrador'])
									<li id="documentos-eliminados-li" >
										<a href="{{ asset('documentos/listado/eliminados') }}">
										<i class="fa fa-trash"></i>
											Eliminados
										</a>
									</li>
									@endrole



									{{-- @permission(['ver-documentos-con-recepcion-por-confirmar','match-documentos-con-recepcion'])
									<li id="documentos-con-recepcion-li" >
										<a href="{{ asset('documentos/listado/bodega') }}">
										<i class="fas fa-clipboard-list"></i>
											Con Recepción &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;por Confirmar
										</a>
									</li>
									@endpermission --}}

									{{-- @permission(['ver-documentos-con-recepcion-confirmada','desvincular-documentos-con-recepcion'])
									<li id="documentos-con-recepcion-listos-li" >
										<a href="{{ asset('documentos/listado/bodega/listos') }}">
										<i class="fas fa-link"></i>
											Desvincular &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Recepción &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Confirmada
										</a>
									</li>
									@endpermission --}}

									@permission(['ver-documentos-sin-recepcion'])
									<li id="documentos-sin-recepcion-li" >
										<a href="{{ asset('documentos/listado/sin_bodega/') }}">
											<i class="fas fa-ban" ></i>
											Sin Recepción
										</a>
									</li>
									@endpermission

									@permission(['ver-documentos-rechazados-visto-bueno'])
									<li id="documentos-rechazo-visto-bueno-li" >
										<a href="{{ asset('documentos/listado/rechazo_visto_bueno/') }}">
											<i class="fas fa-id-card-alt" ></i>
											Rechazo Visto &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Bueno
										</a>
									</li>
									@endpermission

									@permission(['ver-documentos-pendiente-cuadratura','cuadrar-documento','editar-documento','eliminar-documento','match-documentos-con-recepcion'])
									<li id="documentos-pendiente-validacion-li" >
										<a href="{{ asset('documentos/listado/pendiente_validacion/') }}">
											<i class="fas fa-thumbs-down"></i>
											Pendiente &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Validación
										</a>
									</li>
									@endpermission

									@permission(['ver-documentos-cuadrados','invalidar-documento','desvincular-documentos-con-recepcion'])
									<li id="documentos-validados-li" >
										<a href="{{ asset('documentos/listado/validados/') }}">
											<i class="fas fa-thumbs-up"></i>
											Validados
										</a>
									</li>
									@endpermission

									@permission(['problemas-documento','quitar-problemas-documento'])
									<li id="documentos-con-problemas-li" >
										<a href="{{ asset('documentos/listado/con_problemas/') }}">
											<i class="fas fa-exclamation-triangle"></i>
											Con problemas
										</a>
									</li>
									@endpermission
								</ul>
							</li>
							@endpermission

							@permission(['ver-general-documento-recepcionado','ver-documentos-recepcionados','ver-archivo-documento',
										'ver-general-recepcion-sin-documento','comentar-error-recepcion'])
							<li id="bodega" >
								<a id="bodega-a" href="javascript:;" >
									<i class="fas fa-clipboard-list"></i>
									<span class="title" >
										Recepción
									</span>
									<span class="arrow"></span>
								</a>
								<ul class="sub-menu">
									@permission(['ver-general-recepcion-sin-documento','comentar-error-recepcion'])
									<li id="recepcion-bodega-sin-documento-li" >
										<a href="{{ asset('recepcion_bodega/listado/sin_documento') }}">
										<i class="fas fa-ban"></i>
											Recepción Sin &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Documento
										</a>
									</li>
									@endpermission

									@permission(['ver-general-documento-recepcionado','ver-documentos-recepcionados','ver-archivo-documento'])
									<li id="recepcion-bodega-match-pendiente-li" >
										<a href="{{ asset('recepcion_bodega/match_pendiente') }}">
											<i class="fas fa-code-branch fa-rotate-270" ></i>
											Recepción Con &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Documento
										</a>
									</li>
									@endpermission

									@permission(['ver-general-documento-recepcionado','ver-documentos-recepcionados','ver-archivo-documento'])
									<li id="recepcion-bodega-match-li" >
										<a href="{{ asset('recepcion_bodega/match') }}">
										<i class="fas fa-link"></i>
											Recepción Con &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Match
										</a>
									</li>
									@endpermission

									@role(['propietario'])
									<li id="recepcion-bodega-oc-li" >
										<a href="{{ asset('recepcion_bodega/listado/oc/') }}">
										<i class="fas fa-file-contract"></i>
											Ordenes de Compra
										</a>
									</li>
									@endrole
								</ul>
							</li>
							@endpermission						
						
						</ul>
					</li>
				@endpermission
				@endrole

				@permission([
				             'listado-por-devengar','devengar-documento',
							 'listado-devengado','editar-devengo'
							])
					<li id="devengo" class="" >
						<a id="devengo-a" href="javascript:;" >
							<i class="fas fa-money-bill"></i>
							<span class="title" >
								Devengo
							</span>
							<span class="arrow">
							</span>
						</a>
						<ul class="sub-menu">

							@permission(['listado-por-devengar','devengar-documento'])
							{{-- <li id="devengar-li" >
								<a href="{{ asset('devengar') }}">
									<i class="fas fa-gavel"></i>
									Por Devengar
								</a>
							</li> --}}
							<li id="devengar-dos-li" >
								<a href="{{ asset('devengar/version_2') }}">
									<i class="fas fa-gavel fa-flip-horizontal"></i>
									Por Devengar
								</a>
							</li>
							@endpermission

							@permission(['listado-devengado','editar-devengo'])
							<li id="devengados-li" >
								<a href="{{ asset('devengar/devengados') }}">
									<i class="fas fa-money-bill-wave"></i>
									Devengado
								</a>
							</li>
							@endpermission

							@permission(['carga-sigfe'])
							<li id="devengo-upload-sigfe-li" >
								<a href="{{ asset('devengar/upload/sigfe') }}">
									<i class="fas fa-money-bill"></i>
									<i class="fas fa-file-alt"></i>
									Carga ID SIGFE
								</a>
							</li>
							@endpermission

							@permission(['carga-sigfe'])
							<li id="devengo-upload-items-sigfe-li" >
								<a href="{{ asset('devengar/upload/items_sigfe') }}">
									<i class="fas fa-money-bill"></i>
									<i class="fas fa fa-list-ol"></i>
									Carga Items SIGFE
								</a>
							</li>
							@endpermission

						</ul>
					</li>
				@endpermission


				@permission([
							 'carga-individual-factoring','carga-masiva-factoring','grilla-factoring',
							 'eliminar-factoring','grilla-no-factura','ver-factoring','editar-factoring',
							 'grilla-factoring-pagado'
							])
				<li id="tesoreria" class="" >
					<a id="tesoreria-a" href="javascript:;" >
						<i class="fas fa-piggy-bank"></i>
						<span class="title" >
							Tesoreria
						</span>
						<span class="arrow">
						</span>
					</a>
					<ul class="sub-menu">
						<li id="deuda-flotante-li" >
							<a href="{{ asset('deuda_flotante') }}">
								<i class="fa fa-file-text"></i>
								Deuda Flotante
							</a>
						</li>

						<li id="nomina-pago-li" >
							<a href="{{ asset('nomina_pago') }}">
								<i class="fas fa-cash-register"></i>
								Nóminas de Pago
							</a>
						</li>

						<li id="comprobantes-li" >
							<a href="{{ asset('comprobantes') }}">
								<i class="fas fa-stamp"></i>
								Comprobantes
							</a>
						</li>

						<li id="comprobantes-carga-sigfe-li" >
							<a href="{{ asset('comprobantes/carga/sigfe') }}">
								<i class="fas fa-stamp"></i>
								<i class="fas fa-file-alt"></i>
								Carga &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Comprobantes SIGFE
							</a>
						</li>

						<li id="comprobantes-carga-pagos-sigfe-li" >
							<a href="{{ asset('comprobantes/carga/pagos_sigfe') }}">
								<i class="fas fa-stamp"></i>
								Carga Pagos SIGFE
							</a>
						</li>

						<li id="documentos-pagados-li" >
							<a href="{{ asset('documentos_pagados') }}">
								<i class="fas fa-money-bill-wave"></i>
								Docs. Pagados
							</a>
						</li>

						@permission([
									 'carga-individual-factoring','carga-masiva-factoring','grilla-factoring',
						             'eliminar-factoring','grilla-no-factura','ver-factoring','editar-factoring',
									 'grilla-factoring-pagado'
									])
							<li id="factoring" class="" >
								<a id="factoring-a" href="javascript:;" >
									<i class="fas fa-file-export"></i>
									<span class="title" >
										Factoring
									</span>
									<span class="arrow">
									</span>
								</a>
								<ul class="sub-menu">
									@permission(['carga-individual-factoring'])
									<li id="factoring-carga-individual-li" >
										<a href="{{ asset('factoring/carga/individual') }}">
											<i class="fa fa-files-o" ></i>
											Carga Individual
										</a>
									</li>
									@endpermission

									@permission(['carga-masiva-factoring'])
									<li id="factoring-carga-masiva-li" >
										<a href="{{ asset('factoring/carga/masiva') }}">
											<i class="fas fa-file-import"></i>
											Carga Masiva
										</a>
									</li>
									@endpermission

									@permission(['grilla-factoring','eliminar-factoring'])
									<li id="factoring-grilla-li" >
										<a href="{{ asset('factoring/grilla') }}">
											<i class="fas fa-list"></i>
											Grilla Factoring
										</a>
									</li>
									@endpermission

									@permission(['grilla-no-factura','ver-factoring','editar-factoring'])
									<li id="factoring-grilla-no-factura-li" >
										<a href="{{ asset('factoring/grilla/no_factura') }}">
											<i class="fas fa-list"></i>
											Grilla No Factura
										</a>
									</li>
									@endpermission

									@permission(['grilla-factoring-pagado'])
									<li id="factoring-grilla-pagado-li" >
										<a href="{{ asset('factoring/grilla/pagado') }}">
											<i class="fas fa-list"></i>
											Grilla Factoring &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Pagado
										</a>
									</li>
									@endpermission
								</ul>
							</li>
						@endpermission
					</ul>
				</li>
				@endpermission

				<li id="gestion-contratos" class="" >
					<a id="gestion-contratos-a" href="javascript:;" >
						<i class="fas fa-file-signature"></i>
						<span class="title" >
							Gestión Contratos
						</span>
						<span class="arrow">
						</span>
					</a>
					<ul class="sub-menu">
						<li id="contratos-li" >
							<a href="{{ asset('contratos') }}">
								<i class="fas fa-id-card-alt"></i>
								Contratos
							</a>
						</li>

						<li id="ordenes-compra-li" >
							<a href="{{ asset('ordenes_compra') }}">
								<i class="fas fa-file-contract"></i>
								Ordenes de Compra
							</a>
						</li>

					</ul>
				</li>

				@role(['propietario','administrador'])
					<li id="graficos" class="" >
						<a id="graficos" href="javascript:;" >
							<i class="fas fa-chart-bar"></i>
							<span class="title" >
								Gráficos
							</span>
							<span class="arrow">
							</span>
						</a>
						<ul class="sub-menu">
							<li id="documentos-ingresados-li" >
								<a href="{{ asset('graficos/documentos_ingresados') }}">
									<i class="fa fa-file-text"></i>
									Dcts. Ingresados
								</a>
							</li>
							
							<li id="items-devengados-li" >
								<a href="{{ asset('graficos/items_devengados') }}">
									<i class="fa fa-list-ol"></i>
									Ítems Presupuestarios
								</a>
							</li>

							<li id="usuarios-acepta-li" >
								<a href="{{ asset('graficos/usuarios_acepta') }}">
									<i class="fas fa-chart-area"></i>
									Usuarios y &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Archivos Acepta
								</a>
							</li>

							<li id="usuarios-documentos-li" >
								<a href="{{ asset('graficos/usuarios_documentos') }}">
									<i class="fas fa-chart-area"></i>
									Usuarios y &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Documentos
								</a>
							</li>

						</ul>
					</li>
				@endrole			
				
				@role(['propietario', 'administrador', 'administrador-devengo', 'usuario-general', 'usuario-ingreso'])
					<li id="reportes" class="" >
						<a id="reportes-a" href="javascript:;" >
							<i class="fas fa-folder-open"></i>
							<span class="title" >
								Reportes
							</span>
							<span class="arrow">
							</span>
						</a>
						<ul class="sub-menu">
							<li id="libro-de-compras-li" >
								<a href="{{ asset('reportes/libro_de_compras') }}">
									<i class="fas fa-book"></i>
									Libro de Compras
								</a>
							</li>

							<li id="libro-de-honorarios-li" >
								<a href="{{ asset('reportes/libro_de_honorarios') }}">
									<i class="fas fa-book"></i>
									Libro de Honorarios
								</a>
							</li>

							<li id="libro-de-deudas-li" >
								<a href="{{ asset('reportes/libro_de_deudas') }}">
									<i class="fas fa-book"></i>
									Libro de Deudas
								</a>
							</li>

						</ul>
					</li>
				@endrole

				@role(['propietario', 'administrador', 'administrador-devengo', 'usuario-general', 'visita', 'usuario-ingreso'])
					<li id="nube" class="" >
						<a id="nube-a" href="javascript:;" >
							<i class="fas fa-cloud"></i>
							<span class="title" >
								Nube
							</span>
							<span class="arrow">
							</span>
						</a>
						<ul class="sub-menu">
							<li id="nube-libro-de-compras-li" >
								<a href="{{ asset('nube/libro_de_compras') }}">
									<i class="fas fa-book"></i>
									Libro de Compras
								</a>
							</li>

							<li id="nube-libro-de-honorarios-li" >
								<a href="{{ asset('nube/libro_de_honorarios') }}">
									<i class="fas fa-book"></i>
									Libro de Honorarios
								</a>
							</li>

							<li id="nube-libro-de-deudas-li" >
								<a href="{{ asset('nube/libro_de_deudas') }}">
									<i class="fas fa-book"></i>
									Libro de Deudas
								</a>
							</li>
						</ul>
					</li>
				@endrole

				@permission(['solicitar-visto-bueno','ver-memo-solicitud-visto-bueno','editar-solicitud-visto-bueno',
				             'registro-visto-bueno','editar-registro-visto-bueno'])
				<li id="referente-tecnico-opciones" class="" >
					<a id="referente-tecnico-opciones-a" href="javascript:;" >
						<i class="fas fa-id-card-alt"></i>
						<span class="title" >
							Referente Técnico
						</span>
						<span class="arrow">
						</span>
					</a>
					<ul class="sub-menu">
						@permission(['solicitar-visto-bueno'])
                        <li id="solicitar-visto-bueno-li" >
							<a href="{{ asset('referente_tecnico/vistobueno/index') }}">
								<i class="fas fa-tasks"></i>
								Solicitar Visto Bueno
							</a>
						</li>
						@endpermission

						@permission(['ver-memo-solicitud-visto-bueno','editar-solicitud-visto-bueno','registro-visto-bueno'])
						<li id="registro-visto-bueno-li" >
							<a href="{{ asset('referente_tecnico/memo/index') }}">
								<i class="fas fa-file-alt"></i>
								Registro Visto Bueno
							</a>
						</li>
						@endpermission

						@permission(['ver-memo-solicitud-visto-bueno','registro-visto-bueno','editar-registro-visto-bueno'])
						<li id="historial-memos-li" >
							<a href="{{ asset('referente_tecnico/memo/historial') }}">
								<i class="fas fa-archive"></i>
								Historial
							</a>
						</li>
						@endpermission

					</ul>
				</li>
				@endpermission

				@role(['propietario'])
					<li id="upload" class="">
						<a id="upload-a"href="javascript:;" >
							<i class="fas fa-database"></i>
							<span class="title">
								Upload Masivo
							</span>
							<span class="arrow">
							</span>
						</a>
						<ul class="sub-menu">

							<li id="contratos-upload-li">
								<a href="{{ asset('contratos/upload') }}">
									<i class="fas fa-id-card-alt"></i>
									Auxiliar Contratos
								</a>
							</li>

							<li id="libro-compra-upload-li" >
								<a href="{{ asset('libro_compra/upload/masivo') }}">
									<i class="fas fa-file-alt"></i>
									Libro de Compras
								</a>
							</li>

							<li id="proveedores-upload-li" >
								<a href="{{ asset('proveedores/upload/masivo') }}">
									<i class="fas fa-truck" ></i>
									Proveedor
								</a>
							</li>

							<li id="documentos-upload-li" >
								<a href="{{ asset('documentos/upload/masivo') }}">
									<i class="fas fa-file-alt"></i>
									Documentos
								</a>
							</li>

							<li id="devengar-upload-li" >
								<a href="{{ asset('devengar/upload/masivo') }}">
									<i class="fas fa-money-bill"></i>
									<i class="fas fa-file-alt"></i>
									Devengo
								</a>
							</li>

							<li id="items-upload-li" >
								<a href="{{ asset('item/upload/masivo') }}">
									<i class="fa fa-list-ol"></i>
									Ítem Presupuestario
								</a>
							</li>

							<li id="visto-bueno-upload-li" >
								<a href="{{ asset('referente_tecnico/vistobueno/upload/masivo') }}">
									<i class="fas fa-tasks"></i>
									Visto Bueno
								</a>
							</li>

							<li id="archivos-varios-upload-li" >
								<a href="{{ asset('archivos_acepta/varios/upload/masivo') }}">
									<i class="fas fa-folder-open"></i>
									Archivos
								</a>
							</li>

							<li id="archivos-excel-upload-li" >
								<a href="{{ asset('archivos_acepta/excels/upload/masivo') }}">
									<i class="fas fa-folder-open"></i>
									Archivos Acepta Excel
								</a>
							</li>
							
							<li id="archivos-excel-archivo-upload-li" >
								<a href="{{ asset('archivos_acepta/excels/archivos/upload/masivo') }}">
									<i class="fas fa-folder-open"></i>
									Archivos Acepta
								</a>
							</li>

							<li id="recepcion-bodega-contrato-upload-li" >
								<a href="{{ asset('recepcion_bodega/upload/contrato/masivo') }}">
									<i class="fas fa-server"></i>
									Ws Contrato
								</a>
							</li>

							<li id="recepcion-bodega-orden-upload-li" >
								<a href="{{ asset('recepcion_bodega/upload/orden_compra/masivo') }}">
									<i class="fas fa-server"></i>
									Ws Orden Compra
								</a>
							</li>

							<li id="recepcion-bodega-documento-upload-li" >
								<a href="{{ asset('recepcion_bodega/upload/documento/masivo') }}">
									<i class="fas fa-server"></i>
									Ws Documento
								</a>
							</li>

							<li id="recepcion-bodega-item-upload-li" >
								<a href="{{ asset('recepcion_bodega/upload/item/masivo') }}">
									<i class="fas fa-server"></i>
									Ws Item Presupuestario
								</a>
							</li>

							<li id="recepcion-bodega-archivo-upload-li" >
								<a href="{{ asset('recepcion_bodega/upload/archivo/masivo') }}">
									<i class="fas fa-server"></i>
									Ws Archivo
								</a>
							</li>
							
						</ul>
					</li>
				@endrole

				@permission(['crear-user','editar-user','eliminar-user','ver-user','ver-general-user',
							 'crear-proveedor','editar-proveedor','eliminar-proveedor','ver-proveedor','ver-general-proveedor',
							 'crear-tipo-documento','editar-tipo-documento','eliminar-tipo-documento','ver-tipo-documento','ver-general-tipo-documento',
							 'crear-item-presupuestario','editar-item-presupuestario','eliminar-item-presupuestario','ver-item-presupuestario','ver-general-item-presupuestario',
							 'crear-modalidad-compra','editar-modalidad-compra','eliminar-modalidad-compra','ver-modalidad-compra','ver-general-modalidad-compra',
							 'crear-tipo-adjudicacion','editar-tipo-adjudicacion','eliminar-tipo-adjudicacion','ver-tipo-adjudicacion','ver-general-tipo-adjudicacion',
							 'crear-tipo-informe','editar-tipo-informe','eliminar-tipo-informe','ver-tipo-informe','ver-general-tipo-informe',
							 'crear-tipo-archivo','editar-tipo-archivo','eliminar-tipo-archivo','ver-tipo-archivo','ver-general-tipo-archivo',
							 'crear-motivo-rechazo','editar-motivo-rechazo','eliminar-motivo-rechazo','ver-motivo-rechazo','ver-general-motivo-rechazo',
							 'crear-motivo-problema','editar-motivo-problema','eliminar-motivo-problema','ver-motivo-problema','ver-general-motivo-problema',
							 'crear-referente-tecnico','editar-referente-tecnico','eliminar-referente-tecnico','ver-referente-tecnico','ver-general-referente-tecnico'])
                <li id="mantenedores" class="">
					<a id="mantenedores-a"href="javascript:;" >
						<i class="fa fa-cogs"></i>
						<span class="title">
							Mantenedor
						</span>
						<span class="arrow">
						</span>
					</a>
					<ul class="sub-menu">
						@role(['propietario','administrador'])
							<li id="perfiles-li" >
								<a href="{{ asset('perfiles') }}">
									<i class="fas fa-user-tag"></i>
									Perfiles
								</a>
							</li>

							<li id="permisos-li" >
								<a href="{{ asset('permisos') }}">
									<i class="fas fa-user-shield fa-flip-horizontal"></i>
									Permisos
								</a>
							</li>
						@endrole
						@permission(['crear-user','editar-user','eliminar-user','ver-user','ver-general-user'])
							<li id="usuarios-li" >
								<a href="{{ asset('usuarios') }}">
									<i class="fas fa-users"></i>
									Usuarios
								</a>
							</li>
						@endpermission

						@permission(['crear-proveedor','editar-proveedor','eliminar-proveedor','ver-proveedor','ver-general-proveedor'])
						<li id="proveedores-li" >
							<a href="{{ asset('proveedores') }}">
								<i class="fas fa-truck" ></i>
								Proveedor
							</a>
						</li>
						@endpermission

						@permission(['crear-tipo-documento','editar-tipo-documento','eliminar-tipo-documento','ver-tipo-documento','ver-general-tipo-documento'])
						<li id="tipos-documento-li" >
							<a href="{{ asset('tipos_documento') }}">
								<i class="fas fa-file-alt" ></i>
								Tipo Documento
							</a>
						</li>
						@endpermission

						@permission(['crear-item-presupuestario','editar-item-presupuestario','eliminar-item-presupuestario','ver-item-presupuestario','ver-general-item-presupuestario'])
						<li id="items-li" >
							<a href="{{ asset('item') }}">
								<i class="fa fa-list-ol"></i>
								Ítem Presupuestario
							</a>
						</li>
						@endpermission

						@permission(['crear-modalidad-compra','editar-modalidad-compra','eliminar-modalidad-compra','ver-modalidad-compra','ver-general-modalidad-compra'])
						<li id="modalidades-compra-li" >
							<a href="{{ asset('modalidad_compra') }}">
								<i class="fas fa-shopping-basket"></i>
								Modalidad de Compra
							</a>
						</li>
						@endpermission

						@permission(['crear-tipo-adjudicacion','editar-tipo-adjudicacion','eliminar-tipo-adjudicacion','ver-tipo-adjudicacion','ver-general-tipo-adjudicacion'])
						<li id="tipo-adjudicacion-li" >
							<a href="{{ asset('tipo_adjudicacion') }}">
								<i class="fas fa-list"></i>
								Tipo Adjudicación
							</a>
						</li>
						@endpermission

						@permission(['crear-tipo-informe','editar-tipo-informe','eliminar-tipo-informe','ver-tipo-informe','ver-general-tipo-informe'])
						<li id="tipo-informe-li" >
							<a href="{{ asset('tipo_informe') }}">
								<i class="fas fa-file-contract"></i>
								Tipo Informe
							</a>
						</li>
						@endpermission

						@permission(['crear-tipo-archivo','editar-tipo-archivo','eliminar-tipo-archivo','ver-tipo-archivo','ver-general-tipo-archivo'])
						<li id="tipo-archivo-li" >
							<a href="{{ asset('tipo_archivo') }}">
								<i class="fas fa-file"></i>
								Tipo Archivo
							</a>
						</li>
						@endpermission

						@permission(['crear-motivo-rechazo','editar-motivo-rechazo','eliminar-motivo-rechazo','ver-motivo-rechazo','ver-general-motivo-rechazo'])
						<li id="motivo-rechazo-li" >
							<a href="{{ asset('motivo_rechazo') }}">
								<i class="fas fa-ban"></i>
								Motivo Rechazo
							</a>
						</li>
						@endpermission

						{{-- @permission(['crear-motivo-rezagar','editar-motivo-rezagar','eliminar-motivo-rezagar','ver-motivo-rezagar','ver-general-motivo-rezagar']) --}}
						<li id="motivo-rezagar-li" >
							<a href="{{ asset('motivo_rezagar') }}">
								<i class="fas fa-hourglass-half"></i>
								Motivo Rezagar
							</a>
						</li>
						{{-- @endpermission --}}

						@permission(['crear-motivo-problema','editar-motivo-problema','eliminar-motivo-problema','ver-motivo-problema','ver-general-motivo-problema'])
						<li id="motivo-problema-li" >
							<a href="{{ asset('motivo_problema') }}">
								<i class="fas fa-exclamation-triangle"></i>
								Motivo Problema
							</a>
						</li>
						@endpermission

						{{-- @permission(['crear-medio-pago','editar-medio-pago','eliminar-medio-pago','ver-medio-pago','ver-general-medio-pago']) --}}
						<li id="medio-pago-li" >
							<a href="{{ asset('medio_pago') }}">
								<i class="fas fa-dollar-sign"></i>
								Medios de Pago
							</a>
						</li>
						{{-- @endpermission --}}

						{{-- @permission(['crear-cuenta-contable','editar-cuenta-contable','eliminar-cuenta-contable','ver-cuenta-contable','ver-general-cuenta-contable']) --}}
						<li id="cuenta-contable-li" >
							<a href="{{ asset('cuenta_contable') }}">
								<i class="fas fa-money-check-alt"></i>
								Cuenta Contable
							</a>
						</li>
						{{-- @endpermission --}}

						{{-- @permission(['crear-cuenta-bancaria','editar-cuenta-bancaria','eliminar-cuenta-bancaria','ver-cuenta-bancaria','ver-general-cuenta-bancaria']) --}}
						<li id="cuenta-bancaria-li" >
							<a href="{{ asset('cuenta_bancaria') }}">
								<i class="fas fa-money-check"></i>
								Cuenta Bancaria
							</a>
						</li>
						{{-- @endpermission --}}


						@permission(['crear-referente-tecnico','editar-referente-tecnico','eliminar-referente-tecnico','ver-referente-tecnico','ver-general-referente-tecnico'])
						<li id="referente-tecnico-li" >
							<a href="{{ asset('referente_tecnico') }}">
								<i class="fas fa-id-card-alt"></i>
								Referente Técnico
							</a>
						</li>
						@endpermission

						{{-- @permission(['crear-unidad','editar-unidad','eliminar-unidad','ver-unidad','ver-general-unidad']) --}}
						<li id="unidad-li" >
							<a href="{{ asset('unidad') }}">
								<i class="fas fa-address-book"></i>
								Unidad
							</a>
						</li>
						{{-- @endpermission --}}

						{{-- @permission(['crear-profesion','editar-profesion','eliminar-profesion','ver-profesion','ver-general-profesion']) --}}
						<li id="profesion-li" >
							<a href="{{ asset('profesion') }}">
								<i class="fas fa-user-graduate"></i>
								Profesión
							</a>
						</li>
						{{-- @endpermission --}}

						{{-- @permission(['crear-tipo-prestacion','editar-tipo-prestacion','eliminar-tipo-prestacion','ver-tipo-prestacion','ver-general-tipo-prestacion']) --}}
						<li id="tipo-prestacion-li" >
							<a href="{{ asset('tipo_prestacion') }}">
								<i class="fas fa-user-tie"></i>
								Tipo Prestación
							</a>
						</li>
						{{-- @endpermission --}}

						{{-- @permission(['crear-categoria','editar-categoria','eliminar-categoria','ver-categoria','ver-general-categoria']) --}}
						<li id="categoria-li" >
							<a href="{{ asset('categoria') }}">
								<i class="fas fa-tags"></i>
								Categoría
							</a>
						</li>
						{{-- @endpermission --}}
						
					</ul>
				</li>
				@endpermission

				<li id="salir" class="">
					<a href="{{ url('/logout') }}" onclick="event.preventDefault(); document.getElementById('logout-form').submit();">
	                    <i class="menu-icon fa fa-power-off" style="color:#d12611;"></i> Salir</a>
				</li>
				
		
				
			</ul>
			<!-- END SIDEBAR MENU -->
		</div>
	</div>
</div>
<!-- END SIDEBAR -->
@yield('content')
<!-- BEGIN CONTENT -->
<!--
<div class="page-content-wrapper">
	<div class="page-content-wrapper">
		<div class="page-content">
		-->
			<!-- BEGIN SAMPLE PORTLET CONFIGURATION MODAL FORM-->
			<!--
			<div class="modal fade" id="portlet-config" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
				<div class="modal-dialog">
					<div class="modal-content">
						<div class="modal-header">
							<button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
							<h4 class="modal-title">Modal title</h4>
						</div>
						<div class="modal-body">
							 Widget settings form goes here
						</div>
						<div class="modal-footer">
							<button type="button" class="btn btn-success">Save changes</button>
							<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
						</div>
					</div>
					-->
					<!-- /.modal-content -->
					<!--
				</div>
				-->
				<!-- /.modal-dialog -->
				<!--
			</div>
			-->
			<!-- END SAMPLE PORTLET CONFIGURATION MODAL FORM-->
			
			<!-- BEGIN PAGE HEADER-->
			<!--
			<div class="row">
				<div class="col-md-12">
				-->
					<!-- BEGIN PAGE TITLE & BREADCRUMB-->
					<!--
					<h3 class="page-title">
					Dashboard
					</h3>
					<ul class="page-breadcrumb breadcrumb">
						<li>
							<i class="fa fa-home"></i>
							<a href="index-2.html">Home</a>
							<i class="fa fa-angle-right"></i>
						</li>
						<li>
							<a href="#">Dashboard</a>
						</li>
						
					</ul>
					-->
					<!-- END PAGE TITLE & BREADCRUMB-->
					<!--
				</div>
			</div>
			-->
			<!-- END PAGE HEADER-->
			<!--
		</div>
	</div>
</div>
-->
<!-- END CONTENT -->

<div id="modalEditPerfil"></div>
<div class="modal fade" id="modalCarga" role="dialog" aria-labelledby="mymodalCarga" aria-hidden="true" >
    <div style="padding-top: 15%" class="text-center">
		<img src="{{ asset('logo-finanzas-navA.png') }}" id="modalCargaImg">
		<h5 class="text-center" style="color:white;font-style: italic;"><strong>Cargando</strong></h5>
		<br>
		<h4 class="text-center" style="color:#275360;margin-top:3%;"><strong><i class="fas fa-spinner fa-5x fa-pulse"></i></strong></h4>
    </div>
</div>
</div>
<!-- END CONTAINER -->
<!-- BEGIN FOOTER -->
<div class="footer">
<div class="footer-inner">
	 {{ date('Y') }} &copy; Hospital San Juan de Dios - Departamento de Informática.
</div>
<div class="footer-tools">
	<span class="go-top">
		<i class="fa fa-angle-up"></i>
	</span>
</div>
</div>
<!-- END FOOTER -->
<!-- BEGIN JAVASCRIPTS(Load javascripts at bottom, this will reduce page load time) -->
<!-- BEGIN CORE PLUGINS -->
<!--[if lt IE 9]>
<script src="assets/plugins/respond.min.js"></script>
<script src="assets/plugins/excanvas.min.js"></script> 
<![endif]-->
{{-- 
<script src="{{ asset('assets/plugins/jquery-1.10.2.min.js') }} " type="text/javascript"></script>

<script src="{{ asset('assets/plugins/jquery-migrate-1.2.1.min.js') }}" type="text/javascript"></script>

<script src="{{ asset('assets/plugins/jquery-1.12.5.js') }} " type="text/javascript"></script>

<script src="{{ asset('assets/plugins/jquery-migrate-1.4.1.js') }}" type="text/javascript"></script>
--}}
<script src="{{ asset('assets/plugins/jquery-3.4.1.js') }} " type="text/javascript"></script>

<script src="{{ asset('assets/plugins/jquery-migrate-3.1.0.js') }}" type="text/javascript"></script>
<!-- IMPORTANT! Load jquery-ui-1.10.3.custom.min.js before bootstrap.min.js to fix bootstrap tooltip conflict with jquery ui tooltip -->
<script src="{{ asset('assets/plugins/jquery-ui/jquery-ui-1.12.1.min.js') }}" type="text/javascript"></script>
<script src="{{ asset('assets/plugins/moment.min.js') }}" type="text/javascript"></script>
<script src="{{ asset('assets/plugins/moment-with-locales.min.js') }}" type="text/javascript"></script>
<script src="{{ asset('assets/plugins/bootstrap/js/bootstrap.min.js') }}" type="text/javascript"></script>

{{-- Falta actualizar --}}
<script src="{{ asset('assets/plugins/bootstrap-hover-dropdown/twitter-bootstrap-hover-dropdown.min.js') }}" type="text/javascript"></script>

<script src="{{ asset('assets/plugins/jquery-slimscroll/jquery.slimscroll.min.js') }}" type="text/javascript"></script>
<script src="{{ asset('assets/plugins/jquery.blockui.min.js') }}" type="text/javascript"></script>
<script src="{{ asset('assets/plugins/jquery.cokie.min.js') }}" type="text/javascript"></script>

{{-- 
<script src="{{ asset('assets/plugins/uniform/jquery.uniform.min.js') }}" type="text/javascript"></script>
--}}
<script src="{{asset('assets/plugins/jquery-validation/dist/jquery.validate.min.js')}}"></script>
<script src="{{asset('assets/plugins/jquery-validation/dist/localization/messages_es.min.js')}}"></script>

<script src="{{asset('assets/plugins/bootstrap-toastr/toastr.min.js')}}"></script>
<script src="{{asset('assets/scripts/ui-toastr.js')}}"></script>
<!-- END CORE PLUGINS -->
{{--
<!-- BEGIN PAGE LEVEL PLUGINS -->
<script src="{{ asset('assets/plugins/jqvmap/jqvmap/jquery.vmap.js') }}" type="text/javascript"></script>
<script src="{{ asset('assets/plugins/jqvmap/jqvmap/maps/jquery.vmap.russia.js') }}" type="text/javascript"></script>
<script src="{{ asset('assets/plugins/jqvmap/jqvmap/maps/jquery.vmap.world.js') }}" type="text/javascript"></script>
<script src="{{ asset('assets/plugins/jqvmap/jqvmap/maps/jquery.vmap.europe.js') }}" type="text/javascript"></script>
<script src="{{ asset('assets/plugins/jqvmap/jqvmap/maps/jquery.vmap.germany.js') }}" type="text/javascript"></script>
<script src="{{ asset('assets/plugins/jqvmap/jqvmap/maps/jquery.vmap.usa.js') }}" type="text/javascript"></script>
<script src="{{ asset('assets/plugins/jqvmap/jqvmap/data/jquery.vmap.sampledata.js') }}" type="text/javascript"></script>
<script src="{{ asset('assets/plugins/jquery.peity.min.js') }}" type="text/javascript"></script>
<script src="{{ asset('assets/plugins/jquery.pulsate.min.js') }}" type="text/javascript"></script>
<script src="{{ asset('assets/plugins/jquery-knob/js/jquery.knob.js') }}" type="text/javascript"></script>
<script src="{{ asset('assets/plugins/flot/jquery.flot.js') }}" type="text/javascript"></script>
<script src="{{ asset('assets/plugins/flot/jquery.flot.resize.js') }}" type="text/javascript"></script>
<script src="{{ asset('assets/plugins/bootstrap-daterangepicker/moment.min.js') }}" type="text/javascript"></script>
<script src="{{ asset('assets/plugins/bootstrap-daterangepicker/daterangepicker.js') }}" type="text/javascript"></script>
<script src="{{ asset('assets/plugins/gritter/js/jquery.gritter.js') }}" type="text/javascript"></script>
<!-- IMPORTANT! fullcalendar depends on jquery-ui-1.10.3.custom.min.js for drag & drop support -->
<script src="{{ asset('assets/plugins/fullcalendar/fullcalendar/fullcalendar.min.js') }}" type="text/javascript"></script>
<script src="{{ asset('assets/plugins/jquery-easy-pie-chart/jquery.easy-pie-chart.js') }}" type="text/javascript"></script>
<script src="{{ asset('assets/plugins/jquery.sparkline.min.js') }}" type="text/javascript"></script>
<!-- END PAGE LEVEL PLUGINS -->
--}}
<!-- BEGIN PAGE LEVEL SCRIPTS -->
<script src="{{ asset('assets/scripts/app.js') }}" type="text/javascript"></script>
<script src="{{ asset('assets/scripts/index.js') }}" type="text/javascript"></script>
<script src="{{ asset('assets/scripts/tasks.js') }}" type="text/javascript"></script>
<!-- END PAGE LEVEL SCRIPTS -->
@stack('script-footer')
<script type="text/javascript">

    optionsToastr = {
        "closeButton": true,
        "debug": true,
        //"positionClass": "toast-top-right",
		"positionClass": "toast-bottom-right",
        "onclick": null,
        "showDuration": "1000",
        "hideDuration": "1000",
        "timeOut": "5000",
        "extendedTimeOut": "1000",
        "showEasing": "swing",
        "hideEasing": "linear",
        "showMethod": "fadeIn",
        "hideMethod": "fadeOut",
		"preventDuplicates": true,
		"preventOpenDuplicates": true
	}

	//elimina todos los puntos del string
	function remplaza(str1) {
		str1 = str1.replace(/\./g, '');
		str1 = str1.replace(',','.');
		str1 = str1.replace(/\,/g,'');
		//para que no finalize en punto
		if(str1.charAt(str1.length-1) == '.'){
			str1 = str1.replace('.','.0');
		}
		return str1;
	}
	
	
	function formatMoneyDecimal(number, places, symbol, thousand, decimal) {

		//revisa si puso coma al final
		if( number.charAt(number.length-1) == "," || number.charAt(number.length-1) == "." ) {
			var com = true;
		}

		//"limpia el numero"
		number = remplaza(number);

		//si tenia punto decimal lo borra
		if(com){
			number = number.replace('.0','');
		}

		//comprueba si es un numero
		if ( !isNaN(number) ) {

			places = !isNaN(places = Math.abs(places)) ? places : 0;
			//restringe cuantos decimales permite
			if(number.length >=1){
				if(number.indexOf(".") != -1){
					var places = number.length - number.indexOf(".")-1;
					if(places > 1){
						places = 1;
					}
				}
			}

			number = number || 0;
			symbol = symbol !== undefined ? symbol : "";
			thousand = thousand || ".";
			decimal = decimal || ",";
			var negative = number < 0 ? "-" : "",
			i = parseInt(number = Math.abs(+number || 0).toFixed(places), 10) + "",
			j = (j = i.length) > 3 ? j % 3 : 0;
			number = symbol + negative + (j ? i.substr(0, j) + thousand : "") + i.substr(j).replace(/(\d{3})(?=\d)/g, "$1" + thousand) + (places ? decimal + Math.abs(number - i).toFixed(places).slice(2) : "");

			//le agrega la coma al final como texto
			if(com && !number.match(/\,/g)){
				return number + ',';
			}
			else{
				//console.log(number);
				return number;
			}
		} else {

			toastr.warning("Solo se permiten números", "Atención", optionsToastr);
			//elimina todo lo que no sea numero
			number = number.match(/[\d\.]*/g)[0].replace('.',',');
			//return formatMoneyDecimal(number);

		}

	}

	//cambia un entero a formato moneda
	function formatMoney(number, places, symbol, thousand, decimal) {
		//revisando si es numero
		//console.log(number);
		if (!isNaN(number)){
			number = number || 0;
			places = !isNaN(places = Math.abs(places)) ? places : 0;
			symbol = symbol !== undefined ? symbol : "";
			thousand = thousand || ".";
			decimal = decimal || ",";
			var negative = number < 0 ? "-" : "",
			i = parseInt(number = Math.abs(+number || 0).toFixed(places), 10) + "",
			j = (j = i.length) > 3 ? j % 3 : 0;
			return symbol + negative + (j ? i.substr(0, j) + thousand : "") + i.substr(j).replace(/(\d{3})(?=\d)/g, "$1" + thousand) + (places ? decimal + Math.abs(number - i).toFixed(places).slice(2) : "");
		}
		else{
			// toastr["warning"]("Solo se permiten numeros");
			toastr.warning("Solo se permiten números", "Atención", optionsToastr);
			number = number.replace(/[^\d\.]*/g, '');
			return formatMoney(number);
		}
	}

/*
* Notificaciones, es necesaria una accion de usuario en ellas.
* -Estan los documentos del sistema que tienen match con documentos de bodega,
*  se debe confirmar la "union", para conectar los documentos.
* -Estan los documentos de acepta que tiene match con documentos de bodega,
*  se debe ingresar el documento acepta para luego conectarlo con el documento de
*  bodega.
* -Estan los vistos buenos pendientes de los referentes tecnicos
* - Estan los documentos con rechazo visto bueno
*/
var notificaciones = 0;
var notificacionDocumentosRecepcionPorConfirmar = 0;
var notificacionDocumentosAceptaRecepcionBodega = 0;
var notificacionVistosBuenosPendientes = 0;
var notificacionRechazoVistoBueno = 0;

/*
* Alertas, muestran alertas a los usuarios, no es necesaria una accion.
* Esta la alerta de la conexion automatica entre documentos del sistema y
* documentos de la bodega, son documentos Cenabast y de OC = Intermediación.
*/
var alertas = 0;
var alertaDocumentosIntermediacionConBodega = 0;

$(document).ready(function() {

	/*
	* Funciones para Notificaciones
	*/

	function mostrarNotificaciones()
	{
		notificaciones = 0;
		
		notificaciones = notificaciones + notificacionDocumentosRecepcionPorConfirmar;
		notificaciones = notificaciones + notificacionDocumentosAceptaRecepcionBodega;
		notificaciones = notificaciones + notificacionVistosBuenosPendientes;
		notificaciones = notificaciones + notificacionRechazoVistoBueno;

		$('#numeroNotificaciones').html(notificaciones);
		if ( notificaciones > 0 ) {
			$('#header_notification_bar').css('display','');
		} else {
			$('#header_notification_bar').css('display','none');
		}
	}
	
	function getDocumencoConRecepcionBodegaPorConfirmar() 
	{
		// ir a buscar la cantidad de documentos
		$.get( '{{ url("documentos/num/bodega/por_confirmar") }}', function( respuesta ) {
            // console.log( respuesta );
			if ( respuesta.estado == 'success' ) {
				if ( respuesta.numDoc > 0 ) {
					notificacionDocumentosRecepcionPorConfirmar = respuesta.numDoc;
					$('#numDocRecepcion').html( respuesta.liReemplaza );
				} else {
					// Ocultar notificacion
					$('#numDocRecepcion').html('');
					notificacionDocumentosRecepcionPorConfirmar = 0;
				}
			}
        });
	}

	function getDocumencoAceptaConRecepcionBodega() 
	{
		// ir a buscar la cantidad de documentos
		$.get( '{{ url("archivos_acepta/num/bodega") }}', function( respuesta ) {
            // console.log( respuesta );
			if ( respuesta.estado == 'success' ) {
				if ( respuesta.numDoc > 0 ) {
					notificacionDocumentosAceptaRecepcionBodega = respuesta.numDoc;
					$('#numAceptaRecepcion').html( respuesta.liReemplaza );
				} else {
					// Ocultar notificacion
					$('#numAceptaRecepcion').html('');
					notificacionDocumentosAceptaRecepcionBodega = 0;
				}
			}
        });
	}

	function getDocumencoRechazoVistoBueno() 
	{
		// ir a buscar la cantidad de documentos
		$.get( '{{ url("documentos/num/rechazo_visto_bueno") }}', function( respuesta ) {
            // console.log( respuesta );
			if ( respuesta.estado == 'success' ) {
				if ( respuesta.numDoc > 0 ) {
					notificacionRechazoVistoBueno = respuesta.numDoc;
					$('#numRechazoVistoBueno').html( respuesta.liReemplaza );
				} else {
					// Ocultar notificacion
					$('#numRechazoVistoBueno').html('');
					notificacionRechazoVistoBueno = 0;
				}
			}
        });
	}
	
	/*
	* Funciones para mostrar alertas
	*/

	function mostrarAlertas()
	{
		alertas = 0;
		if ( alertaDocumentosIntermediacionConBodega != 0) {
			alertas = alertas + alertaDocumentosIntermediacionConBodega;
		}

		$('#numeroAlertas').html(alertas);
		if ( alertas > 0 ) {
			$('#header_alert_bar').css('display','');
		} else {
			$('#header_alert_bar').css('display','none');
		}
	}
	
	function getDocumencoIntermediacionConBodega() 
	{
		// ir a buscar la cantidad de documentos
		$.get( '{{ url("documentos/bodega/intermediacion") }}', function( respuesta ) {
            // console.log( respuesta );
			if ( respuesta.estado == 'success' ) {
				if ( respuesta.numDoc > 0 ) {
					alertaDocumentosIntermediacionConBodega = respuesta.numDoc;
					$('#numDocIntermediacion').html( respuesta.liReemplaza );
				} else {
					// Ocultar notificacion
					$('#numDocIntermediacion').html('');
					alertaDocumentosIntermediacionConBodega = 0;
				}
			}
        });
	}
	
	// Carga automaticamente los archivos del tipo factura en los documentos
	function cargarArchivosEnLosDocumentos()
	{
		$.get( '{{ url("archivos_acepta/carga_archivos_en_documentos") }}', function( respuesta ) {
            console.log( respuesta );
			if ( respuesta.estado == 'success' ) {
				console.log('cantidad de cargas : '+ respuesta.cantidadCargas)
			}
        });
	}

	/*
	* Funciones para Referente Tecnico
	*/
	function getCantidadVistosBuenosPendientes()
	{
		// ir a buscar la cantidad de vistos buenos
		$.get( '{{ url("referente_tecnico/num/vistos_buenos_pendientes") }}', function( respuesta ) {
            // console.log( respuesta );
			if ( respuesta.estado == 'success' ) {
				if ( respuesta.numDoc > 0 ) {
					notificacionVistosBuenosPendientes = respuesta.numDoc;
					$('#numVistosBuenosPendientes').html( respuesta.liReemplaza );
				} else {
					// Ocultar notificacion
					$('#numVistosBuenosPendientes').html('');
					notificacionVistosBuenosPendientes = 0;
				}
			}
        });
	}


	{{-- 
		Filtros para ejecutar funciones dependiendo del perfil del usuario
	--}}
	@role(['propietario','administrador','administrador-devengo','usuario-general','usuario-ingreso'])

		setInterval(getDocumencoConRecepcionBodegaPorConfirmar, 11000); // 11 segundos
		setInterval(getDocumencoAceptaConRecepcionBodega, 15000); // 15 segundos
		setInterval(getDocumencoRechazoVistoBueno, 18000); // 18 segundos

		setInterval(getDocumencoIntermediacionConBodega, 14000); // 14 segundos
		
	@endrole

	@role(['referente-tecnico'])
		setInterval(getCantidadVistosBuenosPendientes, 10000); // 10 segundos
	@endrole
	
	setInterval(mostrarNotificaciones, 2000);
	setInterval(mostrarAlertas, 200000); // 20 segundos


   //App.init(); // initlayout and core plugins
   /*
   Index.init();
   
   Index.initJQVMAP(); // init index page's custom scripts
   Index.initCalendar(); // init index page's custom scripts
   Index.initCharts(); // init index page's custom scripts
   Index.initChat();
   Index.initMiniCharts();
   Index.initPeityElements();
   Index.initKnowElements();
   Index.initDashboardDaterange();
   Tasks.initDashboardWidget();
   */
});

/*
* Funcion para editar el perfil del usuario logeado
*/
function editarPerfil()
{
	$.get( '{{ url("usuarios/modal/editar_perfil") }}', function( respuesta ) {
		$( "#modalEditPerfil" ).html( respuesta );
		$( "#modalEditarPerfil" ).modal();
	});
}
</script>
<!-- END JAVASCRIPTS -->
<script type="text/javascript">  var _gaq = _gaq || [];  _gaq.push(['_setAccount', 'UA-37564768-1']);  _gaq.push(['_setDomainName', 'keenthemes.com']);  _gaq.push(['_setAllowLinker', true]);  _gaq.push(['_trackPageview']);  (function() {    var ga = document.createElement('script'); ga.type = 'text/javascript'; ga.async = true;    ga.src = ('https:' == document.location.protocol ? 'https://' : 'http://') + 'stats.g.doubleclick.net/dc.js';    var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(ga, s);  })();</script></body>
<!-- END BODY -->

<!-- Mirrored from www.keenthemes.com/preview/conquer/ by HTTrack Website Copier/3.x [XR&CO'2013], Fri, 23 May 2014 03:02:02 GMT -->
</html>