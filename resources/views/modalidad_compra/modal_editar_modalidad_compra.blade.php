<link href="{{ asset('sistema/css/modales.css') }} " rel="stylesheet" type="text/css"/>
<div class="modal fade" id="modalEditar" role="dialog" aria-labelledby="myModalLabel3" aria-hidden="true">
	<div class="modal-dialog" style="width:1100px;">
		<div class="modal-content">
            <div class="modal-header">
				<button type="button" title="Cerrar" class="close" data-dismiss="modal" aria-hidden="true"></button>
				<h4 class="modal-title" ><strong><i class="fas fa-shopping-basket"></i>&nbsp;&nbsp;Editar Modalidad de Compra</strong></h4>
            </div>

            <form action="{{ asset('modalidad_compra/editar_modalidad') }}" method="post" class="horizontal-form" id="form-modalidad" autocomplete="off">
                <div class="modal-body">

                    <div class="form-body">
                        <div class="note note-danger" id="divErrores" style="display:none;">
                            <h4 class="block" style="margin-bottom:5 px;">Debe Completar los siguientes campos: </h4>
                            <ul id="ulErrores"></ul>
                        </div>

                        <div class="form-group row">
                            <label for="nombre" class="col-sm-2 control-label label-form">Nombre <span class="span-label">*</span></label>
                            <div class="col-sm-8 form-validate">
                                <input type="text" class="form-control" id="nombre" name="nombre" required value="{{ $modalidadCompra->nombre }}">
                            </div>
                        </div>

                        <div class="form-group row">
                            <label for="valor_predeterminado" class="col-sm-3 control-label label-form">Valor Predeterminado</label>
                            <div class="col-sm-7 form-validate">
                                <input type="text" class="form-control" id="valor_predeterminado" name="valor_predeterminado" value="{{ $modalidadCompra->predeterminado }}">
                            </div>
                        </div>

                    </div>
                </div>

                <div class="modal-footer form-actions right">
                    <input type="hidden" name="_token" value="{{ csrf_token() }}">
                    <input type="hidden" name="id_modalidad" value="{{ $modalidadCompra->id }}">

                    <button type="button" title="Cancelar" class="btn btn-default" data-dismiss="modal">Cancelar</button>
                    <button type="submit" title="Editar Modalidad Compra" class="btn btn-warning" id="botonGuardar"><i class="far fa-save" style="color:black;"></i> Editar Modalidad de Compra</button>
                </div>
            </form>

        </div>
    </div>
</div>

<script>
    $(document).ready(function() {


        $("#form-modalidad").validate({
            highlight: function(element) {
                $(element).closest('.form-validate').removeClass('has-success');
                $(element).closest('.form-validate').addClass('has-error');
            },
            unhighlight: function(element) {
                $(element).closest('.form-validate').removeClass('has-error');
                $(element).closest('.form-validate').addClass('has-success');
            },
            errorElement: 'span',
            errorClass: 'help-block',
            errorPlacement: function (error, element) {
                console.log(element);
                if (element.parent('.input-group').length) {
                    error.insertAfter(element.parent());
                } else {
                    error.insertAfter(element);
                }
            },
            rules: {
            },
            messages: {
            },

            //para enviar el formulario por ajax
            submitHandler: function(form) {
                esperandoEdicion();

                let formData = new FormData(form);

                $.ajax({
                    url: form.action,
                    type: form.method,
                    //data: $(form).serialize(),
                    data: formData,
                    processData: false,
                    contentType: false,
                    success: function(respuesta) {
                        console.log(respuesta);
                        if ( respuesta.estado == 'error' ) {
                            toastr.error('No es posible realizar la acción'+'<br><strong>Error: '+ respuesta.mensaje +'</strong>', 'Atención', optionsToastr);
                        } else if ( respuesta.estado == 'success') {
                            
                            toastr.success(respuesta.mensaje, 'Atención', optionsToastr);
                            // Cambiar la informacion de la fila de la tabla de modalidad de compra
                            editarFilaTabla(respuesta);
                            $("#modalEditar").modal("hide");
                            
                        }
                        
                    }            
                }).fail( function(respuesta) {//fail ajax
                    if ( respuesta.status == 400 ) {
                        mostrarErroresValidator(respuesta);
                    } else if ( respuesta.status == 500 ) {
                        toastr.error('No es posible realizar la acción, error en el servidor', 'Atención', optionsToastr);
                    } else {
                        toastr.error('No es posible realizar la acción', 'Atención', optionsToastr);
                    }
                    
                })
                .always(function() {
                    listaEdicion();
                });//ajax
                
            }

        });

    });

    function esperandoEdicion()
    {   
        $('.page-header-fixed *').css('cursor', 'wait');
        $("#botonGuardar").attr("disabled",true);
    }

    function listaEdicion()
    {
        $('.page-header-fixed *').css('cursor', '');
        $("#botonGuardar").attr("disabled",false);
    }

    function mostrarErroresValidator(respuesta)
    {
        if ( respuesta.responseJSON ) {
            //console.log(respuesta.responseJSON);
            let htmlErrores = '';
            for (let k in respuesta.responseJSON) { 
                //console.log(k, respuesta.responseJSON[k]);
                htmlErrores += '<li>' + respuesta.responseJSON[k] + '</li>';
            }

            $('#ulErrores').html(htmlErrores);
            $('#divErrores').css('display','');
            toastr.error('No es posible realizar la acción'+'<br>'+'Errores:<br> <ul>'+htmlErrores+'</ul>', 'Atención', optionsToastr);
        }
    }

    function editarFilaTabla(respuesta)
    {

        let botones = '<td width="10%"><div class="btn-group">';
        @permission(['ver-modalidad-compra'])
        botones += '<button class="btn btn-success btn-xs" title="Ver Modalidad Compra" onclick="ver('+ respuesta.id +');">';
        botones += '<i class="fas fa-eye"></i></button>';
        @endpermission
        @permission(['editar-modalidad-compra'])
        botones += '<button class="btn btn-warning btn-xs" title="Editar Modalidad Compra" onclick="editar('+ respuesta.id +');">';
        botones += '<i class="fas fa-edit"></i></button>';
        @endpermission
        @permission(['eliminar-modalidad-compra'])
        botones += '<button class="btn btn-danger btn-xs" title="Eliminar Modalidad Compra" onclick="eliminar('+ respuesta.id +');">';
        botones += '<i class="fas fa-trash"></i></button>';
        @endpermission
        botones += '</div></td>';

        tablaModalidades.row('#modalidad_'+ respuesta.id).data([
            respuesta.id,
            respuesta.nombre,
            respuesta.predeterminado,
            botones
        ]).draw();
    }

</script>