@extends('master')

@section('title', 'Mantenedor de Modalidad de Compra')

@push('css-head')
<!-- BEGIN PAGE LEVEL STYLES -->
<link rel="stylesheet" type="text/css" href="{{asset('assets/plugins/select2/select2_conquer.css')}}"/>
<link rel="stylesheet" href="{{asset('assets/plugins/data-tables/DT_bootstrap.css')}}"/>
<!-- END PAGE LEVEL STYLES -->

@endpush

@section('content')

<!-- BEGIN CONTENT -->

<div class="page-content-wrapper">
	<div class="page-content-wrapper">
		<div class="page-content">

			<!-- BEGIN PAGE CONTENT-->
			<div class="row">
				<div class="col-md-12">
					<!-- BEGIN EXAMPLE TABLE PORTLET-->
					<div class="portlet">
						<div class="portlet-title">
							<div class="caption">
								<i class="fas fa-shopping-basket"></i> Mantenedor de Modalidad de Compra
							</div>
						</div>
						<div class="portlet-body">

                            <div class="table-toolbar">
                                <div class="btn-group">
                                    @permission(['crear-modalidad-compra'])
                                    <button title="Agregar Modalidad Compra" class="btn btn-success" onclick="nuevo();">
										<i class="fa fa-plus"></i> Agregar Modalidad Compra
									</button>
                                    @endpermission
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-12">
                                    <div class="panel">
                                    {{-- 
                                        <div class="panel-heading">
                                            <div class="caption">
                                                <strong style="color: #69aa46;">Últimos Documentos Ingresados</strong>
                                            </div>
                                        </div>
                                        --}}
                                        <div class="panel-body">
                                            <div class="table-responsive">
                                                <table class="table table-striped table-bordered table-hover table-full-width" id="tabla_modalidades">
                                                    <thead>
                                                    <tr>
                                                        <th width="10%">ID</th>
                                                        <th width="40%">Nombre</th>
                                                        <th width="40%">Valor Predeterminado</th>
                                                        <th width="10%">
                                                            <i class="fa fa-cog"></i>
                                                        </th>
                                                    </tr>
                                                    </thead>
                                                    <tbody>
                                                    @forelse($modalidadesCompra as $modalidad)
                                                    <tr id="modalidad_{{ $modalidad->id }}">
                                                        <td width="10%">{{ $modalidad->id }}</td>
                                                        <td width="40%" class="text-center">{{ $modalidad->nombre }}</td>
                                                        <td width="40%">{{ $modalidad->predeterminado }}</td>
                                                        <td width="10%">
                                                            <div class="btn-group">
                                                                @permission(['ver-modalidad-compra'])
                                                                <button class="btn btn-success btn-xs" title="Ver Modalidad Compra" onclick="ver({{ $modalidad->id }});">
                                                                    <i class="fas fa-eye"></i>
                                                                </button>
                                                                @endpermission
                                                                @permission(['editar-modalidad-compra'])
                                                                <button class="btn btn-warning btn-xs" title="Editar Modalidad Compra" onclick="editar({{ $modalidad->id }});">
                                                                    <i class="fas fa-edit"></i>
                                                                </button>
                                                                @endpermission
                                                                @permission(['eliminar-modalidad-compra'])
                                                                <button class="btn btn-danger btn-xs" title="Eliminar Modalidad Compra" onclick="eliminar({{ $modalidad->id }});">
                                                                    <i class="fas fa-trash"></i>
                                                                </button>
                                                                @endpermission
                                                            </div>
                                                        </td>
                                                    </tr>
                                                    @empty

                                                    @endforelse
                                                    </tbody>
                                                </table>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
						</div>
					</div>
					<!-- END EXAMPLE TABLE PORTLET-->
				</div>
			</div>
			<!-- END PAGE CONTENT-->
			
			
			
			
		</div> <!--/div class="page-content" -->
	</div> <!--/div class="page-content-wrapper" -->
</div><!--/div class="page-content-wrapper" -->
<!-- END CONTENT -->
<div id="modal"></div>
<div id="modal2"></div>
@endsection

@push('script-footer')
<!-- BEGIN PAGE LEVEL PLUGINS -->
<script type="text/javascript" src="{{ asset('assets/plugins/select2/select2.min.js') }} "></script>
{{--<script type="text/javascript" src="{{ asset('assets/plugins/data-tables/jquery.dataTables.min.js') }}"></script>--}}
<script type="text/javascript" src="{{ asset('datatables/jquery.dataTables.min.js') }}"></script>
<script type="text/javascript" src="{{ asset('assets/plugins/data-tables/DT_bootstrap.js') }}"></script>

<link rel="stylesheet" type="text/css" href="{{asset('assets/plugins/bootstrap-datepicker/css/datepicker.css')}}"/>

<script type="text/javascript" src="{{asset('assets/plugins/bootstrap-datepicker/js/bootstrap-datepicker.js')}}"></script>
<script type="text/javascript" src="{{asset('assets/plugins/bootstrap-datetimepicker/js/bootstrap-datetimepicker.js')}}"></script>


<!-- END PAGE LEVEL PLUGINS -->
<!-- BEGIN PAGE LEVEL SCRIPTS -->
<script src="{{ asset('assets/scripts/app.js') }} "></script>
<script src="{{ asset('assets/scripts/table-advanced.js') }} "></script>


<script type="text/javascript">

    let tablaModalidades = $('#tabla_modalidades').DataTable({
        "language": {
            "emptyTable":     "No hay datos disponibles en la tabla",
            "info":           "Mostrando registros del _START_ al _END_ de un total de _TOTAL_ registros",
            "infoEmpty":      "Showing 0 to 0 of 0 entries",
            "infoFiltered":   "(filtrado de un total de _MAX_ registros)",
            "infoPostFix":    "",
            "thousands":      ",",
            "lengthMenu":     "Mostrar _MENU_ registros",
            "loadingRecords": "Cargando...",
            "processing":     "Procesando...",
            "search":         "Buscar: ",
            "zeroRecords":    "No matching records found",
            "paginate": {
                "first":      "Primero",
                "last":       "Último",
                "next":       "Siguiente",
                "previous":   "Anterior"
            },
            "aria": {
                "sortAscending":  ": Activar para ordenar la columna de manera ascendente",
                "sortDescending": ": Activar para ordenar la columna de manera descendente"
            }
        },
        "lengthMenu": [
            [10, 15, 20, -1],
            [10, 15, 20, "Todos"] // change per page values here
        ],
        // set the initial value
        "displayLength": 10,
        "paging":   true,
        "ordering": true,
        "info":     true,
        "searching": true
    });

    jQuery('#tabla_modalidades_filter input').addClass("form-control input-large"); // modify table search input
    jQuery('#tabla_modalidades_wrapper .dataTables_length select').addClass("form-control input-small"); // modify table per page dropdown
    jQuery('#tabla_modalidades_wrapper .dataTables_length select').select2(); // initialize select2 dropdown

    $('#tabla_modalidades_column_toggler input[type="checkbox"]').change(function() {
        /* Get the DataTables object again - this is not a recreation, just a get of the object */
        var iCol = parseInt($(this).attr("data-column"));
        var bVis = oTable.fnSettings().aoColumns[iCol].bVisible;
        oTable.fnSetColumnVis(iCol, (bVis ? false : true));
    });

	$(document).ready(function() {
		App.init(); // initlayout and core plugins
		//TableAdvanced.init();
        
   		$("#mantenedores").addClass( "active" );
		$("#modalidades-compra-li").addClass( "active" );
		$("#mantenedores-a").append( '<span class="selected"></span>' );

	});

    function ver(id)
    {
        $.get( '{{ url("modalidad_compra/modal/ver") }}/' + id, function( data ) {
            $( "#modal" ).html( data );
            $( "#modalVer" ).modal();
        });
    }

    function editar(id)
    {
        $.get( '{{ url("modalidad_compra/modal/editar") }}/' + id, function( data ) {
            $( "#modal" ).html( data );
            $( "#modalEditar" ).modal();
        });
    }

    function nuevo()
    {
        $.get( '{{ url("modalidad_compra/create") }}', function( data ) {
            $( "#modal" ).html( data );
            $( "#modalCrear" ).modal();
        });
    }

    function eliminar(id)
    {
        $.get( '{{ url("modalidad_compra/modal/eliminar") }}/' + id, function( data ) {
            $( "#modal" ).html( data );
            $( "#modalEliminar" ).modal();
        });
    }

</script>

@endpush