<link href="{{ asset('sistema/css/modales.css') }} " rel="stylesheet" type="text/css"/>
<div class="modal fade" id="modalEditarPerfil" role="dialog" aria-labelledby="myModalLabel3" aria-hidden="true">
	<div class="modal-dialog" style="width:1100px;">
		<div class="modal-content">
            <div class="modal-header">
				<button type="button" title="Cerrar" class="close" data-dismiss="modal" aria-hidden="true"></button>
				<h4 class="modal-title" ><strong><i class="fas fa-user-edit"></i>&nbsp;&nbsp;Editar Perfil</strong></h4>
            </div>

            <form action="{{ asset('usuarios/editar_perfil') }}" method="post" class="horizontal-form" id="form" autocomplete="off">
                <div class="modal-body">
                    <div class="form-body">
                        <div class="note note-danger" id="divErrores" style="display:none;">
                            <h4 class="block" style="margin-bottom:5 px;">Debe Completar los siguientes campos: </h4>
                            <ul id="ulErrores"></ul>
                        </div>

                        <div class="form-group row">
                            <label for="rut" class="col-sm-2 control-label label-form">Rut <span class="span-label">*</span></label>
                            <div class="col-sm-3 form-validate">
                                <input type="text" class="form-control" id="rut" name="rut" required value="{{ $usuario->formatRut() }}">
                            </div>
                        </div>

                        <div class="form-group row">
                            <label for="nombre" class="col-sm-2 control-label label-form">Nombre <span class="span-label">*</span></label>
                            <div class="col-sm-10 form-validate">
                                <input type="text" class="form-control" id="nombre" name="nombre" required value="{{ $usuario->name }}">
                            </div>
                        </div>

                        <div class="form-group row">
                            <label for="email" class="col-sm-2 control-label label-form">E-mail </label>
                            <div class="col-sm-4 form-validate">
                                <input type="email" class="form-control" id="email" name="email" value="{{ $usuario->email }}" >
                            </div>

                            <label for="iniciales" class="col-sm-2 control-label label-form">Iniciales </label>
                            <div class="col-sm-4 form-validate">
                                <input type="text" class="form-control" id="iniciales" name="iniciales" value="{{ $usuario->iniciales }}">
                            </div>
                        </div>

                        <div class="form-group row">
                            <label for="nueva_clave" class="col-sm-2 control-label label-form">Nueva Clave </label>
                            <div class="col-sm-6 form-validate">
                                <input type="password" class="form-control" id="nueva_clave" name="nueva_clave"
                                minlength="6" placeholder="Nueva Clave, debe tener un largo minímo de 6 caracteres">
                            </div>
                        </div>

                        <div class="form-group row">
                            <label for="perfil_sistema" class="col-sm-2 control-label label-form">Perfil Sistema </label>
                            <div class="col-sm-6 form-validate">
                                <input type="text" class="form-control" id="perfil_sistema" name="perfil_sistema" value="@if ( $usuario->roles ){{ $usuario->roles->first()['display_name'] }}@endif" readonly>
                            </div>
                        </div>

                    </div>
                </div>

                <div class="modal-footer form-actions right">
                    <input type="hidden" name="_token" value="{{ csrf_token() }}">
                    <input type="hidden" name="_id" value="{{ $usuario->id }}">

                    <button type="button" title="Cancelar" class="btn btn-default" data-dismiss="modal">Cancelar</button>
                    <button type="submit" title="Editar Perfil" class="btn btn-warning" id="botonGuardar"><i class="far fa-save" style="color:black;"></i> Editar Perfil</button>
                </div>
            </form>

        </div>
    </div>
</div>

<script src="{{asset('assets/plugins/jquery-rut/jquery.Rut.min.js')}}" type="text/javascript"></script>
<script>
    $(document).ready(function() {

        $(".select2").select2();
        
        $("#rut").Rut({
  			on_error: function(){
                toastr.error("El Rut ingresado no es valido", optionsToastr);
             },
		   format_on: 'keyup'
		});

        $("#form").validate({
            highlight: function(element) {
                $(element).closest('.form-validate').removeClass('has-success');
                $(element).closest('.form-validate').addClass('has-error');
            },
            unhighlight: function(element) {
                $(element).closest('.form-validate').removeClass('has-error');
                $(element).closest('.form-validate').addClass('has-success');
            },
            errorElement: 'span',
            errorClass: 'help-block',
            errorPlacement: function (error, element) {
                console.log(element);
                if (element.parent('.input-group').length) {
                    error.insertAfter(element.parent());
                } else {
                    error.insertAfter(element);
                }
            },
            rules: {
            },
            messages: {
            },

            //para enviar el formulario por ajax
            submitHandler: function(form) {
                esperandoEdicion();

                let formData = new FormData(form);
                formData.delete('rut');
                let rut = $('#rut').val().replace(/\./g, '');
                formData.append('rut',rut);

                $.ajax({
                    url: form.action,
                    type: form.method,
                    //data: $(form).serialize(),
                    data: formData,
                    processData: false,
                    contentType: false,
                    success: function(respuesta) {
                        console.log(respuesta);
                        
                        if ( respuesta.estado == 'error' ) {
                            toastr.error('No es posible realizar la acción'+'<br><strong>Error: '+ respuesta.mensaje +'</strong>', 'Atención', optionsToastr);
                        } else if ( respuesta.estado == 'success') {
                            
                            toastr.success(respuesta.mensaje, 'Atención', optionsToastr);
                            
                            $("#modalEditarPerfil").modal("hide");
                            
                        }
                        
                    }            
                }).fail( function(respuesta) {//fail ajax
                    if ( respuesta.status == 400 ) {
                        mostrarErroresValidator(respuesta);
                    } else if ( respuesta.status == 500 ) {
                        toastr.error('No es posible realizar la acción, error en el servidor', 'Atención', optionsToastr);
                    } else {
                        toastr.error('No es posible realizar la acción', 'Atención', optionsToastr);
                    }
                    
                })
                .always(function() {
                    listaEdicion();
                });//ajax
                
            }

        });

    });

    function esperandoEdicion()
    {   
        $('.page-header-fixed *').css('cursor', 'wait');
        $("#botonGuardar").attr("disabled",true);
    }

    function listaEdicion()
    {
        $('.page-header-fixed *').css('cursor', '');
        $("#botonGuardar").attr("disabled",false);
    }

    function mostrarErroresValidator(respuesta)
    {
        if ( respuesta.responseJSON ) {
            //console.log(respuesta.responseJSON);
            let htmlErrores = '';
            for (let k in respuesta.responseJSON) { 
                //console.log(k, respuesta.responseJSON[k]);
                htmlErrores += '<li>' + respuesta.responseJSON[k] + '</li>';
            }

            $('#ulErrores').html(htmlErrores);
            $('#divErrores').css('display','');
            toastr.error('No es posible realizar la acción'+'<br>'+'Errores:<br> <ul>'+htmlErrores+'</ul>', 'Atención', optionsToastr);
        }
    }

</script>