<link href="{{ asset('sistema/css/modales.css') }} " rel="stylesheet" type="text/css"/>
<div class="modal fade" id="modalEditar" role="dialog" aria-labelledby="myModalLabel3" aria-hidden="true">
	<div class="modal-dialog" style="width:1100px;">
		<div class="modal-content">
            <div class="modal-header">
				<button type="button" title="Cerrar" class="close" data-dismiss="modal" aria-hidden="true"></button>
				<h4 class="modal-title" ><strong><i class="fas fa-user-edit"></i>&nbsp;&nbsp;Editar Usuario</strong></h4>
            </div>

            <form action="{{ asset('usuarios/editar') }}" method="post" class="horizontal-form" id="form" autocomplete="off">
                <div class="modal-body">
                    <div class="form-body">
                        <div class="note note-danger" id="divErrores" style="display:none;">
                            <h4 class="block" style="margin-bottom:5 px;">Debe Completar los siguientes campos: </h4>
                            <ul id="ulErrores"></ul>
                        </div>

                        <div class="form-group row">
                            <label for="rut" class="col-sm-2 control-label label-form">Rut <span class="span-label">*</span></label>
                            <div class="col-sm-3 form-validate">
                                <input type="text" class="form-control" id="rut" name="rut" required value="{{ $usuario->formatRut() }}">
                            </div>
                        </div>

                        <div class="form-group row">
                            <label for="nombre" class="col-sm-2 control-label label-form">Nombre <span class="span-label">*</span></label>
                            <div class="col-sm-10 form-validate">
                                <input type="text" class="form-control" id="nombre" name="nombre" required value="{{ $usuario->name }}">
                            </div>
                        </div>

                        <div class="form-group row">
                            <label for="email" class="col-sm-2 control-label label-form">E-mail </label>
                            <div class="col-sm-4 form-validate">
                                <input type="email" class="form-control" id="email" name="email" value="{{ $usuario->email }}" >
                            </div>

                            <label for="iniciales" class="col-sm-2 control-label label-form">Iniciales </label>
                            <div class="col-sm-4 form-validate">
                                <input type="text" class="form-control" id="iniciales" name="iniciales" value="{{ $usuario->iniciales }}">
                            </div>
                        </div>

                        <div class="form-group row">
                            <label for="responsable" class="col-sm-2 control-label label-form">Responsable </label>
                            <div class="col-sm-4 form-validate">
                            <div class="checkbox check" >
                                <label class="label_checkbox">
                                    <input type="checkbox" class="check_input" id="responsable" name="responsable" value="1" @if($usuario->responsable == 1) checked @endif >
                                </label>
                                </div>
                            </div>

                            <label for="suspendido" class="col-sm-2 control-label label-form">Suspendido </label>
                            <div class="col-sm-4 form-validate">
                            <div class="checkbox check" >
                                <label class="label_checkbox">
                                    <input type="checkbox" class="check_input" id="suspendido" name="suspendido" value="1" @if($usuario->suspendido == 1) checked @endif >
                                </label>
                                </div>
                            </div>
                        </div>

                        <div class="form-group row">
                            <label for="perfil" class="col-sm-2 control-label label-form">Perfil <span class="span-label">*</span></label>
                            <div class="col-sm-4 form-validate">
                                <select name="perfil" id="perfil" class="form-control select2" required >
                                <option value="" >Seleccione</option>
                                @foreach ( $perfiles as $perfil)
                                    <option value="{{ $perfil->id }}" @if( $usuario->roles )@if( $perfil->id == $usuario->roles->first()['id'] ) selected @endif @endif >{{ $perfil->display_name }}</option>
                                @endforeach
                            </select>
                            </div>
                        </div>

                    </div>
                </div>

                <div class="modal-footer form-actions right">
                    <input type="hidden" name="_token" value="{{ csrf_token() }}">
                    <input type="hidden" name="_id" value="{{ $usuario->id }}">

                    <button type="button" title="Cancelar" class="btn btn-default" data-dismiss="modal">Cancelar</button>
                    <button type="submit" title="Editar Usuario" class="btn btn-warning" id="botonGuardar"><i class="far fa-save" style="color:black;"></i> Editar Usuario</button>
                </div>
            </form>

        </div>
    </div>
</div>

<script>
    $(document).ready(function() {

        $(".select2").select2();
        
        $("#rut").Rut({
  			on_error: function(){
                toastr.error("El Rut ingresado no es valido", optionsToastr);
             },
		   format_on: 'keyup'
		});

        $("#form").validate({
            highlight: function(element) {
                $(element).closest('.form-validate').removeClass('has-success');
                $(element).closest('.form-validate').addClass('has-error');
            },
            unhighlight: function(element) {
                $(element).closest('.form-validate').removeClass('has-error');
                $(element).closest('.form-validate').addClass('has-success');
            },
            errorElement: 'span',
            errorClass: 'help-block',
            errorPlacement: function (error, element) {
                console.log(element);
                if (element.parent('.input-group').length) {
                    error.insertAfter(element.parent());
                } else {
                    error.insertAfter(element);
                }
            },
            rules: {
            },
            messages: {
            },

            //para enviar el formulario por ajax
            submitHandler: function(form) {
                esperandoEdicion();

                let formData = new FormData(form);
                formData.delete('rut');
                let rut = $('#rut').val().replace(/\./g, '');
                formData.append('rut',rut);

                $.ajax({
                    url: form.action,
                    type: form.method,
                    //data: $(form).serialize(),
                    data: formData,
                    processData: false,
                    contentType: false,
                    success: function(respuesta) {
                        console.log(respuesta);
                        if ( respuesta.estado == 'error' ) {
                            toastr.error('No es posible realizar la acción'+'<br><strong>Error: '+ respuesta.mensaje +'</strong>', 'Atención', optionsToastr);
                        } else if ( respuesta.estado == 'success') {
                            
                            toastr.success(respuesta.mensaje, 'Atención', optionsToastr);
                            // Cambiar la informacion de la fila de la tabla principal
                            editarFilaTabla(respuesta);
                            $("#modalEditar").modal("hide");
                            
                        }
                        
                    }            
                }).fail( function(respuesta) {//fail ajax
                    if ( respuesta.status == 400 ) {
                        mostrarErroresValidator(respuesta);
                    } else if ( respuesta.status == 500 ) {
                        toastr.error('No es posible realizar la acción, error en el servidor', 'Atención', optionsToastr);
                    } else {
                        toastr.error('No es posible realizar la acción', 'Atención', optionsToastr);
                    }
                    
                })
                .always(function() {
                    listaEdicion();
                });//ajax
                
            }

        });

    });

    function esperandoEdicion()
    {   
        $('.page-header-fixed *').css('cursor', 'wait');
        $("#botonGuardar").attr("disabled",true);
    }

    function listaEdicion()
    {
        $('.page-header-fixed *').css('cursor', '');
        $("#botonGuardar").attr("disabled",false);
    }

    function mostrarErroresValidator(respuesta)
    {
        if ( respuesta.responseJSON ) {
            //console.log(respuesta.responseJSON);
            let htmlErrores = '';
            for (let k in respuesta.responseJSON) { 
                //console.log(k, respuesta.responseJSON[k]);
                htmlErrores += '<li>' + respuesta.responseJSON[k] + '</li>';
            }

            $('#ulErrores').html(htmlErrores);
            $('#divErrores').css('display','');
            toastr.error('No es posible realizar la acción'+'<br>'+'Errores:<br> <ul>'+htmlErrores+'</ul>', 'Atención', optionsToastr);
        }
    }

    function editarFilaTabla(respuesta)
    {

        let botones = '<td width="10%"><div class="btn-group">';
        @permission(['ver-user'])
        botones += '<button class="btn btn-success btn-xs" title="Ver Usuario" onclick="ver('+ respuesta.id +');">';
        botones += '<i class="fas fa-eye"></i></button>';
        @endpermission
        @permission(['editar-user'])
        botones += '<button class="btn btn-warning btn-xs" title="Editar Usuario" onclick="editar('+ respuesta.id +');">';
        botones += '<i class="fas fa-user-edit"></i></button>';
        @endpermission
        @permission(['eliminar-user'])
        botones += '<button class="btn btn-danger btn-xs" title="Eliminar Usuario" onclick="eliminar('+ respuesta.id +');">';
        botones += '<i class="fas fa-user-times"></i></button>';
        @endpermission
        botones += '</div></td>';

        tablaPrincipal.row('#tr_'+ respuesta.id).data([
            respuesta.id,
            respuesta.nombre,
            respuesta.rut,
            respuesta.perfil,
            respuesta.iniciales,
            respuesta.email,
            respuesta.suspendido,
            respuesta.responsable,
            botones
        ]).draw();
    }

</script>