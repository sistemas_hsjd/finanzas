@extends('master')

@section('title', 'Mantenedor de Usuarios')

@push('css-head')
<!-- BEGIN PAGE LEVEL STYLES -->
<link rel="stylesheet" type="text/css" href="{{asset('assets/plugins/select2/select2_conquer.css')}}"/>
<link rel="stylesheet" href="{{asset('assets/plugins/data-tables/DT_bootstrap.css')}}"/>
<!-- END PAGE LEVEL STYLES -->

@endpush

@section('content')

<!-- BEGIN CONTENT -->

<div class="page-content-wrapper">
	<div class="page-content-wrapper">
		<div class="page-content">

			<!-- BEGIN PAGE CONTENT-->
			<div class="row">
				<div class="col-md-12">
					<!-- BEGIN EXAMPLE TABLE PORTLET-->
					<div class="portlet">
						<div class="portlet-title">
							<div class="caption">
								<i class="fas fa-users"></i> Mantenedor de Usuarios
							</div>
						</div>
						<div class="portlet-body">
                            <div class="table-toolbar">
                                <div class="btn-group">
                                    @permission(['crear-user'])
                                    <button title="Agregar Usuario" class="btn btn-success" onclick="nuevo();">
										<i class="fas fa-user-plus"></i> Agregar Usuario
									</button>
                                    @endpermission
                                </div>
                            </div>
                            
                            <div class="row">
                                <div class="col-md-12">
                                    <div class="panel">
                                    {{-- 
                                        <div class="panel-heading">
                                            <div class="caption">
                                                <strong style="color: #69aa46;">Últimos Documentos Ingresados</strong>
                                            </div>
                                        </div>
                                        --}}
                                        <div class="panel-body">
                                            <div class="table-responsive">
                                                <table class="table table-striped table-bordered table-hover table-full-width" id="tabla_principal">
                                                    <thead>
                                                    <tr>
                                                        <th>ID</th>
                                                        <th >Nombre</th>
                                                        <th>Rut</th>
                                                        <th>Perfil</th>
                                                        <th>Iniciales</th>
                                                        <th>E-mail</th>
                                                        <th>Suspendido</th>
                                                        <th>Responsable</th>
                                                        <th class="text-center">
                                                            <i class="fas fa-users-cog fa-lg"></i>
                                                        </th>
                                                    </tr>
                                                    </thead>
                                                    <tbody>
                                                    @forelse($usuarios as $usuario)
                                                    <tr id="tr_{{ $usuario->id }}">
                                                        <td >{{ $usuario->id }}</td>
                                                        <td >{{ $usuario->name }}</td>
                                                        <td>{{ $usuario->formatRut() }}</td>
                                                        <td>@if ( $usuario->roles ){{ $usuario->roles->first()['display_name'] }}@endif</td>
                                                        <td>{{ $usuario->iniciales }}</td>
                                                        <td>{{ $usuario->email }}</td>
                                                        <td>{{ $usuario->suspendido() }}</td>
                                                        <td>{{ $usuario->responsable() }}</td>
                                                        <td >
                                                            <div class="btn-group">
                                                                @permission(['ver-user'])
                                                                <button class="btn btn-success btn-xs" title="Ver Usuario" onclick="ver({{ $usuario->id }});">
                                                                    <i class="fas fa-eye"></i>
                                                                </button>
                                                                @endpermission
                                                                @permission(['editar-user'])
                                                                <button class="btn btn-warning btn-xs" title="Editar Usuario" onclick="editar({{ $usuario->id }});">
                                                                    <i class="fas fa-user-edit"></i>
                                                                </button>
                                                                @endpermission
                                                                @permission(['eliminar-user'])
                                                                @if( $usuario->id != 85 && $usuario->id != 1 )
                                                                    <button class="btn btn-danger btn-xs" title="Eliminar Usuario" onclick="eliminar({{ $usuario->id }});">
                                                                        <i class="fas fa-user-times"></i>
                                                                    </button>
                                                                @endif
                                                                @endpermission
                                                            </div>
                                                        </td>
                                                    </tr>
                                                    @empty

                                                    @endforelse
                                                    </tbody>
                                                </table>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
						</div>
					</div>
					<!-- END EXAMPLE TABLE PORTLET-->
				</div>
			</div>
			<!-- END PAGE CONTENT-->
			
			
			
			
		</div> <!--/div class="page-content" -->
	</div> <!--/div class="page-content-wrapper" -->
</div><!--/div class="page-content-wrapper" -->
<!-- END CONTENT -->
<div id="modal"></div>
<div id="modal2"></div>
@endsection

@push('script-footer')
<script src="{{asset('assets/plugins/jquery-rut/jquery.Rut.min.js')}}" type="text/javascript"></script>
<!-- BEGIN PAGE LEVEL PLUGINS -->
<script type="text/javascript" src="{{ asset('assets/plugins/select2/select2.min.js') }} "></script>
{{--<script type="text/javascript" src="{{ asset('assets/plugins/data-tables/jquery.dataTables.min.js') }}"></script>--}}
<script type="text/javascript" src="{{ asset('datatables/jquery.dataTables.min.js') }}"></script>
<script type="text/javascript" src="{{ asset('assets/plugins/data-tables/DT_bootstrap.js') }}"></script>

<link rel="stylesheet" type="text/css" href="{{asset('assets/plugins/bootstrap-datepicker/css/datepicker.css')}}"/>

<script type="text/javascript" src="{{asset('assets/plugins/bootstrap-datepicker/js/bootstrap-datepicker.js')}}"></script>
<script type="text/javascript" src="{{asset('assets/plugins/bootstrap-datetimepicker/js/bootstrap-datetimepicker.js')}}"></script>


<!-- END PAGE LEVEL PLUGINS -->
<!-- BEGIN PAGE LEVEL SCRIPTS -->
<script src="{{ asset('assets/scripts/app.js') }} "></script>

<script type="text/javascript">

    let tablaPrincipal = $('#tabla_principal').DataTable({
        "language": {
            "emptyTable":     "No hay datos disponibles en la tabla",
            "info":           "Mostrando registros del _START_ al _END_ de un total de _TOTAL_ registros",
            "infoEmpty":      "Showing 0 to 0 of 0 entries",
            "infoFiltered":   "(filtrado de un total de _MAX_ registros)",
            "infoPostFix":    "",
            "thousands":      ",",
            "lengthMenu":     "Mostrar _MENU_ registros",
            "loadingRecords": "Cargando...",
            "processing":     "Procesando...",
            "search":         "Buscar: ",
            "zeroRecords":    "No matching records found",
            "paginate": {
                "first":      "Primero",
                "last":       "Último",
                "next":       "Siguiente",
                "previous":   "Anterior"
            },
            "aria": {
                "sortAscending":  ": Activar para ordenar la columna de manera ascendente",
                "sortDescending": ": Activar para ordenar la columna de manera descendente"
            }
        },
        "lengthMenu": [
            [10, 15, 20, -1],
            [10, 15, 20, "Todos"] // change per page values here
        ],
        // set the initial value
        "displayLength": 10,
        "paging":   true,
        "ordering": true,
        "info":     true,
        "searching": true
    });

    jQuery('#tabla_principal_filter input').addClass("form-control input-large"); // modify table search input
    jQuery('#tabla_principal_wrapper .dataTables_length select').addClass("form-control input-small"); // modify table per page dropdown
    jQuery('#tabla_principal_wrapper .dataTables_length select').select2(); // initialize select2 dropdown

    $('#tabla_principal_column_toggler input[type="checkbox"]').change(function() {
        /* Get the DataTables object again - this is not a recreation, just a get of the object */
        var iCol = parseInt($(this).attr("data-column"));
        var bVis = oTable.fnSettings().aoColumns[iCol].bVisible;
        oTable.fnSetColumnVis(iCol, (bVis ? false : true));
    });

	$(document).ready(function() {
		App.init(); // initlayout and core plugins
		//TableAdvanced.init();
        
   		$("#mantenedores").addClass( "active" );
		$("#usuarios-li").addClass( "active" );
		$("#mantenedores-a").append( '<span class="selected"></span>' );

	});

    function ver(id)
    {
        $.get( '{{ url("usuarios/modal/ver") }}/' + id, function( data ) {
            $( "#modal" ).html( data );
            $( "#modalVer" ).modal();
        });
    }

    function editar(id)
    {
        $.get( '{{ url("usuarios/modal/editar") }}/' + id, function( data ) {
            $( "#modal" ).html( data );
            $( "#modalEditar" ).modal();
        });
    }

    function nuevo()
    {
        $.get( '{{ url("usuarios/create") }}', function( data ) {
            $( "#modal" ).html( data );
            $( "#modalCrear" ).modal();
        });
    }

    function eliminar(id)
    {
        $.get( '{{ url("usuarios/modal/eliminar") }}/' + id, function( data ) {
            $( "#modal" ).html( data );
            $( "#modalEliminar" ).modal();
        });
    }

</script>

@endpush