function esperandoCrear()
{   
    $('.page-header-fixed *').css('cursor', 'wait');
    $("#botonGuardar").attr("disabled",true);
}

function listoCrear()
{ 
    $('.page-header-fixed *').css('cursor', '');
    $("#botonGuardar").attr("disabled",false);
}

function mostrarErroresValidator(respuesta)
{
    if ( respuesta.responseJSON ) {
        //console.log(respuesta.responseJSON);
        let htmlErrores = '';
        for (let k in respuesta.responseJSON) { 
            //console.log(k, respuesta.responseJSON[k]);
            htmlErrores += '<li>' + respuesta.responseJSON[k] + '</li>';
        }

        $('#ulErrores').html(htmlErrores);
        $('#divErrores').css('display','');
        toastr.error('No es posible realizar la acción'+'<br>'+'Errores:<br> <ul>'+htmlErrores+'</ul>', 'Atención', optionsToastr);
    }
}

/**
 * Da formato a los valores ingresados en los input que sean de dinero
 */
function ingresoPesos(input) {

    let valorIngresado = parseInt($('#' + input.id).val().replace(/\./g, ''));

    if ( montoMaximoOc != 'Sin Contrato' ) {
        
        if ( valorIngresado > parseInt(montoMaximoOc) ) {

            $("#" + input.id).val(formatMoney( montoMaximoOc ) );
            toastr.warning('<strong style="color:black;">el monto OC no puede superar la sumatoria de los montos de las OC del contrato</strong>', 'Atención', optionsToastr);

        } else if ( valorIngresado < parseInt(montoMinimoOc) ) {

            $("#" + input.id).val(formatMoney( montoMinimoOc ) );
            toastr.warning('<strong style="color:black;">el monto OC no puede ser menor a lo consumido por los documentos</strong>', 'Atención', optionsToastr);

        } else {

            if ( isNaN(valorIngresado) ) {

                $("#" + input.id).val(formatMoney( montoMinimoOc ) );
                if ( parseInt(montoMinimoOc) > 0 ) {
                    toastr.warning('<strong style="color:black;">el monto OC no puede ser menor a lo consumido por los documentos</strong>', 'Atención', optionsToastr);
                }
                
            } else {

                $("#" + input.id).val(formatMoney(($("#" + input.id).val()).replace(/\./g, '')));

            }

        }

    } else if ( valorIngresado < parseInt(montoMinimoOc) ) {

        $("#" + input.id).val(formatMoney( montoMinimoOc ) );
        toastr.warning('<strong style="color:black;">el monto OC no puede ser menor a lo consumido por los documentos</strong>', 'Atención', optionsToastr);

    } else {

        if ( isNaN(valorIngresado) ) {

            $("#" + input.id).val(formatMoney( montoMinimoOc ) );
            if ( parseInt(montoMinimoOc) > 0 ) {
                toastr.warning('<strong style="color:black;">el monto OC no puede ser menor a lo consumido por los documentos</strong>', 'Atención', optionsToastr);
            }
            
        } else {

            $("#" + input.id).val(formatMoney(($("#" + input.id).val()).replace(/\./g, '')));

        }

    }
    
}

/**
 * Valida el archivo seleccionado para subir
 */
function validarArchivo() {

    let archivo = $('#archivo').val().replace(/.*(\/|\\)/, ''); //val();
    let extension = archivo.substring(archivo.lastIndexOf('.'));
    if (archivo == '') {
        $('#div_archivo').css('display', 'none');
        tablaArchivo.clear().draw(); // se remueven todas las filas de la tabla
        return false;
    } else {
        if (extension != '.pdf') {
            toastr.error('El archivo no tiene la extension correcta: ' + '<strong>PDF</strong>', 'Atención', optionsToastr);
            $('#archivo').val('');
            $('#div_archivo').css('display', 'none');
            tablaArchivo.clear().draw(); // se remueven todas las filas de la tabla
            return false;
        } else {
            mostrarInfoArchivo(archivo);
            return true;
        }
    }

}

/**
 * Muestra la informacion del archivo seleccionado para subir
 */
function mostrarInfoArchivo(archivo) {

    tablaArchivo.clear().draw(); // se remueven todas las filas de la tabla
    let nombreArchivo = archivo.substring(0, archivo.lastIndexOf('.'));
    let tamañoArchivo = $('#archivo')[0].files[0].size;

    tablaArchivo.row.add([
        nombreArchivo,
        tamañoArchivo + " bits",
        selectTipoArchivo
    ]).draw(false);

    $('#div_archivo').css('display', '');

}

/**
 * obtiene los contratos del proveedor seleccionado
 */
function getProveedor(input) {
    console.log('busca contratos');
    if (input.value != '') {

        $.ajaxSetup({
            headers: { 'X-CSRF-TOKEN': '{{ csrf_token() }}' }
        });

        $.ajax({
            url: urlGetContratos + '/' + input.value,
            type: 'get',

            success: function(respuesta) {

                    if ( respuesta.estado == 'success' ) {

                        if ( respuesta.cantidadContratos > 0 ) {

                            let html = '<select name="contrato" id="contrato" class="form-control select2" onchange="setContrato(this);" >';
                            html += '<option value="">Seleccione</option>';
                            html += respuesta.options;
                            html += '</select>';
                            $("#setear_select_contrato").html(html);
                            $('#contrato').select2();
                            $('#div_seleccion_contrato').css('display', '');
                            toastr.info(respuesta.mensaje , "Atención", optionsToastr);

                        } else {

                            toastr.info(respuesta.mensaje , "Atención", optionsToastr);

                        }

                        $('#proveedor').select2("readonly", true);

                    } else {

                        toastr.warning(respuesta.mensaje , "Atención", optionsToastr);
                        $('#div_seleccion_contrato').css('display', 'none');
                        $('#proveedor').select2("readonly", false);

                    }
                    
            } // succes

        }).fail(function() {
            toastr.warning("No es posible buscar datos, intentelo más tarde", "Atención", optionsToastr);
            $('#div_seleccion_contrato').css('display', 'none');
            $('#proveedor').select2("readonly", false);
        });

    } else {
        $('#div_seleccion_contrato').css('display', 'none');
        $('#proveedor').select2("readonly", false);
    }

}

function setContrato(input) {

    console.log('seleccionando contrato');

    if (input.value != '') {

        var contrato = document.getElementById('contrato_seleccionado_' + input.value);
        montoMaximoOc = contrato.dataset.montomaximooc;
        console.log('contrato : ');
        console.log(contrato);

        $('#monto_oc').val(formatMoney( ( montoMaximoOc ).replace(/\./g, '')));

        let inicioVigencia ='<input type="text" class="form-control fechas" id="inicio_vigencia" name="inicio_vigencia" required value="' + contrato.dataset.iniciovigenciacontrato + '"  >';
        inicioVigencia += '<span class="input-group-addon" onclick="fecha(\'inicio_vigencia\');" style="cursor:pointer;"><i class="fa fa-calendar" aria-hidden="true" style="color: black;"></i></span>';

        $('#div_inicio_vigencia').html(inicioVigencia);

        $('#inicio_vigencia').datepicker({
            format: 'dd/mm/yyyy',
            // endDate: new Date(),
            startDate: contrato.dataset.iniciovigenciacontrato,
            endDate: contrato.dataset.finvigenciacontrato,
            autoclose: true,
            language: 'es'
        });


        let finVigencia = '<input type="text" class="form-control fechas" id="fin_vigencia" name="fin_vigencia" required value="' + contrato.dataset.finvigenciacontrato + '"  >';
        finVigencia += '<span class="input-group-addon" onclick="fecha(\'fin_vigencia\');" style="cursor:pointer;"><i class="fa fa-calendar" aria-hidden="true" style="color: black;"></i></span>';

        $('#div_fin_vigencia').html(finVigencia);

        $('#fin_vigencia').datepicker({
            format: 'dd/mm/yyyy',
            // endDate: new Date(),
            startDate: contrato.dataset.iniciovigenciacontrato,
            endDate: contrato.dataset.finvigenciacontrato,
            autoclose: true,
            language: 'es'
        });

    } else {

        montoMaximoOc = 'Sin Contrato';
        $('#monto_oc').val(0);

        let inicioVigencia ='<input type="text" class="form-control fechas" id="inicio_vigencia" name="inicio_vigencia" required value="' + fechaActual + '" >';
        inicioVigencia += '<span class="input-group-addon" onclick="fecha(\'inicio_vigencia\');" style="cursor:pointer;"><i class="fa fa-calendar" aria-hidden="true" style="color: black;"></i></span>';

        $('#div_inicio_vigencia').html(inicioVigencia);

        $('#inicio_vigencia').datepicker({
            format: 'dd/mm/yyyy',
            autoclose: true,
            language: 'es'
        });


        let finVigencia = '<input type="text" class="form-control fechas" id="fin_vigencia" name="fin_vigencia" required value="' + fechaActual + '" >';
        finVigencia += '<span class="input-group-addon" onclick="fecha(\'fin_vigencia\');" style="cursor:pointer;"><i class="fa fa-calendar" aria-hidden="true" style="color: black;"></i></span>';

        $('#div_fin_vigencia').html(finVigencia);

        $('#fin_vigencia').datepicker({
            format: 'dd/mm/yyyy',
            autoclose: true,
            language: 'es'
        });

    }

}