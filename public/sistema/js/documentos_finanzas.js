// Variables globales, para utilizar en distintas funciones.
if ( ! numeroDocumentoRefacturacion ) {
    var numeroDocumentoRefacturacion = 0;
}

var numeroDocumentoRecepcion = 0;

if ( ! montoMaximoNc ) {
    var montoMaximoNc = false;
}

var añoEmision = 2020;

/**
 * Valida el numero de docuento y el numero de licitacion.
 * depende de la modalidad de compra seleccionada
 */
function validaNumDocumentoAndLicitacion() {

    let idModalidadCompra = parseInt($('#modalidad_compra').val());
    if (idModalidadCompra == 1) {
        if ($('#numero_orden_compra').val().indexOf('1641-') == -1) {

            $('#numero_orden_compra').val('1641-' + $('#numero_orden_compra').val());
            toastr.warning('El "N° Orden de Compra" debe iniciar con "1641-"', 'Atención', optionsToastr);
            return false;

        }
    }

    if ($('#numero_licitacion').val() != '') {
        if ($('#numero_licitacion').val().indexOf('1641-') == -1) {

            $('#numero_licitacion').val('1641-' + $('#numero_licitacion').val());
            toastr.warning('El "N° Licitación" debe iniciar con "1641-"', 'Atención', optionsToastr);
            return false;

        }
    }

    return true;

}

/**
 * Valida que el valor total del documento este distribuido en todos los items
 * presupuestarios añadidos al documento.
 */
function validaTotalDocumentoConItemsPresupuestario() {

    // si encuentra el item 34, pasa la validación no lo considera al sumar los items
    if (arregloItems.length == 0) {

        toastr.error('Debe agregar los ítems presupuestarios', 'Atención', optionsToastr);
        return false;

    } else {

        let totalDocumento = parseInt($('#valor_total_documento').val().replace(/\./g, ''));

        let totalItemsPresupuestarios = 0;
        arregloItems.forEach(function(elemento, indice, array) {
            if ( elemento !== 135 ) { // 135, id item 34
                totalItemsPresupuestarios += parseInt($('#valor_item_' + elemento).val().replace(/\./g, ''));
            }
        });

        if (totalDocumento == totalItemsPresupuestarios) {
            return true;
        } else {
            toastr.error('La suma de todos los ítem presupuestarios debe ser igual que el total de la factura ingresado en <br><strong style="color:black;">Valor total Documento</strong>', 'Atención', optionsToastr);
            return false;
        }
    }
    
}

/**
 * Muestra los errores del validator de laravel
 */
function mostrarErroresValidator(respuesta) {

    if (respuesta.responseJSON) {
        let htmlErrores = '';
        for (let k in respuesta.responseJSON) {
            htmlErrores += '<li>' + respuesta.responseJSON[k] + '</li>';
        }

        $('#ulErrores').html(htmlErrores);
        $('#divErrores').css('display', '');
        toastr.error('No es posible realizar la acción' + '<br>' + 'Errores:<br> <ul>' + htmlErrores + '</ul>', 'Atención', optionsToastr);

    }
}

/**
 * Desactiva boton para guardar los cambios
 */
function esperandoIngresoDocumento() {
    $('.page-header-fixed *').css('cursor', 'wait');
    $("#botonGuardar").attr("disabled", true);
}

/**
 * Habilita boton para guardar los cambios
 */
function listoIngresoDocumento() {
    $('.page-header-fixed *').css('cursor', '');
    $("#botonGuardar").attr("disabled", false);
}

/**
 * obtiene el proveedor seleccionado, sirve para seleccionarlo en el otro select
 */
function getProveedor(input) {

    getDatosContrato();
    getDatosOc();
    if (input.value != '') {
        // $.ajaxSetup({
        //     headers: { 'X-CSRF-TOKEN': '{{ csrf_token() }}' }
        // });

        // $.ajax({
        //     url: urlGetProveedor + '/' + input.value,
        //     type: 'get',

        //     success: function(proveedor) {

        //             $('#tipo_documento').select2('val', '').trigger('change');
        //             getContratosProveedor(input.value);
        //             getOrdenesCompraProveedor(input.value);

        //         } // succes

        // }).fail(function() {
        //     toastr.warning("No es posible buscar datos, intentelo más tarde", "Atención", optionsToastr);
        // });

        $('#tipo_documento').select2('val', '').trigger('change');
        getContratosProveedor(input.value);
        getOrdenesCompraProveedor(input.value);

    } else {

        $("#nombre_proveedor").select2("data", { id: '', text: '' });
        
    }

}


/**
 * Al apretar los calendarios al costado del input
 * muestra calendario para seleccionar fecha
 */
function fechaDocumento(id_fecha) {
    $('#' + id_fecha).datepicker("show");
}

/**
 * al cambiar la fecha documento se valida que sea mayor que la fecha de recepcion
 */
function cambioFechaDocumento(input) {

    //se obtienen las fechas ingresadas en formato para poder comparar
    var fecha_aux = $("#fecha_recepcion").val().split("/");
    var fecha_recepcion = new Date(parseInt(fecha_aux[2]), parseInt(fecha_aux[1] - 1), parseInt(fecha_aux[0]));

    var fecha_aux2 = input.value.split("/");
    var fecha_documento = new Date(parseInt(fecha_aux2[2]), parseInt(fecha_aux2[1] - 1), parseInt(fecha_aux2[0]));

    if (fecha_recepcion < fecha_documento) {
        toastr.info("La Fecha Recepción no puede ser anterior a la Fecha Documento", "Atención", optionsToastr);
        $("#fecha_recepcion").val($("#fecha_documento").val());
        return false;
    }

    añoEmision = parseInt(fecha_aux2[2]);
    setLabelImpuestoBoletas( añoEmision );    
    setMontosParaBoleta();
}

/**
 * Al cambiar la fecha de recepcion se valida que sea menor que la fecha de documento
 */
function cambioFechaRecepcion(input) {

    //se obtienen las fechas ingresadas en formato para poder comparar
    var fecha_aux = $("#fecha_documento").val().split("/");
    var fecha_documento = new Date(parseInt(fecha_aux[2]), parseInt(fecha_aux[1] - 1), parseInt(fecha_aux[0]));

    var fecha_aux2 = input.value.split("/");
    var fecha_recepcion = new Date(parseInt(fecha_aux2[2]), parseInt(fecha_aux2[1] - 1), parseInt(fecha_aux2[0]));

    if (fecha_documento > fecha_recepcion) {
        toastr.info("La Fecha Recepción no puede ser anterior a la Fecha Documento", "Atención", optionsToastr);
        $("#fecha_recepcion").val($("#fecha_documento").val());
        return false;
    }
}

/**
 * Valida el monto del item.
 * depende del valor total y de la suma de valores de los items
 */
function validarMontoItem(input) {
    // Se obtiene el número del item presupuestario, el id
    let itemPresupuestario = input.id.match(/[0-9]+/g);
    let totalDocumento = parseInt($('#valor_total_documento').val().replace(/\./g, ''));
    let totalItemsPresupuestarios = 0;

    arregloItems.forEach(function(elemento, indice, array) {
        if (elemento != itemPresupuestario) {
            totalItemsPresupuestarios += parseInt($('#valor_item_' + elemento).val().replace(/\./g, ''));
        }
    });

    let diferenciaTotales = totalDocumento - totalItemsPresupuestarios;
    let valorIngresado = parseInt($('#' + input.id).val().replace(/\./g, ''));

    if (valorIngresado > diferenciaTotales) {
        toastr.warning('La suma de todos los ítem presupuestarios no puede superar el total del documento ingresado en <br><strong style="color:black;">Valor total Documento</strong>', 'Atención', optionsToastr);
        $("#" + input.id).val(formatMoney(diferenciaTotales));
    } else {
        $("#" + input.id).val(formatMoney(($("#" + input.id).val()).replace(/\./g, '')));
    }
}

/**
 * Da formato a los valores ingresados en los input que sean de dinero
 */
function ingresoPesos(input) {
    // console.log("soy tu ingresoPesos : "+input.id);
    if (input.id.indexOf('valor_item_') == 0) {
        validarMontoItem(input);
    } else {
        // es el input de valor_total_documento
        $("#" + input.id).val(formatMoney(($("#" + input.id).val()).replace(/\./g, '')));
        if ( $('#valor_total_documento_actualizado').length ) {
            modificaValorTotalDocumentoActualizado();
        }

        let idTipoDocumento = parseInt($('#tipo_documento').val());
        if ( idTipoDocumento == 3) {
            setMontosParaBoleta();
        }
        
        evaluarMaximoNC();

        evaluarValorTotalDocumentoConSaldoOcContrato();
    }
}

/**
 * Evaluar el valor total, comparado con el monto maximoNC.
 * Solo si el tipo de documento es NC
 */
function evaluarMaximoNC()
{
    if ( $('#tipo_documento').val() == 4 || $('#tipo_documento').val() == 10 ) {
        let totalDocumento = parseInt($('#valor_total_documento').val().replace(/\./g, ''));
        if ( (montoMaximoNc - totalDocumento) < 0 ) {
            $('#valor_total_documento').val(formatMoney(montoMaximoNc));
            toastr.warning('El Valor Total no puede superar el valor total actualizado de la factura seleccionada', 'Atención', optionsToastr);
        }
    }
}

/**
 * modifica el valvalor_total_documento_actualizado.
 * utiliza el monto_aplicado y el valor_total_documento
 * Es para hacer el match con las recepciones
 */
function modificaValorTotalDocumentoActualizado() {
    let totalDocumento = parseInt($('#valor_total_documento').val().replace(/\./g, ''));
    let totalMontoNcNd = 0;
    if ( $('#monto_aplicado').length ) {
        totalMontoNcNd = parseInt($('#monto_aplicado').val().replace(/\./g, ''));
    }
    
    // console.log('totalDocumento : ' + totalDocumento);
    // console.log('totalMontoNcNd : ' + totalMontoNcNd);
    let totalDocumentoActualizado = totalDocumento + totalMontoNcNd;
    // console.log('totalDocumentoActualizado : ' + totalDocumentoActualizado);
    $('#valor_total_documento_actualizado').val(formatMoney(totalDocumentoActualizado));
}

/**
 * Valida el archivo seleccionado para subir
 */
function validarArchivo() {

    let archivo = $('#archivo').val().replace(/.*(\/|\\)/, ''); //val();
    let extension = archivo.substring(archivo.lastIndexOf('.'));
    if (archivo == '') {
        $('#div_archivo').css('display', 'none');
        tablaArchivo.clear().draw(); // se remueven todas las filas de la tabla
        return false;
    } else {
        if (extension != '.pdf') {
            toastr.error('El archivo no tiene la extension correcta: ' + '<strong>PDF</strong>', 'Atención', optionsToastr);
            $('#archivo').val('');
            $('#div_archivo').css('display', 'none');
            tablaArchivo.clear().draw(); // se remueven todas las filas de la tabla
            return false;
        } else {
            mostrarInfoArchivo(archivo);
            return true;
        }
    }
}

/**
 * Muestra la informacion del archivo seleccionado para subir
 */
function mostrarInfoArchivo(archivo) {
    tablaArchivo.clear().draw(); // se remueven todas las filas de la tabla
    let nombreArchivo = archivo.substring(0, archivo.lastIndexOf('.'));
    let tamañoArchivo = $('#archivo')[0].files[0].size;

    tablaArchivo.row.add([
        nombreArchivo,
        tamañoArchivo + " bits",
        selectTipoArchivo
    ]).draw(false);

    $('#div_archivo').css('display', '');
}

/**
 * Muestra modal para agregar item.
 * Se utiliza el arregloItem para no buscar los items ya seleccionados.
 */
function agregarItem() {
    let arregloItemsJSON = JSON.stringify(arregloItems);
    var selectDocBodega = document.getElementById('doc_disponible_bodega'); // toma el documento de recepcion
    // console.log('selectDocBodega es : ' + selectDocBodega);
    // console.log(selectDocBodega);
    // console.dir(selectDocBodega);
    var idDocBodega = null;
    if (selectDocBodega != null && selectDocBodega.value != '') {
        var idDocBodega = selectDocBodega.value;
    }

    // console.log('2 idDocBodega es : ' + idDocBodega);
    $.get(urlAgregarItem + "/" + arregloItemsJSON + "/" + idDocBodega, function(data) {
        $("#modal2").html(data);
        $("#modalAgregarItemPresupuestario").modal();
    });
}

/**
 * agrega una fila a la tabla de item presupuestarios.
 * Se agrega el item seleccionado en la modal.
 */
function nuevaFilaItemsPresupuestarios(respuesta) {

    let valorTotalDocumento = '';
    if (arregloItems.length == 0) {
        // si es 0, es porque es el primer item y debe tener el valor del documento.
        valorTotalDocumento += ' value="' + $('#valor_total_documento').val() + '" ';
    }

    // Se agrega el id del item al arreglo, para no mostrarlo en la modal.
    arregloItems.push(respuesta.id_item);

    let inputValor = '<div class="input-group form-validate">';
    inputValor += '<span class="input-group-addon">$</span>';
    inputValor += '<input type="text" class="form-control numero_documento" ' + valorTotalDocumento + ' id="valor_item_' + respuesta.id_item + '" name="valor_item[' + respuesta.id_item + ']" required onkeyup="ingresoPesos(this);">';
    inputValor += '</div>';

    let botonEliminar = '<div class="btn-group">';
    botonEliminar += '<button type="button" class="btn btn-danger btn-xs" title="Eliminar Item" onclick="eliminarItem(' + respuesta.id_item + ');">';
    botonEliminar += '<i class="fa fa-trash"></i> Eliminar'
    botonEliminar += '</button></div>';

    let rowNode = tablaItemsPresupuestarios.row.add([
        respuesta.codigo,
        respuesta.clasificacionPresupuestario + ' <span class="span-label">*</span>',
        inputValor,
        botonEliminar
    ]).draw(false).node();

    // Se añaden clases para dar formato a la nueva fila
    $(rowNode).find('td').eq(0).addClass('td_items');
    $(rowNode).find('td').eq(1).addClass('td_items');
    $(rowNode).find('td').eq(3).addClass('td_items_dos');

    $(rowNode).find('td').eq(0).addClass('col-sm-2');
    $(rowNode).find('td').eq(1).addClass('col-sm-6');
    $(rowNode).find('td').eq(2).addClass('col-sm-3');
    $(rowNode).find('td').eq(3).addClass('col-sm-1');

    // Se entrega el id del item a la fila, para poder eliminar posteriormente
    $(rowNode).attr("id", respuesta.id_item);

    if ($('#div_items').css('display') == 'none') {
        $('#div_items').fadeOut(400, function() {
            $('#div_items').css('display', '');
        });
    }
}

/**
 * Elimina un item de la tabla de los items presupuestarios
 */
function eliminarItem(idItem, sinEspera = false) {
    // Se quita el id del arreglo de items.
    let index = arregloItems.indexOf(idItem);
    arregloItems.splice(index, 1);
    if (sinEspera == false) {
        // Se elimina la fila
        $('#' + idItem).fadeOut(400, function() {
            tablaItemsPresupuestarios.row('#' + idItem).remove().draw();
        });

        // Contar la cantidad de filas para ocultar la tabla
        // Se utiliza 1 porque siempre hay una fila por defecto
        if (tablaItemsPresupuestarios.rows().count() == 1) {
            $('#div_items').fadeOut(400, function() {
                $('#div_items').css('display', 'none');
            });
        }
    } else {
        // Se elimina la fila
        tablaItemsPresupuestarios.row('#' + idItem).remove().draw();
        // Contar la cantidad de filas para ocultar la tabla
        // Se utiliza 1 porque siempre hay una fila por defecto
        if (tablaItemsPresupuestarios.rows().count() == 1) {
            $('#div_items').css('display', 'none');
        }
    }
}

/**
 * Set el DOM despues de agregar un item 
 */
function setDOM() {
    $('#nombre_proveedor').select2('val', '');
    $('#rut_proveedor').select2('val', '');
    $('#tipo_documento').select2('val', '');
    $('#numero_documento').val(null);
    $('#modalidad_compra').select2('val', '');
    $('#tipo_adjudicacion').select2('val', '');
    $('#numero_orden_compra').val(null);
    $('#tipo_informe').select2('val', '');
    // $('#responsable').select2('val', '');
    $('#valor_total_documento').val(null);
    $('#referente_tecnico').select2('val', '');
    $('#numero_licitacion').val(null);
    $('#observacion_documento').val(null);
    //Archivo
    $('#archivo').val(null);
    $('#div_archivo').css('display', 'none');
    tablaArchivo.clear().draw(); // se remueven todas las filas de la tabla
    //Items
    arregloItems = [];
    $('#div_items').css('display', 'none');
    tablaItemsPresupuestarios.clear().draw(); // se remueven todas las filas de la tabla

    // Errores validator laravel
    $('#ulErrores').html('');
    $('#divErrores').css('display', 'none');

    // info proveedor y documento bodega
    $('#orden_compra_prov').val('');
    $('#fecha_oc').val('');
    $('#monto_maximo_oc').val('');
    $('#saldo_oc').val('');
    $('#divBodega').css('display', 'none');
    $('#divDocumentosRecepcion').html('');
    // Guias de despacho
    $('#divGuiasDespacho').css('display', 'none');
    $('#divGuiasRecepcionBodega').html('');


    $('.form-validate').removeClass('has-success');
}

/**
 * Trae los documentos para reemplazar por el documento actual
 * Depende del proveedor y del tipo de tipo de documento
 */
function setReemplaza(tipoDocumento) {
    let idTipoDocumento = parseInt(tipoDocumento.value);
    let idProveedor = parseInt($('#nombre_proveedor').val());
    // buscar los documentos de bodega
    getDocumentosBodega(idProveedor, idTipoDocumento);
    // buscar las guias de despacho de bodega
    getGuiasDespachoBodega(idProveedor, idTipoDocumento);

    getFacturasRechazadasAcepta(idProveedor, idTipoDocumento); 
    

    if ( ! isNaN(idProveedor) ) {

        if ( idTipoDocumento == 1 || idTipoDocumento == 4 || idTipoDocumento == 5 || idTipoDocumento == 10 || idTipoDocumento == 11 || idTipoDocumento == 8 ) {
            $('#div_valores_boleta').css('display','none');
            $('#div_relacionado').html('');

            $.ajax({
                url: urlSetReemplaza + '/' + idProveedor + '/' + idTipoDocumento,
                type: 'get',

                success: function(documentos) {
                        // console.log(documentos);
                        //se pasan los datos al select
                        if (documentos.estado == 'success') {
                            let htmlReemplaza = '<label for="reemplaza" class="label-form">';
                            let htmlRequired = '';
                            // let htmlValidate = '';
                            let htmlFuncion = '';

                            if (idTipoDocumento == 1) {
                                htmlReemplaza += 'Reemplaza a ';
                                htmlFuncion = 'onchange="setDocumentoReemplazaFactura(this);"';

                            } else if (idTipoDocumento == 4 || idTipoDocumento == 5 || idTipoDocumento == 10 || idTipoDocumento == 11) {

                                htmlReemplaza += 'Factura Asociada <span class="span-label">*</span>';
                                htmlRequired = 'required';
                                // htmlValidate = 'form-validate';
                                htmlFuncion = 'onchange="setDocumentoNcNd(this);"';

                                if ( (idTipoDocumento == 10 || idTipoDocumento == 11) && documentos.numCarta != '' ) {
                                    $('#numero_documento').val( documentos.numCarta );
                                }

                            } else if (idTipoDocumento == 8) {

                                htmlReemplaza += 'Factura Refacturada <span class="span-label">*</span>';
                                htmlRequired = 'required';
                                // htmlValidate = 'form-validate';

                            }

                            htmlReemplaza += '</label>';
                            // htmlReemplaza += '<div class="col-sm-4 ' + htmlValidate + '">';
                            htmlReemplaza += '<select name="reemplaza" id="reemplaza" class="form-control select2" ' + htmlRequired + ' ' + htmlFuncion + ' >';
                            htmlReemplaza += '<option value="">Seleccione</option>';
                            htmlReemplaza += documentos.mensaje; // se añaden las opciones traidas desde el controlador
                            // htmlReemplaza += '</select></div>';
                            htmlReemplaza += '</select>';

                            $('#div_relacionado').html(htmlReemplaza);
                            $('#reemplaza').select2();
                        } else if (documentos.estado == 'error') {
                            toastr.warning(documentos.mensaje, "Atención", optionsToastr);
                        }

                    } // succes

            }).fail(function() {
                // toastr.warning("No es posible buscar datos, intentelo más tarde", "Atención", optionsToastr);
                $('#div_relacionado').html('');
            });
        } else if ( idTipoDocumento == 3) {

            $('#div_relacionado').html('');
            $('#div_valores_boleta').css('display','');

            // Boletas de Honorarios Afecta
            // console.log('Seleccionaste bolestas afectas');

            setMontosParaBoleta();
            
        }

    } else {
        $('#div_relacionado').html('');
        $('#div_valores_boleta').css('display','none');
    }

}

/**
 * Para entregar los montos relacionados a las boletas
 */
function setMontosParaBoleta( ) {

    if ( $('#valor_total_documento').val() != '') {

        let totalDocumento = parseInt($('#valor_total_documento').val().replace(/\./g, ''));
        // console.log('totalDocumento antes del if : '+ totalDocumento);
        if ( ! isNaN(totalDocumento) ) {
            // console.log('totalDocumento : '+ totalDocumento);
            let impuesto = 0;
            añoEmision = $("#fecha_recepcion").val().split("/")[2];
            if ( añoEmision == 2019 ) {
                impuesto = totalDocumento * 0.1;
            }

            if ( añoEmision == 2020 ) {
                impuesto = totalDocumento * 0.1075;
            }
            // console.log('añoEmision : '+ añoEmision);
            
            let liquido = totalDocumento - parseInt(Math.round(impuesto));

            // console.log('impuesto : '+ impuesto);
            // console.log('liquido : '+ liquido);

            // console.log('impuesto Math.round( : '+ Math.round(impuesto) );
            // console.log('liquido Math.round( : '+ Math.round(liquido) );

            $('#liquido').val( formatMoney( parseInt(Math.round(liquido)) ) );
            $('#impuesto').val( formatMoney( parseInt(Math.round(impuesto)) ) );
            
        }

    }
    
}

/**
 * 
 * Trae todos los documentos recepcionados en bodega 
 * Estan asociados al proveedor y al tipo de documento
 * seleccionado.
 */
function getDocumentosBodega(idProveedor, idTipoDocumento) {

    if ($('#numero_documento').attr('readonly')) {
        $('#numero_documento').removeAttr('readonly');
        $('#numero_documento').val('');
    }

    if ($('#valor_total_documento').attr('readonly')) {
        $('#valor_total_documento').removeAttr('readonly');
        $('#valor_total_documento').val('');
    }

    if ( ! isNaN(idProveedor) && ! isNaN(idTipoDocumento) ) {

        $('#divBodega').css('display', 'none');
        $('#divDocumentosRecepcion').html('');
        $.ajax({
            url: urlGetDocumentosBodega + '/' + idProveedor + '/' + idTipoDocumento,
            type: 'get',

            success: function(documentos) {
                    // console.log(documentos);
                    //se pasan los datos al select
                    if (documentos.estado == 'success') {

                        let htmlReemplaza = '<select name="doc_disponible_bodega" id="doc_disponible_bodega" class="form-control select2" onchange="setDocumento(this);" >';
                        htmlReemplaza += '<option value="">Seleccione</option>';
                        htmlReemplaza += documentos.mensaje; // se añaden las opciones traidas desde el controlador
                        htmlReemplaza += '</select>';

                        $('#divDocumentosRecepcion').html(htmlReemplaza);
                        $('#doc_disponible_bodega').select2();
                        $('#divBodega').show();

                    } else if (documentos.estado == 'error') {

                        $('#divBodega').css('display', 'none');
                        toastr.warning(documentos.mensaje, "Atención", optionsToastr);

                    }

                } // succes

        }).fail(function() {
            $('#divDocumentosRecepcion').html('');
            $('#divBodega').css('display', 'none');
        });
    } else {
        $('#divDocumentosRecepcion').html('');
        $('#divBodega').css('display', 'none');
    }
}

/**
 * Actualiza los campos del documento según el documento de bodega seleccionado
 * Actualiza los campos de la OC según el documento de bodega seleccionado
 */
function setDocumento(documentoBodega) {

    if (documentoBodega.value != '') {

        var docBodega = document.getElementById('docBodega_' + documentoBodega.value);
        var datosRecepcion = JSON.parse(docBodega.dataset.datosrecepcion);
        numeroDocumentoRecepcion = datosRecepcion.numeroDocumento;
        // console.info(datosRecepcion);
        // console.log(datosRecepcion);
        // console.dir(datosRecepcion);
        // console.table(datosRecepcion);
        // Evaluar si los números de documento, son los mismos
        // Evaluar si el numero de documento de la recepcion concuerda con el de refacturacion
        if ( ( $('#numero_documento').val() != numeroDocumentoRecepcion) && ( numeroDocumentoRefacturacion != numeroDocumentoRecepcion ) ) {

            $('#doc_disponible_bodega').select2('val', '');
            numeroDocumentoRecepcion = 0;
            toastr.info("El N° documento o el N° de documento a reemplazar<br>debe ser igual al N° documento de recepción.", "Atención", optionsToastr);
            

        } else {
            
            // $('#numero_documento').val(docBodega.dataset.numdocumento);
            // $('#valor_total_documento').val(docBodega.dataset.valordocumento);

            // Validar que la diferencia de valores entre la recepcion seleccionada 
            // y el documento no sea mayor a 100 ni menor a 100
            // si existe el valor_total_documento_actualizado, se utiliza su valor para la diferencia
            let diferenciaTotales = 0;
            if ( $('#valor_total_documento_actualizado').length ) {
                diferenciaTotales = parseInt( $('#valor_total_documento_actualizado').val().replace(/\./g, '') ) - datosRecepcion.valorDocumento;
            } else {
                diferenciaTotales = parseInt( $('#valor_total_documento').val().replace(/\./g, '') ) - datosRecepcion.valorDocumento;
            }

            /**
             * Si el limite de 8 días para rechazar el documento se supera.
             * No se considera la diferencia en los montos, ya que se debe esperar
             * una nota de credito o debito por la factura.
             */
            diasRechazo = null;
            if ( $('#dias_rechazo').length ) {
                diasRechazo = parseInt( $('#dias_rechazo').val()) ;
            }
            // console.log('diasRechazo: '+ diasRechazo);
            // console.log('diasRechazo type: '+ typeof diasRechazo);
    
            if ( ( diasRechazo != null && diasRechazo < 0 ) || ( diferenciaTotales <= 100 && diferenciaTotales >= -100 ) ) {
                $('#numero_documento').attr('readonly', 'readonly');
                $('#valor_total_documento').attr('readonly', 'readonly');

                // Set de la OC
                if ( datosRecepcion.numeroOrdenCompra != '' && datosRecepcion.numeroOrdenCompra != null ) {
                    $('#numero_orden_compra').val(datosRecepcion.numeroOrdenCompra);
                    $('#orden_compra_prov').val(datosRecepcion.numeroOrdenCompra);
                }

                $('#fecha_oc').val(datosRecepcion.fechaOrdenCompra);
                $('#monto_maximo_oc').val(datosRecepcion.montoMaximoOrdenCompra);
                $('#saldo_oc').val(datosRecepcion.saldoOrdenCompra);
                // Fin Set de la OC

                // Dejar readonly guias_disponible_bodega
                if ($('#guias_disponible_bodega').length > 0) {
                    $('#guias_disponible_bodega').select2("readonly", true);
                }

                // Agregar item presupuestario automaticamente
                var idDocumentoRecepcion = documentoBodega.value;
                getItemsAutomaticosRecepcion(idDocumentoRecepcion);

            } else {
                $('#doc_disponible_bodega').select2('val', '');
                numeroDocumentoRecepcion = 0;

                $('#orden_compra_prov').val('');
                $('#fecha_oc').val('');
                $('#monto_maximo_oc').val('');
                $('#saldo_oc').val('');
                if ( $('#valor_total_documento_actualizado').length ) {
                    toastr.info("La diferencia entre el Valor Tot. Act. del documento y el valor total de la recepción no puede ser mayor ni menor de $100", "Atención", optionsToastr);
                } else {
                    toastr.info("La diferencia entre el Valor Total del documento y el valor total de la recepción no puede ser mayor ni menor de $100", "Atención", optionsToastr);
                }
                
            }

        }

    } else {
        numeroDocumentoRecepcion = 0;

        $('#orden_compra_prov').val('');
        $('#fecha_oc').val('');
        $('#monto_maximo_oc').val('');
        $('#saldo_oc').val('');

        $('#numero_documento').val('');
        $('#valor_total_documento').val('');

        $('#numero_documento').removeAttr('readonly');
        $('#valor_total_documento').removeAttr('readonly');

        if ($('#guias_disponible_bodega').length > 0) {
            $('#guias_disponible_bodega').select2("readonly", false);
        }

    }

}

/**
 * 
 * @param {int} idDocumentoRecepcion 
 * Trae el id del documento de recepcion seleccionado para el match.
 * Con este id se buscan los items 
 */
function getItemsAutomaticosRecepcion(idDocumentoRecepcion)
{

    var arregloItemsJSON = JSON.stringify(arregloItems);
    $.ajax({
        url: urlGetItemsAutomaticos + '/' + arregloItemsJSON + '/' + idDocumentoRecepcion,
        type: 'get',

        success: function(respuesta) {

            if ( respuesta.estado == 'success' ) {
                
                /**
                 * Para agregar items presupuestarios.
                 * recorrer los items
                 * evaluar si estan en el arreglo de item
                 * en caso de no estar, agregar.
                 */
                for (var index = 0; index < respuesta.items.length; index++) {
                    // console.info(arregloItems.includes(respuesta.items[index].id_item));
                    if ( !arregloItems.includes(respuesta.items[index].id_item) ) {
                        nuevaFilaItemsPresupuestarios(respuesta.items[index]);
                    }
                }

            } else {
                toastr.warning(respuesta.mensaje, "Atención", optionsToastr);
            }

        } // succes

    }).fail(function(error) {
        // console.log('problemas con traer el item presupuestario automatico');
        // console.log(error);
    });

}

/**
 * Trae todas las guias de despacho recepcionadas en Bogeda
 * Estan asociadas al proveedor.
 * Pueden ser seleccionadas N guias.
 */
function getGuiasDespachoBodega(idProveedor, idTipoDocumento) {

    $('#total_monto_guias').val(0);
    if (idProveedor != NaN && idTipoDocumento == 1) {
        // Solo para facturas electronicas
        $('#divGuiasDespacho').css('display', 'none');
        $('#divGuiasRecepcionBodega').html('');

        $.ajax({
            url: urlGetGuiasDespachoBodega + '/' + idProveedor,
            type: 'get',

            success: function(documentos) {
                    //se pasan los datos al select
                    if (documentos.estado == 'success') {

                        let htmlReemplaza = '<select name="guias_disponible_bodega[]" id="guias_disponible_bodega" class="form-control select2" multiple placeholder="Seleccione">';
                        htmlReemplaza += documentos.mensaje; // se añaden las opciones traidas desde el controlador
                        htmlReemplaza += '</select>';

                        $('#divGuiasRecepcionBodega').html(htmlReemplaza);
                        $('#guias_disponible_bodega').select2();
                        $('#guias_disponible_bodega').on('select2-selecting', function(e) {

                            // No se permite añadir el elemento si excede el monto del documento
                            if (!setTotalMontoGuias(e.val, 'Agregar')) {
                                e.preventDefault();
                            }

                        });

                        $('#guias_disponible_bodega').on('select2-removing', function(e) {

                            // No se permite añadir el elemento si excede el monto del documento
                            if (!setTotalMontoGuias(e.val, 'Quitar')) {
                                e.preventDefault();
                            }

                        });
                        $('#divGuiasDespacho').show();

                    } else if (documentos.estado == 'error') {

                        $('#divGuiasDespacho').css('display', 'none');
                        toastr.warning(documentos.mensaje, "Atención", optionsToastr);

                    }

                } // succes

        }).fail(function() {

            $('#divGuiasRecepcionBodega').html('');
            $('#divGuiasDespacho').css('display', 'none');

        });

    } else {

        $('#divGuiasDespacho').css('display', 'none');
        $('#divGuiasRecepcionBodega').html('');

    }

}

/**
 * Actualiza el monto total de las guias.
 * Debe ser igual al valor total del documento para poder guardar la relacion
 */
function setTotalMontoGuias(guiaDespachoSeleccionada, opcion) {

    let guiaDespacho = document.getElementById('guiaDespacho_' + guiaDespachoSeleccionada);
    let totalDocumento = parseInt( $('#valor_total_documento').val().replace(/\./g, '') );
    let totalGuias = parseInt($('#total_monto_guias').val().replace(/\./g, ''));
    let respuesta = false;

    if (totalDocumento) {
        if (opcion == 'Agregar') {

                totalGuiasNuevo = totalGuias + parseInt(guiaDespacho.dataset.valordocumento);

                // todo bien
                $('#total_monto_guias').val(formatMoney(totalGuiasNuevo));
                respuesta = true;
                // Dejar readonly doc_disponible_bodega
                if ( $('#doc_disponible_bodega').length > 0 ) {
                    $('#doc_disponible_bodega').select2("readonly", true);
                }

        } else {

            // La opcion es Quitar la guia
            totalGuiasNuevo = totalGuias - parseInt(guiaDespacho.dataset.valordocumento);
            $('#total_monto_guias').val(formatMoney(totalGuiasNuevo));
            respuesta = true;
            if (totalGuiasNuevo == 0) {
                // Dejar readonly doc_disponible_bodega
                if ($('#doc_disponible_bodega').length > 0) {
                    $('#doc_disponible_bodega').select2("readonly", false);
                }
            }

        }

    } else {
        toastr.info("No se pueden seleccionar Guías de Despacho si el Valor Total del Documento no tiene monto", "Atención", optionsToastr);
    }

    return respuesta;
}

/**
 * Valida que el monto total de las guias y el valor total del documento coincidan
 * si existe el valor_total_documento_actualizado, se utiliza su valor para validar
 */
function validaTotalGuiasConTotalDocumento() {

    let totalDocumento = 0;
    let totalGuias = parseInt($('#total_monto_guias').val().replace(/\./g, ''));
    let respuesta = true;

    if ( totalGuias > 0) {
        if ( $('#valor_total_documento_actualizado').length ) {
            totalDocumento = parseInt($('#valor_total_documento_actualizado').val().replace(/\./g, ''));
        } else {
            totalDocumento = parseInt($('#valor_total_documento').val().replace(/\./g, ''));
        }

        /**
         * Si el limite de 8 días para rechazar el documento se supera.
         * No se considera la diferencia en los montos, ya que se debe esperar
         * una nota de credito o debito por la factura.
         */
        diasRechazo = null;
        if ( $('#dias_rechazo').length ) {
            diasRechazo = parseInt( $('#dias_rechazo').val()) ;
        }

        let diferenciaTotales = totalDocumento - totalGuias;

        if ( ! ( ( diasRechazo != null && diasRechazo < 0 ) || ( diferenciaTotales <= 100 && diferenciaTotales >= -100 ) ) ) {
            if ( $('#valor_total_documento_actualizado').length ) {
                toastr.error('La diferencia entre el Valor Tot. Act. del documento y el valor Total Guías de la recepción no puede ser mayor ni menor de $100', 'Atención', optionsToastr);
            } else {
                toastr.error('La diferencia entre el Valor Total del documento y el valor Total Guías de la recepción no puede ser mayor ni menor de $100', 'Atención', optionsToastr);
            }

            respuesta = false;
        }
    
        // if ( ( diasRechazo != null && diasRechazo < 0 ) || ( diferenciaTotales > 100 || diferenciaTotales < -100 ) ) {
        //     if ( $('#valor_total_documento_actualizado').length ) {
        //         toastr.error('La diferencia entre el Valor Tot. Act. del documento y el valor Total Guías de la recepción no puede ser mayor ni menor de $100', 'Atención', optionsToastr);
        //     } else {
        //         toastr.error('La diferencia entre el Valor Total del documento y el valor Total Guías de la recepción no puede ser mayor ni menor de $100', 'Atención', optionsToastr);
        //     }
        //     return false;
        // } else {
        //     return true;
        // }
    } 
    // else {
    //     return true;
    // }

    return respuesta;

}

/**
 * Actualizas los campos del documentos según el documento del proveedor seleccionado
 * Depende del tipo de documento, NC o ND
 */
function setDocumentoNcNd(documentoProveedor) {
    // console.log('documentoProveedor value: ' + documentoProveedor.value);
    // Se eliminan los items de la tabla
    arregloItems.forEach(function(idItem, indice, array) {
        eliminarItem(idItem, true);
    });

    if (documentoProveedor.value != '') {
        var docProveedor = document.getElementById('docProv_' + documentoProveedor.value);
        var datosDoc = JSON.parse(docProveedor.dataset.datosdocumento);

        $('#numero_orden_compra').val(datosDoc.ordenCompra);
        $('#modalidad_compra').select2('val', datosDoc.idModalidadCompra);
        $('#tipo_adjudicacion').select2('val', datosDoc.idTipoAdjudicacion);
        $('#tipo_informe').select2('val', datosDoc.idTipoInforme);
        // $('#responsable').select2('val', datosDoc.idResponsable);
        $('#referente_tecnico').select2('val', datosDoc.idReferenteTecnico);
        $('#numero_licitacion').val(datosDoc.numeroLicitacon);

        // console.log('Total actualizado de la factura seleccionada : '+ datosDoc.totalActualizadoDocumento);
        // console.log('Total de la factura seleccionada : '+ datosDoc.totalDocumento);
        montoMaximoNc = parseInt(datosDoc.totalActualizadoDocumento);
        // console.log('montoMaximoNc : '+  montoMaximoNc);
        // console.log(' typeOF montoMaximoNc : '+ typeof(montoMaximoNc) );
        evaluarMaximoNC();


        /**
         * Para agregar items presupuestarios.
         * recorrer los items
         * evaluar si estan en el arreglo de item
         * en caso de no estar, agregar.
         */
        for (var index = 0; index < datosDoc.items.length; index++) {
            // console.info(arregloItems.includes(datosDoc.items[index].id_item));
            if (!arregloItems.includes(datosDoc.items[index].id_item)) {
                nuevaFilaItemsPresupuestarios(datosDoc.items[index]);
            }
        }

    } else {
        $('#numero_orden_compra').val('');
        $('#modalidad_compra').select2('val', '');
        $('#tipo_adjudicacion').select2('val', '');
        $('#tipo_informe').select2('val', '');
        // $('#responsable').select2('val', '');
        $('#referente_tecnico').select2('val', '');
        $('#numero_licitacion').val('');
        montoMaximoNc = false;
    }
}

/**
 * Da formato a la orden de compra que se debe ingresar
 */
function setOrdenCompra(modalidadCompra) {
    let idModalidadCompra = parseInt(modalidadCompra.value);
    let numeroOrderCompra = $('#numero_orden_compra').val();

    if (idModalidadCompra != NaN) {

        $.ajax({
            url: urlSetOrdenCompra + '/' + idModalidadCompra,
            type: 'get',

            success: function(respuesta) {
                    //console.log(respuesta);

                    // Se modifica el div según la respuesta del controlador
                    $('#div_orden_compra').html('');
                    if (respuesta.estado == 'success') {

                        $('#div_orden_compra').html(respuesta.mensaje);
                        if (respuesta.predeterminado == true) {
                            $('#numero_orden_compra').select2();
                        } else {
                            if (idModalidadCompra == 1 && numeroOrderCompra.indexOf('1641-') == -1) {
                                $('#numero_orden_compra').val('1641-' + numeroOrderCompra);
                            } else {
                                $('#numero_orden_compra').val(numeroOrderCompra);
                            }
                        }

                    } else if (respuesta.estado == 'error') {
                        toastr.warning(respuesta.mensaje, "Atención", optionsToastr);
                    }

                } // succes

        }).fail(function() {
            // toastr.warning("No es posible buscar datos, intentelo más tarde", "Atención", optionsToastr);

        });
    }
}

/**
 * Quita un elemeno de la tabla 
 */
function quitarElementoTabla(id) {
    $('#doc_' + id).fadeOut(400, function() {
        tablaDocsAcepta.row('#doc_' + id).remove().draw();
    });
}

/**
 * Actualiza la tabla según la accion realizada
 */
function actualizaTabla(respuesta) {

    if (respuesta.datatable == 'ingresados') {
        
        let botones = '<td ><div class="btn-group">';
        if (verDocumento == '1') {
            botones += '<button class="btn btn-success btn-xs" title="Ver Documento" onclick="verDocumento(' + respuesta.id + ');">';
            botones += '<i class="fas fa-eye"></i></button>';
        }

        if (editarDocumento == '1') {
            botones += '<button class="btn btn-warning btn-xs" title="Editar Documento" onclick="editarDocumento(' + respuesta.id + ');">';
            botones += '<i class="fas fa-edit"></i></button>';
        }

        if (verDocumento == '1') {
            botones += '<button class="btn btn-primary btn-xs" title="Trazabilidad Documento" onclick="traza(' + respuesta.id + ');">';
            botones += '<i class = "fas fa-map-signs fa-lg"></i></button>';
        }

        botones += '</div></td>';

        tablaPrincipal.row('#tr_' + respuesta.id).data([
            respuesta.estadoDocumento,
            respuesta.rutProveedor,
            respuesta.nombreProveedor,
            respuesta.tipoDocumento,
            respuesta.numeroDocumento,
            respuesta.documentoCompra,
            respuesta.fechaRecepcion,
            respuesta.fechaDocumento,
            respuesta.items,
            respuesta.totalDocumento,
            botones
        ]).draw();

    } else if (respuesta.datatable == 'editar_pendientes_validacion' || respuesta.datatable == 'con_problema') {

        if (respuesta.tieneRelacion == 1) {

            $('#tr_' + respuesta.id).fadeOut(400, function() {
                tablaPrincipal.row('#tr_' + respuesta.id).remove().draw();
            });

        } else {

            let botones = '<td ><div class="btn-group">';
            if (editarDocumento == '1') {
                botones += '<button class="btn btn-warning btn-xs" title="Editar Documento" onclick="editar(' + respuesta.id + ');">';
                botones += '<i class="fas fa-edit"></i></button>';
            }

            if (cuadrarDocumento == '1') {
                botones += '<button class="btn btn-success btn-xs" title="Validar Documento" onclick="validar(' + respuesta.id + ');">';
                botones += '<i class="fas fa-thumbs-up fa-lg"></i></button>';
            }

            if ( respuesta.datatable != 'con_problema' ) {

                if (eliminarDocumento == '1') {
                    botones += '<button class="btn btn-danger btn-xs" title="Eliminar Documento" onclick="eliminar(' + respuesta.id + ');">';
                    botones += '<i class="fas fa-trash"></i></button>';
                }

            }
            

            botones += '</div></td>';

            tablaPrincipal.row('#tr_' + respuesta.id).data([
                respuesta.diferenciaDias,
                respuesta.fechaIngreso,
                respuesta.rutProveedor,
                respuesta.nombreProveedor,
                respuesta.posibleMatch, // ¿Match?
                respuesta.tipoDocumento,
                respuesta.numeroDocumento,
                respuesta.documentoCompra,
                respuesta.fechaRecepcion,
                respuesta.fechaDocumento,
                respuesta.items,
                respuesta.totalDocumento,
                botones
            ]).draw();
            
            $('#tr_undefined').attr('id', 'tr_' + respuesta.id );
        }
    } else if (respuesta.datatable == 'devengar') {

        let botones = '<td ><div class="btn-group">';
        botones += '<button class="btn btn-info btn-xs" title="Devengar Documento" onclick="devengarDocumento(' + respuesta.id + ');">';
        botones += '<i class="fas fa-gavel"></i></button>';
        botones += '<button class="btn btn-warning btn-xs" title="Editar Documento" onclick="editarDocumento(' + respuesta.id + ');">';
        botones += '<i class="fas fa-edit"></i></button>';
        botones += '</div></td>';

        tablaPrincipal.row('#tr_' + respuesta.id).data([
            respuesta.tipoInforme,
            respuesta.rutProveedor,
            respuesta.nombreProveedor,
            respuesta.tipoDocumento,
            respuesta.numeroDocumento,
            respuesta.modalidadCompra,
            respuesta.documentoCompra,
            respuesta.fechaRecepcion,
            respuesta.totalDocumento,
            botones
        ]).draw();

    } else if (respuesta.datatable == 'devengar_listo_documento' || respuesta.datatable == 'pendientes_validacion' || respuesta.datatable == 'validados' || respuesta.datatable == 'documentos_eliminados') {

        $('#tr_' + respuesta.id).fadeOut(400, function() {
            tablaPrincipal.row('#tr_' + respuesta.id).remove().draw();
        });

    } else if (respuesta.datatable == 'devengar_editado_documento') {

        $('#tabla_documentos').DataTable().ajax.reload();

    }
}

/**
 * Funcion que trae un listado de las facturas rechazadas de Acepta
 * Permite refacturarar dichas facturas.
 * Con el fin de poder realizar el match por su n° de documento
 */
function getFacturasRechazadasAcepta(idProveedor, idTipoDocumento) {

    if ( idTipoDocumento == 1) {

        $('#div_rechazados_acepta').html('');
        $('#div_rechazados_acepta').css('display', 'none');

        $.ajax({
            url: urlGetFacturasRechazadasAcepta + '/' + idProveedor ,
            type: 'get',

            success: function(documentos) {
                    // console.log(documentos);
                    //se pasan los datos al select
                    if (documentos.estado == 'success') {
                        let htmlReemplaza = '<label for="reemplaza_archivo_acepta" class="label-form">Reemplaza al Archivo Acepta Rechazado </label>';
                        let htmlFuncion = 'onchange="setDocumentoReemplazaRechazado(this);"';

                        // htmlReemplaza += '<div class="col-sm-4 form-validate">';
                        htmlReemplaza += '<select name="reemplaza_archivo_acepta" id="reemplaza_archivo_acepta" class="form-control select2"  ' + htmlFuncion + ' >';
                        htmlReemplaza += '<option value="">Seleccione</option>';
                        htmlReemplaza += documentos.mensaje; // se añaden las opciones traidas desde el controlador
                        // htmlReemplaza += '</select></div>';
                        htmlReemplaza += '</select>';
                        htmlReemplaza += '<span class="help-block">Estos Archivos estan rechazados</span>';

                        $('#div_rechazados_acepta').html(htmlReemplaza);
                        $('#div_rechazados_acepta').css('display', '');
                        $('#reemplaza_archivo_acepta').select2();

                    } else if (documentos.estado == 'error') {
                        toastr.warning(documentos.mensaje, "Atención", optionsToastr);
                    }

                } // succes

        }).fail(function() {
            // toastr.warning("No es posible buscar datos, intentelo más tarde", "Atención", optionsToastr);
            $('#div_rechazados_acepta').html('');
            $('#div_rechazados_acepta').css('display', 'none');
        });

    } else {
        $('#div_rechazados_acepta').html('');
        $('#div_rechazados_acepta').css('display', 'none');
    }
}

/**
 * Es para utilizar el n° del documento al que se reemplazara para el match con las recepciones
 * en este caso son documentos rechazados de acepta
 */
function setDocumentoReemplazaRechazado(documentoRechazado) {

    if (documentoRechazado.value != '') {
        // Desactivar el select de documentos reemplazar
        if ( $('#reemplaza').length > 0 ) {
            $('#reemplaza').select2("readonly", true);
        }

        var docRechazado = document.getElementById('docRechazadoProv_' + documentoRechazado.value);
        var datosDoc = JSON.parse(docRechazado.dataset.datosdocumento);

        // $('#valor_total_documento').val(datosDoc.totalDocumento);
        numeroDocumentoRefacturacion = datosDoc.numeroDocumento;

        // Validar que el documento seleccionado concuerda con la recepcion seleccionada
        if ( $('#doc_disponible_bodega').length > 0 && numeroDocumentoRecepcion != 0 && numeroDocumentoRecepcion != numeroDocumentoRefacturacion ) {

            $('#doc_disponible_bodega').select2('val', '');
            $('#doc_disponible_bodega').trigger("change");
            
        }

    } else {
        // Poder seleccionar otro documento de reemplazo
        if ( $('#reemplaza').length > 0 ) {
            $('#reemplaza').select2("readonly", false);
        }

        numeroDocumentoRefacturacion = 0;
    }
    
}

/**
 * Es para utilizar el n° del documento al que se reemplazara para el match con las recepciones
 * en este caso son documento que se encuentran en el sistema
 */
function setDocumentoReemplazaFactura(documentoProveedor) {

    if (documentoProveedor.value != '') {
        // Desactivar el select de documentos reemplazar de acepta
        if ( $('#reemplaza_archivo_acepta').length > 0 ) {
            $('#reemplaza_archivo_acepta').select2("readonly", true);
        }

        var docProveedor = document.getElementById('docProv_' + documentoProveedor.value);
        var datosDoc = JSON.parse(docProveedor.dataset.datosdocumento);

        // $('#valor_total_documento').val(datosDoc.totalDocumento);
        numeroDocumentoRefacturacion = datosDoc.numeroDocumento;

        // Validar que el documento seleccionado concuerda con la recepcion seleccionada
        if ( $('#doc_disponible_bodega').length > 0 && numeroDocumentoRecepcion != 0 && numeroDocumentoRecepcion != numeroDocumentoRefacturacion ) {

            $('#doc_disponible_bodega').select2('val', '');
            $('#doc_disponible_bodega').trigger("change");
            
        }

    } else {
        // Poder seleccionar otro documento de reemplazo
        if ( $('#reemplaza_archivo_acepta').length > 0 ) {
            $('#reemplaza_archivo_acepta').select2("readonly", false);
        }

        numeroDocumentoRefacturacion = 0;
    }

}

/**
 * Funcion para setear elementos dependiendo del tipo de informe seleccionado
 */
function setTipoInforme(tipoInforme) {
    if ( tipoInforme.value != '' && parseInt(tipoInforme.value) == 1) {
        // console.log('value de tipoInforme : ' + tipoInforme.value );
        // $('#responsable').select2('val', 17); // Pablo Osorio
        $('#referente_tecnico').select2('val', 13); // Francisco Bermejo
    }
}

/**
 * Funcion para setear el Label del impuesto de las boletas de honorarios afectas
 * Depende de la fecha del documento (fecha de emisión)
 */
function setLabelImpuestoBoletas(añoEmision)
{
    if ( añoEmision == 2020 ) {
        $('#label_impuesto').html('10.75% Impuesto');
    }

    if ( añoEmision == 2019 ) {
        $('#label_impuesto').html('10% Impuesto');
    }
}

/**
 * Valida que concuerde la recepcion de hermes
 * con el documento a ingresar
 */
function setRecepcionHermes(recepcionHermes)
{
    if (recepcionHermes.value != '') {

        var docRecepcion = document.getElementById('recepcionHermes_' + recepcionHermes.value);
        var datosRecepcion = JSON.parse(docRecepcion.dataset.datosrecepcion);
        numeroDocumentoRecepcion = datosRecepcion.numeroDocumento;
        
        // console.dir(datosRecepcion);
        // console.table(datosRecepcion);

        // Evaluar si los números de documento, son los mismos
        // Evaluar si el numero de documento de la recepcion concuerda con el de refacturacion
        if ( ( $('#numero_documento').val() != numeroDocumentoRecepcion) && ( numeroDocumentoRefacturacion != numeroDocumentoRecepcion ) ) {

            $('#factura_recepcion_hermes').select2('val', '');
            numeroDocumentoRecepcion = 0;
            toastr.info("El N° documento o el N° de documento a reemplazar<br>debe ser igual al N° documento de recepción.", "Atención", optionsToastr);
            

        } else {
            
            // Validar que la diferencia de valores entre la recepcion seleccionada 
            // y el documento no sea mayor a 100 ni menor a 100
            // si existe el valor_total_documento_actualizado, se utiliza su valor para la diferencia
            let diferenciaTotales = 0;
            if ( $('#valor_total_documento_actualizado').length ) {
                diferenciaTotales = parseInt( $('#valor_total_documento_actualizado').val().replace(/\./g, '') ) - datosRecepcion.valorDocumento;
            } else {
                diferenciaTotales = parseInt( $('#valor_total_documento').val().replace(/\./g, '') ) - datosRecepcion.valorDocumento;
            }

            /**
             * Si el limite de 8 días para rechazar el documento se supera.
             * No se considera la diferencia en los montos, ya que se debe esperar
             * una nota de credito o debito por la factura.
             */
            diasRechazo = null;
            if ( $('#dias_rechazo').length ) {
                diasRechazo = parseInt( $('#dias_rechazo').val()) ;
            }
            // console.log('diasRechazo: '+ diasRechazo);
            // console.log('diasRechazo type: '+ typeof diasRechazo);
    
            if ( ( diasRechazo != null && diasRechazo < 0 ) || ( diferenciaTotales <= 100 && diferenciaTotales >= -100 ) ) {
                $('#numero_documento').attr('readonly', 'readonly');
                $('#valor_total_documento').attr('readonly', 'readonly');

                // // Set de la OC
                // if ( datosRecepcion.numeroOrdenCompra != '' && datosRecepcion.numeroOrdenCompra != null ) {
                //     $('#numero_orden_compra').val(datosRecepcion.numeroOrdenCompra);
                //     $('#orden_compra_prov').val(datosRecepcion.numeroOrdenCompra);
                // }

                // $('#fecha_oc').val(datosRecepcion.fechaOrdenCompra);
                // $('#monto_maximo_oc').val(datosRecepcion.montoMaximoOrdenCompra);
                // $('#saldo_oc').val(datosRecepcion.saldoOrdenCompra);
                // Fin Set de la OC

                // Dejar readonly guias_disponible_hermes
                if ($('#guias_disponible_hermes').length > 0) {
                    $('#guias_disponible_hermes').select2("readonly", true);
                }

            } else {
                $('#factura_recepcion_hermes').select2('val', '');
                numeroDocumentoRecepcion = 0;

                // $('#orden_compra_prov').val('');
                // $('#fecha_oc').val('');
                // $('#monto_maximo_oc').val('');
                // $('#saldo_oc').val('');
                if ( $('#valor_total_documento_actualizado').length ) {
                    toastr.info("La diferencia entre el Valor Tot. Act. del documento y el valor total de la recepción no puede ser mayor ni menor de $100", "Atención", optionsToastr);
                } else {
                    toastr.info("La diferencia entre el Valor Total del documento y el valor total de la recepción no puede ser mayor ni menor de $100", "Atención", optionsToastr);
                }
                
            }

        }

    } else {
        numeroDocumentoRecepcion = 0;

        // $('#orden_compra_prov').val('');
        // $('#fecha_oc').val('');
        // $('#monto_maximo_oc').val('');
        // $('#saldo_oc').val('');

        // $('#numero_documento').val('');
        // $('#valor_total_documento').val('');

        $('#numero_documento').removeAttr('readonly');
        $('#valor_total_documento').removeAttr('readonly');

        if ($('#guias_disponible_hermes').length > 0) {
            $('#guias_disponible_hermes').select2("readonly", false);
        }

    }

}

/**
 * Actualiza el monto total de las guias.
 * Debe ser igual al valor total del documento para poder guardar la relacion.
 */
function setTotalMontoGuiasHermes(guiaDespachoSeleccionada, opcion) {

    let guiaDespacho = document.getElementById('guiaDespachoHermes_' + guiaDespachoSeleccionada);
    let totalDocumento = parseInt( $('#valor_total_documento').val().replace(/\./g, '') );
    let totalGuias = parseInt($('#total_monto_guias_hermes').val().replace(/\./g, ''));
    let respuesta = false;

    if (totalDocumento) {
        if (opcion == 'Agregar') {

                totalGuiasNuevo = totalGuias + parseInt(guiaDespacho.dataset.valordocumento);

                // todo bien
                $('#total_monto_guias_hermes').val(formatMoney(totalGuiasNuevo));
                respuesta = true;
                // Dejar readonly factura_recepcion_hermes
                if ( $('#factura_recepcion_hermes').length > 0 ) {
                    $('#factura_recepcion_hermes').select2("readonly", true);
                }

        } else {

            // La opcion es Quitar la guia
            totalGuiasNuevo = totalGuias - parseInt(guiaDespacho.dataset.valordocumento);
            $('#total_monto_guias_hermes').val(formatMoney(totalGuiasNuevo));
            respuesta = true;
            if (totalGuiasNuevo == 0) {
                // Dejar readonly factura_recepcion_hermes
                if ($('#factura_recepcion_hermes').length > 0) {
                    $('#factura_recepcion_hermes').select2("readonly", false);
                }
            }

        }

    } else {
        toastr.info("No se pueden seleccionar Guías de Despacho si el Valor Total del Documento no tiene monto", "Atención", optionsToastr);
    }

    return respuesta;
}

/**
 * Valida que el monto total de las guias de hermes y el valor total del documento coincidan
 * si existe el valor_total_documento_actualizado, se utiliza su valor para validar
 */
function validaTotalGuiasHermesConTotalDocumento() {

    let totalDocumento = 0;
    let totalGuias = parseInt($('#total_monto_guias_hermes').val().replace(/\./g, ''));
    let respuesta = true;

    if ( totalGuias > 0) {
        if ( $('#valor_total_documento_actualizado').length ) {
            totalDocumento = parseInt($('#valor_total_documento_actualizado').val().replace(/\./g, ''));
        } else {
            totalDocumento = parseInt($('#valor_total_documento').val().replace(/\./g, ''));
        }

        /**
         * Si el limite de 8 días para rechazar el documento se supera.
         * No se considera la diferencia en los montos, ya que se debe esperar
         * una nota de credito o debito por la factura.
         */
        diasRechazo = null;
        if ( $('#dias_rechazo').length ) {
            diasRechazo = parseInt( $('#dias_rechazo').val()) ;
        }

        let diferenciaTotales = totalDocumento - totalGuias;

        if ( ! ( ( diasRechazo != null && diasRechazo < 0 ) || ( diferenciaTotales <= 100 && diferenciaTotales >= -100 ) ) ) {
            if ( $('#valor_total_documento_actualizado').length ) {
                toastr.error('La diferencia entre el Valor Tot. Act. del documento y el valor Total Guías de Hermes no puede ser mayor ni menor de $100', 'Atención', optionsToastr);
            } else {
                toastr.error('La diferencia entre el Valor Total del documento y el valor Total Guías de Hermes no puede ser mayor ni menor de $100', 'Atención', optionsToastr);
            }

            respuesta = false;
        }
    }

    return respuesta;

}

/**
 * Trae las ordenes de compra del proveedor
 * 
 */
function getOrdenesCompraProveedor(idProveedor, idContrato = null) {

    if ( ! isNaN(idProveedor) ) {

        $.ajax({
            url: urlGetOrdenesCompraProveedor + '/' + idProveedor + '/' + idContrato,
            type: 'get',

            success: function(respuesta) {
                    //se pasan los datos al select
                    if (respuesta.estado == 'success') {

                        let html = '<label for="orden_compra" class="label-form">Orden de Compra<span class="span-label">*</span></label>  ';
                        html += '<select name="orden_compra" id="orden_compra" class="form-control select2" onchange="getDatosOc(this);" >';
                        html += '<option value="">Seleccione</option>';
                        html += respuesta.options; // se añaden las opciones traidas desde el controlador
                        html += '</select>';

                        $('#setear_select_oc_boleta').html(html);
                        $('#orden_compra').select2();

                        $('#informacion_recepcion').css('display', 'none');
                        $('#informacion_orden_compra').css('display','');
                        $('.ocultar_segun_oc').css('display', 'none');

                    } else if (respuesta.estado == 'error') {

                        // No se encuentran OC
                        let html = '<label for="orden_compra" class="label-form">Orden de Compra<span class="span-label">*</span></label>  ';
                        html += '<select name="orden_compra" id="orden_compra" class="form-control select2" onchange="getDatosOc(this);" >';
                        html += '<option value="">Seleccione</option>';
                        html += '</select>';

                        $('#setear_select_oc_boleta').html(html);
                        $('#orden_compra').select2();

                        $('#informacion_orden_compra').css('display','none');
                        $('.ocultar_segun_oc').css('display', 'none');
                        toastr.warning(respuesta.mensaje, "Atención", optionsToastr);

                    }

                } // succes

        }).fail(function() {

            $('#informacion_orden_compra').css('display','none');
            $('.ocultar_segun_oc').css('display', 'none');

        });


    } else {

        $('#informacion_orden_compra').css('display','none');
        $('.ocultar_segun_oc').css('display', 'none');
        getDatosOc();

    }

}

/**
 * Para mostrar los datos de la oc seleccionada
 */
function getDatosOc(input = null) {

    // console.log(input);
    if ( input != null && input.value != '') {

        var oc = document.getElementById('oc_seleccionada_' + input.value);
        // console.log('oc seleccionada : ' + oc );

        $('#fecha_orden_compra').val(oc.dataset.fechaOc);
        $('#monto_orden_compra').val(oc.dataset.montoOc);
        $('#saldo_orden_compra').val(oc.dataset.saldoOc);
        $('#inicio_vigencia_orden_compra').val(oc.dataset.inicioVigenciaOc);
        $('#fin_vigencia_orden_compra').val(oc.dataset.finVigenciaOc);
        $('.ocultar_segun_oc').css('display', '');

        $('#numero_orden_compra').val( oc.text );

        let periodo = '<input type="text" class="form-control fechas" id="periodo" name="periodo" required >';
        periodo += '<span class="input-group-addon " onclick="fechaDocumento(\'periodo\');" style="cursor:pointer;">';
        periodo += '<i class="fa fa-calendar" aria-hidden="true" style="color: black;"></i></span>';

        $('#div_periodo').html(periodo);

        $('#periodo').datepicker({
            format: 'mm/yyyy',
            // endDate: new Date(),
            startDate: oc.dataset.inicioVigenciaOcPeriodo,
            endDate: oc.dataset.finVigenciaOcPeriodo,
            autoclose: true,
            language: 'es',
            viewMode: 'months',
            minViewMode: 'months'
        });

        // console.log(oc);
        
    } else {

        $('#fecha_orden_compra').val('');
        $('#monto_orden_compra').val('');
        $('#saldo_orden_compra').val('');
        $('#inicio_vigencia_orden_compra').val('');
        $('#fin_vigencia_orden_compra').val('');
        $('.ocultar_segun_oc').css('display', 'none');

    }

    evaluarValorTotalDocumentoConSaldoOcContrato();

}

/**
 * Trae los contratos del proveedor
 * 
 */
function getContratosProveedor(idProveedor) {

    if ( ! isNaN(idProveedor) ) {

        $.ajax({
            url: urlGetContratosProveedor + '/' + idProveedor,
            type: 'get',

            success: function(respuesta) {
                    //se pasan los datos al select
                    if (respuesta.estado == 'success') {

                        let html = '<label for="contrato" class="label-form">Contrato<span class="span-label">*</span></label>';
                        html += '<select name="contrato" id="contrato" class="form-control select2" onchange="getDatosContrato(this);" >';
                        html += '<option value="">Seleccione</option>';
                        html += respuesta.options; // se añaden las opciones traidas desde el controlador
                        html += '</select>';

                        $('#setear_select_contrato').html(html);
                        $('#contrato').select2();

                        $('#informacion_recepcion').css('display', 'none');
                        $('#informacion_orden_compra').css('display','');
                        $('.ocultar_segun_oc').css('display', 'none');

                    } else if (respuesta.estado == 'error') {
                        // no hay contratos disponibles

                        let html = '<label for="contrato" class="label-form">Contrato<span class="span-label">*</span></label>';
                        html += '<select name="contrato" id="contrato" class="form-control select2" onchange="getDatosContrato(this);" >';
                        html += '<option value="">Seleccione</option>';
                        html += '</select>';
                        $('#setear_select_contrato').html(html);
                        $('#contrato').select2();

                        $('.ocultar_segun_contrato').css('display', 'none');
                        $('#informacion_orden_compra').css('display','none');

                        toastr.warning(respuesta.mensaje, "Atención", optionsToastr);

                    }

                } // succes

        }).fail(function() {

            $('#informacion_orden_compra').css('display','none');

        });


    } else {

        $('#informacion_orden_compra').css('display','none');
        getDatosContrato();

    }

}

/**
 * Para mostrar los datos del contrato seleccionado.
 * Se deben traer las OC relacionadas, 
 * si el contrato no se selecciona, traer las oc con el id del proveedor
 */
function getDatosContrato(input = null) {

    // console.log(input);
    if ( input != null && input.value != '') {

        var contrato = document.getElementById('contrato_seleccionado_' + input.value);
        // console.log('contrato seleccionado : ');

        $('#inicio_vigencia_contrato').val(contrato.dataset.inicioVigenciaContrato);
        $('#fin_vigencia_contrato').val(contrato.dataset.finVigenciaContrato);
        $('#monto_contrato').val(contrato.dataset.montoContrato);
        $('#saldo_contrato').val(contrato.dataset.saldoContrato);
        $('#valor_hora_contrato').val(contrato.dataset.valorHora);
        $('.ocultar_segun_contrato').css('display', '');

        $('#tipo_adjudicacion').select2('val', contrato.dataset.tipoAdjudicacion);
        $('#referente_tecnico').select2('val', contrato.dataset.referenteTecnico);

        let periodo = '<input type="text" class="form-control fechas" id="periodo" name="periodo" required >';
        periodo += '<span class="input-group-addon " onclick="fechaDocumento(\'periodo\');" style="cursor:pointer;">';
        periodo += '<i class="fa fa-calendar" aria-hidden="true" style="color: black;"></i></span>';

        $('#div_periodo').html(periodo);

        $('#periodo').datepicker({
            format: 'mm/yyyy',
            // endDate: new Date(),
            startDate: contrato.dataset.inicioVigenciaContratoPeriodo,
            endDate: contrato.dataset.finVigenciaContratoPeriodo,
            autoclose: true,
            language: 'es',
            viewMode: 'months',
            minViewMode: 'months'
        });

        console.log(contrato);

        if ( contrato.dataset.valorHora == 'Por procedimiento' || contrato.dataset.valorHora == '0' ) {

            $('#valor_total_documento').removeAttr('readonly');
            $('#ocultar_segun_valor_hora').css('display', 'none');

        } else {

            $('#valor_total_documento').attr('readonly', 'true');
            $('#ocultar_segun_valor_hora').css('display', '');

        }

        if ( contrato.dataset.saldoPreventivo != '' || contrato.dataset.saldoCorrectivo != '' ) {

            $('#monto_preventivo').val(contrato.dataset.montoPreventivo);
            $('#monto_correctivo').val(contrato.dataset.montoCorrectivo);
            $('#saldo_preventivo').val(contrato.dataset.saldoPreventivo);
            $('#saldo_correctivo').val(contrato.dataset.saldoCorrectivo);
            $('#ocultar_segun_saldos_preventivos_correctivos').css('display', '');

        } else {

            $('#monto_preventivo').val('');
            $('#monto_correctivo').val('');
            $('#saldo_preventivo').val('');
            $('#saldo_correctivo').val('');
            $('#ocultar_segun_saldos_preventivos_correctivos').css('display', 'none');

        }

        getOrdenesCompraProveedor($('#nombre_proveedor').val(), input.value);
        
    } else {

        $('#inicio_vigencia_contrato').val('');
        $('#fin_vigencia_contrato').val('');
        $('#monto_contrato').val('');
        $('#saldo_contrato').val('');
        $('#valor_hora_contrato').val('');
        $('#horas_boleta').val('');
        $('#ocultar_segun_valor_hora').css('display', 'none');
        $('.ocultar_segun_contrato').css('display', 'none');

        $('#monto_preventivo').val('');
        $('#monto_correctivo').val('');
        $('#saldo_preventivo').val('');
        $('#saldo_correctivo').val('');
        $('#ocultar_segun_saldos_preventivos_correctivos').css('display', 'none');

    }

    evaluarValorTotalDocumentoConSaldoOcContrato();


}

function ingresoHoras(input) {

    $("#" + input.id).val(formatMoneyDecimal(($("#" + input.id).val()).replace(/\./g, '')));
    setTotalBoleta();
        
}

/**
 * Da el monto de la boleta dependiendo de la cantidad de horas ingresadas
 */
function setTotalBoleta() {

    let valorHora = parseInt($('#valor_hora_contrato').val().replace(/\./g, ''));
    if ( valorHora != 0 && valorHora != '' ) {

        let horas = parseFloat( $('#horas_boleta').val().replace(/\./g, '').replace(/\,/g, '.') );
        let totalBoleta = horas * valorHora;

        $('#valor_total_documento').val( formatMoney( parseInt( Math.round(totalBoleta) ) ) );
        $('#valor_total_documento').attr("readonly", true);
        evaluarValorTotalDocumentoConSaldoOcContrato();
        setMontosParaBoleta();

    } else {

        $('#valor_total_documento').attr("readonly", false);

    }

}

/**
 * Evalua que el valor total del documento no supere el saldo de
 * la orden de compra y del contrato.
 * Se debe evaluar primero con el saldo de la OC (que depende del contrato)
 * Si no existe el saldo de la OC, se evalua el saldo del contrato
 */
function evaluarValorTotalDocumentoConSaldoOcContrato() {

    let totalDocumento = parseInt($('#valor_total_documento').val().replace(/\./g, ''));
    let saldoOc = parseInt($('#saldo_orden_compra').val().replace(/\./g, ''));
    let saldoContrato = parseInt($('#saldo_contrato').val().replace(/\./g, ''));
    let valorHora = parseInt($('#valor_hora_contrato').val().replace(/\./g, ''));
    let saldoPreventivo = parseInt($('#saldo_preventivo').val().replace(/\./g, ''));
    let saldoCorrectivo = parseInt($('#saldo_correctivo').val().replace(/\./g, ''));

    let montoOc = parseInt($('#monto_orden_compra').val().replace(/\./g, ''));
    let montoContrato = parseInt($('#monto_contrato').val().replace(/\./g, ''));
    let montoPreventivo = parseInt($('#monto_preventivo').val().replace(/\./g, ''));
    let montoCorrectivo = parseInt($('#monto_correctivo').val().replace(/\./g, ''));

    // NC y CCC
    if ( $('#tipo_documento').val() == 4 || $('#tipo_documento').val() == 10 ) {

        if ( $('#saldo_preventivo').val() != '' &&  $('#saldo_correctivo').val() != '' ) {

            if ( $('input:radio[name=montoOcupar]:checked').val() === 'preventivo' ) {

                if ( ( totalDocumento + saldoPreventivo > montoPreventivo ) ) {
        
                    $('#valor_total_documento').val( formatMoney( saldoPreventivo - montoPreventivo ) );            
                    toastr.warning('El Valor Total más el saldo preventivo no puede superar el monto preventivo', 'Atención', optionsToastr);                    
        
                }

            } else if ( $('input:radio[name=montoOcupar]:checked').val() === 'correctivo' ) {

                if ( ( totalDocumento + saldoCorrectivo > montoCorrectivo ) ) {
        
                    $('#valor_total_documento').val( formatMoney( saldoCorrectivo - montoCorrectivo ) );            
                    toastr.warning('El Valor Total más el saldo Correctivo no puede superar el monto Correctivo', 'Atención', optionsToastr);                    
        
                }

            }

        } else {

            if ( ! isNaN(saldoOc) && ( totalDocumento + saldoOc > montoOc ) ) {

                $('#valor_total_documento').val(formatMoney(saldoOc - montoOc));            
                toastr.warning('El Valor Total más el saldo de la Orden de Compra no puede superar el monto de la Orden de Compra', 'Atención', optionsToastr);

                if ( !isNaN(valorHora) && valorHora != 'Por procedimiento' && valorHora != 0 && valorHora != '' ) {

                    horasMaximas = (saldoOc - montoOc) / valorHora;
                    $('#horas_boleta').val( formatMoney( parseInt( horasMaximas ) ) );

                }
                    

            } else {

                if ( ! isNaN(saldoContrato) && ( totalDocumento + saldoContrato > montoContrato) ) {

                    $('#valor_total_documento').val(formatMoney(saldoContrato - montoContrato));            
                    toastr.warning('El Valor Total más el saldo del contrato no puede superar el monto del Contrato', 'Atención', optionsToastr);

                    if ( !isNaN(valorHora) && valorHora != 'Por procedimiento' && valorHora != 0 && valorHora != '' ) {

                        horasMaximas = (saldoContrato - montoContrato) / valorHora;
                        $('#horas_boleta').val( formatMoney( parseInt( horasMaximas ) ) );

                    }
                    
                }

            }

        }

    } else {

        if ( $('#saldo_preventivo').val() != '' &&  $('#saldo_correctivo').val() != '' ) {

            if ( $('input:radio[name=montoOcupar]:checked').val() === 'preventivo' ) {

                if ( ( totalDocumento > saldoPreventivo ) ) {
        
                    $('#valor_total_documento').val(formatMoney(saldoPreventivo));            
                    toastr.warning('El Valor Total no puede superar el saldo preventivo', 'Atención', optionsToastr);                    
        
                }

            } else if ( $('input:radio[name=montoOcupar]:checked').val() === 'correctivo' ) {

                if ( ( totalDocumento > saldoCorrectivo ) ) {
        
                    $('#valor_total_documento').val(formatMoney(saldoCorrectivo));            
                    toastr.warning('El Valor Total no puede superar el saldo correctivo', 'Atención', optionsToastr);                    
        
                }

            }

        } else {

            if ( ! isNaN(saldoOc) && ( totalDocumento > saldoOc ) ) {

                $('#valor_total_documento').val(formatMoney(saldoOc));            
                toastr.warning('El Valor Total no puede superar el saldo de la Orden de Compra', 'Atención', optionsToastr);

                if ( !isNaN(valorHora) && valorHora != 'Por procedimiento' && valorHora != 0 && valorHora != '' ) {

                    horasMaximas = saldoOc / valorHora;
                    $('#horas_boleta').val( formatMoney( parseInt( horasMaximas ) ) );

                }

            } else {

                if ( ! isNaN(saldoContrato) && ( totalDocumento > saldoContrato ) ) {

                    $('#valor_total_documento').val(formatMoney(saldoContrato));            
                    toastr.warning('El Valor Total no puede superar el saldo del contrato', 'Atención', optionsToastr);

                    if ( !isNaN(valorHora) && valorHora != 'Por procedimiento' && valorHora != 0 && valorHora != '' ) {

                        horasMaximas = saldoContrato / valorHora;
                        $('#horas_boleta').val( formatMoney( parseInt( horasMaximas ) ) );

                    }
                    
                }

            }

        }
    
    }

}

function montoOcuparPreventivoCorrectivo() {

    let totalDocumento = parseInt($('#valor_total_documento').val().replace(/\./g, ''));
    let saldoPreventivo = parseInt($('#saldo_preventivo').val().replace(/\./g, ''));
    let saldoCorrectivo = parseInt($('#saldo_correctivo').val().replace(/\./g, ''));
    let montoPreventivo = parseInt($('#monto_preventivo').val().replace(/\./g, ''));
    let montoCorrectivo = parseInt($('#monto_correctivo').val().replace(/\./g, ''));

    if ( $('#saldo_preventivo').val() != '' &&  $('#saldo_correctivo').val() != '' ) {

        if ( $('input:radio[name=montoOcupar]:checked').val() === 'preventivo' ) {

            if ( $('#tipo_documento').val() == 4 || $('#tipo_documento').val() == 10 ) {

                if ( ( totalDocumento + saldoPreventivo > montoPreventivo ) ) {

                    $('#valor_total_documento').val( formatMoney( saldoPreventivo - montoPreventivo ) );
                    toastr.warning('El Valor Total más el saldo preventivo no puede superar el monto preventivo', 'Atención', optionsToastr);

                }

            } else {

                if ( ( totalDocumento > saldoPreventivo ) ) {
        
                    $('#valor_total_documento').val(formatMoney(saldoPreventivo));            
                    toastr.warning('El Valor Total no puede superar el saldo preventivo', 'Atención', optionsToastr);                    
        
                }

            }

        } else if ( $('input:radio[name=montoOcupar]:checked').val() === 'correctivo' ) {

            if ( $('#tipo_documento').val() == 4 || $('#tipo_documento').val() == 10 ) {

                if ( ( totalDocumento + saldoCorrectivo > montoCorrectivo ) ) {
        
                    $('#valor_total_documento').val( formatMoney( saldoCorrectivo - montoCorrectivo ) );            
                    toastr.warning('El Valor Total más el saldo correctivo no puede superar el monto correctivo', 'Atención', optionsToastr);                    
        
                }

            } else {

                if ( ( totalDocumento > saldoCorrectivo ) ) {
        
                    $('#valor_total_documento').val(formatMoney(saldoCorrectivo));            
                    toastr.warning('El Valor Total no puede superar el saldo correctivo', 'Atención', optionsToastr);                    
        
                }

            }

        }

    }

}