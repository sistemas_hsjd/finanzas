function esperandoCrear() {

    $('.page-header-fixed *').css('cursor', 'wait');
    $("#botonGuardar").attr("disabled",true);

}

function listoCrear() {

    $('.page-header-fixed *').css('cursor', '');
    $("#botonGuardar").attr("disabled",false);

}

function mostrarErroresValidator(respuesta) {
    if ( respuesta.responseJSON ) {
        //console.log(respuesta.responseJSON);
        let htmlErrores = '';
        for (let k in respuesta.responseJSON) { 
            //console.log(k, respuesta.responseJSON[k]);
            htmlErrores += '<li>' + respuesta.responseJSON[k] + '</li>';
        }

        $('#ulErrores').html(htmlErrores);
        $('#divErrores').css('display','');
        toastr.error('No es posible realizar la acción'+'<br>'+'Errores:<br> <ul>'+htmlErrores+'</ul>', 'Atención', optionsToastr);
    }
}

/**
 * Da formato a los valores ingresados en los input que sean de dinero
 */
function ingresoPesos(input) {

    if ( input.id.indexOf('monto_contrato') == 0 && $('#tipo_contrato').val() == 2 ) {

        validarMontoContrato(input);

    } else if ( input.id.indexOf('monto_contrato') == 0 && $('#tipo_contrato').val() != 2 ) {

        $("#" + input.id).val(formatMoney(($("#" + input.id).val()).replace(/\./g, '')));

    } else if ( input.id.indexOf('monto_preventivo') == 0 || input.id.indexOf('monto_correctivo') == 0 ) {
        
        // evaluar junto con la suma del contrato
        validarMontosPreventivoCorrectivo(input);

    } else {

        $("#" + input.id).val(formatMoney(($("#" + input.id).val()).replace(/\./g, '')));

    }

    if ( input.id.indexOf('valor_hora') == 0 ) {

        setPorProcedimiento();

    }
    
}

function validarMontosPreventivoCorrectivo(input) {

    let montoContrato = parseInt($('#monto_contrato').val().replace(/\./g, ''));
    let diferenciaMontos = 0;

    if ( input.id.indexOf('monto_preventivo') == 0  ) {

        if ( $('#monto_correctivo').val() == '' ) {

            $('#monto_correctivo').val(0);

        }

        diferenciaMontos = montoContrato - parseInt( $('#monto_correctivo').val().replace(/\./g, '') );

    } else if ( input.id.indexOf('monto_correctivo') == 0  ) {

        if ( $('#monto_preventivo').val() == '' ) {

            $('#monto_preventivo').val(0);

        }

        diferenciaMontos = montoContrato - parseInt($('#monto_preventivo').val().replace(/\./g, ''));

    }


    let valorIngresado = parseInt($('#' + input.id).val().replace(/\./g, ''));

    if ( valorIngresado > diferenciaMontos ) {

        toastr.warning('<strong> La suma de los montos preventivo y correctivo, no puede superar el monto del contrato </strong>', 'Atención', optionsToastr);
        $("#" + input.id).val(formatMoney(diferenciaMontos));
        

    } else {

        if ( input.id.indexOf('monto_preventivo') == 0  ) {

            if( valorIngresado < parseInt(montoMinimoPreventivo) ) {

                $("#" + input.id).val(formatMoney( montoMinimoPreventivo ) );
                toastr.warning('<strong style="color:black;">el monto Preventivo no puede ser menor a lo consumido por los documentos</strong>', 'Atención', optionsToastr);
    
            } else if ( isNaN(valorIngresado) ) {
    
                $("#" + input.id).val(formatMoney( montoMinimoPreventivo ) );
                if ( parseInt(montoMinimoPreventivo) > 0 ) {
                    toastr.warning('<strong style="color:black;">el monto Preventivo no puede ser menor a lo consumido por los documentos</strong>', 'Atención', optionsToastr);
                }
    
            } else {

                $("#" + input.id).val(formatMoney(($("#" + input.id).val()).replace(/\./g, '')));

            }

        } else if ( input.id.indexOf('monto_correctivo') == 0  ) {

            if( valorIngresado < parseInt(montoMinimoCorrectivo) ) {

                $("#" + input.id).val(formatMoney( montoMinimoCorrectivo ) );
                toastr.warning('<strong style="color:black;">el monto Correctivo no puede ser menor a lo consumido por los documentos</strong>', 'Atención', optionsToastr);
    
            } else if ( isNaN(valorIngresado) ) {
    
                $("#" + input.id).val(formatMoney( montoMinimoCorrectivo ) );
                if ( parseInt(montoMinimoCorrectivo) > 0 ) {
                    toastr.warning('<strong style="color:black;">el monto Correctivo no puede ser menor a lo consumido por los documentos</strong>', 'Atención', optionsToastr);
                }
    
            } else {

                $("#" + input.id).val(formatMoney(($("#" + input.id).val()).replace(/\./g, '')));

            }

        }

    }

}

function validarMontoContrato(input) {

    // el monto del contrato no puede ser menor a la suma de los montos preventivo y correctivo
    let valorIngresado = parseInt($('#' + input.id).val().replace(/\./g, ''));
    let totalMontosPreventivoCorrectivo = 0;

    if ( $('#monto_correctivo').val() == '' ) {

        $('#monto_correctivo').val(0);

    }

    if ( $('#monto_preventivo').val() == '' ) {

        $('#monto_preventivo').val(0);

    }

    totalMontosPreventivoCorrectivo = parseInt($('#monto_preventivo').val().replace(/\./g, '')) + parseInt( $('#monto_correctivo').val().replace(/\./g, '') );

    if ( valorIngresado < totalMontosPreventivoCorrectivo ) {

        toastr.warning('<strong> El monto del contrato no puede ser menor a la suma de los montos preventivo y correctivo </strong>', 'Atención', optionsToastr);
        $("#" + input.id).val(formatMoney(totalMontosPreventivoCorrectivo));

    } else {

        if( valorIngresado < parseInt(montoMinimoContrato) ) {

            $("#" + input.id).val(formatMoney( montoMinimoContrato ) );
            toastr.warning('<strong style="color:black;">el monto Contrato no puede ser menor a lo consumido por los documentos</strong>', 'Atención', optionsToastr);

        } else if ( isNaN(valorIngresado) ) {

            $("#" + input.id).val(formatMoney( montoMinimoContrato ) );
            if ( parseInt(montoMinimoContrato) > 0 ) {
                toastr.warning('<strong style="color:black;">el monto Contrato no puede ser menor a lo consumido por los documentos</strong>', 'Atención', optionsToastr);
            }

        } else {

            $("#" + input.id).val(formatMoney(($("#" + input.id).val()).replace(/\./g, '')));

        }

    }

}

/**
 * Valida el archivo seleccionado para subir
 */
function validarArchivo() {

    let archivo = $('#archivo').val().replace(/.*(\/|\\)/, ''); //val();
    let extension = archivo.substring(archivo.lastIndexOf('.'));
    if (archivo == '') {
        $('#div_archivo').css('display', 'none');
        tablaArchivo.clear().draw(); // se remueven todas las filas de la tabla
        return false;
    } else {
        if (extension != '.pdf') {
            toastr.error('El archivo no tiene la extension correcta: ' + '<strong>PDF</strong>', 'Atención', optionsToastr);
            $('#archivo').val('');
            $('#div_archivo').css('display', 'none');
            tablaArchivo.clear().draw(); // se remueven todas las filas de la tabla
            return false;
        } else {
            mostrarInfoArchivo(archivo);
            return true;
        }
    }

}

/**
 * Muestra la informacion del archivo seleccionado para subir
 */
function mostrarInfoArchivo(archivo) {

    tablaArchivo.clear().draw(); // se remueven todas las filas de la tabla
    let nombreArchivo = archivo.substring(0, archivo.lastIndexOf('.'));
    let tamañoArchivo = $('#archivo')[0].files[0].size;

    tablaArchivo.row.add([
        nombreArchivo,
        tamañoArchivo + " bits",
        selectTipoArchivo
    ]).draw(false);

    $('#div_archivo').css('display', '');

}

function setTipoContrato(input) {

    if (input.value != '') {

        $('#botonGuardar').css('display','');

        if ( input.value == 1 ) {

            $('#honorarios').css('display', '');
            $('#honorarios .requerido').attr("required", true);

            $('#convenios').css('display', 'none');
            $('#convenios .requerido').attr("required", false);

        } else {

            $('#honorarios').css('display', 'none');
            $('#honorarios .requerido').attr("required", false);

            $('#convenios').css('display', '');
            $('#convenios .requerido').attr("required", true);

        }

    } else {

        $('#honorarios').css('display', 'none');
        $('#convenios').css('display', 'none');
        $('#botonGuardar').css('display','none');
        
        $('#honorarios .requerido').attr("required", false);
        $('#convenios .requerido').attr("required", false);

    }

}

/**
 * Habilita el checked de por procedimiento
 */
function setPorProcedimiento() {

    if ( $('#valor_hora').val() != 0 ) {

        $('#por_procedimiento').attr('readonly', true);
        $('#por_procedimiento').attr("disabled", true);

    } else {

        $('#por_procedimiento').attr('readonly', false);
        $('#por_procedimiento').attr("disabled", false);

    }
    
}

function setValorHora() {

    if ( $('#por_procedimiento').prop('checked') ) {

        $('#valor_hora').attr('readonly', true);
        $('#valor_hora').attr("disabled", true);
        $('#valor_hora').val('');

    } else {

        $('#valor_hora').attr('readonly', false);
        $('#valor_hora').attr("disabled", false);

    }

}